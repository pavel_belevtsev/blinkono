//
//  UIAlertView+Blocks.h
//  
//
//  Created by Pavel Belevtsev on 26.11.13.
//  Copyright (c) 2013 Synergetica. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIAlertView (Blocks)

- (void)showWithCompletion:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion;

@end
