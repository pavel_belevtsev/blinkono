//
//  UIButton+Color.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 2/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Color)

- (void)setBackgroundColor:(UIColor *)color forState:(UIControlState)state;
@end
