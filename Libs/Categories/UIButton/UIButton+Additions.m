//
//  UIButton+Additions.m
//  KalturaUnified
//
//  Created by Alex Zchut on 3/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "UIButton+Additions.h"

static NSString *kBtnClickEnabled = @"btnClickEnabled";

@implementation UIButton(Additions)

- (BOOL) clickEnabled {
    NSNumber *value = objc_getAssociatedObject(self, &kBtnClickEnabled);
    return (value) ? [value boolValue] : TRUE;
}

-(void)setClickEnabled:(BOOL)enabled {
    objc_setAssociatedObject(self, &kBtnClickEnabled, @(enabled),  OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void) centerTitleAndImageWithSpacing:(CGFloat)spacing {
    self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
    self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
}

@end
