//
//  TVUnderlineButton.m
//  TVinciCustomDemo
//
//  Created by Pavel Belevtsev on 10.07.13.
//  Copyright (c) 2013 synergetica. All rights reserved.
//

#import "TVUnderlineButton.h"

@implementation TVUnderlineButton

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setDefaults];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setDefaults];
    }
    return self;
}

- (id)initWithView:(UIView *)view {
    
    self = [super initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    if (self) {
        
        [view addSubview:self];
        [self setDefaults];
    }
    return self;
    
}

- (void) setDefaults {
    [self addTarget: self action: @selector(touchDown:) forControlEvents: UIControlEventTouchDown];
    [self addTarget: self action: @selector(touchDown:) forControlEvents: UIControlEventTouchDragEnter];
    
    [self addTarget: self action: @selector(touchUp:) forControlEvents: UIControlEventTouchUpInside];
    [self addTarget: self action: @selector(touchUp:) forControlEvents: UIControlEventTouchUpOutside];
    [self addTarget: self action: @selector(touchUp:) forControlEvents: UIControlEventTouchCancel];
    [self addTarget: self action: @selector(touchUp:) forControlEvents: UIControlEventTouchDragExit];
    self.underlinePosition = -2.0;
}

- (void)touchDown: (id)sender
{
    isPressed = YES;
    [self setNeedsDisplay];
}

- (void)touchUp: (id)sender
{
    isPressed = NO;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    if (_underlineNormal || isPressed) {

        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetStrokeColorWithColor(context, self.currentTitleColor.CGColor);
        
        // Draw them with a 1.0 stroke width.
        CGContextSetLineWidth(context, 1.0);
        
        // Work out line width
        NSString *text = self.titleLabel.text;
        //CGSize sz = [text sizeWithFont:self.titleLabel.font forWidth:rect.size.width lineBreakMode:self.titleLabel.lineBreakMode];
        
        CGSize sz = [text boundingRectWithSize:rect.size
                                       options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{NSFontAttributeName:self.titleLabel.font}
                                       context:nil].size;
        
        CGFloat width = rect.size.width;
        CGFloat offset = (rect.size.width - sz.width) / 2;
        
        if (offset > 0 && offset < rect.size.width) {
            width -= offset;
        } else {
            offset = 0.0;
        }
        
        if (self.contentHorizontalAlignment == UIControlContentHorizontalAlignmentLeft) {
        
            width -= offset;
            offset = 0;
            
        }
        
        CGFloat baseline = rect.size.height + self.titleLabel.font.descender + self.underlinePosition - 0.5;
        
        // Draw a single line from left to right
        CGContextMoveToPoint(context, offset, baseline);
        CGContextAddLineToPoint(context, width, baseline);
        CGContextStrokePath(context);

    }
    
}

-(void) setUnderlinePosition:(CGFloat)underlinePosition {
    _underlinePosition = underlinePosition;
    
    [self setNeedsDisplay];
}

-(void) setUnderlineNormal:(BOOL)underlineNormal {
    _underlineNormal = underlineNormal;
    
    [self setNeedsDisplay];
}

@end
