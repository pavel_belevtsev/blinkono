//
//  UIButton+Block.m
//  KalturaUnified
//
//  Created by Alex Zchut on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "UIButton+Block.h"
#import <objc/runtime.h>

@implementation UIButton (Block)

static char overviewKey;

@dynamic actions;

- (void) setTouchUpInsideAction:(void(^)(UIButton *btn))block {
    if (!block) {
        self.actions = nil;
        return;
    }
    if ([self actions] == nil) {
        [self setActions:[[NSMutableDictionary alloc] init]];
    }
    [[self actions] setObject:block forKey:kUIButtonBlockTouchUpInside];
    [self addTarget:self action:@selector(doTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) setAction:(NSString*)action withBlock:(void(^)(UIButton *btn))block {
    if ([self actions] == nil) {
        [self setActions:[[NSMutableDictionary alloc] init]];
    }
    
    [[self actions] setObject:block forKey:action];
    
    if ([kUIButtonBlockTouchUpInside isEqualToString:action]) {
        [self addTarget:self action:@selector(doTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
}


- (void)setActions:(NSMutableDictionary*)actions {
    objc_setAssociatedObject (self, &overviewKey,actions,OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableDictionary*)actions {
    return objc_getAssociatedObject(self, &overviewKey);
}

- (void)doTouchUpInside:(id)sender {
    void(^block)(UIButton *btn);
    block = [self actions][kUIButtonBlockTouchUpInside];
    if (block)
        block(self);
}

@end