//
//  UIButton+Block.h
//  KalturaUnified
//
//  Created by Alex Zchut on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kUIButtonBlockTouchUpInside @"TouchInsideWithButton"

#import <UIKit/UIKit.h>

@class UIButton;

@interface UIButton (Block)

@property (nonatomic, strong) NSMutableDictionary *actions;

- (void) setAction:(NSString*)action withBlock:(void(^)(UIButton *btn))block;
- (void) setTouchUpInsideAction:(void(^)(UIButton *btn))block;
@end