//
//  UIButton+Additions.h
//  KalturaUnified
//
//  Created by Alex Zchut on 3/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton(Additions)

@property (nonatomic, readwrite) BOOL clickEnabled;

-(void) centerTitleAndImageWithSpacing:(CGFloat)spacing;

@end