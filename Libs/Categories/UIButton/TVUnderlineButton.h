//
//  TVUnderlineButton.h
//  TVinciCustomDemo
//
//  Created by Pavel Belevtsev on 10.07.13.
//  Copyright (c) 2013 synergetica. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TVUnderlineButton : UIButton {
    
    BOOL isPressed;
    
}

- (id)initWithView:(UIView *)view;

@property (nonatomic, assign) CGFloat underlinePosition;
@property (nonatomic, assign) BOOL underlineNormal;

@end
