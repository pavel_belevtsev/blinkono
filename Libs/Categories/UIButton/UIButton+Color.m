//
//  UIButton+Color.m
//  KalturaUnified
//
//  Created by Rivka Schwartz on 2/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "UIButton+Color.h"

@implementation UIButton (Color)

- (void)setBackgroundColor:(UIColor *)color forState:(UIControlState)state
{
    UIView *colorView = [[UIView alloc] initWithFrame:self.frame];
    colorView.backgroundColor = color;
    
    UIGraphicsBeginImageContext(colorView.bounds.size);
    [colorView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self setBackgroundImage:colorImage forState:state];
}
@end
