//
//  Array+Additions.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 7/15/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import Foundation

extension Array {
    mutating func remove <U: Equatable> (object: U) {
        for i in stride(from: self.count-1, through: 0, by: -1) {
            if let element = self[i] as? U {
                if element == object {
                    self.removeAtIndex(i)
                }
            }
        }
    }
}