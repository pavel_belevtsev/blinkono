//
//  TVRental+Additions.m
//  KalturaUnified
//
//  Created by Israel Berezin on 6/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVRental+Additions.h"
#import <objc/runtime.h>

static NSString *kProductCode = @"ProductCode";
static NSString *kPriceObj = @"PriceObj";
static NSString *kCurrencySign = @"currencySign";


@implementation TVRental (Additions)

- (NSString*) productCode {
    return objc_getAssociatedObject(self, &kProductCode);
}

-(void)setProductCode:(NSString*)value {
    objc_setAssociatedObject(self, &kProductCode, value,  OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString*) price {
    return objc_getAssociatedObject(self, &kPriceObj);
}

-(void)setPrice:(NSString*)value {
    objc_setAssociatedObject(self, &kPriceObj, value,  OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString*) currencySign {
    return objc_getAssociatedObject(self, &kCurrencySign);
}

-(void)setCurrencySign:(NSString*)value {
    objc_setAssociatedObject(self, &kCurrencySign, value,  OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
