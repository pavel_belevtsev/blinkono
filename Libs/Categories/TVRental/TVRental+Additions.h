//
//  TVRental+Additions.h
//  KalturaUnified
//
//  Created by Israel Berezin on 6/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TVRental.h>

@interface TVRental (Additions)
@property (nonatomic, copy) NSString* productCode;
@property (nonatomic, copy) NSString* price;
@property (nonatomic, copy) NSString* currencySign;

@end
