//
//  UIDevice+Network.h
//  KalturaUnified
//
//  Created by Alex Zchut on 6/22/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Network)

- (BOOL) supportsCellularNetwork;

@end
