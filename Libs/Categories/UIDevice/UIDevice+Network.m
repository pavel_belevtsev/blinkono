    //
//  UIDevice+Network.m
//  KalturaUnified
//
//  Created by Alex Zchut on 6/22/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <ifaddrs.h>
#import "UIDevice+Network.h"

@implementation UIDevice (Network)

- (BOOL) supportsCellularNetwork {
    struct ifaddrs * addrs;
    const struct ifaddrs * cursor;
    bool found = false;
    if (getifaddrs(&addrs) == 0) {
        cursor = addrs;
        while (cursor != NULL) {
            NSString *name = [NSString stringWithUTF8String:cursor->ifa_name];
            if ([name isEqualToString:@"pdp_ip0"]) {
                found = true;
                break;
            }
            cursor = cursor->ifa_next;
        }
        freeifaddrs(addrs);
    }
    return found;
}

@end
