//
//  UIDevice+Orientation.h
//  KalturaUnified
//
//  Created by Alex Zchut on 1/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

@interface UIDevice (Orientation)
-(void)setOrientation:(UIInterfaceOrientation)orientation animated:(BOOL)animated;
-(void)setOrientation:(UIInterfaceOrientation)orientation;
@end
