//
//  NSDictionary+Additions.h
//  KalturaUnified
//
//  Created by Alex Zchut on 2/16/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

@interface NSDictionary (Additions)
-(NSString*) JSONstringWithPrettyPrint:(BOOL) prettyPrint;
-(NSString*) JSONstring;
- (id)objectForCaseInsensitiveKey:(NSString *)key;
@end