//
//  TVMixedCompanionManager+Additions.h
//  
//
//  Created by Israel Berezin on 29/10/2015.
//
//
#import <TvinciSDK/TVMixedCompanionManager.h>


typedef enum CompanionViewUIState
{
    WhatsThis = 0,
    Connecting,
    Connected,
    ScanForDevices,
    DevicesNotFound,
    DevicesFound,
    Castting
}CompanionViewUIState;

typedef NS_ENUM(NSInteger, CompanionPlayerState)
{
    CompanionPlayerStateUnknown = 0,
    CompanionPlayerStatePlay,
    CompanionPlayerStatePause,
    CompanionPlayerStateStop,
    CompanionPlayerStateError
};

@interface TVMixedCompanionManager (Additions)

@property (nonatomic, readwrite) NSInteger currentUIState;
@property (nonatomic, readwrite) CompanionPlayerState currentCompanionPlayerState;
@property (nonatomic, strong) TVMediaItem * casstingMediaItem;

@end
