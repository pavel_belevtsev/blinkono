//
//  TVMixedCompanionManager+Additions.m
//  
//
//  Created by Israel Berezin on 29/10/2015.
//
//

#import "TVMixedCompanionManager+Additions.h"
#import <objc/runtime.h>


static NSString *kCastingKey = @"CastingKey";
static NSString *kCurrentCompanionPlayerStateKey = @"currentCompanionPlayerStateKey";
static NSString *kCasstingMediaItem = @"currentCompanionCasstingMediaItem";

@implementation TVMixedCompanionManager (Additions)

- (NSInteger) currentUIState {
    NSNumber *type = objc_getAssociatedObject(self, &kCastingKey);
    return (type) ? [type integerValue] : 0;
}

-(void)setCurrentUIState:(NSInteger)currentUIState {
    objc_setAssociatedObject(self, &kCastingKey, @(currentUIState),  OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CompanionPlayerState) currentCompanionPlayerState {
    NSNumber *type = objc_getAssociatedObject(self, &kCurrentCompanionPlayerStateKey);
    return (type) ? [type integerValue] : 0;
}

-(void)setCurrentCompanionPlayerState:(CompanionPlayerState)currentCompanionPlayerState{
    objc_setAssociatedObject(self, &kCurrentCompanionPlayerStateKey, @(currentCompanionPlayerState),  OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (TVMediaItem *) casstingMediaItem {
    return objc_getAssociatedObject(self, &kCasstingMediaItem);
}

-(void)setCasstingMediaItem:(TVMediaItem *)casstingMediaItem{
    objc_setAssociatedObject(self, &kCasstingMediaItem, casstingMediaItem,  OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end
