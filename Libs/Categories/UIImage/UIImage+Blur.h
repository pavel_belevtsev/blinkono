//
//  UIImage+Blur.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Blur)

- (UIImage *)blurredImageWithRadius:(float)radius;

@end
