//
//  BaseModelObject+Additions.h
//  KalturaUnified
//
//  Created by Alex Zchut on 4/22/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

@interface BaseModelObject (Additions)

- (id)initWithCoder:(NSCoder *)decoder;
- (void) encodeWithCoder:(NSCoder *)encoder;
- (NSDictionary *)dictionaryValue;
@end
