//
//  TVMediaItem+Additions.m
//  KalturaUnified
//
//  Created by Alex Zchut on 4/22/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseModelObject+Additions.h"
#import "NSData+Base64.h"

@implementation BaseModelObject (Additions)

- (void) encodeWithCoder:(NSCoder *)encoder {
    [[self dictionaryValue] enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop){
        if ([obj respondsToSelector:@selector(encodeWithCoder:)]) {
            if (obj)
                [encoder encodeObject:obj forKey:key];
        }
    }];
}

- (id)initWithCoder:(NSCoder *)decoder {
    
    if (self = [self init])
    {
        NSMutableArray *propertyKeys = [self.class propertyKeys];
        // Loop through the properties
        for (NSString *key in propertyKeys)
        {
            // Decode the property, and use the KVC setValueForKey: method to set it
            id value = [decoder decodeObjectForKey:key];
            if (![value isKindOfClass:[NSNull class]])
                [self setValue:value forKey:key];
        }
    }
    return self;
}

+ (NSMutableArray *) propertyKeys {
    NSMutableArray *propertyKeys = [NSMutableArray array];
    Class currentClass = self.class;
    
    while ([currentClass superclass]) { // avoid printing NSObject's attributes
        unsigned int outCount, i;
        objc_property_t *properties = class_copyPropertyList(currentClass, &outCount);
        for (i = 0; i < outCount; i++) {
            objc_property_t property = properties[i];
            const char *propName = property_getName(property);
            if (propName) {
                NSString *propertyName = [NSString stringWithUTF8String:propName];
                [propertyKeys addObject:propertyName];
            }
        }
        free(properties);
        currentClass = [currentClass superclass];
    }
    return propertyKeys;
}

- (NSDictionary *)dictionaryValue
{
    NSMutableArray *propertyKeys = [self.class propertyKeys];
    return [self dictionaryWithValuesForKeys:propertyKeys];
}

@end
