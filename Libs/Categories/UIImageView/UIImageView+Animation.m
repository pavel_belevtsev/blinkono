//
//  UIImageView+Animation.m
//  
//
//  Created by Pavel Belevtsev on 18.06.14.
//  Copyright (c) 2014 Synergetica. All rights reserved.
//

#import "UIImageView+Animation.h"
#import "UIImage+Tint.h"

@implementation UIImageView (Animation)

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)startBrandAnimation {

    [self.layer removeAllAnimations];
    
    self.image = [[UIImage imageNamed:@"spinner"] imageTintedWithColor:APST.brandColor];
    
    self.hidden = NO;
    
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = @(M_PI * 2.0);
    rotationAnimation.duration = 1.0;
    rotationAnimation.repeatCount = HUGE_VALF;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [self.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
}

- (void)stopBrandAnimation {
    
    self.hidden = YES;
    
    [self.layer removeAllAnimations];
    
}

@end
