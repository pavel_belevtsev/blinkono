//
//  UIImageView+Animation.h
//  
//
//  Created by Pavel Belevtsev on 18.06.14.
//  Copyright (c) 2014 Synergetica. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Animation)

- (void)startBrandAnimation;
- (void)stopBrandAnimation;

@end
