//
//  UIView+Additions.h
//  Tvinci
//
//  Created by Orr Matarasso on 8/8/13.
//  Copyright (c) 2013 Sergata Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    UIViewFlipAnimationTypeFlipFromLeft,
    UIViewFlipAnimationTypeFlipFromRight,
} UIViewFlipAnimationType;

@interface UIView (Additions)


@property CGPoint position;
@property CGFloat x;
@property CGFloat y;
@property CGFloat top;
@property CGFloat bottom;
@property CGFloat left;
@property CGFloat right;

@property CGSize size;
@property CGFloat width;
@property CGFloat height;

/*
 * Method to remove all subviews
 */
-(void) removeAllSubviews;
-(void) removeAllSubviewsExceptSubview:(UIView*)subview;
-(void) removeAllSubviewsExceptSubviewIsKindOfClass:(Class)cls;
-(void) removeAllSubviewsKindOfClass:(Class)cls;
-(void) performBlockOnSubviewsKindOfClass:(Class)cls block:(void(^)(UIView* subview))block;

- (UIView*)descendantOrSelfWithClass:(Class)cls;

/**
 * Shortcut methods for flipping a UIView into another UIView
 */
+ (void)flipFromView:(UIView *)fromView toView:(UIView *)toView duration:(NSTimeInterval)duration;
+ (void)flipFromView:(UIView *)fromView toView:(UIView *)toView withType:(UIViewFlipAnimationType)animationType duration:(NSTimeInterval)duration;


@end