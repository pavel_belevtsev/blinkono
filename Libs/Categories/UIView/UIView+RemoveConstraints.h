//
//  UIView+RemoveConstraints.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 6/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RemoveConstraints)
- (void)removeAllConstraints;

@end
