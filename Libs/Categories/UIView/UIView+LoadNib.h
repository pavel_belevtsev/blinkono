//
//  UIView+LoadNib.h
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (LoadNib)
+(id) loadNibName:(NSString *) nibName;

@end
