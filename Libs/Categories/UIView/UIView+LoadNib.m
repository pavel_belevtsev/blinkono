//
//  UIView+LoadNib.m
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "UIView+LoadNib.h"

@implementation UIView (LoadNib)




+(id) loadNibName:(NSString *) nibName
{
    if (nibName == nil)
    {
        nibName = NSStringFromClass([self class]);
    }

    NSArray * views = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    
    for (UIView * view in views)
    {
        if ([view isKindOfClass:[self class]])
        {
            return view;
        }
    }
    
    return nil;
}

@end
