//
//  APSTVProgram+MetaDataTagsHandling.m
//  KalturaUnified
//
//  Created by Rivka Schwartz on 5/25/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "APSTVProgram+MetaDataTagsHandling.h"
#import "NSDictionary+Additions.h"

@implementation APSTVProgram (MetaDataTagsHandling)

-(BOOL) isStartOverEnabled
{
    NSString * startOverFlag = [self.metaData objectForCaseInsensitiveKey:kEPGFlagStartOver];
    BOOL canStartOver = (startOverFlag && [startOverFlag caseInsensitiveCompare:@"yes"]== NSOrderedSame);
    return canStartOver;
}

-(BOOL) isCathupEnabled
{
    NSString * catchupFlag = [self.metaData objectForCaseInsensitiveKey:kEPGFlagCatchup];
    BOOL canCatchup = (catchupFlag && [catchupFlag caseInsensitiveCompare:@"yes"]== NSOrderedSame);
    return canCatchup;
}

-(BOOL) isRecordingEnabled
{
    NSString * allowRecording = [self.metaData objectForCaseInsensitiveKey:@"allowRecording"];
    return [allowRecording isEqualToString:@"yes"];
}
@end
