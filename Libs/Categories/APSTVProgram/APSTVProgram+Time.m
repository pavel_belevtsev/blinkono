//
//  APSTVProgram+Time.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 05.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "APSTVProgram+Time.h"

@implementation APSTVProgram (Time)

- (BOOL)programFinished {
    if (!self.startDateTime || !self.endDateTime){
        return NO;
    }
    
    return ([[NSDate date] timeIntervalSince1970] > [self.endDateTime timeIntervalSince1970]);
}

- (BOOL)programIsOn {
    
    return [self programInTime:[NSDate date]];
    
}

- (BOOL)programInTime:(NSDate *)date {
    
    if (!self.startDateTime || !self.endDateTime){
        return NO;
    }
    
    return (([date timeIntervalSince1970] <= [self.endDateTime timeIntervalSince1970]) &&
            ([date timeIntervalSince1970] >= [self.startDateTime timeIntervalSince1970]));
    
}

@end
