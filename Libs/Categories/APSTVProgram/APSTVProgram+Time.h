//
//  APSTVProgram+Time.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 05.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>
#import <TvinciSDK/APSEPGManager.h>

@interface APSTVProgram (Time)

- (BOOL)programFinished;
- (BOOL)programIsOn;
- (BOOL)programInTime:(NSDate *)date;

@end
