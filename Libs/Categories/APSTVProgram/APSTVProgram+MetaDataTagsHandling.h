//
//  APSTVProgram+MetaDataTagsHandling.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 5/25/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface APSTVProgram (MetaDataTagsHandling)

-(BOOL) isStartOverEnabled;
-(BOOL) isCathupEnabled;
-(BOOL) isRecordingEnabled;
@end
