//
//  NSMutableDictionary+RenameKey.m
//  Vodafone
//
//  Created by Israel Berezin on 5/27/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "NSMutableDictionary+RenameKey.h"

@implementation NSMutableDictionary (RenameKey)

-(void)renameDictionaryKeyFrom:(NSString*)oldKey toNewKey:(NSString*)newKey
{
    id value = [self objectForKey:oldKey];
    if (value)
    {
        [self setObject:value forKey:newKey];
    }
    [self removeObjectForKey:oldKey];
}

@end
