//
//  NSMutableDictionary+RenameKey.h
//  Vodafone
//
//  Created by Israel Berezin on 5/27/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (RenameKey)

-(void)renameDictionaryKeyFrom:(NSString*)oldKey toNewKey:(NSString*)newKey;

@end
