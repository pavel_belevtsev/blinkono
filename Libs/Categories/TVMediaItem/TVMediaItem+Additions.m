//
//  TVMediaItem+Additions.m
//  KalturaUnified
//
//  Created by Alex Zchut on 4/22/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVMediaItem+Additions.h"
#import <objc/runtime.h>
#import "NSData+Base64.h"
#import <CommonCrypto/CommonDigest.h>

static NSString *kListTypeKey = @"ListTypeKey";
static NSString *kDownloadItem = @"DownloadItem";

@implementation TVMediaItem (Additions)

- (NSInteger) listType {
    NSNumber *type = objc_getAssociatedObject(self, &kListTypeKey);
    return (type) ? [type integerValue] : 0;
}

-(void)setListType:(NSInteger)listType {
    objc_setAssociatedObject(self, &kListTypeKey, @(listType),  OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (id) downloadItemRef {
    return objc_getAssociatedObject(self, &kDownloadItem);
}

-(void)setDownloadItemRef:(id)downloadItem {
    objc_setAssociatedObject(self, &kDownloadItem, downloadItem,  OBJC_ASSOCIATION_ASSIGN);
}

- (NSString*) base64 {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    return [data base64EncodedString];
}

- (NSData*) data {
    return [NSKeyedArchiver archivedDataWithRootObject:self];
}

- (BOOL) isFreeMedia {
    return (self.priceType == TVPriceTypeFree);
}

- (NSString*) durationDescription {
    NSString *metadataRuntime = [self.metaData objectForKey:@"Runtime"];
    if (metadataRuntime && metadataRuntime.length) {
        
        NSInteger min = (int)([metadataRuntime integerValue]) / 60;
        if (min < 5) {
            min = [metadataRuntime integerValue];
        }
        
        return [NSString stringWithFormat:@"%ld %@", (long)min, LS(@"min")];
    }
    return @"";
}

- (NSInteger) duration {

    return self.mainFile.duration;
//    NSInteger duration = 0;
//    NSString *metadataRuntime = [self.metaData objectForKey:@"Runtime"];
//    if (metadataRuntime && metadataRuntime.length) {
//        
//        duration = (int)([metadataRuntime integerValue]) / 60;
//        if (duration < 5) {
//            duration = [metadataRuntime integerValue];
//        }
//    }
//    return duration;
}

- (NSString*) releaseYearDescription {
    NSString *metadataYear = [self.metaData objectForKey:@"Release year"];
    if (metadataYear && metadataYear.length) {
        return metadataYear;
    }
    return @"";
}

- (NSString*) countryDescription {
    NSArray *tagsCountry = [self.tags objectForKey:@"Country"];
    if (tagsCountry && tagsCountry.count) {
        return tagsCountry.firstObject;
    }
    return @"";
}

+ (TVMediaItem*) objectFromBase64:(NSString*) base64 {
    NSData *data = [NSData dataFromBase64String: base64];
    
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

+ (TVMediaItem*) objectFromDataAtPath:(NSString*) path {
    return [self objectFromData:[NSData dataWithContentsOfFile:path]];
}
+ (TVMediaItem*) objectFromData:(NSData*) data {
    if (data)
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return nil;
}

- (NSString *) downloadItemId {
    return [Utils md5:[self.mediaID stringByAppendingString:self.name]];
}

- (BOOL) hasDownloadFile
{
    TVFile *file = [self.files filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"format == %@", APST.TVCMediaFormatDownload]].firstObject;
    return file!=nil;
}
@end
