//
//  TVMediaItem+Additions.h
//  KalturaUnified
//
//  Created by Alex Zchut on 4/22/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

typedef enum MediaItemBelongsToListType
{
    kMediaItemBelongsToListTypeUndefined = 2000,
    kMediaItemBelongsToListTypeRentals,
    kMediaItemBelongsToListTypeWatchList,
    kMediaItemBelongsToListTypePermittedSubscriptions,
    kMediaItemBelongsToListTypeExpiredSubscriptions,
    kMediaItemBelongsToListTypeFavorites,
    kMediaItemBelongsToListTypeHistory,
    kMediaItemBelongsToListTypeRecordings,
    kMediaItemBelongsToListTypeDownload

} MediaItemBelongsToListType;

@interface TVMediaItem (Additions)

@property (nonatomic, readwrite) NSInteger listType;
@property (nonatomic, weak) id downloadItemRef;

- (NSString*) base64;
- (NSString *) downloadItemId;
- (NSData*) data;
- (BOOL) isFreeMedia;
- (NSString*) durationDescription;
- (NSString*) releaseYearDescription;
- (NSString*) countryDescription;
- (NSInteger) duration;
- (BOOL) hasDownloadFile;

+ (TVMediaItem*) objectFromBase64:(NSString*) base64;
+ (TVMediaItem*) objectFromData:(NSData*) data;
+ (TVMediaItem*) objectFromDataAtPath:(NSString*) path;
@end
