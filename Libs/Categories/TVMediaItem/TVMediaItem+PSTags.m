//
//  TVMediaItem+PSTags.m
//  Vodafone
//
//  Created by Israel Berezin on 5/27/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//
#import <TvinciSDK/TVMediaItem.h>

#import "TVMediaItem+PSTags.h"
#import "NSMutableDictionary+RenameKey.h"

@implementation TVMediaItem (PSTags)


-(NSDictionary *) parseMetas:(NSArray *)metas
{
    NSMutableDictionary *metaParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in metas)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:TVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *value = [tagAsDictionary objectOrNilForKey:TVMediaItemTagValuesKey];
            if (value != nil)
            {
                [metaParsed setObject:value forKey:title];
            }
        }
    }
    [metaParsed renameDictionaryKeyFrom:@"Year" toNewKey:@"Release year"];
    [metaParsed renameDictionaryKeyFrom:@"Display_Run_Time" toNewKey:@"Run time"];
  
    
    
    return [NSDictionary dictionaryWithDictionary:metaParsed];
}

-(NSDictionary *) parseTags:(NSArray *)tags
{
    NSMutableDictionary *tagsParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in tags)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:TVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *values = [tagAsDictionary objectOrNilForKey:TVMediaItemTagValuesKey];
            NSArray *valuesSeparated = [values componentsSeparatedByString:@"|"];
            if (valuesSeparated != nil)
            {
                [tagsParsed setObject:valuesSeparated forKey:title];
            }
        }
    }
    
    if ([tagsParsed objectForKey:@"epg_id"] == nil)
    {
        if ([self.metaData objectForKey:@"Genre"])
        {
            NSString *genreStr = [self.metaData objectForKey:@"Genre"];
            NSArray * genreArr =[NSArray arrayWithObject:genreStr]; //[genreStr componentsSeparatedByString:@"/"]; 
            [tagsParsed setObject:genreArr forKey:@"Genre"];
        }
    }
    
    return [NSDictionary dictionaryWithDictionary:tagsParsed];
}

@end
