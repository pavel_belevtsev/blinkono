//
//  EUPermittedItems.h
//  Utils
//
//  Created by Tarek Issa on 2/4/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import "BaseNetworkObject.h"

@interface EUPermittedItems : NSObject

@property (nonatomic, assign) TVNetworkQueue *queue;

- (instancetype)initWithQueue: (TVNetworkQueue*) queue;
- (void) getUserOnlinePurchasesSubscriptionOrRentalIsSubscription:(BOOL)isSubscription WithCompletionBlock : (void (^)(NSArray* rentalsArray, NSError* error)) completion;
- (void) getDomainOnlinePurchasesSubscriptionOrRentalIsSubscription:(BOOL)isSubscription WithCompletionBlock : (void (^)(NSArray* purchasesArray,NSError* error)) completion;

@end
