//
//  EUPermittedItems.m
//  Utils
//
//  Created by Tarek Issa on 2/4/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import "EUPermittedItems.h"
#import <TvinciSDK/TVRental.h>


@interface EUPermittedItems ()


@end


@implementation EUPermittedItems

- (instancetype)initWithQueue: (TVNetworkQueue*) queue {
    if (self = [super init]) {
        _queue = queue;
    }
    return self;
}
- (void) getUserOnlinePurchasesSubscriptionOrRentalIsSubscription:(BOOL)isSubscription WithCompletionBlock : (void (^)(NSArray* rentalsArray, NSError* error)) completion
{
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetUserPermittedItems];
    
    [request setCompletionBlock:^{
        
        NSArray * rentalsArray = [request JSONResponse];
        NSArray * rentalsMediaItems = [self buildRantelOrSubsciptionArray:rentalsArray IsSubscription:isSubscription];
        completion(rentalsMediaItems, NULL);
    }];
    
    [request setFailedBlock:^{
        
        NSString * resposeString = request.responseString;
        NSLog(@"%@",resposeString);
        NSError* err = [NSError errorWithDomain:@"OnlinePurchasesErrorDomain" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request, @"Request", nil]];
        completion(nil, err);
    }];
    
    [self.queue sendRequest:request];
    
}

- (void) getDomainOnlinePurchasesSubscriptionOrRentalIsSubscription:(BOOL)isSubscription WithCompletionBlock : (void (^)(NSArray* purchasesArray,NSError* error)) completion
{
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetDomainPermittedItems];
    
    [request setCompletionBlock:^{
        
        NSArray * rentalsArray = [request JSONResponse];
        NSArray * rentalsMediaItems = [self buildRantelOrSubsciptionArray:rentalsArray IsSubscription:isSubscription];
        completion(rentalsMediaItems, NULL);
    }];
    
    [request setFailedBlock:^{
        
        NSString * resposeString = request.responseString;
        NSLog(@"%@",resposeString);
        NSError* err = [NSError errorWithDomain:@"OnlinePurchasesErrorDomain" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request, @"Request", nil]];
        completion(nil, err);
    }];
    
    [self.queue sendRequest:request];
    
}

-(NSArray *)buildRantelOrSubsciptionArray:(NSArray*)rentalsArray IsSubscription:(BOOL)isSubscription
{
    NSMutableArray * rentalsMediaItems = [NSMutableArray array];
    
    for (NSDictionary * dict in rentalsArray )
    {
        TVRental * rentalItem = [[TVRental alloc] initWithDictionary:dict];
        if (isSubscription == NO)
        {
            if ([rentalItem isRental] == YES)
            {
                [rentalsMediaItems addObject:rentalItem];
            }
        }
        else
        {
            if ([rentalItem isRental] == NO)
            {
                [rentalsMediaItems addObject:rentalItem];
            }
        }
    }
    return [NSArray arrayWithArray:rentalsMediaItems];
}

@end
