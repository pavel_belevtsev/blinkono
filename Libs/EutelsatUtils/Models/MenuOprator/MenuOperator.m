//
//  MenuOprator.m
//  
//
//  Created by Luda Fux on 1/5/14.
//  Copyright (c) 2014 Quickode Ltd. All rights reserved.
//

#import "MenuOperator.h"
#import <TvinciSDK/TVSessionManager.h>
#import <TvinciSDK/TvinciSDK.h>
#import <TvinciSDK/TVOperator.h>
#import <TvinciSDK/TVLocale.h>
#import "CustomInitObject.h"

#define kOperatorKey @"m_nSSOOperatorID"

@implementation MenuOperator

- (instancetype)initWithQueue: (TVNetworkQueue*) queue {
    if (self = [super init]) {
        _queue = queue;
    }
    return self;
}

-(void)createOperatorToUser: (void (^)(BOOL result)) completion
{
    NSNumber * operatorID = [[[TVSessionManager sharedTVSessionManager] userData] objectForKey:kOperatorKey];
    if ([[TVConfigurationManager sharedTVConfigurationManager] defaultIPNOID].length >0)
    {
        NSInteger temp = [[[TVConfigurationManager sharedTVConfigurationManager] defaultIPNOID] integerValue];
        
        operatorID = @(temp);
    }

    if (!operatorID) {
        completion(FALSE);
        return;
    }
    
    NSArray * operators = [NSArray arrayWithObjects:operatorID, nil];
    __weak TVPAPIRequest * request = [TVPSiteAPI requestForGetOperatorsWithOperators:operators delegate:nil];
    
    [request setFailedBlock:^{
        NSLog(@"faild %@",request.responseString);
        completion(FALSE);
    }];
    
    [request setCompletionBlock:^{
        
        //NSLog(@"IPNO Succeded %@",request);
        NSDictionary * dictionary = [[request JSONResponse] lastObject];
        TVIPNOperator * operator = [[TVIPNOperator alloc] initWithDictionary:dictionary];
        [TVSessionManager sharedTVSessionManager].userOperator = operator;
        [self insertMenuIdToOperator];
        completion(TRUE);
    }];
    
    [self.queue sendRequest:request];
}

-(void)insertMenuIdToOperator
{
    NSString * menuID = @"";
    NSArray * operatorsArr = [TVConfigurationManager sharedTVConfigurationManager].OperatorsArr;
    TVIPNOperator *userOperator = [TVSessionManager sharedTVSessionManager].userOperator;
    for (TVOperator * operator in operatorsArr)
    {
        if ([operator.OperatorID integerValue] == userOperator.operatorID)
        {
            ASLogInfo(@"operator.OperatorID = %d",[operator.OperatorID intValue]);
            
            menuID = [operator.menuIDs firstObject];
            [TVSessionManager sharedTVSessionManager].userOperator.operatorMenuId = menuID;
            return;
        }
    }
}

- (void) getMenuByCurrentOpratorWithCompletionBlock : (void (^)(NSDictionary* menuItemsDic,NSError* error)) completion
{
    
    NSString * userName = [TVSessionManager sharedTVSessionManager].userOperator.operatorUsername;
    NSString * password = [TVSessionManager sharedTVSessionManager].userOperator.operatorPassword; // @"11111";
    NSString * menuId   = [TVSessionManager sharedTVSessionManager].userOperator.operatorMenuId;
    
    if (!userName || !password || !menuId) {
        NSError* err = [NSError errorWithDomain:@"GetMenuErrorDomain" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"No user logged in found.",NSLocalizedDescriptionKey, nil]];
        completion(nil, err);
    }else{
        
        NSDictionary * postData = [NSDictionary dictionaryWithObject:menuId forKey:@"ID"];
        
        // make "CustomInitObject" with deep copy!
        TVInitObject * initObject =  [TVSessionManager sharedTVSessionManager].sharedInitObject;
        
        CustomInitObject * customInitObject = [[CustomInitObject alloc] initWithInitObject:initObject andAPIUser:userName andAPIPaswword:password];
        
        __weak TVPAPIRequest * request = [TVPSiteAPI requestForGetOperatorsWithCustomInitObject:customInitObject AndOtherPostData:postData delegate:nil];
        
        [request setCompletionBlock:^{
            
            if ([request.responseString length]>0)
            {
                [MenuM saveUserMenu:request.responseString];
            
                NSDictionary *response =  [request JSONResponse];
                completion(response, NULL);
            }
            else
            {
                NSError* err = [NSError errorWithDomain:@"GetMenuErrorDomain" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request.error, @"getMediasRequest", nil]];
                completion(nil, err);
   
            }
        }];
        
        [request setFailedBlock:^{
            
            //NSString * resposeString = request.responseString;
            //NSLog(@"%@",resposeString);
            NSError* err = [NSError errorWithDomain:@"GetMenuErrorDomain" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request.error, @"getMediasRequest", nil]];
            completion(nil, err);
        }];
        [self.queue sendRequest:request];
    }
}

- (NSInteger)getMenuIDByOperatorID:(NSInteger)operatorID {
    NSArray* OperatorsArr = [[TVConfigurationManager sharedTVConfigurationManager] OperatorsArr];
    if (OperatorsArr) {
        //  Run through all Operators and look for the relevant one.
        for (TVOperator* operator in OperatorsArr) {
            if (operatorID == [operator.OperatorID integerValue]) {
                return [[operator.menuIDs lastObject] integerValue];
            }
        }
    }
    
    return -1;
}

@end
