//
//  MenuOperator.h
//  
//
//  Created by Luda Fux on 1/6/14.
//  Copyright (c) 2014 Quickode Ltd. All rights reserved.
//

#import "BaseNetworkObject.h"

@interface MenuOperator : NSObject

@property (nonatomic, assign) TVNetworkQueue *queue;

- (instancetype)initWithQueue: (TVNetworkQueue*) queue;
- (void)createOperatorToUser: (void (^)(BOOL result)) completion;
- (void) getMenuByCurrentOpratorWithCompletionBlock : (void (^)(NSDictionary* menuItemsDic,NSError* error)) completion;
- (NSInteger)getMenuIDByOperatorID:(NSInteger)operatorID;

@end
