//
//  CustomInitObject.m
//  
//
//  Created by Luda Fux on 1/6/14.
//  Copyright (c) 2014 Quickode Ltd. All rights reserved.
//

#import "CustomInitObject.h"
#import <TvinciSDK/TVLocale.h>

@implementation CustomInitObject

/*
 @property (copy , nonatomic) NSString *siteGUID;
 @property (nonatomic , copy) NSString *platform;
 @property (copy , nonatomic) NSString *APIUsername;
 @property (copy , nonatomic) NSString *APIPassword;
 @property (nonatomic , retain) TVLocale *locale;

@property (assign , nonatomic) NSInteger domainID;
@property (readonly) NSString *udid;

 @property (copy , nonatomic) NSString *localeLanguage;
 @property (copy , nonatomic) NSString *localeCountry;
 @property (copy , nonatomic) NSString *localeDevice;
 @property (assign , nonatomic) TVLocaleUserState localeUserState;
 @property (nonatomic ,copy) NSString *localeUserStateString; */

-(id) initWithInitObject:(TVInitObject *)initObject
{
    if (self = [self init])
    {
        // do deep copy to items
        self.siteGUID = initObject.siteGUID;
        self.platform = initObject.platform;
        self.APIUsername = initObject.APIUsername;
        self.APIPassword = initObject.APIPassword;
        self.domainID = initObject.domainID;
        
        NSDictionary * temp = [initObject.locale keyValueRepresentation];
        self.locale = [TVLocale localeWithDictionary:temp];


    }
    return self;
}

-(id) initWithInitObject:(TVInitObject *)initObject andAPIUser:(NSString *)user andAPIPaswword:(NSString *)password
{
    if (self = [self initWithInitObject:initObject])
    {
        // do deep copy to items
        self.APIUsername = user;
        self.APIPassword = password;
    }
    return self;
}

@end
