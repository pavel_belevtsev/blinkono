//
//  CustomInitObject.h
//  
//
//  Created by Luda Fux on 1/6/14.
//  Copyright (c) 2014 Quickode Ltd. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>
#import <TvinciSDK/TVInitObject.h>

@interface CustomInitObject : TVInitObject

-(id) initWithInitObject:(TVInitObject *)initObject;
-(id) initWithInitObject:(TVInitObject *)initObject andAPIUser:(NSString *)user andAPIPaswword:(NSString *)password;

@end
