//
//  BaseNetworkObject.h
//  Utils
//
//  Created by Tarek Issa on 12/15/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ASIHTTPRequest;
@class TVMediaItem;

@interface BaseNetworkObject : NSObject {
    
}

@property (nonatomic, readwrite, retain) TVNetworkQueue *networkQueue;

/*!
 * @abstract Add the request to the network queue
 */
- (void) sendRequest : (ASIHTTPRequest *) request;
- (void) clearQueueOfRequest;

@end
