//
//  BaseNetworkObject.m
//  Utils
//
//  Created by Tarek Issa on 12/15/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "BaseNetworkObject.h"


@interface BaseNetworkObject () {
    
}

@end

@implementation BaseNetworkObject

#pragma mark - Memory
-(void) dealloc {
    [self clearQueueOfRequest];
    self.networkQueue = nil;
}

-(void) clearQueueOfRequest {
    [self.networkQueue cleen];
}

#pragma mark - Getters

-(TVNetworkQueue *) networkQueue {
    if (!_networkQueue) {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

#pragma mark - Asynchronic Dangerous Stuff
-(void) sendRequest:(ASIHTTPRequest *)request {
    if (request) {
        [self.networkQueue sendRequest:request];
    }
}

@end
