//
//  TVCplayer.h
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 6/19/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TVCDeclerations.h"
#import "TVMediaToPlayInfo.h"


extern NSString * const TVCSubtitlesAvaialable;

typedef enum TVCSeekResult {
	TVCSeekResult_OK = 0,
	TVCSeekResult_OutOfRange,
	TVCSeekResult_CannotSeek
} TVCSeekResult;



@class TVMediaToPlayInfo;
@interface TVCplayer : UIViewController <TVCPlayerProtocol>{
}


#pragma mark - Abstract methods

- (void)Play;
- (void)Stop;
- (void)Pause;
- (void)Resume;
- (TVCSeekResult)seekToTimeFloat:(float)time;
- (TVCSeekResult)seekToTime:(TimeStruct)time;
- (void)playMedia:(TVMediaToPlayInfo *)mediaToPlayInfo;
- (void)cleanPlayer;

- (void) forceMute;
- (void) unForceMute;

- (void)startBuffering;
- (void)stopBuffering;
- (void)DrmFinishedSuccesfullyForMediaItem:(TVMediaItem *)MediaItm;

-(void)cleanPersonalization; //used to do new personalization then have new app version!

/*  Subtitles   */
- (void)turnSubtitle:(BOOL)on;
/*   2 cases:
 - Case isCustomSubtitlesActive = YES, so 'lan' Parameter is TVCSubsLan enum.
 - Case isCustomSubtitlesActive = NO, so 'lan' Parameter is index of 'getSubtitlesList' returned array.
 
 */
- (void)changeSubtitlesLanguageTo:(NSInteger)lan;
- (NSArray *)getSubtitlesList;



/*  Audio options */    //  TVAudioLanguage
- (void) changeAudioLanguageTo:(NSInteger) language;
- (NSArray *)getAudioLanguagesList;





/*  setRectDraw - provided as a workaround for a bug in 'SecurePlayer' framework. Fixes sutitle issue: subtitles are not positioned in the right plavce   */
- (void) setRectDraw:(UIInterfaceOrientation)toInterfaceOrientation;

/*   If isCustomSubtitlesActive = YES, then TVCPlayerSubtitlesProtocol will fire the delegate methods, otherwise a built-in subtitls format will be shown on video screen   */
@property (nonatomic, readonly)                         BOOL isSubtitleOn;
@property (readwrite, assign)                           BOOL isCustomSubtitlesActive;
@property (nonatomic, readonly)                         TVMediaToPlayInfo * mediaToPlayInfo;
@property (readwrite, assign)                           id<TVCplayerStatusProtocol> delegate;

@property (nonatomic, readonly)                         TVDRMType drmType;

@property (readwrite, assign)                           id<TVCPlayerSubtitlesProtocol> subtitlesDelegate;
@property (readwrite, assign, getter=playerStatus)      TVPPlaybackState playerStatus;
@property (readwrite, assign, getter=movieLoadStatus)      TVPMovieLoadState movieLoadStatus;

@property (nonatomic, readonly, getter = getAvailableStreamTime) float availableStramTime;

@property (retain, nonatomic,readonly) NSString * internalVersion;
@property (nonatomic, assign, readonly) BOOL isDeviceSecured;


@property (nonatomic, assign) NSInteger playerStatusBeforeResign;
@property (nonatomic, assign) NSInteger timeOutSeconds;

/**
 *   pre agreed license for the player for spesific client
 */
//@property (nonatomic, strong, getter=playerLicenceId,setter=setPlayerLicenceId:) NSString * playerLicenceId;



@end
