//
//  DrmManager.h
//  drmTest
//
//  Created by Tarek Issa on 7/12/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVCDeclerations.h"
#import "DrmProviderBaseProtocols.h"

@class DrmProviderBase;
@class DrmManager;

@protocol DrmManagerProtocol <NSObject>

- (void)DrmFinishedWith:(TVCDrmStatus)status withURL:(NSURL*)url;
- (DrmProviderBase *) drmProvider:(DrmManager *)drmManager;
- (void) drmManager:(DrmManager *)drmManager licenseError:(NSDictionary *) info;


@end

@interface DrmManager : NSObject <DrmProviderProtocol> {
@private
    
}


@property (nonatomic, assign) id<DrmManagerProtocol> delegate;
@property (nonatomic, retain) DrmProviderBase* drmProvider;
@property (nonatomic, assign) id<TVCDrmProviderBaseDataProvider> drmDataProvider;



+ (DrmManager *) sharedDRMManager;
- (void)StartProcess:(DrmProviderBase *)drmProvider ForMediaItem:(TVMediaToPlayInfo *)mediaItem andDelegate:(id<DrmManagerProtocol>)aDelegate;
- (void)kill;

- (BOOL) registerAsset:(NSString*)localPath;
- (void) licenceTimeRemaining:(NSString*)assetPath completion:(void(^)(BOOL success, NSTimeInterval remainingTime))completion;



@end
