/************************************************************************
VisualOn Proprietary
Copyright (c) 2013, VisualOn Incorporated. All Rights Reserved

VisualOn, Inc., 4675 Stevens Creek Blvd, Santa Clara, CA 95051, USA

All data and information contained in or disclosed by this document are
confidential and proprietary information of VisualOn, and all rights
therein are expressly reserved. By accepting this material, the
recipient agrees that this material and the information contained
therein are held in confidence and in trust. The material may only be
used and/or disclosed as authorized in a license agreement controlling
such use and disclosure.
************************************************************************/

#import <Foundation/Foundation.h>

#ifndef SKIP_IN_MAIN_DOCUMENTATION
#import "VOOSMPAnalyticsFilter.h"
#import "VOOSMPAnalyticsInfo.h"
#endif

#import "VOOSMPType.h"

@protocol VOCommonPlayerAnalytics <NSObject>

/**
 * enable analytics display.
 *
 * <p><ul>
 * <li> To enable, this API must be called before calling the {@link VOCommonPlayerControl#open} method
 * <li> This API should be called ASAP after calling the {VOCommonPlayerConfiguration#setPreAgreedLicense}
 * <li> To change the refresh time, This API can be called during playback.
 * </ul></p>
 *
 * @param   time   [in] Time <seconds> to refresh the display, set to 0 to disable. Valid time should be greater than or equal to 3.
 *
 * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
 *
 */
- (VO_OSMP_RETURN_CODE) enableAnalyticsDisplay:(int)time;


/**
 * Set display type of analytics.
 * <p>
 * This API must be called after calling {@link VOCommonPlayerAnalytics#enableAnalyticsDisplay}
 * </p>
 *
 * @param   type   [in] The display type. The default is VO_OSMP_DISPLAY_NULL.
 *
 * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
 */
- (VO_OSMP_RETURN_CODE) setAnalyticsDisplayType:(VO_OSMP_DISPLAY_TYPE)type;


/**
 * enable analytics Foundation.
 *
 * <p><ul>
 * <li> To enable, this API must be called before calling the {@link VOCommonPlayerControl#open} method
 * <li> This API should be called ASAP after calling the {VOCommonPlayerConfiguration#setPreAgreedLicense}
 * </ul></p>
 *
 * @param   enable [in] Enable/Disable; true to enable, false to disable(default).
 *
 * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
 *
 */
- (VO_OSMP_RETURN_CODE) enableAnalyticsFoundation:(bool)enable;


/**
 * Set customer specified user ID for analytics foundation.
 * <p>
 * This API must be called after calling {@link VOCommonPlayerAnalytics#enableAnalyticsFoundation}
 * </p>
 *
 * @param   cuid [in] The customer specified user ID
 *
 * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
 */
- (VO_OSMP_RETURN_CODE) setAnalyticsFoundationCUID:(NSString *)cuid;


/**
 * Enable or disable location information in analytics foundation.
 * <p>
 * This API must be called after calling {@link VOCommonPlayerAnalytics#enableAnalyticsFoundation}
 * </p>
 *
 * @param enable [in] Enable/disable; true to enable, false to disable (default).
 *
 * @return {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
 */
- (VO_OSMP_RETURN_CODE) enableAnalyticsFoundationLocation:(bool)enable;


/**
 * Get video FPS from analytics display.
 * <p> Need enable analytic with {@link enableAnalyticsDisplay} </p>
 *
 * @return  FPS; -1 if unsuccessful.
 */
- (float) getAnalyticsFPS;


/**
 * Set cache time and start analytics data collection.
 *
 * @param   cacheTime  [in] Time <seconds> to cache the analytics data.
 *
 * @return  {@link VO_OSMP_ERR_NONE} if successful
 */
- (VO_OSMP_RETURN_CODE) enableAnalytics:(int)cacheTime;


/**
 * Get current analytics data.
 *
 * @param   filter   [in] Filters, specified as an {@link VOOSMPAnalyticsFilter} object,
 *                       to be applied to cached analytic data before being returned.
 *
 * @param   data     [out] Filtered analytics data. Refer to {@link VOOSMPAnalyticsInfo}.
 *
 * @return  A {@link VOOSMPAnalyticsInfo} object if successful; nil if unsuccessful
 */
- (id<VOOSMPAnalyticsInfo>) getAnalytics:(VOOSMPAnalyticsFilter *)filter;


/**
 * Get video decoding bitrate(bps) for each of the last 10 seconds
 *
 * @return an array(NSNumber) of size 10 in which elements are the video
 *         decoding bitrates of each of the last 10 seconds.
 *         The bitrate information is arranged in the way that
 *         the smaller the array index is, the closer
 *         the bitrate is to the current time. That is,
 *         element 0 is the bitrate of the closest second
 *         before the current time; element 1 is the bitrate
 *         that is 1 second before element 0; element 2 is the
 *         bitrate that is 1 second before element 1, and so on.
 */
- (NSArray *) getVideoDecodingBitrate;


/**
 * Get audio decoding bitrate(bps) for each of the last 10 seconds
 *
 * @return an array(NSNumber) of size 10 in which elements are the audio
 *         decoding bitrates of each of the last 10 seconds.
 *         The bitrate information is arranged in the way that
 *         the smaller the array index is, the closer
 *         the bitrate is to the current time. That is,
 *         element 0 is the bitrate of the closest second
 *         before the current time; element 1 is the bitrate
 *         that is 1 second before element 0; element 2 is the
 *         bitrate that is 1 second before element 1, and so on.
 */
- (NSArray *) getAudioDecodingBitrate;

@end
