//
//  DxRightsInfo.h
//  DxDLC
//
//  Created by Hagai Hanan on 4/28/11.
//  Copyright 2011 Discretix Technologies. All rights reserved.
//


#import <Foundation/Foundation.h>

typedef enum {
	DxRightsStatusNotValid,
	DxRightsStatusClockNotSet,
	DxRightsStatusExpired,
	DxRightsStatusFuture,
	DxRightsStatusValid
} DxRightsStatus;


//! \brief
//! DxRightsInfo is an object that contains a set of permissions (i.e. permission to play, display, execute,...) and a set of constraints (i.e. date-time, count, interval,...). 
//! \details
//! A DxRightsInfo object can contain one constraint (at most) of every of the following kinds:
//! <br />
//! <ol>
//! <li>Count - The user can use the content for specific number of times.</li>
//! <li>Date Time - The user can use the content only after a specified start time has arrived (if start time is defined) and until a specified end time arrives (if end time is defined).</li>
//! <li>Interval - The user can use the content for the specified period of time starting from the first usage of content.</li>
//! </ol>
//! All the constraints apply to all the permissions.
//! The object is valid only if all the constraints are sufficed (i.e. the constraints conditions are ANDed).
@interface DxRightsInfo : NSObject {
	DxRightsStatus rightsStatus;
	
	BOOL isTimeConstrained;
	BOOL isIntervalConstrained;
	BOOL isCountConstrained;
	
	NSDate *startTime;
	NSDate *endTime;
	unsigned int intervalPeriodInSeconds;
	
	unsigned int initialCount;
	unsigned int countLeft;
}

//! \brief
//! Initialize the DxRightsInfo object with internal data objects. 
//! For intenal use only.
//! \param[in] rightsObject - internal data object.
//! \return DxRightsInfo object.
- (id)initWithRightsObject:(const void*)rightsObject;

//! Rights Information status.
@property (readonly, assign) DxRightsStatus rightsStatus;

//! Indicates whether the license has time constraints, hence start from a certain date and end in other.
@property (readonly, assign) BOOL isTimeConstrained;
//! Indicates whether the license has interval constraints, hence how much time from current time can we still have the license valid.
@property (readonly, assign) BOOL isIntervalConstrained;
//! Indicates whether the license has count constraints, hence how many times can we play the media.
@property (readonly, assign) BOOL isCountConstrained;

//! Start date of the time constrained license. Valid only if license is time constrained.<br>Note: Start date is expressed as the Coordinated Universal Time (UTC).
@property (readonly, retain) NSDate *startTime;
//! End date of the time constrained license. Valid only if license is time constrained.<br>Note: End date is expressed as the Coordinated Universal Time (UTC).
@property (readonly, retain) NSDate *endTime;
//! Indicates how much time do we have in seconds till the license becomes invalid. Valid only if license is interval constrained.
@property (readonly, assign) unsigned int intervalPeriodInSeconds;
//! Indicates how many times could the content be played since it was purchased. Valid only if license is count constrained.
@property (readonly, assign) unsigned int initialCount;
//! Indicates how many times could the content be played from now. Valid only if license is count constrained.
@property (readonly, assign) unsigned int countLeft;

@end
