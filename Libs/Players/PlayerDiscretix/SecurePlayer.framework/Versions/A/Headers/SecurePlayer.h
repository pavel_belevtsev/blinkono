//
//  SecurePlayer.h
//  Secure Player
//
//  Copyright 2011 Discretix Technologies. All rights reserved.
//

//! \mainpage
//! The Discretix SecurePlayer SDK for iOS is delivered as a framework for easy integration into xcode projects.
//! The SDK supplies DRM operations and media control utilities to purchase and play video content.

#import <SecurePlayer/DxDrmManager.h>
#import <SecurePlayer/DxRightsInfo.h>
#import <SecurePlayer/VODXPlayer.h>

