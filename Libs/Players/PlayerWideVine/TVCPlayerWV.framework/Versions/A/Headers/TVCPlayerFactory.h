//
//  TVCPlayerFactory.h
//  YES_iPhone
//
//  Created by Tarek Issa on 7/4/13.
//  Copyright (c) 2013 Tarek Issa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVCplayer.h"
#import "TVCDeclerations.h"

@class DrmProviderBase;
@interface TVCPlayerFactory : NSObject

+ (TVCPlayerFactory *) playerFactory;
- (TVCplayer *)GetPlayerForMediaItem:(TVMediaItem *)mediaItem;
- (TVCplayer *)GetClearPlayer;
- (TVCplayer *)GetPlayer;
- (TVCplayer *)GetPlayerByType:(TVDRMType)type;
- (DrmProviderBase *) drmProvider;


@end
