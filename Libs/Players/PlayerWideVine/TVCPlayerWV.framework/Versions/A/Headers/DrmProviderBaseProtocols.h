//
//  DrmProviderBaseProtocols.h
//  TVCPlayerStaticLibrary
//
//  Created by Rivka Peleg on 2/10/15.
//  Copyright (c) 2015 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DrmProviderBase;
@class TVMediaToPlayInfo;


@protocol DrmProviderProtocol <NSObject>
@required
- (void)ProviderFinishedWithStatus:(TVCDrmStatus)status withURL:(NSURL*)url;
@end


@protocol TVCDrmProviderBaseDataProvider <NSObject>
- (void) drmProvider:(DrmProviderBase *) sender downloadMediaLicenseDataForMediaToPlay:(TVMediaToPlayInfo *) mediaItemToPlay withCompletionBlock:(void (^)(NSString*)) completion;
@end


@protocol TVCDrmEventListener <NSObject>
-(void)  detectedEvenetID:(NSString *) eventID info:(NSDictionary *) dictionary;
@end
