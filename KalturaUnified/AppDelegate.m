//
//  AppDelegate.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "AppDelegate.h"
#import <TvinciSDK/TVLogHandler.h>

#import "ViewControllersFactoryVF.h"
#import "CustomViewFactoryVF.h"
#import "Crittercism.h"
#import "AnalyticsManager.h"
#import "APSEventMonitorWindow.h"
#import "TVCompanionSTBHandler.h"
#import <TvinciSDK/TVMixedCompanionManager.h>
#import "CustomViewFactory.h"
#import <TvinciSDK/TVDataRefreshHandler.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <TvinciSDK/NSString+FilePath.h>
#import <TvinciSDK/NSArray+Equivalent.h>
//#import "GuidelinesBasedAuthorizationProviderVF.h"
#import "OnoConfigurationManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

+ (AppDelegate *)sharedAppDelegate {
    return (AppDelegate *) [UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    TVAbstractCompanionHandler* h1 = [[TVCompanionSTBHandler alloc] init];
    [[TVMixedCompanionManager sharedInstance] addHandler:h1];
   
    
    [TVLogHandler setLogLevel:TVLogLevelOff];
    [[OnoConfigurationManager sharedTVConfigurationManager] setUDIDType:TvinciUDIDType];
    
    [self setupDMS];
    
    [[[ViewControllersFactoryVF alloc] init] becomeDefaultFactory];
    [[[CustomViewFactoryVF alloc] init] becomeDefaultFactory];
//    [[[GuidelinesBasedAuthorizationProviderVF alloc]init] becomeDefaultFactory];
    
    [[VFTypography instance] becomeDefaultTypography];

    self.blockOrientationRotation = YES;
    
    [AnalyticsM setup];
    
    [LoginM internetReachabilityStart];
    
    if (APST.hasDownloadToGo)
        [[DTGManager sharedInstance] start];

    if (!IS_IOS_8_OR_LATER){
        self.window = [[APSEventMonitorWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    }
    self.window.rootViewController = [[ViewControllersFactory defaultFactory] initialViewController];
    [self.window makeKeyAndVisible];
    
    if (APST.crashlyticsInfo) {
        [Fabric with:@[CrashlyticsKit]];
    }
    else {
        NSString *crittercismID = APST.crittercismId;
        if (crittercismID) [Crittercism enableWithAppID:crittercismID];
    }
    
    return YES;
}

-(void)setupDMS
{
    NSDictionary *features = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Features" ofType:@"plist"]];

    NSString* baseDMS = [features objectForKey:@"baseDMS"];
    
    if (baseDMS) {
        [[TVConfigurationManager sharedTVConfigurationManager] setDmsBaseURL:baseDMS];
    }
}

-(BOOL)application:(UIApplication*)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if ([[url scheme] isEqualToString:APST.deepLinkingScheme]) {
        NSLog(@"URL - %@", [url absoluteString]);
        NSLog(@"host: %@", [url host]);
        NSLog(@"path: %@", [url path]);
        NSLog(@"query: %@", [url query]);
        [DeepLinkingM registerDeepLinkingAction:url];
    } else {
        [FBSession.activeSession handleOpenURL:url];
        self.tryToOpenFBSession = NO;
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[TVDataRefreshHandler sharedInstance] executeRefreshTasksWithStartBlock:nil compeltionBlock:nil];
    
    if (NavM.container) {
        if (NavM.container.centerViewController) {
            NSArray *controllers = [NavM.container.centerViewController viewControllers];
            BaseViewController *controller = [controllers lastObject];
            
            if ([controller isKindOfClass:[BaseViewController class]]) {
                [((BaseViewController *)controller) restartHUD];
            }
        }
    }
    
    if (self.tryToOpenFBSession){
        LoginM.facebookScope = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];
        self.tryToOpenFBSession = NO;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    
    if (isPhone) {
        if (self.blockOrientationRotation) {
            return UIInterfaceOrientationMaskPortrait;
        } else {
            return UIInterfaceOrientationMaskAllButUpsideDown;
        }
    }
    else {
        return UIInterfaceOrientationMaskLandscape;
    }
}

-(void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    self.backgroundSessionCompletionHandler = completionHandler;
    completionHandler(UIBackgroundFetchResultNewData);

}

@end
