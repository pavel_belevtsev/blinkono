//
//  WatchAdapter.m
//  KalturaUnified
//
//  Created by Rivka Schwartz on 3/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "WatchAdapter.h"

@implementation WatchAdapter

-(void)setProgram:(APSTVProgram *)program
{
    if (_program != program)
    {
        _program = program;
        _mediaItem = nil;
    }
}


-(void)setMediaItem:(TVMediaItem *)mediaItem
{
    if (_mediaItem != mediaItem)
    {
        _mediaItem = mediaItem;
        _program = nil;
    }
}




-(void) sendWatchAction
{
    
    __weak TVPAPIRequest * request = nil;
    
    NSArray * arrayAddtionalInfo = nil;
    
    if (self.mediaItem && self.mediaItem.mediaWebLink) {
        
        arrayAddtionalInfo = @[@{@"key": @"obj:url",@"value":self.mediaItem.mediaWebLink.absoluteString?self.mediaItem.mediaWebLink.absoluteString:@""}];
        request = [TVPSocialAPI requestForDoUserActionWithUserAction:TVUserSocialActionWATCHES assetType:TVAssetType_MEDIA assetID:self.mediaItem.mediaID.integerValue extraParams:arrayAddtionalInfo SocialPlatform:TVSocialPlatformFacebook delegate:nil];
    }
    else if (self.program)
    {
        request = [TVPSocialAPI requestForDoUserActionWithUserAction:TVUserSocialActionWATCHES assetType:TVAssetType_PROGRAM assetID:[self.program.epgId integerValue] extraParams:arrayAddtionalInfo SocialPlatform:TVSocialPlatformFacebook delegate:nil];
    }
    
    [request setFailedBlock:^{
           }];
    
    [request setCompletionBlock:^{
            }];
    
    [self sendRequest:request];
}


@end
