//
//  LoginManager.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TvinciSDK/TvinciSDK.h>
#import <TvinciSDK/Reachability.h>

extern NSString * const TVNUserDefaultsRememberMeFlagKey;

@interface LoginManager : NSObject

+ (LoginManager *)instance;
+ (UIImage *)getProfilePicture;

- (void)logout;
- (void)fullLogout;
- (void)rememberMeImplementation;
- (BOOL)isSignedIn;
- (void)signIn:(NSString*)email password:(NSString*)password;
- (void)signInSecure;
- (BOOL)isFacebookUser;
- (NSString *)getProfileName;
- (void)internetReachabilityStart;
- (void)seperatePermissions:(NSArray *) permissions;

@property (assign, nonatomic) BOOL didRememberMePressed;

@property (nonatomic, strong) NSArray *domainUsers;
@property (nonatomic, strong) TVUser *domainMasterUser;
@property (nonatomic, strong) TVFacebookUser *facebookUser;

@property (nonatomic) NSInteger DomainRestriction;
@property (nonatomic) NSInteger nDeviceLimit;
@property (nonatomic) NSInteger concurrentDeviceLimit;
@property (nonatomic) NSInteger nCurrentDevices;

@property (nonatomic, strong) TVUser *userRegister;
@property (nonatomic, strong) NSString *userRegisterPassword;

@property (nonatomic, strong) TVUser *userAddToDomain;
@property (nonatomic, strong) NSString *userAddToDomainSiteGUID;

@property (nonatomic, strong) NSString *emailLogin;
@property (nonatomic, strong) NSString *passwordLogin;

@property (nonatomic, strong) NSDictionary *facebookScope;
@property (nonatomic, strong) Reachability *internetReachability;

@property (nonatomic, strong) NSString *externalShareWatches;

@property (nonatomic, strong) NSArray *readPermissions;
@property (nonatomic, strong) NSArray *writePermissions;

@end
