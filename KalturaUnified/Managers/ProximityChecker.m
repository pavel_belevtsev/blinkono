//
//  ProximityChecker.m
//  KalturaUnified
//
//  Created by p-obischenko on 09/02/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "ProximityChecker.h"
#import "APSTypography.h"

#ifdef PLAYER
    #import <TvinciSDK/DomainHomeNetworkValidatorManger.h>
#endif

@interface ProximityChecker () {
    NSTimer* _timer;
    NSTimeInterval _timeInterval;
    
    ProximityCheckerHandler _handler;
}

@end

@implementation ProximityChecker

#pragma mark - Singleton

+ (instancetype)instance {
    static ProximityChecker* checker = nil;
    if (!checker) {
        checker = [[ProximityChecker alloc] init];
    }
    return checker;
}

#pragma mark - Lifecycle

- (id)init {
    self = [super init];
    if (self) {
        _timeInterval = 6.f;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBecomeActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [self stopChecking];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Checking

- (BOOL)proximityEnabled {
    return [APSTypography instance].proximity;
}

- (BOOL)needCheckingProgramWithMetaData:(NSDictionary*)metaData catchingUp:(BOOL)catchingUp {
    NSString *value = [metaData objectForKey:catchingUp ? @"catchupOutOfHome" : @"liveTVOutOfHome"];
    BOOL skipChecking = metaData && value && ([value isKindOfClass:[NSString class]] && [value isEqualToString:@"YES"]);
    
    return [self proximityEnabled] && !skipChecking;
}

- (void)startChecking:(ProximityCheckerHandler)handler {
    _handler = handler;
    [self startTimer];
}

- (void)stopChecking {
    _handler = nil;
    [self stopTimer];
}

- (BOOL)check {
    BOOL inDomain = YES;
    
//#ifdef PLAYER
//    userNetworkInDomainStatus status = [[DomainHomeNetworkValidatorManger sharedDomainHomeNetworkValidatorManger] checkIfuserInDomainNetwork];
//    inDomain = status == userNetworkIsInDomain;
//#endif
    
    return inDomain;
}

#pragma mark - Timer

- (void)startTimer {
    _timer = [NSTimer scheduledTimerWithTimeInterval:_timeInterval target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];
    [_timer fire];
}

- (void)timerFireMethod:(NSTimer *)timer {
    if (_handler) {
        _handler([self check]);
    }
}

- (void)stopTimer {
    [_timer invalidate];
    _timer = nil;
}

#pragma mark - Notifications

- (void)didBecomeActiveNotification:(NSNotification*)notification {
    [self updateProximityState];
}

- (void)updateProximityState {
//#ifdef PLAYER
//    if ([TVConfigurationManager sharedTVConfigurationManager].state == TVConfigurationStateProperlyConfigured) {
//        [[DomainHomeNetworkValidatorManger sharedDomainHomeNetworkValidatorManger] reInit];
//    }
//#endif
}

@end
