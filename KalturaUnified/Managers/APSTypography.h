//
//  APSTypography.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APSTypography : NSObject

+ (APSTypography *)instance;

- (void)updateData;
- (void)becomeDefaultTypography;


@property (nonatomic, strong) NSString *brandName;
@property (nonatomic, strong) UIColor *brandColor;
@property (nonatomic, strong) UIColor *brandDarkColor;
@property (nonatomic, strong) UIColor *brandTextColor;
@property (nonatomic, strong) UIColor *facebookColor;

@property (nonatomic, strong) NSURL *aboutUsUrl;
@property (nonatomic, strong) NSURL *urlTermsOfService;
@property (nonatomic, strong) NSString *mailAddress;
@property (nonatomic, strong) NSURL *joinUrl;

@property (nonatomic, strong) NSString *googleAnalyticsId;
@property (nonatomic, strong) NSString *crittercismId;
@property (nonatomic, strong) NSDictionary *crashlyticsInfo;

@property (nonatomic, readwrite) BOOL category_16_9;
@property (nonatomic, readwrite) BOOL hasWatchList;
@property (nonatomic, readwrite) BOOL hasInAppPurchases;
@property (nonatomic, readwrite) BOOL hasDownloadToGo;
@property (nonatomic, strong) NSDictionary *dtgParams;
@property BOOL ipno;
@property BOOL multiUser;
@property BOOL menuFotterTV;

@property (nonatomic, strong) NSString *appBundlePhone;
@property (nonatomic, strong) NSString *appVersionPhone;
@property (nonatomic, strong) NSString *appBundlePad;
@property (nonatomic, strong) NSString *appVersionPad;

@property (nonatomic, strong) NSURL *logoUrl;

@property (nonatomic, readwrite) NSInteger numberOfRetroDays;
@property (nonatomic, readwrite) NSInteger numberOfWeekDays;

@property (nonatomic, strong) NSString *TVCMediaFormatMainHD;
@property (nonatomic, strong) NSString *TVCMediaFormatMainSD;
@property (nonatomic, strong) NSString *TVCMediaFormatTrailer;
@property (nonatomic, strong) NSString *TVCMediaFormatDownload;

@property (nonatomic, strong) NSArray *socialProviders;

@property BOOL socialFeed;
@property BOOL fbSupport;
@property BOOL enableFBSharePrivacy;
@property BOOL sharePrivacy;
@property BOOL searchEPG;
@property BOOL likeEPG;
@property BOOL likeLive;
@property BOOL STBFollowMe;
@property BOOL showHomePageTitles;
@property BOOL supportRecordings;
@property BOOL myZoneFullPageEnabled;
@property BOOL anonymousUserAllowedToPlayFreeItems;
@property (nonatomic, strong) NSArray *myZoneItems;
@property BOOL companion;
@property (nonatomic, assign) BOOL proximity;
@property (nonatomic, strong) NSString *deepLinkingScheme;
@property (nonatomic, readwrite) NSInteger pageSize;
@property BOOL showChannelNumber;
@property BOOL showGuestUser;
@property BOOL catchUpEPG;
@property BOOL  settingsAndLoginFromMenu;
@property (nonatomic, strong) NSNumber *followMeMinimalTime;
@property BOOL contactUsPageSettingsEnabled;
@property BOOL billingHistorySettingsEnabled;
@property BOOL hasCommercialization;

@property (nonatomic, readwrite) NSInteger minPasswordLength;



@end
