//
//  LikeAdapter.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseManagement.h"

@class LikeAdapter;
@protocol LikeAdapterDelegate <NSObject>

-(void) likeAdapter:(LikeAdapter*)likeAdapter comleteUpdateLikeStatus:(BOOL) status;

@end

@interface LikeAdapter : BaseManagement 

-(void) setupMediaItem:(TVMediaItem *) mediaItem;
-(void) setupProgram:(APSTVProgram *) program;

-(instancetype)initWithMediaItem:(TVMediaItem *) mediaItem control:(UIButton *) button;
-(instancetype)initWithMediaItem:(TVMediaItem *) mediaItem control:(UIButton *) button label:(UILabel *) label;
-(instancetype)initWithMediaItem:(TVMediaItem *) mediaItem control:(UIButton *) button label:(UILabel *) label imgLike:(UIImageView *) imgLike labelCounter:(UILabel *) labelCounter;

-(instancetype)initWithProgram:(APSTVProgram *) program control:(UIButton *) button;
-(instancetype)initWithProgram:(APSTVProgram *) program control:(UIButton *) button label:(UILabel *) label imgLike:(UIImageView *) imgLike labelCounter:(UILabel *) labelCounter;

- (void)refresh;

@property (retain, nonatomic) UIButton * buttonLike;
@property (weak, nonatomic) UILabel * labelLike;
@property (weak, nonatomic) UIImageView * imgLikeCounter;
@property (weak, nonatomic) UILabel * labelLikeCounter;
@property BOOL hideButtonTitleForLikes, needToGetAssetsStats;
@property (weak, nonatomic) id<LikeAdapterDelegate> delegate;

@end
