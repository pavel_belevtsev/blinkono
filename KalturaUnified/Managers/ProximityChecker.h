//
//  ProximityChecker.h
//  KalturaUnified
//
//  Created by p-obischenko on 09/02/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TVEPGProgram;
typedef void(^ProximityCheckerHandler)(BOOL inDomain);

@interface ProximityChecker : NSObject

+ (instancetype)instance;

- (BOOL)needCheckingProgramWithMetaData:(NSDictionary*)metaData catchingUp:(BOOL)catchingUp;
- (void)startChecking:(ProximityCheckerHandler)handler;
- (void)stopChecking;
- (BOOL)check;

@end
