//
//  DeviceManagement.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "DeviceManagement.h"

@implementation DeviceManagement


- (void)requestForGetDeviceDomains:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest * request = [TVPDomainAPI requestForGetDeviceDomains:nil];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
        
    }];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [self sendRequest:request];

    
}

- (void)requestForAddDeviceToDomainWithDeviceName:(NSString *)deviceName deviceBrandID:(NSInteger)deviceBrandID completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest *request = [TVPDomainAPI requestForAddDeviceToDomainWithDeviceName:deviceName deviceBrandID:deviceBrandID delegate:nil];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
        
    }];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [self sendRequest:request];
    
}

- (void)requestForSubmitAddDeviceToDomainRequestWithDeviceName:(NSString *)deviceName deviceBrandID:(NSInteger)deviceBrandID completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest *request = [TVPDomainAPI requestForSubmitAddDeviceToDomainRequestWithDeviceName:deviceName deviceBrandId:deviceBrandID delegate:nil];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
        
    }];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [self sendRequest:request];
    
}

- (void)requestForChangeDeviceDomainStatus:(BOOL)isActive completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForChangeDeviceDomainStatus:isActive delegate:nil];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
    
    }];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [self sendRequest:request];
    
}

- (void)checkDeviceDomain:(void(^)())completion {
    
    NSInteger masterDomainID = 0;
    __block NSInteger realDomainID = 0;
    
    id domainId = [[NSUserDefaults standardUserDefaults] objectForKey:@"DomainID"];
    if (domainId) {
        masterDomainID = [domainId integerValue];
    }
    
    BOOL isRegistered = [LoginM isSignedIn];
    [self requestForGetDeviceDomains:^(id jsonResponse) {
        
//        if (jsonResponse) {
        
            BOOL registered = NO;
            
            NSArray * domains = [jsonResponse arrayByRemovingNSNulls];
            for (NSDictionary * dict in domains) {
                
                id responseDomainId =  dict[@"DomainID"];
                if (responseDomainId) {
                    realDomainID = [responseDomainId integerValue];
                }
                
                if (realDomainID && masterDomainID && realDomainID == masterDomainID) {
                    registered = YES;
                }
            }
            
            if (!registered) {
                
                if (masterDomainID) {
                    
                    [LoginM logout];
                    
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DomainID"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [LoginM fullLogout];
                    
                } else {
                    
                    if (realDomainID) {
                        
                        [[NSUserDefaults standardUserDefaults] setObject:@(realDomainID) forKey:@"DomainID"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                }
            }
            
            TVUser *user = [TVSessionManager sharedTVSessionManager].currentUser;
            if (!user || [user.siteGUID intValue] == 0 || !isRegistered) {
                if (user) {
                    [[TVSessionManager sharedTVSessionManager] logoutFromIPNO];
                }
                registered = NO;
            }
//        }
        
        completion();
        
    }];
    
}

@end
