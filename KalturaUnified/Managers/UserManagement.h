//
//  UserManagement.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 28.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseManagement.h"

extern NSString * const KDomainUsersListUpdatedNotification;

@interface UserManagement : BaseManagement

- (void)requestForGetDomainMasterUsers:(void(^)(id jsonResponse))completionBlock failedBlock:(void(^)(NSString* localizedError))failedBlock;
- (void)requestForGetSiteGuidWithUserName:(NSString *)userName password:(NSString *)password completionBlock:(void(^)(id jsonResponse))completionBlock;
- (void)requestForSignUpWithNewUser:(TVUser *)user password:(NSString *)password completionBlock:(void(^)(id jsonResponse))completionBlock;
- (void)requestForAddUserToDomainWithDomainId:(NSInteger)domainID userGuid:(NSString *)userGuid masteUserGuid:(NSString *)masterUserGuid completionBlock:(void(^)(id jsonResponse))completionBlock;
- (void)requestForSubmitAddUserToDomainWithMasterUsername:(NSString *)masterUsername completionBlock:(void(^)(id jsonResponse))completionBlock;
- (void)requestForGetUserDetailsByUsername:(NSString *)username completionBlock:(void(^)(id jsonResponse))completionBlock;

- (void)loadDomainUsers:(void(^)())completion;


@end
