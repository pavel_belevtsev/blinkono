//
//  VFTypography.m
//  KalturaUnified
//
//  Created by Synergetica LLC on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "VFTypography.h"

@implementation VFTypography



+ (VFTypography *)instance {
    
    
    
    static APSTypography *instance = nil;
    
    @synchronized (self)
    {
        instance = [APSTypography instance];

        if ( instance == nil ||
            [instance isMemberOfClass: [APSTypography class]]
            )
        {
            instance = [[self alloc] init];
            [instance becomeDefaultTypography];
        }
    }
    return (VFTypography*)instance;
    
}

- (id)init {
    self = [super init];
    if (self)
    {
        // additional initialization
        self.darkGrayColor = [self grayWith255Value:51];
        self.grayScale1Color = [self grayWith255Value:66];
        self.grayScale2Color = [self grayWith255Value:102];
        self.grayScale3Color = [self grayWith255Value:140];
        self.grayScale4Color = [self grayWith255Value:153];
        self.grayScale5Color = [self grayWith255Value:175];
        self.grayScale6Color = [self grayWith255Value:204];
        self.grayScale7Color = [self grayWith255Value:235];
        self.grayScale8Color = [self grayWith255Value:244];
        self.brandColor = [UIColor colorWithRed:230.0/255.0 green:0.0 blue:0.0 alpha:1.0];
    }
    return self;
}


- (UIColor*) grayWith255Value:(float)value
{
    return [UIColor colorWithRed:value/255.0 green:value/255.0 blue:value/255.0 alpha:1.0];
}


- (void)updateData {
    
    [super updateData];
    self.brandColor = [UIColor colorWithRed:230.0/255.0 green:0 blue:0 alpha:1.0];
}

- (void) modifyBrandButtonToGray:(UIButton*) button
{
    [button setBackgroundImage:nil forState:UIControlStateNormal];
    [button setBackgroundImage:nil forState:UIControlStateHighlighted];
    
    button.layer.cornerRadius = 4;
    button.backgroundColor = [VFTypography instance].grayScale2Color;
    [button setTintColor:[VFTypography instance].grayScale8Color];

    [button setTitleColor:[VFTypography instance].grayScale8Color forState:UIControlStateNormal];
}



@end
