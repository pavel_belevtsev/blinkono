//
//  EPGManager.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 03.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGManager.h"

@implementation EPGManager

+ (EPGManager *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (id)init {
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)sortChannelsList:(NSMutableArray *)array {
    
    [array sortUsingComparator: ^(TVMediaItem *media1, TVMediaItem *media2) {
        
        id obj1 = [media1.metaData objectForKey:@"Channel number"];
        id obj2 = [media2.metaData objectForKey:@"Channel number"];
        
        if (obj1 && obj2) {
            
            if ([obj1 integerValue] > [obj2 integerValue]) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            
            if ([obj1 integerValue] < [obj2 integerValue]) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            
        }
        
        return (NSComparisonResult)NSOrderedSame;
    }];
    
}

- (void)loadEPGChannels:(BaseViewController *)delegate completion:(void(^)(NSMutableArray *result))completion {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    [self loadEPGChannels:delegate page:0 array:array completion:completion];
    

}

- (void)loadEPGChannels:(BaseViewController *)delegate page:(int)page array:(NSMutableArray *)array completion:(void(^)(NSMutableArray *result))completion {

    NSInteger channelId = 0;
    
    TVMenuItem *menuItem = [MenuM menuItem:TVPageLayoutEPG];
    if (menuItem) {
        
        channelId = [[menuItem.layout objectForKey:ConstKeyMenuItemChannel] integerValue];
        
    }
    
    [delegate requestForGetChannelMediaList:channelId pageSize:maxPageSize pageIndex:page orderBy:TVOrderByName completion:^(NSArray *result) {
        
        if ([result count]) {
            
            for (NSDictionary *dic in result) {
                TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dic];
                [array addObject:item];
            }
            
        }
        
        if ([result count] == maxPageSize) {
            
            [self loadEPGChannels:delegate page:(page + 1) array:array completion:completion];
            
        } else {
        
            [self sortChannelsList:array];
            
            completion(array);
            
        }
        
    }];
    
}

- (TVMediaItem *)getChannelById:(NSString *)mediaId {
    
    NSInteger key = [mediaId integerValue];
    
    for (TVMediaItem *channel in _channelsList) {
        if ([channel.mediaID integerValue] == key) {
            
            return channel;
            
        }
    }
    
    return nil;
}


- (void) updateEPGListWithFavorite:(TVMediaItem *)mediaItem {
    
    for (NSUInteger i = 0; i<self.channelsList.count; i++){
        
        TVMediaItem *item = [self.channelsList objectAtIndex:i];
        
        if ([item.mediaID isEqualToString:mediaItem.mediaID] && item.isFavorite != mediaItem.isFavorite){
            
            item.isFavorite = !item.isFavorite;
            
            [self.channelsList replaceObjectAtIndex:i withObject:item];
            
            break;
        }
    }
}

- (void)updateEPGListWithFavorite:(TVMediaItem *)mediaItem value:(BOOL)value {
    
    TVMediaItem *item = [[self.channelsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"mediaID == %@", mediaItem.mediaID]] firstObject];
    if (item && item.isFavorite != value)
        item.isFavorite = value;
}

- (void)updateEPGList:(NSMutableArray *)array delegate:(BaseViewController *)delegate completion:(void(^)(NSMutableArray *result))completion {
    
    self.channelsList = array;
    
    [self refreshUserRelatedActionsWithCompletion:completion delegate:delegate];
}

#pragma mark - Handle empty wholes in programs list

- (NSMutableArray *)addEmptyProgramsForEmptySpacesInProgramsArray:(NSMutableArray *)programsArray forDate:(NSDate *)currentDate privateContext:(NSManagedObjectContext *)privateContext {
    /*
    for (APSTVProgram *obj in programsArray) {
        NSLog(@"'%@' - '%@'\n%@", obj.startDateTime, obj.endDateTime, obj.name);
    }
    */
    if (programsArray.count > 0) {
        // start of day
        APSTVProgram *firstProgram = [programsArray objectAtIndex:0];
        
        unsigned int      intFlags   = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
        NSCalendar       *calendar   = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *componentsFirst = [[NSDateComponents alloc] init];
        
        componentsFirst = [calendar components:intFlags fromDate:currentDate];
        currentDate = [calendar dateFromComponents:componentsFirst];
        
        NSDate *startOfCurrentDate = currentDate;
        
        if ([self date:startOfCurrentDate isBetweenStartDate:firstProgram.startDateTime andEndDate:firstProgram.endDateTime]) {
            // do nothing
        } else {
            APSTVProgram *emptyProgram = [self createEmptyProgramWithStartTime:startOfCurrentDate endTime:firstProgram.startDateTime name:LS(@"tv_guide_empty_item_msg") programId:@"-1" privateContext:privateContext];
            if (emptyProgram) [programsArray insertObject:emptyProgram atIndex:0];
        }
        
        // mid of day
        NSInteger programIndex = 0;
        APSTVProgram *currentProgram;
        APSTVProgram *nextProgram;
        while (programIndex < programsArray.count-1) {
            currentProgram = [programsArray objectAtIndex:programIndex];
            nextProgram = [programsArray objectAtIndex:programIndex+1];
            if (![nextProgram.startDateTime isEqualToDate:currentProgram.endDateTime]) {
                APSTVProgram *emptyProgram = [self createEmptyProgramWithStartTime:currentProgram.endDateTime endTime:nextProgram.startDateTime name:LS(@"tv_guide_empty_item_msg") programId:@"-1" privateContext:privateContext];
                if (emptyProgram) {
                    [programsArray insertObject:emptyProgram atIndex:programIndex+1];
                    programIndex++;
                }
            }
            programIndex++;
        }
        
        // end of day
        APSTVProgram *lastProgram = [programsArray objectAtIndex:programsArray.count-1];
        
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setDay:1];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDate *endOfCurrentDate = [gregorian dateByAddingComponents:components toDate:currentDate options:0];
        
        if ([self date:endOfCurrentDate isBetweenStartDate:lastProgram.startDateTime andEndDate:lastProgram.endDateTime]) {
            // do nothing
        } else {
            APSTVProgram *emptyProgram = [self createEmptyProgramWithStartTime:lastProgram.endDateTime endTime:endOfCurrentDate name:LS(@"tv_guide_empty_item_msg") programId:@"-1" privateContext:privateContext];
            if (emptyProgram) [programsArray insertObject:emptyProgram atIndex:programsArray.count];
        }
    } else {
        
        NSDate *startOfCurrentDate = currentDate;
        
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setDay:1];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDate *endOfCurrentDate = [gregorian dateByAddingComponents:components toDate:currentDate options:0];
        
        APSTVProgram *emptyProgram = [self createEmptyProgramWithStartTime:startOfCurrentDate endTime:endOfCurrentDate name:LS(@"tv_guide_empty_item_msg") programId:@"-1" privateContext:privateContext];
        if (emptyProgram) [programsArray insertObject:emptyProgram atIndex:0];
    }
    
    return programsArray;
}

- (APSTVProgram *)createEmptyProgramWithStartTime:(NSDate *)startTime endTime:(NSDate *)endTime name:(NSString *)programName programId:(NSString *)programId privateContext:(NSManagedObjectContext *)privateContext {
    
    if ([startTime timeIntervalSince1970] > [endTime timeIntervalSince1970]) return nil;
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"APSTVProgram" inManagedObjectContext:privateContext];
    
    APSTVProgram *emptyProgram = (APSTVProgram *)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];

    emptyProgram.startDateTime = startTime;
    emptyProgram.endDateTime = endTime;
    emptyProgram.name = programName;
    emptyProgram.epgChannelID = programId;
    
    return emptyProgram;
}

- (BOOL)date:(NSDate *)date isBetweenStartDate:(NSDate *)beginDate andEndDate:(NSDate *)endDate {
    
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}

-(void) refreshUserRelatedActionsWithCompletion:(void(^)(NSMutableArray *result))completion delegate:(BaseViewController *)delegate
{
    NSMutableArray *arrayIds = [[NSMutableArray alloc] init];
    
    for (TVMediaItem *mediaItem in self.channelsList) {
        [arrayIds addObject:mediaItem.mediaID];
    }
    
    if ([arrayIds count]) {
        
        [delegate requestForGetAreMediasFavorites:arrayIds completion:^(NSArray *result) {
            
            NSMutableArray * favoritesChannelsIds = [NSMutableArray array];
            
            for (NSDictionary *favItem in result) {
                if ([[favItem objectForKey:@"Value"] intValue] && [favItem objectForKey:@"Key"]) {
                    [favoritesChannelsIds addObject:[NSString stringWithFormat:@"%@",[favItem objectForKey:@"Key"]]];
                }
            }
            
            for (TVMediaItem * channel in self.channelsList)
            {
                if ([favoritesChannelsIds containsObject:channel.mediaID])
                {
                    channel.isFavorite = YES;
                }
                else
                {
                    channel.isFavorite  = NO;
                }
            }
            
           
            
            completion(self.channelsList);
            
        }];
        
        
    } else {
        
        completion(self.channelsList);
        
    }
    
    }

@end
