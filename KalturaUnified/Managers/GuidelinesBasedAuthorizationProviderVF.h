//
//  GuidelinesBasedAuthorizationProviderVF.h
//  Vodafone
//
//  Created by Israel Berezin on 6/17/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "GuidelinesBasedAuthorizationProvider.h"

@interface GuidelinesBasedAuthorizationProviderVF : GuidelinesBasedAuthorizationProvider

@end
