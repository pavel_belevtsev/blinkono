//
//  AnalyticsEvent.m
//  KalturaUnified
//
//  Created by Admin on 23.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "AnalyticsEvent.h"

@implementation AnalyticsEvent

- (void)clearFields {
    self.category = @"";
    self.action = @"";
    self.label = @"";
    self.value = nil;
}

@end
