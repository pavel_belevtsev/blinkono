//
//  AnalyticsEvent.h
//  KalturaUnified
//
//  Created by Admin on 23.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnalyticsEvent : NSObject

@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *action;
@property (nonatomic, strong) NSString *label;
@property (nonatomic, strong) NSNumber *value;

- (void)clearFields;

@end
