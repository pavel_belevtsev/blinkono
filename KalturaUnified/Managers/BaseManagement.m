//
//  BaseManagement.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseManagement.h"

@implementation BaseManagement

- (void)dealloc {
    [self clearQueueOfRequest];
    self.networkQueue = nil;
}

#pragma mark - TVNetworkQueue

- (TVNetworkQueue *)networkQueue {
    if (_networkQueue == nil) {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

- (void)sendRequest:(ASIHTTPRequest *)request {
    if (request != nil) {
        [self.networkQueue sendRequest:request];
    }
}

- (void)clearQueueOfRequest {
    [self.networkQueue cleen];
}


@end
