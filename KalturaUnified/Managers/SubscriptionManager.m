//
//  SubscriptionManager.m
//  KalturaUnified
//
//  Created by Alex Zchut on 2/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SubscriptionManager.h"
#import <TvinciSDK/TVinci.h>
#import "NSDictionary+Additions.h"

typedef void (^mediaItemSubscriptionsCallback)(NSArray *arrSubscriptionsPurchased, NSArray *arrSubscriptionsToPurchase);

@implementation SubscriptionManager

+ (SubscriptionManager *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

#pragma mark - Medias
- (void) getMediasForPermittedSubscriptions:(BaseViewController *)delegateController isForUser:(BOOL) isForUser renewal:(BOOL)isRenewal completion:(void(^)(NSMutableArray *arrMedias))completion {
    [self getPermitedSubscriptions:delegateController isForUser:isForUser completion:^(NSArray *arrPermittedSubscriptions) {
        NSArray *arr = [arrPermittedSubscriptions filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVSubscriptionData_purchasingInfo *pInfo, NSDictionary *bindings) {
            return (pInfo.isSubRenewable == isRenewal);
        }]];
        
        NSArray *arrSubscriptionsIds = [arr valueForKeyPath:@"@distinctUnionOfObjects.subscriptionCode"];
        if (arrSubscriptionsIds.count) {
            [self getSubscriptionMedias:arrSubscriptionsIds delegate:delegateController completion:completion];
        }
        else {
            completion(nil);
        }
    }];
}

- (void) getMediasForExpiredSubscriptions:(BaseViewController *)delegateController isForUser:(BOOL) isForUser renewal:(BOOL)isRenewal completion:(void(^)(NSMutableArray *arrMedias))completion {
    
    [self getExpiredSubscriptions:delegateController isForUser:isForUser completion:^(NSArray *arrExpiredubscriptions) {
        NSArray *arr = [arrExpiredubscriptions filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVSubscriptionData_purchasingInfo *pInfo, NSDictionary *bindings) {
            return (pInfo.isSubRenewable == isRenewal);
        }]];
        
        NSArray *arrSubscriptionsIds = [arr valueForKeyPath:@"@distinctUnionOfObjects.subscriptionCode"];
        if (arrSubscriptionsIds.count) {
            [self getSubscriptionMedias:arrSubscriptionsIds delegate:delegateController completion:completion];
        }
        else {
            completion(nil);
        }
    }];
}

- (void) getSubscriptionMedias:(NSArray*)arrSubscriptionIds delegate:(BaseViewController *)delegateController completion:(void(^)(NSMutableArray *arrMedias))completion {
    
    __block NSMutableArray* arrMediaItems = [NSMutableArray array];
    dispatch_group_t group = dispatch_group_create();
    
    for (NSNumber *subId in arrSubscriptionIds) {
        dispatch_group_enter(group);
        __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetMediasInPackage:[subId integerValue]
                                                                       pictureSize:(isPhone)?[TVPictureSize iPhoneItemCellPictureSize] : [TVPictureSize iPadItemCellPictureSize]
                                                                          pageSize:100
                                                                         pageIndex:0
                                                                          delegate:nil];
            
        
        [request setCompletionBlock:^{
            NSArray *arrMedias = [self parseMediasDetailsToObjects:[request JSONResponse]];
            if (arrMedias.count) {
                [arrMediaItems addObjectsFromArray:arrMedias];
            }
            dispatch_group_leave(group);
        }];
        
        [request setFailedBlock:^{
            dispatch_group_leave(group);
        }];
        
        [delegateController sendRequest:request];
    }
    
    dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        completion(arrMediaItems);
    });
}

- (void) getSubscriptionsContainingMediaItem:(TVMediaItem *)mediaItem delegate:(BaseViewController *)delegate completion: (mediaItemSubscriptionsCallback) completion {
    
    NSArray *arrFiles = [mediaItem.files filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"format == %@", APST.TVCMediaFormatMainHD]];
    if (!arrFiles.count)
        arrFiles = [mediaItem.files filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"format == %@", APST.TVCMediaFormatMainSD]];
    
    if (arrFiles.count) {
        TVFile *file = [arrFiles firstObject];
        __weak TVPAPIRequest * request = [TVPPricingAPI requestforGetSubscriptionIDsContainingMediaFile:^(RPGetSubscriptionIDsContainingMediaFile *requestParams) {
            requestParams.mediaId = [mediaItem.mediaID integerValue];
            requestParams.fileId = [file.fileID integerValue];
        }];
        
        [request setCompletionBlock:^{
            NSArray *arrSubscriptionIds = [[request JSONResponse] arrayByRemovingNSNulls];
            [self getSubscriptionsPricesWithCoupon:arrSubscriptionIds delegate:delegate completion:completion];
        }];
        
        [request setFailedBlock:^{
            completion(nil, nil);
        }];
        [delegate sendRequest:request];
    }
    else
        completion(nil, nil);
}

#pragma mark - SubscriptionsPricesWithCoupon
- (void) getSubscriptionsPricesWithCoupon:(NSArray*)arrSubscriptionIds delegate:(BaseViewController *)delegateController completion:(mediaItemSubscriptionsCallback)completion {
    
    if (!arrSubscriptionIds.count) {
        completion(nil, nil);
        return;
    }
    
    NSString *userGuid = @"";
    if ([TVSessionManager sharedTVSessionManager].currentUser) {
        userGuid = [TVSessionManager sharedTVSessionManager].currentUser.siteGUID;
    }
    
    __weak TVPAPIRequest * request = [TVPConditionalAccessAPI requestForGetSubscriptionsPricesWithCoupon:^(RPGetSubscriptionsPricesWithCoupon *requestParams) {
        
        requestParams.arrSubscriptionIds = arrSubscriptionIds;
        requestParams.userGuid = userGuid;
        requestParams.couponCode = @"";
        requestParams.languageCode = @"";
        requestParams.deviceName = @"";
    }];
    
    [request setCompletionBlock:^{
        NSArray *arrSubscriptionsPrices = [[request JSONResponse] arrayByRemovingNSNulls];
        
        NSMutableArray *arrNotAllowedSubscriptionIds = [NSMutableArray array];
        NSMutableArray *arrAllowedSubscriptionsIds = [NSMutableArray array];
        for (NSDictionary *dict in arrSubscriptionsPrices) {
            
            TVSubscriptionPrice *billingInfo = [[TVSubscriptionPrice alloc] initWithDictionary:dict];
            if ([MediaGRA isPriceReasonPurchased:(TVPriceType)billingInfo.priceReason]) {
                [arrAllowedSubscriptionsIds addObject:billingInfo.subscriptionCode];
                break;
            }
            else {
                [arrNotAllowedSubscriptionIds addObject:billingInfo.subscriptionCode];
            }
        }
        
        //get subscriptions data for all ids
        NSMutableArray *arrIDs = [NSMutableArray arrayWithArray:arrNotAllowedSubscriptionIds];
        [arrIDs addObjectsFromArray:arrAllowedSubscriptionsIds];
        
        NSPredicate *predicateNotAllowedSubscriptionIds = [NSPredicate predicateWithFormat:@"SELF IN %@", arrNotAllowedSubscriptionIds];
        NSPredicate *predicateAllowedSubscriptionsIds = [NSPredicate predicateWithFormat:@"SELF IN %@", arrAllowedSubscriptionsIds];
        
        [self getSubscriptionsData:arrIDs delegate:delegateController needPurchasingInfo:TRUE completion:^(NSArray *arrSubscriptions) {
            NSMutableArray *arrAllowedSubscriptions = [NSMutableArray array];
            NSMutableArray *arrNotAllowedSubscriptions = [NSMutableArray array];
            for (TVSubscriptionData *subData in arrSubscriptions) {
                BOOL bAdded = FALSE;
                if (arrAllowedSubscriptionsIds.count)
                    if ([predicateAllowedSubscriptionsIds evaluateWithObject:subData.subscriptionCode]) {
                        [arrAllowedSubscriptions addObject:subData];
                        bAdded = TRUE;
                    }
                if (!bAdded) {
                    if (arrNotAllowedSubscriptionIds.count)
                        if ([predicateNotAllowedSubscriptionIds evaluateWithObject:subData.subscriptionCode])
                            [arrNotAllowedSubscriptions addObject:subData];
                }
            }
            
            //#warning REMOVE THESE DEBUG LINES
            //#ifdef DEBUG
            //            if (arrNotAllowedSubscriptions.count>1 && !arrAllowedSubscriptions.count)
            //                arrAllowedSubscriptions = [NSMutableArray arrayWithObject:arrNotAllowedSubscriptions[0]];
            //
            //            TVSubscriptionData *subscriptionData = arrAllowedSubscriptions[0];
            //            if (!subscriptionData.purchasingInfo) {
            //                    subscriptionData.purchasingInfo = [TVSubscriptionData_purchasingInfo new];
            //                    subscriptionData.purchasingInfo.stringPurchaseDate = @"2015-02-15T10:47:00";
            //                    subscriptionData.purchasingInfo.stringEndDate = @"2015-04-15T20:47:00";
            //                }
            //
            //#endif
            completion(arrAllowedSubscriptions, arrNotAllowedSubscriptions);
        }];
    }];
    
    [request setFailedBlock:^{
        completion(@[], @[]);
    }];
    
    [delegateController sendRequest:request];
}


#pragma mark - SubscriptionsData
- (void) getSubscriptionsData:(NSArray*)arrSubscriptionIds delegate:(BaseViewController *)delegateController completion:(void(^)(NSArray *arrSubscriptions))completion {
    [self getSubscriptionsData:arrSubscriptionIds delegate:delegateController needPurchasingInfo:FALSE completion:completion ];
}

- (void) getSubscriptionsData:(NSArray*)arrSubscriptionIds delegate:(BaseViewController *)delegateController needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptions))completion
{
    [self getSubscriptionsData:arrSubscriptionIds delegate:delegateController needPurchasingInfo:needPurchasingInfo completion:completion withInApp:YES];
}


- (void) getSubscriptionsData:(NSArray*)arrSubscriptionIds delegate:(BaseViewController *)delegateController needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptions))completion withInApp:(BOOL) withInApp{
    
    if (!arrSubscriptionIds.count) {
        completion(@[]);
        return;
    }
    
    __weak TVPAPIRequest * request = [TVPPricingAPI requestforGetSubscriptionData:^(RPGetSubscriptionData *requestParams) {
        requestParams.arrSubscriptionIds = arrSubscriptionIds;
    }];
    
    [request setCompletionBlock:^{
        NSArray *arr = [[request JSONResponse] arrayByRemovingNSNulls];
        NSMutableArray *arrSubscriptions = [NSMutableArray array];
        
        for (NSDictionary *dict in arr) {
            NSError *err = nil;
            TVSubscriptionData *sub = [[TVSubscriptionData alloc] initWithDictionary:dict error:&err];
            // "PSC : Vodafone Ono : OPF-1593 CLONE - Available Subscription is empty while user purchased Subscription
            if (withInApp)
            {
                if (sub && sub.productCode.length > 0) {
                    [arrSubscriptions addObject:sub];
                }
            }
            else
            {   if (sub)
                {
                 [arrSubscriptions addObject:sub];
                }
            }
        }
        
        if (needPurchasingInfo) {
            [self getPermitedSubscriptions:delegateController isForUser:FALSE completion:^(NSArray *arrPermittedSubscriptions) {
                if (arrPermittedSubscriptions.count && withInApp == YES)
                {
                    [arrSubscriptions enumerateObjectsUsingBlock:^(TVSubscriptionData *obj, NSUInteger idx, BOOL *stop) {
                        //assigning pruchasing info to subscription data
                        obj.purchasingInfo = [[arrPermittedSubscriptions filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVSubscriptionData_purchasingInfo *pi, NSDictionary *bindings) {
                            return [[@(pi.subscriptionCode) stringValue] isEqualToString: obj.subscriptionCode];
                        }]] firstObject];
                    }];
                }
                if (!withInApp)
                {
                    if (completion)
                    {
                        completion(arrSubscriptions);
                    }
                }
                else
                {
                    [self getSubscriptionsDataStep2:arrSubscriptions completion:completion];
                }

            }];
        }
        else {
            if (!withInApp)
            {
                if (completion)
                {
                    completion(arrSubscriptions);
                }
            }
            else
            {
                [self getSubscriptionsDataStep2:arrSubscriptions completion:completion];
            }
        }
    }];
    
    [request setFailedBlock:^{
        completion(@[]);
    }];
    
    [delegateController sendRequest:request];
}

- (void) getSubscriptionsDataStep2:(NSMutableArray*)arrSubscriptions completion:(void(^)(NSArray *arrSubscriptions))completion {
    
    __block NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    [IAP getSKProductsForProductCodes:[arrSubscriptions valueForKeyPath:@"@distinctUnionOfObjects.productCode"] completion:^(NSArray *arrSKProducts) {
        for (SKProduct *product in arrSKProducts) {
            TVSubscriptionData *data = [arrSubscriptions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productCode == %@", product.productIdentifier]].firstObject;
            if (data)
            {
                data.priceCode = [TVSubscriptionData_subscriptionPriceCode new];
                data.priceCode.prise = [TVSubscriptionData_prise new];
                data.priceCode.prise.price = [product.price floatValue];
                
                [numberFormatter setLocale:product.priceLocale];
                data.priceCode.prise.name = [numberFormatter stringFromNumber:product.price];
            }
        }
        
        //remove subscriptions without SKProduct
        [arrSubscriptions removeObjectsInArray:[arrSubscriptions filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVSubscriptionData *data, NSDictionary *bindings) {
            return (data.priceCode == nil);
        }]]];
        
        completion(arrSubscriptions);
    }];
}


#pragma mark - GetSubscriptions
- (void) getExpiredSubscriptions:(BaseViewController *)delegateController isForUser:(BOOL)isForUser completion:(void(^)(NSArray *arrExpiredSubscriptions))completion {
    
    //TODO: NO DOMAIN EXPIRED SUBSCRIPTION API - isForUser not used
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetUserExpiredSubscriptionsWithBatchSize:100 delegate:nil];
    
    [request setCompletionBlock:^{
        NSArray *arr = [[request JSONResponse] arrayByRemovingNSNulls];
        NSMutableArray *arrExpiredSubscriptions = [NSMutableArray array];
        
        for (NSDictionary *dict in arr) {
            NSError *err = nil;
            TVSubscriptionData_purchasingInfo *sub = [[TVSubscriptionData_purchasingInfo alloc] initWithDictionary:dict error:&err];
            [arrExpiredSubscriptions addObject:sub];
        }
        
        completion(arrExpiredSubscriptions);
    }];
    
    [request setFailedBlock:^{
        completion(nil);
    }];
    
    [delegateController sendRequest:request];
}

- (void) getPermitedSubscriptions:(BaseViewController *)delegateController isForUser:(BOOL)isForUser completion:(void(^)(NSArray *arrPermittedSubscriptions))completion {
    
    __weak TVPAPIRequest * request = (isForUser) ? [TVPMediaAPI requestForGetUserPermittedSubscriptions] : [TVPConditionalAccessAPI requestForGetDomainPermittedSubscriptions];
    
    [request setCompletionBlock:^{
        NSArray *arr = [[request JSONResponse] arrayByRemovingNSNulls];
        NSMutableArray *arrPermittedSubscriptions = [NSMutableArray array];
        
        for (NSDictionary *dict in arr) {
            NSError *err = nil;
            TVSubscriptionData_purchasingInfo *sub = [[TVSubscriptionData_purchasingInfo alloc] initWithDictionary:dict error:&err];
            [arrPermittedSubscriptions addObject:sub];
        }
        
        completion(arrPermittedSubscriptions);
    }];
    
    [request setFailedBlock:^{
        completion(nil);
    }];
    
    [delegateController sendRequest:request];
}

#pragma mark - SubscriptionProductCode
- (void) getSubscriptionProductCode:(NSInteger) subscriptionCode delegate:(BaseViewController *)delegateController completion:(void(^)(NSString *productCode))completion {
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetSubscriptionProductCode:^(RPGetSubscriptionProductCode *requestParams) {
        requestParams.subscriptionCode = subscriptionCode;
    }];
    
    [request setCompletionBlock:^{
        completion([request JSONResponse]);
    }];
    
    [request setFailedBlock:^{
        completion(nil);
    }];
    
    [delegateController sendRequest:request];
}

#pragma mark - AllSubscriptionsDataFromVirtualMedias
- (void) getAllSubscriptionIdsFromVirtualMedias:(BaseViewController *)delegateController completion:(void(^)(NSArray *arrSubscriptionsIds, NSArray *arrVirtualMedias))completion {
    
    NSString *mediaPackageType = [TVConfigurationManager sharedTVConfigurationManager].mediaTypes[TVMediaTypePackage]; //package
    
    //get all virtual medias for subscripptions
    [delegateController requestForSearchMediaOrList:@[] AndList:@[] mediaType:mediaPackageType pageSize:100 pageIndex:0 orderBy:TVOrderByAndOrList_ID exact:NO completionBlock:^(id jsonResponse){
        NSArray *result = jsonResponse;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Key == 'Base ID'"];
        NSArray *filteredArray = [[[result valueForKeyPath:@"@unionOfArrays.Metas"] filteredArrayUsingPredicate:predicate] valueForKeyPath:@"@distinctUnionOfObjects.Value"];
        NSMutableArray *filteredAndNotEmpty = [NSMutableArray arrayWithArray:filteredArray];
        [filteredAndNotEmpty removeObject:@""];
        //save subscriptions Item Details
        NSArray *arrVirtualMedias = [self parseMediasDetailsToObjects:result];
        completion(filteredAndNotEmpty, arrVirtualMedias);
    } failedBlock:^(NSString* localizedError) {
        completion(@[], @[]);
    }];
}


- (void) getAllSubscriptionDataFromVirtualMedias:(BaseViewController *)delegateController needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptionsData))completion
{
     [self getAllSubscriptionDataFromVirtualMedias:delegateController appendIDs:nil needPurchasingInfo:needPurchasingInfo completion:completion withInApp:YES];
}

- (void) getAllSubscriptionDataFromVirtualMedias:(BaseViewController *)delegateController appendIDs:(NSArray*) arrSubIDsToAppend needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptionsData))completion
{
   [self getAllSubscriptionDataFromVirtualMedias:delegateController appendIDs:arrSubIDsToAppend needPurchasingInfo:needPurchasingInfo completion:completion withInApp:YES];
}


- (void) getAllSubscriptionDataFromVirtualMedias:(BaseViewController *)delegateController needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptionsData))completion withInApp:(BOOL) withInapp{
    [self getAllSubscriptionDataFromVirtualMedias:delegateController appendIDs:nil needPurchasingInfo:needPurchasingInfo completion:completion withInApp:withInapp];
    
}

- (void) getAllSubscriptionDataFromVirtualMedias:(BaseViewController *)delegateController appendIDs:(NSArray*) arrIDsToAppend needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptionsData))completion withInApp:(BOOL) withInApp {
    [self getAllSubscriptionIdsFromVirtualMedias:delegateController completion:^(NSArray *arrSubscriptionsIds, NSArray *arrVirtualMedias) {
        NSMutableArray *arrIds = [NSMutableArray arrayWithArray:arrSubscriptionsIds];
        //check if there are item related subscriptions and add to array
        if (arrIDsToAppend && arrIDsToAppend.count)
            [arrIds addObjectsFromArray:arrIDsToAppend];
        //remove duplicate values
        [arrIds setArray:[[NSSet setWithArray:arrIds] allObjects]];
        
        [self getSubscriptionsData:arrIds delegate:delegateController needPurchasingInfo:needPurchasingInfo completion:^(NSArray *arrSubscriptions) {
            
            [arrSubscriptions enumerateObjectsUsingBlock:^(TVSubscriptionData *obj, NSUInteger idx, BOOL *stop) {
                //assigning virtual media to subscription data
                obj.virtualMediaItem = [[arrVirtualMedias filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVMediaItem *item, NSDictionary *bindings) {
                    return [item.metaData[@"Base ID"] isEqualToString: obj.subscriptionCode];
                }]] firstObject];
            }];
            
            completion(arrSubscriptions);
        } withInApp:withInApp];
    }];
}

#pragma mark - parse medias details
- (NSArray*) parseMediasDetailsToObjects:(NSArray*) arrMedias {
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *dict in arrMedias) {
        TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dict];
        [arr addObject:item];
    }
    return arr;
}

#pragma mark - ChargeUserWithInApp
- (void) chargeUserWithInApp:(SubscriptionChargeUserWithInAppRequestParams *)params completion: (void (^)(BOOL success, NSString* statusDescription)) completion {
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForChargeUserWithInApp:^(RPChargeUserWithInApp *requestParams) {
        TVSubscriptionData *sub = params.subscriptionData;
        requestParams.currency = sub.subscriptionPriceCode.prise.currency.currencyCD3;
        requestParams.price = sub.subscriptionPriceCode.prise.price;
        requestParams.productCode = params.productCode;
        requestParams.receipt = [params.receipt base64EncodedStringWithOptions:0];
    }];
    
    [request setCompletionBlock:^{
        NSDictionary *dictResponse = [request JSONResponse];
        NSLog(@"SubscriptionManager.chargeUserWithInApp: %@", dictResponse);
        
        //get updated purchased subscription data
        [self getPermitedSubscriptions:params.delegate isForUser:TRUE completion:^(NSArray *arrPermittedSubscriptions) {
            if (arrPermittedSubscriptions.count)
                params.subscriptionData.purchasingInfo = [[arrPermittedSubscriptions filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVSubscriptionData_purchasingInfo *pi, NSDictionary *bindings) {
                    return [[@(pi.subscriptionCode) stringValue] isEqualToString: params.subscriptionData.subscriptionCode];
                }]] firstObject];
            
            completion((params.subscriptionData.purchasingInfo) ? TRUE : FALSE, dictResponse[@"m_sStatusDescription"]);
        }];
        
    }];
    
    [request setFailedBlock:^{
        completion(FALSE, nil);
    }];
    
    [params.delegate sendRequest:request];
}

#pragma mark - cancel
- (void) cancelSubscription:(NSInteger) subscriptionCode transaction:(NSInteger) transactionPurchaseId delegate:(BaseViewController *)delegateController completion:(void(^)(BOOL result))completion {
    __weak TVPAPIRequest * request = [TVPConditionalAccessAPI requestForCancelSubscription:^(RPCancelSubscription *requestParams) {
        requestParams.subId = subscriptionCode;
        requestParams.purchaseId = transactionPurchaseId;
    }];
    
    [request setCompletionBlock:^{
        NSLog(@"cancelSubscription: %@", [request JSONResponse]);
        completion(TRUE);
    }];
    
    [request setFailedBlock:^{
        completion(FALSE);
    }];
    
    [delegateController sendRequest:request];
}

#pragma mark - additions
//get non- recurring subscription left period
- (NSString*) getNRSubscriptionLeftPeriod:(TVSubscriptionData*) data {
    NSString *timeLeftString = @"";
    NSInteger timeLeftValue = [[NSDate date] distanceInDaysToDate: data.purchasingInfo.endDate];
    if (timeLeftValue >= 2) {
        timeLeftString = (timeLeftValue > 1) ? LS(@"item_page_buy_subscription_days_left") : LS(@"item_page_buy_subscription_day_left");
    }
    else {
        timeLeftValue = [[NSDate date] distanceInHoursToDate: data.purchasingInfo.endDate];
        timeLeftString = (timeLeftValue > 1) ? LS(@"item_page_buy_subscription_hours_left") : LS(@"item_page_buy_subscription_hour_left");
    }
    
    return [NSString stringWithFormat:timeLeftString, @(timeLeftValue)];
}

@end
