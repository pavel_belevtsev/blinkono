//
//  RecoredAdapter.m
//  KalturaUnified
//
//  Created by Israel Berezin on 2/10/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "RecoredAdapter.h"

#define seriesSourceKey @"season"
#define seasonIdKey @"seasonId"

@interface RecoredAdapter()
//@property NSInteger numberOfDeleateFunctionComplate;
@end
@implementation RecoredAdapter


-(id)init
{
    self = [super init];
    if (self)
    {
        self.recoredDicByChannels = [[NSMutableDictionary alloc]init];
    }
    return self;
}


#pragma mark - delete or cancel one rcored Item -

-(void)deleteOneRecored:(TVNPVRRecordItem *) recordItem Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    __block TVNPVRRecordItem * weakRecordItem = recordItem;
    __weak APSTVProgram * weakEpgProgram = epgProgram;
    __weak TVMediaItem * weakMediaItem = mediaItem;

    NSString * recordID =[self recredAssetIdToRecording:recordItem Program:epgProgram mediaItem:mediaItem];

    if (recordID == nil || [recordID isEqualToString:@"-10"])
    {

        [self loadRecoredByProgram:epgProgram andChannel:mediaItem completion:^(BOOL finished) {
            if (finished == YES)
            {
                RecoredHelper * rec = [self recoredHelperToepgProgram:epgProgram mediaItem:mediaItem];
                weakRecordItem = rec.recordItem;
                [self chooseDeleteOrCancelOneRecored:weakRecordItem Program:weakEpgProgram mediaItem:weakMediaItem];
            }
            else
            {
                 [self.delegate recoredAdapter:self complateCancelOrDeleateAssetRecording:weakRecordItem Program:weakEpgProgram mediaItem:weakMediaItem recoredAdapterCallName:RecoredAdapterCallNameDeleateItem recoredAdapterStatus:RecoredAdapterStatusFailed];
            }
        }];
        return;
    }
    [self chooseDeleteOrCancelOneRecored:recordItem Program:epgProgram mediaItem:mediaItem];
}

-(void)chooseDeleteOrCancelOneRecored:(TVNPVRRecordItem *) recordItem Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    NSDate* startTime = epgProgram.startDateTime;
    NSDate* endTime = epgProgram.endDateTime;
    NSDate * now = [NSDate date];

    if([now compare:startTime] == NSOrderedDescending && [now compare:endTime] == NSOrderedAscending) // now!
    {
        [self deleteAssetRecording:recordItem Program:epgProgram mediaItem:mediaItem];
    }
    else if([now compare:startTime] == NSOrderedDescending) // before!
    {
        [self deleteAssetRecording:recordItem Program:epgProgram mediaItem:mediaItem];
    }
    else // futher
    {
        [self cancelAssetRecording:recordItem Program:epgProgram mediaItem:mediaItem];
    }
}

- (void)cancelAssetRecording:(TVNPVRRecordItem *) recordItem  Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    NSString * recordID =[self recredAssetIdToRecording:recordItem Program:epgProgram mediaItem:mediaItem];
    __weak TVNPVRRecordItem * weakRecordItem = recordItem;
    __weak APSTVProgram * weakEpgProgram = epgProgram;
    __weak TVMediaItem * weakMediaItem = mediaItem;

    
    if (recordID != nil && [recordID length]>0)
    {
        __weak TVPAPIRequest * request = [TVPMediaAPI requestForCancelAssetRecordingWithRecordingId:recordID delegate:nil];
        
        [request setFailedBlock:^{
            
            NSLog(@"%@",request);
        }];
        
        [request setCompletionBlock:^{
            //NSLog(@"%@",request);
            [self complateCancelOrDeleateRecord:weakRecordItem Program:weakEpgProgram mediaItem:weakMediaItem request:request recoredAdapterCallName:RecoredAdapterCallNameCancelItem];
        }];
        
        [self sendRequest:request];
    }
    else
    {
         [self.delegate recoredAdapter:self complateCancelOrDeleateAssetRecording:recordItem Program:epgProgram mediaItem:mediaItem recoredAdapterCallName:RecoredAdapterCallNameCancelItem recoredAdapterStatus:RecoredAdapterStatusFailed];
    }
}


- (void)deleteAssetRecording:(TVNPVRRecordItem *) recordItem Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    __weak TVNPVRRecordItem * weakRecordItem = recordItem;
    __weak APSTVProgram * weakEpgProgram = epgProgram;
    __weak TVMediaItem * weakMediaItem = mediaItem;
    
    NSString * recordID =[self recredAssetIdToRecording:recordItem Program:epgProgram mediaItem:mediaItem];
    if (recordID != nil && [recordID length]>0)
    {
        __weak TVPAPIRequest * request = [TVPMediaAPI requestForDeleteAssetRecordingWithRecordingId:recordID delegate:nil];
        
        [request setFailedBlock:^{
            
            NSLog(@"%@",request);
            [self.delegate recoredAdapter:self complateCancelOrDeleateAssetRecording:recordItem Program:epgProgram mediaItem:mediaItem recoredAdapterCallName:RecoredAdapterCallNameDeleateItem recoredAdapterStatus:RecoredAdapterStatusFailed];
            
        }];
        
        [request setCompletionBlock:^{
            
            //NSLog(@"%@",request);
            [self complateCancelOrDeleateRecord:weakRecordItem Program:weakEpgProgram mediaItem:weakMediaItem request:request recoredAdapterCallName:RecoredAdapterCallNameDeleateItem];

        }];
        
        [self sendRequest:request];
    }
    else
    {
         [self.delegate recoredAdapter:self complateCancelOrDeleateAssetRecording:recordItem Program:epgProgram mediaItem:mediaItem recoredAdapterCallName:RecoredAdapterCallNameDeleateItem recoredAdapterStatus:RecoredAdapterStatusFailed];
    }
}

-(void)complateCancelOrDeleateRecord:(TVNPVRRecordItem *) recordItem  Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem  request:(TVPAPIRequest *) request recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName
{
    NSDictionary *response = [request JSONResponse];
    NSString * status = [response objectForKey:@"status"];
    RecoredAdapterStatus recoredAdapterStatus;
    if ([status isEqualToString:@"OK"] || [status isEqualToString:@"InvalidAssetID"])
    {
        recoredAdapterStatus = RecoredAdapterStatusOk;
        // remove item from local memory.
        NSMutableDictionary * dic = [self takeAllChannelRecoreds:mediaItem];
        [dic removeObjectForKey:epgProgram.epgId];
        if (!dic) {
            [self.recoredDicByChannels setObject:dic forKey:mediaItem.mediaID];
        }
    }
    else
    {
        recoredAdapterStatus = RecoredAdapterStatusFailed;
    }
    if([self.delegate respondsToSelector:@selector(recoredAdapter:complateCancelOrDeleateAssetRecording:Program:mediaItem:recoredAdapterCallName:recoredAdapterStatus:)])
    {
        [self.delegate recoredAdapter:self complateCancelOrDeleateAssetRecording:recordItem Program:epgProgram mediaItem:mediaItem recoredAdapterCallName:recoredAdapterCallName recoredAdapterStatus:recoredAdapterStatus];
    }
}

-(void) cancelOrDeleteSeriesRecordingWithCnacelRecoredType:(CnacelRecoredType)cnacelRecoredType WithRecoredHelper:(RecoredHelper*)recoredHelper
{
    if ([recoredHelper.recoredSereisId length]>0)
    {
        [self makeCancelOrDeleteSeriesRecordingWithCnacelRecoredType:cnacelRecoredType WithRecoredHelper:recoredHelper];
    }
    else
    {
        [self getSeriesRecordingsWithRecoredItem:recoredHelper AndCnacelRecoredType:cnacelRecoredType];
    }
}

#pragma mark - recored - 

- (void)recordSeriesByProgram:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    __weak APSTVProgram * weakEpgProgram = epgProgram;
    __weak TVMediaItem * weakMediaItem = mediaItem;
    
    NSDictionary *dic = epgProgram.metaData;
    NSString * seriesValue = [dic objectForKey:@"series ID"];
    NSLog(@"seriesName = %@",seriesValue);
    
    if ([seriesValue length]==0)
    {
        NSLog(@"seriesValue in nil !!!! ");
        [self.delegate recoredAdapter:self complateRecordAssetOrSeriesToToChannel:mediaItem Program:epgProgram recoredAdapterCallName:RecoredAdapterCallNameRecoredItemSeries recoredAdapterStatus:RecoredAdapterStatusFailed];
        return;
    }

    __weak TVPAPIRequest * request = [TVPMediaAPI requestForRecordSeriesByProgramId:epgProgram.epgId withdelegate:nil];
    
    [request setFailedBlock:^{
        
        NSLog(@"%@",request);
        [self.delegate recoredAdapter:self complateRecordAssetOrSeriesToToChannel:mediaItem Program:epgProgram recoredAdapterCallName:RecoredAdapterCallNameRecoredItemSeries recoredAdapterStatus:RecoredAdapterStatusFailed];
        
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@",request);
        NSDictionary *response = [request JSONResponse];
        NSString * status = [response objectForKey:@"status"];
        
        if ([status isEqualToString:@"OK"] || [status isEqualToString:@"AssetAlreadyScheduled"])
        {
//            NSMutableDictionary * recoredsTOChannel = [self.recoredDicByChannels objectForKey:mediaItem.mediaID];
//            
//            RecoredHelper * recHelp = nil;//
//            if ([[recoredsTOChannel allKeys] count]==0)
//            {
//                recoredsTOChannel = [NSMutableDictionary dictionary];
//            }
//            recHelp = [[RecoredHelper alloc] init];
//            recHelp.program = weakEpgProgram;
//            recHelp.mediaItem = weakMediaItem;
//            recHelp.recordItem = nil;
//            if ([status isEqualToString:@"OK"] )
//            {
//                recHelp.recoredId = [response objectForKey:@"recordingID"];
//                recHelp.recoredSereisId = [response objectForKey:@"recordingID"];
//            }
//            else
//            {
//                recHelp.recoredId = [response objectForKey:@"-10"];
//                recHelp.recoredSereisId = [response objectForKey:@"-10"];
//
//            }
//            recHelp.recordingItemStatus = RecoredHelperRecoreded;
//            recHelp.isSeries = YES;
//            
//            if (weakEpgProgram)
//            {
//                [recoredsTOChannel setObject:recHelp forKey:weakEpgProgram.epgId];
//                [self.recoredDicByChannels setObject:recoredsTOChannel forKey:weakMediaItem.mediaID];
//            }
            
          
            NSString *seriesTag = [weakEpgProgram.metaData objectForKey:@"series ID"];
            NSArray *filteredArray = [[self.delegate currentProgramsToChannel:weakMediaItem] filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(APSTVProgram *program, NSDictionary *bindings) {
                return [[program.metaData objectForKey:@"series ID"] isEqualToString:seriesTag];
            }]];
            [self loadRecoredByPrograms:filteredArray andChannel:weakMediaItem isSeries:YES completion:^(BOOL finished) {
                if (finished) {
                    [self.delegate recoredAdapter:self complateRecordAssetOrSeriesToToChannel:mediaItem Program:epgProgram recoredAdapterCallName:RecoredAdapterCallNameRecoredItemSeries recoredAdapterStatus:RecoredAdapterStatusOk];
                }
            }];

        }
        else
        {
            [self.delegate recoredAdapter:self complateRecordAssetOrSeriesToToChannel:mediaItem Program:epgProgram recoredAdapterCallName:RecoredAdapterCallNameRecoredItemSeries recoredAdapterStatus:RecoredAdapterStatusFailed];
        }

    }];
    
    
    [self sendRequest:request];
}



- (void)recordAsset:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    __weak APSTVProgram * program = epgProgram;
    __weak TVMediaItem * channel = mediaItem;
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForRecordAssetWithEPGId:epgProgram.epgId delegate:nil];
    
    [request setFailedBlock:^{
        NSLog(@"%@",request);
           [self.delegate recoredAdapter:self complateRecordAssetOrSeriesToToChannel:mediaItem Program:epgProgram recoredAdapterCallName:RecoredAdapterCallNameRecoredItem recoredAdapterStatus:RecoredAdapterStatusFailed];
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@",request);
        NSDictionary *response = [request JSONResponse];
        NSString * status = [response objectForKey:@"status"];
        
        if ([status isEqualToString:@"OK"] ||  [status isEqualToString:@"AssetAlreadyScheduled"])
        {
            NSMutableDictionary * recoredsTOChannel = [self.recoredDicByChannels objectForKey:mediaItem.mediaID];
            
            RecoredHelper * recHelp = nil;//
            if ([[recoredsTOChannel allKeys] count]==0)
            {
                recoredsTOChannel = [NSMutableDictionary dictionary];
            }
            recHelp = [[RecoredHelper alloc] init];
            recHelp.program = program;
            recHelp.mediaItem = channel;
            recHelp.recordItem = nil;
            if ([status isEqualToString:@"OK"])
            {
                recHelp.recoredId = [response objectForKey:@"recordingID"];
            }
            else
            {
                recHelp.recoredId = @"-10";
            }
            recHelp.recordingItemStatus = RecoredHelperRecoreded;
            
            if (program)
            {
                [recoredsTOChannel setObject:recHelp forKey:program.epgId];
                [self.recoredDicByChannels setObject:recoredsTOChannel forKey:channel.mediaID];
            }
            if ([self.delegate respondsToSelector:@selector(recoredAdapter:complateRecordAssetOrSeriesToToChannel:Program:recoredAdapterCallName:recoredAdapterStatus:)])
            {
                [self.delegate recoredAdapter:self complateRecordAssetOrSeriesToToChannel:mediaItem Program:epgProgram recoredAdapterCallName:RecoredAdapterCallNameRecoredItem recoredAdapterStatus:RecoredAdapterStatusOk];
            }
        }
       
        else
        {
            [self.delegate recoredAdapter:self complateRecordAssetOrSeriesToToChannel:mediaItem Program:epgProgram recoredAdapterCallName:RecoredAdapterCallNameRecoredItem recoredAdapterStatus:RecoredAdapterStatusFailed];
        }

    }];
    
    [self sendRequest:request];
    
    
}

-(RecoredHelper *)recoredHelperToepgProgram:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    NSMutableDictionary * recoredsTOChannel = [self.recoredDicByChannels objectForKey:mediaItem.mediaID];
    RecoredHelper * recHelp = [recoredsTOChannel objectForKey:epgProgram.epgId];
    return recHelp;
}


-(TVPAPIRequest *)loadRecoredByChannel:(TVMediaItem *)epgChannel andPrograms:(NSArray*)currentProgramsToChannel scrollToProgram:(APSTVProgram *) scrolledProgram
{
    
    if (APST.supportRecordings == NO || [LoginM isSignedIn] == NO)
    {
        return nil;
    }
    
    __weak RecoredAdapter * weakSelf = self;
    __weak TVMediaItem * weakChannel = epgChannel;
    
    APSTVProgram * prog = [currentProgramsToChannel firstObject];
    
    NSString * epgChannelId = prog.epgChannelID;
    if ([epgChannelId isEqualToString:@"-1"])
    {
        for (APSTVProgram * temoProg in currentProgramsToChannel)
        {
            NSString * tempEpgChannelId = temoProg.epgChannelID;
            if ([tempEpgChannelId isEqualToString:@"-1"]== NO)
            {
                epgChannelId = tempEpgChannelId;
                break;
            }
        }
    }
    if ([epgChannelId isEqualToString:@"-1"])
    {
        return nil;
    }
    NSMutableArray * programsIdsArray = [NSMutableArray array];
    for (APSTVProgram * prog in currentProgramsToChannel)
    {
        if (prog.epgId)
        {
            [programsIdsArray addObject:prog.epgId];
        }
    }
    if ([currentProgramsToChannel count]==1 && scrolledProgram)
    {
        [programsIdsArray removeAllObjects];
        programsIdsArray = nil;
    }
    __weak TVPAPIRequest * request =  [TVPMediaAPI requestForGetRecordingsWithPageSize:100
                                                                             pageIndex:0
                                                                              searchBy:TVRecordingSearchByTypeOther
                                                                          epgChannelID:epgChannelId
                                                                       recordingStatus:TVRecordingStatusUnknown
                                                                          recordingIDs:nil
                                                                           programsIds:programsIdsArray
                                                                             seriesIds:nil
                                                                             startDate:[NSDate date]
                                                                               orderBy:TVRecordOrderByStartTime
                                                                        orderDirection:TVRecordOrderDirectionAscending
                                                                          withdelegate:nil];
    [request setFailedBlock:^{
        
        NSLog(@"setFailedBlock: %@",request);
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@ \nweakChannel = %@",request,weakChannel);
        
        NSArray *response = [request JSONResponse];
        if (response == nil || [response count] < 1)
        {
            [weakSelf.delegate recoredAdapter:weakSelf complateLoadRecoredItemsToChannel:weakChannel isChannelHeveRecoredItems:NO scrollToProgram:scrolledProgram];
            return ;
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSArray *response = [request JSONResponse];
            
            if (response == nil || [response count] < 1)
            {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                     [weakSelf.delegate recoredAdapter:weakSelf complateLoadRecoredItemsToChannel:weakChannel isChannelHeveRecoredItems:NO scrollToProgram:scrolledProgram];
                    return ;
                });
            }
            
            NSMutableArray * records = [NSMutableArray array];
            for (NSDictionary * dictionary in response)
            {
                TVNPVRRecordItem * item = [[TVNPVRRecordItem alloc] initWithDictionary:dictionary];
                if (item.recordingStatus != TVRecordingStatusCancelled)
                {
                    [records addObject:item];
                }
            }
            
            NSMutableDictionary * recoredsTOChannel = [weakSelf.recoredDicByChannels objectForKey:weakChannel.mediaID];
            
            for (TVNPVRRecordItem * recordItem in records)
            {
                APSTVProgram * program = nil;
//                NSLog(@"recordItem.epgIdentifier = %@",recordItem.epgIdentifer);
                
                for (APSTVProgram * prog in currentProgramsToChannel)
                {
//                    NSLog(@"epgIdentifier   = %@",prog.epgIdentifier);
                    
                    if ([prog.epgIdentifier isEqualToString:recordItem.epgIdentifer])
                    {
                        program = prog;
                        break;
                    }
                }
                RecoredHelper * recHelp = nil;//
                if ([[recoredsTOChannel allKeys] count]==0)
                {
                    recoredsTOChannel = [NSMutableDictionary dictionary];
                }
                recHelp = [[RecoredHelper alloc] init];
                recHelp.program = program;
                recHelp.mediaItem = weakChannel;
                recHelp.recordItem = recordItem;
                recHelp.recoredId = recordItem.recordingID;
                recHelp.recordingItemStatus = RecoredHelperRecoreded;
                
                if ([recordItem.recordSource isEqualToString:seriesSourceKey])
                {
                    recHelp.isSeries = YES;
//                    recHelp.seasonId = [[recordItem.epgTags objectForKey:seasonIdKey]lastObject];
                }
                if (program)
                {
                    [recoredsTOChannel setObject:recHelp forKey:program.epgId];
                    [self.recoredDicByChannels setObject:recoredsTOChannel forKey:weakChannel.mediaID];
                }
                
            }
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                if ([weakSelf.delegate respondsToSelector:@selector(recoredAdapter:complateLoadRecoredItemsToChannel:isChannelHeveRecoredItems:scrollToProgram:)])
                {
                    [weakSelf.delegate recoredAdapter:weakSelf complateLoadRecoredItemsToChannel:weakChannel isChannelHeveRecoredItems:YES scrollToProgram:scrolledProgram];
                }
            });
        });
    }];
    
    [self sendRequest:request];
    
    return request;
}
-(TVPAPIRequest *)loadRecoredByProgram:(APSTVProgram*)program andChannel:(TVMediaItem *)epgChannel completion:(void (^)(BOOL finished))completion {
    return [self loadRecoredByPrograms:[NSArray arrayWithObject:program] andChannel:epgChannel isSeries:NO completion:completion];
}

-(TVPAPIRequest *)loadRecoredByPrograms:(NSArray*)programs andChannel:(TVMediaItem *)epgChannel isSeries:(BOOL)isSeries completion:(void (^)(BOOL finished))completion
{
    
    NSArray *programsIdArray = nil;
    
    if ([[programs firstObject] isKindOfClass:[APSTVProgram class]])
    {
        programsIdArray = [programs valueForKeyPath:@"@distinctUnionOfObjects.epgId"];
    }
    else
    {
        programsIdArray = programs;
    }

    __weak TVMediaItem * weakChannel = epgChannel;

    __weak RecoredAdapter * weakSelf = self;
    
    __weak TVPAPIRequest * request =  [TVPMediaAPI requestForGetRecordingsWithPageSize:100
                                                                             pageIndex:0
                                                                              searchBy:TVRecordingSearchByTypeOther
                                                                          epgChannelID:nil
                                                                       recordingStatus:TVRecordingStatusUnknown
                                                                          recordingIDs:nil
                                                                           programsIds:programsIdArray
                                                                             seriesIds:nil
                                                                             startDate:[NSDate date]
                                                                               orderBy:TVRecordOrderByStartTime
                                                                        orderDirection:TVRecordOrderDirectionAscending
                                                                          withdelegate:nil];
    [request setFailedBlock:^{
        
        NSLog(@"setFailedBlock: %@",request);
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@ \nweakChannel = %@",request,weakChannel);
        
        NSArray *response = [request JSONResponse];
        if (response == nil || [response count] < 1)
        {
            completion (NO);
            return;
        }
        
        NSMutableArray * records = [NSMutableArray array];
        for (NSDictionary * dictionary in response) {
            TVNPVRRecordItem * item = [[TVNPVRRecordItem alloc] initWithDictionary:dictionary];
            if (item.recordingStatus != TVRecordingStatusCancelled)
            {
                [records addObject:item];
            }
        }
        
        for (TVNPVRRecordItem * recordItem in records) {
            APSTVProgram * program = (APSTVProgram*)[[programs filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(APSTVProgram *evaluatedObject, NSDictionary *bindings) {
                return [evaluatedObject.epgIdentifier isEqualToString:recordItem.epgIdentifer];
            }]] firstObject];
            
            NSMutableDictionary * recoredsTOChannel = [weakSelf.recoredDicByChannels objectForKey:weakChannel.mediaID];
            TVNPVRRecordItem * recordItem = [records firstObject];
            RecoredHelper * recHelp = nil;//
            if ([[recoredsTOChannel allKeys] count]==0)
            {
                recoredsTOChannel = [NSMutableDictionary dictionary];
            }
            recHelp = [[RecoredHelper alloc] init];
            recHelp.program = program;
            recHelp.mediaItem = weakChannel;
            recHelp.recordItem = recordItem;
            recHelp.recoredId = recordItem.recordingID;
            recHelp.recordingItemStatus = RecoredHelperRecoreded;
            if (!isSeries) {
                if ([recordItem.recordSource isEqualToString:seriesSourceKey]) {
                    recHelp.isSeries = YES;
                }
            }
            else {
                recHelp.isSeries = isSeries;
            }
            
            if (program)
            {
                [recoredsTOChannel setObject:recHelp forKey:program.epgId];
                [self.recoredDicByChannels setObject:recoredsTOChannel forKey:weakChannel.mediaID];
            }
        }
        completion (YES);
    }];
    
    [self sendRequest:request];
    
    return request;
}

#pragma mark - Utilts -

-(void)cleanAllReoredsDataFromMemory
{
    [self.recoredDicByChannels removeAllObjects];
}

-(NSMutableDictionary*)takeAllChannelRecoreds:(TVMediaItem *)channel
{
    return [self.recoredDicByChannels objectForKey:channel.mediaID];
}

-(RecoredHelper*)takeOrMakeRecoredHelperWithMediaItem:(TVMediaItem*) mediaItem andProgram:(APSTVProgram*)program isSeries:(BOOL)isSeries
{
    NSMutableDictionary * recoredsTOChannel = [self.recoredDicByChannels objectForKey:mediaItem.mediaID];
    RecoredHelper * recHelp = nil;//
    if (!recoredsTOChannel)
    {
        recoredsTOChannel = [NSMutableDictionary dictionary];
        recHelp = [[RecoredHelper alloc] init];
        recHelp.program = program;
        recHelp.mediaItem = mediaItem;
        recHelp.isSeries = isSeries;
        recHelp.recordingItemStatus = RecoredHelperCurrenlyRecordeing;
        [recoredsTOChannel setObject:recHelp forKey:program.epgId];
        [self.recoredDicByChannels setObject:recoredsTOChannel forKey:mediaItem.mediaID];
    }
    else
    {
        RecoredHelper * prog = [recoredsTOChannel objectForKey:program.epgId];
        if (!prog)
        {
            recHelp = [[RecoredHelper alloc] init];
            recHelp.program = program;
            recHelp.mediaItem = mediaItem;
            recHelp.isSeries = isSeries;
            recHelp.recordingItemStatus = RecoredHelperCurrenlyRecordeing;
            [recoredsTOChannel setObject:recHelp forKey:program.epgId];
        }
        else
        {
            return prog;
        }
    }
    return  recHelp;
}

-(NSString*)recredAssetIdToRecording:(TVNPVRRecordItem *) recordItem  Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    NSString * recordID = recordItem.recordingID;
    if ([recordID length]==0)
    {
        RecoredHelper * rec = [self recoredHelperToepgProgram:epgProgram mediaItem:mediaItem];
        recordID = rec.recordItem.recordingID;
        if ([recordID length]==0)
        {
            recordID = rec.recoredId;
        }
    }
    
    return recordID;
}


#pragma mark - delete series -

-(void) makeCancelOrDeleteSeriesRecordingWithCnacelRecoredType:(CnacelRecoredType)cnacelRecoredType WithRecoredHelper:(RecoredHelper*)recoredHelper
{
    if (cnacelRecoredType == CnacelRecoredTypeOnlyOldEpisodes)
    {
        [self cancelSeriesRecordingWithRecoredHelper:recoredHelper isDelateAll:NO];
    }
    else
    {
       // self.numberOfDeleateFunctionComplate=0;
        [self deleteSeriesRecordingWithRecoredHelper:recoredHelper isDelateAll:YES];
      //  [self cancelSeriesRecordingWithRecoredHelper:recoredHelper isDelateAll:YES];
    }
}

- (void)getSeriesRecordingsWithRecoredItem:(RecoredHelper*)recoredHelper AndCnacelRecoredType:(CnacelRecoredType)cnacelRecoredType
{
    __weak RecoredHelper * weakRecoredHelper = recoredHelper;
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetSeriesRecordingsWithPageSize:100 pageIndex:0 orderBy:TVRecordOrderByStartTime orderDirection:TVRecordOrderDirectionAscending withdelegate:nil];
    
    [request setFailedBlock:^{
        
        NSLog(@"%@",request);
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@",request);
        NSArray *response = [request JSONResponse];
        NSMutableArray * seriesRecoredItems = [NSMutableArray array];
        for (NSDictionary * dic in response) {
            TVNPVRSeriesItem * item = [[TVNPVRSeriesItem alloc] initWithDictionary:dic];
            if (item)
            {
                [seriesRecoredItems addObject:item];
            }
        }
        TVNPVRSeriesItem * currSeriesItem = [self findRecoredSeriesitemFrom:seriesRecoredItems ToRecoredItem:weakRecoredHelper.recordItem];
        weakRecoredHelper.recoredSereisId = currSeriesItem.recordID;
        
        [self makeCancelOrDeleteSeriesRecordingWithCnacelRecoredType:cnacelRecoredType WithRecoredHelper:weakRecoredHelper];
    }];
    
    
    [self sendRequest:request];
}

-(TVNPVRSeriesItem *)findRecoredSeriesitemFrom:(NSArray*)seriesRecoredItems ToRecoredItem:(TVNPVRRecordItem *) recordItem
{
    TVNPVRSeriesItem * seriesItem = nil;
    NSString * recordSeriesID = nil;
    
    if ([recordItem.recordSource isEqualToString:seriesSourceKey])
    {
        recordSeriesID = [[recordItem.epgTags objectForKey:seasonIdKey] lastObject];
    }
    
    if ([recordSeriesID length]>0)
    {
        for (TVNPVRSeriesItem * item in seriesRecoredItems)
        {
            if ([item.seriesId isEqual:recordSeriesID])
            {
                seriesItem = item;
                break;
            }
        }
    }
    return seriesItem;
}

- (void)deleteSeriesRecordingWithRecoredHelper:(RecoredHelper*)recoredHelper isDelateAll:(BOOL)isDelateAll
{
    __weak RecoredHelper* weakRecoredHelper = recoredHelper;
    
    __weak TVNPVRRecordItem * weakRecordItem = recoredHelper.recordItem;
    __weak APSTVProgram * weakEpgProgram = recoredHelper.program;
    __weak TVMediaItem * weakMediaItem = recoredHelper.mediaItem;
    
    NSString * recordID = recoredHelper.recoredSereisId;
    
    if (recordID != nil && [recordID length]>0)
    {
        __weak TVPAPIRequest * request = [TVPMediaAPI requestForDeleteSeriesRecordingWithSeriesRecordingId:recordID withdelegate:nil];
        
        [request setFailedBlock:^{
            
            NSLog(@"%@",request);
            if (isDelateAll)
            {
                [self complateCancelAndDeleateSeriesRecordingWithRecoredHelper:weakRecoredHelper recoredAdapterCallName:RecoredAdapterCallNameCancelSeries recoredAdapterStatus:RecoredAdapterStatusFailed];
            }
            else
            {
                [self.delegate recoredAdapter:self complateCancelOrDeleateAssetRecording:weakRecordItem Program:weakEpgProgram mediaItem:weakMediaItem recoredAdapterCallName:RecoredAdapterCallNameDeleateSeries recoredAdapterStatus:RecoredAdapterStatusFailed];
            }
            
        }];
        
        [request setCompletionBlock:^{
            
            //NSLog(@"%@",request);
            
            if (isDelateAll)
            {
                [self complateCancelAndDeleateSeriesRecordingWithRecoredHelper:weakRecoredHelper recoredAdapterCallName:RecoredAdapterCallNameCancelSeries recoredAdapterStatus:RecoredAdapterStatusOk];
            }
            else
            {
                [self.delegate recoredAdapter:self complateCancelOrDeleateAssetRecording:weakRecordItem Program:weakEpgProgram mediaItem:weakMediaItem recoredAdapterCallName:RecoredAdapterCallNameDeleateSeries recoredAdapterStatus:RecoredAdapterStatusOk];
            }
            
        }];
        
        [self sendRequest:request];
    }
    else
    {
        if (isDelateAll)
        {
            [self complateCancelAndDeleateSeriesRecordingWithRecoredHelper:weakRecoredHelper recoredAdapterCallName:RecoredAdapterCallNameCancelSeries recoredAdapterStatus:RecoredAdapterStatusOk];
        }
        else
        {
            [self.delegate recoredAdapter:self complateCancelOrDeleateAssetRecording:weakRecordItem Program:weakEpgProgram mediaItem:weakMediaItem recoredAdapterCallName:RecoredAdapterCallNameDeleateSeries recoredAdapterStatus:RecoredAdapterStatusOk];
        }
    }
}

- (void)cancelSeriesRecordingWithRecoredHelper:(RecoredHelper*)recoredHelper isDelateAll:(BOOL)isDelateAll
{
    __weak RecoredHelper* weakRecoredHelper = recoredHelper;
    __weak TVNPVRRecordItem * weakRecordItem = recoredHelper.recordItem;
    __weak APSTVProgram * weakEpgProgram = recoredHelper.program;
    __weak TVMediaItem * weakMediaItem = recoredHelper.mediaItem;
    
    NSString * recordID = recoredHelper.recoredSereisId;
    
    if (recordID != nil && [recordID length]>0)
    {
        __weak TVPAPIRequest * request = [TVPMediaAPI requestForCancelSeriesRecordingWithSeriesRecordingId:recordID withdelegate:nil];
        
        [request setFailedBlock:^{
            
            NSLog(@"%@",request);
            if (isDelateAll)
            {
                [self complateCancelAndDeleateSeriesRecordingWithRecoredHelper:weakRecoredHelper recoredAdapterCallName:RecoredAdapterCallNameCancelSeries recoredAdapterStatus:RecoredAdapterStatusOk];
            }
            else
            {
                [self.delegate recoredAdapter:self complateCancelOrDeleateAssetRecording:weakRecordItem Program:weakEpgProgram mediaItem:weakMediaItem recoredAdapterCallName:RecoredAdapterCallNameCancelSeries recoredAdapterStatus:RecoredAdapterStatusOk];
            }
        }];
        
        [request setCompletionBlock:^{
            
            //NSLog(@"%@",request);
            if (isDelateAll)
            {
                [self complateCancelAndDeleateSeriesRecordingWithRecoredHelper:weakRecoredHelper recoredAdapterCallName:RecoredAdapterCallNameCancelSeries recoredAdapterStatus:RecoredAdapterStatusOk];
            }
            else
            {
                [self complateCancelSeriesRecordingWithRecoredHelper:weakRecoredHelper recoredAdapterCallName:RecoredAdapterCallNameCancelSeries recoredAdapterStatus:RecoredAdapterStatusOk];
            }
            
            
        }];
        
        [self sendRequest:request];
    }
    else
    {
        if (isDelateAll)
        {
            [self complateCancelAndDeleateSeriesRecordingWithRecoredHelper:weakRecoredHelper recoredAdapterCallName:RecoredAdapterCallNameCancelSeries recoredAdapterStatus:RecoredAdapterStatusOk];
        }
        else
        {
            [self.delegate recoredAdapter:self complateCancelOrDeleateAssetRecording:weakRecordItem Program:weakEpgProgram mediaItem:weakMediaItem recoredAdapterCallName:RecoredAdapterCallNameCancelSeries recoredAdapterStatus:RecoredAdapterStatusOk];
        }
    }
}

-(void)complateCancelAndDeleateSeriesRecordingWithRecoredHelper:(RecoredHelper*)recoredHelper recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus;
{
   // self.numberOfDeleateFunctionComplate ++;
   // if (self.numberOfDeleateFunctionComplate ==2)
   // {
//        self.numberOfDeleateFunctionComplate =0;
        [self.recoredDicByChannels removeObjectForKey:recoredHelper.mediaItem.mediaID];
        NSArray * epgArray = [NSArray arrayWithObject:recoredHelper.program];
        [self loadRecoredByChannel:recoredHelper.mediaItem andPrograms:epgArray scrollToProgram:recoredHelper.program];
        if ([self.delegate respondsToSelector:@selector(recoredAdapter:complateCancelOrDeleateSeriesRecording:recoredAdapterStatus:)])
        {
            [self.delegate recoredAdapter:self complateCancelOrDeleateSeriesRecording:recoredHelper recoredAdapterStatus:recoredAdapterStatus];
        }
   // }
}

-(void)complateCancelSeriesRecordingWithRecoredHelper:(RecoredHelper*)recoredHelper recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus;
{
   [self.recoredDicByChannels removeObjectForKey:recoredHelper.mediaItem.mediaID];
    NSArray * epgArray = [NSArray arrayWithObject:recoredHelper.program];
    [self loadRecoredByChannel:recoredHelper.mediaItem andPrograms:epgArray scrollToProgram:recoredHelper.program];
    if ([self.delegate respondsToSelector:@selector(recoredAdapter:complateCancelOrDeleateSeriesRecording:recoredAdapterStatus:)])
    {
        [self.delegate recoredAdapter:self complateCancelOrDeleateSeriesRecording:recoredHelper recoredAdapterStatus:recoredAdapterStatus];
    }
}

@end
