//
//  EPGManager.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 03.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"
#import "TVMediaItem+PSTags.h"

@interface EPGManager : NSObject

+ (EPGManager *)instance;

- (void)sortChannelsList:(NSArray *)array;
- (void)loadEPGChannels:(BaseViewController *)delegate completion:(void(^)(NSMutableArray *result))completion;
- (void)updateEPGList:(NSMutableArray *)array delegate:(BaseViewController *)delegate completion:(void(^)(NSMutableArray *result))completion;
- (void)updateEPGListWithFavorite:(TVMediaItem *)mediaItem;
- (void)updateEPGListWithFavorite:(TVMediaItem *)mediaItem value:(BOOL)value;
- (NSMutableArray *)addEmptyProgramsForEmptySpacesInProgramsArray:(NSMutableArray *)programsArray forDate:(NSDate *)currentDate privateContext:(NSManagedObjectContext *)privateContext;

@property (nonatomic, strong) NSMutableArray *channelsList;


/**
 *  refresh Favorites status
 */
-(void) refreshUserRelatedActionsWithCompletion:(void(^)(NSMutableArray *result))completion delegate:(BaseViewController *)delegate;


@end
