//
//  VFTypography.h
//  KalturaUnified
//
//  Created by Synergetica LLC on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VFTypography: APSTypography

+ (VFTypography *)instance;


@property (nonatomic, strong) UIColor *darkGrayColor;
@property (nonatomic, strong) UIColor *grayScale1Color;
@property (nonatomic, strong) UIColor *grayScale2Color;
@property (nonatomic, strong) UIColor *grayScale3Color;
@property (nonatomic, strong) UIColor *grayScale4Color;
@property (nonatomic, strong) UIColor *grayScale5Color;
@property (nonatomic, strong) UIColor *grayScale6Color;
@property (nonatomic, strong) UIColor *grayScale7Color;
@property (nonatomic, strong) UIColor *grayScale8Color;

- (void) modifyBrandButtonToGray:(UIButton*) button;

@end
