//
//  DeviceManagement.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseManagement.h"

@interface DeviceManagement : BaseManagement

- (void)requestForGetDeviceDomains:(void(^)(id jsonResponse))completionBlock;
- (void)requestForAddDeviceToDomainWithDeviceName:(NSString *)deviceName deviceBrandID:(NSInteger)deviceBrandID completionBlock:(void(^)(id jsonResponse))completionBlock;
- (void)requestForSubmitAddDeviceToDomainRequestWithDeviceName:(NSString *)deviceName deviceBrandID:(NSInteger)deviceBrandID completionBlock:(void(^)(id jsonResponse))completionBlock;
- (void)requestForChangeDeviceDomainStatus:(BOOL)isActive completionBlock:(void(^)(id jsonResponse))completionBlock;

- (void)checkDeviceDomain:(void(^)())completion;

@end
