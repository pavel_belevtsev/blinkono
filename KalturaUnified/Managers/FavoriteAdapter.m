//
//  FavoriteAdapter.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 17.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "FavoriteAdapter.h"

@interface FavoriteAdapter ()

@property (strong, nonatomic) TVMediaItem * mediaItem;

@end

@implementation FavoriteAdapter

#define kFavoriteAdapterChangeObserver @"FavoriteAdapter_UpdateMediaItemFavoriteStatus"


- (void)setMediaItem:(TVMediaItem *)mediaItem {
    if (_mediaItem==mediaItem)
        return;
    if (_mediaItem)
        [self unregisterObserversForMedia:_mediaItem];
    _mediaItem = mediaItem;
    if (_mediaItem)
        [self registerObserversForMedia:_mediaItem];
}

- (void)dealloc {
    self.mediaItem = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - KVO

- (void) registerObserversForMedia:(TVMediaItem*)item {
    [item addObserver:self forKeyPath:@"isFavorite" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:nil];
}

- (void) unregisterObserversForMedia:(TVMediaItem*)item {
    [item removeObserver:self forKeyPath:@"isFavorite"];
}

- (id)initWithMediaItem:(TVMediaItem *) mediaItem control:(UIButton *) button {
    return [self initWithMediaItem:mediaItem control:button label:nil];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([object isKindOfClass:[TVMediaItem class]]) {
        TVMediaItem *item = object;
        if ([keyPath isEqualToString:@"isFavorite"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kFavoriteAdapterChangeObserver object:nil userInfo:@{@"id": item.mediaID, @"isFavorite": @(item.isFavorite)}];
        }
    }
}

#pragma mark - View functions
- (id)initWithMediaItem:(TVMediaItem *)mediaItem control:(UIButton *) button label:(UILabel *) label {
    self = [super init];
    if (self) {

        self.mediaItem = mediaItem;
        self.buttonFavorite = button;
        self.labelFavorite = label;
        
        __block FavoriteAdapter *sself = self;
        [self.buttonFavorite setTouchUpInsideAction:^(UIButton *sender) {
            if (!sender.selected){
                [sself sendFavoriteAction];
            }
            else {
                [sself sendUnFavoriteAction];
            }
        }];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kFavoriteAdapterChangeObserver object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFavoriteStatusFromPost:) name:kFavoriteAdapterChangeObserver object:nil];

        [self refresh];
    }
    return self;
}

- (void)setupMediaItem:(TVMediaItem *) mediaItem
{
    self.mediaItem = mediaItem;
    BOOL isSignIn = [LoginM isSignedIn];
    if (isSignIn) [self isFavoriteMedia];
}

- (void) updateFavoriteStatusFromPost: (NSNotification*) notification {
    NSDictionary *dict = [notification userInfo];
    if ([self.mediaItem.mediaID isEqualToString:dict[@"id"]])
        [self updateFavoriteStatus:[dict[@"isFavorite"] boolValue]];
}

- (void)updateFavoriteStatus:(BOOL)value {
    
    NSLog(@"=== updateFavoriteStatus %@   %@", value ? @"YES" : @"NO", self.mediaItem.name);
    
    self.buttonFavorite.selected = value;
    self.labelFavorite.highlighted = value;
    
    if (_mediaItem.isLive) {
        _labelFavorite.text = (value ? LS(@"bookmarked") : LS(@"bookmark"));
        
        [EPGM updateEPGListWithFavorite:_mediaItem value:value];
        
    } else {
        _labelFavorite.text = LS(@"favorite");
    }

    if ([self.delegate respondsToSelector:@selector(favoriteAdapter:comleteUpdateFavoriteStatus:)])
        [self.delegate favoriteAdapter:self comleteUpdateFavoriteStatus:value];
}

-(void) sendUnFavoriteAction
{
    self.buttonFavorite.userInteractionEnabled = NO;
    self.mediaItem.isFavorite = NO;
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForActionDone:TVMediaActionRemoveFromFavorites mediaID:[_mediaItem.mediaID integerValue] mediaType:_mediaItem.mediaTypeID extraValue:0 delegate:nil];
    
    [request setFailedBlock:^{
         NSLog(@"ERROR: %@",request.responseString);
         self.buttonFavorite.userInteractionEnabled = YES;
         if (!self.mediaItem.isFavorite)
             self.mediaItem.isFavorite = YES;
     }];
    
    [request setCompletionBlock:^{
         NSLog(@"%@",request.responseString);
         
         self.buttonFavorite.userInteractionEnabled = YES;
         if (self.mediaItem.isFavorite)
             self.mediaItem.isFavorite = NO;
     }];
    
    [self sendRequest:request];
}

- (void)sendFavoriteAction {
    self.buttonFavorite.userInteractionEnabled = NO;
    self.mediaItem.isFavorite = YES;
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForActionDone:TVMediaActionAddToFavorites mediaID:[_mediaItem.mediaID integerValue] mediaType:_mediaItem.mediaTypeID extraValue:0 delegate:nil];
    
    [request setFailedBlock:^{
         NSLog(@"ERROR: %@",request.responseString);
         self.buttonFavorite.userInteractionEnabled = YES;
         if (self.mediaItem.isFavorite)
             self.mediaItem.isFavorite = NO;
         
     }];
    
    [request setCompletionBlock:^{
         NSLog(@"%@",request.responseString);
         
         self.buttonFavorite.userInteractionEnabled = YES;
         if (!self.mediaItem.isFavorite)
             self.mediaItem.isFavorite = YES;
     }];
    
    [self sendRequest:request];
}

- (void)isFavoriteMedia {
    
    if (_mediaItem && _mediaItem.mediaID!=nil) {
        
        self.buttonFavorite.userInteractionEnabled = NO;
        
        __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetAreMediasFavorites:[NSArray arrayWithObject:_mediaItem.mediaID] delegate:nil];
        
        [request setCompletionBlock:^{
            
            NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
            
            self.buttonFavorite.userInteractionEnabled = YES;
            
            self.buttonFavorite.selected = NO;
            self.labelFavorite.highlighted = NO;
            
            for (NSDictionary *favItem in result) {
                if ([[favItem objectOrNilForKey:@"Value"] intValue] && [favItem objectOrNilForKey:@"Key"]) {
                    if ([[favItem objectOrNilForKey:@"Key"] integerValue] == [_mediaItem.mediaID integerValue]) {
                            self.mediaItem.isFavorite = YES;
                        break;
                    }
                }
            }
        }];
        
        [request setFailedBlock:^{
            self.buttonFavorite.userInteractionEnabled = YES;
        }];
        
        [self sendRequest:request];
    }
}

- (void)refresh {
    [self isFavoriteMedia];
}

@end
