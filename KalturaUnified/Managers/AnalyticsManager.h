//
//  AnalyticsManager.h
//  KalturaUnified
//
//  Created by Admin on 23.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AnalyticsEvent.h"
#import "GAI.h"
#import "TVMediaItem+PSTags.h"

@interface AnalyticsManager : NSObject

@property (nonatomic, strong, readonly) AnalyticsEvent *currentEvent;
@property (nonatomic, strong) id<GAITracker> googleTracker;

+ (AnalyticsManager *)instance;

- (void)setup;
- (void)trackCurrentEvent;
- (void)trackScreen:(NSString *)screenName;
- (void)trackScreenCategory:(TVMenuItem *)category withFilters:(NSArray *)filterList;
- (void)trackScreenSelectedItem:(NSString *)categoryName item:(NSString *)itemName;
- (void)trackEpisodeScreen:(TVMediaItem *)series episode:(TVMediaItem *)episode;

@end
