//
//  AnalyticsManager.m
//  KalturaUnified
//
//  Created by Admin on 23.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "AnalyticsManager.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@interface AnalyticsManager ()

@property (nonatomic, strong, readwrite) AnalyticsEvent *currentEvent;

@end

@implementation AnalyticsManager

+ (AnalyticsManager *)instance {
    static dispatch_once_t p = 0;
    __strong static AnalyticsManager *sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
        sharedObject.currentEvent = [[AnalyticsEvent alloc] init];
    });
    return sharedObject;
}

- (void)setup {
    [GAI sharedInstance].trackUncaughtExceptions = NO;
    [GAI sharedInstance].dispatchInterval = 20;
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
    [[GAI sharedInstance] trackerWithTrackingId:APST.googleAnalyticsId];
    
    self.googleTracker = [[GAI sharedInstance] defaultTracker];
}

- (void)trackCurrentEvent {
    if ([self.currentEvent.category length] &&
        [self.currentEvent.action length]) {
        NSString *label = ([self.currentEvent.label length]) ? self.currentEvent.label : @"";
        GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createEventWithCategory:self.currentEvent.category
                                                                               action:self.currentEvent.action
                                                                                label:label
                                                                                value:self.currentEvent.value];
        [self.googleTracker send:[builder build]];
        [self.currentEvent clearFields];
    }
}

- (void)trackScreen:(NSString *)screenName {
    [self.googleTracker set:kGAIScreenName
                      value:screenName];
    [self.googleTracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)trackScreenCategory:(TVMenuItem *)category withFilters:(NSArray *)filterList {
    NSMutableArray *categoryNames = [NSMutableArray arrayWithCapacity:[filterList count] + 1];
    [categoryNames addObject:category.name];
    for (TVMenuItem *filter in filterList) {
        [categoryNames addObject:filter.name];
    }
    NSString *screenTitle = [categoryNames componentsJoinedByString:@"->"];
    [self trackScreen:screenTitle];
}

- (void)trackScreenSelectedItem:(NSString *)categoryName item:(NSString *)itemName {
    NSString *screenTitle = [NSString stringWithFormat:@"%@ page->%@", categoryName, itemName];
    [self trackScreen:screenTitle];
    
}

- (void)trackEpisodeScreen:(TVMediaItem *)series episode:(TVMediaItem *)episode {
    NSString *screenTitle = [NSString stringWithFormat:@"Episode page:%@S%@E%@",
                             series.name,
                             [episode getMediaItemSeasonNumber],
                             [episode getMediaItemEpisodeNumber]];
    
    [self trackScreen:screenTitle];
}

@end
