//
//  PurchasesManager.m
//  KalturaUnified
//
//  Created by Alex Zchut on 4/21/15.
//  Copyright (c) 2015 Kaltura. All rights reserved
//

#import "PurchasesManager.h"

@interface PurchasesManager ()


@end

@implementation PurchasesManager

#pragma mark - Public Function

+ (PurchasesManager *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (void) getPurchasesForPersonal:(BOOL)isPersonal delegate:(BaseViewController *)delegateController completion:(void (^)(NSArray* arrRentals, NSError* error)) completion {
    __block NSMutableDictionary* dictMediaItemsIds = [NSMutableDictionary dictionary];
    __block NSMutableArray* arrMediaItems = [NSMutableArray array];
    __block NSMutableArray* arrMediaItemsFromRecurringSubscriptions = nil;

    dispatch_group_t group = dispatch_group_create();

    MyZoneViewControllerSubTypes subType = kMyZoneViewControllerSubTypeShowAll;
    PurchasesManagerLoadItems typeLoadItems = kPurchasesManagerLoadItemsObjects;
    if ([delegateController isKindOfClass:[MyZoneInnerViewController class]]) {
        MyZoneInnerViewController *controller = (MyZoneInnerViewController*)delegateController;
        subType = controller.subType;
        typeLoadItems = controller.purchasesLoadType;
    }
    
    BOOL shouldContinueLoadData = TRUE;
    BOOL shouldGetWatchlist = !(subType == kMyZoneViewControllerSubTypeShowAllExceptWatchList
                               || subType == kMyZoneViewControllerSubTypeShowAllPermittedItems);
    if (APST.hasWatchList && shouldGetWatchlist) {
        dispatch_group_enter(group);
        [self getMediaItemsIdsForListItemType:TVListItemType_ALL andlistType:TVListType_Watch delegate:delegateController completion:^(NSArray *arrListMediaIds, NSError *error) {
            if (arrListMediaIds)
                [dictMediaItemsIds setObject:arrListMediaIds forKey:@(kMediaItemBelongsToListTypeWatchList)];
            dispatch_group_leave(group);
        }];
        
        if (subType == kMyZoneViewControllerSubTypeWatchListOnly) {
            //show only data from watch list
            shouldContinueLoadData = FALSE;
        }
    }
    
    if (shouldContinueLoadData) {
        dispatch_group_enter(group);
        if (isPersonal) {
            [self getUserRentals:delegateController completion:^(NSArray *arrRentalMediaIds, NSError *error) {
                if (arrRentalMediaIds)
                    [dictMediaItemsIds setObject:arrRentalMediaIds forKey:@(kMediaItemBelongsToListTypeRentals)];
                dispatch_group_leave(group);
            }];
        }
        else {
            [self getDomainRental:delegateController completion:^(NSArray *arrRentalMediaIds, NSError *error) {
                if (arrRentalMediaIds)
                    [dictMediaItemsIds setObject:arrRentalMediaIds forKey:@(kMediaItemBelongsToListTypeRentals)];
                dispatch_group_leave(group);
            }];
        }
    
        if (APST.hasInAppPurchases) {
            dispatch_group_enter(group);
            [SubscriptionM getMediasForPermittedSubscriptions:delegateController isForUser:isPersonal renewal:NO completion:^(NSMutableArray *arrMedias) {
                if (arrMedias.count) {
                    for (TVMediaItem *item in arrMedias)
                        item.listType = kMediaItemBelongsToListTypePermittedSubscriptions;
                    
                    //remove items from arrMediaItems that exist in list arrMedias
                    NSString *mediaIds = [[arrMedias valueForKeyPath:@"@distinctUnionOfObjects.mediaID"] componentsJoinedByString:@","];
                    [arrMediaItems removeObjectsInArray:[arrMediaItems filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVMediaItem *item, NSDictionary *bindings) {
                        return ([mediaIds rangeOfString:item.mediaID].location != NSNotFound);
                    }]]];
                    
                    NSMutableSet *addedItemsSet  = [NSMutableSet set];
                    for (TVMediaItem *item in arrMedias) {
                        if (![addedItemsSet containsObject:item.description]) {
                            [addedItemsSet addObject:item.description];
                            [arrMediaItems addObject:item];
                        }
                    }

                }
                dispatch_group_leave(group);
            }];
            
            //recurring subscriptions items - to remove from display if exists in non-renewing expired subscriptions
            dispatch_group_enter(group);
            [SubscriptionM getMediasForPermittedSubscriptions:delegateController isForUser:isPersonal renewal:YES completion:^(NSMutableArray *arrMedias) {
                if (arrMedias.count) {
                    arrMediaItemsFromRecurringSubscriptions = [NSMutableArray array];
                    NSMutableSet *addedItemsSet  = [NSMutableSet set];
                    for (TVMediaItem *item in arrMedias) {
                        if (![addedItemsSet containsObject:item.description]) {
                            [addedItemsSet addObject:item.description];
                            [arrMediaItemsFromRecurringSubscriptions addObject:item];
                        }
                    }
                }
                dispatch_group_leave(group);
            }];
            
            if (subType != kMyZoneViewControllerSubTypeShowAllPermittedItems) {
                dispatch_group_enter(group);
                [SubscriptionM getMediasForExpiredSubscriptions:delegateController isForUser:isPersonal renewal:NO completion:^(NSMutableArray *arrMedias) {
                    if (arrMedias.count) {
                        for (TVMediaItem *item in arrMedias)
                            item.listType = kMediaItemBelongsToListTypeExpiredSubscriptions;
                        
                        //remove items from arrMedias that already exist in list arrMediaItems
                        NSString *mediaIds = [[arrMediaItems valueForKeyPath:@"@distinctUnionOfObjects.mediaID"] componentsJoinedByString:@","];
                        [arrMedias removeObjectsInArray:[arrMedias filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVMediaItem *item, NSDictionary *bindings) {
                            return ([mediaIds rangeOfString:item.mediaID].location != NSNotFound);
                        }]]];
                        
                        NSMutableSet *addedItemsSet  = [NSMutableSet set];
                        for (TVMediaItem *item in arrMedias) {
                            if (![addedItemsSet containsObject:item.description]) {
                                [addedItemsSet addObject:item.description];
                                [arrMediaItems addObject:item];
                            }
                        }
                        
                    }
                    dispatch_group_leave(group);
                }];
            }
        }
    }
    
    dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSMutableArray *arrMediaItemsIds = [NSMutableArray array];
        NSMutableArray *arrMediasFromWatchList = dictMediaItemsIds[@(kMediaItemBelongsToListTypeWatchList)];
        NSMutableArray *arrMediasFromRentals = dictMediaItemsIds[@(kMediaItemBelongsToListTypeRentals)];

        //remove rentals items
        [arrMediasFromWatchList removeObjectsInArray:arrMediasFromRentals];
        
        //remove items from arrMediaItems that exist in rcurring sibscriptions
        if (arrMediaItemsFromRecurringSubscriptions && arrMediaItemsFromRecurringSubscriptions.count) {
            NSString *mediaIds = [[arrMediaItemsFromRecurringSubscriptions valueForKeyPath:@"@distinctUnionOfObjects.mediaID"] componentsJoinedByString:@","];
            [arrMediaItems removeObjectsInArray:[arrMediaItems filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVMediaItem *item, NSDictionary *bindings) {
                return ([mediaIds rangeOfString:item.mediaID].location != NSNotFound);
            }]]];
        }
        

        [arrMediaItemsIds addObjectsFromArray:arrMediasFromRentals];
        [arrMediaItemsIds addObjectsFromArray: arrMediasFromWatchList];

        //remove subscription items
        [arrMediasFromWatchList removeObjectsInArray:[arrMediaItems valueForKeyPath:@"@distinctUnionOfObjects.mediaID"]];

        switch (typeLoadItems) {
            case kPurchasesManagerLoadItemsObjects: {
                [self getMediaItemsFromMediaIdsArr:arrMediaItemsIds delegate:delegateController completion:^(NSArray *arrMedias, NSError *error)
                 {
                     if (arrMedias.count) {
                         for (TVMediaItem *item in arrMedias) {
                             NSPredicate *valuePredicate=[NSPredicate predicateWithFormat:@"self.integerValue == %ld",[item.mediaID integerValue]];
                             
                             if ([[arrMediasFromRentals filteredArrayUsingPredicate:valuePredicate] count]) {
                                 item.listType = kMediaItemBelongsToListTypeRentals;
                             }
                             if ([[arrMediasFromWatchList filteredArrayUsingPredicate:valuePredicate] count]) {
                                 item.listType = kMediaItemBelongsToListTypeWatchList;
                             }
                         }
                         [arrMediaItems addObjectsFromArray:arrMedias];
                     }
                     dispatch_async(dispatch_get_main_queue(), ^{
                         completion(arrMediaItems, error);
                     });
                 }];
            }
                break;
            case kPurchasesManagerLoadItemsIDsOnly: {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(arrMediaItemsIds, nil);
                });
            }
                break;
            default:
                break;
        }
    });
}

- (void) isMediaInWatchList:(NSInteger)mediaId delegate:(BaseViewController *)delegateController completion:(void (^)(BOOL success, NSError* error)) completion {
    __weak TVPAPIRequest * request = [TVPUsersAPI requestForIsItemExistsInListWithItemType:TVListItemType_Media listType:TVListType_Watch arrayofIds:@[@(mediaId)] shouldAutoOrder:YES delegate:nil];
    
    [request setFailedBlock:^{
        NSString * resposeString = request.responseString;
        NSLog(@"%@",resposeString);
        NSError* err = [NSError errorWithDomain:@"IsItemExistsInListWithItemType" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request, @"IsItemExistsInListWithItemType", nil]];
        completion(NO, err);
        
    }];
    
    [request setCompletionBlock:^{
        
        BOOL result = false;
        
        NSArray *JSONResponse = [request JSONResponse];
        
        if (JSONResponse && [JSONResponse count] == 1) {
            NSDictionary *item = [JSONResponse objectAtIndex:0];
            
            if ([item objectForKey:@"value"]) {
                result = [[item objectForKey:@"value"] isEqualToString:@"true"];
            }
        }
        completion(result, nil);
    }];
    
    [delegateController sendRequest:request];
}

- (void) addMediaToWatchList:(NSInteger)mediaId delegate:(BaseViewController *)delegateController completion:(void(^)(BOOL success, NSError* error)) completion {
    __weak TVPAPIRequest * request = [TVPUsersAPI requestForAddItemToListWithItemType:TVListItemType_Media listType:TVListType_Watch arrayofIds:@[@(mediaId)] shouldAutoOrder:YES delegate:nil];
    
    [request setFailedBlock:^{
        
        NSString * resposeString = request.responseString;
        NSLog(@"%@",resposeString);
        NSError* err = [NSError errorWithDomain:@"AddItemToListWithItemType" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request, @"AddItemToListWithItemType", nil]];
        completion(NO, err);
        
    }];
    
    [request setCompletionBlock:^{
        completion([[request JSONResponse] boolValue], nil);
    }];
    
    [delegateController sendRequest:request];
}

- (void) removeMediaFromWatchList:(NSInteger)mediaId delegate:(BaseViewController *)delegateController completion:(void(^)(BOOL success, NSError* error)) completion {
    __weak TVPAPIRequest * request = [TVPUsersAPI requestForRemoveItemFromListWithItemType:TVListItemType_Media listType:TVListType_Watch arrayofIds:[NSArray arrayWithObjects:@(mediaId), nil] shouldAutoOrder:YES delegate:nil];
    
    [request setFailedBlock:^{
        
        NSString * resposeString = request.responseString;
        NSLog(@"%@",resposeString);
        NSError* err = [NSError errorWithDomain:@"RemoveItemFromListWithItemType" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request, @"RemoveItemFromListWithItemType", nil]];
        completion(NO, err);
        
    }];
    
    [request setCompletionBlock:^{
        completion([[request JSONResponse] boolValue], nil);
    }];
    
    [delegateController sendRequest:request];
}

#pragma  mark - Network Function Assistances

- (void) getMediaItemsFromMediaIdsArr:(NSArray *)arrMediaIds delegate:(BaseViewController *)delegateController completion:(void (^)(NSArray* arrMedias,NSError* error)) completion {
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetMediasInfoWithMediaIDs:arrMediaIds
                                                                          pictureSize:(isPhone)?[TVPictureSize iPhoneItemCellPictureSize]:[TVPictureSize iPadItemCellPictureSize]
                                                                   includeDynamicInfo:NO
                                                                             delegate:nil];
    [request setCompletionBlock:^{
        
        NSArray * mediaItemsRAW = [request JSONResponse];
        NSMutableArray * arrMedias = [NSMutableArray array];
        for (NSDictionary* mediaItemDic in mediaItemsRAW)
        {
            TVMediaItem* mediaItem = [[TVMediaItem alloc] initWithDictionary:mediaItemDic];
            [arrMedias addObject:mediaItem];
        }
        completion(arrMedias, NULL);
        
    }];
    
    [request setFailedBlock:^{
        
        NSString * resposeString = request.responseString;
        NSLog(@"%@",resposeString);
        NSError* err = [NSError errorWithDomain:@"GetMediasErrorDomain" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request, @"getMediasRequest", nil]];
        completion(nil, err);
    }];
    [delegateController sendRequest:request];
}

- (void) getDomainRental:(BaseViewController *)delegateController completion:(void (^)(NSArray* arrRentalMediaIds, NSError* error)) completion {
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetDomainPermittedItems];
    [request setCompletionBlock:^{
        
        NSArray * rentalsArray = [request JSONResponse];
        NSArray * rentalsMediaIds = [self buildRentalMediasArray:rentalsArray];
        completion(rentalsMediaIds, NULL);
    }];
    
    [request setFailedBlock:^{
        
        NSString * resposeString = request.responseString;
        NSLog(@"%@",resposeString);
        NSError* err = [NSError errorWithDomain:@"OnlinePurchasesErrorDomain" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request, @"Request", nil]];
        completion(nil, err);
    }];
    
    [delegateController sendRequest:request];
}

- (void) getUserRentals:(BaseViewController *)delegateController completion: (void (^)(NSArray* arrRentalMediaIds, NSError* error)) completion {
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetUserPermittedItems];
    
    [request setCompletionBlock:^{
        
        NSArray * rentalsArray = [request JSONResponse];
        NSArray * rentalsMediaIds = [self buildRentalMediasArray:rentalsArray];
        completion(rentalsMediaIds, NULL);
    }];
    
    [request setFailedBlock:^{
        
        NSString * resposeString = request.responseString;
        NSLog(@"%@",resposeString);
        NSError* err = [NSError errorWithDomain:@"OnlinePurchasesErrorDomain" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request, @"Request", nil]];
        completion(nil, err);
    }];
    
    [delegateController sendRequest:request];
}

- (void) getMediaFileRentInfo:(NSInteger) fileID delegate:(BaseViewController *)delegateController completion: (void (^)(TVRental* mediaRentalDetails)) completion {
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetUserPermittedItems];
    
    [request setCompletionBlock:^{
        TVRental *rentalItem = nil;
        for (NSDictionary * dict in [request JSONResponse])
        {
            TVRental *info = [[TVRental alloc] initWithDictionary:dict];
            if (info.mediaFileID == fileID) {
                rentalItem = info;
                break;
            }
        }
        completion(rentalItem);
    }];
    
    [request setFailedBlock:^{
        completion(nil);
    }];
    
    [delegateController sendRequest:request];
}

- (void) getMediaItemsIdsForListItemType:(TVListItemType)tvListItemType andlistType:(TVListType)listType delegate:(BaseViewController *)delegateController completion:(void (^)(NSArray *arrListMediaIds, NSError* error)) completion {
    __weak TVPAPIRequest * request = [TVPUsersAPI requestForGetItemsFromListWithItemType:tvListItemType listType:listType arrayofIds:nil shouldAutoOrder:YES delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSArray * watchesArray = [request JSONResponse];
        NSMutableArray * watchListArr = [NSMutableArray array];
        
        for (NSDictionary* watchDictRAW in watchesArray)
        {
            TVWatchListItem* watchListItem = [[TVWatchListItem alloc] initWithDictionary:watchDictRAW];
            [watchListArr addObject:watchListItem];
        }
        
        NSArray *watchListMediaIds = [self buildWatchListMediasArray:watchListArr];
        completion(watchListMediaIds, NULL);
    }];
    
    [request setFailedBlock:^{
        NSString * resposeString = request.responseString;
        NSLog(@"%@",resposeString);
        NSError* err = [NSError errorWithDomain:@"WatchesListErrorDomain" code:1 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed To send request",NSLocalizedDescriptionKey, request, @"Request", nil]];
        completion(nil, err);
    }];
    
    [delegateController sendRequest:request];
}


#pragma mark - Function Assistances

-(NSArray *)buildRentalMediasArray:(NSArray*)rentalsArray {
    NSMutableArray * rentalsMediaIds = [NSMutableArray array];
    
    for (NSDictionary * dict in rentalsArray )
    {
        //avoid crash on NSNulls
        if (![dict isKindOfClass:[NSDictionary class]])
            continue;
        
        TVRental * rentalItem = [[TVRental alloc] initWithDictionary:dict];
        if ([rentalItem isRental] == YES) {
            [rentalsMediaIds addObject:@(rentalItem.mediaID)];
        }
    }
    return rentalsMediaIds;
}

-(NSArray *) buildWatchListMediasArray:(NSArray*)mediaItemsArr {
    
    NSMutableArray * watchListMediaIds = [NSMutableArray array];

    for (TVWatchListItem* watchListItem in mediaItemsArr) {
        if (![watchListItem.siteGuid isEqualToString:[TVSessionManager sharedTVSessionManager].currentUser.siteGUID]) {
            continue;
        }
        
        for (ItemObj* itemObj in watchListItem.items) {
            [watchListMediaIds addObject:@([itemObj.itemID integerValue])];
        }
    }
    return watchListMediaIds;
}


@end
