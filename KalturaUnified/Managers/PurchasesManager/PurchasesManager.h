//
//  PurchasesManager.h
//  KalturaUnified
//
//  Created by Alex Zchut on 4/21/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseNetworkObject.h"
//#import "TVMediaItem+ListType.h"
#import "TVMediaItem+PSTags.h"
#import "TVMediaItem+Additions.h"
#import "TVRental+Additions.h"

@class TVRental;
@interface PurchasesManager : NSObject

+ (PurchasesManager *)instance;

- (void) isMediaInWatchList:(NSInteger)mediaId delegate:(BaseViewController *)delegateController completion:(void (^)(BOOL success, NSError* error)) completion;
- (void) addMediaToWatchList:(NSInteger)mediaId delegate:(BaseViewController *)delegateController completion:(void(^)(BOOL success, NSError* error)) completion;
- (void) removeMediaFromWatchList:(NSInteger)mediaId delegate:(BaseViewController *)delegateController completion:(void(^)(BOOL success, NSError* error)) completion;
- (void) getPurchasesForPersonal:(BOOL)isPersonal delegate:(BaseViewController *)delegateController completion:(void (^)(NSArray* arrRentals, NSError* error)) completion;
- (void) getMediaFileRentInfo:(NSInteger) fileID delegate:(BaseViewController *)delegateController completion: (void (^)(TVRental* mediaRentalDetails)) completion;
@end
