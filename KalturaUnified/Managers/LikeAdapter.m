//
//  LikeAdapter.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LikeAdapter.h"
#import <TvinciSDK/TVSocialActionInfo.h>
#import <TvinciSDK/TVUserSocialActionDoc.h>
#import <TvinciSDK/TVSessionManager.h>

@interface LikeAdapter ()

@property (strong, nonatomic) TVMediaItem * mediaItem;
@property (strong, nonatomic) APSTVProgram * program;
@property (strong, nonatomic) NSNumber *programLikeStatus;
@end

@implementation LikeAdapter

#define kLikeCounterChangeObserver @"LikeAdapter_UpdateMediaItemLikeCount"

typedef enum ActionType
{
    kActionTypeNotDefined,
    kActionTypeLike,
    kActionTypeUnLike
    
} ActionType;

- (void)setMediaItem:(TVMediaItem *)mediaItem {
    if (_mediaItem==mediaItem)
        return;
    if (_mediaItem)
        [self unregisterObserversForMedia:_mediaItem];
    _mediaItem = mediaItem;
    if (_mediaItem)
        [self registerObserversForMedia:_mediaItem];
}

- (void)setProgram:(APSTVProgram *)program {
    _programLikeStatus = @NO;
    if (_program==program)
        return;
    if (_program)
        [self unregisterObserversForProgram:_program];
    _program = program;
    if (_program)
        [self registerObserversForProgram:_program];
}

- (void)dealloc {
    self.mediaItem = nil;
    self.program = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - KVO

- (void) registerObserversForMedia:(TVMediaItem*)item {
    [item addObserver:self forKeyPath:@"likeCounter" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:nil];
    [item addObserver:self forKeyPath:@"isMediaLike" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:nil];
}

- (void) unregisterObserversForMedia:(TVMediaItem*)item {
    [item removeObserver:self forKeyPath:@"likeCounter"];
    [item removeObserver:self forKeyPath:@"isMediaLike"];
}

- (void) registerObserversForProgram:(APSTVProgram*)item {
    [item addObserver:self forKeyPath:@"likeConter" options:NSKeyValueObservingOptionNew context:nil];
}

- (void) unregisterObserversForProgram:(APSTVProgram*)item {
    [item removeObserver:self forKeyPath:@"likeConter"];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([object isKindOfClass:[TVMediaItem class]]) {
        TVMediaItem *item = object;
        if ([keyPath isEqualToString:@"likeCounter"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kLikeCounterChangeObserver object:nil userInfo:@{@"id": item.mediaID, @"likeCounter": @(item.likeCounter)}];
        }
        
        if ([keyPath isEqualToString:@"isMediaLike"]) {
            [self updateForLikeStatus:item.isMediaLike];
        }
    }
    else if ([object isKindOfClass:[APSTVProgram class]]) {
        APSTVProgram *item = object;
        if ([keyPath isEqualToString:@"likeConter"]) {
            [self updateLikeCounter:[item.likeConter integerValue]];
        }
    }
}

#pragma mark - View functions

-(instancetype)initWithMediaItem:(TVMediaItem *) mediaItem control:(UIButton *) button {
    return [self initWithMediaItem:mediaItem control:button label:nil];
}

-(instancetype)initWithMediaItem:(TVMediaItem *) mediaItem control:(UIButton *) button label:(UILabel *) label {
    return [self initWithMediaItem:mediaItem control:button label:label imgLike:nil labelCounter:nil];
}

-(instancetype)initWithMediaItem:(TVMediaItem *) mediaItem control:(UIButton *) button label:(UILabel *) label imgLike:(UIImageView *) imgLike labelCounter:(UILabel *) labelCounter {
    
    self = [super init];
    if (self) {
        self.needToGetAssetsStats = TRUE;
        self.mediaItem = mediaItem;
        self.buttonLike = button;
        self.labelLike = label;
        self.imgLikeCounter = imgLike;
        self.labelLikeCounter = labelCounter;
        __block LikeAdapter *sself = self;
        if (self.buttonLike)
            [self.buttonLike setTouchUpInsideAction:^(UIButton *sender) {
                if (!sender.selected){
                    [sself sendLikeAction];
                }
                else {
                    [sself sendUnlikeAction];
                }
            }];
        self.hideButtonTitleForLikes = (isPad) ? NO : YES;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kLikeCounterChangeObserver object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLikeCounterFromPost:) name:kLikeCounterChangeObserver object:nil];
    }
    return self;
}

-(id)initWithProgram:(APSTVProgram *) program control:(UIButton *) button
{
    return [self initWithProgram:program control:button label:nil imgLike:nil labelCounter:nil];
}

-(id)initWithProgram:(APSTVProgram *) program control:(UIButton *) button  label:(UILabel *) label imgLike:(UIImageView *) imgLike labelCounter:(UILabel *) labelCounter {

    self = [super init];
    if (self) {
        self.needToGetAssetsStats = TRUE;
        self.program = program;
        self.buttonLike = button;
        self.labelLike = label;
        self.imgLikeCounter = imgLike;
        self.labelLikeCounter = labelCounter;
        self.hideButtonTitleForLikes = (isPad) ? NO : YES;

        __block LikeAdapter *sself = self;
        if (self.buttonLike)
            [self.buttonLike setTouchUpInsideAction:^(UIButton *sender) {
                if (!sender.selected){
                    [sself sendLikeAction];
                }
                else {
                    [sself sendUnlikeAction];
                }
            }];
    }
    return self;
}


-(IBAction)likeButtonPressed:(UIButton *)sender
{
    if (!sender.selected)
    {
        [self sendLikeAction];
    }
    else
    {
        [self sendUnlikeAction];
    }
}

-(void) setupMediaItem:(TVMediaItem *) mediaItem
{
    self.mediaItem = mediaItem;
    BOOL isSignIn = [LoginM isSignedIn];
    if (isSignIn) {
        [self isLikeMedia];
    } else {
        [self updateForLikeStatus:NO];
    }
}

-(void) setupProgram:(APSTVProgram *) program
{
    self.program = program;
    BOOL isSignIn = [LoginM isSignedIn];
    if (isSignIn) {
        [self isLikeMedia];
    } else {
        [self updateForLikeStatus:NO];
    }
}

- (void) setProgramLikeStatus:(NSNumber*) value {
    _programLikeStatus = value;

    [self updateForLikeStatus:[value boolValue]];
}

- (void)updateForLikeStatus:(BOOL)value {
    
    NSString *textValue = (value ? LS(@"liked") : LS(@"like"));
    
    if (!_hideButtonTitleForLikes) {
        [self.buttonLike setTitle:textValue forState:UIControlStateNormal];
        [self.buttonLike setTitle:textValue forState:UIControlStateHighlighted];
        [self.buttonLike setTitle:textValue forState:UIControlStateSelected];
    }
    
    self.buttonLike.selected = value;
    self.labelLike.text = textValue;
    self.labelLike.highlighted = value;
    
    self.imgLikeCounter.highlighted = value;
    self.labelLikeCounter.highlighted = value;
    
    if ([self.delegate respondsToSelector:@selector(likeAdapter:comleteUpdateLikeStatus:)])
        [self.delegate likeAdapter:self comleteUpdateLikeStatus:value];
}

- (void) updateLikeCounterFromPost: (NSNotification*) notification {
    NSDictionary *dict = [notification userInfo];
    if ([self.mediaItem.mediaID isEqualToString:dict[@"id"]])
        [self updateLikeCounter:[dict[@"likeCounter"] integerValue]];
}

- (void) updateLikeCounter:(NSInteger) likeCounter {
    if (self.mediaItem || self.program) {
        if (_labelLikeCounter) {
            NSString *like = [Utils getStringFromInteger:likeCounter];
            if (![self.labelLikeCounter.text isEqualToString:like])
                self.labelLikeCounter.text = like;
        }
    }
    
    if ([_buttonLike.titleLabel.text length] && _hideButtonTitleForLikes) {
        [_buttonLike setTitle:[Utils getStringFromInteger: likeCounter] forState:UIControlStateNormal];
        [_buttonLike setTitle:[Utils getStringFromInteger: likeCounter] forState:UIControlStateSelected];
    }
}

-(void) sendUnlikeAction
{
    self.buttonLike.userInteractionEnabled = NO;
    
    __weak TVPAPIRequest * request  = nil;
    
    NSArray * arrayAddtionalInfo = nil;//@[@{@"key": @"obj:url",@"value":@""}];
    
    if (self.mediaItem)
    {
        request = [TVPSocialAPI requestForDoUserActionWithUserAction:TVUserSocialActionUNLIKE assetType:TVAssetType_MEDIA assetID:self.mediaItem.mediaID.integerValue extraParams:arrayAddtionalInfo SocialPlatform:TVSocialPlatformFacebook delegate:nil];
    }
    else if (self.program)
    {
        request = [TVPSocialAPI requestForDoUserActionWithUserAction:TVUserSocialActionUNLIKE assetType:TVAssetType_PROGRAM assetID:[self.program.epgId integerValue] extraParams:arrayAddtionalInfo SocialPlatform:TVSocialPlatformFacebook delegate:nil];

    }
    
    [request setFailedBlock:
     ^{
         NSLog(@"ERROR: %@",request.responseString);
         self.buttonLike.userInteractionEnabled = YES;
         if (!self.mediaItem.isMediaLike)
             self.mediaItem.isMediaLike = YES;
     }];
    
    [request setCompletionBlock:
     ^{
         NSLog(@"%@",request.responseString);
         
         self.buttonLike.userInteractionEnabled = YES;
         self.mediaItem.isMediaLike = YES;
         
         if ([[request JSONResponse] isKindOfClass:[NSDictionary class]]) {
             NSDictionary *userActionDictionary = [request JSONResponse];
             TVSocialActionInfo *socialActionInfo = [[TVSocialActionInfo alloc] initWithDictionary:userActionDictionary];
             
             if (socialActionInfo.internalStatus == TVSocialActionResponseStatus_OK) {
                 [self updateStatusForType:kActionTypeUnLike];
             }
         } else if ([[request JSONResponse] isKindOfClass:[NSString class]]) {
             
             NSString *result = [request JSONResponse];
             
             if ([result isEqualToString:@"OK"]) {
                 [self updateStatusForType:kActionTypeUnLike];
             }
         }
     }];
    
    [self sendRequest:request];
}

-(void) sendLikeAction
{
    self.buttonLike.userInteractionEnabled = NO;
    
    __weak TVPAPIRequest * request = nil;
    
    NSArray * arrayAddtionalInfo = nil;
    
    if (self.mediaItem)
    {
        arrayAddtionalInfo = @[@{@"key": @"obj:url",@"value":self.mediaItem.mediaWebLink.absoluteString?self.mediaItem.mediaWebLink.absoluteString:@""}];
        request = [TVPSocialAPI requestForDoUserActionWithUserAction:TVUserSocialActionLIKE assetType:TVAssetType_MEDIA assetID:self.mediaItem.mediaID.integerValue extraParams:arrayAddtionalInfo SocialPlatform:TVSocialPlatformFacebook delegate:nil];
    }
    else if (self.program) {
        request = [TVPSocialAPI requestForDoUserActionWithUserAction:TVUserSocialActionLIKE assetType:TVAssetType_PROGRAM assetID:[self.program.epgId integerValue] extraParams:arrayAddtionalInfo SocialPlatform:TVSocialPlatformFacebook delegate:nil];
    }
    
    [request setFailedBlock:^{
         NSLog(@"ERROR: %@",request.responseString);
         self.buttonLike.userInteractionEnabled = YES;
        if (self.mediaItem.isMediaLike)
            self.mediaItem.isMediaLike = NO;
     }];
    
    [request setCompletionBlock:^{

        NSLog(@"%@",request.responseString);
         self.buttonLike.userInteractionEnabled = YES;
        
         if ([[request JSONResponse] isKindOfClass:[NSDictionary class]]) {
             NSDictionary *userActionDictionary = [request JSONResponse];
             TVSocialActionInfo *socialActionInfo = [[TVSocialActionInfo alloc] initWithDictionary:userActionDictionary];
             
             if (socialActionInfo.internalStatus == TVSocialActionResponseStatus_OK ) {
                 [self updateStatusForType:kActionTypeLike];
             }
         }
         else if ([[request JSONResponse] isKindOfClass:[NSString class]]) {
             
             NSString *result = [request JSONResponse];
             if ([result isEqualToString:@"OK"]) {
                 [self updateStatusForType:kActionTypeLike];
             }
         }
     }];
    
    [self sendRequest:request];
}

- (void) updateStatusForType:(ActionType) currentActionType {
    [self updateStatusForType:currentActionType updateCounter:TRUE];
}

- (void) updateStatusForType:(ActionType) currentActionType updateCounter:(BOOL)shouldUpdateCounter {
    if (self.mediaItem) {
        switch (currentActionType) {
            case kActionTypeLike:
                if (!self.mediaItem.isMediaLike)
                    self.mediaItem.isMediaLike = YES;
                else
                    [self updateForLikeStatus:self.mediaItem.isMediaLike];

                if (shouldUpdateCounter)
                    self.mediaItem.likeCounter += 1;
                break;
            case kActionTypeUnLike:
                if (self.mediaItem.isMediaLike)
                    self.mediaItem.isMediaLike = NO;
                else
                    [self updateForLikeStatus:self.mediaItem.isMediaLike];

                if (shouldUpdateCounter)
                    self.mediaItem.likeCounter = MAX(0, self.mediaItem.likeCounter-1);
                break;
            default:
                break;
        }
        //update like counter
        [self updateLikeCounter:self.mediaItem.likeCounter];
    }
    else if (self.program) {
        switch (currentActionType) {
            case kActionTypeLike:
                self.programLikeStatus = @YES;
                if (shouldUpdateCounter)
                    self.program.likeConter = @([self.program.likeConter integerValue]+1);
                break;
            case kActionTypeUnLike:
                self.programLikeStatus = @NO;
                if (shouldUpdateCounter)
                    self.program.likeConter = @(MAX(0,[self.program.likeConter integerValue]-1));
                break;
            default:
                break;
        }
        //update like counter
        [self updateLikeCounter:[self.program.likeConter integerValue]];
        
        if ([self.delegate respondsToSelector:@selector(likeAdapter:comleteUpdateLikeStatus:)])
            [self.delegate likeAdapter:self comleteUpdateLikeStatus:[self.programLikeStatus boolValue]];
    }
}

-(void) isLikeMedia
{
    if (!self.mediaItem && !self.program) {
        return;
    }
    
    //update like counter
    [self updateLikeCounter:self.mediaItem.likeCounter];
    
    self.buttonLike.userInteractionEnabled = NO;
    
    //create updated params
    __block NSInteger likesCountUpdated = 0;
    __block ActionType actionTypeUpdated = kActionTypeNotDefined;
    
    //create async run group
    dispatch_group_t group = dispatch_group_create();

    //get like status
    dispatch_group_enter(group);
    __weak TVPAPIRequest * request = nil;
    if (self.mediaItem) {
        request = [TVPSocialAPI requestforGetUserActionsWithUserAction:TVUserSocialActionLIKE assetType:TVAssetType_MEDIA assetID:self.mediaItem.mediaID.integerValue startIndex:0 recordsNum:4 socialPlatform:TVSocialPlatformFacebook delegate:nil];
    }
    else if (self.program) {
        request = [TVPSocialAPI requestforGetUserActionsWithUserAction:TVUserSocialActionLIKE assetType:TVAssetType_PROGRAM assetID:[self.program.epgId integerValue] startIndex:0 recordsNum:4 socialPlatform:TVSocialPlatformFacebook delegate:nil];
    }
    
    [request setFailedBlock:^{
        self.buttonLike.userInteractionEnabled = YES;
        dispatch_group_leave(group);
    }];
    
    [request setCompletionBlock:^{
        
        self.buttonLike.userInteractionEnabled = YES;
        if (self.mediaItem && self.mediaItem.isMediaLike)
            self.mediaItem.isMediaLike = NO;
        else if (self.program && [self.programLikeStatus boolValue])
            self.programLikeStatus = @NO;
        
        NSArray *responseArray = [request JSONResponse];
        NSDictionary *socialActionDictionary = responseArray.firstObject;
        if (socialActionDictionary) {
            TVUserSocialActionDoc *socialActionDoc = [[TVUserSocialActionDoc alloc] initWithDictionary:socialActionDictionary];
            if (socialActionDoc.isActive) {
                actionTypeUpdated = kActionTypeLike;
            }
            
            if (!socialActionDoc.mediaId) {
                TVUserSocialAction *socialAction = [[TVUserSocialAction alloc] initWithDictionary:socialActionDictionary];
                if (socialAction.socialAction == TVUserSocialActionLIKE) {
                    actionTypeUpdated = kActionTypeLike;
                }
                else {
                    actionTypeUpdated = kActionTypeUnLike;
                }
            }
        }
        else {
            actionTypeUpdated = kActionTypeUnLike;
        }
        dispatch_group_leave(group);
        
    }];
    [self sendRequest:request];
    
    if (self.needToGetAssetsStats) {
        //get likes counter
        dispatch_group_enter(group);
        /* "PSC : ONO : OPF-1245 VFGTVONES-123 - iPhone/App is unstable and crashes */
        [MediaGRA getAssetsStatsById:(self.mediaItem) ? self.mediaItem.mediaID : (self.program.epgId) ? self.program.epgId : @"0"
                                     assetType:(self.mediaItem) ? TVAssetType_MEDIA : TVAssetType_PROGRAM
                                      delegate:self
                                    completion:^(NSInteger likes, NSInteger views, NSInteger votes) {

            likesCountUpdated = likes;
            dispatch_group_leave(group);
        }];
        
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            if (self.mediaItem && self.mediaItem.likeCounter != likesCountUpdated) {
                self.mediaItem.likeCounter = likesCountUpdated;
            }
            else if (self.program && [self.program.likeConter integerValue] != likesCountUpdated) {
                self.program.likeConter = @(likesCountUpdated);
            }
            [self updateStatusForType:actionTypeUpdated updateCounter:FALSE];
        });
    }
}

- (void)refresh {
    [self isLikeMedia];
}

@end
