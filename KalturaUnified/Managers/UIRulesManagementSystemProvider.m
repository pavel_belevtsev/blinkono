//
//  TVUIRulesManagementSystemProvider.m
//  Blink
//
//  Created by Rivka Schwartz on 5/20/15.
//  Copyright (c) 2015 Quickode Ltd. All rights reserved.
//

#import "UIRulesManagementSystemProvider.h"
#import <TvinciSDK/TVSuspensionManager.h>
#import <TvinciSDK/TVCommercializationManager.h>


static NSString *const UIUnitsNames[] = {
    @"UIUnit_live_startOver_buttons",
    @"UIUnit_live_catchup_buttons",
    @"UIUnit_live_record_buttons",
    @"UIUnit_SeekBar_Thumb_StartOver",
    @"UIUnit_SeekBar_Thumb_Catchup",
    @"UIUnit_Play_StartOver",
    @"UIUnit_Play_Catchup",
    @"UIUnit_MyTV_RecordTab",
    @"UIUnit_MyTV_DownloadsTab",
    @"UIUnit_Social_buttons",
    @"UIUnit_favorites_buttons",
    @"UIUnit_live_playPause_button",
    @"UIUnit_Companion_buttons",
    @"UIUnit_Companion_play",
    @"UIUnit_Settings_Downloads",
    @"UIUnit_Download_buttons",
    @"UIUnit_Device_Downloads",
    nil};

NSString *TVNameForUIUnit(UIRulesUnit rulesUnit){
    
    NSString *name = nil;
    NSInteger length = (sizeof(UIUnitsNames)/sizeof(UIUnitsNames[0]));
    if (rulesUnit < length)
    {
        name = UIUnitsNames[rulesUnit];
    }
    return name;
    
}



@implementation UIRulesManagementSystemProvider

-(instancetype)init
{
    if (self == [super init])
    {
    }
    
    return self;
}

- (void) startTrackigForUserStatusWithCompletion:(void(^)()) completion
{

    [[TVUIRulesManagementSystem sharedInstance] setDelegate:self];
    [self registerForNotification];
    [[TVCommercializationManager sharedInstance] startTrakingForServicesWithCompletion:
     ^{
        [[TVSuspensionManager sharedInstance] startTrakingForSuspnesionWithCompletion:
         ^{
             if (completion)
             {
                 completion();
             }
        }];
    }];


}

-(void) registerForNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userStatusChanged:) name:NotificationNameSuspendedStatusChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userStatusChanged:) name:NotificationNameCommercializationStatusChanged object:nil];

}

-(void) unregisterForNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

__strong static id _sharedObject = nil;

+ (id) sharedInstance
{
    if (_sharedObject == nil)
    {
        static dispatch_once_t onceToken = 0;
        dispatch_once(&onceToken, ^{
            _sharedObject = [[self alloc] init];
        });
    }
    return _sharedObject;
}

- (void)becomeDefaultFactory {
    
    @synchronized (self)
    {
        if (_sharedObject != self)
        {
            _sharedObject = self;
        }
    }
}



-(BOOL) rulesManagementSystemIsUserSuspended:(TVUIRulesManagementSystem * ) sender
{
    return [[TVSuspensionManager  sharedInstance] isSuspended];
}


-(BOOL) rulesManagementSystem:(TVUIRulesManagementSystem * ) sender
                           isDeniedForService:(TVUIUnitRelatedService ) service
{
    if(!APST.hasCommercialization)
    {
        return NO;
    }

    TVCommercializationServiceType commercializationService = TVCommercializationServiceType_Unknown;
    switch (service)
    {
        case TVUIUnitRelatedService_NPVR:
            commercializationService = TVCommercializationServiceType_NPVR;
            break;
        case TVUIUnitRelatedService_StartOver:
            commercializationService = TVCommercializationServiceType_StartOver;
            break;
        case TVUIUnitRelatedService_Cachup:
            commercializationService = TVCommercializationServiceType_CatchUp;
            break;
        case TVUIUnitRelatedService_Download:
            commercializationService = TVCommercializationServiceType_Download;
            break;
        default:
            commercializationService = TVCommercializationServiceType_Unknown;
            break;
    }

    if (commercializationService == TVCommercializationServiceType_Unknown )
    {
        return NO;
    }
    else
    {
       return ![[TVCommercializationManager sharedInstance]  isServiceSupported:commercializationService];
    }

}

-(BOOL) rulesManagementSystemIsAnonymous:(TVUIRulesManagementSystem * ) sender
{
    return ![[TVSessionManager sharedTVSessionManager] isSignedIn];
    
}

-(BOOL) rulesManagementSystem:(TVUIRulesManagementSystem * ) sender
                            isServiceDetached:(TVUIUnitRelatedService ) service
{

    BOOL isServiceDetached = NO;
    switch (service) {
        case TVUIUnitRelatedService_NPVR:
            isServiceDetached = !APST.supportRecordings;
            break;
        case TVUIUnitRelatedService_StartOver:
            isServiceDetached = !APST.catchUpEPG;
            break;
        case TVUIUnitRelatedService_Cachup:
            isServiceDetached = !APST.catchUpEPG;
            break;
            case TVUIUnitRelatedService_Companion:
            isServiceDetached = !APST.companion;
            break;
        case TVUIUnitRelatedService_Download:
            isServiceDetached = !APST.hasDownloadToGo;
            break;
        case TVUIUnitRelatedService_Unknown:
            break;

        default:
            break;
    }
    
    return isServiceDetached;
}

-(NSString *) rulesManagementSystemInputFileName:(TVUIRulesManagementSystem * ) sender
{
    return @"UIRulesUnitsMap";
}

-(void) userStatusChanged:(NSNotification *) notification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationNameUserStatusChanged object:nil];
}


- (void)dealloc
{
    [self unregisterForNotification];
}
@end
