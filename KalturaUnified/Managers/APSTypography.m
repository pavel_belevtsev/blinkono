//
//  APSTypography.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "APSTypography.h"

@implementation APSTypography

__strong static id sharedObject = nil;

+ (APSTypography *)instance {
    static dispatch_once_t p = 0;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (id)init {
    self = [super init];
    if (self)
    {
        NSDictionary *features = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Features" ofType:@"plist"]];
        

        self.brandName = [features objectForKey:@"brandName"];
        self.brandColor = CS([features objectForKey:@"brandColor"]);
        self.brandTextColor = CS([features objectForKey:@"brandTextColor"]);
        self.facebookColor = CS([features objectForKey:@"facebookColor"]);
        
        self.aboutUsUrl = [NSURL URLWithString:[features objectForKey:@"aboutUsUrl"]];
        self.urlTermsOfService = [NSURL URLWithString:[features objectForKey:@"urlTermsOfService"]];
        self.mailAddress = [features objectForKey:@"mailAddress"];
        
        self.ipno = [[features objectForKey:@"ipno"] boolValue];
        self.multiUser = [[features objectForKey:@"multiUser"] boolValue];
        self.category_16_9 = [[features objectForKey:@"category_16_9"] boolValue];
        self.hasWatchList = [[features objectForKey:@"hasWatchList"] boolValue];
        self.hasInAppPurchases = [[features objectForKey:@"hasInAppPurchases"] boolValue];
        self.hasDownloadToGo = [[features objectForKey:@"hasDownloadToGo"] boolValue];
        self.dtgParams = [features objectForKey:@"dtgParams"];

        
        self.joinUrl = [NSURL URLWithString:[features objectForKey:@"joinUrl"]];
        
        self.googleAnalyticsId = [features objectForKey:@"googleAnalyticsId"];
        self.crittercismId = [features objectForKey:@"crittercismId"];
        self.crashlyticsInfo = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Fabric"];
        
        self.appBundlePhone = [features objectForKey:@"appBundlePhone"];
        self.appVersionPhone = [features objectForKey:@"appVersionPhone"];
        
        self.appBundlePad = [features objectForKey:@"appBundlePad"];
        self.appVersionPad = [features objectForKey:@"appVersionPad"];
        
        
        if (self.appBundlePad == nil && self.appBundlePhone == nil && self.appVersionPad == nil  && self.appVersionPhone == nil)
        {
            NSString * appBundle = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleIdentifierKey];
            NSString * versionId =  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            
            self.appBundlePhone = [NSString stringWithFormat:@"%@.phone",appBundle];
            self.appBundlePad = [NSString stringWithFormat:@"%@.tablet",appBundle];
            
            self.appVersionPhone = versionId;
            self.appVersionPad = versionId;
            
        }
        
        self.numberOfRetroDays = [[features objectForKey:@"numberOfRetroDays"] integerValue];
        self.numberOfWeekDays = [[features objectForKey:@"numberOfWeekDays"] integerValue];
        
        self.socialFeed = [[features objectForKey:@"socialFeed"] boolValue];
        self.fbSupport = [[features objectForKey:@"fbSupport"] boolValue];
        self.enableFBSharePrivacy = [[features objectForKey:@"enableFBSharePrivacy"] boolValue];
        self.sharePrivacy = [[features objectForKey:@"sharePrivacy"] boolValue];
        self.searchEPG = [[features objectForKey:@"searchEPG"] boolValue];
        self.likeEPG = [[features objectForKey:@"likeEPG"] boolValue];
        self.likeLive = [[features objectForKey:@"likeLive"] boolValue];
        self.STBFollowMe = [[features objectForKey:@"STBFollowMe"] boolValue];
        self.showHomePageTitles = [[features objectForKey:@"showHomePageTitles"] boolValue];
        
        self.myZoneItems = [features objectForKey:@"myZoneItems"];
        
        self.companion = [[features objectForKey:@"companion"] boolValue];
        self.proximity = [[features objectForKey:@"proximity"] boolValue];
        
        self.menuFotterTV = [[features objectForKey:@"menuFotterTV"] boolValue];
        
        self.supportRecordings = [[features objectForKey:@"supportRecoredings"] boolValue];
        self.myZoneFullPageEnabled = [[features objectForKey:@"myZoneFullPageEnabled"] boolValue];
        
        self.deepLinkingScheme = [features objectForKey:@"deepLinkingScheme"];
        self.pageSize = [[features objectForKey:@"pageSize"] integerValue];
        if (self.pageSize == 0) self.pageSize = 40;
        
        self.showChannelNumber = [[features objectForKey:@"showChannelNumber"] boolValue];
        self.showGuestUser = [[features objectForKey:@"showGuestUser"] boolValue];
        
        self.catchUpEPG = [[features objectForKey:@"catchUpEPG"] boolValue];
        
        self.anonymousUserAllowedToPlayFreeItems = [[features objectForKey:@"anonymousUserAllowedToPlayFreeItems"] boolValue];
        self.settingsAndLoginFromMenu = [[features objectForKey:@"SettingsAndLoginInMenu"] boolValue];
        self.socialProviders = [features objectForKey:@"SocialProviders"];
        
        self.followMeMinimalTime = [features objectForKey:@"followMeMinimalTime"];
        
        self.contactUsPageSettingsEnabled = [[features objectForKey:@"contactUsSettingsEnabled"] boolValue];
        self.billingHistorySettingsEnabled = [[features objectForKey:@"billingHistorySettingsEnabled"] boolValue];
        
        self.minPasswordLength = [[features objectForKey:@"minPasswordLength"] integerValue];
        self.hasCommercialization = [[features objectForKey:@"hasCommercialization"] boolValue];
        
        
        [self updateData];
        
    }
    return self;
}

- (void)updateData {
    
    NSDictionary *customSkin = [[NSUserDefaults standardUserDefaults] objectForKey:TVNUserDefaultsCustomSkin];
    if (customSkin) {
        
        if ([customSkin objectForKey:@"brandColor"]) {
            self.brandColor = CS([customSkin objectForKey:@"brandColor"]);
        }
        
        if ([customSkin objectForKey:@"brandTextColor"]) {
            self.brandTextColor = CS([customSkin objectForKey:@"brandTextColor"]);
        }
        
        if ([customSkin objectForKey:@"logoURLString"]) {
            self.logoUrl = [NSURL URLWithString:[customSkin objectForKey:@"logoURLString"]];
        }
        
        if ([customSkin objectForKey:@"aboutUsUrl"]) {
            self.aboutUsUrl = [NSURL URLWithString:[customSkin objectForKey:@"aboutUsUrl"]];
        }
        
        if ([customSkin objectForKey:@"mailAddress"]) {
            self.mailAddress = [customSkin objectForKey:@"mailAddress"];
        }
        
    }
    
    UIColor *color = _brandColor;
    
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    float valueR = components[0] - 0.25;
    if (valueR < 0) valueR = 0.0;
    float valueG = components[1] - 0.25;
    if (valueG < 0) valueG = 0.0;
    float valueB = components[2] - 0.25;
    if (valueB < 0) valueB = 0.0;
    
    self.brandDarkColor = [UIColor colorWithRed:valueR green:valueG blue:valueB alpha:1.0];
}

- (void)becomeDefaultTypography
{
    sharedObject = self;
}

@end
