//
//  ForceUpgraadeManager.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 3/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ForceUpgraadeManager : NSObject

+ (instancetype)sharedInstance;
-(BOOL) isApplicationContinuencePermitted;
-(void) handleForceDownload;
@end
