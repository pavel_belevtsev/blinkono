//
//  FavoriteAdapter.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 17.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FavoriteAdapter;

@protocol FavoriteAdapterDelegate <NSObject>

- (void) favoriteAdapter:(FavoriteAdapter*)favoriteAdapter comleteUpdateFavoriteStatus:(BOOL) status;

@end

@interface FavoriteAdapter : BaseManagement

- (void)setupMediaItem:(TVMediaItem *)mediaItem;

- (id)initWithMediaItem:(TVMediaItem *)mediaItem control:(UIButton *) button;
- (id)initWithMediaItem:(TVMediaItem *)mediaItem control:(UIButton *) button label:(UILabel *) label;

- (void)refresh;

@property (strong, nonatomic) UIButton *buttonFavorite;
@property (weak, nonatomic) UILabel * labelFavorite;

@property (assign,nonatomic) id<FavoriteAdapterDelegate> delegate;

@end
