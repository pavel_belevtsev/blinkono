//
//  RefreshDataManagerViewController.m
//  KalturaUnified
//
//  Created by Rivka Schwartz on 4/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "RefreshDataManager.h"
#import <TvinciSDK/TVDataRefreshHandler.h>

@interface RefreshDataManager ()

@end

@implementation RefreshDataManager



+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    
    dispatch_once(&once, ^
                  {
                      sharedInstance = [self new];
                  });
    
    return sharedInstance;
}


//-(instancetype)init
//{
//    if (self == [super init])
//    {
//    }
//    
//    return self;
//}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) addAllTasks
{
    [self refreshChannels];
}


-(void) refreshChannels
{
    
    [[TVDataRefreshHandler sharedInstance] addDataRefreshTaskWithRefreshPeriod:10 offset:10 target:self withRefreshBlock:^(void(^ completion )(TVDataRefreshTaskResult result))
     {
         
         [EPGM loadEPGChannels:self completion:^(NSMutableArray *result) {
             
             [EPGM updateEPGList:result delegate:self completion:^(NSMutableArray *result) {
                 
                 if (completion)
                 {
                     completion(TVDataRefreshTaskResultRefreshed);
                 }
             }];
             
         }];
         
     }];
    
   

}
@end
