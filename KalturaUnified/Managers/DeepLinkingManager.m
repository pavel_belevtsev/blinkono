//
//  DeepLinkingManager.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.02.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "DeepLinkingManager.h"

@implementation DeepLinkingManager

+ (DeepLinkingManager *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (id)init {
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (BOOL)registerDeepLinkingAction:(NSURL *)url {
    
    DeepLinkingType result = DeepLinkingTypeCorrupted;
    
    NSString *mediaId = @"";
    NSString *categoryName = @"";
    
    NSArray *dlTypes = [NSArray arrayWithObjects:@"Home", @"LiveTV", @"EPG", @"MyZone", @"OnlinePurchases", @"Downloads", @"Recordings", nil];
    
    NSString *host = [url host];
    //NSString *path = [url path];
    NSString *query = [url query];
    //NSLog(@"%@ %@ %@", host, path, query);
    
    NSArray *queryArray = [query componentsSeparatedByString:@"&"];
    
    if ([host isEqualToString:@"menu"]) {
        
        if ([queryArray count]) {
            NSString *first = [queryArray objectAtIndex:0];
            NSArray *params = [first componentsSeparatedByString:@"="];
            
            if (([params count] == 2) && [[[params objectAtIndex:0] lowercaseString] isEqualToString:@"type"]) {
                NSString *type = [params objectAtIndex:1];
                
                for (int i = 0; i < [dlTypes count]; i++) {
                    NSString *strType = [[dlTypes objectAtIndex:i] uppercaseString];
                    
                    if ([[type uppercaseString] isEqualToString:strType]) {
                        result = i;
                    }
                }
                
                
                if ([[type lowercaseString] isEqualToString:@"category"] && ([queryArray count] > 1)) {
                    
                    NSString *second = [queryArray objectAtIndex:1];
                    NSArray *params = [second componentsSeparatedByString:@"="];
                    
                    if (([params count] == 2) && [[[params objectAtIndex:0] lowercaseString] isEqualToString:@"name"]) {
                        NSString *name = [params objectAtIndex:1];

                        if ([name length]) {
                            
                            result = DeepLinkingTypeCategory;
                            categoryName = [name stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
                        }
                        
                    }
                }
                
            }
        }
        
    
    } else if ([host isEqualToString:@"media"]) {
        
        result = DeepLinkingTypeCorrupted;
        
        if ([queryArray count] == 1) {
            NSString *first = [queryArray objectAtIndex:0];
            NSArray *params = [first componentsSeparatedByString:@"="];

            if (([params count] == 2) && [[[params objectAtIndex:0] lowercaseString] isEqualToString:@"id"]) {
                NSString *idValue = [params objectAtIndex:1];
                
                if ([idValue integerValue] > 0) {
                    result = DeepLinkingTypeMedia;
                    mediaId = idValue;
                }

            }
            
        }
        
    }
    
    
    if (result == DeepLinkingTypeCorrupted) {
        result = DeepLinkingTypeHome;
    }
    
    if (result != DeepLinkingTypeCorrupted) {
        DeepLinkingAction *action = [[DeepLinkingAction alloc] init];
        action.deepLinkingType = result;
        action.mediaId = mediaId;
        action.categoryName = categoryName;
        self.deepLinkingAction = action;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:KDeepLinkingActionNotification object:nil];
    }
    
    return (result != DeepLinkingTypeCorrupted);
    
}

@end
