//
//  FeaturesMap.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeaturesMap : NSObject

+ (FeaturesMap *)instance;

@end
