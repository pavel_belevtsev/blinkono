//
//  PPVManager.h
//  KalturaUnified
//
//  Created by Israel Berezin on 6/7/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseNetworkObject.h"
#import "PPVPurchaseParams.h"
#import "TVRental+Additions.h"

@interface PPVManager : BaseNetworkObject

+ (PPVManager *)instance;

-(void)getPPVModuleDataWithPPVmodel:(NSString*)PPVmodel productCode:(NSString*)productCode completion:(void(^)(TVPPVModelData *subscriptionData))completion;

- (void) chargeUserWithInApp:(PPVChargeUserWithInAppRequestParams *)params completion: (void (^)(BOOL success, NSString* statusDescription)) completion;

- (NSString*) getNR_PPVLeftPeriod:(TVRental*) data;

@end
