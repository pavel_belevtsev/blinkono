//
//  PPVManager.m
//  KalturaUnified
//
//  Created by Israel Berezin on 6/7/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PPVManager.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

#pragma mark - Init -

@implementation PPVManager

+ (PPVManager *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

#pragma mark - API -

-(void)getPPVModuleDataWithPPVmodel:(NSString*)PPVmodel productCode:(NSString*)productCode completion:(void(^)(TVPPVModelData *ppvData))completion
{
    ASLogInfo(@"PPVmodel = %@",PPVmodel);
    __weak TVPAPIRequest *request = [TVPPricingAPI requestForGetPPVModuleData:PPVmodel delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSDictionary *result = [[request JSONResponse] dictionaryByRemovingNSNulls];
        NSError *err = nil;
        
        TVPPVModelData * data = [[TVPPVModelData alloc] initWithDictionary:result error:&err];
        if (completion)
        {
            __block NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
            [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
            [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            [IAP getSKProductForProductCode:productCode completion:^(SKProduct *product) {
                if (product) {
                    [numberFormatter setLocale:product.priceLocale];
                    data.PPVPriceCode.prise.name = [numberFormatter stringFromNumber:product.price];
                    data.productCode = productCode;
                }
                completion(data);
            }];
        }
    }];
    
    [request setFailedBlock:^{
        
    }];
    
    [self sendRequest:request];
}

- (void) chargeUserWithInApp:(PPVChargeUserWithInAppRequestParams *)params completion: (void (^)(BOOL success, NSString* statusDescription)) completion
{
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForChargeUserForMediaFileInApp:^(RPChargeUserPPVWithInApp *requestParams) {
        
        PPVFullData *sub = params.ppvData;
        TVPPVModelData* ppvData = sub.ppvData;
        
        requestParams.price = ppvData.PPVPriceCode.prise.price;
        requestParams.currency = ppvData.PPVPriceCode.prise.currency.currencyCD3;
        requestParams.productCode = params.productCode;
        requestParams.ppvModuleCode = [NSString stringWithFormat:@"%d",ppvData.objectCode];
        requestParams.receipt = [params.receipt base64EncodedStringWithOptions:0];
    }];
    
    [request setCompletionBlock:^{
        NSDictionary *dictResponse = [request JSONResponse];
        NSLog(@"SubscriptionManager.chargeUserWithInApp: %@", dictResponse);
        
        //get updated purchased subscription data
        [self getPermitedItems:params.delegate isForUser:TRUE completion:^(NSArray *arrPermittedItems) {
            
            if (arrPermittedItems.count)
                params.ppvData.rentalInfo = [[arrPermittedItems filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVRental *pi, NSDictionary *bindings) {
                    return (pi.mediaFileID == params.ppvData.rentalInfo.mediaFileID);
                }]] firstObject];
            
            completion((params.ppvData.rentalInfo.purchaseDate) ? TRUE : FALSE, dictResponse[@"m_sStatusDescription"]);
        }];
    }];
    
    [request setFailedBlock:^{
        completion(FALSE, nil);
    }];
    
    [params.delegate sendRequest:request];
}

- (void) getPermitedItems:(BaseViewController *)delegateController isForUser:(BOOL)isForUser completion:(void(^)(NSArray *arrPermittedItems))completion {
    
    __weak TVPAPIRequest * request = (isForUser) ? [TVPMediaAPI requestForGetUserPermittedItems] : [TVPMediaAPI requestForGetDomainPermittedItems];
    
    [request setCompletionBlock:^{
        NSArray *arr = [[request JSONResponse] arrayByRemovingNSNulls];
        NSMutableArray *arrPermittedItems = [NSMutableArray array];
        
        for (NSDictionary *dict in arr)
        {
            TVRental *sub = [[TVRental alloc] initWithDictionary:dict];
            [arrPermittedItems addObject:sub];
        }
        
        completion(arrPermittedItems);
    }];
    
    [request setFailedBlock:^{
        completion(nil);
    }];
    
    [delegateController sendRequest:request];
}

#pragma mark - Utilts -

- (NSString*) getNR_PPVLeftPeriod:(TVRental*) data
{
    TVRental * rent = data;
    
    NSString *timeLeftString = @"";
    NSInteger timeLeftValue = [[NSDate date] distanceInDaysToDate: rent.endDate];
    if (timeLeftValue >= 2)
    {
        timeLeftString = (timeLeftValue > 1) ? LS(@"item_page_buy_subscription_days_left") : LS(@"item_page_buy_subscription_day_left");
    }
    else
    {
        timeLeftValue = [[NSDate date] distanceInHoursToDate: rent.endDate];
        timeLeftString = (timeLeftValue > 1) ? LS(@"item_page_buy_subscription_hours_left") : LS(@"item_page_buy_subscription_hour_left");
    }
    
    return [NSString stringWithFormat:timeLeftString, @(timeLeftValue)];
}

#pragma mark - IP Address -

- (NSString *)getIPAddress
{
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}
@end
