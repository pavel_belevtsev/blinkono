//
//  PPVFullData.h
//  KalturaUnified
//
//  Created by Israel Berezin on 6/7/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVRental+Additions.h"
#import <TvinciSDK/TVPPVModelData.h>


//ppvRentalInfo: (TVRental*) rentalInfo  ppvData:(TVPPVModelData *)ppvData
@interface PPVFullData : NSObject

@property (strong,nonatomic) TVRental* rentalInfo;
@property (strong,nonatomic) TVPPVModelData* ppvData;

@end
