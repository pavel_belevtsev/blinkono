//
//  WatchAdapter.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 3/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseNetworkObject.h"

@interface WatchAdapter : BaseNetworkObject

@property (strong, nonatomic) TVMediaItem * mediaItem;
@property (strong, nonatomic) APSTVProgram * program;

-(void) sendWatchAction;

@end
