//
//  ApplicationSettings.m
//  KalturaUnified
//
//  Created by Rivka Schwartz on 6/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "OfflineManager.h"


@interface OfflineManager ()

//@property (assign, nonatomic) BOOL firstLaunch;
@end


@implementation OfflineManager


+ (id) sharedInstance {
    static dispatch_once_t onceToken = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
        [_sharedObject setup];
    });
    return _sharedObject;
}


-(void)setIsOffline:(BOOL)isOffline
{
    if (_isOffline !=  isOffline)
    {
        _isOffline = isOffline;

        [[TVSDKSettings sharedInstance] setOfflineMode:isOffline];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationNameApplicationSettingsOfflineModeChanged object:nil];
    }
}


-(void) setup
{
//    self.firstLaunch = YES;
//    [self registerForNotification];
}



//-(void) registerForNotification
//{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
//
//}
//
//-(void) unregisterForNotification
//{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//}
//
//-(void) appDidBecomeActive:(NSNotification *) notification
//{
//    BOOL isConnectivityIsOffline = LoginM.internetReachability.currentReachabilityStatus == NotReachable;
//    if (self.firstLaunch == NO && [NU(UIUnit_Device_Downloads) isUIUnitExist])
//    {
//        if (isConnectivityIsOffline != self.isOffline)
//        {
//            self.isOffline = isConnectivityIsOffline;
//        }
//
//    }
//}
//
//-(void) appDidEnterBackground:(NSNotification *) notification
//{
//    self.firstLaunch = NO;
//}





@end
