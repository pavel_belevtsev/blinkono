//
//  ApplicationSettings.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 6/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

#define APP_SET [OfflineManager sharedInstance]

#define NotificationNameApplicationSettingsOfflineModeChanged @"NotificationNameApplicationSettingsOfflineModeChanged"

@interface OfflineManager : NSObject

+ (id) sharedInstance;
@property (assign, nonatomic) BOOL isOffline;

@end
