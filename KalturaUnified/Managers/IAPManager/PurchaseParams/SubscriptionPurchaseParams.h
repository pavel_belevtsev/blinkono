//
//  SubscriptionPurchaseParams.h
//  KalturaUnified
//
//  Created by Alex Zchut on 2/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/SKProduct.h>
#import <StoreKit/SKPayment.h>
#import <StoreKit/SKPaymentTransaction.h>

//
@interface SubscriptionRequestParams: NSObject
@property (nonatomic, weak) BaseViewController *delegate;
@property (nonatomic, strong) TVSubscriptionData *subscriptionData;
@end

//
@interface SubscriptionPurchaseRequestParams : SubscriptionRequestParams
@property (nonatomic, retain) NSString *productCode;
@end

//
@interface SubscriptionChargeUserWithInAppRequestParams : SubscriptionRequestParams
@property (nonatomic, retain) NSString *productCode;
@property (nonatomic, strong) NSData *receipt;
@end

//
@interface SubscriptionResponseParams: NSObject
@property (nonatomic, readwrite) BOOL success;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong) NSString *statusDescription;
@end



