//
//  PPVPurchaseParams.h
//  KalturaUnified
//
//  Created by Israel Berezin on 6/7/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/SKProduct.h>
#import <StoreKit/SKPayment.h>
#import <StoreKit/SKPaymentTransaction.h>
#import "PPVFullData.h"

@interface PPVRequestParams : NSObject

@property (nonatomic, weak) BaseViewController *delegate;
@property (nonatomic, strong) PPVFullData *ppvData;

@end

@interface PPVPurchaseRequestParams : PPVRequestParams
@property (nonatomic, copy) NSString *productCode;
@end

@interface PPVChargeUserWithInAppRequestParams : PPVRequestParams
@property (nonatomic, copy) NSString *productCode;
@property (nonatomic, strong) NSData *receipt;
@end

@interface PPVResponseParams: NSObject
@property (nonatomic, readwrite) BOOL success;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong) NSString *statusDescription;
@end

