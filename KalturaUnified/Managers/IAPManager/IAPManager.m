//
//  IAPManager.m
//  KalturaUnified
//
//  Created by Alex Zchut on 2/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SCPStoreKitManager.h"
#import "IAPManager.h"

@interface IAPManager ()
@property (nonatomic, retain) NSMutableArray *arrSKProducts;
@end

@implementation IAPManager

+ (IAPManager *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (instancetype)init {
    self = [super init];
    if (self)
    {        
        _arrSKProducts = [NSMutableArray array];
    }
    return self;
}

- (void) getSKProductForProductCode:(NSString*) productCode completion:(void(^)(SKProduct *product)) completion {
    [self getSKProductsForProductCodes:@[productCode] completion:^(NSArray *arrSKProducts) {
        completion(arrSKProducts.firstObject);
    }];
}

- (void) getSKProductsForProductCodes:(NSArray*) arrProductCodes completion:(void(^)(NSArray *arrSKProducts)) completion {
    __block void(^weakCompletion)(NSArray *arrSKProducts) = completion;
    NSMutableArray *arrSKProducts = [NSMutableArray array];
    NSMutableArray *arrSKProductsToGet = [NSMutableArray array];

    if (arrProductCodes && arrProductCodes.count) {
        for (NSString *code in arrProductCodes) {
            SKProduct *sk = [self getCachedSKProductForIdentifier:code];
            if (sk) {
                [arrSKProducts addObject:sk];
            }
            else {
                [arrSKProductsToGet addObject:code];
            }
        }
        
        if (!arrSKProductsToGet.count)
            completion(arrSKProducts);
        else {
            NSSet *identifiers = [[NSSet alloc] initWithArray:arrSKProductsToGet];
            
            [[SCPStoreKitManager sharedInstance] requestProductsWithIdentifiers:identifiers productsReturnedSuccessfully:^(NSArray *products) {
                for (SKProduct *product in products) {
                    [arrSKProducts addObject:product];
                    [self setCacheForSKProduct: product];
                }
                weakCompletion(arrSKProducts);
                weakCompletion = nil;
            } invalidProducts:^(NSArray *invalidProducts) {
                if (weakCompletion)
                    weakCompletion(arrSKProducts);
            } failure:^(NSError *error) {
                //error
                if (weakCompletion)
                    weakCompletion(arrSKProducts);
            }];
        }
    }
    else
        completion(nil);
}




- (void) purchaseSubscription:(void(^)(SubscriptionPurchaseRequestParams *reqParams))requestParams completion:(void(^)(SubscriptionResponseParams *resParams))responseParams  {
    
#if TARGET_IPHONE_SIMULATOR
    // where is the simulator
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    NSLog(@"AppStore login data path: 'data/Library/com.apple.itunesstored', delete dir to logout");
#endif
    
    //init params
    SubscriptionPurchaseRequestParams *reqParams = [SubscriptionPurchaseRequestParams new];
    SubscriptionResponseParams *resParams = [SubscriptionResponseParams new];
    //fill request params
    requestParams(reqParams);
    
    [self getSKProductForProductCode:reqParams.productCode completion:^(SKProduct *product) {
        if (product) {
            [[SCPStoreKitManager sharedInstance] requestPaymentForProduct:product paymentTransactionStatePurchasing:^(NSArray *transactions) {
                //purchasing
            } paymentTransactionStatePurchased:^(NSArray *transactions) {
                //purchased
                NSArray *arrProductCodes = [transactions valueForKeyPath:@"@distinctUnionOfObjects.payment.productIdentifier"];
                [self proceedWithTransaction:arrProductCodes
                               requestParams:reqParams
                                  completion:^(BOOL success, NSString *statusDescription) {
                                      resParams.success = success;
                                      resParams.statusDescription = statusDescription;
                                      responseParams(resParams);
                                  }];
                
            } paymentTransactionStateFailed:^(NSArray *transactions) {
                //failed
                resParams.success = FALSE;
                resParams.statusDescription = @"SKPaymentTransactionStateFailed";
                resParams.error = (transactions.count) ? [(SKPaymentTransaction*)transactions[0] error] : nil;
                
                //clear SKProducts cache
                [self.arrSKProducts removeAllObjects];
                
                responseParams(resParams);
            } paymentTransactionStateRestored:^(NSArray *transactions) {
                //restored
                [self proceedWithTransaction:[transactions lastObject]
                               requestParams:reqParams
                                  completion:^(BOOL success, NSString *statusDescription) {
                                      resParams.success = success;
                                      resParams.statusDescription = statusDescription;
                                      responseParams(resParams);
                                  }];
                
            } failure:^(NSError *error) {
                //error
                resParams.success = FALSE;
                resParams.statusDescription = @"SKPaymentTransactionStateFailed";
                resParams.error = error;
                responseParams(resParams);
            }];
        }
        else {
            resParams.success = FALSE;
            resParams.statusDescription = @"SKPaymentTransactionStateFailed";
            responseParams(resParams);
        }
    }];
}

- (void) restoreSubscriptions:(void(^)(SubscriptionRequestParams *reqParams))requestParams completion:(void(^)(SubscriptionResponseParams *resParams))responseParams {
    
    //init params
    SubscriptionRequestParams *reqParams = [SubscriptionRequestParams new];
    SubscriptionResponseParams *resParams = [SubscriptionResponseParams new];
    //fill request params
    requestParams(reqParams);
    
    [[SCPStoreKitManager sharedInstance] restorePurchasesPaymentTransactionStateRestored:^(NSArray *transactions) {
//        NSArray *arrProductCodes = [transactions valueForKeyPath:@"@distinctUnionOfObjects.payment.productIdentifier"];
//        [self proceedWithTransaction:arrProductCodes
//                       requestParams:reqParams
//                          completion:^(BOOL success, NSString *statusDescription) {
//                              resParams.success = success;
//                              resParams.statusDescription = statusDescription;
//                              responseParams(resParams);
//                          }];
        resParams.success = TRUE;
        resParams.statusDescription = @"SKRestoreTransactionStateCompleted";
        responseParams(resParams);
    } paymentTransactionStateFailed:^(NSArray *transactions) {
        resParams.success = FALSE;
        resParams.statusDescription = @"SKRestoreTransactionStateFailed";
        responseParams(resParams);
    } failure:^(NSError *error) {
        resParams.success = FALSE;
        resParams.statusDescription = @"SKRestoreTransactionStateFailed";
        responseParams(resParams);
    }];
}

- (void) proceedWithTransaction:(NSArray *)arrTransactionProductCodes
                           requestParams:(SubscriptionRequestParams*) reqParams
                              completion:(void(^)(BOOL success, NSString *statusDescription))completion {
    
    if (reqParams.subscriptionData) {
        [self proceedWithTransactionStep2:arrTransactionProductCodes requestParams:reqParams availableSubscriptions:nil completion:completion];
    }
    else {
        [SubscriptionM getAllSubscriptionDataFromVirtualMedias:reqParams.delegate needPurchasingInfo:FALSE completion:^(NSArray *arrSubscriptions) {
            [self proceedWithTransactionStep2:arrTransactionProductCodes requestParams:reqParams availableSubscriptions:arrSubscriptions completion:completion];
         }];
    }
}

- (void) proceedWithTransactionStep2:(NSArray *)arrTransactionProductCodes
                       requestParams:(SubscriptionRequestParams*) reqParams
              availableSubscriptions:(NSArray *)arrAvailableSubscriptions
                          completion:(void(^)(BOOL success, NSString *statusDescription))completion {
    
    __block NSInteger proceededCount = 0;

   
        for (NSString *productCode in arrTransactionProductCodes) {
            NSPredicate *predicateSubscription = [NSPredicate predicateWithBlock:^BOOL(TVSubscriptionData *sub, NSDictionary *bindings) {
                return ([sub.productCode isEqualToString:productCode]);
            }];
            SubscriptionChargeUserWithInAppRequestParams *requestParams = [SubscriptionChargeUserWithInAppRequestParams new];
            requestParams.productCode = productCode;
            requestParams.delegate = reqParams.delegate;
            requestParams.subscriptionData = (reqParams.subscriptionData) ?: [[arrAvailableSubscriptions filteredArrayUsingPredicate:predicateSubscription] firstObject];
            requestParams.receipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
            
            if (requestParams.subscriptionData && completion)
                [SubscriptionM chargeUserWithInApp:requestParams
                                        completion:^(BOOL success, NSString *statusDescription) {
                                            ++proceededCount;
                                            if (proceededCount == arrTransactionProductCodes.count)
                                                completion(success, statusDescription);
                                        }];
            else {
                ++proceededCount;
                if (proceededCount == arrTransactionProductCodes.count) {
                    completion(TRUE, @"proceeded");
                    completion = nil; //prevent multiple calls if the subscription already purchased and reactivated, 2 call being performed by Apple
                }
            }
        }
   
  



//    [SubscriptionM getSubscriptionsData:arrIds delegate:self completion:^(NSArray *arrSubscriptions) {
//    if (isLocalReceiptValidation) {
//        [LocalReceiptValidation validateReceiptWithCompletion:^(BOOL success, SCPStoreKitIAPReceipt *receiptLatestInApp) {
//            if (success) {
//                params.originalTransactionIdentifier = receiptLatestInApp.originalTransactionIdentifier;
//                [SubscriptionM chargeUserWithInApp:params completion:^(BOOL success, NSString *statusDescription) {
//                    completion (success, statusDescription);
//                }];
//            }
//            else
//                completion (FALSE, nil);
//        }];
//    }
//    else {
//        [RemoteReceiptValidation validateReceiptWithProduct:product completion:^(BOOL success, NSDictionary *receiptInfo) {
//            if (success) {
//                params.originalTransactionIdentifier = receiptInfo[@"original_transaction_id"];
//                [SubscriptionM chargeUserWithInApp:params completion:^(BOOL success, NSString *statusDescription) {
//                    completion (success, statusDescription);
//                }];
//            }
//            else
//                completion (FALSE, nil);
//        }];
//    }
}

#pragma mark - SKProduct local cache
- (void) setCacheForSKProduct:(SKProduct*) product {
    id obj = [self getCachedSKProductForIdentifier:product.productIdentifier];
    if (!obj)
        [_arrSKProducts addObject:product];
}

- (SKProduct*) getCachedSKProductForIdentifier:(NSString*) productIdentifier {
    return [[_arrSKProducts filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productIdentifier == %@", productIdentifier]] firstObject];
}

#pragma mark - PPV -

- (void) purchasePPVMediaFile:(void(^)(PPVPurchaseRequestParams *reqParams))requestParams completion:(void(^)(PPVResponseParams *resParams))responseParams
{
    
#if TARGET_IPHONE_SIMULATOR
    // where is the simulator
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    NSLog(@"AppStore login data path: 'data/Library/com.apple.itunesstored', delete dir to logout");
#endif
    
    //init params
    PPVPurchaseRequestParams *reqParams = [PPVPurchaseRequestParams new];
    PPVResponseParams *resParams = [PPVResponseParams new];
    //fill request params
    requestParams(reqParams);
    
    [self getSKProductForProductCode:reqParams.productCode completion:^(SKProduct *product) {
        if (product)
        {
            [[SCPStoreKitManager sharedInstance] requestPaymentForProduct:product paymentTransactionStatePurchasing:^(NSArray *transactions) {
                //purchasing
            } paymentTransactionStatePurchased:^(NSArray *transactions) {
                //purchased
                NSArray *arrProductCodes = [transactions valueForKeyPath:@"@distinctUnionOfObjects.payment.productIdentifier"];
                [self proceedPPVWithTransaction:arrProductCodes
                                  requestParams:reqParams
                                     completion:^(BOOL success, NSString *statusDescription) {
                                         resParams.success = success;
                                         resParams.statusDescription = statusDescription;
                                         responseParams(resParams);
                                     }];
                
            } paymentTransactionStateFailed:^(NSArray *transactions) {
                //failed
                resParams.success = FALSE;
                resParams.statusDescription = @"SKPaymentTransactionStateFailed";
                resParams.error = (transactions.count) ? [(SKPaymentTransaction*)transactions[0] error] : nil;
                
                //clear SKProducts cache
                [self.arrSKProducts removeAllObjects];
                
                responseParams(resParams);
            } paymentTransactionStateRestored:^(NSArray *transactions) {
                //restored
                [self proceedPPVWithTransaction:[transactions lastObject]
                                  requestParams:reqParams
                                     completion:^(BOOL success, NSString *statusDescription) {
                                         resParams.success = success;
                                         resParams.statusDescription = statusDescription;
                                         responseParams(resParams);
                                     }];
                
            } failure:^(NSError *error) {
                //error
                resParams.success = FALSE;
                resParams.statusDescription = @"SKPaymentTransactionStateFailed";
                resParams.error = error;
                responseParams(resParams);
            }];
        }
        else
        {
            resParams.success = FALSE;
            resParams.statusDescription = @"SKPaymentTransactionStateFailed";
            responseParams(resParams);
        }
    }];
}

- (void) proceedPPVWithTransaction:(NSArray *)arrTransactionProductCodes
                  requestParams:(PPVRequestParams*) reqParams
                     completion:(void(^)(BOOL success, NSString *statusDescription))completion {
    
    if (reqParams.ppvData)
    {
        [self proceedPPVWithTransactionStep2:arrTransactionProductCodes requestParams:reqParams availableSubscriptions:nil completion:completion];
    }
    else
    {
        completion (FALSE, LS(@"item_page_buy_subscription_error_description"));
    }
}

- (void) proceedPPVWithTransactionStep2:(NSArray *)arrTransactionProductCodes
                       requestParams:(PPVRequestParams*) reqParams
              availableSubscriptions:(NSArray *)arrAvailableSubscriptions
                          completion:(void(^)(BOOL success, NSString *statusDescription))completion {
    
    __block NSInteger proceededCount = 0;
    
    
    for (NSString *productCode in arrTransactionProductCodes)
    {
        NSPredicate *predicateSubscription = [NSPredicate predicateWithBlock:^BOOL(TVSubscriptionData *sub, NSDictionary *bindings) {
            return ([sub.productCode isEqualToString:productCode]);
        }];
        PPVChargeUserWithInAppRequestParams *requestParams = [PPVChargeUserWithInAppRequestParams new];
        requestParams.productCode = productCode;
        requestParams.delegate = reqParams.delegate;
        requestParams.ppvData = (reqParams.ppvData) ?: [[arrAvailableSubscriptions filteredArrayUsingPredicate:predicateSubscription] firstObject];
        requestParams.receipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
        
        if (requestParams.ppvData && completion)
            [PPVM chargeUserWithInApp:requestParams
                                    completion:^(BOOL success, NSString *statusDescription) {
                                        ++proceededCount;
                                        if (proceededCount == arrTransactionProductCodes.count)
                                            completion(success, statusDescription);
                                    }];
        else
        {
            ++proceededCount;
            if (proceededCount == arrTransactionProductCodes.count) {
                completion(TRUE, @"proceeded");
                completion = nil; //prevent multiple calls if the subscription already purchased and reactivated, 2 call being performed by Apple
            }
        }
    }

}

@end
