//
//  IAPManager.h
//  KalturaUnified
//
//  Created by Alex Zchut on 2/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseManagement.h"
#import "SubscriptionPurchaseParams.h"
#import "PPVPurchaseParams.h"
#import "TVRental+Additions.h"

@interface IAPManager : BaseManagement

+ (IAPManager *)instance;
- (void) purchaseSubscription:(void(^)(SubscriptionPurchaseRequestParams *reqParams))requestParams completion:(void(^)(SubscriptionResponseParams *resParams))responseParams;

- (void) purchasePPVMediaFile:(void(^)(PPVPurchaseRequestParams *reqParams))requestParams completion:(void(^)(PPVResponseParams *resParams))responseParams;

- (void) restoreSubscriptions:(void(^)(SubscriptionRequestParams *reqParams))requestParams completion:(void(^)(SubscriptionResponseParams *resParams))responseParams;
- (void) getSKProductsForProductCodes:(NSArray*) arrProductCodes completion:(void(^)(NSArray *arrSKProducts)) completion;
- (void) getSKProductForProductCode:(NSString*) productCode completion:(void(^)(SKProduct *product)) completion;
@end
