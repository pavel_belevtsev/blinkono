//
//  LocalReceiptValidation.m
//  KalturaUnified
//
//  Created by Alex Zchut on 2/17/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "LocalReceiptValidation.h"

@implementation LocalReceiptValidation

//+ (void) validateReceiptWithCompletion:(void(^)(BOOL success, SCPStoreKitIAPReceipt *receiptLatestInApp))completion {
//    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
//    NSString *bundleVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
//    [[SCPStoreKitReceiptValidator sharedInstance] validateReceiptWithBundleIdentifier:bundleIdentifier
//                                                                        bundleVersion:bundleVersion
//                                                                             tryAgain:NO
//                                                                     showReceiptAlert:NO
//                                                                       alertViewTitle:nil
//                                                                     alertViewMessage:nil
//      success:^(SCPStoreKitReceipt *receipt) {
//          NSUInteger index = [receipt.inAppPurchases indexOfObjectPassingTest:^BOOL(SCPStoreKitIAPReceipt *obj, NSUInteger idx, BOOL *stop) {
//              if ([obj.subscriptionExpiryDate timeIntervalSince1970] > [[self getLocalGMTdate] timeIntervalSince1970]) {
//                  *stop = YES;
//                  return YES;
//              }
//              return NO;
//          }];
//          if (index != NSNotFound)
//              completion(TRUE, receipt.inAppPurchases[index]);
//          else
//              completion(FALSE, nil);
//          
//      } failure:^(NSError *error) {
//          completion(FALSE, nil);
//      }];
//}
//
//+ (NSDate*)getLocalGMTdate
//{
//    NSDate *localDate = [NSDate date];
//    NSTimeInterval timeZoneOffset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
//    NSTimeInterval gmtTimeInterval = [localDate timeIntervalSinceReferenceDate] - timeZoneOffset;
//    return [NSDate dateWithTimeIntervalSinceReferenceDate:gmtTimeInterval];
//}
@end
