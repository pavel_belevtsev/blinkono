//
//  RemoteReceiptValidation.h
//  KalturaUnified
//
//  Created by Alex Zchut on 2/17/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SKProduct;
@interface RemoteReceiptValidation : NSObject

+ (void) validateReceiptWithProduct:(SKProduct*) product completion:(void(^)(BOOL success, NSDictionary *receiptInfo))completion;

@end

