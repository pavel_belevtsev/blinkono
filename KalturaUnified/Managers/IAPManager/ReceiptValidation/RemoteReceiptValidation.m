//
//  RemoteReceiptValidation.m
//  KalturaUnified
//
//  Created by Alex Zchut on 2/17/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "RemoteReceiptValidation.h"

#define urlSandboxServer  [NSURL URLWithString:@"https://sandbox.itunes.apple.com/verifyReceipt"]
#define urlLiveServer     [NSURL URLWithString:@"https://buy.itunes.apple.com/verifyReceipt"]

@implementation RemoteReceiptValidation

+ (void) validateReceiptWithProduct:(SKProduct*) product completion:(void(^)(BOOL success, NSDictionary *receiptInfo))completion {
    
    NSData *receiptData = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
    NSError *error;
    NSMutableDictionary *requestContents = [NSMutableDictionary dictionaryWithObject:
                                            [receiptData base64EncodedStringWithOptions:0] forKey:@"receipt-data"];
    requestContents[@"password"] = @"";
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents options:0 error:&error];

#ifdef DEBUG
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:urlSandboxServer];
#else
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:urlLiveServer];
#endif
    
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:storeRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            NSInteger status = [jsonResponse[@"status"] integerValue];
            //NSString *originalAppVersion = jsonResponse[@"receipt"][@"original_application_version"];
            
            if (status != 0) {
                completion(FALSE, nil);
            }
            else {
                NSArray *arrInAppReceipts = jsonResponse[@"receipt"][@"in_app"];
                NSUInteger index = [arrInAppReceipts indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
                    if ([obj[@"product_id"] isEqualToString:product.productIdentifier] && [obj[@"expires_date_ms"] doubleValue] > ([[NSDate date] timeIntervalSince1970]*1000)) {
                        *stop = YES;
                        return YES;
                    }
                    return NO;
                }];
                if (index != NSNotFound)
                    completion(TRUE, arrInAppReceipts[index]);
                else
                    completion(FALSE, nil);
            }
        } else {
            completion(FALSE, nil);
        }
    }] resume];
}

@end

