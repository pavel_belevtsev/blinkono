//
//  DeepLinkingAction.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.02.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeepLinkingAction : NSObject

typedef NS_ENUM(NSInteger, DeepLinkingType) {
    DeepLinkingTypeCorrupted = -1,
    DeepLinkingTypeHome,
    DeepLinkingTypeLiveTV,
    DeepLinkingTypeEPG,
    DeepLinkingTypeMyZone,
    DeepLinkingTypeOnlinePurchases,
    DeepLinkingTypeDownloads,
    DeepLinkingTypeRecordings,
    DeepLinkingTypeCategory,
    DeepLinkingTypeMedia,
};

@property DeepLinkingType deepLinkingType;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *mediaId;

@end
