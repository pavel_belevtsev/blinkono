//
//  SubscriptionManager.h
//  KalturaUnified
//
//  Created by Alex Zchut on 2/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVMediaItem+PSTags.h"

@interface SubscriptionManager : BaseNetworkObject

+ (SubscriptionManager *)instance;

- (void) getSubscriptionsContainingMediaItem:(TVMediaItem *)mediaItem delegate:(BaseViewController *)delegateController completion: (void (^)(NSArray *arrSubscriptionsPurchased, NSArray *arrSubscriptionsToPurchase)) completion;
- (void) getSubscriptionsData:(NSArray*)arrSubscriptionIds delegate:(BaseViewController *)delegateController completion:(void(^)(NSArray *arrSubscriptions))completion;
- (void) getSubscriptionsData:(NSArray*)arrSubscriptionIds delegate:(BaseViewController *)delegateController needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptions))completion;
- (void) getSubscriptionsData:(NSArray*)arrSubscriptionIds delegate:(BaseViewController *)delegateController needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptions))completion withInApp:(BOOL) withInApp;
- (void) getMediasForExpiredSubscriptions:(BaseViewController *)delegateController isForUser:(BOOL) isForUser renewal:(BOOL)isRenewal completion:(void(^)(NSMutableArray *arrMedias))completion;
- (void) getMediasForPermittedSubscriptions:(BaseViewController *)delegateController isForUser:(BOOL) isForUser renewal:(BOOL)isRenewal completion:(void(^)(NSMutableArray *arrMedias))completion;

- (void) chargeUserWithInApp:(SubscriptionChargeUserWithInAppRequestParams *)params completion: (void (^)(BOOL success, NSString* statusDescription)) completion;

- (void) getAllSubscriptionDataFromVirtualMedias:(BaseViewController *)delegateController needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptionsData))completion;
- (void) getAllSubscriptionDataFromVirtualMedias:(BaseViewController *)delegateController appendIDs:(NSArray*) arrSubIDsToAppend needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptionsData))completion;

- (void) getAllSubscriptionDataFromVirtualMedias:(BaseViewController *)delegateController needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptionsData))completion withInApp:(BOOL) withInApp;
- (void) getAllSubscriptionDataFromVirtualMedias:(BaseViewController *)delegateController appendIDs:(NSArray*) arrSubIDsToAppend needPurchasingInfo:(BOOL)needPurchasingInfo completion:(void(^)(NSArray *arrSubscriptionsData))completion withInApp:(BOOL) withInApp ;

- (void) cancelSubscription:(NSInteger) subscriptionCode transaction:(NSInteger) transactionPurchaseId delegate:(BaseViewController *)delegateController completion:(void(^)(BOOL result))completion;
- (NSString*) getNRSubscriptionLeftPeriod:(TVSubscriptionData*) data;

@end