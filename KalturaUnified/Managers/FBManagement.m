//
//  FBManagement.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "FBManagement.h"
#import "AppDelegate.h"

@implementation FBManagement

+ (FBManagement *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (void)requestForFBConfigWithDelegate:(void(^)(BOOL success))completionBlock {
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForFBConfigWithDelegate:nil];
    
    [request setFailedBlock:^{
        
        completionBlock(FALSE);
        
    }];
    
    [request setCompletionBlock:^{
        
        LoginM.facebookScope = [request JSONResponse];
        [LoginM seperatePermissions:[[LoginM.facebookScope objectForKey:@"scope"] componentsSeparatedByString:@","]];

        completionBlock(TRUE);
    }];
    
    [self sendRequest:request];
}

- (void)requestForGetFBUserDataWithToken:(NSString *)token completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForGetFBUserDataWithToken:token delegate:nil];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
        
    }];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    
    [self sendRequest:request];
}

- (void)requestForFBUserRegisterWithToken:(NSString*)token createNewDomain:(BOOL)createNewDomain completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForFBUserRegisterWithToken:token createNewDomain:createNewDomain delegate:nil];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
        
    }];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [self sendRequest:request];
}

- (void)requestForFBUserRegisterWithToken:(NSString*)token completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForFBUserRegisterWithToken:token createNewDomain:NO delegate:nil];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
        
    }];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [self sendRequest:request];
}


- (void)requestForGetFBUserDataWithToken:(void(^)(TVFacebookUser *user))completion {
    NSString *facebookToken = [[FBSession.activeSession accessTokenData] accessToken];
    
    [self requestForGetFBUserDataWithToken:facebookToken completionBlock:^(id jsonResponse) {
        if (jsonResponse) {
            
            completion([[TVFacebookUser alloc] initWithDictionary:[jsonResponse dictionaryByRemovingNSNulls]]);
            
        } else {            
            completion(nil);
        }
    }];
}

@end
