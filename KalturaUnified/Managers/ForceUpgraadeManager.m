//
//  ForceUpgraadeManager.m
//  KalturaUnified
//
//  Created by Rivka Schwartz on 3/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "ForceUpgraadeManager.h"

@implementation ForceUpgraadeManager


+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    
    dispatch_once(&once, ^
                  {
                      sharedInstance = [self new];
                  });
    
    return sharedInstance;
}

-(BOOL) isApplicationContinuencePermitted
{
    NSString * isForceUpdate = [[[TVConfigurationManager sharedTVConfigurationManager] dictionaryVersionInformation] objectForKey:TVconfigVersionInformationIsforceupdateKey];
    return !(isForceUpdate.integerValue == 1);
}


-(void) handleForceDownload
{
    [self showForceDownloadAlert];
}


-(void) showForceDownloadAlert
{
    NSString * title = @"upgrade text";
    NSString * content = @"upgrade content";
    
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:title message:content delegate:self cancelButtonTitle:@"עדכון" otherButtonTitles:nil];
    [alert show];
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString * deviceKey = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?TVconfigForcedUpdateURLiPadKey:TVconfigForcedUpdateURLiPhoneKey;
    NSString * linkString = [[[TVConfigurationManager sharedTVConfigurationManager] dictionaryForceDownloadLinks] objectForKey:deviceKey];
    NSURL * url = [NSURL URLWithString:linkString];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];;
    }
    
    [self showForceDownloadAlert];
    
}


@end
