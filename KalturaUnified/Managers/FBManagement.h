//
//  FBManagement.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseManagement.h"
#import "MyTokenCachingStrategy.h"

@interface FBManagement : BaseManagement

+ (FBManagement *)instance;

- (void)requestForGetFBUserDataWithToken:(void(^)(TVFacebookUser *user))completion;
- (void)requestForFBConfigWithDelegate:(void(^)(BOOL success))completionBlock;
- (void)requestForFBUserRegisterWithToken:(NSString*)token createNewDomain:(BOOL)createNewDomain completionBlock:(void(^)(id jsonResponse))completionBlock;
- (void)requestForFBUserRegisterWithToken:(NSString*)token completionBlock:(void(^)(id jsonResponse))completionBlock;

@end
