//
//  NavigationManager.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const KAuthenticationCompletedNotification;
extern NSString * const KMenuItemsUpdatedNotification;
extern NSString * const KLogoutNotification;
extern NSString * const KLoginNotification;
extern NSString * const KRenameNotification;
extern NSString * const KFbNotification;
extern NSString * const KMenuItemsUpdatedErrorNotification;
extern NSString * const KDeepLinkingActionNotification;

@class PlayerViewController;
@class MFSideMenuContainerViewController;
@interface NavigationManager : NSObject

+ (NavigationManager *)instance;

@property (nonatomic, strong) MFSideMenuContainerViewController *container;
@property (nonatomic, strong) PlayerViewController *playerViewController;

- (void)setPhonePortrait;

@end
