//
//  BaseManagement.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseManagement : NSObject

- (void)sendRequest:(ASIHTTPRequest *)request;

@property (nonatomic, readwrite, strong) TVNetworkQueue *networkQueue;

@end
