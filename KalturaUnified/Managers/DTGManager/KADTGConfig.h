//
//  KADTGConfig.h
//  KalturaUnified
//
//  Created by Noam Tamim on 6/10/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KADTGConfig : NSObject
+(NSInteger)getProviderName;
@end
