//
//  KADownloadManager.swift
//  KADownloadManager
//
//  Created by Alex Zchut on 4/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import Foundation
import UIKit

@objc public enum DownloadManagersType: NSInteger {
    case Simple = 0
    case M3U8
    case WVM
    
    var description : String {
        var str : String
        switch self {
            case .Simple:
                str = NSStringFromClass(KADownloadManagerInstanceSimple);
                break
            case .M3U8:
                str = NSStringFromClass(KADownloadManagerInstanceM3U8);
                break
            case .WVM:
                str = NSStringFromClass(KADownloadManagerInstanceWVM);
                break
        }
        return str
    }
}

//MARK: - iOS version
private let iosVersion = NSString(string: Device.systemVersion).doubleValue
let Device = UIDevice.currentDevice()
let iOS9 = iosVersion >= 9
let iOS8 = iosVersion >= 8
let iOS7 = iosVersion >= 7 && iosVersion < 8
let hostIP = "127.0.0.1"
let hostScheme = "http"
let hostPort = 8080
let managerScheme : String = "com.kaltura.KADownloadManager"
let baseDestination = NSHomeDirectory().stringByAppendingPathComponent("Library/Caches/\(managerScheme)")
let fileDestination = baseDestination.stringByAppendingPathComponent("Items")
let fileDestinationUrl = "/Library/Caches/\(managerScheme)/Items"

//MARK: -
public class KADownloadManager: NSObject, KADownloadManagerInstanceDelegate {
    
    static var instance: KADownloadManager!
    
    //MARK: pivate variables
    private var activeDownloader    : KADownloadManagerInstanceBase?
    private var localDB : KALocalDB = KALocalDB()
    
    //MARK: public variables
    var settings : KADownloadSettings?
    var items : NSMutableArray = NSMutableArray()

    //MARK: constractor
    override init() {
        super.init()
        if !NSFileManager.defaultManager().fileExistsAtPath(fileDestination) {
            NSFileManager.defaultManager().createDirectoryAtPath(fileDestination, withIntermediateDirectories: true, attributes: nil, error: nil)
        }
        KAFileSavingUtility.addSkipBackupAttributeToItemAtURL(fileDestination)
        self.settings = KADownloadSettings(db: self.localDB)
        self.setAllActiveNotCompletedDownloadsToInterruptedState()
        
        println("---> baseDestination: \(baseDestination)")
    }
    
    public func setDownloadManagerType(managerType: DownloadManagersType) {
        var anyobjectype : AnyClass = NSClassFromString(managerType.description)
        var nsobjectype : NSObject.Type = anyobjectype as! NSObject.Type
        activeDownloader = nsobjectype() as? KADownloadManagerInstanceBase
        activeDownloader?.delegate = self
    }
    
    //MARK: - Public functions
    public class func sharedInstance() -> KADownloadManager {
        self.instance = (self.instance ?? KADownloadManager())
        return self.instance
    }
    
    func addDownloadItem(item: KADownloadItem) {
        self.activeDownloader?.addDownloadItem(item)
    }
    
    func interruptActiveDownloadTasks() {
        activeDownloader?.interruptActiveDownloadTasks()
    }
    
    func resumeInterruptedDownloadTasks() {
        self.canStartDownload { (success, falseReason) -> Void in
            if success {
                self.activeDownloader?.resumeInterruptedDownloadTasks()
            }
        }
    }
    
    func setAllActiveNotCompletedDownloadsToInterruptedState() {
        self.localDB.getAllNotCompletedItems { (items) -> Void in
            if items.count > 0 {
                
                var ids : NSArray? = items.valueForKeyPath("ID") as? NSArray
                if let arr = ids {
                    var arrm : NSMutableArray = NSMutableArray(array: arr)
                    for i in 0..<arr.count {
                        arrm[i] = "'\(arrm[i])'"
                    }
                    self.localDB.updateItemsState(arrm, state: KADownloadItemDownloadState.Interrupted, completion: { (success) -> Void in
                        //OK
                    })
                }
            }
        }
    }
    
    //MARK: - KADownloadManagerDelegate functions
    internal func downloadStarted(itemId: String, arrDownloadItemUrls: NSMutableArray, completion: (success: Bool) -> Void) {
        self.localDB.insertItemUrls(itemId, arrDownloadItemUrls: arrDownloadItemUrls, completion: { (success) -> Void in
            self.checkQuotaLimit(itemId, completion: { (success, falseReason) -> Void in
                if success {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusStarted_".stringByAppendingString(itemId), object: nil)
                    })
                    completion(success: Bool(true))
                }
                else {
                    self.localDB.deleteItem(itemId, completion: { (success) -> Void in
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusInterrupted_".stringByAppendingString(itemId), object: nil, userInfo: ["progress": NSNumber(double: 0), "reason": NSNumber(int: falseReason.rawValue)])
                        })
                        completion(success: Bool(false))
                    })
                }
            })
        })
    }
    
    internal func downloadWillStart(itemId: String) {
        self.localDB.updateItemState(itemId, state: KADownloadItemDownloadState.InProgress) { (success) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusWillStart_".stringByAppendingString(itemId), object: nil)
            })
        }
    }

    internal func downloadCanceled(itemId: String) {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusCancelled_".stringByAppendingString(itemId), object: nil)
        })
    }
    
    internal func downloadInterrupted(itemId: String, arrRelatedItemUrls : NSArray) {
        self.downloadInterrupted(itemId, arrRelatedItemUrls: arrRelatedItemUrls, reason: KADownloadItemInterruptionReason.Connectivity)
    }
    
    internal func downloadInterrupted(itemId: String, arrRelatedItemUrls : NSArray, reason: KADownloadItemInterruptionReason) {
        self.localDB.updateItemState(itemId, state: KADownloadItemDownloadState.Interrupted) { (success) -> Void in
            self.localDB.updateItemUrlsStatus(arrRelatedItemUrls, completion: { (success) -> Void in
                self.localDB.getItemProgress(itemId) { (progress, notCompleted) -> Void in
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusInterrupted_".stringByAppendingString(itemId), object: nil, userInfo: ["progress": NSNumber(double: progress), "reason": NSNumber(int: reason.rawValue)])
                    })
                }
            })
        }
    }
    
    internal func downloadPaused(itemId: String, arrRelatedItemUrls : NSArray) {
        self.localDB.updateItemState(itemId, state: KADownloadItemDownloadState.Paused) { (success) -> Void in
            self.localDB.updateItemUrlsStatus(arrRelatedItemUrls, completion: { (success) -> Void in
                self.localDB.getItemProgress(itemId) { (progress, notCompleted) -> Void in
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusPaused_".stringByAppendingString(itemId), object: nil, userInfo: ["progress": NSNumber(double: progress)])
                    })
                }
            })
        }
    }
    
    internal func downloadResumed(itemId: String) {
        self.localDB.updateItemState(itemId, state: KADownloadItemDownloadState.InProgress) { (success) -> Void in
            
            self.checkQuotaLimit(itemId, completion: { (success, falseReason) -> Void in
                if success {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusResumed_".stringByAppendingString(itemId), object: nil)
                    })
                }
                else {
                    self.downloadInterrupted(itemId, arrRelatedItemUrls: [], reason: falseReason)
                }
            })
        }
    }
    
    internal func downloadFinished(itemId: String) {
        self.localDB.updateItemState(itemId, state: KADownloadItemDownloadState.Downloaded) { (success) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusDownloaded_".stringByAppendingString(itemId), object: nil)
            })
        }
        self.resumeInterruptedDownloadTasks()
    }
    
    internal func downloadFinishedForItemUrl(itemUrl: KADownloadItemUrl, completion: (success: Bool) -> Void) {
        itemUrl.expected = itemUrl.written
        self.localDB.updateItemUrlStatus(itemUrl, completion: { (success) -> Void in
            completion(success: success)
        })
    }
    
    internal func downloadFinishedWithFailure(itemId: String, error: NSError) {
        self.localDB.updateItemRetry(itemId, completion: { (retriesCount : Int32) -> Void in
            self.settings!.getSettingsKey(KADownloadSettings.maxRetriesCountAllowed, completion: { (retriesCountAllowed : String) -> Void in
                var retriesAllowed : Int32 = NSString(string: retriesCountAllowed).intValue
                if retriesAllowed > 0 && retriesCount < retriesAllowed {
                    self.getDownloadItemById(itemId, completion: { (item) -> Void in
                        self.resumeDownloadItem(item)
                    })
                }
                else {
                    self.localDB.updateItemState(itemId, state: KADownloadItemDownloadState.Failed) { (success) -> Void in
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusFailed_".stringByAppendingString(itemId), object: nil, userInfo: ["error": error])
                        })
                    }
                }
            })
        })
    }
    
    internal func downloadInProgress(itemId: String, progress: Double) {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusInProgress_".stringByAppendingString(itemId), object: nil, userInfo: ["progress": NSNumber(double: progress)])
        })
    }
    
    internal func getDownloadItemState(itemId: String, completion: (state: KADownloadItemDownloadState) -> Void) {
        self.localDB.getItemState(itemId, completion: { (state) -> Void in
            completion(state: state)
        })
    }
    
    internal func getDownloadItemUrl(url: String, completion: (obj: KADownloadItemUrl?) -> Void) {
        self.localDB.getItemUrl(url, completion: { (obj) -> Void in
            completion(obj: obj)
        })
    }
    
    internal func getDownloadItemUrls(itemId: String, completion: (arrNotCompleted : NSMutableArray, arrCompleted : NSMutableArray) -> Void) {
        self.localDB.getItemUrls(itemId, completion: { (arrNotCompleted, arrCompleted) -> Void in
            completion(arrNotCompleted: arrNotCompleted, arrCompleted: arrCompleted)
        })
    }
    
    func getFirstDownloadItemForState(state: KADownloadItemDownloadState ,completion: (item: KADownloadItem?) -> Void) {
        self.localDB.getFirstItemForState(state, completion: { (item) -> Void in
            completion(item: item)
        })
    }
    
    func getSettingsKeys(completion: (dict: NSDictionary) -> Void) {
        self.settings?.getAllSettings({ (dict) -> Void in
            completion(dict: dict)
        })
    }
    
    func getDownloadItemById(itemId: String, completion: (item: KADownloadItem) -> Void) {
        if let item = self.getItemFromItemsArray(itemId) { //has item in items array
            completion(item: item)
        }
        else {
            self.localDB.getItem(itemId, completion: { (itemData: NSDictionary) -> Void in
                if itemData.count > 0 { //has item in db
                    var item : KADownloadItem = KADownloadItem(itemId: itemData["itemId"] as! String, itemUrl: itemData["itemUrl"] as! String, itemFormat: itemData["itemFormat"] as! String)
                    item.downloadState = KADownloadItemDownloadState(rawValue: itemData["state"]!.intValue)!
                    item.updateDate = itemData["updateDate"]!.doubleValue
                    item.firstWatchDate = itemData["firstWatchDate"]!.doubleValue
                    item.hasWatchRestriction = itemData["hasWatchRestriction"]!.boolValue

                    item.DBID = itemData["DBID"]!.intValue
                    if let mediaMark: AnyObject = itemData["mediaMark"] {
                        item.mediaMark = KADownloadItem_mediaMark(mediaMarkInfo: mediaMark as! NSDictionary)
                    }
                    self.localDB.getItemStreams(item, completion: { (success) -> Void in
                        self.localDB.getItemProgress(itemId, completion: { (progress, notCompleted) -> Void in
                            item.currentProgress = progress
                            self.items.addObject(item)
                            completion(item: item)
                        })
                    })
                }
            })
        }
    }
    
    internal func downloadFinishedEventsForBackgroundURLSession(session: NSURLSession) {
        var appDelegate : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if let isBackgroundCompletionHandler = appDelegate.backgroundSessionCompletionHandler {
            var completionHandler = appDelegate.backgroundSessionCompletionHandler
            appDelegate.backgroundSessionCompletionHandler = nil
            completionHandler!()
        }
    }
    
    //MARK: - Items
    func deleteItem(itemId: String, completion: (success: Bool) -> Void) {
        self.localDB.deleteItem(itemId, completion: { (success) -> Void in
            //remove item directory after removing from db
            KAFileSavingUtility.removeFolderAtPath(fileDestination.stringByAppendingPathComponent(itemId))
            if let item = self.getItemFromItemsArray(itemId) { //has item in items array
                self.items.removeObject(item)
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC))), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
                self.cancelDownloadItem(itemId)
            }
            completion(success: success)
        })
    }
    
    func deleteAllItems(completion: (success: Bool) -> Void) {
        self.localDB.deleteAllItems({ (success) -> Void in
            //remove items directories after removing from db
            self.items.removeAllObjects()
            KAFileSavingUtility.removeFoldersAtPath(fileDestination)
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC))), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
                self.cancelAllDownloads()
            }
            completion(success: success)

        })
    }
    
    func getItem(itemId: String, itemUrl: String, itemFormat: String, completion: (item: KADownloadItem) -> Void) {
        if let item = self.getItemFromItemsArray(itemId) { //has item in items array
            completion(item: item)
        }
        else {
            self.localDB.getItem(itemId, completion: { (itemData: NSDictionary) -> Void in
                if itemData.count > 0 { //has item in db
                    var item : KADownloadItem = KADownloadItem(itemId: itemData["itemId"] as! String, itemUrl: itemData["itemUrl"] as! String, itemFormat: itemData["itemFormat"] as! String)
                    item.downloadState = KADownloadItemDownloadState(rawValue: itemData["state"]!.intValue)!
                    item.updateDate = itemData["updateDate"]!.doubleValue;
                    item.firstWatchDate = itemData["firstWatchDate"]!.doubleValue;
                    item.hasWatchRestriction = itemData["hasWatchRestriction"]!.boolValue
                    item.DBID = itemData["DBID"]!.intValue
                    if let mediaMark: AnyObject = itemData["mediaMark"] {
                        item.mediaMark = KADownloadItem_mediaMark(mediaMarkInfo: mediaMark as! NSDictionary)
                    }
                    self.localDB.getItemStreams(item, completion: { (success) -> Void in
                        self.localDB.getItemProgress(itemId, completion: { (progress, notCompleted) -> Void in
                            item.currentProgress = progress
                            self.items.addObject(item)
                            completion(item: item)
                        })
                    })
                }
                else {
                    var newItem : KADownloadItem = KADownloadItem(itemId: itemId, itemUrl: itemUrl, itemFormat: itemFormat)
                    if newItem.url != "" {
                        self.items.addObject(newItem)
                    }
                    completion(item: newItem)
                }
            })
        }

    }
    
    func getAllItems(completion: (items: NSMutableArray) -> Void) {
        self.localDB.getAllItems { (itemsData: NSMutableArray) -> Void in
            for itemData in itemsData {
                var itemId : String = itemData["itemId"] as! String
                var itemUrl : String = itemData["itemUrl"]  as! String
                var itemFormat : String = itemData["itemFormat"]  as! String
                var updateDate : Double = itemData["updateDate"]!.doubleValue
                var firstWatchDate : Double = itemData["firstWatchDate"]!.doubleValue
                var hasWatchRestriction : Bool = itemData["hasWatchRestriction"]!.boolValue
                var DBID : Int32 = itemData["DBID"]!.intValue
                var state: NSNumber = itemData["state"] as! NSNumber
                var mediaMark : NSDictionary? = itemData["mediaMark"] as? NSDictionary
                
                if let item = self.getItemFromItemsArray(itemId) { //has item in items array
                    item.DBID = DBID;
                    item.url = itemUrl
                    item.downloadState = KADownloadItemDownloadState(rawValue: state.intValue)!
                    item.updateDate = updateDate;
                    item.firstWatchDate = firstWatchDate;
                    if let mediaMark = mediaMark {
                        item.mediaMark = KADownloadItem_mediaMark(mediaMarkInfo: mediaMark as NSDictionary)
                    }
                }
                else {
                    var item : KADownloadItem = KADownloadItem(itemId: itemId, itemUrl: itemUrl, itemFormat: itemFormat)
                    item.downloadState = KADownloadItemDownloadState(rawValue: state.intValue)!
                    item.updateDate = updateDate;
                    item.firstWatchDate = firstWatchDate
                    item.hasWatchRestriction = hasWatchRestriction
                    item.DBID = DBID;
                    if let mediaMark = mediaMark {
                        item.mediaMark = KADownloadItem_mediaMark(mediaMarkInfo: mediaMark as NSDictionary)
                    }
                    self.localDB.getItemStreams(item, completion: { (success) -> Void in
                        self.localDB.getItemProgress(itemId, completion: { (progress, notCompleted) -> Void in
                            item.currentProgress = progress
                            self.items.addObject(item)
                        })
                    })
                }
            }
            if itemsData.count == 0 {
                self.items.removeAllObjects()
            }
            completion(items: self.items)
        }
    }

    //MARK: - KADownloadItem functions
    func updateItemFirstWatchDate(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.localDB.updateItemFirstWatchDate(item, completion: completion)
    }
    func updateItemState(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.localDB.updateItemState(item, completion: completion)
    }
    
    func updateSelectedVideoStreams(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.localDB.updateSelectedVideoStreams(item, completion: completion)
    }
    
    func updateSelectedAudioStreams(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.localDB.updateSelectedAudioStreams(item, completion: completion)
    }
    
    func updateSelectedSubtitles(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.localDB.updateSelectedSubtitles(item, completion: completion)
    }
    
    func loadItemInfo(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.canStartDownloadItem(0, notCompleted: 0) { (success: Bool, progress, falseReason: KADownloadItemInterruptionReason) -> Void in
            if success {
                self.activeDownloader?.loadItemInfo(item, completion: { (updatedItem, success) -> Void in
                    if success {
                        self.localDB.deleteItem(item.ID, completion: { (success) -> Void in
                            self.localDB.addItem(item, completion: { (success) -> Void in
                                completion(success: success)
                            })
                        })
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusFailed_".stringByAppendingString(item.ID), object: nil, userInfo: ["noerror": ""])
                        })
                        completion(success: Bool(false))
                    }
                })
            }
            else {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusInterrupted_".stringByAppendingString(item.ID), object: nil, userInfo: ["progress": NSNumber(double: progress), "reason": NSNumber(int: falseReason.rawValue)])
                })
                completion(success: Bool(false))
            }
        }
    }
    
    func resumeDownloadItem(item: KADownloadItem) {
        self.checkQuotaLimit(item.ID, completionWithProgress: { (success, progress, falseReason) -> Void in
            if success {
                self.activeDownloader?.resumeDownloadItem(item);
            }
            else {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    NSNotificationCenter.defaultCenter().postNotificationName("NotificationDownloadStatusInterrupted_".stringByAppendingString(item.ID), object: nil, userInfo: ["progress": NSNumber(double: progress), "reason": NSNumber(int: falseReason.rawValue)])
                })
            }
        })
    }
    
    func pauseDownloadItem(item: KADownloadItem) {
        self.activeDownloader?.pauseDownloadItem(item);
    }
    
    func cancelDownloadItem(itemId: String) {
        self.activeDownloader?.cancelDownloadItem(itemId);
    }

    func cancelAllDownloads() {
        self.activeDownloader?.cancelAllDownloadItems();
    }
    
    func updateDownloadedItemLicense(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.activeDownloader?.updateDownloadedItemLicense(item, completion: { (success) -> Void in
            completion(success: success)
        })
    }
    
    func isDownloadedItemLicenseValid(item: KADownloadItem, completion: (success: Bool, remainingTime: NSTimeInterval) -> Void) {
        if let downloader = self.activeDownloader {
            return downloader.isDownloadedItemLicenseValid(item, completion: completion)
        }
        else {
            completion(success: Bool(false), remainingTime: -1)
        }
    }
    
    func updateItemWatchRestriction(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.localDB.updateItemWatchRestriction(item, completion: { (success) -> Void in
            completion(success: success)
        })
    }
    
    func updateItemMediaMark(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.localDB.updateItemMediaMark(item, completion: { (success) -> Void in
            completion(success: success)
        })
    }
    
    func updateItemWatchStatus(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.localDB.updateItemWatchStatus(item, completion: { (success) -> Void in
            completion(success: success)
        })
    }
    
    // MARK: - Local functions
    func getItemFromItemsArray(itemId: String) -> KADownloadItem? {
        return self.items.filteredArrayUsingPredicate(NSPredicate(format: "self.ID = %@", itemId)).first as? KADownloadItem
    }
    
    
    // MARK: - Quota functions
    func checkQuotaLimit(itemId: String, completion: (success: Bool, falseReason: KADownloadItemInterruptionReason) -> Void) {
        self.checkQuotaLimit(itemId, completionWithProgress: { (success, progress, falseReason) -> Void in
            completion(success: success, falseReason: falseReason)
        })
    }

    func checkQuotaLimit(itemId: String, completionWithProgress: (success: Bool, progress: Double, falseReason: KADownloadItemInterruptionReason) -> Void) {
        self.localDB.getItemProgress(itemId, completion: { (progress, notCompleted : Double) -> Void in
            self.canStartDownloadItem(progress, notCompleted: notCompleted, completion: completionWithProgress)
        })
    }
    
    func canStartDownloadItem (progress: Double, notCompleted : Double, completion: (success: Bool, progress: Double, falseReason: KADownloadItemInterruptionReason) -> Void) {
        self.getCurrentQuotaBasedLibrarySize({ (currentLibrarySize, inProgressDownloadsSize, allowedLibrarySize, freeSpaceOnDevice) -> Void in
            let remainedAllowedLibrarySize : Int64 = allowedLibrarySize - currentLibrarySize - inProgressDownloadsSize
            let expectedForCurrentDownload : Double = notCompleted * 1.05
            if remainedAllowedLibrarySize > 0 {
                if Double(freeSpaceOnDevice) > expectedForCurrentDownload {
                    completion(success: Bool(true), progress: progress, falseReason: KADownloadItemInterruptionReason.Undefined)
                }
                else {
                    completion(success: Bool(false), progress: progress, falseReason: KADownloadItemInterruptionReason.DeviceOutOfFreeSpace)
                }
            }
            else {
                completion(success: Bool(false), progress: progress, falseReason: KADownloadItemInterruptionReason.DownloadQuotaLimit)
            }
        })
    }
    
    func canStartDownload(completion: (success: Bool, falseReason: KADownloadItemInterruptionReason) -> Void) {
        self.canStartDownloadItem(0, notCompleted: 0) { (success, progress, falseReason) -> Void in
            completion(success: success, falseReason: falseReason)
        }
    }
    
    func getCurrentQuotaBasedLibrarySize(completion: (currentLibrarySize: Int64, inProgressDownloadsSize: Int64, allowedLibrarySize: Int64, freeSpaceOnDevice: Int64) -> Void) {
        self.settings!.getSettingsKey(KADownloadSettings.downloadQuota, completion: { (quotaValue : String) -> Void in
            KAFileSavingUtility.getDiskspace({ (total : Int64 , free : Int64) -> Void in
                self.getCurrentInProgressDownloadsNotPlacedInLibraryDirYet({ (currentInProgressSize: Int64) -> Void in
                    
                    var librarySize : Int64 = KAFileSavingUtility.getPathSize(fileDestination)
                    let librarySizeMaxSizeBasedOnQuota : Int64 = Int64(Double(total) * NSString(string: quotaValue).doubleValue / 100.00)
                    if Double(librarySize) / Double(librarySizeMaxSizeBasedOnQuota) < 0.001 {
                        librarySize = 0
                    }

                    completion(currentLibrarySize: librarySize, inProgressDownloadsSize: currentInProgressSize, allowedLibrarySize: librarySizeMaxSizeBasedOnQuota, freeSpaceOnDevice: free)
                })
            })
        })
    }
    
    func getCurrentInProgressDownloadsNotPlacedInLibraryDirYet(completion: (currentInProgressSize: Int64) -> Void) {
        self.localDB.getAllNotCompletedItemsSize { (size) -> Void in
            completion(currentInProgressSize: size)
        }
    }
}