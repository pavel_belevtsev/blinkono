//
//  KADownloadItem_video.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 6/16/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

public class KADownloadItem_video: NSObject {
    var ID: Int32 = 0
    var bandwidth: Int32 = 0
    var programId: Int32 = 0
    var codecs: String = ""
    var resolution: String = ""
    var audio: String = ""
    var video: String = ""
    var subtitles: String = ""
    var closedCaptions: String = ""
    var URI: String = ""
    var fileSavePath: String = ""
    var m3u8UrlStr: String = ""
    var isSelected: Bool = false
    
    override init() {
        super.init()
    }
}