//
//  KADownloadItem_media.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 6/16/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

public class KADownloadItem_media: NSObject {
    var ID: Int32 = 0
    var type: String = ""
    var URI: String = ""
    var fileSavePath: String = ""
    var groupId: String = ""
    var language: String = ""
    var assocLanguage: String = ""
    var name: String = ""
    var instreamId: String = ""
    var characteristics: String = ""
    var m3u8UrlStr: String = ""
    var isDefault: Bool = false
    var autoSelect: Bool = false
    var forced: Bool = false
    var isSelected: Bool = false
    
    override init() {
        super.init()
    }
}