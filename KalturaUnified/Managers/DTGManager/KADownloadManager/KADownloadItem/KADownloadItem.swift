//
//  KADownloadItem.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 5/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

@objc public enum KADownloadItemInterruptionReason: Int32 {
    case Undefined = 1000
    case Connectivity
    case DownloadQuotaLimit
    case DeviceOutOfFreeSpace
}

@objc public enum KADownloadItemDownloadState: Int32 {
    
    case Initial
    case GetInfo
    case WillStart
    case Started
    case InProgress
    case InDownloadQueue
    case Interrupted
    case Paused
    case Resumed
    case Downloaded
    case Cancelled
    case Failed
}

struct DownloadStatistics {
    var totalBytesExpectedToWrite: Int64
    var totalBytesWritten: Int64
    
    func valueAsDict() -> NSDictionary {
        return ["totalBytesExpectedToWrite":NSNumber(longLong: totalBytesExpectedToWrite), "totalBytesWritten":NSNumber(longLong: totalBytesWritten)]
    }
    
    static func initWithDict(dict: NSDictionary) -> DownloadStatistics {
        return DownloadStatistics(totalBytesExpectedToWrite: dict["totalBytesExpectedToWrite"]!.longLongValue, totalBytesWritten: dict["totalBytesWritten"]!.longLongValue)
    }
}

//MARK: - KADownloadManagerInstanceDelegate protocol
@objc protocol KADownloadItemDelegate {
    func downloadWillStart(item: KADownloadItem);
    func downloadStarted(item: KADownloadItem)
    func downloadPaused(item: KADownloadItem);
    func downloadInterrupted(item: KADownloadItem);
    func downloadFailed(item: KADownloadItem, error: NSError?);
    func downloadResumed(item: KADownloadItem);
    func downloadFinished(item: KADownloadItem);
    func downloadInProgress(item: KADownloadItem);
}

@objc public class KADownloadItem: NSObject {

    private let notificationManager = NotificationManager()
    private var delegates = [KADownloadItemDelegate]()
    
    var DBID: Int32 = 0
    var ID : String = ""
    var url : String = ""
    var format : String = ""
    var downloadState : KADownloadItemDownloadState = KADownloadItemDownloadState.Initial
    var videoStreams : [KADownloadItem_video] = []
    var audioStreams : [KADownloadItem_media] = []
    var subtitles : [KADownloadItem_media] = []
    var selectedVideoStreams : [KADownloadItem_video] = []
    var selectedAudioStreams : [KADownloadItem_media] = []
    var selectedSubtitles : [KADownloadItem_media] = []
    var mediaMark :  KADownloadItem_mediaMark?
    var tempMediaMark :  KADownloadItem_mediaMark?
    var currentProgress : Double = 0
    var lastInterruptionReason : KADownloadItemInterruptionReason = KADownloadItemInterruptionReason.Undefined
    var hasWatchRestriction : Bool = Bool(false)
    var updateDate : Double = 0
    var updateDateString :  String {
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .ShortStyle;
        
        return dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: updateDate))
    }
    
    var firstWatchDate : Double = 0
    var wasWatched : Bool {
        get {
            return (firstWatchDate > 0)
        }
        set {
            if (newValue == true) {
                firstWatchDate = NSDate().timeIntervalSince1970
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                    KADownloadManager.sharedInstance().updateItemWatchStatus(self, completion: { (success) -> Void in
                        //OK
                    })
                })
            }
        }
    }
    
    var metadataFolderPath : String {
        var path : String = fileDestination.stringByAppendingPathComponent(self.ID).stringByAppendingPathComponent("metadata")
        
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken, { () -> Void in
            KAFileSavingUtility.createFolderAtPath(path)
        })
        return path
    }
    
    var sizeOnDisk : String  {
        var size : String = ""
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken, { () -> Void in
            size = KAFileSavingUtility.getPathSizeInUnits(fileDestination.stringByAppendingPathComponent(self.ID))
        })
        return size
    }
    
    var contentFolderPath : String {
        var path : String = fileDestination.stringByAppendingPathComponent(self.ID).stringByAppendingPathComponent("content")
        var onceToken : dispatch_once_t = 0;
        dispatch_once(&onceToken, { () -> Void in
            KAFileSavingUtility.createFolderAtPath(path)
        });
        return path
    }
    
    var contentFolderUrlComponents : NSURLComponents {
        var uc : NSURLComponents = NSURLComponents(string: fileDestinationUrl.stringByAppendingPathComponent(self.ID).stringByAppendingPathComponent("metadata"))!
        uc.host = hostIP
        uc.scheme = hostScheme
        uc.port = hostPort
        
        return uc
    }
    
    var playUrl : NSURL {
        
        var pc : NSURLComponents = NSURLComponents(string: fileDestinationUrl.stringByAppendingPathComponent(self.ID).stringByAppendingPathComponent("content"))!
        pc.host = hostIP
        pc.scheme = hostScheme
        pc.port = hostPort
        
        var qs: NSArray = self.url.lastPathComponent.componentsSeparatedByString("?")
        pc.path? = pc.path!.stringByAppendingPathComponent(qs[0] as! String)
        
        return pc.URL!
    }
    
    var filePath : String {
        var path: String =  fileDestinationUrl.stringByAppendingPathComponent(self.ID).stringByAppendingPathComponent("content")
        var qs: NSArray = self.url.lastPathComponent.componentsSeparatedByString("?")
        return path.stringByAppendingPathComponent(qs[0] as! String)
    }
    
    //MARK: - init object
    init(itemId: String, itemUrl: String, itemFormat: String) {
        super.init()
        self.ID = itemId
        self.url = itemUrl
        self.format = itemFormat
        
        let notificationDownloadStatus = NotificationGroup(
            "NotificationDownloadStatusWillStart_".stringByAppendingString(itemId),
            "NotificationDownloadStatusStarted_".stringByAppendingString(itemId),
            "NotificationDownloadStatusInProgress_".stringByAppendingString(itemId),
            "NotificationDownloadStatusPaused_".stringByAppendingString(itemId),
            "NotificationDownloadStatusResumed_".stringByAppendingString(itemId),
            "NotificationDownloadStatusDownloaded_".stringByAppendingString(itemId),
            "NotificationDownloadStatusCancelled_".stringByAppendingString(itemId),
            "NotificationDownloadStatusInterrupted_".stringByAppendingString(itemId),
            "NotificationDownloadStatusFailed_".stringByAppendingString(itemId))

        notificationManager.registerGroupObserver(notificationDownloadStatus, block: { notif in
            
            switch(notif.name) {
                case "NotificationDownloadStatusWillStart_".stringByAppendingString(itemId):
                    self.downloadState = KADownloadItemDownloadState.WillStart
                    for delegate in self.delegates {
                        delegate.downloadWillStart(self)
                    }
                    break
                case "NotificationDownloadStatusStarted_".stringByAppendingString(itemId):
                    self.downloadState = KADownloadItemDownloadState.Started
                    for delegate in self.delegates {
                        delegate.downloadStarted(self)
                    }
                    break
                case "NotificationDownloadStatusInProgress_".stringByAppendingString(itemId):
                    if self.downloadState == KADownloadItemDownloadState.InProgress || self.downloadState == KADownloadItemDownloadState.Started || self.downloadState == KADownloadItemDownloadState.Resumed {
                        self.downloadState = KADownloadItemDownloadState.InProgress
                        let userInfo:Dictionary<String,AnyObject> = notif.userInfo as! Dictionary<String,AnyObject>
                        var progress : NSNumber = userInfo["progress"] as! NSNumber
                        self.currentProgress = progress.doubleValue;
                        for delegate in self.delegates {
                            delegate.downloadInProgress(self)
                        }
                    }
                    break
                case "NotificationDownloadStatusDownloaded_".stringByAppendingString(itemId):
                    self.downloadState = KADownloadItemDownloadState.Downloaded
                    for delegate in self.delegates {
                        delegate.downloadFinished(self)
                    }
                    break
                case "NotificationDownloadStatusFailed_".stringByAppendingString(itemId):
                    self.downloadState = KADownloadItemDownloadState.Failed
                    let userInfo:Dictionary<String,AnyObject> = notif.userInfo as! Dictionary<String,AnyObject>
                    var error : NSError? = userInfo["error"] as? NSError
                    for delegate in self.delegates {
                        delegate.downloadFailed(self, error: error)
                    }
                    break
                case "NotificationDownloadStatusCancelled_".stringByAppendingString(itemId):
                    self.delegates.removeAll(keepCapacity: true)
                    break
                case "NotificationDownloadStatusPaused_".stringByAppendingString(itemId):
                    self.downloadState = KADownloadItemDownloadState.Paused
                    let userInfo:Dictionary<String,AnyObject> = notif.userInfo as! Dictionary<String,AnyObject>
                    var progress : NSNumber = userInfo["progress"] as! NSNumber
                    self.currentProgress = progress.doubleValue;
                    for delegate in self.delegates {
                        delegate.downloadPaused(self)
                    }
                    break
                case "NotificationDownloadStatusResumed_".stringByAppendingString(itemId):
                    self.downloadState = KADownloadItemDownloadState.Resumed
                    for delegate in self.delegates {
                        delegate.downloadResumed(self)
                    }
                    break
                case "NotificationDownloadStatusInterrupted_".stringByAppendingString(itemId):
                    let userInfo:Dictionary<String,AnyObject> = notif.userInfo as! Dictionary<String,AnyObject>
                    var reason : NSNumber = userInfo["reason"] as! NSNumber
                    if let progress = userInfo["progress"] as? NSNumber{
                        if progress.doubleValue > 0 {
                            self.downloadState = KADownloadItemDownloadState.Interrupted
                        }
                        else if self.downloadState == KADownloadItemDownloadState.WillStart {
                            self.downloadState = KADownloadItemDownloadState.Initial
                        }
                    }
                    else {
                        self.downloadState = KADownloadItemDownloadState.Interrupted
                    }
                    self.lastInterruptionReason = KADownloadItemInterruptionReason(rawValue: reason.intValue)!
                    for delegate in self.delegates {
                        delegate.downloadInterrupted(self)
                    }
                    break
                default:
                    break
            }
        })
    }
    
    //MARK: - item methods
    func loadInfo(completion: (item: KADownloadItem, success: Bool) -> Void) {
        
        if self.downloadState == KADownloadItemDownloadState.Initial {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                KADownloadManager.sharedInstance().loadItemInfo(self, completion: { (success: Bool) -> Void in
                    if success {
                        //update item download state
                        self.downloadState = KADownloadItemDownloadState.GetInfo
                        
                        let group : dispatch_group_t = dispatch_group_create()
                        let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                        
                        dispatch_group_enter(group)
                        KADownloadManager.sharedInstance().updateItemState(self, completion: { (success) -> Void in
                            dispatch_group_leave(group)
                        })
                        
                        dispatch_group_enter(group)
                        for element in self.subtitles {
                            self.selectedSubtitles.append(element)
                        }
                        KADownloadManager.sharedInstance().updateSelectedSubtitles(self, completion: { (success) -> Void in
                            dispatch_group_leave(group)
                        })
                        
                        dispatch_group_notify(group, queue, {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                completion(item: self, success: success)
                            })
                        })
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            completion(item: self, success: success)
                        })
                    }
                })
            })
        }
        else {
            completion(item: self, success: Bool(false))
        }
    }
    
    func selectVideoStreams(indexes: NSArray, completion: (success: Bool) -> Void) {
        self.videoStreams.sort({ $0.bandwidth > $1.bandwidth })
        self.selectedVideoStreams.removeAll(keepCapacity: false)
        if self.videoStreams.count > 0 {
            for i in 0..<indexes.count {
                var stream: KADownloadItem_video = self.videoStreams[(indexes[i] as! NSNumber).integerValue] as KADownloadItem_video
                self.selectedVideoStreams.append(stream);
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                KADownloadManager.sharedInstance().updateSelectedVideoStreams(self, completion: { (success) -> Void in
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(success: success)
                    })
                })
            })
        }
        else {
             completion(success: Bool(false))
        }
    }
    
    func selectAudioStreams(indexes: NSArray, completion: (success: Bool) -> Void) {
        self.selectedAudioStreams.removeAll(keepCapacity: false)
        if self.audioStreams.count > 0 {
            for i in 0..<indexes.count {
                var stream: KADownloadItem_media = self.audioStreams[(indexes[i] as! NSNumber).integerValue] as KADownloadItem_media
                self.selectedAudioStreams.append(stream);
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                KADownloadManager.sharedInstance().updateSelectedAudioStreams(self, completion: { (success) -> Void in
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(success: success)
                    })
                })
            });
        }
        else {
            completion(success: Bool(true))
        }
    }
    
    func startDownload() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            KADownloadManager.sharedInstance().addDownloadItem(self)
        })
    }
    
    func pause() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            KADownloadManager.sharedInstance().pauseDownloadItem(self)
        })
    }
    
    func resume() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            KADownloadManager.sharedInstance().resumeDownloadItem(self)
        })
    }
    
    func updateMediaMark() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            KADownloadManager.sharedInstance().updateItemMediaMark(self, completion: { (success) -> Void in
                //OK
            })
        })
    }
    
    func updateItemWatchRestriction(value: Bool) {
        self.hasWatchRestriction = value
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            KADownloadManager.sharedInstance().updateItemWatchRestriction(self, completion: { (success) -> Void in
                //OK
            })
        })
    }
    
    func updateLicense() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            KADownloadManager.sharedInstance().updateDownloadedItemLicense(self, completion: { (success) -> Void in
                //OK
            })
        })
    }
    
    func isLicenseValid(completion: (success: Bool, remainingTime: NSTimeInterval) -> Void) {
        KADownloadManager.sharedInstance().isDownloadedItemLicenseValid(self, completion: completion)
    }
    
    func reset() {
        notificationManager.deregisterAll()
    }
    
    func addDelegate(delegate: KADownloadItemDelegate?) {
        //remove all nulls
        self.delegates = filter(self.delegates, {!($0 is NSNull)})
        
        if let value = delegate {
            self.removeDelegate(value)
            self.delegates.insert(value, atIndex: 0)
        }
    }
    
    func removeDelegate(delegate: KADownloadItemDelegate?) {
        if let value = delegate {
            if let index = self.findIdenticalObject(self.delegates, value: value) {
                self.delegates.removeAtIndex(index)
            }
        }
    }
    
    // MARK: - helper functions
    func findIdenticalObject<T : AnyObject>(array: [T], value: T) -> Int? {
        for (index, elem) in enumerate(array) {
            if elem === value {
                return index
            }
        }
        return nil
    }
}
