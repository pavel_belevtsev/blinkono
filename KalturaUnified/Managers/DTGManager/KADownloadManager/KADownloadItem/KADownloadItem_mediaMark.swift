//
//  KADownloadItem_mediaMark.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 6/16/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

public class KADownloadItem_mediaMark: NSObject {
    var status: Int32 = 0
    var groupId: Int32 = 0
    var locationSec: Double = 0
    var mediaId: String = ""
    var deviceId: String = ""
    var deviceName: String = ""
    var siteGUID: String = ""
    var updateDate: Double = 0
    
    
    init(mediaMarkInfo: NSDictionary!) {
        super.init()
        self.status = mediaMarkInfo["status"]!.intValue
        self.groupId = mediaMarkInfo["groupId"]!.intValue
        self.locationSec = mediaMarkInfo["locationSec"]!.doubleValue
        
        let mId: AnyObject? = mediaMarkInfo["mediaId"]
        self.mediaId = "\(mId!)"
        self.deviceId = mediaMarkInfo["deviceId"] as! String
        self.deviceName = mediaMarkInfo["deviceName"] as! String
        self.siteGUID = mediaMarkInfo["siteGUID"] as! String
        self.updateDate = mediaMarkInfo["updateDate"]!.doubleValue
        
    }
}