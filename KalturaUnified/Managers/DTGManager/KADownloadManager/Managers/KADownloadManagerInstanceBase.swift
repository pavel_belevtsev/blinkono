//
//  KADownloadManagerInstanceBase.swift
//  KADownloadManager
//
//  Created by Alex Zchut on 4/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import Foundation

//MARK: - KADownloadManagerInstanceDelegate protocol
protocol KADownloadManagerInstanceDelegate {
    func downloadWillStart(itemId: String)
    func downloadStarted(itemId: String, arrDownloadItemUrls: NSMutableArray, completion: (success: Bool) -> Void)
    func downloadCanceled(itemId: String)
    func downloadFinished(itemId: String)
    func downloadFinishedForItemUrl(itemUrl: KADownloadItemUrl, completion: (success: Bool) -> Void)
    func downloadFinishedWithFailure(itemId: String, error: NSError)
    func downloadFinishedEventsForBackgroundURLSession(session: NSURLSession)
    func downloadInProgress(itemId: String, progress: Double)
    func downloadPaused(itemId: String, arrRelatedItemUrls : NSArray)
    func downloadInterrupted(itemId: String, arrRelatedItemUrls : NSArray)
    func downloadResumed(itemId: String)
    func getDownloadItemById(itemId: String, completion: (item: KADownloadItem) -> Void)
    func getDownloadItemUrl(url: String, completion: (obj: KADownloadItemUrl?) -> Void)
    func getDownloadItemUrls(itemId: String, completion: (arrNotCompleted : NSMutableArray, arrCompleted : NSMutableArray) -> Void)
    func getDownloadItemState(itemId: String, completion: (state: KADownloadItemDownloadState) -> Void)
    func getFirstDownloadItemForState(state: KADownloadItemDownloadState ,completion: (item : KADownloadItem?) -> Void)
    func getSettingsKeys(completion: (dict: NSDictionary) -> Void)
}

//MARK: - KADownloadManagerInstanceBase class
class KADownloadManagerInstanceBase: NSObject, NSURLSessionDelegate {

    //MARK: - Variables
    var sessionManager    : AFURLSessionManager!
    var arrDownloadingData : NSMutableArray = NSMutableArray()
    var arrDownloadedData : NSMutableArray = NSMutableArray()

    var delegate : KADownloadManagerInstanceDelegate? {
        didSet {
            sessionManager = self.backgroundSession()
            self.setSessionManagerBlocks()
        }
    }
    
    //MARK: - init Methods
    func backgroundSession() -> AFURLSessionManager? {
        struct sessionStruct {
            static var onceToken : dispatch_once_t = 0;
            static var sessionManager   : AFURLSessionManager? = nil
        }
        
        dispatch_once(&sessionStruct.onceToken, { () -> Void in
            let sessionIdentifer     : NSString = "\(managerScheme).BackgroundSession.".stringByAppendingString(NSStringFromClass(KADownloadManagerInstanceBase.self))
            var sessionConfiguration : NSURLSessionConfiguration
            if iOS7 {
                sessionConfiguration = NSURLSessionConfiguration.backgroundSessionConfiguration(sessionIdentifer as String)
            }
            else {
                sessionConfiguration = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier(sessionIdentifer as String)
            }
            
//            var sessionConfiguration : NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
            
            self.delegate!.getSettingsKeys({ (dict) -> Void in
                sessionConfiguration.allowsCellularAccess = dict[KADownloadSettings.enableDownloadOnCellularData]!.boolValue
                sessionConfiguration.timeoutIntervalForRequest = dict[KADownloadSettings.timeoutIntervalForRequest]!.doubleValue
                sessionConfiguration.timeoutIntervalForResource = dict[KADownloadSettings.timeoutIntervalForResource]!.doubleValue
                sessionConfiguration.HTTPMaximumConnectionsPerHost = dict[KADownloadSettings.httpMaximumConnectionsPerHost]!.integerValue
                
                sessionStruct.sessionManager = AFURLSessionManager(sessionConfiguration: sessionConfiguration)
                sessionStruct.sessionManager!.operationQueue.maxConcurrentOperationCount = dict[KADownloadSettings.simultaneousDownloadItems]!.integerValue
            })
        })
        return sessionStruct.sessionManager!
    }
    
    func setSessionManagerBlocks() {
        self.sessionManager.setDidFinishEventsForBackgroundURLSessionBlock { ( session :NSURLSession!) -> Void in
            self.delegate?.downloadFinishedEventsForBackgroundURLSession(session);
        }
    }
    
    func performActionOnDownloadComplete(itemId: String, completion: (success: Bool) -> Void) {
        completion(success: true)
    }

    //MARK: - Base functions to override on child classes
    func loadItemInfo(item: KADownloadItem, completion: (updatedItem: KADownloadItem, success: Bool) -> Void) {
        completion(updatedItem: item, success: false)
    }
    
    func interruptActiveDownloadTasks() {
        
        var arrParentItemIds: NSArray = self.arrDownloadingData.valueForKeyPath("@distinctUnionOfObjects.parentItemId") as! NSArray
        
        for i in 0..<arrParentItemIds.count {
            var itemId: String = arrParentItemIds[i] as! String
            self.delegate?.getDownloadItemById(itemId, completion: { (item: KADownloadItem) -> Void in
                switch(item.downloadState) {
                    case .InProgress, .WillStart, .Started, .Resumed:
                        var currentTasksURLsForParentItemId: NSArray = self.getAllRelatedKADownloadItemUrlsByParentItemId(arrParentItemIds[i] as! String)
                        self.delegate?.downloadInterrupted(itemId, arrRelatedItemUrls: currentTasksURLsForParentItemId)
                        break
                    default:
                        break
                }
            })
        }

        var downloadTasks : NSArray = self.downloadTasks()
        
        for downloadTask in downloadTasks {
            var itemUrl : KADownloadItemUrl? = self.getKADownloadItemUrl(downloadTask.originalRequest.URL!.absoluteString!)
            var taskState : NSURLSessionTaskState = downloadTask.state
            switch(taskState) {
            case NSURLSessionTaskState.Running:
                downloadTask.cancelByProducingResumeData({ (resumeData: NSData!) -> Void in
                    if (resumeData != nil) {
                        if let itemUrl = self.getKADownloadItemUrl(downloadTask.originalRequest.URL!.absoluteString!) {
                            resumeData.writeToFile(itemUrl.fileSavePath.stringByAppendingPathExtension("data")!, atomically: true)
                        }
                        else {
                            self.delegate?.getDownloadItemUrl(downloadTask.originalRequest.URL!.absoluteString!, completion: { (obj : KADownloadItemUrl?) -> Void in
                                if let obj = obj {
                                    resumeData.writeToFile(obj.fileSavePath.stringByAppendingPathExtension("data")!, atomically: true)
                                }
                            })
                        }
                    }
                })
                break
            default:
                downloadTask.cancel()
                break
            }
        }
    }
    
    func resumeInterruptedDownloadTasks() {
        self.delegate?.getFirstDownloadItemForState(KADownloadItemDownloadState.Interrupted, completion: { (item : KADownloadItem?) -> Void in
            if let downloadItem = item {
                self.resumeDownloadItem(downloadItem)
            }
        })
    }
    
    func addDownloadItem(item: KADownloadItem) {
        //not implemented
    }
    
    func addInternalDownloadTask(itemUrl: KADownloadItemUrl) {
        self.addInternalDownloadTask(itemUrl, resume: Bool(true))
    }
    
    func addInternalDownloadTask(itemUrl: KADownloadItemUrl, resume: Bool) {
        var fileSavePathExtension : NSString? = itemUrl.fileSavePath.lastPathComponent.pathExtension
        if (fileSavePathExtension?.length == 0) {
            itemUrl.fileSavePath = itemUrl.fileSavePath.stringByAppendingPathExtension("file")!
        }
        
        var taskResumeData : NSData?
        var folderDir : String = itemUrl.fileSavePath.stringByDeletingLastPathComponent
        if !NSFileManager.defaultManager().fileExistsAtPath(folderDir) {
            NSFileManager.defaultManager().createDirectoryAtPath(folderDir, withIntermediateDirectories: true, attributes: nil, error: nil)
        }
        else {
            
            var resumeDateFileName : String = itemUrl.fileSavePath.stringByAppendingPathExtension("data")!
            if NSFileManager.defaultManager().fileExistsAtPath(resumeDateFileName) {
                taskResumeData = NSData(contentsOfFile: resumeDateFileName)
                var hasValidResumeData : Bool = self.isValidResumeData(taskResumeData)
                if hasValidResumeData == false {
                    taskResumeData = nil
                }
            }
            else {
                taskResumeData = nil
            }
        }
        
        var downloadTask : NSURLSessionDownloadTask?
        
        if let taskResumeData = taskResumeData {
            downloadTask = self.sessionManager.session.downloadTaskWithResumeData(taskResumeData)
        }
        else {
            var url          : NSURL = NSURL(string: itemUrl.URI as String)!
            var request      : NSURLRequest = NSURLRequest(URL: url)
            downloadTask = sessionManager.downloadTaskWithRequest(request, progress: nil, destination: { (url : NSURL!, response : NSURLResponse!) -> NSURL! in
                return NSURL(fileURLWithPath: itemUrl.fileSavePath)
                }, completionHandler: nil)
        }
        
        if resume == true {
            downloadTask?.resume()
        }
    }
    
    func cancelDownloadItem(itemId: String) {
        self.removeDownloadItemTasks(itemId)
        self.delegate?.downloadCanceled(itemId)
    }
    
    
    func removeDownloadItemTasks(itemId: String) {
        var downloadTasks : NSArray = self.downloadTasks()
        
        var currentTasksURLsForParentItemId: NSArray = self.getKADownloadItemUrlsStrings(itemId)
        let predicate = NSPredicate(block: { (task: AnyObject!, dict: [NSObject : AnyObject]!) -> Bool in
            return currentTasksURLsForParentItemId.containsObject((task as! NSURLSessionDownloadTask).originalRequest.URL!.absoluteString!)
        })
        
        let filteredDownloadTasks : NSArray = downloadTasks.filteredArrayUsingPredicate(predicate)
        for downloadTask in filteredDownloadTasks {
            if downloadTask.state != NSURLSessionTaskState.Canceling {
                println("--> cancelling: \(downloadTask.originalRequest.URL!.absoluteString)")
                downloadTask.cancel()
            }
        }
        var relatedItemUrls: NSArray = self.getAllRelatedKADownloadItemUrlsByParentItemId(itemId)
        self.arrDownloadingData.removeObjectsInArray(relatedItemUrls as [AnyObject])
    }

    
    func cancelAllDownloadItems() {
        var downloadTasks : NSArray = self.downloadTasks()
        for downloadTask in downloadTasks {
            println("--> cancelling: \(downloadTask.originalRequest.URL!.absoluteString)")
            downloadTask.cancel()
        }
    }
    
    func pauseDownloadItem(item: KADownloadItem) {
        
        //get all relates urls for downloading item
        var currentTasksURLsForParentItemId: NSArray = self.getKADownloadItemUrlsStrings(item.ID)
        
        if currentTasksURLsForParentItemId.count > 0 {
            //update current state for all not completed downloads (to calculate the % of download if needed)
            var sampleKADownloadItem = self.getKADownloadItemUrl("\(currentTasksURLsForParentItemId[0])")
            var arrRelatedNotStartedDownloads : NSArray = NSArray(array: self.getAllRelatedKADownloadItemUrls(sampleKADownloadItem!))
            
            //notify delegate
            self.delegate?.downloadPaused(item.ID, arrRelatedItemUrls: arrRelatedNotStartedDownloads)
        }
        else {
            //notify delegate
            self.delegate?.downloadPaused(item.ID, arrRelatedItemUrls: [])
        }
        
        var downloadTasks : NSArray = self.downloadTasks()
        
        //filter all current tasks to remain only related to this item
        let predicate = NSPredicate(block: { (task: AnyObject!, dict: [NSObject : AnyObject]!) -> Bool in
            return currentTasksURLsForParentItemId.containsObject((task as! NSURLSessionDownloadTask).originalRequest.URL!.absoluteString!)
        })
        let filteredDownloadTasks : NSArray = downloadTasks.filteredArrayUsingPredicate(predicate)
        
        for downloadTask in filteredDownloadTasks {
            var taskState : NSURLSessionTaskState = downloadTask.state
            switch(taskState) {
                case NSURLSessionTaskState.Running:
                    downloadTask.cancelByProducingResumeData({ (resumeData: NSData!) -> Void in
                        if (resumeData != nil) {
                            if let itemUrl = self.getKADownloadItemUrl(downloadTask.originalRequest.URL!.absoluteString!) {
                                resumeData.writeToFile(itemUrl.fileSavePath.stringByAppendingPathExtension("data")!, atomically: true)
                            }
                        }
                    })
                break;
            default:
                break;
            }
        }
    }
    
    func resumeDownloadItem(item: KADownloadItem) {
        
        //notify delegate
        self.delegate?.downloadResumed(item.ID)

        var downloadTasks : NSArray = self.downloadTasks()

        //get all relates urls for downloading item
        var currentTasksURLsForParentItemId: NSArray = self.getKADownloadItemUrlsStrings(item.ID)
        
        //filter all current tasks to remain only related to this item
        let predicate = NSPredicate(block: { (task: AnyObject!, dict: [NSObject : AnyObject]!) -> Bool in
            return currentTasksURLsForParentItemId.containsObject((task as! NSURLSessionDownloadTask).originalRequest.URL!.absoluteString!)
        })
        let filteredDownloadTasks : NSArray = downloadTasks.filteredArrayUsingPredicate(predicate)
        
        for downloadTask in filteredDownloadTasks {
            var taskState : NSURLSessionTaskState = downloadTask.state
            switch(taskState) {
            case NSURLSessionTaskState.Suspended:
                downloadTask.resume()
                break;
            default:
                break;
            }
        }

        if currentTasksURLsForParentItemId.count > 0 {
            if currentTasksURLsForParentItemId.count > filteredDownloadTasks.count { //there are more tasks in array than actually downloading, create download tasks
                var sampleKADownloadItem = self.getKADownloadItemUrl("\(currentTasksURLsForParentItemId[0])")
                var arrRelatedNotStartedDownloads : NSMutableArray = NSMutableArray(array: self.getAllRelatedKADownloadItemUrls(sampleKADownloadItem!))
                
                let predicateForAlreadyDownloadingItems = NSPredicate(block: { (item: AnyObject!, dict: [NSObject : AnyObject]!) -> Bool in
                    return filteredDownloadTasks.valueForKeyPath("originalRequest.URL.absoluteString")!.containsObject(item.URI)
                })
                let arrObjectsToRemove : NSArray = arrRelatedNotStartedDownloads.filteredArrayUsingPredicate(predicateForAlreadyDownloadingItems)
                arrRelatedNotStartedDownloads.removeObjectsInArray(arrObjectsToRemove as! [AnyObject])
                
                for i in 0..<arrRelatedNotStartedDownloads.count {
                    self.addInternalDownloadTask(arrRelatedNotStartedDownloads[i] as! KADownloadItemUrl)
                }
            }
        }
        else {
            self.delegate?.getDownloadItemUrls(item.ID, completion: { (arrNotCompletedItemUrls : NSMutableArray, arrCompletedItemUrls : NSMutableArray) -> Void in
                self.arrDownloadingData.addObjectsFromArray(arrNotCompletedItemUrls as [AnyObject])
                self.arrDownloadedData.addObjectsFromArray(arrCompletedItemUrls as [AnyObject])
                
                if arrNotCompletedItemUrls.count == 0 && arrCompletedItemUrls.count == 0 {
                    item.downloadState = KADownloadItemDownloadState.Initial
                    item.loadInfo({ (im : KADownloadItem, success: Bool) -> Void in
                        if success {
                            im.selectVideoStreams([0], completion: { (success) -> Void in
                                if success {
                                    im.selectAudioStreams([0], completion: { (success) -> Void in
                                        if success {
                                            im.startDownload()
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
                else {
                    if arrNotCompletedItemUrls.count > 0 {
                        for i in 0..<arrNotCompletedItemUrls.count {
                            self.addInternalDownloadTask(arrNotCompletedItemUrls[i] as! KADownloadItemUrl)
                        }
                    }
                    else {//all urls are completed
                        self.performActionOnDownloadComplete(item.ID, completion: { (success) -> Void in
                            if success == true {
                                self.delegate?.downloadFinished(item.ID)
                            }
                        })
                    }
                }
            })
        }
    }
    
    func updateDownloadedItemLicense(item: KADownloadItem, completion: (success: Bool) -> Void) {
        //implement in child classes
    }
    
    func isDownloadedItemLicenseValid(item: KADownloadItem, completion: (success: Bool, remainingTime: NSTimeInterval) -> Void) {
        //implement in child classes
    completion(success: Bool(true), remainingTime: 10000)
    }
    
    //MARK: - current tasks
    func tasks() -> NSArray {
        return self.tasksForKeyPath("tasks")
    }
    
    func dataTasks() -> NSArray {
        return self.tasksForKeyPath("dataTasks")
    }
    
    func uploadTasks() -> NSArray {
        return self.tasksForKeyPath("uploadTasks")
    }
    
    func downloadTasks() -> NSArray {
        return self.tasksForKeyPath("downloadTasks")
    }
    
    func tasksForKeyPath(keyPath: NSString) -> NSArray {
        var tasks     : NSArray! = NSArray()
        var semaphore : dispatch_semaphore_t = dispatch_semaphore_create(0)
        sessionManager.session.getTasksWithCompletionHandler { (dataTasks, uploadTasks, downloadTasks) -> Void in
            if keyPath == "dataTasks" {
                tasks = dataTasks
            } else if keyPath == "uploadTasks" {
                tasks = uploadTasks
                
            } else if keyPath == "downloadTasks" {
                if let pendingTasks = downloadTasks {
                    tasks = downloadTasks
                    println("pending tasks \(tasks)")
                }
            } else if keyPath == "tasks" {
                tasks = ([dataTasks, uploadTasks, downloadTasks] as AnyObject).valueForKeyPath("@unionOfArrays.self") as! NSArray
                
                println("pending task\(tasks)")
            }
            
            dispatch_semaphore_signal(semaphore)
        }
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
        return tasks
    }
    
    // MARK: - Validations
    func isValidResumeData(resumeData: NSData?) -> Bool {
        if resumeData == nil {
            return false
        }
        if resumeData?.length < 0 {
            return false
        }
        
        var error : NSError?
        var resumeDictionary : AnyObject! = NSPropertyListSerialization.propertyListWithData(resumeData!, options: Int(NSPropertyListMutabilityOptions.Immutable.rawValue), format: nil, error: &error)
        
        var localFilePath : NSString? = resumeDictionary?.objectForKey("NSURLSessionResumeInfoLocalPath") as? NSString
        if localFilePath == nil {
            return false
        }
        
        if localFilePath?.length < 1 {
            return false
        }
        
        var fileManager : NSFileManager! = NSFileManager.defaultManager()
        return fileManager.fileExistsAtPath(localFilePath! as String)
    }
    
    //MARK: - KADownloadItemUrl functions
    func getKADownloadItemUrl(url: String) -> KADownloadItemUrl? {
        return self.arrDownloadingData.filteredArrayUsingPredicate(NSPredicate(format: "self.URI = %@", url)).first as? KADownloadItemUrl
    }
    
    func getKADownloadItemUrlsStrings(itemId: String) -> NSArray {
        return (self.arrDownloadingData.filteredArrayUsingPredicate(NSPredicate(format: "parentItemId = %@", itemId)) as NSArray).valueForKeyPath("URI") as! NSArray
    }
    
    func getAllRelatedKADownloadItemUrls(item: KADownloadItemUrl) -> NSArray {
        return self.arrDownloadingData.filteredArrayUsingPredicate(NSPredicate(format: "parentItemId = %@", item.parentItemId))
    }
    
    func getAllRelatedKADownloadItemUrlsByParentItemId(itemId: String) -> NSArray {
        return self.arrDownloadingData.filteredArrayUsingPredicate(NSPredicate(format: "parentItemId = %@", itemId))
    }
    
    
    func getAllRelatedCompletedKADownloadItemUrls(item: KADownloadItemUrl) -> NSArray {
        return self.arrDownloadedData.filteredArrayUsingPredicate(NSPredicate(format: "parentItemId = %@", item.parentItemId))
    }
    
    func removeAllSameKADownloadItemUrls(item: KADownloadItemUrl) {
        var arrItemsToRemove : NSArray = self.arrDownloadingData.filteredArrayUsingPredicate(NSPredicate(format: "parentItemId = %@ AND URI = %@", item.parentItemId, item.URI))
        self.arrDownloadingData.removeObjectsInArray(arrItemsToRemove as [AnyObject])
    }
}
