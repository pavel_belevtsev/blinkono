//
//  KADownloadItemUrl.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 5/10/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import UIKit

class KADownloadItemUrl: NSObject {
   
    var URI: String = ""
    var fileSavePath: String = ""
    var fileTempSavePath: String = ""

    var parentItemId: String = ""
    var expected: Int64 = 0
    var written: Int64 = 0

    init(URI: String, fileSavePath: String, parentItemId: String, expected : Int64, written : Int64) {
        super.init()
        self.URI = URI
        self.parentItemId = parentItemId
        self.fileSavePath = fileSavePath
        self.expected = expected
        self.written = written
    }
}