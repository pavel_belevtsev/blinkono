//
//  KADownloadManagerInstanceWVM.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 6/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

class KADownloadManagerInstanceWVM: KADownloadManagerInstanceSimple {

    override func performActionOnDownloadComplete(itemId: String, completion: (success: Bool) -> Void) {
        
        self.delegate?.getDownloadItemById(itemId, completion: { (item : KADownloadItem) -> Void in
            self.updateDownloadedItemLicense(item, completion: completion)
        })
    }
    
    // MARK: - License handle
    override func updateDownloadedItemLicense(item: KADownloadItem, completion: (success: Bool) -> Void) {
        
        var assetPath = item.filePath
        println("assetPath: \(assetPath)")
        let success = WidevineDRMadapter.registerAsset(assetPath)
        completion(success: success)
    }
    
    override func isDownloadedItemLicenseValid(item: KADownloadItem, completion: (success: Bool, remainingTime: NSTimeInterval) -> Void) {
        
        var assetPath = item.filePath
        println("assetPath: \(assetPath)")
        WidevineDRMadapter.isLicenseValid(assetPath, completion: completion)
    }
}
