//
//  WidevineAdapter.m
//  KalturaUnified
//
//  Created by Noam Tamim on 6/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#ifdef PLAYER_WV
#import <TVCPlayerWV/DrmManager.h>
#endif

#import "WidevineDRMadapter.h"
#import "WViPhoneAPI.h"

@implementation WidevineDRMadapter

+(void) testPlayback:(NSString*)assetPath {
    
#if PLAYER_WV==1
    // try to get a playable url
    NSMutableString* playableURL = [NSMutableString stringWithCapacity:1024];
    WViOsApiStatus wvStatus = WV_Play(assetPath, playableURL, nil);
    if (wvStatus == WViOsApiStatus_OK) {
        NSLog(@"Playable URL: %@", playableURL);
    } else {
        NSLog(@"Failed playback: %d", wvStatus);
    }
    WV_Stop();
    
#endif
}

+(BOOL) registerAsset:(NSString*)assetPath {

    BOOL returnValue = YES;
#if PLAYER_WV
    returnValue = [[DrmManager sharedDRMManager] registerAsset:assetPath];
#endif
    return returnValue;
}


+(void) isLicenseValid:(NSString*)assetPath completion:(void(^)(BOOL success, NSTimeInterval remainingTime))completion
{
    
#if PLAYER_WV
    [[DrmManager sharedDRMManager] licenceTimeRemaining:assetPath completion:completion];
#else
    if (completion)
    {
        completion(YES,100000);
    }
#endif
}

@end
