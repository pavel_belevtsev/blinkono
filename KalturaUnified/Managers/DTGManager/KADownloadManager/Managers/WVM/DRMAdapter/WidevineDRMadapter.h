//
//  WidevineAdapter.h
//  KalturaUnified
//
//  Created by Noam Tamim on 6/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WidevineDRMadapter : NSObject
+(BOOL) registerAsset:(NSString*)localPath;
+(void) testPlayback:(NSString*)assetPath;
+(void) isLicenseValid:(NSString*)assetPath completion:(void(^)(BOOL success, NSTimeInterval remainingTime))completion;
@end
