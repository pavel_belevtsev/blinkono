//
//  KADownloadManagerInstanceSimple.swift
//  KADownloadManager
//
//  Created by Alex Zchut on 4/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import Foundation
//import UIKit


class KADownloadManagerInstanceSimple: KADownloadManagerInstanceBase {
    
    override func loadItemInfo(item: KADownloadItem, completion: (updatedItem: KADownloadItem, success: Bool) -> Void) {
        
        var videoStream : KADownloadItem_video = KADownloadItem_video()
        videoStream.URI = item.url
        item.videoStreams.append(videoStream)
        
        completion(updatedItem: item, success: Bool(true))
    }
    
    override func addDownloadItem(item: KADownloadItem) {
        super.addDownloadItem(item)
        
        var url : NSURL = NSURL(string: item.url)!
        var fileName : String = item.url.lastPathComponent.componentsSeparatedByString("?").first!
        var folderDir : String = item.contentFolderPath
        var fileSavePath = folderDir.stringByAppendingPathComponent(fileName)
        
        self.delegate?.downloadWillStart(item.ID)
        
        let group : dispatch_group_t = dispatch_group_create()
        let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        
        var arrDownloadItemUrls : NSMutableArray = NSMutableArray()
        
        dispatch_group_enter(group)
        var request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "HEAD"
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            if (error == nil) {
                var di : KADownloadItemUrl = KADownloadItemUrl(URI: item.url, fileSavePath: fileSavePath, parentItemId: item.ID, expected: response.expectedContentLength, written: 0)
                arrDownloadItemUrls.addObject(di)
            }
            dispatch_group_leave(group)
        })
        
        dispatch_group_notify(group, queue, {
            //notify delegate
            if arrDownloadItemUrls.count > 0 {
                var di : KADownloadItemUrl = arrDownloadItemUrls.firstObject() as! KADownloadItemUrl
                self.delegate?.downloadStarted(item.ID, arrDownloadItemUrls: arrDownloadItemUrls, completion: { (success) -> Void in
                    if success {
                        self.arrDownloadingData.addObject(di)
                        self.addInternalDownloadTask(di)
                    }
                })
            }
            else {
                self.delegate?.downloadFinishedWithFailure(item.ID, error: NSError(domain: managerScheme, code: 199, userInfo: [:]))
            }
        })
    }
    
    override func setSessionManagerBlocks() {
        super.setSessionManagerBlocks()
        self.sessionManager.setDownloadTaskDidWriteDataBlock { (session: NSURLSession!, downloadTask: NSURLSessionDownloadTask!, bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) -> Void in
            var itemUrl : KADownloadItemUrl? = self.getKADownloadItemUrl(downloadTask.originalRequest.URL!.absoluteString!)
            
            if let item = itemUrl {
                item.written = totalBytesWritten
                
                if (item.expected > 0) {
                    var progress : Double = Double(item.written) / Double(item.expected)
                    
                    if progress <= 1 {
                        self.delegate?.downloadInProgress(item.parentItemId, progress: progress)
                    }
                }
            }
        }
        
        self.sessionManager.setDownloadTaskDidFinishDownloadingBlock { (session: NSURLSession!, downloadTask: NSURLSessionDownloadTask!, location: NSURL!) -> NSURL! in
            var itemUrl : KADownloadItemUrl? = self.getKADownloadItemUrl(downloadTask.originalRequest.URL!.absoluteString!)
            
            if let item = itemUrl {
                var error       : NSError?
                var fileManager : NSFileManager = NSFileManager.defaultManager()
                var success     : Bool = fileManager.moveItemAtURL(location, toURL: NSURL(fileURLWithPath: item.fileSavePath)!, error: &error)
                
                //remove completed task
                self.arrDownloadingData.removeObject(item)
                
                if let hasError = error {
                    self.delegate?.downloadFinishedWithFailure(item.parentItemId, error: error!)
                }
                else {
                    //update finished download in db
                    self.delegate?.downloadFinishedForItemUrl(item, completion: { (success) -> Void in
                        //remove same if exists
                        self.removeAllSameKADownloadItemUrls(item)
                        //post completion
                        //post completion
                        self.performActionOnDownloadComplete(item.parentItemId, completion: { (success) -> Void in
                            if success == true {
                                self.delegate?.downloadFinished(item.parentItemId)
                            }
                        })
                    })
                }
            }
            return location
        }
        
        self.sessionManager.setTaskDidCompleteBlock { (session: NSURLSession!, task: NSURLSessionTask!, error: NSError!) -> Void in
            if let error = error {
                println("Download complete with error: \(error.userInfo)")
                let errorUserInfo : NSDictionary? = error.userInfo
                
                var fileURL : String = errorUserInfo?.objectForKey("NSErrorFailingURLStringKey") as! String
                var itemUrl : KADownloadItemUrl? = self.arrDownloadingData.filteredArrayUsingPredicate(NSPredicate(format: "self.URI = %@", fileURL)).first as? KADownloadItemUrl
                
                if let hasReasonKey: AnyObject = errorUserInfo?.objectForKey("NSURLErrorBackgroundTaskCancelledReasonKey") {
                    let errorReasonNum : Int = Int(errorUserInfo?.objectForKey("NSURLErrorBackgroundTaskCancelledReasonKey") as! NSNumber)
                    if errorReasonNum == NSURLErrorCancelledReasonUserForceQuitApplication || errorReasonNum == NSURLErrorCancelledReasonBackgroundUpdatesDisabled {
                        
                        var resumeData : NSData? = errorUserInfo?.objectForKey(NSURLSessionDownloadTaskResumeData) as? NSData
                        var newTask : NSURLSessionDownloadTask
                        
                        if let resumeData = resumeData {
                            var hasValidResumeData : Bool = self.isValidResumeData(resumeData)
                            if hasValidResumeData == true {
                                newTask = self.sessionManager.session.downloadTaskWithResumeData(resumeData)
                                if (itemUrl != nil) {
                                    resumeData.writeToFile(itemUrl!.fileSavePath.stringByAppendingPathExtension("data")!, atomically: true)
                                }
                            } else {
                                newTask = self.sessionManager.session.downloadTaskWithURL(NSURL(string: fileURL as String)!)
                            }
                        }
                        else {
                            newTask = self.sessionManager.session.downloadTaskWithURL(NSURL(string: fileURL as String)!)
                        }
                        
                        newTask.suspend()
                        return
                    }
                }
                else if error.code != NSURLErrorCancelled {
                    var resumeData  : NSData? = errorUserInfo?.objectForKey(NSURLSessionDownloadTaskResumeData) as? NSData
                    var newTask : NSURLSessionDownloadTask
                    
                    if let resumeData = resumeData {
                        var hasValidResumeData : Bool = self.isValidResumeData(resumeData)
                        if hasValidResumeData == true {
                            newTask = self.sessionManager.session.downloadTaskWithResumeData(resumeData)
                            if (itemUrl != nil) {
                                resumeData.writeToFile(itemUrl!.fileSavePath.stringByAppendingPathExtension("data")!, atomically: true)
                            }
                        } else {
                            newTask = self.sessionManager.session.downloadTaskWithURL(NSURL(string: fileURL as String)!)
                        }
                        
                        if let item = itemUrl {
                            self.delegate?.getDownloadItemState(item.parentItemId, completion: { (state) -> Void in
                                if state == KADownloadItemDownloadState.InProgress {
                                    newTask.resume()
                                }
                                else {
                                    newTask.suspend()
                                }
                            })
                        }
                        else {
                            newTask.suspend()
                        }
                    }
                    else {
                        if let item = itemUrl {
                            if error.code == -997 || error.localizedDescription == "Lost connection to background transfer service" {
                                self.removeDownloadItemTasks(item.parentItemId)
                                
                                //notify delegate
                                self.delegate?.downloadFinishedWithFailure(item.parentItemId, error: error)
                            }
                            else {
                                newTask = self.sessionManager.session.downloadTaskWithURL(NSURL(string: item.URI)!)
                                self.delegate?.getDownloadItemState(item.parentItemId, completion: { (state) -> Void in
                                    if state == KADownloadItemDownloadState.InProgress {
                                        newTask.resume()
                                    }
                                    else {
                                        newTask.suspend()
                                    }
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func isDownloadedItemLicenseValid(item: KADownloadItem, completion: (success: Bool, remainingTime: NSTimeInterval) -> Void) {
        completion(success: Bool(true), remainingTime: 10000)
    }
}
