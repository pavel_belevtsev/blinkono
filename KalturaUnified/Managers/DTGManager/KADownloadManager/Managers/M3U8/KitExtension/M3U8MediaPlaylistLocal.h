//
//  M3U8MediaPlaylistLocal.h
//  KalturaUnified
//
//  Created by Alex Zchut on 4/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "M3U8MediaPlaylist.h"

@interface M3U8MediaPlaylistLocal : M3U8MediaPlaylist

+ (instancetype)initWithContent:(NSString *)string baseURL:(NSURL *)baseURL;
+ (instancetype)initWithContentOfURL:(NSURL *)URL error:(NSError **)error;

@end
