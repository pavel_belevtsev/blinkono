//
//  M3U8ExtXMediaList+Additions.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 4/30/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import Foundation

extension M3U8ExtXMediaList {
    var audioLanguages : NSArray {
        get {
            var arrTemp : NSMutableArray = NSMutableArray()
            let items = self.audioList()
            for i in 0..<items.count {
                arrTemp.addObject(items.extXMediaAtIndex(i))
            }
            return arrTemp.valueForKeyPath("language") as! NSArray
        }
    }
}
