//
//  M3U8MasterPlaylistLocal.m
//  KalturaUnified
//
//  Created by Alex Zchut on 4/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "M3U8MasterPlaylistLocal.h"

@implementation M3U8MasterPlaylistLocal

+ (instancetype)initWithContent:(NSString *)string baseURL:(NSURL *)baseURL {
    return [[M3U8MasterPlaylistLocal alloc] initWithContent:string baseURL:baseURL];
}

+ (instancetype)initWithContentOfURL:(NSURL *)URL error:(NSError **)error {
    return [[M3U8MasterPlaylistLocal alloc] initWithContentOfURL:URL error:error];
}
@end
