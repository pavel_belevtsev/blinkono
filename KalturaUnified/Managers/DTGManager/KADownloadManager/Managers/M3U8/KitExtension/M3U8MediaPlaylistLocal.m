//
//  M3U8MediaPlaylistLocal.m
//  KalturaUnified
//
//  Created by Alex Zchut on 4/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "M3U8MediaPlaylistLocal.h"

@implementation M3U8MediaPlaylistLocal

+ (instancetype)initWithContent:(NSString *)string baseURL:(NSURL *)baseURL {
    return [[M3U8MediaPlaylistLocal alloc] initWithContent:string baseURL:baseURL];
}

+ (instancetype)initWithContentOfURL:(NSURL *)URL error:(NSError **)error {
    return [[M3U8MediaPlaylistLocal alloc] initWithContentOfURL:URL error:error];
}

@end
