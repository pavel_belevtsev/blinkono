//
//  M3U8MasterPlaylistLocal.h
//  KalturaUnified
//
//  Created by Alex Zchut on 4/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "M3U8MasterPlaylist.h"

@interface M3U8MasterPlaylistLocal : M3U8MasterPlaylist

+ (instancetype)initWithContent:(NSString *)string baseURL:(NSURL *)baseURL;
+ (instancetype)initWithContentOfURL:(NSURL *)URL error:(NSError **)error;
@end
