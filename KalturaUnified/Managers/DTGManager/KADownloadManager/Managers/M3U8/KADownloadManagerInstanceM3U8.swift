//
//  KADownloadManagerInstanceM3U8.swift
//  KADownloadManager
//
//  Created by Alex Zchut on 4/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import Foundation

class KADownloadManagerInstanceM3U8: KADownloadManagerInstanceBase {
    
    var m3u8Parser : M3U8MasterPlaylistLocal = M3U8MasterPlaylistLocal()

    //MARK: - Download operations
    
    override func loadItemInfo(item: KADownloadItem, completion: (updatedItem: KADownloadItem, success: Bool) -> Void) {
        
        var error       : NSError?
        var fileManager : NSFileManager = NSFileManager.defaultManager()

        if let url = NSURL(string: item.url) {
            var request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "HEAD"
            NSURLSession.sharedSession().downloadTaskWithRequest(request, completionHandler: { (url: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
                if (error == nil) {
                    if response.expectedContentLength <= 1024000*5 {
                        request.HTTPMethod = "GET"
                        NSURLSession.sharedSession().downloadTaskWithRequest(request, completionHandler: { (url: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
                            if (error == nil) {
                                var fileName : String = item.url.lastPathComponent.componentsSeparatedByString("?").first!
                                var folderDir : String = item.contentFolderPath
                                var fileSavePath = folderDir.stringByAppendingPathComponent(fileName)
                                
                                //create/update folder
                                KAFileSavingUtility.createSavePath(fileSavePath);
                                let fileManager = NSFileManager.defaultManager()
                                fileManager.moveItemAtURL(url, toURL: NSURL(fileURLWithPath: fileSavePath)!, error: nil)
                                
                                if let fileContent = String(contentsOfFile: fileSavePath as String, encoding: NSUTF8StringEncoding, error: nil) {
                                    var updatedFileContent : String = fileContent
                                    var m3u8Parser = M3U8MasterPlaylistLocal.initWithContent(fileContent, baseURL: nil)
                                    
                                    //----- video streams
                                    m3u8Parser.xStreamList.sortByBandwidthInOrder(NSComparisonResult.OrderedAscending)
                                    let countVS = m3u8Parser.xStreamList.count
                                    for i in 0..<countVS {
                                        var streamInfo : M3U8ExtXStreamInf = m3u8Parser.xStreamList.extXStreamInfAtIndex(UInt(i))
                                        var videoStream : KADownloadItem_video = KADownloadItem_video()
                                        videoStream.bandwidth = Int32(streamInfo.bandwidth)
                                        videoStream.programId = Int32(streamInfo.programId)
                                        videoStream.codecs = (streamInfo.codecs != nil) ? streamInfo.codecs : ""
                                        videoStream.audio = (streamInfo.audio != nil) ? streamInfo.audio : ""
                                        videoStream.video = (streamInfo.video != nil) ? streamInfo.video : ""
                                        videoStream.subtitles = (streamInfo.subtitles != nil) ? streamInfo.subtitles : ""
                                        videoStream.closedCaptions = (streamInfo.closedCaptions != nil) ? streamInfo.closedCaptions : ""
                                        videoStream.URI = (streamInfo.URI != nil) ? streamInfo.URI : ""
                                        videoStream.resolution = "\(streamInfo.resolution.width)X\(streamInfo.resolution.height)"
                                        videoStream.m3u8UrlStr = (streamInfo.m3u8URL() != nil) ? streamInfo.m3u8URL().absoluteString! : ""
                                        
                                        //update content
                                        var url : String = "video".stringByAppendingPathComponent(videoStream.URI.md5().stringByAppendingPathExtension("m3u8")!)
                                        updatedFileContent = updatedFileContent.stringByReplacingOccurrencesOfString(videoStream.URI, withString: url)
                                        videoStream.fileSavePath = url
                                        
                                        item.videoStreams.append(videoStream)
                                    }
                                    
                                    //----- audio streams
                                    let countAS = m3u8Parser.xMediaList.audioList().count
                                    for i in 0..<countAS {
                                        var mediaInfo : M3U8ExtXMedia = m3u8Parser.xMediaList.audioList().extXMediaAtIndex(UInt(i))
                                        var mediaStream : KADownloadItem_media = KADownloadItem_media()
                                        mediaStream.type = mediaInfo.type() != nil ? mediaInfo.type() : ""
                                        mediaStream.URI = mediaInfo.URI() != nil ? mediaInfo.URI() : ""
                                        mediaStream.groupId = mediaInfo.groupId() != nil ? mediaInfo.groupId() : ""
                                        mediaStream.language = mediaInfo.language() != nil ? mediaInfo.language() : ""
                                        mediaStream.assocLanguage = mediaInfo.assocLanguage() != nil ? mediaInfo.assocLanguage() : ""
                                        mediaStream.name = mediaInfo.name() != nil ? mediaInfo.name() : ""
                                        mediaStream.instreamId = mediaInfo.instreamId() != nil ? mediaInfo.instreamId() : ""
                                        mediaStream.characteristics = mediaInfo.characteristics() != nil ? mediaInfo.characteristics() : ""
                                        mediaStream.isDefault = Bool(mediaInfo.isDefault())
                                        mediaStream.autoSelect = Bool(mediaInfo.autoSelect())
                                        mediaStream.forced = Bool(mediaInfo.forced())
                                        
                                        //update content
                                        var url : String = "audio".stringByAppendingPathComponent(mediaStream.URI.md5().stringByAppendingPathExtension("m3u8")!)
                                        updatedFileContent = updatedFileContent.stringByReplacingOccurrencesOfString(mediaStream.URI, withString: url)
                                        mediaStream.fileSavePath = url
                                        
                                        item.audioStreams.append(mediaStream)
                                    }
                                    
                                    //----- subtitles streams
                                    let countST = m3u8Parser.xMediaList.subtitleList().count
                                    for i in 0..<countST {
                                        var mediaInfo : M3U8ExtXMedia = m3u8Parser.xMediaList.subtitleList().extXMediaAtIndex(UInt(i))
                                        var mediaStream : KADownloadItem_media = KADownloadItem_media()
                                        mediaStream.type = mediaInfo.type() != nil ? mediaInfo.type() : ""
                                        mediaStream.URI = mediaInfo.URI() != nil ? mediaInfo.URI() : ""
                                        mediaStream.groupId = mediaInfo.groupId() != nil ? mediaInfo.groupId() : ""
                                        mediaStream.language = mediaInfo.language() != nil ? mediaInfo.language() : ""
                                        mediaStream.assocLanguage = mediaInfo.assocLanguage() != nil ? mediaInfo.assocLanguage() : ""
                                        mediaStream.name = mediaInfo.name() != nil ? mediaInfo.name() : ""
                                        mediaStream.instreamId = mediaInfo.instreamId() != nil ? mediaInfo.instreamId() : ""
                                        mediaStream.characteristics = mediaInfo.characteristics() != nil ? mediaInfo.characteristics() : ""
                                        mediaStream.isDefault = Bool(mediaInfo.isDefault())
                                        mediaStream.autoSelect = Bool(mediaInfo.autoSelect())
                                        mediaStream.forced = Bool(mediaInfo.forced())
                                        
                                        //update content
                                        var url : String = "subtitles".stringByAppendingPathComponent(mediaStream.URI.md5().stringByAppendingPathExtension("m3u8")!)
                                        updatedFileContent = updatedFileContent.stringByReplacingOccurrencesOfString(mediaStream.URI, withString: url)
                                        mediaStream.fileSavePath = url
                                        
                                        item.subtitles.append(mediaStream)
                                    }
                                    
                                    //update file
                                    updatedFileContent.writeToFile(fileSavePath, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
                                    
                                    completion(updatedItem: item , success: Bool(true))
                                }
                                else {
                                    completion(updatedItem: item, success: Bool(false))
                                }
                            }
                            else {
                                completion(updatedItem: item, success: Bool(false))
                            }
                        }).resume()
                    }
                    else {
                        //m3u8 file is large than expected (5mb)
                        completion(updatedItem: item, success: Bool(false))
                    }
                }
            }).resume()
        }
        else {
            completion(updatedItem: item, success: Bool(false))
        }
    }

    override func addDownloadItem(item: KADownloadItem) {
        super.addDownloadItem(item)
        
        var url          : NSURL = NSURL(string: item.url)!
        
        for i in 0..<item.selectedVideoStreams.count {
            var video : KADownloadItem_video = item.selectedVideoStreams[i] as KADownloadItem_video;
            var fileURL : String = item.url.stringByDeletingLastPathComponent.stringByAppendingPathComponent(video.URI)

            var u : KADownloadItemUrl = KADownloadItemUrl(URI: fileURL, fileSavePath: item.contentFolderPath.stringByAppendingPathComponent(video.fileSavePath), parentItemId: item.ID, expected: 0, written: 0)

            self.arrDownloadingData.addObject(u)

            self.addInternalDownloadTask(u)
        }
        
        //notify delegate
        self.delegate?.downloadWillStart(item.ID)
    }
    
    override func setSessionManagerBlocks() {
        super.setSessionManagerBlocks()
        self.sessionManager.setDownloadTaskDidWriteDataBlock { (session: NSURLSession!, downloadTask: NSURLSessionDownloadTask!, bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) -> Void in
            var itemUrl : KADownloadItemUrl? = self.getKADownloadItemUrl(downloadTask.originalRequest.URL!.absoluteString!)
            
            if let item = itemUrl {
                item.written += bytesWritten
                item.expected = totalBytesExpectedToWrite
                
                var arrDownloadingDataForParentItemId : NSArray = self.getAllRelatedKADownloadItemUrls(item)
                var arrDownloadedDataForParentItemId : NSArray = self.getAllRelatedCompletedKADownloadItemUrls(item)
                var totalExpectedForParent = arrDownloadingDataForParentItemId.valueForKeyPath("@sum.expected") as! NSNumber
                var totalWrittenForParent  = arrDownloadingDataForParentItemId.valueForKeyPath("@sum.written") as! NSNumber
                var totalCompletedWrittenForParent  = arrDownloadedDataForParentItemId.valueForKeyPath("@sum.expected") as! NSNumber
                
                var totalExpected : Double = Double(totalExpectedForParent.longLongValue + totalCompletedWrittenForParent.longLongValue)
                var totalWritten : Double = Double(totalWrittenForParent.longLongValue + totalCompletedWrittenForParent.longLongValue)
                
                if (totalExpected > 0) {
                    var progress : Double = totalWritten / totalExpected
                    
                    if progress <= 1 {
                        self.delegate?.downloadInProgress(item.parentItemId, progress: progress)
                    }
                }
            }
        }
        
        self.sessionManager.setDownloadTaskDidFinishDownloadingBlock { (session: NSURLSession!, downloadTask: NSURLSessionDownloadTask!, location: NSURL!) -> NSURL! in
            var itemUrl : KADownloadItemUrl? = self.getKADownloadItemUrl(downloadTask.originalRequest.URL!.absoluteString!)
            
            if let item = itemUrl {
                var error       : NSError?
                let fileManager = NSFileManager.defaultManager()
                var success     : Bool = fileManager.moveItemAtURL(location, toURL: NSURL(fileURLWithPath: item.fileSavePath)!, error: &error)
                if let err = error {
                    if err.code == 516 { //file exists
                        fileManager.removeItemAtPath(item.fileSavePath, error: nil)
                        success = fileManager.moveItemAtURL(location, toURL: NSURL(fileURLWithPath: item.fileSavePath)!, error: &error)
                    }
                }
                
                //remove completed task
                self.arrDownloadingData.removeObject(item)
                self.arrDownloadedData.addObject(item)
                
                //remained downloads for parentItemId
                var arrDownloadingDataForParentItemId : NSArray = self.getAllRelatedKADownloadItemUrls(item)
                
                var urlExtension : NSString? = item.fileSavePath.lastPathComponent.pathExtension
                let hasM3U8 = (item.URI as NSString).rangeOfString(".m3u8").location != NSNotFound
                
                if (hasM3U8 && urlExtension == "file") || urlExtension == "m3u8" {
                    
                    var fileContent = String(contentsOfFile: item.fileSavePath as String, encoding: NSUTF8StringEncoding, error: nil)
                    
                    if fileContent!.isExtendedM3Ufile() {
                        var mediaList : M3U8MediaPlaylistLocal = M3U8MediaPlaylistLocal.initWithContent(fileContent, baseURL: nil)
                        var urls : NSArray? = mediaList.segmentList.valueForKeyPath("segmentInfoList.@distinctUnionOfObjects.URI") as? NSArray
                        
                        if let segments = urls {
                            var arrDownloadItemUrls : NSMutableArray = NSMutableArray()
                            
                            //get first item expected size to apply to all
                            let group : dispatch_group_t = dispatch_group_create()
                            let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                            var expectedSizeForSingleFile : Int64 = 1024000
                            if segments.count > 0 {
                                let itemIndexToTakeForSize = (segments.count > 10) ? 10 : 0
                                dispatch_group_enter(group)
                                var fileURL = item.URI.stringByDeletingLastPathComponent.stringByAppendingPathComponent(segments[itemIndexToTakeForSize] as! String)
                                var request: NSMutableURLRequest = NSMutableURLRequest(URL: NSURL(string: fileURL)!)
                                request.HTTPMethod = "HEAD"
                                NSURLSession.sharedSession().downloadTaskWithRequest(request, completionHandler: { (url: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
                                    if (error == nil) {
                                        expectedSizeForSingleFile = response.expectedContentLength
                                    }
                                    dispatch_group_leave(group)
                                }).resume()
                            }
                            
                            dispatch_group_notify(group, queue, {
                                for url in segments {
                                    var fileURL = item.URI.stringByDeletingLastPathComponent.stringByAppendingPathComponent(url as! String)
                                    var fileSavePath = item.fileSavePath.stringByDeletingLastPathComponent.stringByAppendingPathComponent(url as! String)
                                    
                                    var di : KADownloadItemUrl = KADownloadItemUrl(URI: fileURL, fileSavePath: fileSavePath, parentItemId: item.parentItemId, expected: expectedSizeForSingleFile, written: 0)
                                    arrDownloadItemUrls.addObject(di)
                                }

//                                //get ts expected sizes
//                                for itemUrl in arrDownloadItemUrls {
//                                    if let u = itemUrl as? KADownloadItemUrl {
//
//                                        var request: NSMutableURLRequest = NSMutableURLRequest(URL: NSURL(string: u.URI)!)
//                                        request.HTTPMethod = "HEAD"
//                                        NSURLSession.sharedSession().downloadTaskWithRequest(request, completionHandler: { (url: NSURL!, response: NSURLResponse!, error: NSError!) -> Void in
//                                            if (error == nil) {
//                                                //update existing item
//                                                if let di = self.getKADownloadItemUrl(request.URL!.absoluteString!) {
//                                                    di.expected = response.expectedContentLength
//                                                }
//                                            }
//                                        }).resume()
//                                    }
//                                }
                                
                                //start download even if we dont have all heads for ts files to avoid delaying in download
                                self.delegate?.downloadStarted(item.parentItemId, arrDownloadItemUrls: arrDownloadItemUrls, completion: { (success) -> Void in
                                    if success {
                                        self.arrDownloadingData.addObjectsFromArray(arrDownloadItemUrls as [AnyObject])
                                        
                                        for itemUrl in arrDownloadItemUrls {
                                            if let u = itemUrl as? KADownloadItemUrl {
                                                self.addInternalDownloadTask(u)
                                            }
                                        }
                                    }
                                })
                            })
                        }
                    }
                }
                else {
                    self.delegate?.downloadFinishedForItemUrl(item, completion: { (success) -> Void in
                        if arrDownloadingDataForParentItemId.count == 0 {
                            //remove same if exists
                            self.removeAllSameKADownloadItemUrls(item)
                            //check if there are more parts to download
                            self.delegate?.getDownloadItemUrls(itemUrl!.parentItemId, completion: { (arrNotCompletedItemUrls : NSMutableArray, arrCompletedItemUrls : NSMutableArray) -> Void in
                                //continue not completed download parts if not currently in list
                                if arrNotCompletedItemUrls.count > 0 {
                                    self.arrDownloadingData.addObjectsFromArray(arrNotCompletedItemUrls as [AnyObject])
                                    for url in arrNotCompletedItemUrls {
                                        println("\((url as! KADownloadItemUrl).URI)")
                                        self.addInternalDownloadTask(url as! KADownloadItemUrl)
                                    }
                                }
                                else {
                                    //post completion
                                    self.performActionOnDownloadComplete(itemUrl!.parentItemId, completion: { (success) -> Void in
                                        if success == true {
                                            self.delegate?.downloadFinished(item.parentItemId)
                                        }
                                    })
                                }
                            })
                        }
                        else {
                            self.removeAllSameKADownloadItemUrls(item)
                        }
                    })
                }
                
                return NSURL(fileURLWithPath: item.fileSavePath)
            }
            else {
                return location
            }
        }
        
        self.sessionManager.setTaskDidCompleteBlock { (session: NSURLSession!, task: NSURLSessionTask!, error: NSError!) -> Void in
            var fileURL : String = task.currentRequest.URL!.absoluteString!
            var itemUrl : KADownloadItemUrl? = self.arrDownloadingData.filteredArrayUsingPredicate(NSPredicate(format: "self.URI = %@", task.originalRequest.URL!.absoluteString!)).first as? KADownloadItemUrl
            
            if let error = error {
                println("Download complete with error: \(error.userInfo)")
                let errorUserInfo : NSDictionary? = error.userInfo
                if let hasReasonKey: AnyObject = errorUserInfo?.objectForKey("NSURLErrorBackgroundTaskCancelledReasonKey") {
                    let errorReasonNum : Int = Int(errorUserInfo?.objectForKey("NSURLErrorBackgroundTaskCancelledReasonKey") as! NSNumber)
                    if errorReasonNum == NSURLErrorCancelledReasonUserForceQuitApplication || errorReasonNum == NSURLErrorCancelledReasonBackgroundUpdatesDisabled {
                        
                        var resumeData : NSData? = errorUserInfo?.objectForKey(NSURLSessionDownloadTaskResumeData) as? NSData
                        var newTask = task
                        
                        if let resumeData = resumeData {
                            var hasValidResumeData : Bool = self.isValidResumeData(resumeData)
                            if hasValidResumeData == true {
                                newTask = self.sessionManager.session.downloadTaskWithResumeData(resumeData)
                                if (itemUrl != nil) {
                                    resumeData.writeToFile(itemUrl!.fileSavePath.stringByAppendingPathExtension("data")!, atomically: true)
                                }
                            } else {
                                newTask = self.sessionManager.session.downloadTaskWithURL(NSURL(string: fileURL as String)!)
                            }
                        }
                        else {
                            newTask = self.sessionManager.session.downloadTaskWithURL(NSURL(string: fileURL as String)!)
                        }
                        
                        newTask.suspend()
                        return
                    }
                }
                else if error.code != NSURLErrorCancelled {
                    var resumeData  : NSData? = errorUserInfo?.objectForKey(NSURLSessionDownloadTaskResumeData) as? NSData
                    var newTask = task
                    
                    if let resumeData = resumeData {
                        var hasValidResumeData : Bool = self.isValidResumeData(resumeData)
                        if hasValidResumeData == true {
                            newTask = self.sessionManager.session.downloadTaskWithResumeData(resumeData)
                            if (itemUrl != nil) {
                                resumeData.writeToFile(itemUrl!.fileSavePath.stringByAppendingPathExtension("data")!, atomically: true)
                            }
                        }
                        else {
                            newTask = self.sessionManager.session.downloadTaskWithURL(NSURL(string: fileURL as String)!)
                        }
                        
                        if (newTask != nil) {
                            if let item = itemUrl {
                                self.delegate?.getDownloadItemState(item.parentItemId, completion: { (state) -> Void in
                                    if state == KADownloadItemDownloadState.InProgress {
                                        newTask.resume()
                                    }
                                    else {
                                        newTask.suspend()
                                    }
                                })
                            }
                            else {
                                newTask.suspend()
                            }
                        }
                        else {
                            task.cancel()
                        }
                    }
                    else {
                        if let item = itemUrl {
                            if error.code == -997 || error.localizedDescription == "Lost connection to background transfer service" {
                                self.removeDownloadItemTasks(item.parentItemId)
                                
                                //notify delegate
                                self.delegate?.downloadFinishedWithFailure(item.parentItemId, error: error)
                            }
                            else {
                                self.removeAllSameKADownloadItemUrls(item)
                                self.delegate?.getDownloadItemState(item.parentItemId, completion: { (state) -> Void in
                                    self.addInternalDownloadTask(item, resume: (state == KADownloadItemDownloadState.InProgress))
                                })
                                
                                task.cancel()
                            }
                        }
                        else {
                            task.cancel()
                        }
                    }
                }
                else {
                    if let item = itemUrl {
                        if error.code == -997 || error.localizedDescription == "Lost connection to background transfer service" {
                            self.removeDownloadItemTasks(item.parentItemId)
                            
                            //notify delegate
                            self.delegate?.downloadFinishedWithFailure(item.parentItemId, error: error)
                        }
                    }
                }
            }
        }
    }
    override func performActionOnDownloadComplete(itemId: String, completion: (success: Bool) -> Void) {
        
        self.delegate?.getDownloadItemById(itemId, completion: { (item : KADownloadItem) -> Void in
            self.updateDownloadedItemLicense(item, completion: completion)
        })
    }
    
    // MARK: - License handle
    override func updateDownloadedItemLicense(item: KADownloadItem, completion: (success: Bool) -> Void) {
        if let assetPath = item.playUrl.absoluteString {
            println("assetPath: \(assetPath)")
            let success = PlayReadyDRMadapter.registerAsset(assetPath)
            completion(success: success)
        }
    }

    override func isDownloadedItemLicenseValid(item: KADownloadItem, completion: (success: Bool, remainingTime: NSTimeInterval) -> Void) {
        if let assetPath = item.playUrl.absoluteString {
            println("assetPath: \(assetPath)")
            PlayReadyDRMadapter.isLicenseValid(assetPath, completion: completion)
        }
        else {
            completion(success: Bool(false), remainingTime: -1)
        }
    }
}
