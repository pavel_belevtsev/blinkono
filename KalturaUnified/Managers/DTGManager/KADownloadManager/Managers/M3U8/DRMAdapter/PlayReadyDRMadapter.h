//
//  PlayReadyDRMadapter.h
//  KalturaUnified
//
//  Created by Alex Zchut on 6/14/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlayReadyDRMadapter : NSObject

+(BOOL) registerAsset:(NSString*)localPath;
+(void) isLicenseValid:(NSString*)assetPath completion:(void(^)(BOOL success, NSTimeInterval remainingTime))completion;
@end
