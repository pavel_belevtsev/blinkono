//
//  PlayReadyDRMadapter.m
//  KalturaUnified
//
//  Created by Alex Zchut on 6/14/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayReadyDRMadapter.h"

#ifdef PLAYER_DX
#import <SecurePlayer/SecurePlayer.h>
#endif

@implementation PlayReadyDRMadapter

+(BOOL) registerAsset:(NSString*)assetPath {
    return TRUE;
}

+(void) isLicenseValid:(NSString*)assetPath completion:(void(^)(BOOL success, NSTimeInterval remainingTime))completion {
    
//#ifdef PLAYER_DX
//
//    DxDrmManagerStatus status;
//    NSArray * rightsInfoObjects = [[DxDrmManager sharedManager] getRightObjectsForFile:assetPath result:&status];
//    DxRightsInfo * rightsInfo = [rightsInfoObjects lastObject];
//
//    BOOL isIntervalValid = (rightsInfo.isIntervalConstrained && rightsInfo.intervalPeriodInSeconds > 0);
//    BOOL isCountsValid = (rightsInfo.isCountConstrained && rightsInfo.countLeft > 0);
//
//    if (isIntervalValid) {
//        completion(rightsInfo.rightsStatus, rightsInfo.intervalPeriodInSeconds);
//    }
//    else if (isCountsValid) {
//        completion(rightsInfo.rightsStatus, rightsInfo.countLeft);
//    }
//    else {
//        completion(rightsInfo.rightsStatus, 10000);
//    }
//
//#endif
    completion(TRUE, 10000);
}

@end

