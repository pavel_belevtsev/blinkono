//
//  KADownloadSettings.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 5/31/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import Foundation

public class KADownloadSettings: NSObject {
    
    // MARK: - Public static variables
    public static let hasDefaultSettings = "hasDefaultSettings"
    public static let enableDownloadOnCellularData = "enableDownloadOnCellularData"
    public static let autoResumeDownloads = "autoResumeDownloads"
    public static let downloadQuota = "downloadQuota"
    public static let downloadQuality = "downloadQuality"
    
    public static let simultaneousDownloadItems = "simultaneousDownloadItems"
    public static let timeoutIntervalForRequest = "timeoutIntervalForRequest"
    public static let timeoutIntervalForResource = "timeoutIntervalForResource"
    public static let httpMaximumConnectionsPerHost = "httpMaximumConnectionsPerHost"
    public static let maxRetriesCountAllowed = "maxRetriesCountAllowed"

    public static let parentalCode = "parentalCode"

    // MARK: - Private variables
    private weak var localDB : KALocalDB?
    
    init(db: KALocalDB) {
        super.init()
        self.localDB = db
    }

    // MARK: - functions
    func getSettingsKey(key: String, completion: (value: String) -> Void) {
        self.localDB?.getSettingsKey(key, completion: { (value) -> Void in
            completion(value: value)
        })
    }
    
    func getAllSettings(completion: (dict: NSMutableDictionary) -> Void) {
        self.localDB?.getAllSettings { (dict) -> Void in
            completion(dict: dict)
        }
    }
    
    func setSettingsKey(key: String, value: String, completion: (success: Bool) -> Void) {
        self.localDB?.addSettingsKey(key, value: value) { (success) -> Void in
            completion(success: success)
        }
    }
    
    func setBulkSettings(dict: NSMutableDictionary, completion: (success: Bool) -> Void) {
        self.localDB?.addBulkSettings(dict) { (success) -> Void in
            completion(success: success)
        }
    }
    
}