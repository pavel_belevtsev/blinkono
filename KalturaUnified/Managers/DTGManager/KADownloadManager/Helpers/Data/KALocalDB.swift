//
//  DTGLocalData.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 4/30/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import Foundation

class KALocalDB: NSObject {
    
    var databasePath = NSString()
    
    //MARK: constractor
    override init() {
        super.init()

        let filemgr = NSFileManager.defaultManager()
        databasePath = baseDestination.stringByAppendingPathComponent("KADownloadManager.db")
        
        KAFileSavingUtility.createFolderAtPath(baseDestination as String)
        
        if !filemgr.fileExistsAtPath(databasePath as String) {
            let db = FMDatabase(path: databasePath as String)
            
            if db == nil {
                println("Error: \(db.lastErrorMessage())")
            }
            
            if db.open() {
                let sql_stmt1 = "CREATE TABLE IF NOT EXISTS Items (ID INTEGER PRIMARY KEY AUTOINCREMENT, itemId NVARCHAR(200), itemUrl TEXT, itemFormat TEXT, state INTEGER, hasWatchRestriction BOOLEAN, firstWatchDate DOUBLE, retries INTEGER, createDate DOUBLE, updateDate DOUBLE)"
                if !db.executeStatements(sql_stmt1) {
                    println("Error: \(db.lastErrorMessage())")
                }
                
                let sql_stmt2 = "CREATE TABLE IF NOT EXISTS ItemVideoStreams (ID INTEGER PRIMARY KEY AUTOINCREMENT, itemId NVARCHAR(200), bandwidth INTEGER, programId INTEGER, codecs TEXT, audio TEXT, video TEXT, subtitles TEXT, closedCaptions TEXT, URI TEXT, fileSavePath TEXT, m3u8UrlStr TEXT, isSelected BOOLEAN)"
                if !db.executeStatements(sql_stmt2) {
                    println("Error: \(db.lastErrorMessage())")
                }
                
                let sql_stmt3 = "CREATE TABLE IF NOT EXISTS ItemAudioStreams (ID INTEGER PRIMARY KEY AUTOINCREMENT, itemId NVARCHAR(200), type TEXT, URI TEXT, fileSavePath TEXT, groupId TEXT, language TEXT, assocLanguage TEXT, name TEXT, instreamId TEXT, characteristics TEXT, isDefault INTEGER, autoSelect INTEGER, forced INTEGER, m3u8UrlStr TEXT, isSelected BOOLEAN)"
                if !db.executeStatements(sql_stmt3) {
                    println("Error: \(db.lastErrorMessage())")
                }
                
                let sql_stmt4 = "CREATE TABLE IF NOT EXISTS ItemSubtitleStreams (ID INTEGER PRIMARY KEY AUTOINCREMENT, itemId NVARCHAR(200), type TEXT, URI TEXT, fileSavePath TEXT, groupId TEXT, language TEXT, assocLanguage TEXT, name TEXT, instreamId TEXT, characteristics TEXT, isDefault INTEGER, autoSelect INTEGER, forced INTEGER, m3u8UrlStr TEXT, isSelected BOOLEAN)"
                if !db.executeStatements(sql_stmt4) {
                    println("Error: \(db.lastErrorMessage())")
                }
                
                let sql_stmt5 = "CREATE TABLE IF NOT EXISTS ItemUrls (ID INTEGER PRIMARY KEY AUTOINCREMENT, itemId NVARCHAR(200), URI TEXT, fileSavePath TEXT, expected INTEGER, written INTEGER, isCompleted BOOLEAN)"
                if !db.executeStatements(sql_stmt5) {
                    println("Error: \(db.lastErrorMessage())")
                }
                
                let sql_stmt6 = "CREATE TABLE IF NOT EXISTS Settings (ID INTEGER PRIMARY KEY AUTOINCREMENT, key NVARCHAR(200), value NVARCHAR(200), updateDate DOUBLE)"
                if !db.executeStatements(sql_stmt6) {
                    println("Error: \(db.lastErrorMessage())")
                }
                
                let sql_stmt7 = "CREATE TABLE IF NOT EXISTS ItemMediaMark (ID INTEGER PRIMARY KEY AUTOINCREMENT, itemId NVARCHAR(200), status INTEGER, groupId INTEGER, locationSec DOUBLE, mediaId NVARCHAR(200), deviceId NVARCHAR(200), deviceName TEXT, siteGUID INTEGER, updateDate DOUBLE)"
                if !db.executeStatements(sql_stmt7) {
                    println("Error: \(db.lastErrorMessage())")
                }
                
                db.close()
            }
            else {
                println("Error: \(db.lastErrorMessage())")
            }
        }
    }
    
    // MARK: - Add data

    func addItem(item: KADownloadItem, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            
            let insertSQL = "INSERT INTO Items (itemId, itemUrl, itemFormat, state, firstWatchDate, hasWatchRestriction, retries, createDate, updateDate) VALUES ('\(item.ID)', '\(item.url)', '\(item.format)', \(item.downloadState.rawValue), 0, \(item.hasWatchRestriction ? 1 : 0), 0, \(NSDate().timeIntervalSince1970), \(NSDate().timeIntervalSince1970))"
            
            if !db.executeUpdate(insertSQL, withArgumentsInArray: nil) {
                println("Error: \(db.lastErrorMessage())")
                db.close()
                completion(success: Bool(false))
            }
            else {
                item.DBID = 999999
                if let mediaMark = item.tempMediaMark {
                    let insertSQLmm = "INSERT INTO ItemMediaMark (itemId, status, groupId, locationSec, mediaId, deviceId, deviceName, siteGUID, updateDate) VALUES ('\(item.ID)', \(mediaMark.status), \(mediaMark.groupId), \(mediaMark.locationSec), '\(mediaMark.mediaId)', '\(mediaMark.deviceId)', '\(mediaMark.deviceName)', '\(mediaMark.siteGUID)', \(NSDate().timeIntervalSince1970))"
                    db.executeUpdate(insertSQLmm, withArgumentsInArray: nil)
                }
                self.addItemStreams(item, completion: { (success) -> Void in
                    if (success) {
                        db.close()
                        completion(success: Bool(true))
                    }
                })
            }
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    func addItemStreams(item: KADownloadItem, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            var insertSQL : String = ""
            for streamInfo in item.videoStreams as NSArray as! [KADownloadItem_video] {
                streamInfo.emptyingNullStrings()
                
                insertSQL = insertSQL.stringByAppendingString("INSERT INTO ItemVideoStreams (itemId, bandwidth, programId, codecs, audio, video, subtitles, closedCaptions, URI, fileSavePath, m3u8UrlStr, isSelected) VALUES ('\(item.ID)', \(streamInfo.bandwidth), \(streamInfo.programId), '\(streamInfo.codecs)', '\(streamInfo.audio)', '\(streamInfo.video)', '\(streamInfo.subtitles)', '\(streamInfo.closedCaptions)', '\(streamInfo.URI)', '\(streamInfo.fileSavePath)', '\(streamInfo.m3u8UrlStr)', 0); ")
            }
            for streamInfo in item.audioStreams as NSArray as! [KADownloadItem_media] {
                streamInfo.emptyingNullStrings()

                insertSQL = insertSQL.stringByAppendingString("INSERT INTO ItemAudioStreams (itemId, type, URI, fileSavePath, groupId, language, assocLanguage, name, instreamId, characteristics, isDefault, autoSelect, forced, m3u8UrlStr, isSelected) VALUES ('\(item.ID)', '\(streamInfo.type)', '\(streamInfo.URI)', '\(streamInfo.fileSavePath)', '\(streamInfo.groupId)', '\(streamInfo.language)', '\(streamInfo.assocLanguage)', '\(streamInfo.name)', '\(streamInfo.instreamId)', '\(streamInfo.characteristics)', \(streamInfo.isDefault), \(streamInfo.autoSelect), \(streamInfo.forced), '\(streamInfo.m3u8UrlStr)', 0);")
            }
            
            for streamInfo in item.subtitles as NSArray as! [KADownloadItem_media]  {
                streamInfo.emptyingNullStrings()

                insertSQL = insertSQL.stringByAppendingString("INSERT INTO ItemSubtitleStreams (itemId, type, URI, fileSavePath, groupId, language, assocLanguage, name, instreamId, characteristics, isDefault, autoSelect, forced, m3u8UrlStr, isSelected) VALUES ('\(item.ID)', '\(streamInfo.type)', '\(streamInfo.URI)', '\(streamInfo.fileSavePath)', '\(streamInfo.groupId)', '\(streamInfo.language)', '\(streamInfo.assocLanguage)', '\(streamInfo.name)', '\(streamInfo.instreamId)', '\(streamInfo.characteristics)', \(streamInfo.isDefault), \(streamInfo.autoSelect), \(streamInfo.forced), '\(streamInfo.m3u8UrlStr)', 1);")
            }
            
            if db.executeUpdate(insertSQL, withArgumentsInArray: nil) {
                self.updateItemState(item, completion: { (success) -> Void in
                    completion(success: success)
                })
            }
            else {
                println("Error: \(db.lastErrorMessage())")
                completion(success: Bool(false))
            }
        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    func insertItemUrls(itemId: String, arrDownloadItemUrls: NSMutableArray, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            
            let deleteSQL = "DELETE FROM ItemUrls WHERE itemId = '\(itemId)'"
            db.executeUpdate(deleteSQL, withArgumentsInArray: nil)
            
            var insertSQL : String = ""
            for i in 0..<arrDownloadItemUrls.count {
                var itemUrl : KADownloadItemUrl = arrDownloadItemUrls[i] as! KADownloadItemUrl
                insertSQL = insertSQL.stringByAppendingString("INSERT INTO ItemUrls (itemId, URI, fileSavePath, expected, written, isCompleted) VALUES ('\(itemUrl.parentItemId)', '\(itemUrl.URI)', '\(itemUrl.fileSavePath)', \(itemUrl.expected), 0, 0);")
            }
            
            if !db.executeStatements(insertSQL){
                println("Error: \(db.lastErrorMessage())")
                completion(success: Bool(false))
            }
            else {
                completion(success: Bool(true))
            }
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    func addSettingsKey(key: String, value: String, completion: (success: Bool) -> Void) {
        self.addBulkSettings(NSDictionary(object: value, forKey: key), completion: completion);
    }
    
    func addBulkSettings(dict: NSDictionary, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        var insertSQL : String = ""
        if db.open() {
            for (key, value) in dict {
                insertSQL = insertSQL.stringByAppendingString("DELETE FROM Settings WHERE key = '\(key)'; INSERT INTO Settings (key, value, updateDate) VALUES ('\(key)', '\(value)', \(NSDate().timeIntervalSince1970));")
            }
            if !db.executeStatements(insertSQL){
                println("Error: \(db.lastErrorMessage())")
                db.close()
                completion(success: Bool(false))
            }
            else {
                db.close()
                completion(success: Bool(true))
            }
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    // MARK: - Update data
    func updateSelectedVideoStreams(item: KADownloadItem, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        let array = NSArray(array: item.selectedVideoStreams)
        var iDs : String = array.valueForKeyPath("@distinctUnionOfObjects.ID")!.componentsJoinedByString(",")

        if db.open() {
            let insertSQL = "UPDATE ItemVideoStreams SET isSelected = 0 WHERE itemId = '\(item.ID)'; UPDATE ItemVideoStreams SET isSelected = 1 WHERE ID in (\(iDs)"
            
            if !db.executeUpdate(insertSQL, withArgumentsInArray: nil) {
                println("Error: \(db.lastErrorMessage())")
            }
            else {
                
            }
            completion(success: Bool(true))
            db.close()
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    func updateSelectedAudioStreams(item: KADownloadItem, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        let array = NSArray(array: item.selectedAudioStreams)
        var iDs : String = array.valueForKeyPath("@distinctUnionOfObjects.ID")!.componentsJoinedByString(",")

        if db.open() {
            
            let insertSQL = "UPDATE ItemAudioStreams SET isSelected = 0 WHERE itemId = '\(item.ID)'; UPDATE ItemAudioStreams SET isSelected = 1 WHERE ID in (\(iDs)"
            
            if !db.executeUpdate(insertSQL, withArgumentsInArray: nil) {
                println("Error: \(db.lastErrorMessage())")
            }
            db.close()

            completion(success: Bool(true))
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    func updateSelectedSubtitles(item: KADownloadItem, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        let array = NSArray(array: item.selectedSubtitles)
        var iDs : String = array.valueForKeyPath("@distinctUnionOfObjects.ID")!.componentsJoinedByString(",")
        
        if db.open() {
            
            let insertSQL = "UPDATE ItemSubtitleStreams SET isSelected = 0 WHERE itemId = '\(item.ID)'; UPDATE ItemSubtitleStreams SET isSelected = 1 WHERE ID in (\(iDs)"
            
            if !db.executeUpdate(insertSQL, withArgumentsInArray: nil) {
                println("Error: \(db.lastErrorMessage())")
            }
            db.close()
            completion(success: Bool(true))
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    
    func updateItemState(item: KADownloadItem, completion: (success: Bool) -> Void) {
        self.updateItemState(item.ID, state: item.downloadState, completion: completion)
    }
    
    func updateItemState(itemId: String, state: KADownloadItemDownloadState, completion: (success: Bool) -> Void) {
        var str : String = "'\(itemId)'"
        self.updateItemsState([str], state: state, completion: completion)
    }
    
    func updateItemsState(itemIds: NSArray, state: KADownloadItemDownloadState, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        var ids : String = itemIds.componentsJoinedByString(",")
        if db.open() {
            
            let insertSQL = "UPDATE Items SET state = \(state.rawValue), updateDate = \(NSDate().timeIntervalSince1970) WHERE itemId in (\(ids))"
            
            if !db.executeUpdate(insertSQL, withArgumentsInArray: nil) {
                println("Error: \(db.lastErrorMessage())")
                completion(success: Bool(false))
            }
            else {
                completion(success: Bool(true))
            }
            db.close()
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    func updateItemFirstWatchDate(item: KADownloadItem, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        if db.open() {
            let insertSQL = "UPDATE Items SET firstWatchDate = \(item.firstWatchDate) WHERE itemId = '\(item.ID)'"
            
            if !db.executeUpdate(insertSQL, withArgumentsInArray: nil) {
                println("Error: \(db.lastErrorMessage())")
                completion(success: Bool(false))
            }
            else {
                completion(success: Bool(true))
            }
            db.close()
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    func updateItemUrlStatus(itemUrl : KADownloadItemUrl, completion: (success: Bool) -> Void) {
        self.updateItemUrlsStatus([itemUrl], completion: completion)
    }
    
    func updateItemUrlsStatus(arrUrls : NSArray, completion: (success: Bool) -> Void) {
        if arrUrls.count > 0 {
            let db = FMDatabase(path: databasePath as String)
            
            var updateSQL : String = ""
            if db.open() {
                for itemUrl in arrUrls as! [KADownloadItemUrl]{
                    if itemUrl.expected >= itemUrl.written {
                        updateSQL = updateSQL.stringByAppendingString("UPDATE ItemUrls SET written = \(itemUrl.written), expected = \(itemUrl.expected), isCompleted = \(itemUrl.expected == itemUrl.written ? 1 : 0) WHERE itemId = '\(itemUrl.parentItemId)' AND URI = '\(itemUrl.URI)';")
                    }
                }
                db.executeUpdate(updateSQL, withArgumentsInArray: nil)
                completion(success: Bool(true))
                db.close()

            } else {
                println("Error: \(db.lastErrorMessage())")
                completion(success: Bool(false))
            }
        }
        else {
            completion(success: Bool(true))
        }
    }
    
    func updateItemRetry(itemId: String, completion: (retriesCount: Int32) -> Void) {
        var retriesCount : Int32 = 0
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let insertSQL = "UPDATE Items SET retries = retries + 1 WHERE itemId = '\(itemId)'"
            db.executeUpdate(insertSQL, withArgumentsInArray: nil)

            let querySQL = "SELECT retries FROM Items WHERE itemId = '\(itemId)'"
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                if rs.next() == true {
                    retriesCount = rs.intForColumn("retries")
                }
                completion(retriesCount: retriesCount)
            }

            db.close()
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(retriesCount: retriesCount)
        }
    }
    
    func updateItemWatchRestriction(item: KADownloadItem, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        if db.open() {
            
            let insertSQL = "UPDATE Items SET hasWatchRestriction = \(item.hasWatchRestriction ? 1 : 0) WHERE itemId = '\(item.ID)'"
            
            if !db.executeUpdate(insertSQL, withArgumentsInArray: nil) {
                println("Error: \(db.lastErrorMessage())")
                completion(success: Bool(false))
            }
            else {
                completion(success: Bool(true))
            }
            db.close()
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    func updateItemMediaMark(item: KADownloadItem, completion: (success: Bool) -> Void) {
        
        if let mediaMark = item.mediaMark {
            let db = FMDatabase(path: databasePath as String)
            if db.open() {
                let insertSQL = "UPDATE ItemMediaMark SET status = \(mediaMark.status), groupId = \(mediaMark.groupId), updateDate = \(NSDate().timeIntervalSince1970), locationSec = \(mediaMark.locationSec) WHERE itemId = '\(item.ID)' and siteGUID = '\(mediaMark.siteGUID)'"
                
                if !db.executeUpdate(insertSQL, withArgumentsInArray: nil) {
                    println("Error: \(db.lastErrorMessage())")
                    completion(success: Bool(false))
                }
                else {
                    completion(success: Bool(true))
                }
                db.close()
                
            } else {
                println("Error: \(db.lastErrorMessage())")
                completion(success: Bool(false))
            }
        }
        else {
            completion(success: Bool(false))
        }
    }
    
    func updateItemWatchStatus(item: KADownloadItem, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        if db.open() {
            
            let insertSQL = "UPDATE Items SET firstWatchDate = \(item.firstWatchDate) WHERE itemId = '\(item.ID)'"
            
            if !db.executeUpdate(insertSQL, withArgumentsInArray: nil) {
                println("Error: \(db.lastErrorMessage())")
                completion(success: Bool(false))
            }
            else {
                completion(success: Bool(true))
            }
            db.close()
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    // MARK: - Retrieve data

    func getItemState(itemId: String, completion: (state: KADownloadItemDownloadState) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let querySQL = "SELECT state FROM Items WHERE itemId = '\(itemId)'"
            
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                if rs.next() == true {
                    let state: KADownloadItemDownloadState = KADownloadItemDownloadState(rawValue: Int32(rs.intForColumn("state")))!
                    completion(state: state)
                }
                else {
                    completion(state: KADownloadItemDownloadState.Failed)
                }
            }
            db.close()
        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(state: KADownloadItemDownloadState.Failed)
        }
    }
    
    func getItemUrl(url: String, completion: (obj: KADownloadItemUrl?) -> Void) {
        var itemUrl : KADownloadItemUrl?
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let querySQL = "SELECT itemId, URI, fileSavePath, expected, written, isCompleted FROM ItemUrls WHERE URI = '\(url)'"
            
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                if rs.next() == true {
                    itemUrl = KADownloadItemUrl(URI: rs.stringForColumn("URI"), fileSavePath: rs.stringForColumn("fileSavePath"), parentItemId: rs.stringForColumn("itemId"), expected: rs.longLongIntForColumn("expected"), written: rs.longLongIntForColumn("written"))
                }
                completion(obj: itemUrl)
            }
            db.close()
        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(obj: itemUrl)
        }
    }
    
    func getItemUrls(itemId: String, completion: (arrNotCompleted: NSMutableArray, arrCompleted: NSMutableArray) -> Void) {
        var arrCompleted : NSMutableArray = NSMutableArray()
        var arrNotCompleted : NSMutableArray = NSMutableArray()
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let querySQL = "SELECT itemId, URI, fileSavePath, expected, written, isCompleted FROM ItemUrls WHERE itemId = '\(itemId)'"
            
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                while rs.next() == true {
                    var itemUrl : KADownloadItemUrl = KADownloadItemUrl(URI: rs.stringForColumn("URI"), fileSavePath: rs.stringForColumn("fileSavePath"), parentItemId: rs.stringForColumn("itemId"), expected: rs.longLongIntForColumn("expected"), written: rs.longLongIntForColumn("written"))
                    
                    if rs.boolForColumn("isCompleted") {
                        arrCompleted.addObject(itemUrl)
                    }
                    else {
                        arrNotCompleted.addObject(itemUrl)
                    }
                }
                completion(arrNotCompleted: arrNotCompleted, arrCompleted: arrCompleted)
            }
            db.close()
        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(arrNotCompleted: arrNotCompleted, arrCompleted: arrCompleted)
        }
    }
    
    func getFirstItemForState(state: KADownloadItemDownloadState, completion: (item: KADownloadItem?) -> Void) {
        var arrCompleted : NSMutableArray = NSMutableArray()
        var arrNotCompleted : NSMutableArray = NSMutableArray()
        var item : KADownloadItem?

        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let querySQL = "SELECT itemId, itemUrl, itemFormat, state FROM Items WHERE state = \(state.rawValue) ORDER BY updateDate DESC LIMIT 1"
            
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                if rs.next() == true {
                    item = KADownloadItem(itemId: rs.stringForColumn("itemId"), itemUrl: rs.stringForColumn("itemUrl"), itemFormat: rs.stringForColumn("itemFormat"))
                    item!.downloadState = state
                }
            }
            db.close()
            completion(item: item)
        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(item: item)
        }
    }
    
    func getItem(itemId: String, completion: (itemData: NSMutableDictionary) -> Void) {
        var itemData: NSMutableDictionary = NSMutableDictionary()
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let querySQL = "SELECT ID, itemId, itemUrl, itemFormat, state, updateDate, firstWatchDate, hasWatchRestriction FROM Items WHERE itemId = '\(itemId)'"
            if let rs:FMResultSet = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                if rs.next() == true {
                    itemData.setValue(NSNumber(int: rs.intForColumn("ID")), forKey: "DBID");
                    itemData.setValue(rs.stringForColumn("itemId"), forKey: "itemId");
                    itemData.setValue(rs.stringForColumn("itemUrl"), forKey: "itemUrl");
                    itemData.setValue(rs.stringForColumn("itemFormat"), forKey: "itemFormat");
                    itemData.setValue(NSNumber(int: rs.intForColumn("state")), forKey: "state");
                    itemData.setValue(NSNumber(double: rs.doubleForColumn("updateDate")), forKey: "updateDate");
                    itemData.setValue(NSNumber(double: rs.doubleForColumn("firstWatchDate")), forKey: "firstWatchDate");
                    itemData.setValue(NSNumber(bool: rs.boolForColumn("hasWatchRestriction")), forKey: "hasWatchRestriction");
                    
                    let querySQLmm = "SELECT status, groupId, locationSec, mediaId, deviceId, deviceName, siteGUID, updateDate FROM ItemMediaMark WHERE itemId = '\(itemId)'"
                    if let rs1:FMResultSet = db.executeQuery(querySQLmm, withArgumentsInArray: nil) {
                        if rs1.next() == true {
                            var mediaMark : NSMutableDictionary = NSMutableDictionary()
                            mediaMark.setValue(NSNumber(int: rs1.intForColumn("status")), forKey: "status");
                            mediaMark.setValue(NSNumber(int: rs1.intForColumn("groupId")), forKey: "groupId");
                            mediaMark.setValue(NSNumber(double: rs1.doubleForColumn("locationSec")), forKey: "locationSec");
                            mediaMark.setValue(rs1.stringForColumn("mediaId"), forKey: "mediaId");
                            mediaMark.setValue(rs1.stringForColumn("deviceId"), forKey: "deviceId");
                            mediaMark.setValue(rs1.stringForColumn("deviceName"), forKey: "deviceName");
                            mediaMark.setValue(rs1.stringForColumn("siteGUID"), forKey: "siteGUID");
                            mediaMark.setValue(NSNumber(double: rs1.doubleForColumn("updateDate")), forKey: "updateDate");
                            itemData.setValue(mediaMark, forKey: "mediaMark");
                        }
                    }
                }
            }
            
            db.close()
            completion(itemData: itemData)
        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(itemData: itemData)
        }
    }
    
    func getItemStreams(item: KADownloadItem, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let selectSQL1 = "SELECT ID, bandwidth, programId, codecs, audio, video, subtitles, closedCaptions, URI, fileSavePath, m3u8UrlStr, isSelected FROM ItemVideoStreams WHERE itemId = '\(item.ID)'"
            
            if let rs = db.executeQuery(selectSQL1, withArgumentsInArray: nil) {
                while rs.next() == true {
                    var videoStream : KADownloadItem_video = KADownloadItem_video()
                    videoStream.ID = rs.intForColumn("ID")
                    videoStream.bandwidth = rs.intForColumn("bandwidth")
                    videoStream.programId = rs.intForColumn("programId")
                    videoStream.codecs = rs.stringForColumn("codecs")
                    videoStream.audio = rs.stringForColumn("audio")
                    videoStream.video = rs.stringForColumn("video")
                    videoStream.subtitles = rs.stringForColumn("subtitles")
                    videoStream.closedCaptions = rs.stringForColumn("closedCaptions")
                    videoStream.URI = rs.stringForColumn("URI")
                    videoStream.m3u8UrlStr = rs.stringForColumn("m3u8UrlStr")
                    videoStream.fileSavePath = rs.stringForColumn("fileSavePath")
                    videoStream.isSelected = rs.boolForColumn("isSelected")
                    
                    item.videoStreams.append(videoStream)
                }
            }
            else {
                println("Error: \(db.lastErrorMessage())")
            }
            
            let selectSQL2 = "SELECT ID, type, URI, fileSavePath, groupId, language, assocLanguage, name, instreamId, characteristics, isDefault, autoSelect, forced, m3u8UrlStr, isSelected FROM ItemAudioStreams WHERE itemId = '\(item.ID)'"
            
            if let rs = db.executeQuery(selectSQL2, withArgumentsInArray: nil) {
                while rs.next() == true {
                    var media : KADownloadItem_media = KADownloadItem_media()
                    media.ID = rs.intForColumn("ID")
                    media.type = rs.stringForColumn("type")
                    media.URI = rs.stringForColumn("URI")
                    media.fileSavePath = rs.stringForColumn("fileSavePath")
                    media.groupId = rs.stringForColumn("groupId")
                    media.language = rs.stringForColumn("language")
                    media.assocLanguage = rs.stringForColumn("assocLanguage")
                    media.name = rs.stringForColumn("name")
                    media.instreamId = rs.stringForColumn("instreamId")
                    media.characteristics = rs.stringForColumn("characteristics")
                    media.isDefault = rs.boolForColumn("isDefault")
                    media.autoSelect = rs.boolForColumn("autoSelect")
                    media.forced = rs.boolForColumn("forced")
                    media.m3u8UrlStr = rs.stringForColumn("m3u8UrlStr")
                    media.isSelected = rs.boolForColumn("isSelected")
                    
                    item.audioStreams.append(media)
                }
            }
            else {
                println("Error: \(db.lastErrorMessage())")
            }
            
            let selectSQL3 = "SELECT ID, type, URI, fileSavePath, groupId, language, assocLanguage, name, instreamId, characteristics, isDefault, autoSelect, forced, m3u8UrlStr, isSelected FROM ItemSubtitleStreams WHERE itemId = '\(item.ID)'"
            let results3:FMResultSet? = db.executeQuery(selectSQL3, withArgumentsInArray: nil)
            
            if let rs = db.executeQuery(selectSQL3, withArgumentsInArray: nil) {
                while rs.next() == true {
                    var media : KADownloadItem_media = KADownloadItem_media()
                    media.ID = rs.intForColumn("ID")
                    media.type = rs.stringForColumn("type")
                    media.URI = rs.stringForColumn("URI")
                    media.fileSavePath = rs.stringForColumn("fileSavePath")
                    media.groupId = rs.stringForColumn("groupId")
                    media.language = rs.stringForColumn("language")
                    media.assocLanguage = rs.stringForColumn("assocLanguage")
                    media.name = rs.stringForColumn("name")
                    media.instreamId = rs.stringForColumn("instreamId")
                    media.characteristics = rs.stringForColumn("characteristics")
                    media.isDefault = rs.boolForColumn("isDefault")
                    media.autoSelect = rs.boolForColumn("autoSelect")
                    media.forced = rs.boolForColumn("forced")
                    media.m3u8UrlStr = rs.stringForColumn("m3u8UrlStr")
                    media.isSelected = rs.boolForColumn("isSelected")
                    
                    item.subtitles.append(media)
                }
            }
            else {
                println("Error: \(db.lastErrorMessage())")
            }
            
            db.close()
            completion(success: Bool(true))
        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    func getItemProgress(itemId: String, completion: (progress: Double, notCompleted: Double) -> Void) {
        var written : Double = 0.00
        var total : Double = 1.00

        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let querySQL = "SELECT SUM(written) as written, (SELECT SUM(expected) FROM ItemUrls where itemId = '\(itemId)') as total FROM ItemUrls WHERE itemId = '\(itemId)'"
            
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                if rs.next() == true {
                    written = rs.doubleForColumn("written")
                    total = rs.doubleForColumn("total")
                }
            }
            db.close()
            completion(progress: written/total, notCompleted: (total-written))

        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(progress: written/total, notCompleted: (total-written))
        }
    }
    
    func getAllItems(completion: (items: NSMutableArray) -> Void) {
        var arrItems : NSMutableArray = NSMutableArray()
        let db = FMDatabase(path: databasePath as String)
        //
        if db.open() {
            let querySQL = "SELECT ID, itemId, itemUrl, itemFormat, state, updateDate, firstWatchDate, hasWatchRestriction FROM Items WHERE state not in (\(KADownloadItemDownloadState.Initial.rawValue)) ORDER BY updateDate DESC"
            
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                while rs.next() == true {
                    var itemData: NSMutableDictionary = NSMutableDictionary()
                    var itemId : String = rs.stringForColumn("itemId")
                    itemData.setValue(NSNumber(int: rs.intForColumn("ID")), forKey: "DBID");
                    itemData.setValue(itemId, forKey: "itemId");
                    itemData.setValue(rs.stringForColumn("itemUrl"), forKey: "itemUrl");
                    itemData.setValue(rs.stringForColumn("itemFormat"), forKey: "itemFormat");
                    itemData.setValue(NSNumber(int: rs.intForColumn("state")), forKey: "state");
                    itemData.setValue(NSNumber(double: rs.doubleForColumn("updateDate")), forKey: "updateDate");
                    itemData.setValue(NSNumber(double: rs.doubleForColumn("firstWatchDate")), forKey: "firstWatchDate");
                    itemData.setValue(NSNumber(bool: rs.boolForColumn("hasWatchRestriction")), forKey: "hasWatchRestriction");

                    let querySQLmm = "SELECT status, groupId, locationSec, mediaId, deviceId, deviceName, siteGUID, updateDate FROM ItemMediaMark WHERE itemId = '\(itemId)'"
                    if let rs1:FMResultSet = db.executeQuery(querySQLmm, withArgumentsInArray: nil) {
                        if rs1.next() == true {
                            var mediaMark : NSMutableDictionary = NSMutableDictionary()
                            mediaMark.setValue(NSNumber(int: rs1.intForColumn("status")), forKey: "status");
                            mediaMark.setValue(NSNumber(int: rs1.intForColumn("groupId")), forKey: "groupId");
                            mediaMark.setValue(NSNumber(double: rs1.doubleForColumn("locationSec")), forKey: "locationSec");
                            mediaMark.setValue(rs1.stringForColumn("mediaId"), forKey: "mediaId");
                            mediaMark.setValue(rs1.stringForColumn("deviceId"), forKey: "deviceId");
                            mediaMark.setValue(rs1.stringForColumn("deviceName"), forKey: "deviceName");
                            mediaMark.setValue(rs1.stringForColumn("siteGUID"), forKey: "siteGUID");
                            mediaMark.setValue(NSNumber(double: rs1.doubleForColumn("updateDate")), forKey: "updateDate");
                            itemData.setValue(mediaMark, forKey: "mediaMark");
                        }
                    }

                    arrItems.addObject(itemData);
                }
            }
            db.close()
            completion(items: arrItems)
        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(items: arrItems)
        }
    }
    
    func getAllNotCompletedItems(completion: (items: NSMutableArray) -> Void) {
        var arrItems : NSMutableArray = NSMutableArray()
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let querySQL = "SELECT itemId, itemUrl, itemFormat, state FROM Items WHERE state in (\(KADownloadItemDownloadState.InProgress.rawValue))"
            
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                while rs.next() == true {
                    var itemData: NSMutableDictionary = NSMutableDictionary()
                    var item : KADownloadItem = KADownloadItem(itemId: rs.stringForColumn("itemId"), itemUrl: rs.stringForColumn("itemUrl"), itemFormat: rs.stringForColumn("itemFormat"))
                    item.downloadState = KADownloadItemDownloadState(rawValue: rs.intForColumn("state"))!
                    arrItems.addObject(item);
                }
            }
            db.close()
            completion(items: arrItems)
        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(items: arrItems)
        }
    }
    
    func getAllNotCompletedItemsSize(completion: (size: Int64) -> Void) {
        var size : Int64 = 0
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let querySQL = "SELECT (SUM(expected) - SUM(written)) as size FROM ItemUrls WHERE isCompleted = 0 and ItemId in (SELECT itemId FROM Items WHERE state in (\(KADownloadItemDownloadState.InDownloadQueue.rawValue), \(KADownloadItemDownloadState.Interrupted.rawValue), \(KADownloadItemDownloadState.Paused.rawValue), \(KADownloadItemDownloadState.Failed.rawValue), \(KADownloadItemDownloadState.InProgress.rawValue) ))"
            
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                if rs.next() == true {
                    size = rs.longLongIntForColumn("size")
                }
            }
            db.close()
            completion(size: size)
        }
        else {
            println("Error: \(db.lastErrorMessage())")
            completion(size: size)
        }
    }
    
    func getAllSettings(completion: (dict: NSMutableDictionary) -> Void) {
        var dict: NSMutableDictionary = NSMutableDictionary()
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let querySQL = "SELECT key, value FROM Settings"
            
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                while rs.next() == true {
                    dict.setObject(rs.stringForColumn("value"), forKey: rs.stringForColumn("key"))
                }
            }
            db.close()
        }
        else {
            println("Error: \(db.lastErrorMessage())")
        }
        completion(dict: dict)
    }
    
    func getSettingsKey(key: String, completion: (value: String) -> Void) {
        var value: String = ""
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let querySQL = "SELECT value FROM Settings WHERE Key = '\(key)'"
            
            if let rs = db.executeQuery(querySQL, withArgumentsInArray: nil) {
                if rs.next() == true {
                    value = rs.stringForColumn("value")
                }
            }
            db.close()
        }
        else {
            println("Error: \(db.lastErrorMessage())")
        }
        completion(value: value)

    }
    
    // MARK: - Delete data
    
    func deleteItem(itemId: String, completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let deleteSQL = "DELETE FROM Items WHERE itemId = '\(itemId)'; DELETE FROM ItemVideoStreams WHERE itemId = '\(itemId)'; DELETE FROM ItemAudioStreams WHERE itemId = '\(itemId)'; DELETE FROM ItemSubtitleStreams WHERE itemId = '\(itemId)'; DELETE FROM ItemUrls WHERE itemId = '\(itemId)'; DELETE FROM ItemMediaMark WHERE itemId = '\(itemId)'; "
            
            if !db.executeStatements(deleteSQL) {
                println("Error: \(db.lastErrorMessage())")
            }
            db.close()
            completion(success: Bool(true))
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
    
    func deleteAllItems(completion: (success: Bool) -> Void) {
        let db = FMDatabase(path: databasePath as String)
        
        if db.open() {
            let deleteSQL = "DELETE FROM Items; DELETE FROM ItemVideoStreams; DELETE FROM ItemAudioStreams; DELETE FROM ItemSubtitleStreams; DELETE FROM ItemUrls; DELETE FROM ItemMediaMark;"
            
            if !db.executeStatements(deleteSQL) {
                println("Error: \(db.lastErrorMessage())")
            }
            db.close()
            completion(success: Bool(true))
            
        } else {
            println("Error: \(db.lastErrorMessage())")
            completion(success: Bool(false))
        }
    }
}