//
//  KANotificationManager.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 5/7/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import UIKit

struct NotificationGroup {
    let entries: [String]
    
    init(_ newEntries: String...) {
        entries = newEntries
    }
}

class NotificationManager {
    private var observerTokens: [AnyObject] = []
    
    deinit {
        deregisterAll()
    }
    
    func deregisterAll() {
        for token in observerTokens {
            NSNotificationCenter.defaultCenter().removeObserver(token)
        }
        
        observerTokens = []
    }
    
    func registerObserver(name: String!, block: (NSNotification! -> Void)) {
        let newToken = NSNotificationCenter.defaultCenter().addObserverForName(name, object: nil, queue: nil) {note in
            block(note)
        }
        
        observerTokens.append(newToken)
    }
    
    func registerObserver(name: String!, forObject object: AnyObject!, block: (NSNotification! -> Void)) {
        let newToken = NSNotificationCenter.defaultCenter().addObserverForName(name, object: object, queue: nil) {note in
            block(note)
        }
        
        observerTokens.append(newToken)
    }
    
    func registerGroupObserver(group: NotificationGroup, block: (NSNotification! -> Void)) {
        for name in group.entries {
            self.registerObserver(name, block: block)
        }
    }
}
