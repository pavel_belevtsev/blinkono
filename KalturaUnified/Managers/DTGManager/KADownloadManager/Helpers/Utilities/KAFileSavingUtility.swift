//
//  KAFileSavingUtility.swift
//  KADownloadManager
//
//  Created by Alex Zchut on 4/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import UIKit

class KAFileSavingUtility: NSObject {
    
    class func getUniqueFileNameWithPath(filePath : NSString) -> NSString {
        println("filePath \(filePath)")
        var fullFileName        : NSString = filePath.lastPathComponent
        var fileName            : NSString = fullFileName.stringByDeletingPathExtension
        var fileExtension       : NSString = fullFileName.pathExtension
        var suggestedFileName   : NSString = fileName
        
        var isUnique            : Bool = false
        var fileNumber          : Int = 0
        
        var fileManger          : NSFileManager = NSFileManager.defaultManager()
        
        do {
            var fileDocDirectoryPath : NSString?
            
            if fileExtension.length > 0 {
                fileDocDirectoryPath = "\(filePath.stringByDeletingLastPathComponent)/\(suggestedFileName).\(fileExtension)"
            } else {
                fileDocDirectoryPath = "\(filePath.stringByDeletingLastPathComponent)/\(suggestedFileName)"
            }
            
            var isFileAlreadyExists : Bool = fileManger.fileExistsAtPath(fileDocDirectoryPath! as String)
            
            if isFileAlreadyExists {
                fileNumber++
                suggestedFileName = "\(fileName)(\(fileNumber))"
            } else {
                isUnique = true
                if fileExtension.length > 0 {
                    suggestedFileName = "\(suggestedFileName).\(fileExtension)"
                }
            }
            
        } while isUnique == false
        
        return suggestedFileName
    }
    
    class func createSavePath(fileSavePath : String) {

        var folderDir : String = fileSavePath.stringByDeletingLastPathComponent
        if !NSFileManager.defaultManager().fileExistsAtPath(folderDir) {
            NSFileManager.defaultManager().createDirectoryAtPath(folderDir, withIntermediateDirectories: true, attributes: nil, error: nil)
        }
        else if NSFileManager.defaultManager().fileExistsAtPath(fileSavePath) {
            NSFileManager.defaultManager().removeItemAtPath(fileSavePath, error: nil)
        }
    }
    
    
    class func addSkipBackupAttributeToItemAtURL(docDirectoryPath : String) -> Bool {
        var url : NSURL = NSURL(fileURLWithPath: docDirectoryPath)!
        var fileManager = NSFileManager.defaultManager()
        if fileManager.fileExistsAtPath(url.path!) {
            var error : NSError?
            var success : Bool = url.setResourceValue(NSNumber(bool: true), forKey: NSURLIsExcludedFromBackupKey, error: &error)
            if let hasError = error {
                println("Error excluding \(url.lastPathComponent) from backup \(error)")
            }
            return success
        } else {
            return false
        }
    }
    
    class func createFolderAtPath(path: String) {
        if !NSFileManager.defaultManager().fileExistsAtPath(path) {
            NSFileManager.defaultManager().createDirectoryAtPath(path, withIntermediateDirectories: true, attributes: nil, error: nil)
        }
    }
    
    class func removeFolderAtPath(path: String) {
        var fileManager : NSFileManager = NSFileManager.defaultManager()
        fileManager.removeItemAtPath(path, error: nil)
    }
    
    class func removeFoldersAtPath(path: String) {
        var fileManager : NSFileManager = NSFileManager.defaultManager()
        
        let options: NSDirectoryEnumerationOptions = .SkipsHiddenFiles | .SkipsPackageDescendants | .SkipsSubdirectoryDescendants
        if let dirEnumerator : NSDirectoryEnumerator = fileManager.enumeratorAtURL(NSURL(fileURLWithPath: path)!, includingPropertiesForKeys: [NSURLIsDirectoryKey], options: options, errorHandler: nil) {
            while let element = dirEnumerator.nextObject() as? NSURL {
                var isDir : ObjCBool = false
                if fileManager.fileExistsAtPath(element.path!, isDirectory:&isDir) {
                    if isDir {
                        fileManager.removeItemAtPath(element.path!, error: nil)
                    }
                }
            }
        }
    }
    
    class func getDiskspace(completion: (total: Int64, free: Int64) -> Void) {
        var total: Int64 = 0
        var free: Int64 = 0
        var error : NSError?
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        if let systemAttributes = NSFileManager.defaultManager().attributesOfFileSystemForPath(documentDirectoryPath.last as! String, error: &error) {
            if let freeSize = systemAttributes[NSFileSystemFreeSize] as? NSNumber {
                free = freeSize.longLongValue
            }
            if let totalSize = systemAttributes[NSFileSystemSize] as? NSNumber {
                total = totalSize.longLongValue
            }
        }
        if let hasError = error {
            println("Error Obtaining System Memory Info: Domain = \(error?.domain), Code = \(error?.code)")
        }
        completion(total: total, free: free);
    }
    
    class func getPathSizeInUnits(path: String) -> String {
        var fileSize : Int64 = self.getPathSize(path)
        return "\(Int64(calculateFileSizeInUnit(fileSize))) \(calculateUnit(fileSize))"
    }
    
    class func getPathSize(path: String) -> Int64 {
        let filesArray:[String] = NSFileManager.defaultManager().subpathsOfDirectoryAtPath(path, error: nil)! as! [String]
        var fileSize : Int64 = 0
        
        for fileName in filesArray{
            let filePath = path.stringByAppendingPathComponent(fileName)
            let fileDictionary: NSDictionary = NSFileManager.defaultManager().attributesOfItemAtPath(filePath, error: nil)!
            fileSize += Int64(fileDictionary.fileSize())
        }
        
        return fileSize
    }
    
    class func calculateFileSizeInUnit(contentLength : Int64) -> Float {
        var dataLength : Float64 = Float64(contentLength)
        if dataLength >= (1024.0*1024.0*1024.0) {
            return Float(dataLength/(1024.0*1024.0*1024.0))
        } else if dataLength >= 1024.0*1024.0 {
            return Float(dataLength/(1024.0*1024.0))
        } else if dataLength >= 1024.0 {
            return Float(dataLength/1024.0)
        } else {
            return Float(dataLength)
        }
    }
    
    class func calculateUnit(contentLength : Int64) -> NSString {
        if(contentLength >= (1024*1024*1024)) {
            return "GB"
        } else if contentLength >= (1024*1024) {
            return "MB"
        } else if contentLength >= 1024 {
            return "KB"
        } else {
            return "B"
        }
    }
}

