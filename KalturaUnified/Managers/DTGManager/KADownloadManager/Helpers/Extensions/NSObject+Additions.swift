//
//  NSObject+Additions.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 6/16/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import Foundation

extension NSObject {
    
    //
    // Retrieves an array of property names found on the current object
    // using Objective-C runtime functions for introspection:
    // https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtPropertyIntrospection.html
    //
    func propertyNames() -> Array<String> {
        var results: Array<String> = [];
        
        // retrieve the properties via the class_copyPropertyList function
        var count: UInt32 = 0;
        var myClass: AnyClass = self.classForCoder;
        var properties = class_copyPropertyList(myClass, &count);
        
        // iterate each objc_property_t struct
        for var i: UInt32 = 0; i < count; i++ {
            var property = properties[Int(i)];
            
            // retrieve the property name by calling property_getName function
            var cname = property_getName(property);
            
            // covert the c string into a Swift string
            var name = String.fromCString(cname);
            results.append(name!);
        }
        
        // release objc_property_t structs
        free(properties);
        
        return results;
    }
    
    func emptyingNullStrings() {
        var properties = self.propertyNames();
        for property in properties {
            var value: AnyObject? = self.valueForKey(property);
            if (value?.respondsToSelector(Selector("length")) == nil) {
                self.setValue("", forKey: property)
            }
        }
    }
}