//
//  DTGManager.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 4/16/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import UIKit
import Foundation

@objc public class DTGManager: NSObject {

    static var instance: DTGManager!

    //MARK: variables
    private var activeManager : KADownloadManager?
    public let downloadManagerScheme : String = managerScheme
    //MARK: constractor
    override init() {
        super.init()
        self.activeManager =  KADownloadManager.sharedInstance()
        self.setDefaultSettings { (success) -> Void in
            if let a = DownloadManagersType(rawValue: KADTGConfig.getProviderName()) {
                self.activeManager?.setDownloadManagerType(a)
            }
            
            MongooseWrapper.start()
        }
    }
    
    @objc public func start() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reachabilityChanged:", name: "kNetworkReachabilityChangedNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "mainScreenLoaded:", name: "kMainScreenLoadedNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "offlineModeChanged:", name: "NotificationNameApplicationSettingsOfflineModeChanged", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "userStatusChanged:", name: "NotificationNameUserStatusChanged", object: nil)
    }
    
    //MARK: - Public functions
    @objc public class func sharedInstance() -> DTGManager {
        self.instance = (self.instance ?? DTGManager())
        return self.instance
    }
    
    @objc public func deleteItem(item: KADownloadItem, completion: (success: Bool) -> Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.deleteItem(item.ID, completion: { (success) -> Void in
                if success {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        //resume interrupted downloads
                        self.checkConnectionAndResumeInterruptedDownloads()
                        completion(success: success)
                    })
                }
            })
        })
    }
    
    @objc public func deleteAllItems(completion: (success: Bool) -> Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.deleteAllItems({ (success) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if success {
                        //resume interrupted downloads
                        self.checkConnectionAndResumeInterruptedDownloads()
                    }
                    completion(success: success)
                })
            })
        })
    }
    
    @objc public func getItem(mediaItem: TVMediaItem, completion: (item: KADownloadItem) -> Void) {
        var itemId : String = mediaItem.downloadItemId()
        var itemFormat : String = APSTypography.instance().TVCMediaFormatDownload
        var itemUrl : String?

        var arrFiles : NSArray? = mediaItem.files?.filter{file in file.format == itemFormat}
        if let files = arrFiles {
            var file : TVFile = files.firstObject() as! TVFile
            if let urlString = file.fileURL?.absoluteString {
                itemUrl = urlString
            }
            else {
                itemUrl = ""
            }
        }
        else {
            itemUrl = ""
        }

        //SAMPLES
        //itemUrl = "http://sample-videos.com/video/mp4/480/big_buck_bunny_480p_10mb.mp4"
        //itemUrl = "http://commondatastorage.googleapis.com/wvmedia/tears_high_1080p_4br_tp.wvm"

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.getItem(itemId, itemUrl: itemUrl!, itemFormat: itemFormat, completion: { (item) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(item: item)
                })
            })
        })
    }
    
    @objc public func getAllItems(completion: (items: NSMutableArray) -> Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.getAllItems { (items) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(items: items)
                })
            }
        })
    }
    
    @objc public func isOkToDownload(completion: (success: Bool) -> Void) {
        self.isConnectionOkToDownload { (success) -> Void in
            if success && LoginManager.instance().isSignedIn() == true {
                completion(success: Bool(true))
            }
            else {
                completion(success: Bool(false))
            }
        }
    }
    
    @objc public func updateParentalCode(code: String, completion: (success: Bool) -> Void) {
        var hash : String = code.stringByAppendingPathComponent(managerScheme).md5()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.settings?.setSettingsKey(KADownloadSettings.parentalCode, value: hash, completion: { (success) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(success: success)
                })
            })
        })
    }
    
    @objc public func verifyParentalCode(code: String, completion: (success: Bool) -> Void) {
        var hash : String = code.stringByAppendingPathComponent(managerScheme).md5()
            self.activeManager?.settings?.getSettingsKey(KADownloadSettings.parentalCode, completion: { (value : String) -> Void in
            if hash == value {
                completion(success: Bool(true))
            }
            else {
                completion(success: Bool(false))
            }
        })
    }
    
    @objc public func getCurrentQuotaBasedLibrarySize(completion: (currentLibrarySize: Int64, allowedLibrarySize: Int64, freeSpaceOnDevice: Int64) -> Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.getCurrentQuotaBasedLibrarySize({ (currentLibrarySize, inProgressDownloadsSize, allowedLibrarySize, freeSpaceOnDevice) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(currentLibrarySize: currentLibrarySize + inProgressDownloadsSize, allowedLibrarySize: allowedLibrarySize, freeSpaceOnDevice: freeSpaceOnDevice)
                })
            })
        })
    }
    
    @objc public func resumeInterruptedDownloadTasks() {
        self.checkConnectionAndResumeInterruptedDownloads()
    }

    @objc public func interruptActiveDownloadTasks() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.interruptActiveDownloadTasks()
        })
    }
    
    @objc public func cancelAllDownloads() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.cancelAllDownloads()
        })
    }
    
    // MARK: - settings functions
    func setDefaultSettings(completion: (success: Bool) -> Void) {
        var dict : NSMutableDictionary = NSMutableDictionary()
        self.getSettingsKey(KADownloadSettings.hasDefaultSettings, completion: { (value : String) -> Void in
            if value.boolValue() == false {
                
                //update flag of added default settings
                dict.setValue("true", forKey: KADownloadSettings.hasDefaultSettings)
                
                //user default settings params
                dict.setValue("false", forKey: KADownloadSettings.enableDownloadOnCellularData)
                dict.setValue("true", forKey: KADownloadSettings.autoResumeDownloads)
                dict.setValue("30", forKey: KADownloadSettings.downloadQuota)
                dict.setValue("high", forKey: KADownloadSettings.downloadQuality)
            }
            //features settings - can be changed in app
            dict.setValue(NSString(format: "%d", APSTypography.instance().dtgParams["simultaneousDownloadItems"]!.intValue), forKey: KADownloadSettings.simultaneousDownloadItems)
            dict.setValue(NSString(format: "%d", APSTypography.instance().dtgParams["timeoutIntervalForRequest"]!.intValue), forKey: KADownloadSettings.timeoutIntervalForRequest)
            dict.setValue(NSString(format: "%d", APSTypography.instance().dtgParams["timeoutIntervalForResource"]!.intValue), forKey: KADownloadSettings.timeoutIntervalForResource)
            dict.setValue(NSString(format: "%d", APSTypography.instance().dtgParams["httpMaximumConnectionsPerHost"]!.intValue), forKey: KADownloadSettings.httpMaximumConnectionsPerHost)
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                self.activeManager?.settings?.setBulkSettings(dict, completion: { (success) -> Void in
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completion(success: success)
                    })
                })
            })
        })
    }
    
    @objc public func getSettingsKey(key: String, completion: (value: String) -> Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.settings?.getSettingsKey(key, completion: { (value) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(value: value)
                })
            })
        })

    }
    
    @objc public func getAllSettings(completion: (dict: NSMutableDictionary) -> Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.settings?.getAllSettings { (dict) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(dict: dict)
                })
            }
        })
    }
    
    @objc public func setSettingsKey(key: String, value: String, completion: (success: Bool) -> Void) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            self.activeManager?.settings?.setSettingsKey(key, value: value) { (success) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(success: success)
                })
            }
        })
    }
    
    @objc public func getDownloadedFileUrl(item: KADownloadItem) -> NSURL? {
        var url : NSURL?
        if let a = DownloadManagersType(rawValue: KADTGConfig.getProviderName()) {
            switch(a) {
                case .WVM:
                    url = NSURL(fileURLWithPath: item.filePath)
                default:
                    url = item.playUrl
            }
        }
        return url
    }
    
    // MARK: - local functions
    func checkConnectionAndResumeInterruptedDownloads() {
        var reachability: Reachability = Reachability.reachabilityForInternetConnection()
        self.performActionOnConnectionChange(reachability)
    }
    
    func performActionOnConnectionChange(reachability: Reachability) {
        var status : NetworkStatus = reachability.currentReachabilityStatus()
        if status == 0 {
            println("status: NotReachable")
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                self.activeManager?.interruptActiveDownloadTasks()
            })
        }
        else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                self.activeManager?.settings?.getAllSettings({ (dict) -> Void in
                    var autoResumeDownloads : String = dict.objectForKey(KADownloadSettings.autoResumeDownloads) as! String
                    var enableDownloadOnCellularData : String = dict.objectForKey(KADownloadSettings.enableDownloadOnCellularData) as! String
                    
                    if autoResumeDownloads.boolValue() == true && LoginManager.instance().isSignedIn() == true && OfflineManager.sharedInstance().isOffline == false {
                        if status == 1 {
                            println("status: ReachableViaWWAN")
                            //check if enabled 3G download & user logged in
                            if enableDownloadOnCellularData.boolValue() == true {
                                self.activeManager?.resumeInterruptedDownloadTasks()
                            }
                            else {
                                self.activeManager?.interruptActiveDownloadTasks()
                            }
                        }
                        else if status == 2 {
                            println("status: ReachableViaWiFi")
                            self.activeManager?.resumeInterruptedDownloadTasks()
                        }
                    }
                })
            })
        }
    }
    
    func isConnectionOkToDownload(completion: (success: Bool) -> Void) {
        var reachability: Reachability = Reachability.reachabilityForInternetConnection()
        var status : NetworkStatus = reachability.currentReachabilityStatus()
        println("isOkToDownload: \(status)")
        
        if status == 0 {
            completion(success: Bool(false))
        }
        else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                self.activeManager?.settings?.getAllSettings({ (dict) -> Void in
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        var enableDownloadOnCellularData : String = dict.objectForKey(KADownloadSettings.enableDownloadOnCellularData) as! String
                        
                        if status == 1 {
                            if enableDownloadOnCellularData.boolValue() == true {
                                completion(success: Bool(true))
                            }
                            else {
                                completion(success: Bool(false))
                            }
                        }
                        else if status == 2 {
                            completion(success: Bool(true))
                        }
                    })
                })
            })
        }
    }
    
    
    // MARK: - observers
    func reachabilityChanged(notification: NSNotification){
        var reachability : Reachability = notification.object as! Reachability
        self.performActionOnConnectionChange(reachability)
    }
    
    func mainScreenLoaded(notification: NSNotification) {
        self.checkConnectionAndResumeInterruptedDownloads()
    }
    
    func offlineModeChanged(notification: NSNotification) {
        if OfflineManager.sharedInstance().isOffline == true {
            self.interruptActiveDownloadTasks()
        }
        else {
            self.checkConnectionAndResumeInterruptedDownloads()
        }
    }
    
    func userStatusChanged(notification: NSNotification) {
        if !TVNameForUIUnit(UIUnit_Device_Downloads).isUIUnitExist() {
            self.cancelAllDownloads()
        }
    }
}
