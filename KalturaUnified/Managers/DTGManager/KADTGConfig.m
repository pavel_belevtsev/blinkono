//
//  KADTGConfig.m
//  KalturaUnified
//
//  Created by Noam Tamim on 6/10/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "KADTGConfig.h"

@implementation KADTGConfig
+(NSInteger)getProviderName {
    // TODO: replace with something more robust
#if PLAYER_WV==1
    return DownloadManagersTypeWVM;
#else
    return DownloadManagersTypeM3U8;
#endif
}

@end
