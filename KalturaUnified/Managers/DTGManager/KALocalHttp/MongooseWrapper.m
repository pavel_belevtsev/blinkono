//
//  MongooseWrapper.m
//  KalturaUnified
//
//  Created by Noam Tamim on 6/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MongooseWrapper.h"
#import "mongoose.h"

static struct mg_callbacks callbacks; 

static struct mg_context *ctx;

@implementation MongooseWrapper

+(void)start {

    memset(&callbacks, 0, sizeof(callbacks));
    
    const char *options[] = {
     "document_root", NSHomeDirectory().UTF8String,
     "listening_ports", [self port].UTF8String,
     NULL
    };
        
    ctx = mg_start(&callbacks, NULL, options);
}

+(NSString*) port {
    return @"8080";
}

@end
