//
//  MongooseWrapper.h
//  KalturaUnified
//
//  Created by Noam Tamim on 6/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MongooseWrapper : NSObject
+(void)start;
@end
