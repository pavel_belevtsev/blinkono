//
//  DeepLinkingManager.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.02.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DeepLinkingAction;

@interface DeepLinkingManager : NSObject

+ (DeepLinkingManager *)instance;

- (BOOL)registerDeepLinkingAction:(NSURL *)url;

@property (nonatomic, strong) DeepLinkingAction *deepLinkingAction;

@end
