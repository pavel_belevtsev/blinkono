//
//  NavigationManager.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "NavigationManager.h"

NSString * const KAuthenticationCompletedNotification = @"KAuthenticationCompletedNotification";
NSString * const KMenuItemsUpdatedNotification = @"KMenuItemsUpdatedNotification";
NSString * const KLogoutNotification = @"KLogoutNotification";
NSString * const KLoginNotification = @"KLoginNotification";
NSString * const KRenameNotification = @"KRenameNotification";
NSString * const KFbNotification = @"KFbNotification";
NSString * const KMenuItemsUpdatedErrorNotification = @"KMenuItemsUpdatedErrorNotification";
NSString * const KDeepLinkingActionNotification = @"KDeepLinkingActionNotification";

@implementation NavigationManager

+ (NavigationManager *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (id)init {
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)setPhonePortrait {
    
    if (isPhone) {
        
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        UIDevice *device = [UIDevice currentDevice];

        if ((orientation == UIInterfaceOrientationLandscapeLeft)||(orientation == UIInterfaceOrientationLandscapeRight)) {
            
            if ([device respondsToSelector:@selector(setOrientation:)])
                [device setOrientation:UIInterfaceOrientationPortrait];
        }
    }
}

@end
