//
//  UserManagement.m
//  KalturaUnified
//
//  Created by Synergetica LLC on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UserManagement.h"

NSString * const KDomainUsersListUpdatedNotification = @"KDomainUsersListUpdatedNotification";

@implementation UserManagement

#pragma mark - Request


- (void)requestForGetDomainInfo:(void(^)(id jsonResponse))completionBlock failedBlock:(void(^)(NSString* localizedError))failedBlock {
    
    __weak TVPAPIRequest * request = [TVPDomainAPI requestForGetDomainInfo:nil];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
        
    }];
    
    
    [request setFailedBlock:^{
        
        failedBlock([request.error localizedDescription]);
        
    }];
    
    [self sendRequest:request];
    
}

- (void)requestForGetDomainUsers:(void(^)(id jsonResponse))completionBlock failedBlock:(void(^)(NSString* localizedError))failedBlock {
    
    [self requestForGetDomainInfo:^(id jsonResponse) {
        
        NSDictionary *domainInfo = [jsonResponse dictionaryByRemovingNSNulls];
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        NSMutableArray *arrayPending = [[NSMutableArray alloc] init];
        
        if ([domainInfo objectForKey:@"m_PendingUsersIDs"]) {
            
            NSArray *usersArray = [domainInfo objectForKey:@"m_PendingUsersIDs"];
            
            for (id userID in usersArray) {
                if (userID)
                {
                    [arrayPending addObject:userID];
                }
            }
        }
        
        if ([domainInfo objectForKey:@"m_UsersIDs"]) {
            
            NSArray *usersArray = [domainInfo objectForKey:@"m_UsersIDs"];
            
            for (id userID in usersArray) {
                
                BOOL canAdd = YES;
                
                for (id pendindId in arrayPending) {
                    if ([[pendindId stringValue] isEqualToString:[userID stringValue]]) {
                        canAdd = NO;
                    }
                }
                
                if (canAdd) [array addObject:userID];
            }
        }
        
        __weak TVPAPIRequest * request = [TVPSiteAPI requestforGetUsersData:array delegate:nil];
        
        [request setCompletionBlock:^{
            
            completionBlock([request JSONResponse]);
            
            
        }];
        
        
        [request setFailedBlock:^{
            
            failedBlock([request.error localizedDescription]);
            
        }];
        
        [self sendRequest:request];
        
    } failedBlock:^(NSString *localizedError) {
        
        failedBlock(localizedError);
        
    }];
    
}

- (void)requestForGetDomainMasterUsers:(void(^)(id jsonResponse))completionBlock failedBlock:(void(^)(NSString* localizedError))failedBlock {
    
    [self requestForGetDomainInfo:^(id jsonResponse) {
        
        NSDictionary *domainInfo = jsonResponse;
        
        id masterUserId = nil;
        
        LoginM.DomainRestriction = [[domainInfo objectForKey:@"m_DomainRestriction"] integerValue];
        LoginM.nDeviceLimit = [[domainInfo objectForKey:@"m_nDeviceLimit"] integerValue];
        LoginM.concurrentDeviceLimit = [[domainInfo objectForKey:@"m_nConcurrentLimit"] integerValue];
        LoginM.nCurrentDevices = 0;
        
        NSArray *array = [domainInfo objectForKey:@"m_deviceFamilies"];
        
        if (array && [array isKindOfClass:[NSArray class]]) {
            
            for (NSDictionary *item in array) {
                
                NSArray *arrayItem = [item objectForKey:@"DeviceInstances"];
                
                if (arrayItem && [arrayItem isKindOfClass:[NSArray class]] && [[item objectForKey:@"m_deviceLimit"] integerValue] != 0 ) {
                    LoginM.nCurrentDevices += [arrayItem count];
                }
                
            }
            
        }
        
        NSLog(@"Domain data %d %d %d", (int)LoginM.DomainRestriction, (int)LoginM.nDeviceLimit, (int)LoginM.nCurrentDevices);
        
        if ([domainInfo objectForKey:@"m_masterGUIDs"]) {
            
            NSArray *usersArray = [domainInfo objectForKey:@"m_masterGUIDs"];
            
            for (id userID in usersArray) {
                masterUserId = userID;
            }
        }
        
        if (masterUserId) {
            
            __weak TVPAPIRequest * request = [TVPSiteAPI requestForGetUserDetails:masterUserId delegate:nil];
            
            [request setCompletionBlock:^{
                
                TVUser *user = [[TVUser alloc] initWithDictionary:[[request JSONResponse] objectForKey:@"m_user"]];
                
                LoginM.domainMasterUser = user;
                
                completionBlock([request JSONResponse]);
                
                
            }];
            
            
            [request setFailedBlock:^{
                
                failedBlock([request.error localizedDescription]);
                
            }];
            
            [self sendRequest:request];
        }
        
        
    } failedBlock:^(NSString *localizedError) {
        
        failedBlock(localizedError);
        
    }];
    
}

- (void)requestForGetSiteGuidWithUserName:(NSString *)userName password:(NSString *)password completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForGetSiteGuidWithUserName:userName password:password delegate:nil];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
        
    }];
    
    [self sendRequest:request];
    
}

- (void)requestForSignUpWithNewUser:(TVUser *)user password:(NSString *)password completionBlock:(void(^)(id jsonResponse))completionBlock {

    __weak TVPAPIRequest *request = [TVPSiteAPI requestForSignUpWithNewUser:user password:password affiliateCode:nil delegate:nil];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
        
    }];
    
    [self sendRequest:request];
    
}

- (void)requestForAddUserToDomainWithDomainId:(NSInteger)domainID userGuid:(NSString *)userGuid masteUserGuid:(NSString *)masterUserGuid completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest *request = [TVPDomainAPI requestForAddUserToDomainWithDomainId:domainID userGuid:userGuid masteUserGuid:masterUserGuid delegate:nil];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
        
    }];
    
    [self sendRequest:request];
    
}

- (void)requestForSubmitAddUserToDomainWithMasterUsername:(NSString *)masterUsername completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest * request = [TVPDomainAPI requestForSubmitAddUserToDomainWithMasterUsername:masterUsername delegate:nil];
    
    [request setCompletionBlock:^{
        completionBlock([request JSONResponse]);
    }];
    
    [request setFailedBlock:
     ^{
        completionBlock(nil);
    }];
    
    [self sendRequest:request];
    
}

- (void)requestForGetUserDetailsByUsername:(NSString *)username completionBlock:(void(^)(id jsonResponse))completionBlock  {
    
    __weak TVPAPIRequest * request = [TVPSiteAPI requestForGetUserDetailsByUsername:username delegate:nil];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [request setFailedBlock:^{
        
        completionBlock(nil);
        
    }];
    
    [self sendRequest:request];
    
}

- (void)loadDomainUsers:(void(^)())completion {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"DomainID"]) {
        
        [TVSessionManager sharedTVSessionManager].sharedInitObject.domainID = [[[NSUserDefaults standardUserDefaults] objectForKey:@"DomainID"] integerValue];
       
        [self requestForGetDomainUsers:^(id jsonResponse) {
            
            //NSArray *array = [jsonResponse arrayByRemovingNSNulls];
            NSArray *array = jsonResponse;
            
            NSMutableArray *usersArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *userData in array) {
                
                //NSLog([userData description]);
                
                NSDictionary *mUser = [userData objectForKey:@"m_user"];
                
//                int eUserState = [[mUser objectForKey:@"m_eUserState"] intValue];
                
//                if (eUserState > 0) {
                    
                    TVUser *user = [[TVUser alloc] initWithDictionary:mUser];
                    
                    BOOL canAdd = YES;
                    
                    for (TVUser *userAdded in usersArray) {
                        if ([user.siteGUID isEqualToString:userAdded.siteGUID]) canAdd = NO;
                    }
                    
                    if (canAdd && [user.username length]) {
                        [usersArray addObject:user];
                        NSLog(@"username %@   %@", user.username, user.email);
                    }
                    
//                } else {
//                    
//                    TVUser *user = [[TVUser alloc] initWithDictionary:mUser];
//                    NSLog(@"username pending %@   %@", user.username, user.email);
//                    
//                }
                
                
            }
            
            LoginM.domainUsers = usersArray;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:KDomainUsersListUpdatedNotification object:nil];
            
            [self requestForGetDomainMasterUsers:^(id jsonResponse) {
                
                completion();
                
            } failedBlock:^(NSString *localizedError) {
                
                completion();
                
            }];
            
        } failedBlock:^(NSString *localizedError) {
            
            completion();
            
        }];
        
    } else {
    
        completion();
    
    }
    
}

@end
