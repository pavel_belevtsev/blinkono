//
//  SocialProviderMail.m
//  KalturaUnified
//
//  Created by Alex Zchut on 3/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SocialProviderMail.h"

@implementation SocialProviderMail

- (void) share:(id <SocialProviderDelegate>) delegate mediaItem:(TVMediaItem *)mediaItem {
    [super share:delegate mediaItem:mediaItem];

    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil) {
        
        if ([mailClass canSendMail]) {
            
            MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            
            //NSString *subjectTemplate = LS(@"Subject");
            //NSString *subject = [NSString stringWithFormat:subjectTemplate, _mediaItem.name];
            
            NSString *subject = mediaItem.name;
            
            [controller setSubject:subject];
            
            NSString *strWebLink = (mediaItem.mediaWebLink ? [mediaItem.mediaWebLink absoluteString] : @"");
            NSString *body = [NSString stringWithFormat:@"%@ %@ %@ %@ %@", LS(@"shareMessage_MailBegin"), mediaItem.name ,LS(@"shareMessage_MailMiddle"),strWebLink,LS(@"shareMessage_MailEnd")];
            
            //NSLog(body);
            
            [controller setMessageBody:body isHTML:NO];
            
            //[controller setMessageBody:body isHTML:YES];
            
            [delegate presentViewController:controller];
            
        } else {
            
            UIAlertView *emailAlert = [[UIAlertView alloc] initWithTitle:LS(@"email_Failure") message:LS(@"shareMessage_CantSentMail") delegate:self cancelButtonTitle:[LS(@"ok") uppercaseString] otherButtonTitles: nil];
            [emailAlert show];
        }
        
    } else {
        
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self.delegate dismissViewController];
}
@end
