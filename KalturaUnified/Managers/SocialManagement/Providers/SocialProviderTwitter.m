//
//  SocialProviderTwitter.m
//  KalturaUnified
//
//  Created by Alex Zchut on 3/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SocialProviderTwitter.h"

@implementation SocialProviderTwitter

- (void) share:(id <SocialProviderDelegate>) delegate mediaItem:(TVMediaItem *)mediaItem {
    [super share:delegate mediaItem:mediaItem];
    
    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    NSString *body = @"";
    
    if (APST.ipno) {
        body = LS(@"shareMessage_TwitterFull");
    } else {
        body = [NSString stringWithFormat:@"%@ %@ %@",LS(@"shareMessage_TwitterBegin"), mediaItem.name, LS(@"shareMessage_TwitterEnd")];
    }
    
    body = [NSString stringWithFormat:@"%@ %@ %@",LS(@"shareMessage_TwitterBegin"), mediaItem.name, LS(@"shareMessage_TwitterEnd")];
    
    [tweetSheet setInitialText:body];
    
    NSURL *imageUrl = [Utils getBestPictureURLForMediaItem:mediaItem andSize:CGSizeMake(400.0, 600.0)];
    [[SDWebImageManager sharedManager] downloadImageWithURL:imageUrl options:SDWebImageHighPriority progress:NULL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        [tweetSheet addImage:image];
        
    if (mediaItem.mediaWebLink)
        [tweetSheet addURL:mediaItem.mediaWebLink];
        
        [delegate presentViewController:tweetSheet];
    }];
}

@end
