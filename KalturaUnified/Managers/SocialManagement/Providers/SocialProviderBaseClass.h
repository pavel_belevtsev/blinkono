//
//  SocialProviderBaseClass.h
//  KalturaUnified
//
//  Created by Alex Zchut on 3/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocialManagement.h"
#import <SDWebImage/SDWebImageManager.h>

@protocol SocialProviderDelegate <NSObject>

- (void) presentViewController:(UIViewController*) viewControllerToPresent;
- (void) dismissViewController;
- (void) sendRequest:(ASIHTTPRequest *)request;

@end

@interface SocialProviderBaseClass : NSObject

@property (nonatomic, weak) id <SocialProviderDelegate> delegate;

- (void) share:(id <SocialProviderDelegate>) delegate mediaItem:(TVMediaItem *)mediaItem;
- (void) addComment:(NSString*) text mediaItem:(TVMediaItem *)mediaItem;
- (void) aquirePermissions:(void(^)(BOOL success))completion;

+ (void)logout;

@end
