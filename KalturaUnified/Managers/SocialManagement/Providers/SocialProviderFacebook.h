//
//  SocialProviderFacebook.h
//  KalturaUnified
//
//  Created by Alex Zchut on 3/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTokenCachingStrategy.h"

@interface SocialProviderFacebook : SocialProviderBaseClass

@property (nonatomic, strong) MyTokenCachingStrategy *tokenCaching;

@end
