//
//  SocialProviderFacebook.m
//  KalturaUnified
//
//  Created by Alex Zchut on 3/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SocialProviderFacebook.h"

@implementation SocialProviderFacebook

+ (void)logout {
    
    FBSession* session = [FBSession activeSession];
    
    if (session) {
        [session closeAndClearTokenInformation];
        [session close];
        [FBSession setActiveSession:nil];
        
        NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray* facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"https://facebook.com/"]];
        
        for (NSHTTPCookie* cookie in facebookCookies) {
            [cookies deleteCookie:cookie];
        } 
    }
}

- (void) share:(id <SocialProviderDelegate>) delegate mediaItem:(TVMediaItem *)mediaItem {
    [super share:delegate mediaItem:mediaItem];

    SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    NSString *bodyTemplate = LS(@"shareMessage_Facebook");
    if (mediaItem.mediaWebLink)
    {
        bodyTemplate =[bodyTemplate stringByAppendingString:[NSString stringWithFormat:@"\n%@",mediaItem.mediaWebLink.absoluteString]];
    }

    [facebookSheet setInitialText:bodyTemplate];
    
    NSURL *imageUrl = [Utils getBestPictureURLForMediaItem:mediaItem andSize:CGSizeMake(400.0, 600.0)];
    [[SDWebImageManager sharedManager] downloadImageWithURL:imageUrl options:SDWebImageHighPriority progress:NULL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        [facebookSheet addImage:image];
        
        if (mediaItem.mediaWebLink)
            [facebookSheet addURL:mediaItem.mediaWebLink];
        
        [delegate presentViewController:facebookSheet];
    }];
}

- (void) addComment:(NSString*) text mediaItem:(TVMediaItem *)mediaItem  {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:text, @"message", nil];
    if (mediaItem.mediaWebLink) {
        [params setObject:[mediaItem.mediaWebLink absoluteString] forKey:@"link"];
    }
    
    NSURL *url = [Utils getBestPictureURLForMediaItem:mediaItem andSize:CGSizeMake(320, 180)];
    if (url) {
        [params setObject:[url absoluteString] forKey:@"picture"];
    }
    
    if (mediaItem.pictureURL) {
        [params setObject:[mediaItem.pictureURL absoluteString] forKey:@"picture"];
    }
    [self aquirePermissions:^(BOOL success) {
        if (success) {
            [self verifyFBUserWithMergedUser:^(BOOL success) {
                if (success) {
                    [FBRequestConnection startWithGraphPath:@"/me/feed"
                                                 parameters:params
                                                 HTTPMethod:@"POST"
                                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                              
                                              if (!error) {
                                                  // Link posted successfully to Facebook
                                                  NSLog(@"result: %@", result);
                                              } else {
                                                  // An error occurred, we need to handle the error
                                                  // See: https://developers.facebook.com/docs/ios/errors
                                                  NSLog(@"%@", error.description);
                                              }
                                          }];
                }
                else {
                    //not same user
                    NSLog(@"verifyFBUserWithMergedUser: %d", success);
                }
            }];
        }
        else {
            //no permissions
            NSLog(@"aquirePermissions: %d", success);
        }
    }];
}

- (void) aquirePermissions:(void(^)(BOOL success))completion {
    [self getReadPermissions:^(BOOL success) {
        if (success)
            [self getWritePermissions:^(BOOL success) {
                completion(success);
            }];
        else
            completion(FALSE);
    }];
}

- (void) verifyFBUserWithMergedUser:(void(^)(BOOL success))completion {
    [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
        if ([user[@"id"] isEqualToString:[TVSessionManager sharedTVSessionManager].currentUser.facebookID])
            completion(TRUE);
        else {
            [[self class] logout];
            _tokenCaching = nil;
            completion(FALSE);
        }
    }];
}

#pragma mark - define and check permissions
- (void) getWritePermissions:(void(^)(BOOL success))completion {
    
    [self getScope:^(BOOL success) {
        if (success) {
            if ([[FBSession activeSession] isOpen]) {
                if ([self hasWritePermissions:[FBSession activeSession]])
                    completion(TRUE);
                else
                    [self updateSessionWithPublishPermissions:self.arrPublishPermissions completion:completion];
            }
            else
                [self updateSessionWithPublishPermissions:self.arrPublishPermissions completion:completion];
        }
        else
            completion(FALSE);
    }];
}

- (void) getReadPermissions:(void(^)(BOOL success))completion {
    [self getScope:^(BOOL success) {
        if (success) {
            if ([[FBSession activeSession] isOpen]) {
                if ([self hasReadPermissions:[FBSession activeSession]])
                    completion(TRUE);
                else
                    [self openSessionWithReadPermissions:self.arrReadPermissions completion:completion];
            }
            else
                [self openSessionWithReadPermissions:self.arrReadPermissions completion:completion];
        }
        else
            completion(FALSE);
    }];
}

- (BOOL) hasWritePermissions:(FBSession *)session {
    NSArray *permissions = [session permissions];
    NSArray *requiredPermissions = self.arrPublishPermissions;
    
    for (NSString *perm in requiredPermissions) {
        if (![permissions containsObject:perm]) {
            return NO; // required permission not found
        }
    }
    return YES;
}

- (BOOL) hasReadPermissions:(FBSession *)session {
    NSArray *permissions = [session permissions];
    NSArray *requiredPermissions = self.arrReadPermissions;
    
    for (NSString *perm in requiredPermissions) {
        if (![permissions containsObject:perm]) {
            return NO; // required permission not found
        }
    }
    return YES;
}

- (NSArray*) arrReadPermissions {
    NSArray *permissions = nil;
    if (LoginM.readPermissions && [LoginM.readPermissions count]) {
        permissions = LoginM.readPermissions;
    } else {
        permissions= @[@"email", @"user_birthday"];
    }
    return permissions;
}

- (NSArray*) arrPublishPermissions {
    NSArray *permissions = nil;
    if (LoginM.writePermissions && [LoginM.writePermissions count]) {
        permissions = LoginM.writePermissions;
    } else {
        permissions= @[@"publish_actions"];
    }
    return permissions;
    
}

#pragma mark - get scope
- (void)getScope:(void(^)(BOOL success))completion {
    
    if (LoginM.facebookScope) {
        completion(TRUE);
    } else {
        [FBM requestForFBConfigWithDelegate:completion];
    }
}


#pragma mark - open session
- (void)openSessionWithReadPermissions:(NSArray*) permissions completion:(void(^)(BOOL success))completion {

    NSString * appID = [[LoginM facebookScope] objectForKey:@"appId"];
    
    if (!_tokenCaching) {
        _tokenCaching = [MyTokenCachingStrategy new];
        [_tokenCaching setThirdPartySessionId:appID];
    }
    else {
        NSDictionary *tokenInfo = [_tokenCaching fetchTokenInformation];
        NSArray *savedPermissions = tokenInfo[@"FBTokenInformationPermissionsKey"];
    
        //create new distinct set with existing and new permissions and see if the array is bigger than the old permissions array. -> reset the token cache
        NSMutableSet *set1 = [NSMutableSet setWithArray:savedPermissions];
        [set1 addObjectsFromArray:permissions];
        NSArray *arrAllPermissions = set1.allObjects;
        if (arrAllPermissions.count > savedPermissions.count) {
            _tokenCaching = [MyTokenCachingStrategy new];
            [_tokenCaching setThirdPartySessionId:appID];
        }
    }
    
    FBSessionLoginBehavior behavior  = FBSessionLoginBehaviorWithNoFallbackToWebView;
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]){
        behavior = FBSessionLoginBehaviorUseSystemAccountIfPresent;
    } else {
        behavior = FBSessionLoginBehaviorWithFallbackToWebView;
    }
    
    FBSession *session = [[FBSession alloc] initWithAppID:appID
                                              permissions:permissions
                                          defaultAudience:FBSessionDefaultAudienceEveryone
                                          urlSchemeSuffix:nil
                                       tokenCacheStrategy:_tokenCaching];
    
    [FBSession setActiveSession:session];
    
    [session openWithBehavior:behavior
            completionHandler:^(FBSession *session,
                                FBSessionState state,
                                NSError *error) {
                
        [self sessionStateChanged:session state:state error:error completion:completion];
    }];
}

- (void)updateSessionWithPublishPermissions:(NSArray*) permissions completion:(void(^)(BOOL success))completion {

    [[FBSession activeSession] close];
    
    [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
        [self sessionStateChanged:session state:state error:error completion:completion];
    }];
}


- (void)sessionStateChanged:(FBSession*)session state:(FBSessionState)state error:(NSError*)error completion:(void(^)(BOOL success))completion {
    
    switch (state)
    {
        case FBSessionStateOpen:{
            completion(TRUE);
        }
            break;
            
        case FBSessionStateClosed:{
        }
            break;
            
        case FBSessionStateClosedLoginFailed:
        case FBSessionStateOpenTokenExtended:{
            [FBSession.activeSession closeAndClearTokenInformation];
            completion(FALSE);
        }
            break;
        default:
            break;
    }
}

@end
