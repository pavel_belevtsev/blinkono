//
//  SocialProviderMail.h
//  KalturaUnified
//
//  Created by Alex Zchut on 3/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

@interface SocialProviderMail : SocialProviderBaseClass <MFMailComposeViewControllerDelegate>

@end
