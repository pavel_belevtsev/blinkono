//
//  SocialProviderBaseClass.m
//  KalturaUnified
//
//  Created by Alex Zchut on 3/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SocialProviderBaseClass.h"

@implementation SocialProviderBaseClass

- (void) share:(id <SocialProviderDelegate>) delegate mediaItem:(TVMediaItem *)mediaItem {
    _delegate = delegate;
    
    //implement in child classes
}

- (void) addComment:(NSString*) text mediaItem:(TVMediaItem *)mediaItem {
    //implement in child classes
}

- (void) aquirePermissions:(void(^)(BOOL success))completion {
    //implement in child classes
    completion(FALSE);
}

+ (void)logout {
    
}

@end
