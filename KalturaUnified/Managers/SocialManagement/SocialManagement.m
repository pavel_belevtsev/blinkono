//
//  SocialManagement.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SocialManagement.h"
#import "CommentView.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"

NSString * const KSocialCommentAddedNotification = @"KSocialCommentAddedNotification";

@interface SocialManagement()

@property (nonatomic, strong) CommentView *commentView;

@end

@implementation SocialManagement

- (void)requestforGetSocialFeedWithMediaID:(NSInteger)mediaID completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest * request = [TVPSocialAPI requestforGetSocialFeedWithMediaID:mediaID delegate:nil];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
    }];
    
    [request setFailedBlock:^{
        completionBlock(nil);
    }];
    
    [self sendRequest:request];
}

- (void)requestForAddCommentWithMediaID:(NSInteger)mediaID mediaType:(NSInteger)mediaType writer:(NSString *)writer header:(NSString *)header subheader:(NSString *)subheader content:(NSString *)content autoActive:(BOOL)autoActive completionBlock:(void(^)(id jsonResponse))completionBlock {
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForAddCommentWithMediaID:mediaID mediaType:mediaType writer:writer header:header subheader:subheader content:content autoActive:autoActive delegate:nil];
    
    [request setCompletionBlock:^{
        
        completionBlock([request JSONResponse]);
    }];
    
    [request setFailedBlock:^{
        completionBlock(nil);
    }];
    
    [self sendRequest:request];
}

- (void)updateWithMedia:(TVMediaItem *)mediaItem {

    self.mediaItem = mediaItem;
    
    if (!_imgThumb) {
        self.imgThumb = [[UIImageView alloc] init];
    }
    
    _imgThumb.image = nil;
    
    [_imgThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:_mediaItem andSize:CGSizeMake(400.0, 600.0)]];
}

- (void)shareAction:(NSInteger)socialAction {
    
    NSLog(@"share action %ld", (long)socialAction);
    
    if (socialAction == PlayerActionTypeFacebook) {
        [[self socialProviderById:SocialProviderTypeFacebook] share:self mediaItem:self.mediaItem];
        
    } else if (socialAction == PlayerActionTypeTwitter) {
        [[self socialProviderById:SocialProviderTypeTwitter] share:self mediaItem:self.mediaItem];
        
    } else if (socialAction == PlayerActionTypeEmail) {
        [[self socialProviderById:SocialProviderTypeMail] share:self mediaItem:self.mediaItem];

    } else if (socialAction == PlayerActionTypeComment) {
        [self socialShareAddComment];
    }
    
}

#pragma mark - Favorites

- (void)addMediaToFavorites:(TVMediaItem *)mediaItem {
    
    NSString * liveTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLinear];
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForActionDone:TVMediaActionAddToFavorites mediaID:[mediaItem.mediaID integerValue] mediaType:liveTypeID extraValue:0 delegate:nil];
    
    [request setFailedBlock:^{
        
        NSLog(@"failed request");
    }];
    
    [request setCompletionBlock:^
     {
         
         NSLog(@"addMediaToFavorites complete");
         [EPGM updateEPGListWithFavorite:mediaItem];
     
     }];
    
    [self sendRequest:request];
    
}

- (void)removeMediaFromFavorites:(TVMediaItem *)mediaItem {

    NSString * liveTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLinear];
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForActionDone:TVMediaActionRemoveFromFavorites mediaID:[mediaItem.mediaID integerValue] mediaType:liveTypeID extraValue:0 delegate:nil];
    
    [request setFailedBlock:^{
        NSLog(@"failed request");
    }];
    
    [request setCompletionBlock:^
     {
         NSLog(@"removeMediaFromFavorites complete");
         [EPGM updateEPGListWithFavorite:mediaItem];
     }];
    
    [self sendRequest:request];
}

#pragma mark - FB toggle
- (void)switchFacebookValueChange:(id)sender {
    
    if (![LoginM isFacebookUser] && self.commentView.switchFacebook.isOn){
        NSLog(@"switchFacebookValueChange");
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(loginWithFB)]) {
            [self.delegate loginWithFB];
        }
    }
}

#pragma mark - Comment

- (void)dismissSocialView {
    if (self.commentView.superview) {
        [self.commentView removeFromSuperview];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(dismissSocialShareView)]) {
        [self.delegate dismissSocialShareView];
    }
}

#pragma mark - Comment

- (void)socialShareAddComment {
    if (self.delegate && [self.delegate respondsToSelector:@selector(dismissSocialShareView)]) {
        [self.delegate dismissSocialShareView];
    }
    if (self.commentView.superview) {
        return;
    }
    if (!self.commentView) {
        self.commentView = [[CommentView alloc]init];
        [self.commentView.btnPost addTarget:self action:@selector(postComment) forControlEvents:UIControlEventTouchUpInside];
        [self.commentView.btnCancel addTarget:self action:@selector(cancelComment) forControlEvents:UIControlEventTouchUpInside];
        [self.commentView.switchFacebook addTarget:self action:@selector(switchFacebookValueChange:) forControlEvents:UIControlEventValueChanged];
    }
    
    self.commentView.mediaItem = self.mediaItem;
    [self.commentView prepareForShow];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(showCommentView:)]) {
        [self.delegate showCommentView:self.commentView];
    }
    if (isPhone) {
        [AppDelegate sharedAppDelegate].blockOrientationRotation = YES;
    }
}

- (void)cancelComment {
    if (self.delegate && [self.delegate respondsToSelector:@selector(removeCommentView)]) {
        [self.delegate removeCommentView];
    }
    [self.commentView removeFromSuperview];
    if (isPhone) {
        [AppDelegate sharedAppDelegate].blockOrientationRotation = NO;
    }
}

- (void)postComment {
    
    NSString *content = self.commentView.tvPostDetails.text;
    
    NSString *profileName = [LoginM getProfileName];
    [self requestForAddCommentWithMediaID:[self.mediaItem.mediaID integerValue] mediaType:[[[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:self.mediaItem.mediaTypeName] integerValue] writer:profileName header:LS(@"commentHeader") subheader:@"" content:content autoActive:YES completionBlock:^(id jsonResponse) {
        
        
        if (jsonResponse && [jsonResponse boolValue]) {
            [self socialShareAddCommentToSocialFlatform:content];
            [[NSNotificationCenter defaultCenter] postNotificationName:KSocialCommentAddedNotification object:nil userInfo:[NSDictionary dictionaryWithObject:self.mediaItem.mediaID forKey:@"mediaID"]];
            
        } else {
            [self failToAddComment];
        }
    }];

    [self cancelComment];
}


- (void)socialShareAddCommentToSocialFlatform:(NSString *)content {
    NSString *text = [NSString stringWithFormat:@"%@\n\n%@ %@", self.commentView.tvPostDetails.text, LS(@"shareMessage_Facebook"), self.commentView.mediaItem.name];
    
    if ([self.commentView.switchFacebook isOn]) {
         [[self socialProviderById:SocialProviderTypeFacebook] addComment:text mediaItem:self.commentView.mediaItem];
    }
    if ([self.commentView.switchTweet isOn]) {
        [[self socialProviderById:SocialProviderTypeTwitter] addComment:text mediaItem:self.commentView.mediaItem];
    }
}

- (void)failToAddComment {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(@"failure")
                                                    message:LS(@"post_comment_failed")
                                                   delegate:nil
                                          cancelButtonTitle:[LS(@"ok") uppercaseString]
                                          otherButtonTitles: nil];
    [alert show];
    if (self.delegate && [self.delegate respondsToSelector:@selector(dismissSocialShareView)]) {
        [self.delegate dismissSocialShareView];
    }
}

#pragma mark - providers
-(SocialProviderBaseClass*) socialProviderById: (NSInteger) providerId {
    NSDictionary *providerData = [APST.socialProviders filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSDictionary *dict, NSDictionary *bindings) {
        return ([dict[@"id"] integerValue] == providerId);
    }]].firstObject;
    
    if (providerData) {
        if (_currentProvider && [NSStringFromClass(_currentProvider.class) isEqualToString:providerData[@"className"]])
            return _currentProvider;
        else {
            //create new provider
            _currentProvider = [NSClassFromString(providerData[@"className"]) new];
            return _currentProvider;
        }
    }
    return nil;
}

+ (Class) socialProviderClassById: (NSInteger) providerId {
    NSDictionary *providerData = [APST.socialProviders filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSDictionary *dict, NSDictionary *bindings) {
        return ([dict[@"id"] integerValue] == providerId);
    }]].firstObject;
    
    if (providerData) {
        return NSClassFromString(providerData[@"className"]);
    }
    return [SocialProviderBaseClass class];
}

#pragma mark - providers delegate
- (void) presentViewController:(UIViewController*) viewControllerToPresent {
    [self.delegate presentViewController:viewControllerToPresent];
}

- (void) dismissViewController {
    [self.delegate dismissViewController];
}

@end
