//
//  SocialManagement.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseManagement.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "MyTokenCachingStrategy.h"
#import "SocialProviderBaseClass.h"

extern NSString * const KSocialCommentAddedNotification;

typedef NS_ENUM(NSInteger, PlayerActionType) {
    PlayerActionTypeFacebook = 1,
    PlayerActionTypeTwitter,
    PlayerActionTypeEmail,
    PlayerActionTypeLike,
    PlayerActionTypeComment,
    PlayerActionTypeFavorite
};

typedef NS_ENUM(NSInteger, SocialProviderType) {
    SocialProviderTypeFacebook = 1,
    SocialProviderTypeTwitter,
    SocialProviderTypeMail
};

@protocol SocialManagementDelegate <NSObject>

- (void)showCommentView:(UIView *)commentView;
- (void)removeCommentView;
- (void)dismissSocialShareView;
- (void)presentViewController:(UIViewController *)controller;
- (void)dismissViewController;
- (void)loginWithFB;

@end


@interface SocialManagement : BaseManagement <UINavigationControllerDelegate, SocialProviderDelegate>

- (void)requestforGetSocialFeedWithMediaID:(NSInteger)mediaID completionBlock:(void(^)(id jsonResponse))completionBlock;

- (void)updateWithMedia:(TVMediaItem *)mediaItem;
- (void)shareAction:(NSInteger)socialAction;

- (void)addMediaToFavorites:(TVMediaItem *)mediaItem ;
- (void)removeMediaFromFavorites:(TVMediaItem *)mediaItem;
- (SocialProviderBaseClass*) socialProviderById: (NSInteger) providerId;
+ (Class) socialProviderClassById: (NSInteger) providerId;

@property (nonatomic, weak) TVMediaItem *mediaItem;

@property (nonatomic, weak) id <SocialManagementDelegate> delegate;
@property (nonatomic, weak) BaseViewController *delegateForProvider;

@property (nonatomic, strong) UIImageView *imgThumb;
@property (nonatomic, strong) MyTokenCachingStrategy *tokenCaching;
@property (nonatomic, strong) SocialProviderBaseClass *currentProvider;

@end
