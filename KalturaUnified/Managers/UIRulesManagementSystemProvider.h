//
//  TVUIRulesManagementSystemProvider.h
//  Blink
//
//  Created by Rivka Schwartz on 5/20/15.
//  Copyright (c) 2015 Quickode Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NU(S)   TVNameForUIUnit(S)
#define NotificationNameUserStatusChanged @"NotificationNameUserStatusChanged"

typedef enum
{
    UIUnit_live_startOver_buttons,
    UIUnit_live_catchup_buttons,
    UIUnit_live_record_buttons,
    UIUnit_SeekBar_Thumb_StartOver,
    UIUnit_SeekBar_Thumb_Catchup,
    UIUnit_Play_StartOver,
    UIUnit_Play_Catchup,
    UIUnit_MyTV_RecordTab,
    UIUnit_MyTV_DownloadsTab,
    UIUnit_Social_buttons,
    UIUnit_favorites_buttons,
    UIUnit_live_playPause_button,
    UIUnit_Companion_buttons,
    UIUnit_Companion_play,
    UIUnit_Settings_Downloads,
    UIUnit_Download_buttons,
    UIUnit_Device_Downloads,
    
}UIRulesUnit;

NSString *TVNameForUIUnit(UIRulesUnit rulesUnit);




@interface UIRulesManagementSystemProvider : NSObject <TVUIRulesManagementSystemProvider>

+ (id) sharedInstance;
- (void)becomeDefaultInstance;
- (void) startTrackigForUserStatusWithCompletion:(void(^)()) completion;
    
@end
