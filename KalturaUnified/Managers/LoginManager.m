//
//  LoginManager.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginManager.h"
#import "SocialManagement.h"

NSString * const TVNUserDefaultsRememberMeFlagKey = @"TVNUserDefaultsRememberMeFlagKey";

@implementation LoginManager

+ (LoginManager *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

+ (UIImage *)getProfilePicture {
    UIImage *profileImage;
    if ([[TVSessionManager sharedTVSessionManager] isUserConnectedToFacebook]) {
        NSData * data = [[NSData alloc] initWithContentsOfURL:[[[TVSessionManager sharedTVSessionManager]currentUser] facebookPictureURLForSizeType:TVUserFacebookPictureTypeNormal]];
        profileImage = [UIImage imageWithData:data];
    } else{
        profileImage = [UIImage imageNamed:@"menu_avatar"];
    }
    return profileImage;
}

- (id)init {
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(suspensionStatusChanged:) name:NotificationNameSuspendedStatusChanged  object:nil];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:TVNUserDefaultsRememberMeFlagKey])
        {
            self.didRememberMePressed = [[NSUserDefaults standardUserDefaults] boolForKey:TVNUserDefaultsRememberMeFlagKey];
        }else
        {
            self.didRememberMePressed = YES;            
        }
        
    }
    return self;
}

- (void)logout {
    
    self.externalShareWatches = nil;
   
    [[SocialManagement socialProviderClassById:SocialProviderTypeFacebook] logout];
    
    if (APST.ipno) {
        
        [[TVSessionManager sharedTVSessionManager] logoutFromIPNO];
        
    } else {
        
        [[TVSessionManager sharedTVSessionManager] logout];
        
    }
    
    MenuM.menuItems = MenuM.defaultMenuItems;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:KMenuItemsUpdatedNotification object:nil];
    
}

- (void)fullLogout {
    
    self.externalShareWatches = nil;
    
    if ([[TVSessionManager sharedTVSessionManager] isUserConnectedToFacebook]) {
        
        [[SocialManagement socialProviderClassById:SocialProviderTypeFacebook] logout];
        [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];

    }
    
    [[TVSessionManager sharedTVSessionManager] logout];

}

- (void)rememberMeImplementation {

    [[NSUserDefaults standardUserDefaults] setBool:_didRememberMePressed forKey:TVNUserDefaultsRememberMeFlagKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (BOOL)isSignedIn {
    
        return [[TVSessionManager sharedTVSessionManager] isSignedIn];
}

- (void)signIn:(NSString*)email password:(NSString*)password {
    
    
    [[TVSessionManager sharedTVSessionManager] signInWithUsername:email password:password];

}

- (void)signInSecure {

    [[TVSessionManager sharedTVSessionManager] signInSecureWithUserName:self.facebookUser.tvinciName securePassword:self.facebookUser.data];
}

- (BOOL)isFacebookUser {

    return [[TVSessionManager sharedTVSessionManager] isUserConnectedToFacebook];

}

- (NSString *)getProfileName {
    
    NSString *firstName = @"";
    NSString *lastName = @"";
    
    if ([[TVSessionManager sharedTVSessionManager] currentUser]) {
        firstName = [[TVSessionManager sharedTVSessionManager] currentUser].firstName;
        lastName = [[TVSessionManager sharedTVSessionManager] currentUser].lastName;
    }
    
    if(firstName && lastName)
    {
        NSString *profileName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
        return profileName;
    }
    else
    {
        return @"";
    }
}

- (void)internetReachabilityStart{
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
}

- (void)seperatePermissions:(NSArray *) permissions {
    
    NSMutableArray * readPermissions = [NSMutableArray array];
    NSMutableArray * writePermissions = [NSMutableArray array];
    
    for (NSString * permission in permissions)
    {
        if ([self isPublishPermission:permission])
        {
            [writePermissions addObject:permission];
        }
        else
        {
            [readPermissions addObject:permission];
        }
    }
    
    self.readPermissions = [NSArray arrayWithArray:readPermissions];
    self.writePermissions = [NSArray arrayWithArray:writePermissions];
}


- (BOOL)isPublishPermission:(NSString*)permission {
    return [permission hasPrefix:@"publish"] ||
    [permission hasPrefix:@"manage"] ||
    [permission isEqualToString:@"ads_management"] ||
    [permission isEqualToString:@"create_event"] ||
    [permission isEqualToString:@"rsvp_event"];
}


-(void) suspensionStatusChanged:(NSNotification *) notification
{
    if ([[TVConfigurationManager sharedTVConfigurationManager] loginBlockedBySuspend] && [[TVSuspensionManager sharedInstance] isSuspended] && [[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        
        
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"User_suspended_alert_title")
                                                                message:LS(@"User_suspended_alert_content") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
            [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [[NSNotificationCenter defaultCenter] postNotificationName:KLogoutNotification object:nil];
            }];
            
        
    }
}

@end
