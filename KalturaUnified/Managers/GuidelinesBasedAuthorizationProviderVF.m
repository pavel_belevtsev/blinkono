//
//  GuidelinesBasedAuthorizationProviderVF.m
//  Vodafone
//
//  Created by Israel Berezin on 6/17/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "GuidelinesBasedAuthorizationProviderVF.h"
#import <TvinciSDK/TVSuspensionManager.h>
#import <TvinciSDK/TVCommercializationManager.h>

@implementation GuidelinesBasedAuthorizationProviderVF

static GuidelinesBasedAuthorizationProviderVF *_sharedInstance = nil;


-(BOOL) guidelinesBasedAuthorizationManager:(TVGuidelinesBasedAuthorization * ) sender
                         isDeniedForService:(TVGuidelineRelatedService ) service
{
    return NO;
}

@end
