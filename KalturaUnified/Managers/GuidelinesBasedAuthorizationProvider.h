//
//  TVGuidelinesBasedAuthorizationProvider.h
//  Blink
//
//  Created by Rivka Schwartz on 5/20/15.
//  Copyright (c) 2015 Quickode Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#define GU(S)   TVNameForGuideLineUnit(S)
#define NotificationNameUserStatusChanged @"NotificationNameUserStatusChanged"

typedef enum
{
    GuideLineUnit_live_startOver_buttons,
    GuideLineUnit_live_catchup_buttons,
    GuideLineUnit_live_record_buttons,
    GuideLineUnit_SeekBar_Thumb_StartOver,
    GuideLineUnit_SeekBar_Thumb_Catchup,
    GuideLineUnit_Play_StartOver,
    GuideLineUnit_Play_Catchup,
    GuideLineUnit_MyTV_RecordTab,
    GuideLineUnit_MyTV_DownloadsTab,
    GuideLineUnit_Social_buttons,
    GuideLineUnit_favorites_buttons,
    GuideLineUnit_live_playPause_button,
    GuideLineUnit_Companion_buttons,
    GuideLineUnit_Companion_play,
    GuideLineUnit_Settings_Downloads,
    GuideLineUnit_Download_buttons,
    GuideLineUnit_Device_Downloads,
    
}GuideLineUnit;

NSString *TVNameForGuideLineUnit(GuideLineUnit GuideLineUnit);




@interface GuidelinesBasedAuthorizationProvider : NSObject <TVGuidelinesBasedAuthorizationProvider>

+ (id) sharedInstance;
- (void)becomeDefaultInstance;
- (void) startTrackigForUserStatusWithCompletion:(void(^)()) completion;
- (void)becomeDefaultFactory;
@end
