//
//  RecoredAdapter.h
//  KalturaUnified
//
//  Created by Israel Berezin on 2/10/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseManagement.h"
#import <TvinciSDK/TVNPVRRecordItem.h>
#import "RecoredHelper.h"
#import "NSDate+Utilities.h"

typedef enum
{
    RecoredAdapterCallNameCancelItem=0,
    RecoredAdapterCallNameDeleateItem,
    RecoredAdapterCallNameCancelSeries,
    RecoredAdapterCallNameDeleateSeries,
    RecoredAdapterCallNameRecoredItem,
    RecoredAdapterCallNameRecoredItemSeries
}RecoredAdapterCallName;

typedef enum
{
    RecoredAdapterStatusOk,
    RecoredAdapterStatusFailed
}RecoredAdapterStatus;


@class RecoredAdapter;

@protocol RecoredAdapterDelegate <NSObject>

-(void) recoredAdapter:(RecoredAdapter *)recoredAdapter  complateCancelOrDeleateAssetRecording:(TVNPVRRecordItem *) recordItem  Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus;

-(void) recoredAdapter:(RecoredAdapter *)recoredAdapter  complateCancelOrDeleateSeriesRecording:(RecoredHelper*)recoredHelper recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus;

-(void)recoredAdapter:(RecoredAdapter *)recoredAdapter complateLoadRecoredItemsToChannel:(TVMediaItem*)mediaItem isChannelHeveRecoredItems:(BOOL)isChannelHeveRecoredItems scrollToProgram:(APSTVProgram *) scrolledProgram ;

-(void)recoredAdapter:(RecoredAdapter *)recoredAdapter complateRecordAssetOrSeriesToToChannel:(TVMediaItem*)mediaItem Program:(APSTVProgram *) epgProgram recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus;

-(NSArray*) currentProgramsToChannel:(TVMediaItem*)channel;
@end

@interface RecoredAdapter : BaseManagement

@property (assign,nonatomic) id<RecoredAdapterDelegate> delegate;
@property (strong,nonatomic) NSMutableDictionary * recoredDicByChannels;

- (void)cancelAssetRecording:(TVNPVRRecordItem *) recordItem  Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem;

- (void)deleteAssetRecording:(TVNPVRRecordItem *) recordItem Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem;


- (void)recordSeriesByProgram:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem;

- (void)recordAsset:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem;

-(TVPAPIRequest *)loadRecoredByChannel:(TVMediaItem *)epgChannel andPrograms:(NSArray*)currentProgramsToChannel scrollToProgram:(APSTVProgram *) scrolledProgram;

-(RecoredHelper *)recoredHelperToepgProgram:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem;

-(NSMutableDictionary*)takeAllChannelRecoreds:(TVMediaItem *)channel;

-(void)cleanAllReoredsDataFromMemory;

-(RecoredHelper*)takeOrMakeRecoredHelperWithMediaItem:(TVMediaItem*) mediaItem andProgram:(APSTVProgram*)program isSeries:(BOOL)isSeries;

-(void)deleteOneRecored:(TVNPVRRecordItem *) recordItem Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem;

-(void) cancelOrDeleteSeriesRecordingWithCnacelRecoredType:(CnacelRecoredType)cnacelRecoredType WithRecoredHelper:(RecoredHelper*)recoredHelper;
@end
