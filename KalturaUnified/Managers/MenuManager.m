//
//  MenuManager.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 06.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MenuManager.h"
#import "HomeViewController.h"
#import "CategoryViewController.h"
#import "MenuOperator.h"

NSString * const MainMenuItemsCATEGORY_2_3 = @"Category_2_3";
NSString * const MainMenuItemsCATEGORY_16_9 = @"Category_16_9";
NSString * const MainMenuItemsSEARCH = @"Search";
NSString * const MainMenuItemsONLINE_PURCHASES = @"OnlinePurchases";


NSString * const TVPageLayoutCover = @"Cover";

NSString * const TVNUserDefaultsCustomSkin = @"TVNUserDefaultsCustomSkin";
NSString * const TVNUserSkinChangedNotification = @"TVNUserSkinChangedNotification";
NSString * const TVNUserDefaultsMenuId = @"TVNUserDefaultsMenuId";

@implementation MenuManager

+ (MenuManager *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (id)init {
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (MenuOperator*) menuOperator {
    if (!_menuOperator)
        _menuOperator = [[MenuOperator alloc] initWithQueue:self.networkQueue];
    return _menuOperator;
}

- (void)updateMenuItems:(NSDictionary *)jsonResponse {
    
    NSDictionary *mainMenu = [jsonResponse dictionaryByRemovingNSNulls];
    NSMutableArray *mainMenuItems = [NSMutableArray array];
    NSArray *items = [mainMenu objectForKey:@"MenuItems"];
    
    
    NSString *unFixedURL;
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
    
    for (NSDictionary *itemDict in items) {
        unFixedURL = [itemDict objectForKey:@"URL"];
        for (NSInteger i = 0; i < [unFixedURL length]; i++) {
            if ([[unFixedURL substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"\""] && ![[unFixedURL substringWithRange:NSMakeRange(i+1, 1)] isEqualToString:@"["] && ![[unFixedURL substringWithRange:NSMakeRange(i-1, 1)] isEqualToString:@"]"]) {
                unFixedURL = [unFixedURL stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@"''"];
            }
        }
        
        tempDict = [[NSMutableDictionary alloc] initWithDictionary:itemDict];
        [tempDict setObject:unFixedURL forKey:@"URL"];
        
        if ([[tempDict objectForKey:@"Name"] isEqualToString:@"Start"]) {
            
            //NSLog(@"%@", [tempDict description]);
            
        }

        
        TVMenuItem *menuItem = [[TVMenuItem alloc] initWithDictionary:tempDict];
         
        [mainMenuItems addObject:menuItem];
    }
    
    self.menuItems = mainMenuItems;
    self.defaultMenuItems = mainMenuItems;
    
    if (APST.ipno && [[TVSessionManager sharedTVSessionManager] currentUser]) {
        
        [MenuM loadUserMenu];
        
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:KMenuItemsUpdatedNotification object:nil];
    
}

- (TVMenuItem *)menuCategoryItem:(NSString *)categoryName {
    
    for (TVMenuItem *menuItem in _menuItems) {
        
        if ([menuItem.layout[@"Type"] isEqualToString:MainMenuItemsCATEGORY_2_3] || [menuItem.layout[@"Type"] isEqualToString:MainMenuItemsCATEGORY_16_9]) {
            
            if ([menuItem.name isEqualToString:categoryName]) {
                return menuItem;
            }
            
        }
        
    }
    
    return nil;
    
}

- (TVMenuItem *)menuItem:(NSString *)type {


    NSString * lowerCaseType = [type lowercaseString];

    for (TVMenuItem *menuItem in _menuItems) {

        NSString * lowerCaseMenuType = [menuItem.layout[@"Type"] lowercaseString];
        if ([lowerCaseMenuType isEqualToString:lowerCaseType]) {
            
            return menuItem;
            
        }
        
    }

    return nil;
    
}

- (void)selectMenuItem:(TVMenuItem *)menuItem {
    
    
}


- (void)updateDomainMenu {
    
    if (APST.ipno) {

        NSLog(@"createOperatorToUser");
        
        [self.menuOperator createOperatorToUser:^(BOOL result) {
            if (result)
                [self createMenuOperator];
        }];
    }
    
}

- (void)createMenuOperator {
    
    if ([TVSessionManager sharedTVSessionManager].userOperator) {
        
        TVIPNOperator *userOperator = [TVSessionManager sharedTVSessionManager].userOperator;
        
        [[NSUserDefaults standardUserDefaults] setObject:userOperator.operatorMenuId forKey:TVNUserDefaultsMenuId];
        
        NSMutableDictionary *customSkin = [[NSMutableDictionary alloc] init];
        
        [customSkin setObject:@"#FFFFFF" forKey:@"brandTextColor"];
        
        
        if (userOperator.picURL && [userOperator.picURL.absoluteString length]) {
            [customSkin setObject:userOperator.picURL.absoluteString forKey:@"logoURLString"];
        }
        if (userOperator.colorCode && [userOperator.colorCode length]) {
            [customSkin setObject:userOperator.colorCode forKey:@"brandColor"];
        }
        if (userOperator.operatorAboutUs && [userOperator.operatorAboutUs length]) {
            [customSkin setObject:userOperator.operatorAboutUs forKey:@"aboutUsUrl"];
        }
        if (userOperator.operatorConrtactUs && [userOperator.operatorConrtactUs length]) {
            [customSkin setObject:userOperator.operatorConrtactUs forKey:@"mailAddress"];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:customSkin forKey:TVNUserDefaultsCustomSkin];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [APST updateData];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:TVNUserSkinChangedNotification object:self];
        
        [self.menuOperator getMenuByCurrentOpratorWithCompletionBlock:^(NSDictionary *menuItemsDic, NSError *error) {
            
            //NSLog([menuItemsDic description]);
            
            if (error)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:KMenuItemsUpdatedErrorNotification object:nil];
            }
            NSArray *MenuItems = [menuItemsDic objectForKey:@"MenuItems"];
            NSString *unFixedURL;
            NSMutableDictionary *tempDict;
            
            if (MenuItems) {
                
                NSMutableArray *mainMenuItems = [NSMutableArray array];
                
                for (NSMutableDictionary *itemDict in MenuItems) {
                    
                    unFixedURL = [itemDict objectForKey:@"URL"];
                    for (NSInteger i = 0; i < [unFixedURL length]; i++) {
                        if ([[unFixedURL substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"\""] && ![[unFixedURL substringWithRange:NSMakeRange(i+1, 1)] isEqualToString:@"["] && ![[unFixedURL substringWithRange:NSMakeRange(i-1, 1)] isEqualToString:@"]"]) {
                            unFixedURL = [unFixedURL stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@"''"];
                        }
                    }
                    
                    tempDict = [[NSMutableDictionary alloc] initWithDictionary:itemDict];
                    [tempDict setObject:unFixedURL forKey:@"URL"];
                    
                    TVMenuItem *menuItem = [[TVMenuItem alloc] initWithDictionary:tempDict];
                    [mainMenuItems addObject:menuItem];

                    
                }
                
                self.menuItems = mainMenuItems;

                self.menuItems = [NSArray arrayWithArray: mainMenuItems];
                
                if ([[TVConfigurationManager sharedTVConfigurationManager] defaultIPNOID].length >0)
                {
                    self.defaultMenuItems = [NSArray arrayWithArray: mainMenuItems];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:KMenuItemsUpdatedNotification object:nil];
                
            }
            
        }];
    }
}

- (NSString *)getCachesPath:(NSString *)fileName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [paths objectAtIndex:0];
    
    return [docsDir stringByAppendingPathComponent:fileName];
}

- (void)saveUserMenu:(NSString *)menuStr {
    
    [menuStr writeToFile:[self getCachesPath:@"userMenu.plist"] atomically:NO encoding:NSUTF8StringEncoding error:nil];
}

- (BOOL)loadUserMenu {
    
    NSString *path = [self getCachesPath:@"userMenu.plist"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        
        NSString *jsonString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *mainMenu = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSMutableArray *mainMenuItems = [NSMutableArray array];
        
        NSLog(@"User menu:\n%@]", [mainMenu description]);
        
        NSArray *items = [mainMenu objectForKey:@"MenuItems"];
        
        NSString *unFixedURL;
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
        
        for (NSDictionary *itemDict in items) {
            unFixedURL = [itemDict objectForKey:@"URL"];
            for (NSInteger i = 0; i < [unFixedURL length]; i++) {
                if ([[unFixedURL substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"\""] && ![[unFixedURL substringWithRange:NSMakeRange(i+1, 1)] isEqualToString:@"["] && ![[unFixedURL substringWithRange:NSMakeRange(i-1, 1)] isEqualToString:@"]"]) {
                    unFixedURL = [unFixedURL stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@"''"];
                }
            }
            
            tempDict = [[NSMutableDictionary alloc] initWithDictionary:itemDict];
            [tempDict setObject:unFixedURL forKey:@"URL"];
            
            TVMenuItem *menuItem = [[TVMenuItem alloc] initWithDictionary:tempDict];
            [mainMenuItems addObject:menuItem];
            
        }
        
        self.menuItems = mainMenuItems;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:KMenuItemsUpdatedNotification object:nil];
        
        return YES;
    }
    return NO;
}


@end
