//
//  MenuManager.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 06.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TvinciSDK/TVMenuItem.h>
#import "BaseNetworkObject.h"

typedef NS_ENUM(NSInteger, MenuCategoryTypeSize) {
    MenuCategoryTypeSize_2_3 = 0,
    MenuCategoryTypeSize_16_9
};

extern NSString * const MainMenuItemsCATEGORY_2_3;
extern NSString * const MainMenuItemsCATEGORY_16_9;
extern NSString * const MainMenuItemsSEARCH;
extern NSString * const MainMenuItemsONLINE_PURCHASES;
extern NSString * const TVPageLayoutCover;

extern NSString * const TVNUserDefaultsCustomSkin;
extern NSString * const TVNUserSkinChangedNotification;
extern NSString * const TVNUserDefaultsMenuId;

@class MenuOperator;
@interface MenuManager : BaseNetworkObject

+ (MenuManager *)instance;
- (void)updateMenuItems:(NSDictionary *)jsonResponse;
- (TVMenuItem *)menuItem:(NSString *)type;
- (TVMenuItem *)menuCategoryItem:(NSString *)categoryName;
- (void)selectMenuItem:(TVMenuItem *)menuItem;
- (void)updateDomainMenu;
- (void)saveUserMenu:(NSString *)menuStr;

@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, strong) NSArray *defaultMenuItems;
@property (nonatomic, strong) MenuOperator *menuOperator;
@end
