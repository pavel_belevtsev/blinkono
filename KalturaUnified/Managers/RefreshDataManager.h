//
//  RefreshDataManagerViewController.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 4/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RefreshDataManager : BaseViewController

+ (instancetype)sharedInstance;
-(void) addAllTasks;

@end
