//
//  ViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginBaseViewController.h"

#define notificationNameRestartApp @"notificationNameRestartApp"

@interface ViewController : LoginBaseViewController

@property (nonatomic, weak) IBOutlet UIImageView *imgSpinner;
@property (nonatomic, weak) IBOutlet UIView *viewActivity;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;



@end

