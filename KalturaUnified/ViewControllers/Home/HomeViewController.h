//
//  HomeViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeNavProfileView.h"
#import "MyZoneViewController.h"

@interface HomeViewController : BaseViewController <HomeNavProfileViewDelegate> {
    
    BOOL isInitialized;
    
}

@property (weak, nonatomic) IBOutlet UIView *viewHome;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (strong, nonatomic)  UILabel *labelTitlePad;
@property (weak, nonatomic) IBOutlet UIButton *buttonAvatar;

@property BOOL fullReload;

- (void)updateMenu;

@end
