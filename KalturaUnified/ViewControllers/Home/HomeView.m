//
//  HomeView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "HomeView.h"

@implementation HomeView

- (void)reloadHomePages {
    
}

- (void)reloadHomePagesContent {
    
}

- (void)startTimers {
    
}

- (void)stopTimers {
    
}

@end
