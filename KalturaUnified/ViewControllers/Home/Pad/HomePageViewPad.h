//
//  HomePageViewPad.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 15.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "TVMediaItem+PSTags.h"

@class MediaItemView;

@interface HomePageViewPad : UINibView

- (void)updateData:(TVMenuItem *)menu;
- (void)updateContinueData;
- (void)updateAnimationForOffset:(float)offset;
- (void)startTimer;
- (void)stopTimer;

@property (nonatomic, weak) HomeViewController *delegate;

@property (nonatomic, assign) TVMenuItem *menuItem;

@property (nonatomic, strong) TVMediaItem *mediaItemSpotlight;
@property (nonatomic, strong) NSMutableArray *mediaItemSubviews;

@property (nonatomic, strong) NSMutableArray *items;

@property (nonatomic, strong) NSTimer *timerAnimation;
@property (nonatomic) NSInteger timerStep;

@property (nonatomic, strong) NSTimer *timerSpotlight;
@property (nonatomic, strong) NSMutableArray *spotlightItems;
@property (nonatomic) NSInteger spotlightIndex;

@property (nonatomic, weak) MediaItemView *spotlightItemView;

@end
