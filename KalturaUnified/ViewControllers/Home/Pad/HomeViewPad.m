//
//  HomeViewPad.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "HomeViewPad.h"
#import "HomePageViewPad.h"

@implementation HomeViewPad

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self addingPagerView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadHomePages) name:KMenuItemsUpdatedNotification object:nil];
    
}

- (void)addingPagerView {
    CGRect pageControlFrame = _viewNavigation.bounds;
    self.viewPageControl = [[PageControlView alloc] initWithFrame:pageControlFrame];
    [self.viewPageControl.control addTarget:self action:@selector(pageControlChanged:) forControlEvents:UIControlEventValueChanged];
    self.viewPageControl.numberOfPages = 0;
    [_viewNavigation addSubview:self.viewPageControl];
}


- (void)reloadHomePagesContent {
    
    TVMenuItem *menuItem = [MenuM menuItem:TVPageLayoutHome];
    
    int tag = 0;
    
    if (menuItem) {
        
        for (TVMenuItem *menu in menuItem.children) {
            if ([menu.layout objectOrNilForKey:@"HomePageType"]) {// && ![[menu.layout objectOrNilForKey:@"HomePageType"] isEqualToString:@"HPTV"]) {
                
                HomePageViewPad *homePage = (HomePageViewPad *)[_scrollViewHomePages viewWithTag:(100 + tag++)];
                if (homePage) {
                    [homePage updateContinueData];
                }
            
            }
        }
    }

    
}

- (void)reloadHomePages {
    
    self.viewPageControl.numberOfPages = 0;
    
    [_scrollViewHomePages removeAllSubviewsKindOfClass:[HomePageViewPad class]];

    TVMenuItem *menuItem = [MenuM menuItem:TVPageLayoutHome];
    
    int frameX = 0;
    int tag = 0;
    
    if (menuItem) {
        
        for (TVMenuItem *menu in menuItem.children) {
            if ([menu.layout objectOrNilForKey:@"HomePageType"]) {// && ![[menu.layout objectOrNilForKey:@"HomePageType"] isEqualToString:@"HPTV"]) {
                
                HomePageViewPad *homePage = [[HomePageViewPad alloc] initWithNib];
                homePage.delegate = super.delegate;
                homePage.tag = (100 + tag++);
                [homePage updateData:menu];
                homePage.frame = CGRectMake(frameX, 0, _scrollViewHomePages.frame.size.width, _scrollViewHomePages.frame.size.height);
                [_scrollViewHomePages addSubview:homePage];
                frameX += homePage.frame.size.width;
                _scrollViewHomePages.contentSize = CGSizeMake(frameX, _scrollViewHomePages.frame.size.height);
            }
        }
    }
    
    self.viewPageControl.numberOfPages = tag;
    self.viewPageControl.currentPage = 0;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [_scrollViewHomePages performBlockOnSubviewsKindOfClass:[HomePageViewPad class] block:^(UIView *subview) {
        [(HomePageViewPad*)subview updateAnimationForOffset:(scrollView.contentOffset.x - subview.frame.origin.x)];
    }];
    
    CGFloat pageWidth = CGRectGetWidth(scrollView.frame);
    int page = scrollView.contentOffset.x / pageWidth;
    
    TVMenuItem *menuItem = [MenuM menuItem:TVPageLayoutHome];
    TVMenuItem * item = [menuItem.children objectAtIndex:page];
    [self upadatePageTitle:item];
    
    [self.viewPageControl setCurrentPage:page];
    
}

- (void)pageControlChanged:(id)sender {
    UIPageControl *pageControl = sender;
    CGFloat pageWidth = _scrollViewHomePages.frame.size.width;
    CGPoint scrollTo = CGPointMake(pageWidth * pageControl.currentPage, 0);
    [_scrollViewHomePages setContentOffset:scrollTo animated:YES];
}


-(void)upadatePageTitle:(TVMenuItem*)menuItem
{
    if (APST.showHomePageTitles)
    {
        if (menuItem)
        {
            self.delegate.labelTitlePad.text = menuItem.name;
        }
    }
}

- (void)startTimers {
    
    for (HomePageViewPad *homePage in [_scrollViewHomePages subviews]) {
        if ([homePage isKindOfClass:[HomePageViewPad class]]) {
            [homePage startTimer];
        }
    }
    
}

- (void)stopTimers {
    
    for (HomePageViewPad *homePage in [_scrollViewHomePages subviews]) {
        if ([homePage isKindOfClass:[HomePageViewPad class]]) {
            [homePage stopTimer];
        }
    }
    
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

@end
