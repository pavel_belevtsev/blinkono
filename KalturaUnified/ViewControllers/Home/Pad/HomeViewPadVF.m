//
//  HomeViewPadVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "HomeViewPadVF.h"

@implementation HomeViewPadVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)reloadHomePages {
    
    [super reloadHomePages];
    self.backgroundColor = [VFTypography instance].darkGrayColor;
}

@end
