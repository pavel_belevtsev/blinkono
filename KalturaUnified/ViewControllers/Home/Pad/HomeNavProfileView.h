//
//  HomeNavProfileView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeNavProfileViewDelegate <NSObject>

- (void)openMyZone;

@end

@interface HomeNavProfileView : UIView

@property (nonatomic, weak) IBOutlet UILabel *labelProfileName;
@property (weak, nonatomic) IBOutlet UIButton *buttonProfileAvatar;
@property (nonatomic, weak) id <HomeNavProfileViewDelegate> delegate;

-(void)updateUserName;

@end
