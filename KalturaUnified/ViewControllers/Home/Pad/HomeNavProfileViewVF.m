//
//  HomeNavProfileViewVF.m
//  Vodafone
//
//  Created by Israel Berezin on 2/24/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "HomeNavProfileViewVF.h"

@implementation HomeNavProfileViewVF

/* "PSC : Vodafone Grup : change color & font and Text: JIRA ticket. */
-(void)updateUserName
{
    self.labelProfileName.text = LS(@"navigation_bar_my_name");
    self.hidden = NO;
    CGRect frame = self.labelProfileName.frame;
    frame.origin.x =50;
    self.labelProfileName.frame  =frame;
}

@end
