//
//  HomeNavProfileView.m
//  KalturaUnified
//
//  Created by Synergetica LLC on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "HomeNavProfileView.h"

@implementation HomeNavProfileView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [Utils setLabelFont:_labelProfileName bold:YES];
    
    [self setProfileName];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renameNotification:) name:KRenameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renameNotification:) name:TVSessionManagerLogoutCompletedNotification object:nil];
    
    self.buttonProfileAvatar.isAccessibilityElement = YES;
    self.buttonProfileAvatar.accessibilityLabel = @"Button Avatar Home";
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)updateUserName
{
    NSString *userName = @"";
    if ([TVSessionManager sharedTVSessionManager].currentUser) {
        userName = [TVSessionManager sharedTVSessionManager].currentUser.firstName;
    }
    self.labelProfileName.text = [NSString stringWithFormat:@"%@'s %@", userName, LS(@"zone")];
    self.hidden = NO;

}

- (void)setProfileName {
    
    NSString *userName = @"";
    
    if ([TVSessionManager sharedTVSessionManager].currentUser) {
        userName = [TVSessionManager sharedTVSessionManager].currentUser.firstName;
    }
    
    UIImage *image = [LoginManager getProfilePicture];
    self.buttonProfileAvatar.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.buttonProfileAvatar setImage:image forState:UIControlStateNormal];
    self.buttonProfileAvatar.layer.masksToBounds = YES;
    self.buttonProfileAvatar.layer.cornerRadius = self.buttonProfileAvatar.frame.size.width / 2.f;
    self.buttonProfileAvatar.layer.borderColor = [UIColor grayColor].CGColor;
    self.buttonProfileAvatar.layer.borderWidth = 1.5f;
    
    if (userName.length)
    {
        [self updateUserName];
    }
    else
    {
        if (APST.ipno)
        {
            self.labelProfileName.text = LS(@"navigation_bar_join_now");
        }
        else
        {
            self.labelProfileName.text = LS(@"navigation_bar_my_name");
        }
    }
}

- (IBAction)buttonOpenPressed:(UIButton *)button {
    
    if (_delegate && [_delegate respondsToSelector:@selector(openMyZone)]) {
        [_delegate openMyZone];
    }
    
}

- (void)renameNotification:(NSNotification*)notification {
    [self setProfileName];
}

@end
