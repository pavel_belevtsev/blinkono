//
//  HomePageViewPad.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 15.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "HomePageViewPad.h"
#import "MediaItemView.h"
#import "MediaItemFullscreenView.h"
#import "HomePageContinueView.h"
#import "HomePageNoFriendsView.h"
#import "HomePageFriendsActivityView.h"

const NSInteger homePageAnimationSteps = 30;

@implementation HomePageViewPad

- (void)updateData:(TVMenuItem *)menu {
    
    [self showHUD];
    
    _menuItem = menu;
    
    _spotlightItems = nil;
    
    NSInteger channelSpotlightID = 0, channelSubitemsID = 0;
    
    for (TVMenuItem *menuItem in _menuItem.children) {
        if ([[menuItem.layout objectOrNilForKey:@"Type"] isEqualToString:@"Spotlight"]) {
            channelSpotlightID = menuItem.channelID;
            _spotlightItems = [[NSMutableArray alloc] init];
            _spotlightIndex = 0;
        } else if ([[menuItem.layout objectOrNilForKey:@"Type"] isEqualToString:@"SubItems"]) {
            channelSubitemsID = menuItem.channelID;
        }
    }
    
    NSLog(@"%ld %ld", (long)channelSpotlightID, (long)channelSubitemsID);
    
    if (channelSubitemsID == 0)
        channelSubitemsID = channelSpotlightID;
    
    if (channelSubitemsID == 0) {
        [self loadHPTV];
    } else {
        [self loadChannelWithSpotLightId:channelSpotlightID subItemsId:channelSubitemsID];
    }
    
}

- (void)loadHPTV {
    
    [self.delegate requestForEpgHightlightsFromValue:1 toValue:4 withPageSize:maxPageSize andPageIndex:0 withCompletionBlock:^(NSArray *programsArray, NSError *error)
    {
         NSArray * sortedArray = [programsArray sortedArrayUsingComparator:^NSComparisonResult(TVEPGProgram * program1 , TVEPGProgram * program2) {

                                        return [program2.startDateTime compare:program1.startDateTime];
                                  }];
        
         TVEPGProgram * theProgram = nil;
         
         if (!error && sortedArray.count > 0) {
             
              theProgram = sortedArray[0];
             
             _mediaItemSubviews = [[NSMutableArray alloc] init];
             for (TVEPGProgram *item in sortedArray) {
                 [_mediaItemSubviews addObject:item];
             }
         }
         
         NSLog(@"programForMainGallery\n\n%@",theProgram);
         // set the main gallery
         [self nextStepForSpotLightId:0 subItemsId:0];
    }];
}

- (void)loadChannelWithSpotLightId:(NSInteger)spotLightId subItemsId:(NSInteger)subItemsId {
    
    __block NSInteger channelId = (spotLightId > 0) ? spotLightId : subItemsId;
    [self.delegate requestForGetChannelMediaList:channelId pageSize:100 pageIndex:0 orderBy:TVOrderByNoOrder completion:^(NSArray *result) {
        
        if ([result count]) {
            if (spotLightId > 0 && spotLightId == channelId) {
                
                [_spotlightItems removeAllObjects];
                
                for (NSDictionary *dic in result) {
                    TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dic];
                    [_spotlightItems addObject:item];
                }
                _spotlightIndex = (rand() % [result count]);
                _mediaItemSpotlight = [_spotlightItems objectAtIndex:_spotlightIndex];
            }
            if (subItemsId > 0 && subItemsId == channelId) {
                _mediaItemSubviews = [[NSMutableArray alloc] init];
                for (NSDictionary *dic in result) {
                    TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dic];
                    [_mediaItemSubviews addObject:item];
                }
                
                for (int i = 0; i < [_mediaItemSubviews count]; i++) {
                    int j = (rand() % [_mediaItemSubviews count]);
                    if (i != j) {
                        [_mediaItemSubviews exchangeObjectAtIndex:i withObjectAtIndex:j];
                    }
                }
            }
        }
        [self nextStepForSpotLightId:spotLightId subItemsId:subItemsId];
    }];
}

- (void)nextStepForSpotLightId:(NSInteger)spotLightId subItemsId:(NSInteger)subItemsId {
    
    if (spotLightId > 0 && subItemsId > 0 && spotLightId != subItemsId) {
        [self loadChannelWithSpotLightId:0 subItemsId:subItemsId];
    }
    else {
        
        [self hideHUD];
        
        _items = [[NSMutableArray alloc] init];
        
        NSLog(@"DONE %@   %ld", _mediaItemSpotlight.name, (long)[_mediaItemSubviews count]);
        
        NSString *pageType = [self.menuItem.layout objectOrNilForKey:@"HomePageType"];
        NSLog(@"pageType '%@'", pageType);
        //pageType = @"HP3";
        
        NSDictionary *layoutData = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:pageType ofType:@"plist"]];
        NSArray *array = [layoutData objectForKey:@"items"];
        
        int subItemIndex = 0;
        int tag = 10;
        
        _spotlightItemView = nil;
        
        for (NSDictionary *item in array) {
            //NSLog([item description]);
            
            NSString *type = [item objectForKey:@"type"];
            
            if ([item objectForKey:@"fullscreen"] && _mediaItemSpotlight) {
                
                MediaItemFullscreenView *itemView = [[MediaItemFullscreenView alloc] initWithNib];
                itemView.frame = self.bounds;
                itemView.tag = tag++;
                [self addSubview:itemView];
                [itemView updateData:_mediaItemSpotlight];
                
                itemView.delegateController = self.delegate;
                
                _spotlightItemView = itemView;
                [_items addObject:item];
                
            } else if ([item objectForKey:@"fullscreen"] && [type isEqualToString:@"HPTV"] && _mediaItemSubviews && (subItemIndex < [_mediaItemSubviews count])) {
                
                MediaItemFullscreenView *itemView = [[MediaItemFullscreenView alloc] initWithNib];
                itemView.frame = self.bounds;
                itemView.tag = tag++;
                [self addSubview:itemView];
                
                TVEPGProgram *program = [_mediaItemSubviews objectAtIndex:subItemIndex++];
                
                if ([program isKindOfClass:[TVEPGProgram class]]) {
                    [itemView updateEPGData:program];
                } else {
                    [itemView updateData:((TVMediaItem *)program)];
                }
                
                itemView.delegateController = self.delegate;
                
                [_items addObject:item];
                
            } else if ([type isEqualToString:@"Spotlight"] || [type isEqualToString:@"SubItems"] || [type isEqualToString:@"HPTV"]) {
                
                MediaItemView *itemView = [[MediaItemView alloc] initWithNib];
                itemView.frame = CGRectMake([[item objectForKey:@"x"] intValue], [[item objectForKey:@"y"] intValue], [[item objectForKey:@"width"] intValue], [[item objectForKey:@"height"] intValue]);
                itemView.tag = tag++;
                [self addSubview:itemView];
                
                if ([type isEqualToString:@"Spotlight"] && _mediaItemSpotlight) {
                    [itemView updateData:_mediaItemSpotlight];
                    _spotlightItemView = itemView;
                    
                } else if ([type isEqualToString:@"SubItems"] && _mediaItemSubviews && (subItemIndex < [_mediaItemSubviews count])) {
                    [itemView updateData:[_mediaItemSubviews objectAtIndex:subItemIndex++]];
                } else if ([type isEqualToString:@"HPTV"] && _mediaItemSubviews && (subItemIndex < [_mediaItemSubviews count])) {
                    
                    TVEPGProgram *program = [_mediaItemSubviews objectAtIndex:subItemIndex++];
                    
                    if ([program isKindOfClass:[TVEPGProgram class]]) {
                        [itemView updateEPGData:program];
                    } else {
                        [itemView updateData:((TVMediaItem *)program)];
                    }

                    
                }
                
                itemView.delegateController = self.delegate;
                
                [_items addObject:item];
                
            } else if ([type isEqualToString:@"Continue"]) {
                
                int tagView = tag++;
                [_delegate requestForGetUserStartedWatchingMediasWithNumOfItems:1 completion:^(NSArray *result) {
                   
                    if ([result count]) {
                        
                        NSString *mediaID = [result lastObject];
                        
                        [_delegate requestForGetMediaInfoWithMediaID:[mediaID integerValue] completion:^(NSDictionary *result) {
                            
                            if (result) {
                                MediaItemView *itemView = [[MediaItemView alloc] initWithNib];
                                itemView.frame = CGRectMake([[item objectForKey:@"x"] intValue], [[item objectForKey:@"y"] intValue], [[item objectForKey:@"width"] intValue], [[item objectForKey:@"height"] intValue]);
                                itemView.tag = tagView;
                                [self addSubview:itemView];
                                
                                [itemView updateData:[[TVMediaItem alloc] initWithDictionary:result]];
                                [itemView updateForContinue];
                                itemView.delegateController = self.delegate;
                                
                                [_items addObject:item];
                            }
                        }];
                        
                    } else {
                        
                        HomePageContinueView *itemView = [[HomePageContinueView alloc] initWithNib];
                        itemView.frame = CGRectMake([[item objectForKey:@"x"] intValue], [[item objectForKey:@"y"] intValue], [[item objectForKey:@"width"] intValue], [[item objectForKey:@"height"] intValue]);
                        itemView.tag = tagView;
                        [self addSubview:itemView];
                        
                        [_items addObject:item];
                        
                    }
                    
                }];
                
                

            } else if ([type isEqualToString:@"Friends"]) {
                
                HomePageFriendsActivityView *itemView = [[HomePageFriendsActivityView alloc] initWithNib];
                itemView.frame = CGRectMake([[item objectForKey:@"x"] intValue], [[item objectForKey:@"y"] intValue], [[item objectForKey:@"width"] intValue], [[item objectForKey:@"height"] intValue]);
                itemView.delegateController = _delegate;
                [itemView createButtonsAndLabels];

                [itemView updateFriendActivity];
                
                itemView.tag = tag++;
                [self addSubview:itemView];
                
                [_items addObject:item];
                
            }
            
        }
        
        [self updateAnimationForOffset:self.frame.size.width];
        
        if (_timerAnimation == nil && (self.tag == 100)) {
            
            _timerStep = 0;
            _timerAnimation = [NSTimer scheduledTimerWithTimeInterval:(0.5 / ((float)homePageAnimationSteps)) target:self selector:@selector(animationProcess) userInfo:nil repeats:YES];
        }
    }
}

- (void)animationProcess {

    if (_timerStep < homePageAnimationSteps) {
        _timerStep++;
        float offset = (homePageAnimationSteps - _timerStep) * self.frame.size.width / ((float)homePageAnimationSteps);
        [self updateAnimationForOffset:offset];
        
        if (_timerStep >= homePageAnimationSteps) {
            [_timerAnimation invalidate];
            _timerAnimation = nil;
        }
    }
}

- (void)updateAnimationForOffset:(float)offset {

    float dOffset = fabsf(offset / self.frame.size.width);
    if (dOffset > 1.0) dOffset = 1.0;

    for (int i = 0; i < [_items count]; i++) {
        NSDictionary *item = [_items objectAtIndex:i];
        //UIView *subview = [[self subviews] objectAtIndex:i];
        UIView *subview = [self viewWithTag:(10 + i)];
        
        if (subview) {
            if ([[item objectOrNilForKey:@"animation"] isEqualToString:@"fade"]) {
                subview.alpha = 1.0 - dOffset;
            } else {
                
                float dDelay = (1.0 - ((subview.frame.origin.x + subview.frame.size.width) / (self.frame.size.width))) / 3.0;
                
                float dY = (dOffset * self.frame.size.height) - dDelay * (self.frame.size.height);
                if (dY < 0) dY = 0;
                
                dY = dY * ([[item objectOrNilForKey:@"animation"] isEqualToString:@"up"] ? -1 : 1);
                subview.frame = CGRectMake(subview.frame.origin.x, [[item objectForKey:@"y"] intValue] + dY, subview.frame.size.width, subview.frame.size.height);
                
            }
        }
    }
}

- (void)updateContinueData {
    
    for (MediaItemView *itemViewOld in [self subviews]) {
        
        if (([itemViewOld isKindOfClass:[MediaItemView class]] && itemViewOld.isContinue) || ([itemViewOld isKindOfClass:[HomePageContinueView class]])) {
            
            CGRect rect = itemViewOld.frame;
            int tag = itemViewOld.tag;
            
            [_delegate requestForGetUserStartedWatchingMediasWithNumOfItems:1 completion:^(NSArray *result) {
                
                if ([result count]) {
                    
                    NSString *mediaID = [result lastObject];
                    
                    [_delegate requestForGetMediaInfoWithMediaID:[mediaID integerValue] completion:^(NSDictionary *result) {
                        
                        if (result) {
                            MediaItemView *itemView = [[MediaItemView alloc] initWithNib];
                            itemView.frame = rect;
                            itemView.tag = tag;
                            [self addSubview:itemView];
                            
                            [itemView updateData:[[TVMediaItem alloc] initWithDictionary:result]];
                            [itemView updateForContinue];
                            itemView.delegateController = self.delegate;
                            
                        }
                    }];
                    
                } else {
                    
                    HomePageContinueView *itemView = [[HomePageContinueView alloc] initWithNib];
                    itemView.frame = rect;
                    itemView.tag = tag;
                    [self addSubview:itemView];
                    
                }
                
                [itemViewOld removeFromSuperview];
                
            }];
        } else if ([itemViewOld isKindOfClass:[HomePageFriendsActivityView class]]) {
            
            [((HomePageFriendsActivityView *)itemViewOld) updateFriendActivity];
            
        }
        
    }
    
}

- (void)updateSpotlight {

    UIScrollView *scrollView = (UIScrollView *)[self superview];
    
    if ([scrollView isKindOfClass:[UIScrollView class]]) {
        
        if (scrollView.contentOffset.x == self.frame.origin.x) {
            
            if (_spotlightItems && [_spotlightItems count]) {
                if (++_spotlightIndex >= [_spotlightItems count]) {
                    _spotlightIndex = 0;
                }
                
                if (_spotlightItemView) {
                    
                    [UIView animateWithDuration:0.5 animations:^{
                        _spotlightItemView.alpha = 0.0;
                    } completion:^(BOOL finished) {
                        [_spotlightItemView updateData:[_spotlightItems objectAtIndex:_spotlightIndex]];
                        [UIView animateWithDuration:0.5 animations:^{
                            _spotlightItemView.alpha = 1.0;
                        }];
                        
                    }];
                    
                }
            }
            
        }
        
    }
    
    
}

- (void)startTimer {
    
    if (_timerSpotlight) {
        [self startTimer];
    }
    
    _timerSpotlight = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(updateSpotlight) userInfo:nil repeats:YES];
}

- (void)stopTimer {
    
    if (_timerSpotlight) {
        [_timerSpotlight invalidate];
        _timerSpotlight = nil;
    }
}

@end
