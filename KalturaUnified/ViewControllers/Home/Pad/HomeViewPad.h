//
//  HomeViewPad.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeView.h"
#import "PageControlView.h"

@interface HomeViewPad : HomeView

@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewHomePages;
@property (nonatomic, weak) IBOutlet UIView *viewNavigation;
@property (nonatomic, strong) PageControlView *viewPageControl;

@end
