//
//  CoverPageView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVMediaItem+PSTags.h"

@class CoverView;

@interface CoverPageView : UIView

@property (weak, nonatomic) IBOutlet UIView *viewCoverImage;
@property (weak, nonatomic) IBOutlet UIImageView *coverImage;
@property (weak, nonatomic) IBOutlet UIImageView *coverBlurImage;

- (void)updateData:(TVMediaItem *)mediaItem;
- (void)setImageOffset:(float)offset;
- (void)setImageVOffset:(float)offset;
- (void)showDetails;

@property int index;
@property (nonatomic, strong) TVMediaItem *mediaItem;
@property (nonatomic, weak) CoverView *coverView;

@end
