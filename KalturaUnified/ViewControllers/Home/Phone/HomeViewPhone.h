//
//  HomeViewPhone.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeView.h"

@class CoverView;

@interface HomeViewPhone : HomeView {
    
    CGFloat lastScrollOffsetX;
    
}

- (void)updateData:(TVMenuItem *)item;
- (void)updateContent;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewContent;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) CoverView *coverView;

@end
