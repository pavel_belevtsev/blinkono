//
//  CoverPageView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CoverPageView.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Blur.h"
#import "UIImage+ImageEffects.h"
#import "CoverView.h"

@implementation CoverPageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRegnize:)];
    [self addGestureRecognizer:recognizer];
    
}

- (void)tapGestureRegnize:(UITapGestureRecognizer *)recognizer {
    
    if (_mediaItem && _coverView.delegate) {
        
        if (_mediaItem.isMovie) {
            NSLog(@"%@ %@", _mediaItem.mediaTypeName, _mediaItem.name);
        }
        
    }
    
}


- (void)updateData:(TVMediaItem *)mediaItem {
    
    _coverImage.image = nil;
    
    self.mediaItem =  mediaItem;
    
    NSURL *url = [Utils getBestPictureURLForMediaItem:_mediaItem andSize:CGSizeMake(800.0, 1200.0)];
    
    //__weak UIImageView *imgBlur = self.coverBlurImage;

    __weak CoverPageView *weakSelf = self;
    
    [_coverImage sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!error && image) {
        //    imgBlur.image = [image applyDarkEffect];
            [weakSelf performSelectorInBackground:@selector(updateBlurImage:) withObject:image];
        }
    }];
    
}

- (void)updateBlurImage:(UIImage *)image {
    
    self.coverBlurImage.image = [image applyDarkEffect];
    
}

- (void)setImageOffset:(float)offset {

    _coverImage.frame = CGRectMake(offset, 0, _coverImage.frame.size.width, _coverImage.frame.size.height);
    _coverBlurImage.frame = CGRectMake(offset, 0, _coverBlurImage.frame.size.width, _coverBlurImage.frame.size.height);
    
}

- (void)setImageVOffset:(float)offset {
    
    _coverImage.frame = CGRectMake(0, -offset, _coverImage.frame.size.width, _coverImage.frame.size.height);
    _coverBlurImage.frame = CGRectMake(0, -offset, _coverBlurImage.frame.size.width, _coverBlurImage.frame.size.height);
    _coverBlurImage.alpha = offset / 10.0;
    
}


- (void)showDetails {

}

- (void)dealloc {


}
@end
