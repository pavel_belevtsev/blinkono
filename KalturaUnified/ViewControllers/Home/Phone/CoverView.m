//
//  CoverView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CoverView.h"
#import "CoverPageView.h"
#import "HomeViewPhone.h"
#import <TvinciSDK/UIImage+Resize.h>

const CGFloat navigationBarHeight = 44.0;

@implementation CoverView

- (void)updateData:(TVMenuItem *)item {
    
    if (!self.homeView) {
        self.homeView = [[HomeViewPhone alloc] initWithNib];
        self.homeView.delegate = _delegate;
    }
    
    [_homeView updateData:item];
    
    if ([_coverMediaList count] && _coverItem && ([[_coverItem.layout objectForKey:ConstKeyMenuItemChannel] integerValue] == [[item.layout objectForKey:ConstKeyMenuItemChannel] integerValue])) return;
    
    self.coverItem = item;
    
    [self showHUD];
    
    imgArrow.hidden = YES;
    
    [Utils setLabelFont:labelTitle bold:YES];
    [Utils setLabelFont:labelSubTitle];
    
    currentPage = 0;
    blurImagePage = -1;
    

    
    coverScroll.hidden = YES;
    viewPageControl.hidden = YES;
    
    [self.delegate requestForGetChannelMediaList:[[item.layout objectForKey:ConstKeyMenuItemChannel] integerValue] pageSize:maxPageSize pageIndex:0 orderBy:TVOrderByNoOrder completion:^(NSArray *result) {
        
        if ([result count]) {
            NSMutableArray *mediaItems = [[NSMutableArray alloc] init];
            for (NSDictionary *dic in result) {
                if ([mediaItems count] < 10) {
                    TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dic];
                    [mediaItems addObject:item];
                }
            }
            
            self.coverMediaList = mediaItems;
            
            [self reloadCoverPages];
            
        }
        
        [self hideHUD];
        
    }];
    
}

- (void)reloadCoverPages {
    
    coverScroll.hidden = NO;
    viewPageControl.hidden = NO;
    
    imgArrow.hidden = NO;
    
    NSInteger mediaCount = [_coverMediaList count];
    
    float dotSize = viewPageControl.frame.size.height;
    
    viewPageControl.frame = CGRectMake(viewPageControl.frame.origin.x, viewPageControl.frame.origin.y, dotSize * mediaCount, dotSize);
    
    [viewPageControl removeAllSubviews];
    
    if (mediaCount > 1) {
        for (int i = 0; i < mediaCount; i++) {
            
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(i * dotSize, 0, dotSize, dotSize)];
            img.image = [UIImage imageNamed:(i == 0 ? @"home_cover_page_white" : @"home_cover_page_gray")];
            [viewPageControl addSubview:img];
            
        }

    }
    
    pageLoaded = 0;
    
    NSInteger pageLoadedCount = (mediaCount >= 5 ? 5 : mediaCount);
    
    for (int i = 0; i < pageLoadedCount; i++) {
        
        TVMediaItem *mediaItem = [_coverMediaList objectAtIndex:i];
        
        CGRect frame= CGRectMake(i * coverScroll.frame.size.width, 0.0, coverScroll.frame.size.width, coverScroll.frame.size.height);
        
        CoverPageView *coverView = [[[NSBundle mainBundle] loadNibNamed:@"CoverPageView" owner:self options:nil] objectAtIndex:0];
        coverView.coverView = self;
        coverView.frame = frame;
        coverView.index = i;
        coverView.tag = 100 + i;
        [coverView updateData:mediaItem];
        
        [coverScroll addSubview:coverView];
        
        if (i == 0) {
            
            [self updateLabels:mediaItem];
            
            
        }
        
    }
    
    [coverScroll setContentSize:CGSizeMake(coverScroll.frame.size.width * mediaCount, coverScroll.frame.size.height)];
    
    [viewDivider removeFromSuperview];
    [coverScroll addSubview:viewDivider];
    
    
    [_homeView removeFromSuperview];
    _homeView.frame = CGRectMake(0, navigationBarHeight, coverScroll.frame.size.width, coverScroll.frame.size.height - navigationBarHeight);
    [_homeView updateContent];
    _homeView.scrollViewContent.contentSize = CGSizeMake(coverScroll.contentSize.width, _homeView.scrollViewContent.contentSize.height);
    _homeView.coverView = self;
    _homeView.scrollViewContent.contentOffset = CGPointZero;
    
    [self addSubview:_homeView];
    
    [viewTapHome removeFromSuperview];
    viewTapHome.frame = _homeView.bounds;
    [_homeView.scrollViewContent addSubview:viewTapHome];
    
}

- (void)updateLabels:(TVMediaItem *)mediaItem {
    
    
    labelTitle.text = mediaItem.name;
    
    NSString *subName = @"";
    
    if ([mediaItem.tags objectForKey:@"Genre"]) {
        
        NSArray *array = [mediaItem.tags objectForKey:@"Genre"];
        
        for (NSString *str in array) {
            
            if ([subName length] > 0) {
                subName = [NSString stringWithFormat:@"%@, ", subName];
            }
            
            subName = [NSString stringWithFormat:@"%@%@", subName, str];
            
        }
    }
    
    labelSubTitle.text = subName;
    
}

- (void)updateCoverPageOffset {
    
    UIScrollView *scrollView = coverScroll;
    
    scrollView.contentOffset = CGPointMake(_homeView.scrollViewContent.contentOffset.x, 0);
    
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    if (page >= 0 && page < [[viewPageControl subviews] count]) {
        
        if (page != currentPage) {
            
            currentPage = page;
            for (int i = 0; i < [[viewPageControl subviews] count]; i++) {
                
                UIImageView *img = [[viewPageControl subviews] objectAtIndex:i];
                img.image = [UIImage imageNamed:(i == page ? @"home_cover_page_white" : @"home_cover_page_gray")];
                
            }
            
            [self updateLabels:[_coverMediaList objectAtIndex:page]];
            
        }
        
        int cPage = ((int)(scrollView.contentOffset.x / scrollView.frame.size.width));
        
        if (cPage != pageLoaded) {
            pageLoaded = cPage;
            
            //NSLog(@"pageLoaded %d", pageLoaded);
            
            if (pageLoaded < [_coverMediaList count] - 2) {
                
                int newTag = 100 + pageLoaded + 2;
                
                int i = pageLoaded + 2;
                
                TVMediaItem *mediaItem = [_coverMediaList objectAtIndex:i];
                //NSLog(@"update %d %@", i, mediaItem.name);
                
                CoverPageView *coverView = (CoverPageView *)[coverScroll viewWithTag:newTag];
                
                if (!coverView) {
                    
                    coverView = (CoverPageView *)[coverScroll viewWithTag:100 + pageLoaded - 3];
                    //NSLog(@"coverView %d", 100 + pageLoaded - 3);
                    
                    if (coverView) {
                        
                        CGRect frame= CGRectMake(i * coverScroll.frame.size.width, 0.0, coverScroll.frame.size.width, coverScroll.frame.size.height);
                        coverView.frame = frame;
                        coverView.index = i;
                        coverView.tag = newTag;
                        [coverView updateData:mediaItem];
                        
                    }
                    
                }
                
            }
            
            if (pageLoaded > 1) {
                
                int newTag = 100 + pageLoaded - 2;
                
                int i = pageLoaded - 2;
                
                TVMediaItem *mediaItem = [_coverMediaList objectAtIndex:i];
                //NSLog(@"update %d %@", i, mediaItem.name);
                
                CoverPageView *coverView = (CoverPageView *)[coverScroll viewWithTag:newTag];
                
                if (!coverView) {
                    
                    coverView = (CoverPageView *)[coverScroll viewWithTag:100 + pageLoaded + 3];
                    //NSLog(@"coverView %d", 100 + pageLoaded + 3);
                    
                    if (coverView) {
                        
                        CGRect frame= CGRectMake(i * coverScroll.frame.size.width, 0.0, coverScroll.frame.size.width, coverScroll.frame.size.height);
                        coverView.frame = frame;
                        coverView.index = i;
                        coverView.tag = 100 + i;
                        [coverView updateData:mediaItem];
                        
                    }
                    
                    
                }
                
                
            }
            
            [scrollView performBlockOnSubviewsKindOfClass:[CoverPageView class] block:^(UIView *subview) {
                [(CoverPageView *)subview setImageOffset:0];
            }];
            
            viewDivider.hidden = YES;
            
            homeScroll.frame = CGRectMake(currentPage * coverScroll.frame.size.width, 0, coverScroll.frame.size.width, coverScroll.frame.size.height);
            
        }
        
        
        if ((int)(scrollView.contentOffset.x) % (int)(scrollView.frame.size.width) == 0) {
            //currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
            
            
        } else {
            
            float divider = 3.0;
            
            float offset = (scrollView.contentOffset.x - (scrollView.frame.size.width * currentPage)) / divider;
            
            [scrollView performBlockOnSubviewsKindOfClass:[CoverPageView class] block:^(UIView *subview) {
                int index = ((CoverPageView *)subview).index;
                
                if (index == currentPage) {
                    [(CoverPageView *)subview setImageOffset:offset];
                }
                
                if (offset > 0 && index == currentPage + 1) {
                    [(CoverPageView *)subview setImageOffset:-scrollView.frame.size.width / divider + offset];
                    viewDivider.hidden = NO;
                    viewDivider.frame = CGRectMake((currentPage + 1) * scrollView.frame.size.width, 0, viewDivider.frame.size.width, scrollView.frame.size.height);
                }
                
                if (offset < 0 && index == currentPage - 1) {
                    [(CoverPageView *)subview setImageOffset:scrollView.frame.size.width / divider + offset];
                    viewDivider.hidden = NO;
                    viewDivider.frame = CGRectMake(currentPage * scrollView.frame.size.width - viewDivider.frame.size.width, 0, viewDivider.frame.size.width, scrollView.frame.size.height);
                }
            }];
        }
    }
}

- (IBAction)tapGestureRegnize:(UITapGestureRecognizer *)recognizer {
    
    UIScrollView *scrollView = _homeView.scrollViewContent;
    
    if (scrollView.contentOffset.y == 0) {
    
        int cPage = ((int)(scrollView.contentOffset.x / scrollView.frame.size.width));
        
        if (cPage >= 0 && cPage < [_coverMediaList count]) {
            TVMediaItem *mediaItem = [_coverMediaList objectAtIndex:cPage];
            
            [_delegate openMediaItem:mediaItem];
        }
        
    }
    
}

- (void)updateHome {
    
    viewTapHome.frame = CGRectMake(_homeView.scrollViewContent.contentOffset.x, viewTapHome.frame.origin.y, viewTapHome.frame.size.width, viewTapHome.frame.size.height);
    
    float alpha = _homeView.scrollViewContent.contentOffset.y / _homeView.scrollViewContent.frame.size.height;
    if (alpha < 0) {
        alpha = 0;
    } else if (alpha > 1.0) {
        alpha = 1.0;
    }
    
    viewPageControl.alpha = 1.0 - alpha * 2;
    imgArrow.alpha = viewPageControl.alpha;
    labelTitle.alpha = viewPageControl.alpha;
    labelSubTitle.alpha = viewPageControl.alpha;
    
    __block NSInteger index = 0;
    [coverScroll performBlockOnSubviewsKindOfClass:[CoverPageView class] block:^(UIView *subview) {
        [(CoverPageView *)subview setImageVOffset:alpha * 10];
        index++;

    }];
    
    if (_homeView.scrollViewContent.contentOffset.y <= 0) {
        [self updateCoverPageOffset];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}

@end
