//
//  HomeViewPhone.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "HomeViewPhone.h"
#import "CoverView.h"
#import "MediaItemView.h"
#import "MediaItemTextView.h"
#import "HomePageContinueView.h"
#import "HomePageFriendsActivityView.h"

@implementation HomeViewPhone

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView == self.scrollViewContent) {
        
        scrollView.pagingEnabled = (scrollView.contentOffset.y < scrollView.frame.size.height * 1.0);
        
        if (!scrollView.pagingEnabled && scrollView.contentOffset.y > 0) {
            scrollView.contentOffset = CGPointMake(lastScrollOffsetX, scrollView.contentOffset.y);
        }
        
        if (scrollView.contentOffset.y > 0) {
        //    [self showElements];
        } else {
            lastScrollOffsetX = scrollView.contentOffset.x;
        }
        
        self.viewContent.frame = CGRectMake(self.scrollViewContent.contentOffset.x + 8, self.viewContent.frame.origin.y, self.viewContent.frame.size.width, self.viewContent.frame.size.height);
        
        [_coverView updateHome];
        
    }
    
}

- (void)updateData:(TVMenuItem *)item {
    
    [_viewContent removeAllSubviews];
    
    //if ([type isEqualToString:@"Spotlight_16_9"] || [type isEqualToString:@"Spotlight_2_3"]
    
    CGFloat y = 8.0;
    
    for (TVMenuItem *homeItem in item.children) {
        NSString *type = [homeItem.layout objectForKey:ConstKeyMenuItemType];
        
        CGFloat x = 0.0;
        
        UIView *subview = nil;
        
        if ([type isEqualToString:@"Friends"]) {
            /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
            if (!APST.fbSupport && [LoginM isSignedIn]) {
                continue;
            }
            x = -8.0;
            
             HomePageFriendsActivityView *itemView = [[HomePageFriendsActivityView alloc] initWithNib];
             itemView.delegateController = self.delegate;
            [itemView createButtonsAndLabels];
            [itemView updateFriendActivity];
            
             subview = itemView;
            
        } else if ([type isEqualToString:@"Highlighted Content"]) {
            
            MediaItemView *itemView = [[MediaItemView alloc] initWithNib];
            [itemView updateForPhoneHome];
            itemView.delegateController = self.delegate;
            
            subview = itemView;
            
        } else if ([type isEqualToString:@"Spotlight_16_9"]) {
            MediaItemView *itemView = [[MediaItemView alloc] initWithNib];
            [itemView updateForPhoneHome];
            itemView.delegateController = self.delegate;
            
            subview = itemView;
            
        } else if ([type isEqualToString:@"Spotlight_2_3"]) {
            MediaItemTextView *itemView = [[MediaItemTextView alloc] initWithNib];
            itemView.delegateController = self.delegate;
            
            subview = itemView;
            
        } else if ([type rangeOfString:@"Continue"].location == 0) {
            
            HomePageContinueView *itemView = [[HomePageContinueView alloc] initWithNib];
            subview = itemView;
            
        }
    
        if (subview) {
            subview.frame = CGRectMake(x, y, subview.frame.size.width, subview.frame.size.height);
            [_viewContent addSubview:subview];
            y += subview.frame.size.height + 8.0;
            
            if ([subview isKindOfClass:[HomePageContinueView class]]) {
                
                [super.delegate requestForGetUserStartedWatchingMediasWithNumOfItems:1 completion:^(NSArray *result) {
                    
                    if ([result count]) {
                        
                        NSString *mediaID = [result lastObject];
                        
                        [super.delegate requestForGetMediaInfoWithMediaID:[mediaID integerValue] completion:^(NSDictionary *result) {
                            
                            if (result) {
                                
                                MediaItemView *itemView = [[MediaItemView alloc] initWithNib];
                                [itemView updateForPhoneHome];
                                itemView.delegateController = self.delegate;
                                
                                TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:result];
                                [itemView updateData:item];
                                [itemView updateForContinue];
                                
                                itemView.frame = subview.frame;
                                [subview removeFromSuperview];
                                
                                [_viewContent addSubview:itemView];
                                
                            }
                        }];
                        
                     }
                    
                }];

            } else if ([subview isKindOfClass:[MediaItemView class]]) {
                
                if ([type isEqualToString:@"Highlighted Content"]) {
                    
                    [self.delegate requestForEpgHightlightsFromValue:1 toValue:4 withPageSize:maxPageSize andPageIndex:0 withCompletionBlock:^(NSArray *programsArray, NSError *error) {
                        
                        NSArray * sortedArray = [programsArray sortedArrayUsingComparator:^NSComparisonResult(TVEPGProgram * program1 , TVEPGProgram * program2) {
                            
                            return [program2.startDateTime compare:program1.startDateTime];
                            
                        }];
                        
                        if (!error && sortedArray.count > 0) {
                            
                            [(MediaItemView *)subview updateEPGData:sortedArray[0]];
                        }
                    }];
                    
                    
                } else {
                    
                    [self.delegate requestForGetChannelMediaList:[[homeItem.layout objectForKey:ConstKeyMenuItemChannel] integerValue] pageSize:1 pageIndex:0 orderBy:TVOrderByNoOrder completion:^(NSArray *result) {
                        
                        if ([result count]) {
                            
                            NSDictionary *dic = [result lastObject];
                            TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dic];
                            [(MediaItemView *)subview updateData:item];
                        }
                        
                    }];
                }
                
            } 
        }
    }
    
    _viewContent.frame = CGRectMake(_viewContent.frame.origin.x, _viewContent.frame.origin.y, _viewContent.frame.size.width, y);
    
    [self updateContent];
    
}

- (void)updateContent {
    
    _viewContent.frame = CGRectMake(_viewContent.frame.origin.x, self.frame.size.height, _viewContent.frame.size.width, MAX(_viewContent.frame.size.height, self.frame.size.height));
    _scrollViewContent.contentSize = CGSizeMake(_scrollViewContent.contentSize.width, _viewContent.frame.origin.y + _viewContent.frame.size.height);
    
}

@end
