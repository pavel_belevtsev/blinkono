//
//  CoverView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "TVMediaItem+PSTags.h"

@class HomeViewPhone;

@interface CoverView : UINibView {
    
    IBOutlet UIScrollView *coverScroll;
    IBOutlet UIPageControl *coverPagingControl;
    IBOutlet UIView *slideView;
    IBOutlet UIView *viewTapHome;
    
    IBOutlet UIImageView *imgBlur;

    int currentPage;
    int pageLoaded;
    
    IBOutlet UIView *viewDivider;
    
    IBOutlet UIScrollView *homeScroll;
    
    IBOutlet UIImageView *imgScreenForBlur;

    int blurValue;
    
    IBOutlet UIImageView *imgArrow;
    IBOutlet UIView *viewPageControl;
    IBOutlet UILabel *labelTitle;
    IBOutlet UILabel *labelSubTitle;

    int blurImagePage;
    
}

- (void)updateData:(TVMenuItem *)item;
- (void)updateCoverPageOffset;
- (void)updateHome;

@property (nonatomic, weak) HomeViewController *delegate;
@property (nonatomic, strong) NSArray *coverMediaList;
@property (nonatomic, strong) HomeViewPhone *homeView;
@property (nonatomic, strong) TVMenuItem *coverItem;

@end
