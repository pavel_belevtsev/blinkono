//
//  HomePageFriendsActivityView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomePageFriendsActivityView : UINibView {
    
    IBOutlet UILabel *labelTitle;
    
    IBOutlet UITableView *tableViewActivity;
    
    int friendsItemsCount;
    
    IBOutlet UIView *viewNoActivity;
    IBOutlet UIView *viewNoLogin;

    IBOutlet UILabel *labelNoActivity;
    
    IBOutlet UIButton *buttonSeeMore;
    IBOutlet UIButton *buttonLoginFB;
    
    IBOutlet UIButton *buttonLogin;
    IBOutlet UIButton *buttonLoginFBLogin;
    IBOutlet UILabel *labelNoLogin;
    IBOutlet UIView *viewButtonCreate;
}

- (void) updateFriendActivity;
- (void) createButtonsAndLabels;

@property (nonatomic, weak) BaseViewController *delegateController;
@property (nonatomic, strong) NSMutableArray *myZoneList;
@property (nonatomic, strong) NSMutableArray *usersList;
@property (nonatomic, strong) NSMutableArray *mediasList;

@end
