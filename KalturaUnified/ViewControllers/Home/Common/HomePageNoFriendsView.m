//
//  HomePageNoFriendsView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "HomePageNoFriendsView.h"

@implementation HomePageNoFriendsView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [Utils setLabelFont:_labelTitle bold:YES];
    [Utils setLabelFont:_labelText];
    
    _labelTitle.text = LS(@"friends_activity");
    _labelText.text = LS(@"homepage_friends_activity_sub_text");

    [_buttonLoginFacebook setTitle:LS(@"login_facebook") forState:UIControlStateNormal];
    
}

- (void)updateSkin {
    
    [_delegate initFacebookButtonUI:_buttonLoginFacebook];
    
}

@end
