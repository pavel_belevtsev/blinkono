//
//  MediaItemFullscreenView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MediaItemFullscreenView.h"
#import "UIImageView+WebCache.h"

@implementation MediaItemFullscreenView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
}
/* "PSC : Vodafone Ono :  FIX home page image. */

- (void)updateData:(TVMediaItem *)item {
    
    super.mediaItem = item;
    
    [super.imageViewThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:super.mediaItem andSize:super.imageViewThumb.frame.size] placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"]];
    
    UIImage *sizeableImage = [UIImage imageNamed:@"img_spotlight_shadow"];
    sizeableImage = [sizeableImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    CGFloat fadeImgH = sizeableImage.size.height + 20;
    CGFloat fadeImgY = self.frame.size.height - fadeImgH + 60;
    if (self.fadeImg == nil || [self.fadeImg superview] == nil)
    {
        self.fadeImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, fadeImgY, self.frame.size.width, fadeImgH)];
        self.fadeImg.image = sizeableImage;
        [self addSubview:self.fadeImg];
    }

    
}

@end
