//
//  HomePageFriendsActivityView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "HomePageFriendsActivityView.h"
#import "MyZoneFriendsActivityCell.h"
#import "UIImageView+WebCache.h"
#import "AnalyticsManager.h"

@implementation HomePageFriendsActivityView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {


    }
    return self;
}

- (void)awakeFromNib {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showEmptyScreen) name:TVSessionManagerUserDetailsAvailableNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showEmptyScreen) name:TVSessionManagerSignInCompletedNotification object:nil];
    
    self.usersList = [[NSMutableArray alloc] init];
    self.mediasList = [[NSMutableArray alloc] init];
    self.myZoneList = [[NSMutableArray alloc] init];
    

    [Utils setLabelFont:labelTitle bold:YES];
    labelTitle.text = LS(@"friends_activity");
    /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
    if (!APST.fbSupport) {
        [labelTitle setHidden:!APST.fbSupport];
        [labelNoLogin setHidden:!APST.fbSupport];
    }
    friendsItemsCount = 0;
    
    [Utils setLabelFont:labelNoActivity];
    
    [self updateEmptyScreen];
    [buttonLoginFB setTitle:LS(@"login_with_facebook") forState:UIControlStateNormal];
    viewNoActivity.hidden = YES;
    
    [Utils setButtonFont:buttonSeeMore bold:YES];
    [buttonSeeMore setTitle:LS(@"see_more") forState:UIControlStateNormal];
    [buttonSeeMore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonSeeMore.hidden = YES;
    
    [Utils setLabelFont:labelNoLogin];
    labelNoLogin.adjustsFontSizeToFitWidth = YES;

    labelNoLogin.text = LS(@"hpFriendsActivity_AnonymousMessage");
    
    [buttonLoginFBLogin setTitle:LS(@"login_facebook") forState:UIControlStateNormal];
    
    [buttonLogin setTitle:[NSString stringWithFormat:LS(@"login_brand"), APST.brandName] forState:UIControlStateNormal];

    
    viewNoLogin.hidden = YES;
    
    buttonSeeMore.backgroundColor = APST.brandColor;
    buttonSeeMore.layer.cornerRadius = 3.0f;
    
    [buttonSeeMore addTarget:self action:@selector(buttonSeeMore:) forControlEvents:UIControlEventTouchUpInside];

    [self updateFriendActivity];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
}
    
- (IBAction)buttonFBLoginPressed:(UIButton *)button {

    [AnalyticsM.currentEvent clearFields];
    AnalyticsM.currentEvent.category = @"Login and registration";
    AnalyticsM.currentEvent.action = @"Facebook User Sign in";
    AnalyticsM.currentEvent.label = @"Home page";
    [self.delegateController intermediateLogin:@"fbLogin"];
    
}

-(void) createButtonsAndLabels{
    
    [_delegateController initFacebookButtonUI:buttonLoginFB];
    [_delegateController initFacebookButtonUI:buttonLoginFBLogin];
    [_delegateController initBrandButtonUI:buttonLogin];
    
    /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
    if (!APST.fbSupport) {
        [buttonLogin setFrame:buttonLoginFBLogin.frame];
        [buttonLoginFBLogin setHidden:YES];
    }
    
    TVUnderlineButton *buttonCreate = [_delegateController createUnderlineButtonUI:viewButtonCreate font:buttonLoginFBLogin.titleLabel.font];
    
    if (APST.multiUser) {
        [buttonCreate setTitle:LS(@"guest") forState:UIControlStateNormal];
        buttonCreate.hidden = !APST.showGuestUser;
        [buttonCreate setTouchUpInsideAction:^(UIButton *sender) {
            [self.delegateController intermediateLogin:@"buttonJoinVCPressed"];
        }];
    } else {
        [buttonCreate setTitle:LS(@"create_a_free_account") forState:UIControlStateNormal];
        [buttonCreate setTouchUpInsideAction:^(UIButton *sender) {
            [self.delegateController intermediateLogin:@"buttonCreateAccountPressed"];
        }];
    }
    [buttonCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonCreate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    buttonSeeMore.hidden = ![LoginM isFacebookUser];

}

- (IBAction)itemSelected:(UIButton *)button {
    
    if (button.tag == 1) {
        [self.delegateController intermediateLogin:@"fbLogin"];
        
    } else if (button.tag == 2) {
        [self.delegateController intermediateLogin:@"regularLogin"];
    }
}

- (void)updateEmptyScreen {

    if ([LoginM isSignedIn]){
        viewNoLogin.hidden = YES;
        viewNoActivity.hidden = NO;
        
        if (![LoginM isFacebookUser]) {
           buttonLoginFB.hidden = NO;
            labelNoActivity.text = LS(@"hpFriendsActivity_RegularMessage");
        }
        else{
            buttonLoginFB.hidden = YES;
            labelNoActivity.text = LS(@"hpFriendsActivity_NoActivitiesMessage");
        }
    } else {
        viewNoActivity.hidden = YES;
        viewNoLogin.hidden = NO;
    }

}

- (void)enableContent {
    [self enableContent:YES];
}

- (void)enableContent:(BOOL)isEnable {
    buttonLoginFB.enabled = isEnable;
}

- (void)touchDown:(UIButton *)sender {
    
    sender.backgroundColor = APST.brandDarkColor;
    
}

- (void)touchUp:(UIButton *)sender {
    
    sender.backgroundColor = APST.brandColor;
}

- (void)buttonSeeMore:(UIButton *)sender {
    
    [self touchUp:sender];
    [_delegateController openMyZoneWithActivityLoad];
}

- (void)updateFriendActivity {
    
    viewNoActivity.hidden = YES;
    tableViewActivity.hidden = YES;
    buttonSeeMore.hidden = YES;
    
    if ([LoginM isFacebookUser]) {
        
        __weak HomePageFriendsActivityView *weakSelf = self;
        
        
        NSArray *actions = @[@"WATCHES", @"LIKE", @"POST"];
        
        __weak TVPAPIRequest *request = [TVPSocialAPI requestForGetFriendsActionsWithUserActions:actions assetType:TVAssetType_MEDIA assetID:0 startIndex:0 numOfRecords:100 socialPlatform:TVSocialPlatformFacebook delegate:nil];
        
        [request setCompletionBlock:^{
            
            NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
            
            [weakSelf updateData:result];
            
        }];
        
        
        [request setFailedBlock:^{
            [weakSelf updateEmptyScreen];
        }];
        
        [self.delegateController sendRequest:request];
        
    } else {
        [self updateEmptyScreen];
    }
    
}

- (void)updateData:(NSArray *)result {
    
    if ([result count] > 0) {
        
        buttonSeeMore.hidden = NO;
        [_myZoneList removeAllObjects];
        
        for (NSDictionary *item in result) {
            TVUserSocialActionDoc *action = [[TVUserSocialActionDoc alloc] initWithDictionary:item];
            [_myZoneList addObject:action];
        }
        
        [self updateTableView];
        
    }
    else
    {
        [self updateEmptyScreen];
    }
    
}



- (void)updateTableView {
    
    tableViewActivity.hidden = NO;
    
    if ([_myZoneList count]) {
        
        [tableViewActivity reloadData];
        
    } else {
        [self updateEmptyScreen];
    }
    
}

- (void)showEmptyScreen {
    
    [self updateFriendActivity];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = [_myZoneList count];
    
    if (isPhone) {
        count = MIN(3, count);
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *friendsActivityIdentifier = @"MyZoneFriendsActivityCell";
    MyZoneFriendsActivityCell *friendsActivityCell = [tableView dequeueReusableCellWithIdentifier:friendsActivityIdentifier];
    if(friendsActivityCell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyZoneFriendsActivityCellPhone"
                                                     owner:self
                                                   options:nil];
        friendsActivityCell = [nib objectAtIndex:(isPhone ? 0 : 1)];
    }

    TVUserSocialActionDoc *action = [_myZoneList objectAtIndex:indexPath.row];
    
    
    TVActivitySubject *user = action.activitySubject;
    TVActivityObject *mediaItem = action.activityObject;
    
    friendsActivityCell.avatarImage.image = [UIImage imageNamed:@"friend_activity_no_avatar"];
    if (user.actorPicUrl) {
        [friendsActivityCell.avatarImage sd_setImageWithURL:[NSURL URLWithString:user.actorPicUrl] placeholderImage:[UIImage imageNamed:@"friend_activity_no_avatar"]];
        
    }
    friendsActivityCell.zoneViewController = _delegateController;
    
    friendsActivityCell.index = indexPath.row;
    [friendsActivityCell highlightUserName:user.actorTvinciUsername
                             andMovieTitle:mediaItem.assetName
                          forMessageString:[Utils stringFromSocialAction:action]];
    friendsActivityCell.timeLabel.text = [Utils getActionDurationTimeFromNow:action.createDate];
    
    friendsActivityCell.backgroundColor = [UIColor clearColor];
    
    return friendsActivityCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self watchActivityOfIndex:indexPath.row];
}

-(void) watchActivityOfIndex:(NSInteger) index
{
    TVUserSocialActionDoc *action = [self.myZoneList objectAtIndex:index];
    
    if (action.activityObject.assetType == TVAssetType_MEDIA)
    {
        NSString *mediaId = [Utils getStringFromInteger:action.activityObject.assetID];
        [self.delegateController openMediaItemWithMediaId:mediaId];
    }
    else
    {
        __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetEPGProgramsByIds:@[[NSString stringWithFormat:@"%d",action.activityObject.assetID]] programIdType:TVProgramIdTypeInternal pageSize:1 pageIndex:0 delegate:nil];
        
        [request setFailedBlock:^{
            NSLog(@"failed");
        }];
        
        [request setCompletionBlock:^{
            
            NSLog(@"%@",request);
            NSDictionary * dictionary = [[[request JSONResponse] lastObject] objectForKey:@"m_oProgram"];
            TVEPGProgram * program = [[TVEPGProgram alloc] initWithDictionary:dictionary];
            [self.delegateController playChannelWithEPGChannelID:program.EPGChannelID];
            
        }];
        
        [self.delegateController sendRequest:request];
        
        
    }
    
}

@end
