//
//  MediaItemTextView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaItemView.h"

@interface MediaItemTextView : MediaItemView {
    
}

@property (nonatomic, weak) IBOutlet UILabel *labelText;

@end
