//
//  MediaItemView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MediaItemView.h"
#import "UIImageView+WebCache.h"

@implementation MediaItemView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    UIView *borderView = [_imageViewThumb superview];
    borderView.layer.cornerRadius = 2;
    borderView.layer.masksToBounds = YES;
    
    _imageViewThumb.layer.cornerRadius = 2;
    _imageViewThumb.layer.masksToBounds = YES;
    
    [Utils setLabelFont:_labelTitle];
    [Utils setLabelFont:_labelLike];
    
    _labelTitle.hidden = YES;
    _labelLike.hidden = YES;
    _imgHeart.hidden = YES;
    
    titleMaxHeight = _labelTitle.frame.size.height;
    
    _imageViewThumb.image = [UIImage imageNamed:@"placeholder_16_9.png"];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRegnize:)];
    [self addGestureRecognizer:recognizer];
    
    _viewContinue.hidden = YES;
    [Utils setLabelFont:_labelContinue bold:YES];
    _labelContinue.text = [LS(@"media_item_label_continue") uppercaseString];
}

- (void)tapGestureRegnize:(UITapGestureRecognizer *)recognizer {

    if (_mediaItem && _delegateController) {
        
        [_delegateController openMediaItem:_mediaItem];
        
    } else if (_program && _delegateController) {
        
        [_delegateController showHUDBlockingUI:YES];
        
        [_delegateController requestForGetMediaInfoWithEPGChannelID:_program.EPGChannelID completion:^(NSDictionary *result) {
            
            [_delegateController hideHUD];
            
            if (result) {
                self.mediaItem = [[TVMediaItem alloc] initWithDictionary:result];
                if (_mediaItem) {
                    
                    [_delegateController openMediaItem:_mediaItem];
                    
                }
            }
            
        }];
        
    }
    
}

- (void)updateForPhoneHome {
    
    homePhone = YES;
    
    _labelTitle.font = [UIFont boldSystemFontOfSize:14.0];
    [Utils setLabelFont:_labelTitle bold:YES];
    _labelTitle.numberOfLines = 1;
    
    _labelLike.center = CGPointMake(_labelLike.center.x, _labelLike.center.y + 2);
    _imgHeart.center = CGPointMake(_imgHeart.center.x, _imgHeart.center.y + 2);
    
}

- (void)updateData:(TVMediaItem *)item {
    
    _mediaItem = item;
    [self.likeAdapter setupMediaItem:_mediaItem];

    [_imageViewThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:_mediaItem andSize:CGSizeMake(_imageViewThumb.frame.size.width * 2, _imageViewThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"]];
    
    if (homePhone) {
        _labelTitle.text = _mediaItem.name;
        
    } else {
        _labelTitle.text = [_mediaItem.name uppercaseString];
    
    }
    
    //if ([_mediaItem.name length] == 0) {
    //    _labelTitle.text = @"No Title";
    //}
    
    if (_labelLike) {
        
        _labelLike.text = [Utils getStringFromInteger:_mediaItem.likeCounter];
        
        int likeWidth = [_labelLike.text sizeWithAttributes:@{NSFontAttributeName:_labelLike.font}].width + 5;
        _labelLike.frame = CGRectMake(self.frame.size.width - likeWidth, _labelLike.frame.origin.y, likeWidth, _labelLike.frame.size.height);
        _imgHeart.frame = CGRectMake(_labelLike.frame.origin.x - _imgHeart.frame.size.width, _imgHeart.frame.origin.y, _imgHeart.frame.size.width, _imgHeart.frame.size.height);
        _labelTitle.frame = CGRectMake(_labelTitle.frame.origin.x, _labelLike.frame.origin.y, _imgHeart.frame.origin.x - _labelTitle.frame.origin.x - 5, titleMaxHeight);
        
        CGRect textRect = [_labelTitle.text boundingRectWithSize:_labelTitle.frame.size
                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:@{NSFontAttributeName:_labelTitle.font}
                                                         context:nil];
        
        _labelTitle.frame = CGRectMake(_labelTitle.frame.origin.x, _labelLike.frame.origin.y, _imgHeart.frame.origin.x - _labelTitle.frame.origin.x - 5, textRect.size.height);
        
        
        _labelTitle.hidden = NO;
        _labelLike.hidden = NO;
        _imgHeart.hidden = NO;
        
    }


}

- (void)setEpisodeNumber:(int)number {
    
    _labelTitle.text = [NSString stringWithFormat:LS(@"episode"), [@(number) stringValue]];
    _labelTitle.frame = CGRectMake(_labelTitle.frame.origin.x, _labelLike.frame.origin.y, _imgHeart.frame.origin.x - _labelTitle.frame.origin.x - 5, titleMaxHeight);
    
    CGRect textRect = [_labelTitle.text boundingRectWithSize:_labelTitle.frame.size
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:_labelTitle.font}
                                                     context:nil];
    
    _labelTitle.frame = CGRectMake(_labelTitle.frame.origin.x, _labelLike.frame.origin.y, _imgHeart.frame.origin.x - _labelTitle.frame.origin.x - 5, textRect.size.height);
    
}

- (void)updateEPGData:(TVEPGProgram *)item {
    
    _program = item;

    APSTVProgram *apstvProgram = nil;
    
    if ([item isKindOfClass:[APSTVProgram class]]) {
        
        apstvProgram = (APSTVProgram *)item;
        [self.likeAdapter setupProgram:apstvProgram];
    }
    
    NSURL *pictureURL = _program.pictureURL;
        
    [_imageViewThumb sd_setImageWithURL:pictureURL placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"]];
    
    if (homePhone) {
        _labelTitle.text = _program.name;
        
    } else {
        _labelTitle.text = [_program.name uppercaseString];
        
    }
    
    
    if (_labelLike) {
        
        if (apstvProgram) {
            _labelLike.text = [apstvProgram.likeConter stringValue];
            
        } else {
            _labelLike.text = [Utils getStringFromInteger:_program.likeConter];
            
        }
        
        int likeWidth = [_labelLike.text sizeWithAttributes:@{NSFontAttributeName:_labelLike.font}].width + 5;
        _labelLike.frame = CGRectMake(self.frame.size.width - likeWidth, _labelLike.frame.origin.y, likeWidth, _labelLike.frame.size.height);
        _imgHeart.frame = CGRectMake(_labelLike.frame.origin.x - _imgHeart.frame.size.width, _imgHeart.frame.origin.y, _imgHeart.frame.size.width, _imgHeart.frame.size.height);
        _labelTitle.frame = CGRectMake(_labelTitle.frame.origin.x, _labelLike.frame.origin.y, _imgHeart.frame.origin.x - _labelTitle.frame.origin.x - 5, titleMaxHeight);
        
        CGRect textRect = [_labelTitle.text boundingRectWithSize:_labelTitle.frame.size
                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:@{NSFontAttributeName:_labelTitle.font}
                                                         context:nil];
        
        _labelTitle.frame = CGRectMake(_labelTitle.frame.origin.x, _labelLike.frame.origin.y, _imgHeart.frame.origin.x - _labelTitle.frame.origin.x - 5, textRect.size.height);
        
        
        _labelTitle.hidden = NO;
        _labelLike.hidden = NO;
        _imgHeart.hidden = NO;

    }
    
}

- (void)updateForContinue {
    
    _isContinue = YES;
    _viewContinue.hidden = NO;
    
}

- (LikeAdapter*) likeAdapter {
    if (!_likeAdapter) {
        _likeAdapter = [[LikeAdapter alloc] initWithMediaItem:_mediaItem control:nil label:nil imgLike:nil labelCounter:_labelLike];
        _likeAdapter.needToGetAssetsStats = NO;
    }
    return _likeAdapter;
}

@end
