//
//  MediaItemViewVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MediaItemViewVF.h"

@implementation MediaItemViewVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void) awakeFromNib{
    [super awakeFromNib];
    
    self.labelTitle.textColor = [UIColor whiteColor];
    self.labelLike.textColor = [VFTypography instance].grayScale4Color;
}


@end
