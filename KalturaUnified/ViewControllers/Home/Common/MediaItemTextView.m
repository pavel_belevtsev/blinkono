//
//  MediaItemTextView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MediaItemTextView.h"
#import "UIImageView+WebCache.h"

@implementation MediaItemTextView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [Utils setLabelFont:super.labelTitle bold:YES];
    [Utils setLabelFont:_labelText];
    
    _labelText.hidden = YES;
    
    super.imageViewThumb.image = [UIImage imageNamed:@"placeholder_2_3.png"];
    
}

- (void)updateData:(TVMediaItem *)item {
    
    super.mediaItem = item;
    
    [super.imageViewThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:super.mediaItem andSize:CGSizeMake(super.imageViewThumb.frame.size.width * 2, super.imageViewThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_2_3.png"]];
    
    super.labelTitle.text = super.mediaItem.name;

    _labelText.text = super.mediaItem.mediaDescription;
    
    super.labelLike.text = [Utils getStringFromInteger:super.mediaItem.likeCounter];
    
    super.labelTitle.hidden = NO;
    _labelText.hidden = NO;
    super.labelLike.hidden = NO;
    super.imgHeart.hidden = NO;

}

@end
