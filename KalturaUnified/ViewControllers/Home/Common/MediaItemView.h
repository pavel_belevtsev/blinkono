//
//  MediaItemView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVMediaItem+PSTags.h"

@interface MediaItemView : UINibView {
    
    CGFloat titleMaxHeight;
    BOOL homePhone;
    
}

- (void)updateForPhoneHome;
- (void)updateData:(TVMediaItem *)item;
- (void)updateEPGData:(TVEPGProgram *)item;
- (void)updateForContinue;
- (void)setEpisodeNumber:(int)number;

@property (nonatomic, strong) TVMediaItem *mediaItem;
@property (nonatomic, strong) TVEPGProgram *program;

@property (nonatomic, weak) IBOutlet UIImageView *imageViewThumb;
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UIImageView *imgHeart;
@property (nonatomic, weak) IBOutlet UILabel *labelLike;
@property (nonatomic, weak) IBOutlet UIView *viewContinue;
@property (nonatomic, weak) IBOutlet UILabel *labelContinue;

@property (nonatomic, weak) BaseViewController *delegateController;
@property (nonatomic, strong) LikeAdapter *likeAdapter;

@property BOOL isContinue;

@end
