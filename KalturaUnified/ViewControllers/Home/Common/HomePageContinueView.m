//
//  HomePageContinueView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "HomePageContinueView.h"
#import "UIImage+Tint.h"

@implementation HomePageContinueView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [Utils setLabelFont:_labelTitle bold:YES];
    [Utils setLabelFont:_labelText];
    
    _labelTitle.text = LS(@"homepage_shows_user_watching");
    _labelText.text = LS(@"homepage_shows_user_paused");
    
    if (isPhone) {
    
        UIView *viewContent = [[self subviews] objectAtIndex:0];
        viewContent.layer.cornerRadius = 4;
        viewContent.layer.masksToBounds = YES;
        
        UIImageView *imgView = [[viewContent subviews] objectAtIndex:0];
        UIImage *image = imgView.image;
        imgView.image = [image imageTintedWithColor:[UIColor colorWithWhite:0.5 alpha:1.0]];
        
    }
    
}

@end
