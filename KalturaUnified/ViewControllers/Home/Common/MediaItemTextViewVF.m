//
//  MediaItemTextViewVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 2/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MediaItemTextViewVF.h"

@implementation MediaItemTextViewVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)updateData:(TVMediaItem *)item {
    [super updateData:item];
    
    // sizeToFit will align the text of the description to the top of the frame thus maintaining
    // a fixed distance from the title above it as requested.
    [self.labelText sizeToFit];
}
@end
