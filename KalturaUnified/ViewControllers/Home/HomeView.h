//
//  HomeView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "TVMediaItem+PSTags.h"

@interface HomeView : UINibView

@property (nonatomic, weak) HomeViewController *delegate;

- (void)reloadHomePages;
- (void)reloadHomePagesContent;
- (void)startTimers;
- (void)stopTimers;

@end
