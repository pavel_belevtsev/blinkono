//
//  HomeViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeView.h"
#import "CoverView.h"
#import "MenuViewController.h"
#import "UIImageView+WebCache.h"

@interface HomeViewController ()

@property (nonatomic, weak) IBOutlet UIButton *buttonMenuHome;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    
    self.loadNoInternetView = YES;

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
        self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    if (_labelTitle) [Utils setLabelFont:_labelTitle bold:YES];

    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(avatarPictureUpdate) name:KRenameNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(avatarPictureUpdate) name:TVSessionManagerLogoutCompletedNotification object:nil];

    _buttonAvatar.hidden = YES;
    
    self.buttonMenuHome.isAccessibilityElement = YES;
    self.buttonMenuHome.accessibilityLabel = @"Button Menu Home";
    
    self.buttonAvatar.isAccessibilityElement = YES;
    self.buttonAvatar.accessibilityLabel = @"Button Avatar Home";
    
}

- (void)dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (!isInitialized) {
        isInitialized = YES;
        
        if (isPhone) {
            
            CoverView *coverView = [[CoverView alloc] initWithNib];
            coverView.frame = super.viewContent.bounds;
            [super.viewContent insertSubview:coverView atIndex:0];
            
            coverView.delegate = self;
            
        } else if (isPad) {
            
            HomeView *homeView = [[HomeView alloc] initWithNib];
            homeView.frame = _viewHome.bounds;
            [_viewHome addSubview:homeView];
            homeView.delegate = self;
            
            [homeView reloadHomePages];
        }
    }
    
    [self updateMenu];
    
    super.panModeDefault = MFSideMenuPanModeDefault;
    [self menuContainerViewController].panMode = (MFSideMenuPanMode)super.panModeDefault;
    
}

-(void) viewDidAppear:(BOOL)animated{
    [self avatarPictureUpdate];
    _buttonAvatar.hidden = NO;

    if (isPad) {
        
        HomeView *homeView = [[_viewHome subviews] lastObject];
        [homeView startTimers];
        
    }
    
    [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) deepLinkingAction:nil];
}

- (void)viewWillDisappear:(BOOL)animated {

    if (isPad) {
        
        HomeView *homeView = [[_viewHome subviews] lastObject];
        [homeView stopTimers];
        
    }
}

- (void) avatarPictureUpdate {
    UIImage *image = [LoginManager getProfilePicture];
    self.buttonAvatar.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.buttonAvatar setImage:image forState:UIControlStateNormal];
    self.buttonAvatar.layer.masksToBounds = YES;
    self.buttonAvatar.layer.cornerRadius = self.buttonAvatar.frame.size.width / 2.f;
    self.buttonAvatar.layer.borderColor = [UIColor grayColor].CGColor;
    self.buttonAvatar.layer.borderWidth = 1.5f;
}

- (void)skinChanged {
    
    _imgLogo.image = nil;
    
    if (APST.logoUrl)
    {
        [_imgLogo sd_setImageWithURL:APST.logoUrl];
    }
    else
    {
        _imgLogo.image = [UIImage imageNamed:@"logo_login"];
    }
    
}

- (void)updateMenu {
    
    [self avatarPictureUpdate];
    if (isPhone) {
        
        CoverView *coverView = [[super.viewContent subviews] objectAtIndex:0];
        
        TVMenuItem *menuItem = [MenuM menuItem:TVPageLayoutCover];
        [coverView updateData:menuItem];
        
        _labelTitle.text = menuItem.name;
        [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) selectMenuItem:menuItem];
        
    } else if (isPad) {
        
        HomeView *homeView = [[_viewHome subviews] lastObject];
        if (_fullReload) {
            [homeView reloadHomePages];
        } else {
            [homeView reloadHomePagesContent];
        }
        
        [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) selectMenuItem:[MenuM menuItem:TVPageLayoutHome]];
        
    }
    
    _fullReload = NO;
    
    if (_imgLogo && !APST.showHomePageTitles)
    {
        [self skinChanged];
        
    }
    else
    {
        if (isPad)
        {
            TVMenuItem *menuItem = [MenuM menuItem:TVPageLayoutHome];
            TVMenuItem * first = [menuItem.children firstObject];
            if (first)
            {
                if (self.labelTitlePad == nil)
                {
                    self.labelTitlePad = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 565, 30)];
                    self.labelTitlePad.textAlignment = NSTextAlignmentCenter;
                    [Utils setLabelFont:self.labelTitlePad size:22 bold:YES];
                }
                self.labelTitlePad.center = self.viewNavigationBar.center;
                self.labelTitlePad.textColor = APST.brandTextColor;
                [self.viewNavigationBar addSubview:self.labelTitlePad];
                self.labelTitlePad.text = first.name;
                self.imgLogo.image = nil;
            }
        }
    }
}

- (IBAction)buttonMenuPressed:(UIButton *)button {

    [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{}];
    
}

- (IBAction)buttonAvatarPressed:(UIButton *)button {
    [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) pressMenuItem:[MenuM menuItem:TVPageLayoutMyZone] activityFeedLoadOnMyZoneStart:NO];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
