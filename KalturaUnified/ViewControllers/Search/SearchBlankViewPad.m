//
//  SearchBlankView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 18.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SearchBlankViewPad.h"

@implementation SearchBlankViewPad

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [Utils setLabelFont:_labelTitle bold:YES];
    [Utils setLabelFont:_labelDescription];
    
    _labelDescription.text = LS(@"Try to use other keywords or use our catalog to \nfind shows, movies and live programs.");
    CGPoint centerLabelTitle = _labelDescription.center;
    [_labelDescription sizeToFit];
    _labelDescription.center = centerLabelTitle;
}

-(void) setLabelTitleText :(NSString*) labelTitleText{
    
    _labelTitleText = labelTitleText;

    _labelTitle.text = _labelTitleText;
    CGPoint centerLabelTitle = _labelTitle.center;
    [_labelTitle setNeedsLayout];
    [_labelTitle sizeToFit];
    _labelTitle.center = centerLabelTitle;
    
    _labelDescription.frame = CGRectMake(_labelDescription.frame.origin.x, _labelTitle.frame.origin.y +  _labelTitle.frame.size.height+8, _labelDescription.frame.size.width, _labelDescription.frame.size.height);
}

@end
