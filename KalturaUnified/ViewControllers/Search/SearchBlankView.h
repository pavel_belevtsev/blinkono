//
//  SearchBlankView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 18.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchBlankView : UINibView {
    
    IBOutlet UIImageView *searchIcon;
    
    int initialTitleWidth;
    int initialDescriptionWidth;
    
}

@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UILabel *labelDescription;
@property (nonatomic, weak)  NSString *labelTitleText;
@property (nonatomic, weak)  NSString *labelDescriptionText;

@end
