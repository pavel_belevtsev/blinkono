//
//  SearchListView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 18.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchListView : UINibView <UITextFieldDelegate> {
    
    BOOL needUpdate;
    IBOutlet UIView *searchViewHeader;
    
}

- (void)initSearchList;
- (void)updateSearchList;
- (IBAction)buttonSearchPressed:(UIButton *)button;
- (void)clearValues;
- (void)updateSearchTable;

@property (nonatomic, weak) BaseViewController *delegate;
@property (nonatomic, strong) NSString *searchStr;
@property BOOL updateInProgress;
@property BOOL forEPG;
@property (nonatomic, strong) NSMutableArray *autocompleteSearch;
@property (nonatomic, weak) UITextField *textFieldSearch;

@property (weak, nonatomic) IBOutlet UITableView *tableViewSearch;
@property (nonatomic, weak)     IBOutlet UIButton *buttonSearch;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewSearchBackground;

@property (strong, nonatomic) UIImage *imageNotActiveBackground;

@end
