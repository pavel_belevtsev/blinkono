//
//  SearchTableViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 18.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *labelTitle;
@property (nonatomic, retain) IBOutlet UIView *viewSelection;
@property (nonatomic, retain) IBOutlet UILabel *labelMediaType;
@property (nonatomic, retain) IBOutlet UILabel *labelDescription;

@end
