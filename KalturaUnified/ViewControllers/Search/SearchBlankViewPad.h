//
//  SearchBlankView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 18.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchBlankViewPad : UINibView {
    
    IBOutlet UIImageView *searchIcon;
}

@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UILabel *labelDescription;
@property (nonatomic, weak)  NSString *labelTitleText;

@end
