//
//  SearchBlankView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 18.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SearchBlankView.h"

@implementation SearchBlankView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [Utils setLabelFont:_labelTitle bold:YES];
    [Utils setLabelFont:_labelDescription];
    
    initialTitleWidth = _labelTitle.frame.size.width;
    initialDescriptionWidth = _labelDescription.frame.size.width;
    
    _labelDescription.text = LS(@"search_page_search_no_results_sub_title1");
    CGPoint centerLabelTitle = _labelDescription.center;
    [_labelDescription sizeToFit];
    _labelDescription.center = centerLabelTitle;
}

-(void) setLabelDescriptionText :(NSString*) labelDescriptionText{
    
    _labelDescriptionText = labelDescriptionText;
    
    _labelDescription.text = _labelDescriptionText;
    CGPoint centerLabelDescription = _labelDescription.center;
    _labelDescription.frame = CGRectMake(0, 0, initialDescriptionWidth, 1000);
    [_labelDescription setNeedsLayout];
    [_labelDescription sizeToFit];
    _labelDescription.center = centerLabelDescription;
}

-(void) setLabelTitleText :(NSString*) labelTitleText{
    
    _labelTitleText = labelTitleText;

    _labelTitle.text = _labelTitleText;
    CGPoint centerLabelTitle = _labelTitle.center;
    _labelTitle.frame = CGRectMake(0, 0, initialTitleWidth, 1000);
    [_labelTitle setNeedsLayout];
    [_labelTitle sizeToFit];
    _labelTitle.center = centerLabelTitle;
    
    _labelDescription.frame = CGRectMake(_labelDescription.frame.origin.x, _labelTitle.frame.origin.y +  _labelTitle.frame.size.height+8, _labelDescription.frame.size.width, _labelDescription.frame.size.height);
}

@end
