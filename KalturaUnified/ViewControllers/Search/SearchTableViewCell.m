//
//  SearchTableViewCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 18.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SearchTableViewCell.h"

@implementation SearchTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {

    [super awakeFromNib];
    
    [Utils setLabelFont:self.labelTitle];
    self.viewSelection.backgroundColor = APST.brandColor;
    [Utils setLabelFont:self.labelMediaType];
    
}

- (void)dealloc {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
