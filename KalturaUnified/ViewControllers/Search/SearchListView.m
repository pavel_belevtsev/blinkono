//
//  SearchListView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 18.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SearchListView.h"
#import "SearchTableViewCell.h"

@implementation SearchListView

- (void)setFrame:(CGRect)frame {
    super.frame = frame;
    _tableViewSearch.frame = CGRectMake(0, 0, frame.size.width - 1, frame.size.height - 5);
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];

    _tableViewSearch.tableHeaderView = searchViewHeader;
    
    [Utils setButtonFont:_buttonSearch bold:YES];

    _buttonSearch.hidden = NO;

    [self updateSearchTable];
    
    //self.searchBlankView = [[SearchBlankView alloc] initWithNib];
    //self.searchBlankView.hidden = YES;
    //[self addSubview:self.searchBlankView];
    
    self.autocompleteSearch = [[NSMutableArray alloc] init];
    //self.clipsToBounds = YES;
    //_tableViewSearch.clipsToBounds = YES;
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
    
}


- (void)skinChanged {
    
    [_buttonSearch setTitleColor:APST.brandColor forState:UIControlStateNormal];
    [_buttonSearch setTitleColor:APST.brandDarkColor forState:UIControlStateHighlighted];

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];

    if ([touch.view isKindOfClass:[UIView class]]){

        [_delegate searchProcess:@""];

    }
}

- (void)dealloc {
    
 //   _searchBlankView = nil;
    _autocompleteSearch = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark - UITExtField

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField == self.textFieldSearch) {
        
        [_delegate searchProcess:textField.text];
        _imageViewSearchBackground.image = _imageNotActiveBackground;
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.textFieldSearch) {
        NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        _textFieldSearch.text = [self stringByTrimmingLeadingWhitespace:resultString];
        
        [self performSelector:@selector(updateSearchList) withObject:nil afterDelay:0.1];
        
        return NO;
    }
    
    return YES;
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    if (textField == self.textFieldSearch) {
        
        [self performSelector:@selector(updateSearchList) withObject:nil afterDelay:0.1];
        
    }
    
    return YES;
    
}

#pragma mark -

- (void)initSearchList {
    
    _tableViewSearch.hidden = YES;
    [self updateSearchList];
    
}
- (void)updateSearchList {
    
    if (!self.updateInProgress) {

        [self updateSearchListProcess];
        
    } else {
        
        needUpdate = YES;
        
    }

}

- (void)updateResults:(NSArray *)searchDictionaries {
    
    [_autocompleteSearch removeAllObjects];
    
    if (needUpdate) {
        needUpdate = NO;
        self.updateInProgress = NO;
        
        [self updateSearchListProcess];
        
    } else {
        
        for (NSString *str in searchDictionaries) {
            if ([str rangeOfString:_textFieldSearch.text options:NSCaseInsensitiveSearch].location == 0) {
                [_autocompleteSearch addObject:str];
            }
        }
        
        [self updateSearchTable];
        self.updateInProgress = NO;
        
    }
    
}

- (NSString *)stringByTrimmingLeadingWhitespace:(NSString *)searchString {
    NSInteger i = 0;
    
    while ((i < [searchString length])
           && [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[searchString characterAtIndex:i]]) {
        i++;
    }
    return [searchString substringFromIndex:i];
}

- (void)updateSearchListProcess {
    
    self.searchStr = _textFieldSearch.text;
    
    if ([_textFieldSearch.text length] < 2) {
        
     //   self.searchBlankView.hidden = YES;
        
        [self updateSearchTable];
        
    } else {
        
        self.updateInProgress = YES;
        
        __weak SearchListView *weakSelf = self;
        
        __weak TVPAPIRequest *request;
        
        if (_forEPG) {
            
            request = [TVPMediaAPI requestforGetEPGAutoComplete:_searchStr pageSize:maxPageSize pageIndex:0 delegate:nil];
            
        } else {
            
            NSArray *mediaTypes = @[[[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeSeries],
                                    [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeMovie],
                                    [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeEpisode]];
            
            request = [TVPMediaAPI requestForGetAutoCompleteSearchList:_searchStr fromMediaTypes:mediaTypes delegate:nil];
            
        }
        
        [request setCompletionBlock:^{
            
            NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
            
            [weakSelf updateResults:result];
            
        }];
        
        [request setFailedBlock:^{
            
            weakSelf.updateInProgress = NO;
            if (needUpdate) {
                needUpdate = NO;
                
                [weakSelf updateSearchListProcess];
                
            }
            
        }];
        
        [_delegate sendRequest:request];
        
    }
}

- (void)clearValues {

    self.searchStr = @"";
    
    [self updateSearchList];
    
}

- (void)updateSearchTable {
    
    
   // self.searchBlankView.hidden = YES;
    
    if ([self.searchStr length] >= 2) {
        
        
        NSString *searchText = self.searchStr;
        
        UIFont *font = _buttonSearch.titleLabel.font;
        
        NSString *sText = [NSString stringWithFormat:@"%@ \"%@\"", LS(@"search_page_search_for"), self.searchStr];
        
        float width = [sText sizeWithAttributes:@{NSFontAttributeName:font}].width;
        
        while (width > (_buttonSearch.frame.size.width - 60.0) && [searchText length] > 2) {
            
            searchText = [searchText substringToIndex:[searchText length] - 2];
            
            sText = [NSString stringWithFormat:@"%@ \"%@...\"", LS(@"search_page_search_for"), searchText];
            
            width = [sText sizeWithAttributes:@{NSFontAttributeName:font}].width;
        }

        [_buttonSearch setTitle:sText forState:UIControlStateNormal];
    } else {
        [_buttonSearch setTitle:@"" forState:UIControlStateNormal];
    }
    
    _tableViewSearch.hidden = NO;
    [_tableViewSearch reloadData];

    if ([self.searchStr length] >= 2) {
        _tableViewSearch.hidden = NO;
        [_tableViewSearch reloadData];
    } else {
        _tableViewSearch.hidden = YES;
    }
    
    
}

- (IBAction)buttonSearchPressed:(UIButton *)button {
    
    if ([button.titleLabel.text length] > 0) {
        [_delegate searchProcess:_textFieldSearch.text];
    }
    
}

#pragma mark - TableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [_autocompleteSearch count];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellId= @"SearchTableViewCell";
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SearchTableViewCell" owner:tableView options:nil] objectAtIndex:0];
    }
    
    NSString *itemStr = @"";
    
    if (indexPath.row < [_autocompleteSearch count]) {
        itemStr = [_autocompleteSearch objectAtIndex:indexPath.row];
    }
    
    cell.labelTitle.text = itemStr;
    cell.labelMediaType.text = @"";
    
    UIFont *font = cell.labelTitle.font;
    
    float width = [cell.labelTitle.text sizeWithAttributes:@{NSFontAttributeName:font}].width;
    if (width > tableView.frame.size.width - cell.labelTitle.frame.origin.x) {
        width = tableView.frame.size.width - cell.labelTitle.frame.origin.x;
    }
    
    cell.labelTitle.frame = CGRectMake(cell.labelTitle.frame.origin.x, (cell.frame.size.height - cell.labelTitle.frame.size.height) / 2, width, cell.labelTitle.frame.size.height);
    
    width = cell.labelTitle.frame.origin.x + width + 5.0;
    
    if (width < tableView.frame.size.width) {
        cell.labelMediaType.hidden = NO;
        cell.labelMediaType.frame = CGRectMake(width, cell.labelTitle.frame.origin.y, tableView.frame.size.width - width, cell.labelTitle.frame.size.height);
    } else {
        cell.labelMediaType.hidden = YES;
    }
    
    cell.labelDescription.text = @"";
    
    cell.viewSelection.hidden = YES;
    
    NSString *str = cell.labelTitle.text;
    NSInteger location = [str rangeOfString:self.searchStr options:NSCaseInsensitiveSearch].location;
    if (location != NSNotFound) {
        
        NSString *strOffset = [str substringToIndex:location];
        
        float offset = [strOffset sizeWithAttributes:@{NSFontAttributeName:font}].width;
        
        if (offset < tableView.frame.size.width + cell.labelTitle.frame.origin.x) {
            
            NSString *strSelection = [str substringWithRange:NSMakeRange(location, [self.searchStr length])];
            
            cell.viewSelection.hidden = NO;
            cell.viewSelection.frame = CGRectMake(cell.labelTitle.frame.origin.x + offset, cell.labelTitle.frame.origin.y, [strSelection sizeWithAttributes:@{NSFontAttributeName:font}].width, cell.labelTitle.frame.size.height);
            
        }
        
    }

    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [UIView new];
    cell.selectedBackgroundView = [UIView new];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < [_autocompleteSearch count]) {
        
        NSString *itemStr = [_autocompleteSearch objectAtIndex:indexPath.row];
        _textFieldSearch.text = itemStr;
        [_delegate searchProcess:itemStr];
        
    }
    
}

@end
