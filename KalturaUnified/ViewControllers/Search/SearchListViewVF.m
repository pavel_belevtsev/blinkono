//
//  SearchListViewVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/14/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SearchListViewVF.h"

@implementation SearchListViewVF


/* "PSC : VOdafone Grup : change color : JIRA ticket. */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self.buttonSearch setTitleColor:[VFTypography instance].grayScale6Color forState:UIControlStateNormal];
    [self.buttonSearch setTitleColor:[VFTypography instance].grayScale6Color forState:UIControlStateHighlighted];
}
@end
