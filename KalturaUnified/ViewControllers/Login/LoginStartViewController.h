//
//  LoginStartViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 07.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVUnderlineButton.h"
#import "LoginBaseViewController.h"

@interface LoginStartViewController : LoginBaseViewController

@property (weak, nonatomic) IBOutlet UIButton *buttonInfo;
@property (weak, nonatomic) IBOutlet UIButton *buttonSkip;
@property (weak, nonatomic) IBOutlet UIButton *buttonLoginFacebook;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogin;
@property (weak, nonatomic) IBOutlet UIView *viewCreateAccount;
@property (weak, nonatomic) IBOutlet UIButton *buttonRemoveDevice;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (nonatomic, assign) BOOL isNotRoot;
@property (weak, nonatomic) NSString *segue;
@property (weak, nonatomic) TVFacebookUser *fbUser;

@property (weak, nonatomic) IBOutlet UIView *centerContainer;
@property (nonatomic, strong) TVUnderlineButton *buttonCreateAccount;

-(void)updateLoginLogoImage;
@end
