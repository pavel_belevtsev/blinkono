//
//  LoginAccountViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 07.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginAccountViewController.h"
#import "LoginInfoViewController.h"
#import "LoginForgotViewController.h"
#import "AnalyticsManager.h"

#import "UIImageView+WebCache.h"

#define username_min_length @"3";
@interface LoginAccountViewController ()

@end

@implementation LoginAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = [NSString stringWithFormat:LS(@"login_brand"), APST.brandName];
    
    _labelEmail.text = LS(@"username");
    _labelPasword.text = LS(@"password");
    
    [_buttonLogin setTitle:[NSString stringWithFormat:LS(@"login_brand"), APST.brandName] forState:UIControlStateNormal];
    [self initBrandButtonUI:_buttonLogin];
    
    TVUnderlineButton *buttonForgot = [self createUnderlineButtonUI:_viewForgotButton font:Font_Bold(_buttonLogin.titleLabel.font.pointSize - 1)];
    [buttonForgot setTitle:LS(@"screen_title_forgot_password") forState:UIControlStateNormal];
    [buttonForgot addTarget:self action:@selector(buttonForgotPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [Utils setLabelFont:_labelEmail bold:YES];
    [Utils setLabelFont:_labelPasword bold:YES];
    
    [Utils setTextFieldFont:_textFieldEmail bold:YES];
    [Utils setTextFieldFont:_textFieldPasword bold:YES];
    
    [_buttonCheck setTitle:LS(@"create_account_check") forState:UIControlStateNormal];
    
    if (_labelRememberMe) {
        [Utils setLabelFont:_labelRememberMe bold:YES];
        _labelRememberMe.text = LS(@"login_remember_me");
        [self initSwitchUI:_switchRememberMe];
        
        _switchRememberMe.on = LoginM.didRememberMePressed;
        
        int rememberWidth = [_labelRememberMe.text sizeWithAttributes:@{NSFontAttributeName:_labelRememberMe.font}].width + 40.0;
        int rememberX = ([_labelRememberMe superview].frame.size.width - (rememberWidth + _switchRememberMe.frame.size.width)) / 2;
        
        _labelRememberMe.frame = CGRectMake(rememberX, _labelRememberMe.frame.origin.y, rememberWidth, _labelRememberMe.frame.size.height);
        _switchRememberMe.frame = CGRectMake(rememberX + rememberWidth, _switchRememberMe.frame.origin.y, _switchRememberMe.frame.size.width, _switchRememberMe.frame.size.height);
        
    }
    
    [self updateLogo];
    
    self.textFieldEmail.isAccessibilityElement = YES;
    self.textFieldEmail.accessibilityLabel = @"Text Field Login";
    
    self.textFieldPasword.isAccessibilityElement = YES;
    self.textFieldPasword.accessibilityLabel = @"Text Field Password";
    
    self.buttonLogin.isAccessibilityElement = YES;
    self.buttonLogin.accessibilityLabel = @"Button Login";
    
}

-(void)updateLogo
{
    if (APST.logoUrl)
    {
        [_imgLogo sd_setImageWithURL:APST.logoUrl];
    }
    else
    {
        _imgLogo.image = [UIImage imageNamed:@"logo_login"];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self registerForSignInNotifications];
    [self registerForDeviceStateNotifications];
    [AnalyticsM trackScreen:@"Login with account"];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self unregisterFromSignInNotifications];
    [self unregisterFromDeviceStateNotifications];
    
}

#pragma mark -
#pragma mark TextField

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger textLength = [textField.text length] - range.length + [string length];
    
    UILabel *label = (textField.tag == 0) ? _labelEmail : _labelPasword;
    
    label.hidden = (textLength > 0);
    
    return YES;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    if (textField == _textFieldEmail) {
        [_textFieldPasword becomeFirstResponder];
    }
    
    return YES;
    
}

- (void)closeKeyboard {
    
    [_textFieldEmail resignFirstResponder];
    [_textFieldPasword resignFirstResponder];
    
}


#pragma mark - Buttons Actions

- (IBAction)buttonRememberMePressed {
    
    LoginM.didRememberMePressed = _switchRememberMe.on;
}


- (IBAction)buttonLoginPressed:(UIButton *)button {
    
    [self closeKeyboard];
    
    if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    LoginM.facebookUser = nil;
    
    @try
    {
        if (_textFieldEmail.text.length > 0)
        {
            if (_textFieldPasword.text.length > 0)
            {
                
                [self showHUDBlockingUI:YES];
                
                LoginM.emailLogin = _textFieldEmail.text;
                LoginM.passwordLogin = _textFieldPasword.text;
                [LoginM signIn:_textFieldEmail.text password:_textFieldPasword.text];
                
                if (![AnalyticsM.currentEvent.category length]) {
                    [AnalyticsM.currentEvent clearFields];
                    AnalyticsM.currentEvent.category = @"Login and registration";
                    AnalyticsM.currentEvent.action = @"Sign in";
                    AnalyticsM.currentEvent.label = @"Login screen";
                }
                
                
            }
            else
            {
                NSString* userNameMinLength = [NSString stringWithFormat:@"%u", APST.minPasswordLength];
                [NSException raise:NSInvalidArgumentException format:LS(@"edit_text_amount_error"), userNameMinLength];
            }
        }
        else
        {
            [NSException raise:NSInvalidArgumentException format:LS(@"login_user_invalid_username_error"),nil];
        }
    }
    @catch (NSException *exception)
    {
        [self presentAlertWithTitle:@"" message:exception.description];
    }

}

- (void)buttonForgotPressed:(UIButton *)button {
    
    [self closeKeyboard];
    LoginForgotViewController *controller = [[ViewControllersFactory defaultFactory] loginForgotViewController];
    [self.navigationController pushViewController:controller animated:YES];

}

- (IBAction)buttonCheckPressed:(UIButton *)button {
    
    BOOL wasFirstResponder;
    if ((wasFirstResponder = [_textFieldPasword isFirstResponder])) {
        [_textFieldPasword resignFirstResponder];
    }
    
    [_textFieldPasword setSecureTextEntry:![_textFieldPasword isSecureTextEntry]];
    if (wasFirstResponder) {
        [_textFieldPasword becomeFirstResponder];
        NSString *str = _textFieldPasword.text;
        _textFieldPasword.text = [NSString stringWithFormat:@"%@ ", _textFieldPasword.text];
        _textFieldPasword.text = str;
    }
    
    [button setTitle:([_textFieldPasword isSecureTextEntry] ? LS(@"create_account_check") : LS(@"create_account_hide")) forState:UIControlStateNormal];
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
