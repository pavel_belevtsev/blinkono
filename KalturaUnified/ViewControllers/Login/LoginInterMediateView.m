  //
//  LoginInterMediateView.m
//  KalturaUnified
//
//  Created by Dmytro Rozumeyenko on 1/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "LoginInterMediateView.h"
#import "AnalyticsManager.h"

@implementation LoginInterMediateView

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    if (self) {

        
    }
    return self;
}

- (void)awakeFromNib {
    

}

- (IBAction)itemSelected:(UIButton *)button {
    
    if (button.tag == 1) {
        [AnalyticsM.currentEvent clearFields];
        AnalyticsM.currentEvent.category = @"Login and registration";
        AnalyticsM.currentEvent.action = @"Facebook User Sign in";
        AnalyticsM.currentEvent.label = @"My Zone";
        [self.delegateController intermediateLogin:@"fbLogin"];
        
    } else if (button.tag == 2) {
        [AnalyticsM.currentEvent clearFields];
        AnalyticsM.currentEvent.category = @"Login and registration";
        AnalyticsM.currentEvent.action = @"Sign in";
        AnalyticsM.currentEvent.label = @"My Zone";
        [self.delegateController intermediateLogin:@"regularLogin"];
    }
    
}


-(void) setDelegateController:(id)delegateController {
    
    _delegateController = delegateController;
    
    TVUnderlineButton *buttonCreate = [_delegateController createUnderlineButtonUI:_viewButtonCreate font:_buttonFacebook.titleLabel.font];
    if (APST.multiUser) {
        [buttonCreate setTitle:LS(@"guest") forState:UIControlStateNormal];
        buttonCreate.hidden = !APST.showGuestUser;
        [buttonCreate setTouchUpInsideAction:^(UIButton *sender) {
            [self.delegateController intermediateLogin:@"buttonJoinVCPressed"];
        }];
        
    } else {
        [buttonCreate setTitle:LS(@"create_a_free_account") forState:UIControlStateNormal];
        [buttonCreate setTouchUpInsideAction:^(UIButton *sender) {
            [self.delegateController intermediateLogin:@"buttonCreateAccountPressed"];
        }];
    }
}


@end
