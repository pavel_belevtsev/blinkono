//
//  LoginBaseViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 07.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

#import "UserManagement.h"
#import "DeviceManagement.h"
#import "SocialManagement.h"
#import "LoginNavigationController.h"

@interface LoginBaseViewController : BaseViewController

- (void)registerForDeviceStateNotifications;
- (void)unregisterFromDeviceStateNotifications;
- (void)registerForSignInNotifications;
- (void)unregisterFromSignInNotifications;
- (void)addOrActivateDeviceToDomainProcess;
- (void)hideMergeScreen:(BOOL)cancelActionp;
- (void)updateFacebookUser:(TVFacebookUser *)user;

@property TVDeviceState deviceState;

@property (nonatomic, strong) UserManagement *userManager;
@property (nonatomic, strong) DeviceManagement *deviceManager;
@property (nonatomic, strong) SocialManagement *socialManager;
@property (nonatomic) NSInteger selectedMultiUser;

@property (weak, nonatomic) IBOutlet UIButton *btnRemveDevice;

@end
