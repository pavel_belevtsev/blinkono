//
//  LoginInterMediateView.h
//  KalturaUnified
//
//  Created by Dmytro Rozumeyenko on 1/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginInterMediateView : UINibView


@property (nonatomic, weak) IBOutlet UIView *viewLoginContent;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewLogin;


@property (nonatomic, weak) IBOutlet UILabel *labelLogin;
@property (nonatomic, weak) IBOutlet UIButton *buttonFacebook;
@property (nonatomic, weak) IBOutlet UIButton *buttonLogin;

@property (nonatomic, weak) IBOutlet UIView *viewButtonCreate;
@property (nonatomic, weak) id delegateController;

@end
