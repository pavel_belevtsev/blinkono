//
//  TVNLoginMergeView.h
//  Tvinci
//
//  Created by Pavel Belevtsev on 04.03.14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVNCustomLoginTextField.h"

@interface TVNLoginMergeView : UIView <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UIView *subViewMerge;
@property (nonatomic, strong) IBOutlet UILabel *labelMergeTitle;
@property (nonatomic, strong) IBOutlet UILabel *labelMergeSubTitle;
@property (nonatomic, strong) IBOutlet UIImageView *imgMergeAvatar;
@property (nonatomic, strong) IBOutlet UILabel *labelNameFacebook;
@property (nonatomic, strong) IBOutlet UILabel *labelNameTvinci;

@property (nonatomic, strong) IBOutlet UILabel *labelMergeText;

@property (nonatomic, strong) IBOutlet TVNCustomLoginTextField *txtPassword;
@property (nonatomic, strong) IBOutlet UIButton *btnMerge;
@property (nonatomic, strong) IBOutlet UIButton *btnCancel;

@property (nonatomic, strong) IBOutlet UIButton *btnCheckPassword;
@property BOOL didCheckPasswordPressed;
@property BOOL ignoreKeyboardAnimating;

@end
