//
//  TVNLoginMergeView.m
//  Tvinci
//
//  Created by Pavel Belevtsev on 04.03.14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import "TVNLoginMergeView.h"

@implementation TVNLoginMergeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {

    self.subViewMerge.layer.cornerRadius = 3;
    self.subViewMerge.layer.masksToBounds = YES;
    
    self.labelMergeTitle.font = Font_Bold(self.labelMergeTitle.font.pointSize);
    self.labelMergeTitle.text = NSLocalizedString(@"Merge;DialogTitle", nil);
    
    self.labelMergeSubTitle.font = Font_Reg(self.labelMergeSubTitle.font.pointSize);
    self.labelMergeSubTitle.text = NSLocalizedString(@"Merge;DialogSubTitle", nil);
    
    self.labelMergeText.font = Font_Reg(self.labelMergeText.font.pointSize);
    self.labelMergeText.text = NSLocalizedString(@"Merge;DialogText", nil);
    
    self.labelNameFacebook.font = Font_Bold(self.labelNameFacebook.font.pointSize);
    self.labelNameTvinci.font = Font_Bold(self.labelNameTvinci.font.pointSize);
    
    UIImage *defaultButtonImage = [UIImage imageNamed:@"default_button.png"];
	UIImage *sizeableDefaultButtonImage = [defaultButtonImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
	[self.btnCancel setBackgroundImage:sizeableDefaultButtonImage forState:UIControlStateNormal];
	[self.btnCancel setTitleColor:UIColorWhite forState:UIControlStateNormal];
	self.btnCancel.titleLabel.font = Font_Bold(18);
    self.btnCancel.titleEdgeInsets = UIEdgeInsetsMake(2, 5, 5, 8);
    [self.btnCancel.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.btnCancel setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];

    [self.btnMerge setTitle:NSLocalizedString(@"Merge;DialogTitle", nil) forState:UIControlStateNormal];
    
    self.txtPassword.placeholder = NSLocalizedString(@"Password", nil);
    
    [self.btnCheckPassword setTitle:NSLocalizedString(@"Check", nil) forState:UIControlStateNormal];
	
    [self.btnCheckPassword setBackgroundImage:sizeableDefaultButtonImage forState:UIControlStateNormal];
	[self.btnCheckPassword setTitleColor:UIColorWhite forState:UIControlStateNormal];
	self.btnCheckPassword.titleLabel.font =  Font_Bold(12);
    self.btnCheckPassword.titleEdgeInsets = UIEdgeInsetsMake(2, 5, 5, 8);
    [self.btnCheckPassword.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    self.didCheckPasswordPressed = NO;
    self.ignoreKeyboardAnimating = NO;
    
}

- (IBAction)btnCheckPasswordPressed {
    self.ignoreKeyboardAnimating = YES;
    NSString *checkPassBtnTitle = self.didCheckPasswordPressed ? NSLocalizedString(@"Check", nil) : NSLocalizedString(@"Hide", nil);
    [self.btnCheckPassword setTitle:checkPassBtnTitle forState:UIControlStateNormal];
    self.didCheckPasswordPressed = !self.didCheckPasswordPressed;
    
    //Show keyboard only if we need
    BOOL wasFirstResponder = NO;
    if ((wasFirstResponder = [self.txtPassword isFirstResponder])) {
        [self.txtPassword resignFirstResponder];
    }
    
    [self.txtPassword setSecureTextEntry:!self.didCheckPasswordPressed];
    if (wasFirstResponder) {
        [self.txtPassword becomeFirstResponder];
    }
    self.ignoreKeyboardAnimating = NO;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [UIView animateWithDuration:0.3 animations:^{
    
        self.subViewMerge.center = CGPointMake(self.subViewMerge.center.x, (self.frame.size.height / 2) - 200.0);
        
    }];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
 
    [textField resignFirstResponder];
    
    return YES;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (!self.ignoreKeyboardAnimating) {
        [UIView animateWithDuration:0.3 animations:^{
            
            self.subViewMerge.center = CGPointMake(self.subViewMerge.center.x, (self.frame.size.height / 2));
            
        }];
    }
    
}

@end
