//
//  LoginUserView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginUserView : UIView

@property (nonatomic, strong) IBOutlet UIImageView *imgAvatar;
@property (nonatomic, strong) IBOutlet UILabel *labelName;

@end
