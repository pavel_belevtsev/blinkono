//
//  LoginAssociateViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 26.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginBaseViewController.h"

@interface LoginAssociateViewController : LoginBaseViewController

@property BOOL deviceMode;

@property (nonatomic, weak) IBOutlet UILabel *lblAddDomainTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblAddDomainTitleUser;
@property (nonatomic, weak) IBOutlet UILabel *lblAddDomainUser;
@property (nonatomic, weak) IBOutlet UITextField *txtPasswordLoginAddDomain;
@property (nonatomic, weak) IBOutlet UIButton *btnAddDomainCheckPassword;
@property (nonatomic, weak) IBOutlet UIButton *btnLoginAddDomain;
@property (nonatomic, weak) IBOutlet UILabel *lblAddDomainOr;
@property (nonatomic, weak) IBOutlet UILabel *lblAddDomainSend;
@property (nonatomic, weak) IBOutlet UILabel *lblAddDomainEmail;
@property (nonatomic, weak) IBOutlet UIButton *btnLoginAddDomainEmail;

@end
