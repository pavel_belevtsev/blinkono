//
//  LoginRegisterViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 08.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginRegisterViewController.h"
#import "LoginAssociateViewController.h"

@interface LoginRegisterViewController ()

@end

@implementation LoginRegisterViewController

-(void)locateFildesToMultiUserOption
{
    _imageViewBg.image = [UIImage imageNamed:@"login_register5_bg"];
    
    CGFloat dY = 50.0;
    _labelUsername.hidden = NO;
    _textFieldUsername.hidden = NO;
    
    _labelEmail.frame = CGRectMake(_labelEmail.frame.origin.x, _labelEmail.frame.origin.y + dY, _labelEmail.frame.size.width, _labelEmail.frame.size.height);
    _textFieldEmail.frame = CGRectMake(_textFieldEmail.frame.origin.x, _textFieldEmail.frame.origin.y + dY, _textFieldEmail.frame.size.width, _textFieldEmail.frame.size.height);
    _labelPasword.frame = CGRectMake(_labelPasword.frame.origin.x, _labelPasword.frame.origin.y + dY, _labelPasword.frame.size.width, _labelPasword.frame.size.height);
    _textFieldPasword.frame = CGRectMake(_textFieldPasword.frame.origin.x, _textFieldPasword.frame.origin.y + dY, _textFieldPasword.frame.size.width, _textFieldPasword.frame.size.height);
    _buttonCheck.frame = CGRectMake(_buttonCheck.frame.origin.x, _buttonCheck.frame.origin.y + dY, _buttonCheck.frame.size.width, _buttonCheck.frame.size.height);
    
    _buttonLogin.frame = CGRectMake(_buttonLogin.frame.origin.x, _buttonLogin.frame.origin.y + dY, _buttonLogin.frame.size.width, _buttonLogin.frame.size.height);
    
    _labelNoteText.frame = CGRectMake(_labelNoteText.frame.origin.x, _labelNoteText.frame.origin.y + dY, _labelNoteText.frame.size.width, _labelNoteText.frame.size.height);
    _viewTermsButton.frame = CGRectMake(_viewTermsButton.frame.origin.x, _viewTermsButton.frame.origin.y + dY, _viewTermsButton.frame.size.width, _viewTermsButton.frame.size.height);
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = LS(@"screen_title_registration");
    
    if (APST.multiUser) {
        
        [self locateFildesToMultiUserOption];
    }

    /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
    [_buttonLoginFacebook setHidden:!APST.fbSupport];
    [_labelOr setHidden:!APST.fbSupport];
    
    [_buttonLoginFacebook setTitle:LS(@"login_facebook") forState:UIControlStateNormal];
    [self initFacebookButtonUI:_buttonLoginFacebook];
    
    [_buttonLogin setTitle:LS(@"create_new_account") forState:UIControlStateNormal];
    [self initBrandButtonUI:_buttonLogin];
    
    _labelNoteText.text = LS(@"create_account_processing_agree");
    [Utils setLabelFont:_labelNoteText];
    
    TVUnderlineButton *buttonTerms = [self createUnderlineButtonUI:_viewTermsButton font:_labelNoteText.font];
    [Utils setButtonFont:buttonTerms];
    [buttonTerms setTitle:LS(@"screen_title_terms_of_service") forState:UIControlStateNormal];
    [buttonTerms addTarget:self action:@selector(buttonTermsPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    _labelOr.text = LS(@"create_account_or");
    [Utils setLabelFont:_labelOr bold:YES];
    
    _labelFirst.text = LS(@"create_account_first_name_hint");
    _labelLast.text = LS(@"create_account_last_name_hint");
    _labelUsername.text = LS(@"create_account_username_hint");
    _labelEmail.text = LS(@"create_account_email_hint");
    _labelPasword.text = LS(@"create_account_password_hint");
    
    [Utils setLabelFont:_labelFirst bold:YES];
    [Utils setLabelFont:_labelLast bold:YES];
    [Utils setLabelFont:_labelUsername bold:YES];
    [Utils setLabelFont:_labelEmail bold:YES];
    [Utils setLabelFont:_labelPasword bold:YES];
    
    [Utils setTextFieldFont:_textFieldFirst bold:YES];
    [Utils setTextFieldFont:_textFieldLast bold:YES];
    [Utils setTextFieldFont:_textFieldUsername bold:YES];
    [Utils setTextFieldFont:_textFieldEmail bold:YES];
    [Utils setTextFieldFont:_textFieldPasword bold:YES];
    
    [_buttonCheck setTitle:LS(@"create_account_check") forState:UIControlStateNormal];
    
    viewFormOriginY = _viewForm.frame.origin.y;
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self registerForSignInNotifications];
    [self registerForDeviceStateNotifications];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self unregisterFromSignInNotifications];
    [self unregisterFromDeviceStateNotifications];
    
}


- (void)closeKeyboard {
    
    [_textFieldFirst resignFirstResponder];
    [_textFieldLast resignFirstResponder];
    [_textFieldUsername resignFirstResponder];
    [_textFieldEmail resignFirstResponder];
    [_textFieldPasword resignFirstResponder];
    
}

#pragma mark - Buttons

- (void)buttonTermsPressed:(UIButton *)button {
    
    [self closeKeyboard];
    
    [[UIApplication sharedApplication] openURL:APST.urlTermsOfService];
    
}

- (IBAction)buttonCheckPressed:(UIButton *)button {
    
    BOOL wasFirstResponder;
    if ((wasFirstResponder = [_textFieldPasword isFirstResponder])) {
        [_textFieldPasword resignFirstResponder];
    }
    
    [_textFieldPasword setSecureTextEntry:![_textFieldPasword isSecureTextEntry]];
    if (wasFirstResponder) {
        [_textFieldPasword becomeFirstResponder];
        NSString *str = _textFieldPasword.text;
        _textFieldPasword.text = [NSString stringWithFormat:@"%@ ", _textFieldPasword.text];
        _textFieldPasword.text = str;
    }
    
    [button setTitle:([_textFieldPasword isSecureTextEntry] ? LS(@"create_account_check") : LS(@"create_account_hide")) forState:UIControlStateNormal];
    
}

#pragma mark -
#pragma mark TextField

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger textLength = [textField.text length] - range.length + [string length];
    
    UILabel *label;
    
    if (textField == _textFieldFirst) {
        
        label = _labelFirst;
        
    } else if (textField == _textFieldLast) {
        
        label = _labelLast;
        
    } else if (textField == _textFieldUsername) {
        
        label = _labelUsername;
        
    } else if (textField == _textFieldEmail) {
        
        label = _labelEmail;
        
    } else if (textField == _textFieldPasword) {
        
        label = _labelPasword;

    }
    
    label.hidden = (textLength > 0);
    
    return YES;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    if (textField == _textFieldFirst) {
        [_textFieldLast becomeFirstResponder];
    } else if (textField == _textFieldLast) {
        if (APST.multiUser) {
            [_textFieldUsername becomeFirstResponder];
        } else {
            [_textFieldEmail becomeFirstResponder];
        }
    } else if (textField == _textFieldUsername) {
        [_textFieldEmail becomeFirstResponder];
    } else if (textField == _textFieldEmail) {
        [_textFieldPasword becomeFirstResponder];
    }
    
    return YES;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    int y = 0;
    
    if (textField.tag < 2) {
        
        y = (textField.tag + 1 ) * 50.0;
        
        if (APST.multiUser && (textField != _textFieldUsername)) {
            y += 50;
        }
    }
    
    self.selectedTextField = textField;
    
    [UIView animateWithDuration:0.2 animations:^{
        _viewForm.frame = CGRectMake(_viewForm.frame.origin.x, viewFormOriginY - y, _viewForm.frame.size.width, _viewForm.frame.size.height);
    }];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    self.selectedTextField = nil;
    
    [self performSelector:@selector(checkFormOffset) withObject:nil afterDelay:0.1];
    
    
}

- (void)checkFormOffset {
    
    if (!self.selectedTextField) {
        
        [UIView animateWithDuration:0.2 animations:^{
            _viewForm.frame = CGRectMake(_viewForm.frame.origin.x, viewFormOriginY, _viewForm.frame.size.width, _viewForm.frame.size.height);
        }];
        
    }
    
}

#pragma mark - Login

-(BOOL)isUserNameValid
{
    BOOL usernameIsValid = YES;
    BOOL usernameValidInput = [Utils NSStringIsOnlyDigitsAndChars:_textFieldUsername.text];
    BOOL usernameValidLength = _textFieldUsername.text.length >= 6 && _textFieldUsername.text.length <= 16;
    
    if (usernameValidInput && usernameValidLength) {
        
        usernameIsValid = YES;
        
    } else {
        
        usernameIsValid = NO;
        
    }
    return usernameIsValid;
}

- (IBAction)buttonCreatepressed:(UIButton *)button {

    [self closeKeyboard];
    
    if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    LoginM.facebookUser = nil;
    
    BOOL useUsername = NO;
    
    if (APST.multiUser) {
        
        useUsername = YES;
        
    }
    
    BOOL emailIsValid = [Utils NSStringIsValidEmail:_textFieldEmail.text];
    BOOL usernameIsValid = YES;
    
    if (APST.multiUser)
    {
        usernameIsValid = [self isUserNameValid];
    }
    
    @try {

        NSArray *names = [NSArray arrayWithObjects:_textFieldFirst.text, _textFieldLast.text, nil];

        if (![_textFieldFirst.text isEqualToString:@""] && ![_textFieldLast.text isEqualToString:@""]) {
            
            if (_textFieldEmail.text.length > 0 && usernameIsValid) {
                
                if (emailIsValid) {
                    
                    if (_textFieldPasword.text.length > 0) {
                        
                        if ([Utils NSStringIsOnlyDigitsAndChars:_textFieldPasword.text] && _textFieldPasword.text.length >= 6 && _textFieldPasword.text.length <= 16) {
                            
                            [self showHUDBlockingUI:YES];
                            
                            __block TVUser *newUser = [[TVUser alloc] init];
                            newUser.email = _textFieldEmail.text;
                            NSString *password = _textFieldPasword.text;
                            newUser.firstName = [names firstObject];
                            newUser.lastName = [names lastObject];
                            
                            if (useUsername) {
                                newUser.username = _textFieldUsername.text;
                            } else {
                                newUser.username = newUser.email;
                            }
                            
                            newUser.isAllowNewsLetter = YES;
                            
                            
                            if (APST.multiUser) {
                                
                                [self.userManager requestForGetUserDetailsByUsername:newUser.username completionBlock:^(id jsonResponse) {
                                    
                                    [self hideHUD];
                                    
                                    if (jsonResponse) {
                                        
                                        if ([[jsonResponse objectForKey:@"m_RespStatus"] intValue] == k_signUp_UserNotExists) {
                                            
                                            LoginM.userRegister = newUser;
                                            LoginM.userRegisterPassword = password;
                                            
                                            LoginAssociateViewController *controller = [[ViewControllersFactory defaultFactory] loginAssociateViewController];
                                            [self.navigationController pushViewController:controller animated:YES];
                                            
                                        } else {
                                            
                                            [self presentAlertWithTitle:LS(@"alert_general_error") message:LS(@"login_user_already_exists")];
                                        }
                                        
                                    } else {
                                        
                                        [self presentAlertWithTitle:LS(@"alert_general_error") message:LS(@"login_user_generic_error")];
                                        
                                    }
                                    
                        
                                }];
                                
                            } else {
                                
                                
                                [super.userManager requestForSignUpWithNewUser:newUser password:password completionBlock:^(id result) {
                                    
                                    [self hideHUD];
                                    
                                    if (result) {
                                        
                                        if ([result isKindOfClass:[NSDictionary class]]) {
                                            
                                            int responceStatus = [[result objectForKey:@"m_RespStatus"] intValue];
                                            if (responceStatus == k_signUp_OK || responceStatus == 21) {
                                                
                                                NSLog(@"Registration succedded - now to add domain");////////
                                                newUser.siteGUID = [[result objectForKey:@"m_user"] objectForKey:@"m_sSiteGUID"];
                                                
                                                [self addDomainInfoAsPartOfRegistrationWithNewUser:newUser];
                                                
                                            } else if([[result objectForKey:@"m_RespStatus"] intValue] == k_signUp_UserExists) {
                                                
                                                [self presentAlertWithTitle:@"" message:LS(@"login_user_already_exists")];
                                                
                                            } else if([[result objectForKey:@"m_RespStatus"] intValue] == k_signUp_UserNotActivated) {
                                                
                                                [self presentAlertWithTitle:@"" message:LS(@"general_Error_Registration_UserNotActivated")];
                                                
                                            } else {
                                                
                                                [self presentAlertWithTitle:@"" message:LS(@"login_user_generic_error")];
                                                
                                            }
                                        }
                                        
                                    }
                                    
                                }];
                                
                            }
                            
                        } else {
                            [_textFieldPasword becomeFirstResponder];
                            [NSException raise:NSInvalidArgumentException format:LS(@"create_account_new_password_error_length"), nil];
                        }
                    } else {
                        [_textFieldPasword becomeFirstResponder];
                        [NSException raise:NSInvalidArgumentException format:LS(@"create_account_new_password_error_empty"), nil];
                    }
                } else {
                    [_textFieldEmail becomeFirstResponder];
                    [NSException raise:NSInvalidArgumentException format:LS(@"create_account_email_error_not_valid"), nil];
                    
                }
            } else {
                
                if (_textFieldEmail.text.length > 0 && useUsername) {
                    [_textFieldUsername becomeFirstResponder];
                    [NSException raise:NSInvalidArgumentException format:LS(@"create_account_new_username_error_length"), nil];
                } else {
                    [_textFieldEmail becomeFirstResponder];
                    [NSException raise:NSInvalidArgumentException format:LS(@"create_account_email_error_empty"), nil];
                }
                
                
            }
        } else {
            if ([_textFieldFirst.text isEqualToString:@""]) {
                [_textFieldFirst becomeFirstResponder];
                [NSException raise:NSInvalidArgumentException format:LS(@"edit_text_first_name_error"), nil];
            } else {
                [_textFieldLast resignFirstResponder];
                [NSException raise:NSInvalidArgumentException format: LS(@"edit_text_last_name_error"), nil];
            }
        }
    } @catch (NSException *exception) {
        [self presentAlertWithException:exception];
    }

}

- (IBAction)buttonFaceboookPressed:(UIButton *)button {
    
    if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    [self showHUDBlockingUI:YES];
    
    self.selectedMultiUser = -1;
    [[super.socialManager socialProviderById:SocialProviderTypeFacebook] aquirePermissions:^(BOOL success) {
        if (success){
            [FBM requestForGetFBUserDataWithToken:^(TVFacebookUser *user) {
                [self updateFacebookUser:user];
            }];
        }
        else
            [self hideHUD];
    }];
    
    
}

-(void) addDomainInfoAsPartOfRegistrationWithNewUser:(TVUser *)newUser{

    [self showHUDBlockingUI:YES];
    
    NSString *domainName = [NSString stringWithFormat:@"%@%@",newUser.firstName,newUser.lastName];
    
    __weak TVPAPIRequest *request = [TVPDomainAPI requestForAddDomainWithDomainName:domainName domainDescription:domainName masterGuid:[newUser.siteGUID intValue] delegate:nil];
    
    [request setCompletionBlock:^{

        NSDictionary *result = [request JSONResponse];
        
        if([[[result objectForKey:@"m_oDomain"] objectForKey:@"m_DomainStatus"] intValue] == 0 ||
           [[[result objectForKey:@"m_oDomain"] objectForKey:@"m_DomainStatus"] intValue] == 1)
        {
            NSLog(@"Registration succeeded");
            
            [self login];
            
        }
        else
        {
             NSLog(@"Registration Failed");
             [self hideHUD];
            [self presentAlertWithTitle:@"" message:LS(@"login_user_generic_error")];
        }
        
    }];
    
    
    [request setFailedBlock:^{
        
        NSLog(@"Registration Failed");
        [self hideHUD];
        [self presentAlertWithTitle:@"" message:LS(@"login_user_generic_error")];
        
    }];
    
    [self sendRequest:request];
    

}

- (void)login {
    
    [self closeKeyboard];
    
    @try
    {
        if (_textFieldEmail.text.length > 0)
        {
            if (_textFieldPasword.text.length > 0)
            {
                [_textFieldEmail resignFirstResponder];
                [_textFieldPasword resignFirstResponder];
                
                [self showHUDBlockingUI:YES];
                
                [LoginM signIn:_textFieldEmail.text password:_textFieldPasword.text];
                
            }
            else
            {
                [NSException raise:NSInvalidArgumentException format:LS(@"edit_text_amount_error"), nil];
            }
        }
        else
        {
            [NSException raise:NSInvalidArgumentException format:LS(@"edit_text_email_error"), nil];
        }
    }
    @catch (NSException *exception)
    {
        [self presentAlertWithTitle:@"" message:exception.description andDelegate:nil];
    }
    
}

#pragma mark - 

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
