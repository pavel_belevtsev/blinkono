//
//  LoginStartViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 07.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginStartViewController.h"
#import "LoginAccountViewController.h"
#import "LoginInfoViewController.h"
#import "LoginRegisterViewController.h"
#import <TvinciSDK/OpenUDID.h>
#import "UIImageView+WebCache.h"
#import "LoginMultiViewController.h"
#import "AnalyticsManager.h"
#import "UIAlertView+Blocks.h"

@interface LoginStartViewController ()

@end

@implementation LoginStartViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [Utils setButtonFont:_buttonSkip bold:YES];
    [_buttonSkip setTitle:LS(@"login_skip_now") forState:UIControlStateNormal];
    
    [_buttonLogin setTitle:[NSString stringWithFormat:LS(@"login_brand"), APST.brandName] forState:UIControlStateNormal];
    [self initBrandButtonUI:_buttonLogin];
    
    [_buttonLoginFacebook setTitle:LS(@"login_facebook") forState:UIControlStateNormal];
    [self initFacebookButtonUI:_buttonLoginFacebook];
    
    /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
    if (!APST.fbSupport) {
        [_buttonLogin setFrame:_buttonLoginFacebook.frame];
        [_buttonLoginFacebook setHidden:YES];
    }
    
    self.buttonCreateAccount = [self createUnderlineButtonUI:_viewCreateAccount font:Font_Bold(_buttonLogin.titleLabel.font.pointSize - 1)];
    
    if (APST.multiUser) {
        [_buttonCreateAccount setTitle:LS(@"guest") forState:UIControlStateNormal];
        _buttonCreateAccount.hidden = !APST.showGuestUser;
        [_buttonCreateAccount addTarget:self action:@selector(buttonJoinVCPressed:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        [_buttonCreateAccount setTitle:LS(@"create_a_free_account") forState:UIControlStateNormal];
        [_buttonCreateAccount addTarget:self action:@selector(buttonCreateAccountPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
   
    self.buttonSkip.isAccessibilityElement = YES;
    self.buttonSkip.accessibilityLabel = @"Button Skip Start";
    
    self.buttonLogin.isAccessibilityElement = YES;
    self.buttonLogin.accessibilityLabel = @"Button Login Start";

}

- (void)viewWillAppear:(BOOL)animated {

    self.centerContainer.alpha = 1.0;
    _buttonInfo.alpha = 1.0;
    _buttonSkip.alpha = 1.0;
    
    if (isPad && APST.multiUser && [LoginM.domainUsers count] && ![_segue isEqualToString:@"fbLogin"]) {
        
        LoginMultiViewController *controller = [[ViewControllersFactory defaultFactory] loginMultiViewController];
            
        //[self.navigationController pushViewController:controller animated:NO];
        [self.navigationController setViewControllers:[NSArray arrayWithObject:controller] animated:NO];

        return;
        
    }
    if (isPhone && APST.multiUser && [LoginM.domainUsers count]) {
        _buttonCreateAccount.hidden = NO;
        [_buttonCreateAccount removeTarget:self action:@selector(buttonJoinVCPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_buttonCreateAccount setTitle:LS(@"multiuser_existing_device_addnewuser") forState:UIControlStateNormal];
        [_buttonCreateAccount addTarget:self action:@selector(buttonCreateAccountPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_buttonCreateAccount setNeedsDisplay];
        [_buttonCreateAccount setNeedsLayout];
        
    }
    
    _buttonSkip.alpha = (APST.multiUser && ([LoginM.domainUsers count] > 0)) ? 0.0 : 1.0;
    [self initBrandButtonUI:_buttonLogin];

    [self registerForSignInNotifications];
    [self registerForDeviceStateNotifications];
    
    if (!_isNotRoot){
        
        [super viewWillAppear:animated];
        
        [self updateLoginLogoImage];
        
        [AnalyticsM trackScreen:@"Login main"];

    } else {
        
        self.centerContainer.alpha = 0.0;
        _buttonInfo.alpha = 0.0;
        _buttonSkip.alpha = 0.0;
    }
    
    /* "PSC : ONO : OPF-1911 VFGTVONES-579 - iOS/Urgent modifications for OTG */
    _buttonInfo.hidden=NO;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deepLinkingAction:) name:KDeepLinkingActionNotification object:nil];
    
}

- (void)deepLinkingAction:(NSNotification *)notification {
    
    if (DeepLinkingM.deepLinkingAction) {
        
        if (_buttonSkip.alpha == 1.0) {
            
            [self buttonSkipPressed:_buttonSkip];
            
        }
        
    }
}

-(void)updateLoginLogoImage
{
    if (APST.logoUrl)
    {
        [_imgLogo sd_setImageWithURL:APST.logoUrl];
    }
    else
    {
        _imgLogo.image = [UIImage imageNamed:@"logo_login"];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (_isNotRoot){
        if ([_segue isEqualToString:@"regularLogin"]){
            
            [_buttonLogin sendActionsForControlEvents:UIControlEventTouchUpInside];
            
        } else if ([_segue isEqualToString:@"fbLogin"]){
            
            if (self.fbUser) {
                [self updateFacebookUser:self.fbUser];
            }
            else {
                [_buttonLoginFacebook sendActionsForControlEvents:UIControlEventTouchUpInside];
            }
            
        } else if ([_segue isEqualToString:@"buttonJoinVCPressed"]){
            
            [self buttonJoinVCPressed:nil];
            
        } else if ([_segue isEqualToString:@"buttonCreateAccountPressed"]){
            
            [self buttonCreateAccountPressed:nil];
        }
        
        if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
            [alertView show];
            [[NSNotificationCenter defaultCenter] postNotificationName:KAuthenticationCompletedNotification object:nil];
            return;
        }
        
    }
    
    [self deepLinkingAction:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self unregisterFromSignInNotifications];
    [self unregisterFromDeviceStateNotifications];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KDeepLinkingActionNotification object:nil];
    
}

- (BOOL)showViewNavigation {
    
    return ([self.title isEqualToString:LS(@"login_to_purchase")] || [self.title isEqualToString:LS(@"login_to_watch1")]);
}

#pragma mark - Buttons Actions

-(IBAction) buttinLoginPressed:(UIButton* )button{
    if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    LoginAccountViewController *controller = [[ViewControllersFactory defaultFactory] loginAccountViewController];
    [self.navigationController pushViewController:controller animated:YES];
}


- (void)buttonJoinVCPressed:(UIButton *)button {

    if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    LoginInfoViewController *controller = [[ViewControllersFactory defaultFactory] loginInfoViewController];
    controller.infoTitle = LS(@"guest");
    controller.infoUrl = APST.joinUrl;
    [self.navigationController pushViewController:controller animated:YES];

}

- (IBAction)buttonFaceboookPressed:(UIButton *)button {
    
    if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    [self showHUDBlockingUI:YES];
    
    self.selectedMultiUser = -1;
    [[super.socialManager socialProviderById:SocialProviderTypeFacebook] aquirePermissions:^(BOOL success) {
        if (success){
            [FBM requestForGetFBUserDataWithToken:^(TVFacebookUser *user) {
                [self updateFacebookUser:user];
            }];
        }
        else if (!success) {
            [self hideHUD];
            [[NSNotificationCenter defaultCenter] postNotificationName:KAuthenticationCompletedNotification object:nil];
        }
        
        if (![AnalyticsM.currentEvent.category length]) {
            [AnalyticsM.currentEvent clearFields];
            AnalyticsM.currentEvent.category = @"Login and registration";
            AnalyticsM.currentEvent.action = @"Facebook User Sign in";
            AnalyticsM.currentEvent.label = @"Login screen";
        }
    }];
}

- (void)buttonCreateAccountPressed:(UIButton *)button {
    
    if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    LoginRegisterViewController *controller = [[ViewControllersFactory defaultFactory] loginRegisterViewController];
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (IBAction)buttonSkipPressed:(UIButton *)button {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:KAuthenticationCompletedNotification object:nil];
    
    [AnalyticsM.currentEvent clearFields];
    AnalyticsM.currentEvent.category = @"Login and registration";
    AnalyticsM.currentEvent.action = @"Skip clicked";
    [AnalyticsM trackCurrentEvent];

}

- (IBAction)buttonRemoveDevicePressed:(UIButton *)button {
    
    [LoginM fullLogout];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DomainID"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    __weak TVPAPIRequest * request = [TVPDomainAPI requestForGetDeviceDomains:nil];
    
    [request setFailedBlock:^{
        NSLog(@"GetDeviceDomain failed");
    }];
    
    [request setCompletionBlock:^{
        
        NSInteger deviceBrand = kDeviceBrand_iPhone;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            deviceBrand = kDeviceBrand_iPad;
        }
        else
        {
            deviceBrand = kDeviceBrand_iPhone;
            
        }
        
        NSArray * domains = [request JSONResponse];
        
        for (NSDictionary * dict in domains) {
            
            NSNumber * domainID =  [dict objectForKey:@"DomainID"];
            __weak TVPAPIRequest * removeRequest = [TVPDomainAPI requestForRemoveDeviceFromDomainWithDeviceName:@""  deviceBrandID:deviceBrand domainID:domainID.intValue delegate:nil];
            
            [removeRequest setCompletionBlock:^{
                
                NSLog(@"removing device completed");
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Removing device completed" message:nil delegate:nil cancelButtonTitle:@"Quit App" otherButtonTitles:nil];
                [alert showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    exit(0);
    
                }];
                
                
            }];
            
            
            [self sendRequest:removeRequest];
        }
        
        
    }];
    
    [self sendRequest:request];
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
