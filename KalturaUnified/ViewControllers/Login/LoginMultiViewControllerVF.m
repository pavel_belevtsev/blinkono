//
//  LoginMultiViewControllerVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/6/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "LoginMultiViewControllerVF.h"

@interface LoginMultiViewControllerVF ()

@end

@implementation LoginMultiViewControllerVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [[VFTypography instance] modifyBrandButtonToGray:self.btnLoginMulti];
    [[VFTypography instance] modifyBrandButtonToGray:self.btnCreateNewAccountMulti];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
