//
//  LoginRegisterViewControllerVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/6/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "LoginRegisterViewControllerVF.h"

@interface LoginRegisterViewControllerVF ()

@end

@implementation LoginRegisterViewControllerVF

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [[VFTypography instance] modifyBrandButtonToGray:self.buttonLogin];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// "PSC : ONO : Ono will have email and username as the same field :  OPF-215
-(void)locateFildesToMultiUserOption
{
    //self.imageViewBg.image = [UIImage imageNamed:@"loginself.register5self.bg"];
    
    CGFloat dY = 0.0;
    self.labelUsername.hidden = YES;
    self.textFieldUsername.hidden = YES;
    
    self.labelEmail.frame = CGRectMake(self.labelEmail.frame.origin.x, self.labelEmail.frame.origin.y + dY, self.labelEmail.frame.size.width, self.labelEmail.frame.size.height);
    self.textFieldEmail.frame = CGRectMake(self.textFieldEmail.frame.origin.x, self.textFieldEmail.frame.origin.y + dY, self.textFieldEmail.frame.size.width, self.textFieldEmail.frame.size.height);
    self.labelPasword.frame = CGRectMake(self.labelPasword.frame.origin.x, self.labelPasword.frame.origin.y + dY, self.labelPasword.frame.size.width, self.labelPasword.frame.size.height);
    self.textFieldPasword.frame = CGRectMake(self.textFieldPasword.frame.origin.x, self.textFieldPasword.frame.origin.y + dY, self.textFieldPasword.frame.size.width, self.textFieldPasword.frame.size.height);
    self.buttonCheck.frame = CGRectMake(self.buttonCheck.frame.origin.x, self.buttonCheck.frame.origin.y + dY, self.buttonCheck.frame.size.width, self.buttonCheck.frame.size.height);
    
    self.buttonLogin.frame = CGRectMake(self.buttonLogin.frame.origin.x, self.buttonLogin.frame.origin.y + dY, self.buttonLogin.frame.size.width, self.buttonLogin.frame.size.height);
    
    self.labelNoteText.frame = CGRectMake(self.labelNoteText.frame.origin.x, self.labelNoteText.frame.origin.y + dY, self.labelNoteText.frame.size.width, self.labelNoteText.frame.size.height);
    self.viewTermsButton.frame = CGRectMake(self.viewTermsButton.frame.origin.x, self.viewTermsButton.frame.origin.y + dY, self.viewTermsButton.frame.size.width, self.viewTermsButton.frame.size.height);
}

-(BOOL)isUserNameValid
{
    BOOL emailIsValid = [Utils NSStringIsValidEmail:self.textFieldEmail.text];
    BOOL usernameIsValid = YES;
    
    if (emailIsValid)
    {
        self.textFieldUsername.text = self.textFieldEmail.text;
        usernameIsValid = YES;
    }
    else
    {
        self.textFieldUsername.text = @"";
        usernameIsValid = NO;
    }
    return usernameIsValid;
}

@end
