//
//  LoginAccountViewControllerVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/6/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "LoginAccountViewControllerVF.h"

@interface LoginAccountViewControllerVF ()

@end

@implementation LoginAccountViewControllerVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[VFTypography instance] modifyBrandButtonToGray:self.buttonLogin];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
