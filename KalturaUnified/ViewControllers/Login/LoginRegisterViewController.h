//
//  LoginRegisterViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 08.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginBaseViewController.h"

@interface LoginRegisterViewController : LoginBaseViewController {
    
    int viewFormOriginY;
    
}

@property (weak, nonatomic) IBOutlet UIButton *buttonLoginFacebook;

@property (weak, nonatomic) IBOutlet UIView *viewForm;

@property (weak, nonatomic) IBOutlet UIButton *buttonLogin;
@property (weak, nonatomic) IBOutlet UIView *viewTermsButton;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewBg;

@property (weak, nonatomic) IBOutlet UITextField *textFieldFirst;
@property (weak, nonatomic) IBOutlet UITextField *textFieldLast;
@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPasword;

@property (weak, nonatomic) UITextField *selectedTextField;

@property (weak, nonatomic) IBOutlet UILabel *labelFirst;
@property (weak, nonatomic) IBOutlet UILabel *labelLast;
@property (weak, nonatomic) IBOutlet UILabel *labelUsername;
@property (weak, nonatomic) IBOutlet UILabel *labelEmail;
@property (weak, nonatomic) IBOutlet UILabel *labelPasword;

@property (weak, nonatomic) IBOutlet UILabel *labelNoteText;
@property (weak, nonatomic) IBOutlet UILabel *labelOr;

@property (weak, nonatomic) IBOutlet UIButton *buttonCheck;

-(BOOL)isUserNameValid;
@end
