//
//  LoginBaseViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 07.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginBaseViewController.h"
#import "UIAlertView+Blocks.h"
#import "LoginAssociateViewController.h"
#import "LoginMergeView.h"
#import "LoginViewController.h"
#import "AnalyticsManager.h"

@interface LoginBaseViewController ()

@end

@implementation LoginBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.userManager = [UserManagement new];
    self.deviceManager = [DeviceManagement new];
    self.socialManager = [SocialManagement new];
    _socialManager.delegateForProvider = self;
    
    self.selectedMultiUser = -1;
#ifdef DEBUG // fix 
    
    self.btnRemveDevice.hidden = YES;
    
#else
    
    self.btnRemveDevice.hidden = YES;
    
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Notifications

- (void)registerForDeviceStateNotifications {
    
    // Remove old registrations
    [self unregisterFromDeviceStateNotifications];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVSessionManager *manager = [TVSessionManager sharedTVSessionManager];
    
    [center addObserver:self selector:@selector(deviceStateAvailable:)
                   name:TVSessionManagerDeviceStateAvailableNotification
                 object:manager];
    
    [center addObserver:self selector:@selector(deviceRegistrationCompleted:)
                   name:TVSessionManagerDeviceRegistrationCompletedNotification
                 object:manager];
    
    [center addObserver:self selector:@selector(deviceRegistrationFailed:)
                   name:TVSessionManagerDeviceRegistrationFailedNotification
                 object:manager];
    [center addObserver:self selector:@selector(deviceRegistrationFailedWithKnownError:)
                   name:TVSessionManagerDeviceRegistrationFailedWithKnownErrorNotification
                 object:manager];
    
    [center addObserver:self selector:@selector(deviceActivationCompleted:)
                   name:TVSessionManagerDeviceActivationCompletedNotification
                 object:manager];
    
    [center addObserver:self selector:@selector(deviceActivationFailed:)
                   name:TVSessionManagerDeviceActivationFailedNotification
                 object:manager];
}

- (void)unregisterFromDeviceStateNotifications {
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVSessionManager *manager = [TVSessionManager sharedTVSessionManager];
    
    [center removeObserver:self name:TVSessionManagerDeviceStateAvailableNotification object:manager];
    [center removeObserver:self name:TVSessionManagerDeviceActivationCompletedNotification object:manager];
    [center removeObserver:self name:TVSessionManagerDeviceActivationFailedNotification object:manager];
    [center removeObserver:self name:TVSessionManagerDeviceRegistrationCompletedNotification object:manager];
    [center removeObserver:self name:TVSessionManagerDeviceRegistrationFailedNotification object:manager];
    [center removeObserver:self name:TVSessionManagerDeviceRegistrationFailedWithKnownErrorNotification object:manager];
}

- (void)registerForSignInNotifications {
    
    [self unregisterFromSignInNotifications];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVSessionManager *mngr = [TVSessionManager sharedTVSessionManager];
    [center addObserver:self
               selector:@selector(signInFailed:)
                   name:TVSessionManagerSignInFailedNotification
                 object:mngr];
    
    [center addObserver:self
               selector:@selector(signInCompleted:)
                   name:TVSessionManagerSignInCompletedNotification
                 object:mngr];

    
    [center addObserver:self
               selector:@selector(updateUserDetails:)
                   name:TVSessionManagerUserDetailsAvailableNotification
                 object:mngr];
    
}


- (void)unregisterFromSignInNotifications {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVSessionManager *mngr = [TVSessionManager sharedTVSessionManager];
    [center removeObserver:self name:TVSessionManagerSignInCompletedNotification object:mngr];
    [center removeObserver:self name:TVSessionManagerSignInFailedNotification object:mngr];
    [center removeObserver:self name:TVSessionManagerUserDetailsAvailableNotification object:mngr];

}

#pragma mark - SignIn

- (void)signInCompleted:(NSNotification *) notification {
    
    NSLog(@"signInCompleted");
    NSNumber *state = @([TVSessionManager sharedTVSessionManager].authenticationState);
    AnalyticsM.currentEvent.value = state;
    [AnalyticsM trackCurrentEvent];
   // [[NSNotificationCenter defaultCenter] postNotificationName:KRenameNotification object:nil];
}

- (void)updateUserDetails:(NSNotification *) notification {
    NSLog(@"updateUserDetails");

    [[NSNotificationCenter defaultCenter] postNotificationName:KRenameNotification object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:KMenuItemsUpdatedNotification object:nil];
}

- (void)signInFailed:(NSNotification *) notification {
    
    NSNumber *state = @([TVSessionManager sharedTVSessionManager].authenticationState);
    AnalyticsM.currentEvent.value = state;
    [AnalyticsM trackCurrentEvent];
    [self hideHUD];
    
    NSError *error = [notification.userInfo objectForKey:TVErrorKey];
    
    [self signInFailedWithError:error];

    [TVSessionManager sharedTVSessionManager].showedSignInFailedError = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];
    
}

- (void)signInFailedWithError:(NSError *)error {

    if (error && (error.code == 6)){
        [[SocialManagement socialProviderClassById:SocialProviderTypeFacebook] logout];
    }
    
    UIAlertView *alertView = nil;
    if (error.code == TVLoginStatusUserSuspended) {
          alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"User_suspended_alert_content") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
    }
    else
    {
        alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"login_wrong_username_or_password") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        
    }
    
    
    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];
    }];
}

#pragma mark - User State

- (void)checkCurrentUserState {
    
    if ([TVSessionManager sharedTVSessionManager].currentUser) {
        
        LoginM.userAddToDomain = [TVSessionManager sharedTVSessionManager].currentUser;
        LoginM.userAddToDomainSiteGUID = [TVSessionManager sharedTVSessionManager].currentUser.siteGUID;
        
    }
    
    [_userManager requestForGetDomainMasterUsers:^(id jsonResponse) {
        
        BOOL isDomainMaster = NO;
        
        if (LoginM.domainMasterUser) {
            
            if ([LoginM.domainMasterUser.siteGUID intValue] == [LoginM.userAddToDomainSiteGUID intValue]) {
                
                isDomainMaster = YES;
                
            }
            
        }
        
        [self hideHUD];
        
        if (isDomainMaster || (LoginM.DomainRestriction == 0)) {

            NSString *text = LS(@"loginAddDevice_RegisterDeviceToHousehold");
            NSString *title = LS(@"register_device_msg");
            NSInteger deviceCount = LoginM.nDeviceLimit - LoginM.nCurrentDevices - 1;
            
            if (deviceCount < 0)
            {
                text = [NSString stringWithFormat:LS(@"loginAddDevice_NoDeviceAvailable"),LoginM.nDeviceLimit];
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                                    message:text delegate:nil cancelButtonTitle:LS(@"info_btn_close") otherButtonTitles:nil];
                
                [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    [LoginM fullLogout];
                }];

                
            } else {
                
                if ([text rangeOfString:@"%d"].location != NSNotFound) {
                    text = [NSString stringWithFormat:text, deviceCount];
                }
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                                    message:text delegate:nil cancelButtonTitle:LS(@"cancel") otherButtonTitles:LS(@"confirm"), nil];
                
                [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    if (buttonIndex == 1) {
                        
                        [self showHUDBlockingUI:YES];
                        [self addOrActivateDeviceToDomainProcess];
                        
                    } else {
                        
                        [LoginM fullLogout];
                    }
                    
                }];

            }

        } else {
            
            [self addDeviceToDomainProcess];
            
        }
        
    } failedBlock:^(NSString *localizedError) {
        
        [self hideHUD];
        [LoginM fullLogout];
    }];
    
}

#pragma mark - Facebook User

- (void)updateFacebookUser:(TVFacebookUser *)user {
    
    LoginM.facebookUser = user;
    
    if (LoginM.facebookUser) {
        
        if (self.selectedMultiUser >= 0 && self.selectedMultiUser < [LoginM.domainUsers count]) {
            
            TVUser *user = [LoginM.domainUsers objectAtIndex:self.selectedMultiUser];
            
            NSString *facebookUserId = @"";
            
            if (LoginM.facebookUser) {
                facebookUserId = [LoginM.facebookUser.facebookUser objectForKey:@"id"];
            }
            
            if (facebookUserId && [facebookUserId length] > 0 && ![user.facebookID isEqualToString:facebookUserId]) {
                
                [self facebookGetUserFailed];
                
                self.selectedMultiUser = -1;
                
                return;
            }
        }
        
        TVFacebookStatus status = LoginM.facebookUser.status;
        
        if (status == TVFacebookStatusOK) {
            if ([LoginM isSignedIn]) {
                
                [self hideHUD];
                
                UIAlertView *faceBookUserOccupiedAlert = [[UIAlertView alloc] initWithTitle:LS(@"user_merge_existingFBemail_title")
                                                                                    message:LS(@"trying_to_merge_existing_user")
                                                                                   delegate:self cancelButtonTitle:LS(@"ok")
                                                                          otherButtonTitles:nil];
                
                [faceBookUserOccupiedAlert showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    [[SocialManagement socialProviderClassById:SocialProviderTypeFacebook] logout];
                    [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];
                }];
                
                
            } else {
                [LoginM signInSecure];

            }
            
        } else if (status == TVFacebookStatusMERGE) {
            [self showMergeScreen];
            
        } else if(status == TVFacebookStatusNEWUSER || status == TVFacebookStatusNOTEXIST) {
            
            if ([LoginM isSignedIn]) {
                [self showMergeScreen];
            } else {
                
                if (APST.ipno) {
                    
                    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"DomainID"]) {
                        
                        [self facebookGetUserFailed];
                        return;
                        
                    }
                    
                    [self hideHUD];
                    
                    LoginM.userRegister = nil;
                    LoginM.userRegisterPassword = nil;
                    LoginM.userAddToDomainSiteGUID = LoginM.facebookUser.siteGuid;
                    
                    LoginAssociateViewController *controller = [[ViewControllersFactory defaultFactory] loginAssociateViewController];
                    
                    [self.navigationController pushViewController:controller animated:YES];
                    
                } else {
                    
                    [self facebookRegisterUser];
                }
            }
        } else {
            [self facebookGetUserFailed];
        }
    }
    else {
        [self hideHUD];
        [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];
    }
}

- (void)facebookRegisterUser {
    
    NSString *facebookToken = [[FBSession.activeSession accessTokenData] accessToken];
    
    [FBM requestForFBUserRegisterWithToken:facebookToken createNewDomain:!APST.multiUser completionBlock:^(id jsonResponse) {
        
        if (jsonResponse) {
            
            LoginM.facebookUser = [[TVFacebookUser alloc] initWithDictionary:[jsonResponse dictionaryByRemovingNSNulls]];
            
            TVFacebookStatus status = LoginM.facebookUser.status;
            
            if(status == TVFacebookStatusOK) {
                [LoginM signInSecure];

            } else if (status == TVFacebookStatusNEWUSER){
                
                if (APST.multiUser) {
                    
                    [self hideHUD];
                    
                    LoginM.userAddToDomain = nil;
                    LoginM.userAddToDomainSiteGUID = LoginM.facebookUser.siteGuid;
                    
                    LoginAssociateViewController *controller = [[ViewControllersFactory defaultFactory] loginAssociateViewController];
                    [self.navigationController pushViewController:controller animated:YES];
                    
                } else {
                    [LoginM signInSecure];

                }
                
            } else {
                [self facebookGetUserFailed];
            }
        } else {
            [self facebookGetUserFailed];
        }
        
    }];
}

- (void)facebookGetUserFailed {
    
    [[FBSession activeSession] closeAndClearTokenInformation];
    
    [self hideHUD];

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"login_UnableRegisterUser") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
    
    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];
    }];
    
}

- (void)showMergeScreen {
    
    [self hideHUD];
    
    LoginNavigationController *navigation = (LoginNavigationController *)(self.navigationController);
    
    LoginMergeView *loginMergeView = [[LoginMergeView alloc] initWithNib];
    loginMergeView.frame = navigation.loginViewController.viewContent.bounds;
    loginMergeView.delegate = self;
    loginMergeView.facebookUser = LoginM.facebookUser;
    loginMergeView.accessToken = [[FBSession.activeSession accessTokenData] accessToken];
    [loginMergeView setFBDetails];
    [navigation.loginViewController.viewContent addSubview:loginMergeView];
    
}

- (void)hideMergeScreen:(BOOL)cancelAction {
    
    LoginNavigationController *navigation = (LoginNavigationController *)(self.navigationController);
    
    if ([[navigation.loginViewController.viewContent subviews] count]) {
        LoginMergeView *loginMergeView = [[navigation.loginViewController.viewContent subviews] lastObject];
        if ([loginMergeView isKindOfClass:[LoginMergeView class]]) {
            
            [UIView animateWithDuration:kViewNavigationDuration animations:^{
                loginMergeView.alpha = 0.0;
            } completion:^(BOOL finished) {
                [loginMergeView removeFromSuperview];
                
            }];
            
        }
    }
    
    if (cancelAction) {
        
        [[FBSession activeSession] closeAndClearTokenInformation];
        [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];

    } else {
        
        [_userManager loadDomainUsers:^{}];
        
        [self showHUDBlockingUI:YES];
        
        [self registerForSignInNotifications];
        [self registerForDeviceStateNotifications];
        
        [LoginM signInSecure];
        

    }

}

#pragma mark - Device

- (void)addDeviceToDomainProcess {

    LoginAssociateViewController *controller = [[ViewControllersFactory defaultFactory] loginAssociateViewController];
    controller.deviceMode = YES;
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (void)addOrActivateDeviceToDomainProcess {
    
    if (self.deviceState == TVDeviceStateNotRegistered) {
        
        [_deviceManager requestForAddDeviceToDomainWithDeviceName:[UIDevice currentDevice].name deviceBrandID:(isPad ? kDeviceBrand_iPad : kDeviceBrand_iPhone) completionBlock:^(id jsonResponse) {
            
            if (jsonResponse) {
                
                NSLog(@"requestForAddDeviceToDomainWithDeviceName result '%@'", jsonResponse);
                
                TVDomain * domain = [[TVDomain alloc] initWithDictionary:[jsonResponse objectForKey:@"m_oDomain"]];
                
                if (domain && [[jsonResponse objectForKey:@"m_oDomainResponseStatus"] intValue] == kDomainResponseStatus_OK) {
                    
                    if (LoginM.facebookUser) {
                        
                        [LoginM signInSecure];
                       
                    } else if (LoginM.userAddToDomain) {
                        
                        [LoginM signIn:LoginM.emailLogin password:LoginM.passwordLogin];
                        
                    }
                    
                } else {
                    
                    [self showDomainErrorMessage];
                    
                }
                
            } else {
                
                [self showDomainErrorMessage];
                
            }
            
        }];

    } else {
        
        
        [_deviceManager requestForChangeDeviceDomainStatus:YES completionBlock:^(id jsonResponse) {
            
            if (jsonResponse) {
                
                NSLog(@"requestForChangeDeviceDomainStatus result '%@'", jsonResponse);

                TVDomain * domain = [[TVDomain alloc] initWithDictionary:[jsonResponse objectForKey:@"m_oDomain"]];
                
                if (domain && [[jsonResponse objectForKey:@"m_oDomainResponseStatus"] intValue] == kDomainResponseStatus_OK) {
                    
                    if (LoginM.facebookUser) {
                        
                        [LoginM signInSecure];

                    } else if (LoginM.userAddToDomain) {
                        
                        [LoginM signIn:LoginM.emailLogin password:LoginM.passwordLogin];
                        
                    }
                    
                } else {
                    
                    [self showDomainErrorMessage];
                    
                }
                
            } else {
                
                [self showDomainErrorMessage];
                
            }
            
        }];
    }
    
}

- (void)showDomainErrorMessage {
    
    [self hideHUD];

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"assoc_login_register_domain_title") message:LS(@"assoc_login_register_domain_failed_message") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {

        [LoginM fullLogout];
        
    }];


}

- (void)checkDeviceState {
    
    [self showHUDBlockingUI:YES];
    TVDeviceState dState = [[TVSessionManager sharedTVSessionManager] deviceState];
    switch (dState)
    {
        case TVDeviceStateUnknown:
            [self registerForDeviceStateNotifications];
            [[TVSessionManager sharedTVSessionManager] checkDeviceState];
            break;
        case TVDeviceStateNotRegistered:
            
            if (APST.multiUser) {
                [self performSelector:@selector(checkCurrentUserState) withObject:nil afterDelay:0.5];
            } else {
                [self presentRegisterDeviceDialog];
            }
            
            break;
        case TVDeviceStateActivated:
            [self authenticationCompleted];
            break;
        case TVDeviceStateNotActivated:

            if (APST.multiUser) {
                [self performSelector:@selector(checkCurrentUserState) withObject:nil afterDelay:0.5];
            } else {
                [self presentActivateDeviceDialog];
            }
            
            break;
        default:
            break;
    }
}

- (void)presentRegisterDeviceDialog {
    
    [self registerForDeviceStateNotifications];
    [[TVSessionManager sharedTVSessionManager] registerDevice];
    
}

- (void)presentActivateDeviceDialog {
    
    [self registerForDeviceStateNotifications];
    [[TVSessionManager sharedTVSessionManager] activateDevice];
    
}

- (void)deviceStateAvailable:(NSNotification *) notification {
    
    [self checkDeviceState];
    
}

- (void)deviceActivationFailed:(NSNotification *)notification {
    
    [self hideHUD];
        
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"assoc_login_activate_device_failed_text") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
    
    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];
        
    }];

    
}
/* "PSC : Ten : OPF-1490 iOS - Login - user cant login to the app */
- (void)deviceRegistrationFailed:(NSNotification *)notification {
    
    [self hideHUD];
    
    NSDictionary * userInfo = notification.userInfo;
    NSError * error = [userInfo objectForKey:@"TVSessionManagerErrorKey"];
    NSDictionary * UserInfo1 = error.userInfo;
    NSInteger status = [[UserInfo1 objectForKey:@"m_oDomainResponseStatus"] integerValue];
    
    if (status == 10)
    {
        [[TVSessionManager sharedTVSessionManager] forceCheckDeviceState];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"assoc_login_register_device_failed_text") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        
        [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
            
            if ([LoginM isSignedIn]) {
                
                if ([LoginM isFacebookUser]){
                    [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];
                }
                
                [LoginM logout];
            }
        }];
    }

}

- (void)deviceRegistrationFailedWithKnownError:(NSNotification *)notification {
   
    [self hideHUD];

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"assoc_login_register_device_failed_already_registered_text") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
    
    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        
        if ([LoginM isSignedIn]) {
            
            if ([LoginM isFacebookUser]){
                [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];
            }
            
            [LoginM logout];
        }
    }];
}


- (void)deviceActivationCompleted:(NSNotification *)notification {
    
    [self hideHUD];
    
    [self authenticationCompleted];

}

- (void)authenticationCompleted {
    
    [self unregisterFromDeviceStateNotifications];
    [self unregisterFromSignInNotifications];
    if ([TVConfigurationManager sharedTVConfigurationManager].defaultIPNOID.length ==0)
    {
        [MenuM updateDomainMenu];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:KMenuItemsUpdatedNotification object:nil];
    }
    NSLog(@"authenticationCompleted !!!");
    
    [self hideHUD];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:KAuthenticationCompletedNotification object:nil];
    
}

- (void)deviceRegistrationCompleted:(NSNotification *)notification {
    
    TVDeviceState state = [[TVSessionManager sharedTVSessionManager] deviceState];
    
    if (state == TVDeviceStateActivated) {
        [self authenticationCompleted];
    } else if (state == TVDeviceStateNotActivated) {
        [self presentActivateDeviceDialog];
    } else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"assoc_login_register_device_failed_title") message:LS(@"assoc_login_register_device_failed2_text") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        
        [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:KFbNotification object:nil];
        }];

    }
}

- (void)dealloc {
    
    _userManager = nil;
    _deviceManager = nil;
    _socialManager = nil;
}

@end
