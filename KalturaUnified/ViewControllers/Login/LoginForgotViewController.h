//
//  LoginForgotViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 08.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

@interface LoginForgotViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIButton *buttonSend;

@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;

@property (weak, nonatomic) IBOutlet UILabel *labelEmail;
@property (weak, nonatomic) IBOutlet UILabel *labelText;


@end
