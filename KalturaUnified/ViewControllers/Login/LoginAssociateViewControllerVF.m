//
//  LoginAssociateViewControllerVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/6/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "LoginAssociateViewControllerVF.h"

@interface LoginAssociateViewControllerVF ()

@end

@implementation LoginAssociateViewControllerVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[VFTypography instance] modifyBrandButtonToGray:self.btnLoginAddDomain];
    [[VFTypography instance] modifyBrandButtonToGray:self.btnLoginAddDomainEmail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* "PSC : ONO : SYN-4067 VFGTVONES-169 - You can´t hide the keyboard assigning a household to a user */
- (void)closeKeyboard {
    [self.txtPasswordLoginAddDomain resignFirstResponder];
}

@end
