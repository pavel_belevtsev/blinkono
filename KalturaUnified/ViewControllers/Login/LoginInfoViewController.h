//
//  LoginInfoViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 08.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginInfoViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, strong) NSString *infoTitle;
@property (nonatomic, strong) NSURL *infoUrl;


@end
