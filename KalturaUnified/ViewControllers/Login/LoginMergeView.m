//
//  LoginMergeView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginMergeView.h"

#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"

@interface LoginMergeView ()

@end

@implementation LoginMergeView

@synthesize facebookUser = _facebookUser;
@synthesize accessToken = _accessToken;

#pragma mark -

- (void)closeKeyboard {

    [textFieldPassword resignFirstResponder];
    
}

- (IBAction)buttonMergePressed:(UIButton *)button {

    [self closeKeyboard];
    
    @try {
        if (textFieldPassword.text.length > 0)
        {
            NSString * userName = self.facebookUser.tvinciName;
            NSString * facebookID = [self.facebookUser.facebookUser objectForKey:@"id"];
            
            if (![userName length]) {
                userName = [TVSessionManager sharedTVSessionManager].currentUser.username;
            }
            
            [self showHUD];
            
            __weak LoginMergeView *weakSelf = self;
            
            __weak TVPAPIRequest *request = [TVPSiteAPI requestForFBUserMergeWithToken:_accessToken facebookID:facebookID username:userName password:textFieldPassword.text delegate:nil];
            
            [request setCompletionBlock:^{
                
                [weakSelf mergeResult:[request JSONResponse]];
                
            }];
            
            [request setFailedBlock:^{
                
                [weakSelf hideHUD];
            }];
            
            [_delegate sendRequest:request];
            
            //[self.delegateView userMergeWithToken:self.accessToken facebookID:facebookID username:userName password:textFieldPassword.text];
            
            NSLog(@"Name: %@ / ID: %@ / token: %@ / pass: %@",userName,facebookID,self.accessToken,textFieldPassword.text);
            //[self.delegateView buttonBackPressed:button];
        }
        else
        {
            [NSException raise:NSInvalidArgumentException format:LS(@"create_account_new_password_error_empty"), nil];
        }
    }
    @catch (NSException *exception)
    {
        
        [self.delegate presentAlertWithTitle:@"" message:exception.description andDelegate:nil];
    }

}

- (IBAction) buttonCancelPressed:(UIButton *)button{
    
    [_delegate hideMergeScreen:YES];
    
}

- (void)mergeResult:(NSDictionary *)jsonResponse {

    [self hideHUD];
    
    LoginM.facebookUser = [[TVFacebookUser alloc] initWithDictionary:[jsonResponse dictionaryByRemovingNSNulls]];
    
    if (LoginM.facebookUser.status == TVFacebookStatusWRONGPASSWORDORUSERNAME) {

        [self.delegate presentAlertWithTitle:LS(@"login_wrong_username_or_password") message:nil];
        
        [_delegate hideMergeScreen:YES];
        

    } else if (LoginM.facebookUser.status == TVFacebookStatusCONFLICT) {
        [self.delegate presentAlertWithTitle:LS(@"user_merge_conflict_email_exists_title") message:LS(@"user_merge_conflict_email_exists_body")];
        
        [_delegate hideMergeScreen:YES];
        

    } else if (LoginM.facebookUser.status == TVFacebookStatusMERGEOK){
        
    
        [_delegate hideMergeScreen:NO];
        
 
    } else {
        
        [self.delegate presentAlertWithTitle:LS(@"user_merge_generic_error") message:nil];
        
        [_delegate hideMergeScreen:YES];
    }
}

- (IBAction)buttonCheckPressed:(UIButton *)button {
    
    BOOL wasFirstResponder;
    if ((wasFirstResponder = [textFieldPassword isFirstResponder])) {
        [textFieldPassword resignFirstResponder];
    }
    
    [textFieldPassword setSecureTextEntry:![textFieldPassword isSecureTextEntry]];
    if (wasFirstResponder) {
        [textFieldPassword becomeFirstResponder];
        NSString *str = textFieldPassword.text;
        textFieldPassword.text = [NSString stringWithFormat:@"%@ ", textFieldPassword.text];
        textFieldPassword.text = str;
    }
    
    [button setTitle:([textFieldPassword isSecureTextEntry] ? LS(@"create_account_check") : LS(@"create_account_hide")) forState:UIControlStateNormal];
    
}

#pragma mark -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self closeKeyboard];
}

#pragma mark -
#pragma mark TextField


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
    
}



- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [UIView animateWithDuration:kViewAnimationDuration animations:^{
                                                                    
        viewContent.frame = CGRectMake(viewContent.frame.origin.x, viewContentY - 130.0, viewContent.frame.size.width, viewContent.frame.size.height);

    }];
    
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [UIView animateWithDuration:kViewAnimationDuration animations:^{
        viewContent.frame = CGRectMake(viewContent.frame.origin.x, viewContentY, viewContent.frame.size.width, viewContent.frame.size.height);
    }];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger textLength = [textField.text length] - range.length + [string length];
    
    labelPassword.hidden = (textLength > 0);
    
    return YES;
    
}

#pragma mark -

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    accountOneImage.layer.cornerRadius = accountOneImage.frame.size.width / 2.0;
    accountOneImage.clipsToBounds = YES;
    
    accountTwoImage.layer.cornerRadius = accountTwoImage.frame.size.width / 2.0;
    accountTwoImage.clipsToBounds = YES;
    
    [buttonCheck setTitle:LS(@"create_account_check") forState:UIControlStateNormal];
    
}

- (void)setFBDetails {
    
    [Utils setTextFieldFont:textFieldPassword bold:YES];
    
    [Utils setLabelFont:labelPassword bold:YES];
    labelPassword.text = LS(@"password");
    
    [Utils setLabelFont:titleLabel bold:YES];
    [Utils setLabelFont:titleSubLabel bold:YES];
    
    [Utils setLabelFont:labelText];
    
    [textFieldPassword setSecureTextEntry:YES];
    
    [Utils setLabelFont:accountOneNameLabel bold:YES];
    [Utils setLabelFont:accountTwoNameLabel bold:YES];
    
    
    [Utils setButtonFont:mergeButton bold:YES];
    [Utils setButtonFont:cancelButton bold:YES];
    
    [_delegate initBrandButtonUI:mergeButton];
    if (isPad) {
        [_delegate initSelectedBrandButtonUI:cancelButton];
    }
    
    titleLabel.text = LS(@"user_merge_dialog_title");
    titleSubLabel.text = LS(@"user_merge_dialog_subtitle");
    
    labelText.text = LS(@"user_merge_dialog_text");
    
    [cancelButton setTitle:LS(@"cancel") forState:UIControlStateNormal];
    
    [mergeButton setTitle:LS(@"user_merge_dialog_title") forState:UIControlStateNormal];
    
    accountOneNameLabel.text = self.facebookUser.facebookName;
    //accountTwoNameLabel.text = self.facebookUser.tvinciName;
    NSURL *avatarUrl = self.facebookUser.pictureURL;
    
    if (avatarUrl){

        [accountOneImage sd_setImageWithURL:avatarUrl];
    }
    
    __weak TVPAPIRequest * request = [TVPSiteAPI requestForGetUserDetails:_facebookUser.siteGuid delegate:nil];
    
    [request setCompletionBlock:^{
        
        TVUser *user = [[TVUser alloc] initWithDictionary:[[request JSONResponse] objectForKey:@"m_user"]];
        
        NSString *respFirstName = user.firstName;
        NSString *respLastName = user.lastName;
        
        accountTwoNameLabel.text = [NSString stringWithFormat:@"%@ %@" ,respFirstName, respLastName];
        
    }];
    
    
    [request setFailedBlock:^{
        
    }];
    
    [self.delegate sendRequest:request];
    
    viewContentY = viewContent.frame.origin.y;
    
    NSLog(@"one: %@ / two: %@",_facebookUser.facebookName,_facebookUser.tvinciName);

}

#pragma mark - HUD

- (void)showHUD {
    
    if (self.activityView == nil) {
        
        self.activityView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityView" owner:self options:nil] objectAtIndex:0];
        self.activityView.frame = self.bounds;
        self.activityView.viewBackground.hidden = YES; // SYN-3122
        [self addSubview:self.activityView];
        
    }
    
}

- (void)hideHUD {
    
    if (self.activityView) {
        
        [self.activityView removeFromSuperview];
        self.activityView = nil;
        
    }
    
}

@end
