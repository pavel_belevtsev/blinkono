//
//  LoginMultiViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginBaseViewController.h"
#import "PageControlView.h"

@interface LoginMultiViewController : LoginBaseViewController


@property (nonatomic, weak) IBOutlet UIView *subViewMultiLogin;
@property (nonatomic, weak) IBOutlet UIView *subViewMultiLoginUser;
@property (nonatomic, weak) IBOutlet UIView *subViewMultiLoginFacebookUser;
@property (nonatomic, weak) IBOutlet UIView *subViewMultiLoginNewUser;

@property (nonatomic, weak) IBOutlet UILabel *lblEmailMulti;
@property (nonatomic, weak) IBOutlet UITextField *txtPasswordLoginMulti;
@property (nonatomic, weak) IBOutlet UILabel *lblRememberMeMulti;
@property (nonatomic, weak) IBOutlet UIButton *btnLoginMulti;
@property (nonatomic, weak) IBOutlet UIView *viewForgotPasswordMulti;
@property (nonatomic, weak) IBOutlet UISwitch *switchBtnRememberMeMulti;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollUsersMulti;

@property (nonatomic, weak) IBOutlet UIButton *btnCreateNewAccountMulti;

@property (nonatomic, weak) IBOutlet UIButton *btnLoginFacebookMulti;
@property (nonatomic, weak) IBOutlet UILabel *lblRememberMeFacebookMulti;
@property (nonatomic, weak) IBOutlet UISwitch *switchBtnRememberMeFacebookMulti;

@property (nonatomic, weak) IBOutlet UIView *viewNavigation;
@property (nonatomic, strong) PageControlView *pageControl;

@property (nonatomic, assign) BOOL isNotRoot;
@property (weak, nonatomic) NSString *segue;
@property (weak, nonatomic) TVFacebookUser *fbUser;


@end
