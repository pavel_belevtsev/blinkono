//
//  LoginStartViewControllerVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/6/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "LoginStartViewControllerVF.h"
#import "UIImageView+WebCache.h"

@interface LoginStartViewControllerVF ()

@end

@implementation LoginStartViewControllerVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [[VFTypography instance] modifyBrandButtonToGray:self.buttonLogin];
}

-(void)updateLoginLogoImage
{
    if (isPhone)
    {
        [super updateLoginLogoImage];
        return;
    }
    NSUInteger locate = self.imgLogo.frame.origin.y;
    self.imgLogo.image = [UIImage imageNamed:@"pLogo_login"];
    self.imgLogo.frame = CGRectMake(0, 0, 225, 38);
    self.imgLogo.center = CGPointMake(self.centerContainer.frame.size.width/2, locate);
    
    CGRect frame = self.imgLogo.frame;
    frame.origin.y= locate;
    self.imgLogo.frame = frame;
}
@end
