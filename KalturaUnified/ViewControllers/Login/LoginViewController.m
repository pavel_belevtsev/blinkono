//
//  LoginViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginStartViewController.h"
#import "LoginMultiViewController.h"
#import "LoginAccountViewController.h"
#import "LoginAnimatorPop.h"
#import "LoginAnimatorPush.h"
#import "HomeViewController.h"
#import "UIImageView+WebCache.h"
#import "MenuViewController.h"
#import "LoginNavigationController.h"

@interface LoginViewController ()

@property LoginNavigationController  *currentDetailViewController;
@property (strong, nonatomic) LoginAnimatorPop *animatorPop;
@property (strong, nonatomic) LoginAnimatorPush *animatorPush;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    self.loadNoInternetView = YES;
   [super viewDidLoad];
    // Do any additional setup after loading the view.
        
    loginPoint = @"";
    [Utils setLabelFont:_labelTitle bold:YES];
        
    if (_buttonBack) {
        [Utils setButtonFont:_buttonBack bold:YES];
        [_buttonBack setTitle:LS(@"back") forState:UIControlStateNormal];
    }
    
    self.animatorPop = [LoginAnimatorPop new];
    self.animatorPush = [LoginAnimatorPush new];
    
    BaseViewController *controller = nil;
    ViewControllersFactory* factory = [ViewControllersFactory defaultFactory];
    
    if (APST.multiUser && [LoginM.domainUsers count]) {
        
        if (isPad && (![_segue isEqualToString:@"fbLogin"])) {
            controller = [factory loginMultiViewController];
            ((LoginMultiViewController*)controller).isNotRoot = _isNotRoot;
            ((LoginMultiViewController*)controller).segue = _segue;
            ((LoginMultiViewController*)controller).fbUser = _fbUser;

        } else {
            controller = [factory loginStartViewController];
             ((LoginStartViewController*)controller).isNotRoot = _isNotRoot;
            ((LoginStartViewController*)controller).segue = _segue;
            ((LoginStartViewController*)controller).fbUser = _fbUser;
        }

    } else {
    
        controller = [factory loginStartViewController];
        ((LoginStartViewController*)controller).isNotRoot = _isNotRoot;
        ((LoginStartViewController*)controller).segue = _segue;
        ((LoginStartViewController*)controller).fbUser = _fbUser;
    }

    LoginNavigationController *navigation = [[ViewControllersFactory defaultFactory] loginNavigationController:controller];
    navigation.navigationBarHidden = YES;
    navigation.delegate = self;
    navigation.loginViewController = self;
    
    [self presentDetailController:navigation];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutNotification) name:KLogoutNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginNotification:) name:KLoginNotification object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(buttonBackPressed:) name:KFbNotification object:nil];

    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authenticationCompleted) name:KAuthenticationCompletedNotification object:nil];
 
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KAuthenticationCompletedNotification object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self menuContainerViewController].panMode = MFSideMenuPanModeNone;
    
}

#pragma mark - Logout

- (void)restartHUD {

    [super restartHUD];
    
    if (self.currentDetailViewController) {
     
        BaseViewController *controller = [[_currentDetailViewController viewControllers] lastObject];
        
        if ([controller isKindOfClass:[BaseViewController class]]) {
            [((BaseViewController *)controller) restartHUD];
        }

        
    }
    
}

- (void)logoutNotification {
    
    [LoginM logout];
    
    [self.currentDetailViewController popToRootViewControllerAnimated:NO];
    if ([self menuContainerViewController].menuState == MFSideMenuStateLeftMenuOpen) {
        [[self menuContainerViewController] setMenuState:MFSideMenuStateClosed];
    }
    
    
    [[self menuContainerViewController].centerViewController popToRootViewControllerAnimated:NO];
    prevControllerStack = nil;


}

#pragma mark - Login

- (void)loginNotification:(NSNotification*)notification {
    
    loginPoint = @"";
    
    if ([notification.name isEqualToString:KLoginNotification])
    {
        NSDictionary* userInfo = notification.userInfo;
        if (userInfo && [userInfo objectForKey:@"loginPoint"]){
            loginPoint = [userInfo objectForKey:@"loginPoint"];
        }
    }
    
    prevControllerStack = [[self menuContainerViewController].centerViewController viewControllers];
    
    [self.currentDetailViewController popToRootViewControllerAnimated:NO];
    
    menuState = [self menuContainerViewController].menuState;
    
    if (menuState == MFSideMenuStateLeftMenuOpen)
    {
        [[self menuContainerViewController] setMenuState:MFSideMenuStateClosed];
    }
    
    BOOL animate = NO;
    if ([loginPoint isEqualToString:@"playerVODFreeContent"] || [loginPoint isEqualToString:@"playerVODPaidContent"]){
        animate = YES;
    }
    
    [[self menuContainerViewController].centerViewController popToRootViewControllerAnimated:animate];
}

- (void)setUserInteractionStateForCenterViewController {

    if([[self menuContainerViewController].centerViewController respondsToSelector:@selector(viewControllers)]) {
        NSArray *viewControllers = [[self menuContainerViewController].centerViewController viewControllers];
        for(UIViewController* viewController in viewControllers) {
            viewController.view.userInteractionEnabled = YES;
        }
    }
}



#pragma mark - Login


-(void) verifyFacebookTokenIfNeededWithCompletion:(void(^)(void)) completion
{
    
    if (completion)
    {
        completion();
    }
    
//    if ([[TVSessionManager sharedTVSessionManager] isUserConnectedToFacebook])
//    {
//        TVPAPIRequest * request = [TVPSocialAPI requestforFBTokenValidation:[[[TVSessionManager sharedTVSessionManager] currentUser] faceBookToken] delegate:nil];
//        
//        [request setFailedBlock:^{
//            
//            if (completion)
//            {
//                completion();
//            }
//            
//        }];
//        
//        
//        [request setCompletionBlock:^{
//            
//            if (completion)
//            {
//                completion();
//            }
//        }];
//        
//        [self sendRequest:request];
//    }
//    else
//    {
//        
//    }
}

- (void)authenticationCompleted {
    
    [self verifyFacebookTokenIfNeededWithCompletion:^{

    [LoginM rememberMeImplementation];
        
    NSArray *allControllers = [_currentDetailViewController viewControllers];
    
    UIViewController *firstControllerInStack = [allControllers firstObject];
    
    if (([firstControllerInStack isKindOfClass:[LoginStartViewController class]] || [firstControllerInStack isKindOfClass:[LoginMultiViewController class]]) && allControllers.count <= 2){
        
        if ((((LoginStartViewController*)firstControllerInStack).isNotRoot) || (((LoginMultiViewController*)firstControllerInStack).isNotRoot))
        {
            
            if ([[MenuM menuItem:TVPageLayoutMyZone] isEqual:selectedMenuItem] && /*isPhone&&*/ [LoginM isSignedIn])
            {
                
                for (int i = 0; i<[[self menuContainerViewController].centerViewController viewControllers].count; i++){

                    MyZoneViewController *controller = [[[self menuContainerViewController].centerViewController viewControllers] objectAtIndex:i];
                    
                    if ([controller isKindOfClass:[MyZoneViewController class]])
                    {
                        
                        MyZoneViewController *myZoneViewController = nil;
                        if (isPad)
                        {
                            myZoneViewController = [[ViewControllersFactory defaultFactory] myZoneFullScreenViewController];
                        }
                        else
                        {
                            myZoneViewController = [[ViewControllersFactory defaultFactory] myZoneViewController];
                        }
                                
                        myZoneViewController.activityFeedLoadOnStart = (controller.viewLogin ? NO:YES);
                        
                        NSMutableArray *newControllerStack = [NSMutableArray arrayWithArray:[[self menuContainerViewController].centerViewController viewControllers]];
                        
                        [newControllerStack replaceObjectAtIndex:i withObject:myZoneViewController];
                        [[self menuContainerViewController].centerViewController setViewControllers:newControllerStack  animated:NO];

                        break;
                        
                    }
                }

            }
            
            [self.navigationController popViewControllerAnimated:YES];
            [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) selectMenuItem:selectedMenuItem];

            return;
        }
    }
    
    if (!prevControllerStack) {
        
        [self loadHomeController];

    }
    else {
        if ([loginPoint isEqualToString:@"buttonSettigs"]) {
            if ([LoginM isSignedIn]) {
                [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) buttonSettingsPressed:nil];
            }
            else {
                [self closeLoginView];
            }
        }
        else if ([loginPoint isEqualToString:@"buttonSubscriptions"]) {
            if ([LoginM isSignedIn]) {
                [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) buttonSubscriptionsPressed:nil];
            }
            else {
                [self closeLoginView];
            }
        }
        else if ([loginPoint isEqualToString:@"buttonLive"]) {
            if ([LoginM isSignedIn]) {
                [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) buttonLivePressed:nil];
            }
            else {
                [self closeLoginView];
            }
        }
        else if ([loginPoint isEqualToString:@"buttonLogout"]) {
            if ([LoginM isSignedIn]) {
                [self loadHomeController];
            }
            else {
                [self closeLoginView];
            }
        }
        else if ([loginPoint isEqualToString:@"playerVODPaidContent"] || [loginPoint isEqualToString:@"playerVODFreeContent"]) {
            [self closeLoginView];
        }
        else{
            [self closeLoginView];
        }
    }
    loginPoint = @"";
            }];
}

-(void) loadHomeController {
    
    if (APST.multiUser) {
        
        NSInteger realDomainID = [TVSessionManager sharedTVSessionManager].sharedInitObject.domainID;
        
        if (([[[NSUserDefaults standardUserDefaults] objectForKey:@"DomainID"] integerValue] == 0) && (realDomainID > 0)) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@(realDomainID) forKey:@"DomainID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        [super.userManager loadDomainUsers:^{ }];
        
    }
    HomeViewController *controller = [[ViewControllersFactory defaultFactory] homeViewController];
    
    [[self menuContainerViewController].centerViewController pushViewController:controller animated:YES];
    [controller updateMenu];
    
}

-(void) closeLoginView {
    
    [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) selectMenuItem:selectedMenuItem];

    BOOL animate = YES;
    
    if (menuState == MFSideMenuStateLeftMenuOpen) {
        animate = NO;
        [[self menuContainerViewController] setMenuState:MFSideMenuStateLeftMenuOpen];
    }
    
    [[self menuContainerViewController].centerViewController setViewControllers:prevControllerStack  animated:animate];
    
    [self setUserInteractionStateForCenterViewController];
}

#pragma mark - Buttons

- (IBAction)buttonBackPressed:(UIButton *)button {
    
    NSArray *allControllers = [_currentDetailViewController viewControllers];
    
    UIViewController *firstControllerInStack = [allControllers firstObject];
    
    if ([firstControllerInStack isKindOfClass:[LoginStartViewController class]] && allControllers.count <= 2){
        
        [(LoginStartViewController*)firstControllerInStack hideHUD];
        if (((LoginStartViewController*)firstControllerInStack).isNotRoot){
            [[NSNotificationCenter defaultCenter] postNotificationName:KAuthenticationCompletedNotification object:nil];

            return;
        }
    }
    [_currentDetailViewController popViewControllerAnimated:YES];
}


- (IBAction)buttonClosePressed:(UIButton *)button {
    
    if (([loginPoint isEqualToString: @"playerVODFreeContent"] || [loginPoint isEqualToString: @"playerVODPaidContent"]) && [_currentDetailViewController viewControllers].count == 1){
        [[NSNotificationCenter defaultCenter] postNotificationName:KAuthenticationCompletedNotification object:nil];
    }
    
}

#pragma mark - NavigationController Delegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    if ([viewController isKindOfClass:[LoginStartViewController class]]){
        
        selectedMenuItem = ((MenuViewController *)[self menuContainerViewController].leftMenuViewController).selectedMenuItem;
        
    }
}


- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {
    

    BaseViewController *controller = (BaseViewController *)viewController;
    
    selectedMenuItem = ((MenuViewController *)[self menuContainerViewController].leftMenuViewController).selectedMenuItem;

    if ([controller isKindOfClass:[LoginStartViewController class]]){
        

        
        controller.title = @"";

        BOOL hideButons = NO;
        
        if ([loginPoint isEqualToString: @"playerVODFreeContent"]){
        
            controller.title = LS(@"login_to_watch1");
            hideButons = YES;
        }

        if ([loginPoint isEqualToString: @"playerVODPaidContent"]){
            
            controller.title = LS(@"login_to_purchase");
            hideButons = YES;

        }
    
        ((LoginStartViewController*)controller).buttonSkip.hidden = hideButons;
        ((LoginStartViewController*)controller).buttonRemoveDevice.hidden = hideButons;
        ((LoginStartViewController*)controller).imgLogo.hidden = hideButons;
        
    }
    
    
    [UIView animateWithDuration:kViewNavigationDuration animations:^{
        _viewNavigation.alpha = ([controller showViewNavigation] ? 1.0 : 0);
    }];
    
    _imgLogo.hidden = ![controller showLogoNavigation];
    
    if (!_imgLogo.hidden)
    {
        [self updateLoginLogoImage];
    }
    
    if (_imgLogo){
        _labelTitle.hidden = !_imgLogo.hidden;
    }
    
    _labelTitle.text = controller.title;
    
    if (_buttonBack) {
        _buttonBack.hidden = ([navigationController.viewControllers count] < 2);
    }
    
    if (_buttonClose && ([loginPoint isEqualToString: @"playerVODFreeContent"] || [loginPoint isEqualToString: @"playerVODPaidContent"])){
        _buttonClose.hidden = !([navigationController.viewControllers count] < 2);
    } else {
        _buttonClose.hidden = YES;
    }
}

-(void)updateLoginLogoImage
{
    if (APST.logoUrl)
    {
        [_imgLogo sd_setImageWithURL:APST.logoUrl];
    }
    else
    {
        _imgLogo.image = [UIImage imageNamed:@"logo_login"];
    }
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    

    if (operation == UINavigationControllerOperationPop) {
        return self.animatorPop;
    } else if (operation == UINavigationControllerOperationPush) {
        return self.animatorPush;
    }
    
    return nil;
}

#pragma mark - Navigation

- (void)presentDetailController:(LoginNavigationController*)detailVC {
    
    if (self.currentDetailViewController){
        [self removeCurrentDetailViewController];
    }
    
    [self addChildViewController:detailVC];
    
    detailVC.view.frame = [self frameForDetailController];
    
    [self.detailView addSubview:detailVC.view];
    self.currentDetailViewController = detailVC;
    
    [detailVC didMoveToParentViewController:self];
    
}

- (void)removeCurrentDetailViewController {
    
    [self.currentDetailViewController willMoveToParentViewController:nil];
    [self.currentDetailViewController.view removeFromSuperview];
    [self.currentDetailViewController removeFromParentViewController];

}

- (CGRect)frameForDetailController {
    
    CGRect detailFrame = self.detailView.bounds;
    return detailFrame;

}

#pragma mark -

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KLogoutNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KLoginNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
