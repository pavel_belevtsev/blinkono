//
//  LoginViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginBaseViewController.h"

@interface LoginViewController : LoginBaseViewController <UINavigationControllerDelegate> {

    NSArray  *prevControllerStack;
    MFSideMenuState menuState;
    TVMenuItem *selectedMenuItem;
    NSString *loginPoint;
}

@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIView *viewNavigation;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIButton *buttonClose;
@property (nonatomic, assign) BOOL isNotRoot;
@property (weak, nonatomic) NSString *segue;
@property (nonatomic, retain) TVFacebookUser *fbUser;

@end
