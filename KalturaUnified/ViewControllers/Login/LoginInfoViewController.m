//
//  LoginInfoViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 08.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginInfoViewController.h"
#import "AnalyticsManager.h"

@interface LoginInfoViewController ()

@end

@implementation LoginInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!_infoTitle) {
        
        self.infoTitle = LS(@"screen_title_information");
        self.infoUrl = APST.aboutUsUrl;
        
    }
    
    self.title = self.infoTitle;
    
    [_webView loadRequest:[NSURLRequest requestWithURL:self.infoUrl]];
    [AnalyticsM trackScreen:@"Info"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if (inType == UIWebViewNavigationTypeLinkClicked) {
        
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        
        return NO;
    }
    
    return YES;
}

- (void)dealloc {
    
    _webView.delegate = nil;
    [_webView stopLoading];
    
}

@end
