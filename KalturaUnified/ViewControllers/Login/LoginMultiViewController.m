//
//  LoginMultiViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginMultiViewController.h"
#import "UIImageView+WebCache.h"
#import "LoginUserView.h"
#import "LoginForgotViewController.h"
#import "LoginRegisterViewController.h"
#import "AnalyticsManager.h"
#import <TvinciSDK/OpenUDID.h>
#import "UIAlertView+Blocks.h"

const CGFloat LoginUserMinScale = 0.7;

@interface LoginMultiViewController ()

@property (nonatomic) int multiUserPageIndex;

@end

@implementation LoginMultiViewController

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KDomainUsersListUpdatedNotification object:nil];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initInterface];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUsersList) name:KDomainUsersListUpdatedNotification object:nil];
    
    self.txtPasswordLoginMulti.isAccessibilityElement = YES;
    self.txtPasswordLoginMulti.accessibilityLabel = @"Text Field Multi";
    
    self.btnLoginMulti.isAccessibilityElement = YES;
    self.btnLoginMulti.accessibilityLabel = @"Button Login Multi";

}

- (void)initInterface {
    
    self.txtPasswordLoginMulti.placeholder = LS(@"password");
    self.lblRememberMeMulti.text = LS(@"login_remember_me");
    [self.btnLoginMulti setTitle:LS(@"login") forState:UIControlStateNormal];
    
    [self.btnCreateNewAccountMulti setTitle:LS(@"create_account_txt") forState:UIControlStateNormal];
    
    [self.btnLoginFacebookMulti setTitle:LS(@"login_facebook") forState:UIControlStateNormal];
    self.lblRememberMeFacebookMulti.text = LS(@"login_remember_me");
    
    
    [self initTextFieldUI:_txtPasswordLoginMulti];

    
    [Utils setLabelFont:_lblEmailMulti bold:YES];
    
    [self initBrandButtonUI:self.btnLoginMulti];

    [self initSwitchUI:_switchBtnRememberMeMulti];
    [self initSwitchUI:_switchBtnRememberMeFacebookMulti];
    
    TVUnderlineButton *buttonForgot = [self createUnderlineButtonUI:_viewForgotPasswordMulti font:Font_Bold(_btnLoginMulti.titleLabel.font.pointSize)];
    [buttonForgot setTitle:LS(@"login_forgot_password") forState:UIControlStateNormal];
    [buttonForgot addTarget:self action:@selector(buttonForgotPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [Utils setLabelFont:_lblRememberMeMulti bold:YES];
    [Utils setLabelFont:_lblRememberMeFacebookMulti bold:YES];
    
    int rememberWidth = [_lblRememberMeMulti.text sizeWithAttributes:@{NSFontAttributeName:_lblRememberMeMulti.font}].width + 40.0;
    int rememberX = ([_lblRememberMeMulti superview].frame.size.width - (rememberWidth + _switchBtnRememberMeMulti.frame.size.width)) / 2;
    
    _lblRememberMeMulti.frame = CGRectMake(rememberX, _lblRememberMeMulti.frame.origin.y, rememberWidth, _lblRememberMeMulti.frame.size.height);
    _switchBtnRememberMeMulti.frame = CGRectMake(rememberX + rememberWidth, _switchBtnRememberMeMulti.frame.origin.y, _switchBtnRememberMeMulti.frame.size.width, _switchBtnRememberMeMulti.frame.size.height);
    
    rememberX = ([_lblRememberMeFacebookMulti superview].frame.size.width - (rememberWidth + _switchBtnRememberMeFacebookMulti.frame.size.width)) / 2;
    
    _lblRememberMeFacebookMulti.frame = CGRectMake(rememberX, _lblRememberMeFacebookMulti.frame.origin.y, rememberWidth, _lblRememberMeFacebookMulti.frame.size.height);
    _switchBtnRememberMeFacebookMulti.frame = CGRectMake(rememberX + rememberWidth, _switchBtnRememberMeFacebookMulti.frame.origin.y, _switchBtnRememberMeFacebookMulti.frame.size.width, _switchBtnRememberMeFacebookMulti.frame.size.height);
    
    [self initFacebookButtonUI:self.btnLoginFacebookMulti];
    [self initBrandButtonUI:self.btnCreateNewAccountMulti];
    
    [Utils setTextFieldFont:_txtPasswordLoginMulti bold:YES];
    
    self.btnCreateNewAccountMulti.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    [self updateUsersList];
    
    UIView *viewScroll = [_scrollUsersMulti superview];
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewScrollTap:)];
    [viewScroll addGestureRecognizer:recognizer];
    
}

- (void)viewScrollTap:(UITapGestureRecognizer *)recognizer {

    CGPoint coords = [recognizer locationInView:recognizer.view];
    
    CGFloat pageWidth = _scrollUsersMulti.frame.size.width;
    int page = floor((_scrollUsersMulti.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    if (((int)_scrollUsersMulti.contentOffset.x) % ((int)pageWidth) == 0) {
        
        if (coords.x < _scrollUsersMulti.frame.origin.x) {
            
            if (page > 0) {
                [_scrollUsersMulti setContentOffset:CGPointMake(pageWidth * (page - 1), 0) animated:YES];
            }

        } else if (coords.x > _scrollUsersMulti.frame.origin.x + _scrollUsersMulti.frame.size.width) {
            
            if (page < [LoginM.domainUsers count]) {
                [_scrollUsersMulti setContentOffset:CGPointMake(pageWidth * (page + 1), 0) animated:YES];
            }
        }
    }
    
    
}

- (void)updateUsersList {
    
    if (self.pageControl) [self.pageControl removeFromSuperview];
    if ([LoginM.domainUsers count]) [self pageControlConfiguration];
    
    [self.scrollUsersMulti removeAllSubviewsKindOfClass:[LoginUserView class]];
    
    self.scrollUsersMulti.contentOffset = CGPointZero;
    self.subViewMultiLogin.alpha = 0.0;
    self.multiUserPageIndex = 0;
    
    self.subViewMultiLoginUser.hidden = YES;
    self.subViewMultiLoginNewUser.hidden = YES;
    self.subViewMultiLoginFacebookUser.hidden = YES;
    
    self.switchBtnRememberMeMulti.on = LoginM.didRememberMePressed;
    self.switchBtnRememberMeFacebookMulti.on = LoginM.didRememberMePressed;
    
    for (int i = 0; i < [LoginM.domainUsers count] + 1; i++) {
        
        LoginUserView *userView = [[[NSBundle mainBundle] loadNibNamed:@"LoginUserView" owner:self options:nil] objectAtIndex:0];
        TVUser *user = nil;
        
        if (i < [LoginM.domainUsers count]) {
            user = [LoginM.domainUsers objectAtIndex:i];
            userView.labelName.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
            
            __weak UIImageView *imgAvatar = userView.imgAvatar;
            
            NSURL *imageUrl = [user facebookPictureURLForSizeType:TVUserFacebookPictureTypeLarge];
            if (imageUrl) {
                [userView.imgAvatar sd_setImageWithURL:imageUrl completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (!error && image) {
                        
                    } else {
                        imgAvatar.image = [UIImage imageNamed:@"login_avatar"];
                    }
                }];
            } else {
                userView.imgAvatar.image = [UIImage imageNamed:@"login_avatar"];
            }
            
            if (i == 0) {
                if (user.facebookID && ![user.facebookID isKindOfClass:[NSNull class]] && [user.facebookID length] > 0) {
                    self.subViewMultiLoginFacebookUser.hidden = NO;
                    self.subViewMultiLoginUser.hidden = YES;
                } else {
                    self.txtPasswordLoginMulti.text = @"";
                    self.subViewMultiLoginUser.hidden = NO;
                    self.subViewMultiLoginFacebookUser.hidden = YES;
                }
            }
            
        } else {
            userView.labelName.text = LS(@"add_new_user");
            userView.imgAvatar.image = [UIImage imageNamed:@"login_new_avatar"];
            if (self.subViewMultiLoginUser.hidden && self.subViewMultiLoginFacebookUser.hidden) {
                self.subViewMultiLoginNewUser.hidden = NO;
            }
        }
        userView.tag = 100 + i;
        userView.frame = CGRectMake(userView.frame.size.width * i, self.scrollUsersMulti.frame.size.height - userView.frame.size.height, userView.frame.size.width, userView.frame.size.height);
        if (i == 0) {
            self.subViewMultiLogin.alpha = 1.0;
            if (user) self.lblEmailMulti.text = user.username;
        } else {
            userView.transform = CGAffineTransformMakeScale(LoginUserMinScale, LoginUserMinScale);
        }
        [self.scrollUsersMulti addSubview:userView];
    }
    
    self.scrollUsersMulti.contentSize = CGSizeMake(self.scrollUsersMulti.frame.size.width * ([LoginM.domainUsers count] + 1), self.scrollUsersMulti.frame.size.height);

}

- (IBAction)buttonRemoveDevicePressed:(UIButton *)button {
    
    [LoginM fullLogout];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DomainID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    __weak TVPAPIRequest * request = [TVPDomainAPI requestForGetDeviceDomains:nil];
    
    [request setFailedBlock:^{
        NSLog(@"GetDeviceDomain failed");
    }];
    
    [request setCompletionBlock:^{
        
        NSInteger deviceBrand = kDeviceBrand_iPhone;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            deviceBrand = kDeviceBrand_iPad;
        }
        else
        {
            deviceBrand = kDeviceBrand_iPhone;
            
        }
        
        NSArray * domains = [request JSONResponse];
        
        for (NSDictionary * dict in domains) {
            
            NSNumber * domainID =  [dict objectForKey:@"DomainID"];
            __weak TVPAPIRequest * removeRequest = [TVPDomainAPI requestForRemoveDeviceFromDomainWithDeviceName:[OpenUDID value]  deviceBrandID:deviceBrand domainID:domainID.intValue delegate:nil];
            
            [removeRequest setCompletionBlock:^{
                
                NSLog(@"removing device completed");
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Removing device completed" message:nil delegate:nil cancelButtonTitle:@"Quit App" otherButtonTitles:nil];
                [alert showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    
                    exit(0);
                    
                }];
                
            }];
            
            
            [self sendRequest:removeRequest];
        }
        
        
    }];
    
    [self sendRequest:request];
    
}

- (void)reloadUsersData {
    
    [self showHUDBlockingUI:NO];
    
    [super.userManager loadDomainUsers:^{
        
        [self hideHUD];
        
      //  [self updateUsersList];
        
    }];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    


    [self registerForSignInNotifications];
    [self registerForDeviceStateNotifications];
    [AnalyticsM trackScreen:@"Login main"];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self unregisterFromSignInNotifications];
    [self unregisterFromDeviceStateNotifications];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    _txtPasswordLoginMulti.text = @"";
    
}

- (BOOL)showLogoNavigation {
    
    return YES;
    
}

#pragma mark - PageControl


- (void)pageControlConfiguration {
    CGRect pageControlFrame = _viewNavigation.bounds;
    self.pageControl = [[PageControlView alloc] initWithFrame:pageControlFrame];
    [self.pageControl.control addTarget:self action:@selector(pageControlChanged:) forControlEvents:UIControlEventValueChanged];
    self.pageControl.numberOfPages = [LoginM.domainUsers count] + 1;
    [_viewNavigation addSubview:self.pageControl];
}

- (void)pageControlChanged:(id)sender {
    UIPageControl *pageControl = sender;
    CGFloat pageWidth = CGRectGetWidth(self.scrollUsersMulti.frame);
    CGPoint scrollTo = CGPointMake(pageWidth * pageControl.currentPage, 0);
    [self.scrollUsersMulti setContentOffset:scrollTo animated:YES];
}


#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView == self.scrollUsersMulti) {
        
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        if (page != self.multiUserPageIndex) {
            self.multiUserPageIndex = page;
            
            [self closeKeyboard];
            
            [self.pageControl setCurrentPage:page];
            
            if (page < [LoginM.domainUsers count]) {
                
                TVUser *user = [LoginM.domainUsers objectAtIndex:page];
                if (user.facebookID && ![user.facebookID isKindOfClass:[NSNull class]] && [user.facebookID length] > 0) {
                    self.subViewMultiLoginUser.hidden = YES;
                    self.subViewMultiLoginFacebookUser.hidden = NO;
                } else {
                    self.txtPasswordLoginMulti.text = @"";
                    self.subViewMultiLoginUser.hidden = NO;
                    self.subViewMultiLoginFacebookUser.hidden = YES;
                }
                
                self.subViewMultiLoginNewUser.hidden = YES;
                
                self.lblEmailMulti.text = user.username;
                
            } else {
                
                self.subViewMultiLoginUser.hidden = YES;
                self.subViewMultiLoginFacebookUser.hidden = YES;
                self.subViewMultiLoginNewUser.hidden = NO;
                
            }
        }
        
        float offset = fabs(scrollView.contentOffset.x - (scrollView.frame.size.width * page));
        
        if (offset > pageWidth / 4) {
            self.subViewMultiLogin.alpha = 0.0;
        } else {
            self.subViewMultiLogin.alpha = 1.0 - (offset / (pageWidth / 4));
        }
        
        for (int i = 0; i < [LoginM.domainUsers count]; i++) {
            
            
            
        }
        
        [self.scrollUsersMulti performBlockOnSubviewsKindOfClass:[LoginUserView class] block:^(UIView *subview) {
            float offset = fabs(subview.frame.origin.x - scrollView.contentOffset.x);
            
            float scale = LoginUserMinScale;
            
            if (offset < self.scrollUsersMulti.frame.size.width) {
                scale = LoginUserMinScale + (1.0 - LoginUserMinScale) * ((self.scrollUsersMulti.frame.size.width - offset) / self.scrollUsersMulti.frame.size.width);
            }
            
            subview.transform = CGAffineTransformMakeScale(scale, scale);

        }];
    }
}

#pragma mark -
#pragma mark TextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    _scrollUsersMulti.scrollEnabled = NO;
    
    [UIView animateWithDuration:0.25 animations:^{
        super.viewContent.frame = CGRectMake(super.viewContent.frame.origin.x, -300, super.viewContent.frame.size.width, super.viewContent.frame.size.height);
    }];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    _scrollUsersMulti.scrollEnabled = YES;
    
    [UIView animateWithDuration:0.25 animations:^{
        super.viewContent.frame = CGRectMake(super.viewContent.frame.origin.x, 0, super.viewContent.frame.size.width, super.viewContent.frame.size.height);
    }];
    
}

- (void)closeKeyboard {
    
    [_txtPasswordLoginMulti resignFirstResponder];
    
}


#pragma mark - Buttons Actions


- (IBAction)buttonRememberMePressed:(UISwitch *)switchRemeber {
    
    LoginM.didRememberMePressed = switchRemeber.on;
    
}

- (void)buttonForgotPressed:(UIButton *)button {
    
    [self closeKeyboard];
    LoginForgotViewController *controller = [[ViewControllersFactory defaultFactory] loginForgotViewController];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (IBAction)buttonCreateAccountPressed:(UIButton *)button {
    
    [self closeKeyboard];
    LoginRegisterViewController *controller = [[ViewControllersFactory defaultFactory]loginRegisterViewController];
    [self.navigationController pushViewController:controller animated:YES];

}

- (IBAction)btnLoginMultiPressed {
    
    [self closeKeyboard];
    
    LoginM.emailLogin = self.lblEmailMulti.text;
    LoginM.passwordLogin = self.txtPasswordLoginMulti.text;
    
    NSString *errorStringMessage;
    
    if (LoginM.passwordLogin.length) {
        
        [self showHUDBlockingUI:YES];
        
        [LoginM signIn:LoginM.emailLogin password:LoginM.passwordLogin];
        [AnalyticsM.currentEvent clearFields];
        AnalyticsM.currentEvent.category = @"Login and registration";
        AnalyticsM.currentEvent.action = @"Sign in";
        AnalyticsM.currentEvent.label = @"Login screen";
        
    } else {
        errorStringMessage = LS(@"edit_text_amount_error");
    }
    
    if (errorStringMessage.length) {
        [self presentAlertWithTitle:LS(@"alert_general_error") message:errorStringMessage];
    }
}

- (IBAction)btnFacebookMultiLoginPressed:(id)sender {
    
    if (self.multiUserPageIndex < [LoginM.domainUsers count]) {
        
        self.selectedMultiUser = self.multiUserPageIndex;
    
        [self showHUDBlockingUI:YES];
       
        [[super.socialManager socialProviderById:SocialProviderTypeFacebook] aquirePermissions:^(BOOL success) {
            if (success){
                
                [FBM requestForGetFBUserDataWithToken:^(TVFacebookUser *user) {
                    [self updateFacebookUser:user];
                }];
            }
            else {
                [self hideHUD];
            }
            
            if (![AnalyticsM.currentEvent.category length]) {
                [AnalyticsM.currentEvent clearFields];
                AnalyticsM.currentEvent.category = @"Login and registration";
                AnalyticsM.currentEvent.action = @"Facebook User Sign in";
                AnalyticsM.currentEvent.label = @"Login screen";
            }
        }];
    }
}

#pragma mark -


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
