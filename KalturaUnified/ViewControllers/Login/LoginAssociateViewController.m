//
//  LoginAssociateViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 26.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginAssociateViewController.h"

@interface LoginAssociateViewController ()

@end

@implementation LoginAssociateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.deviceMode) {
        
        self.title = LS(@"assoc_login_header_add_device");
        
        self.lblAddDomainTitle.text = LS(@"assoc_login_header_add_device_title");
        self.lblAddDomainSend.text = LS(@"assoc_login_header_add_device_send");
        
    } else {
        
        self.title = LS(@"assoc_login_header_add_user");
        
        self.lblAddDomainTitle.text = LS(@"assoc_login_header_add_user_title");
        self.lblAddDomainSend.text = LS(@"assoc_login_header_add_user_send");
        
    }
    
    self.txtPasswordLoginAddDomain.placeholder = LS(@"password");
    
    [self.btnAddDomainCheckPassword setTitle:LS(@"create_account_check") forState:UIControlStateNormal];
    [Utils setButtonFont:self.btnAddDomainCheckPassword bold:YES];
    
    self.lblAddDomainTitleUser.text = LS(@"assoc_header_assoc_user");
    
    self.lblAddDomainOr.text = LS(@"create_account_or");
    //self.lblAddDomainSend.text = LS(@"assoc_login_header_assoc_user_send");
    
    [self.btnLoginAddDomain setTitle:LS(@"assoc_login_assign_account") forState:UIControlStateNormal];
    [self.btnLoginAddDomainEmail setTitle:LS(@"assoc_login_assign_account_approval") forState:UIControlStateNormal];
    
    
    TVUser *user = LoginM.domainMasterUser;
    
    [Utils setLabelFont:_lblAddDomainTitle];
    [Utils setLabelFont:_lblAddDomainTitleUser];
    
    [Utils setLabelFont:_lblAddDomainUser bold:YES];
    
    if (user) {
        _lblAddDomainUser.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
    }
    
    [self initBrandButtonUI:self.btnLoginAddDomain];
    [self initTextFieldUI:self.txtPasswordLoginAddDomain];
    self.txtPasswordLoginAddDomain.text = @"";
    
    [Utils setLabelFont:_lblAddDomainEmail bold:YES];
    
    if (user) {
        self.lblAddDomainEmail.text = [NSString stringWithFormat:@"%@", user.email];
    }
    
    [self initBrandButtonUI:self.btnLoginAddDomainEmail];
    
    [Utils setLabelFont:_lblAddDomainOr bold:YES];
    [Utils setLabelFont:_lblAddDomainSend];
    
}

#pragma mark - Buttons actions

- (IBAction)buttonCheckPressed:(UIButton *)button {
    
    BOOL wasFirstResponder;
    if ((wasFirstResponder = [_txtPasswordLoginAddDomain isFirstResponder])) {
        [_txtPasswordLoginAddDomain resignFirstResponder];
    }
    
    [_txtPasswordLoginAddDomain setSecureTextEntry:![_txtPasswordLoginAddDomain isSecureTextEntry]];
    if (wasFirstResponder) {
        [_txtPasswordLoginAddDomain becomeFirstResponder];
        NSString *str = _txtPasswordLoginAddDomain.text;
        _txtPasswordLoginAddDomain.text = [NSString stringWithFormat:@"%@ ", _txtPasswordLoginAddDomain.text];
        _txtPasswordLoginAddDomain.text = str;
    }
    
    [button setTitle:([_txtPasswordLoginAddDomain isSecureTextEntry] ? LS(@"create_account_check") : LS(@"create_account_hide")) forState:UIControlStateNormal];
    
}

- (IBAction)btnAssignToDomainPressed {
    
    [_txtPasswordLoginAddDomain resignFirstResponder];
    
    NSString *passwordRegister = self.txtPasswordLoginAddDomain.text;
    
    NSString *errorStringMessage;
    
    BOOL passwordValidInput = [Utils NSStringIsOnlyDigitsAndChars:passwordRegister];
    BOOL passwordValidLength = passwordRegister.length >= 6 && passwordRegister.length <= 16;
    
    if (passwordValidInput && passwordValidLength) {
        
        TVUser *user = LoginM.domainMasterUser;
        
        if (user) {
            
            [self showHUDBlockingUI:YES];
            
            [self.userManager requestForGetSiteGuidWithUserName:user.username password:passwordRegister completionBlock:^(id jsonResponse) {
                
                if (jsonResponse) {
                    
                    NSLog(@"requestForGetSiteGuidWithUserName result '%@' %d", jsonResponse, [jsonResponse intValue]);
                    if ([jsonResponse intValue] > 0) {
                        
                        if (self.deviceMode) {
                            
                            [super addOrActivateDeviceToDomainProcess];
                            
                        } else {
                            
                            NSString *masterUserGuid = jsonResponse;
                            
                            if (LoginM.userRegister) {
                                
                                [super.userManager requestForSignUpWithNewUser:LoginM.userRegister password:LoginM.userRegisterPassword completionBlock:^(id jsonResponse) {
                                    
                                    if (jsonResponse) {
                                        
                                        int responseStatus = [[jsonResponse objectForKey:@"m_RespStatus"] intValue];
                                        
                                        NSString * siteGUID = [[jsonResponse objectForKey:@"m_user"] objectForKey:@"m_sSiteGUID"];
                                        
                                        LoginM.userRegister.siteGUID = siteGUID;
                                        
                                        if (responseStatus == TVLoginStatusOK || responseStatus == 21) {
                                            
                                            LoginM.userAddToDomain = LoginM.userRegister;
                                            LoginM.userAddToDomainSiteGUID = LoginM.userRegister.siteGUID;
                                            
                                            [self addUserToDomainProcess:masterUserGuid];
                                            
                                        } else {
                                            
                                            [self showDomainErrorMessage];
                                            
                                        }
                                        
                                    } else {
                                        
                                        [self showDomainErrorMessage];
                                        
                                    }
                                    
                                }];
                                
                            } else {
                                
                                
                                 NSString *facebookToken = [[FBSession.activeSession accessTokenData] accessToken];
                                 
                                 [FBM requestForFBUserRegisterWithToken:facebookToken completionBlock:^(id jsonResponse) {
                                 
                                     if (jsonResponse) {
                                 
                                         LoginM.facebookUser = [[TVFacebookUser alloc] initWithDictionary:[jsonResponse dictionaryByRemovingNSNulls]];
                                         
                                         NSLog(@"requestForFBUserRegisterWithToken result '%@'", jsonResponse);
                                         
                                         LoginM.userAddToDomainSiteGUID = LoginM.facebookUser.siteGuid;
                                         
                                         [self addUserToDomainProcess:masterUserGuid];
                                         
                                     } else {
                                         
                                         [self showDomainErrorMessage];
                                         
                                     }
                                     
                                 }];
                                
                                
                            }
                            
                        }
                        
                    } else {
                        
                        [self hideHUD];
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:LS(@"assoc_login_reenter_password") delegate:nil cancelButtonTitle:[LS(@"ok") uppercaseString] otherButtonTitles:nil];
                        [alert show];
                        
                    }
                    
                } else {
                    
                    [self showDomainErrorMessage];
                }
                
                
            }];
            
        }
        
        
    } else {
        [self.txtPasswordLoginAddDomain becomeFirstResponder];
        errorStringMessage = LS(@"edit_text_amount_error");
    }
    
    if (errorStringMessage.length) {
        [self presentAlertWithTitle:LS(@"alert_general_error") message:errorStringMessage];
    }
    
}

- (void)addUserToDomainProcess:(NSString *)masterUserGuid {
    
    [self.userManager requestForAddUserToDomainWithDomainId:[[[NSUserDefaults standardUserDefaults] objectForKey:@"DomainID"] integerValue] userGuid:LoginM.userAddToDomainSiteGUID masteUserGuid:masterUserGuid completionBlock:^(id jsonResponse) {
        
        if (jsonResponse) {
            
            NSLog(@"requestForAddUserToDomainIsMaster result '%@'", jsonResponse);
            // m_oDomainResponseStatus
            TVDomain * domain = [[TVDomain alloc] initWithDictionary:[jsonResponse objectForKey:@"m_oDomain"]];
            
            NSInteger usersLimit = domain.userLimit;
            NSInteger currentUsersNumber = domain.userIDs.count;
            NSInteger remaninigCount = usersLimit-currentUsersNumber;

            if (domain && [[jsonResponse objectForKey:@"m_oDomainResponseStatus"] intValue] == kDomainResponseStatus_OK) {
                
                [self hideHUD];


                NSString * text =  [NSString stringWithFormat: LS(@"assoc_login_register_domain_text"),remaninigCount];

                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(@"assoc_login_register_domain_title")
                                                                message:text
                                                               delegate:self
                                                      cancelButtonTitle:LS(@"goto_login")
                                                      otherButtonTitles:nil];
                alert.tag = 100;
                [alert show];
                
            } else {



                if ([[jsonResponse objectForKey:@"m_oDomainResponseStatus"] intValue] == kDomainResponseStatus_ExceededUserLimit)
                {
                    [self hideHUD];

                    NSString * text = [NSString stringWithFormat:LS(@"assoc_login_register_domain_exceeded_limit_message"),usersLimit];
                    [self presentAlertWithTitle:LS(@"assoc_login_register_domain_title") message:text];

                }
                else
                {
                    [self showDomainErrorMessage];
                }

            }

        } else {
            
            [self showDomainErrorMessage];
            
        }
        
    }];
    
    
}

- (IBAction)btnEmailAddToDomainPressed {
    
    TVUser *user = LoginM.domainMasterUser;
    
    if (user) {
        
        [self showHUDBlockingUI:YES];
        
        NSLog(@"userAddToDomainSiteGUID %@", LoginM.userAddToDomainSiteGUID);
        [TVSessionManager sharedTVSessionManager].sharedInitObject.siteGUID = LoginM.userAddToDomainSiteGUID;
        
        if (self.deviceMode) {
            
            [super.deviceManager requestForSubmitAddDeviceToDomainRequestWithDeviceName:[UIDevice currentDevice].name deviceBrandID:(isPad ? kDeviceBrand_iPad : kDeviceBrand_iPhone) completionBlock:^(id jsonResponse) {
                
                if (jsonResponse) {
                    
                    NSLog(@"requestForSubmitAddDeviceToDomainRequestWithDeviceName result '%@'", jsonResponse );
                    
                    if ([[jsonResponse objectForKey:@"m_oDomainResponseStatus"] intValue] == kDomainResponseStatus_RequestSent) {
                        
                        [self hideHUD];
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:LS(@"assoc_login_email_sent") delegate:self cancelButtonTitle:LS(@"goto_login") otherButtonTitles:nil];
                        alert.tag = 100;
                        [alert show];
                        
                    } else {
                        
                        [LoginM fullLogout];
                        
                        [self showDomainErrorMessage];
                        
                    }

                } else {
                    
                    [LoginM fullLogout];
                    
                    [self showDomainErrorMessage];
                    
                }
                
                
            }];
            
            
        } else {
            
            if (LoginM.userRegister) {
                
                [super.userManager requestForSignUpWithNewUser:LoginM.userRegister password:LoginM.userRegisterPassword completionBlock:^(id jsonResponse) {
                    
                    if (jsonResponse) {
                        
                        NSLog(@"requestForSignUpWithNewUser result '%@'\n%@", jsonResponse, [TVSessionManager sharedTVSessionManager].currentUser.siteGUID);
                        
                        int responseStatus = [[jsonResponse objectForKey:@"m_RespStatus"] intValue];
                        
                        NSString * siteGUID = [[jsonResponse objectForKey:@"m_user"] objectForKey:@"m_sSiteGUID"];
                        
                        LoginM.userRegister.siteGUID = siteGUID;
                        
                        if (responseStatus == TVLoginStatusOK || responseStatus == 21) {
                            
                            [self performSelector:@selector(submitAddUserToDomainWithMasterUsername:) withObject:user.username afterDelay:0.5];
                            
                        } else {
                            
                            [self showDomainErrorMessage];
                        }
                        
                    }  else {
                        
                        [self showDomainErrorMessage];
                    }
                }];
                
            } else {
                
                NSString *facebookToken = [[FBSession.activeSession accessTokenData] accessToken];
                
                [FBM requestForFBUserRegisterWithToken:facebookToken completionBlock:^(id jsonResponse) {
                    
                    if (jsonResponse) {
                        
                        LoginM.facebookUser = [[TVFacebookUser alloc] initWithDictionary:[jsonResponse dictionaryByRemovingNSNulls]];
                        
                        NSLog(@"requestForFBUserRegisterWithToken result '%@'", jsonResponse);
                        
                        LoginM.userAddToDomainSiteGUID = LoginM.facebookUser.siteGuid;
                        
                        [self performSelector:@selector(submitAddUserToDomainWithMasterUsername:) withObject:user.username afterDelay:0.5];
                        

                    } else {
                        
                        [self showDomainErrorMessage];
                    }
                }];
            }
        }
    }
}

- (void)submitAddUserToDomainWithMasterUsername:(NSString *)username {
    
    if (LoginM.userRegister) {
        
        LoginM.userAddToDomain = LoginM.userRegister;
        LoginM.userAddToDomainSiteGUID = LoginM.userRegister.siteGUID;
        
        [TVSessionManager sharedTVSessionManager].sharedInitObject.siteGUID = LoginM.userRegister.siteGUID;
        
    } else {
        
        [TVSessionManager sharedTVSessionManager].sharedInitObject.siteGUID = LoginM.facebookUser.siteGuid;
        
    }
    
    [super.userManager requestForSubmitAddUserToDomainWithMasterUsername:username completionBlock:^(id jsonResponse) {
        
        if (jsonResponse) {
            
            NSLog(@"requestForSubmitAddUserToDomainWithMasterUsername result '%@'\n%@", jsonResponse, [TVSessionManager sharedTVSessionManager].currentUser);
            
            if ([[jsonResponse objectForKey:@"m_oDomainResponseStatus"] intValue] == kDomainResponseStatus_RequestSent) {
                
                [self hideHUD];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:LS(@"assoc_login_email_sent") delegate:self cancelButtonTitle:LS(@"goto_login") otherButtonTitles:nil];
                alert.tag = 100;
                [alert show];
                
            } else {
                
                [self showDomainErrorMessage];
                
            }
            
        } else {
            
            [self showDomainErrorMessage];
        
        }
        
    }];
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if ((alertView.tag == 100) && (buttonIndex == 0)) {
        
        [self showHUDBlockingUI:NO];
        [self.userManager loadDomainUsers:^{
            
            [self hideHUD];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
       
    }
    
}

- (void)showDomainErrorMessage {
    
    [self hideHUD];
    
    [self presentAlertWithTitle:LS(@"assoc_login_register_domain_title") message:LS(@"assoc_login_register_domain_failed_message")];
    
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
 
    [textField resignFirstResponder];

    return YES;
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
