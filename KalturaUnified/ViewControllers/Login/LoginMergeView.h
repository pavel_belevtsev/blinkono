//
//  LoginMergeView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginBaseViewController.h"
#import "ActivityView.h"

@interface LoginMergeView : UINibView {
    
    IBOutlet UITextField *textFieldPassword;

    IBOutlet UILabel *labelText;
    IBOutlet UILabel *labelPassword;
    
    IBOutlet UIButton *mergeButton;
    
    IBOutlet UIView *viewToolbar;
    IBOutlet UIButton *cancelButton;
    
    IBOutlet UIView *viewContent;
    
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *titleSubLabel;
    
    IBOutlet UILabel *accountOneNameLabel;
    IBOutlet UILabel *accountTwoNameLabel;
 
    IBOutlet UIImageView *accountOneImage;
    IBOutlet UIImageView *accountTwoImage;
    
    IBOutlet UIButton *buttonCheck;
    
    TVFacebookUser * facebookUser;
    NSString *accessToken;
    
    float viewContentY;
    
}

- (IBAction) buttonCancelPressed:(UIButton *)button;
- (IBAction) buttonMergePressed:(UIButton *)button;
- (void)setFBDetails;


@property (nonatomic, strong) TVFacebookUser * facebookUser;
@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, weak) LoginBaseViewController *delegate;

@end
