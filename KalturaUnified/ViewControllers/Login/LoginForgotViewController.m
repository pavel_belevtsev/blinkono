//
//  LoginForgotViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 08.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "LoginForgotViewController.h"
#import "LoginInfoViewController.h"
#import "AnalyticsManager.h"

@interface LoginForgotViewController ()

@end

@implementation LoginForgotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = LS(@"login_forgot_password");
    
    _labelText.text = LS(@"forgot_pass_content");
    _labelEmail.text = LS(@"forgot_pass_email_hint");

    [_buttonSend setTitle:[NSString stringWithFormat:LS(@"forgot_password_btn_send"), APST.brandName] forState:UIControlStateNormal];
    [self initBrandButtonUI:_buttonSend];
    
    
    [Utils setLabelFont:_labelEmail bold:YES];
    
    [Utils setTextFieldFont:_textFieldEmail bold:YES];
    
    [Utils setLabelFont:_labelText];
    [AnalyticsM trackScreen:@"Forgot password"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark TextField

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger textLength = [textField.text length] - range.length + [string length];
    
    _labelEmail.hidden = (textLength > 0);
    
    return YES;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
    
}

- (void)closeKeyboard {
    
    [_textFieldEmail resignFirstResponder];
    
}

#pragma mark -
#pragma mark - Buttons Actions

- (IBAction)buttonSendPressed:(UIButton *)button {
    
    [self closeKeyboard];
    
    if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    @try
    {
        if (_textFieldEmail.text.length > 0)
        {
            
            __weak TVPAPIRequest *request = [TVPSiteAPI requestForSendNewPassword:_textFieldEmail.text delegate:nil];
            
            [request setCompletionBlock:^{
                 
                if ([[request JSONResponse] intValue] == 0) {
                    
                    [self presentAlertWithTitle:LS(@"forgot_pass_wrong_username_error") message:nil];
                    
                } else {
                    
                    [self presentAlertWithTitle:LS(@"forgot_pass_email_sent_successfully") message:nil];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }
            }];
            
            [request setFailedBlock:^{
                
                [self presentAlertWithTitle:LS(@"edit_FailedChangePassword") message:nil];

            }];
            
            [self sendRequest:request];
            
        }
        else
        {
            [NSException raise:NSInvalidArgumentException format:LS(@"forgot_pass_invalid_username_error"), nil];
        }
    }
    @catch (NSException *exception)
    {
        [self presentAlertWithTitle:@"" message:exception.description];
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
