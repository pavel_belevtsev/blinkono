//
//  LoginNavigationController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 19.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoginViewController;

@interface LoginNavigationController : UINavigationController

@property (nonatomic, weak) LoginViewController *loginViewController;

@end
