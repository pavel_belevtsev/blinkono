//
//  CategorySortTableViewCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 19.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CategorySortTableViewCell.h"

@implementation CategorySortTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.defaultTextColor = [UIColor lightGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    _labelSort.textColor = (selected || _cellSelected) ? APST.brandColor : self.defaultTextColor;

}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    _labelSort.textColor = (highlighted || _cellSelected) ? APST.brandColor : self.defaultTextColor;
    
}

@end
