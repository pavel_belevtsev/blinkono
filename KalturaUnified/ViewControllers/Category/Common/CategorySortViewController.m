//
//  CategorySortViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 17.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CategorySortViewController.h"
#import "CategorySortTableViewCell.h"

@interface CategorySortViewController ()

@end

@implementation CategorySortViewController

-(void)updateSortArrayAndReloadData:(NSArray*)sortArray
{
    self.sortByArray =  [NSArray arrayWithArray:sortArray];    
    
    NSInteger h = sortRowHeight * [sortArray count];
    
    CGRect frame = self.tableViewSort.frame;
    frame.size.height = h;
    
    self.tableViewSort.frame = frame;
    [self.tableViewSort reloadData];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewContent.layer.cornerRadius = 6;
    self.viewContent.layer.masksToBounds = YES;

    self.tableViewSort.layer.cornerRadius = 6;
    self.tableViewSort.layer.masksToBounds = YES;
    
    self.tableViewFilter.layer.cornerRadius = 6;
    self.tableViewFilter.layer.masksToBounds = YES;
    
    _labelSortBy.text = [LS(@"category_page_sort_by") uppercaseString];

    self.sortByArray = @[@{@"title":LS(@"category_page_newest"), @"sortBy":@(TVOrderByDateAdded)},
                         @{@"title":LS(@"category_page_most_popular"), @"sortBy":@(TVOrderByNumberOfViews)},
                         @{@"title":LS(@"category_page_most_liked"), @"sortBy":@(TVOrderByRating)}];
    
    _labelFilterBy.text = [LS(@"browse") uppercaseString];
    
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    if (maxFilterTableHeight == 0) {
        maxFilterTableHeight = _tableViewFilter.frame.size.height;
    }
    
    [self updateWithMenuItem:_menuItem orderBy:_orderBy filters:_filters];
    
    
}

- (void)updateWithMenuItem:(TVMenuItem *)menuItem orderBy:(TVOrderBy)orderBy filters:(NSArray *)filters {
    
    _menuItem = menuItem;
    _orderBy = orderBy;
    _filters = filters;
    
    [_tableViewSort reloadData];
    
    if ([filters count]) {
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [array addObject:LS(@"all_filters")];
        [array addObject:[_filters lastObject]];
        
        _filterByArray = array;

    } else {
    
        _filterByArray = _menuItem.children;
        
    }
    
    _labelFilterBy.hidden = ![_filterByArray count];

    [_tableViewFilter reloadData];
    
    CGFloat height = [_filterByArray count] * sortRowHeight;
    if (height > maxFilterTableHeight) height = maxFilterTableHeight;
    
    [UIView animateWithDuration:0.3 animations:^{
        _tableViewFilter.frame = CGRectMake(_tableViewFilter.frame.origin.x, _tableViewFilter.frame.origin.y, _tableViewFilter.frame.size.width, height);
    }];
    
}

#pragma mark - UITableViewCell

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == _tableViewSort) {
        return [_sortByArray count];
    } else {
        return [_filterByArray count];
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [cell setBackgroundColor:[UIColor clearColor]];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CategorySortTableViewCell";
    
    CategorySortTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:tableView options:nil] objectAtIndex:0];
    }
    
    if (tableView == _tableViewSort) {
        
        NSDictionary *item = [_sortByArray objectAtIndex:indexPath.row];
        
        cell.labelSort.text = [item objectForKey:@"title"];
        int sortBy = [[item objectForKey:@"sortBy"] intValue];
        
        cell.cellSelected = (sortBy == _orderBy);
        
    } else {
        
        cell.selected = NO;
        
        TVMenuItem *menuItem = [_filterByArray objectAtIndex:indexPath.row];
        
        if ([menuItem isKindOfClass:[TVMenuItem class]]) {
            
            cell.labelSort.text = menuItem.name;
            
            if ([_filters count] && [menuItem isEqual:[_filters lastObject]]) {
                cell.cellSelected = YES;
                
            }
            
        } else {
            
            cell.labelSort.text = (NSString *)menuItem;
            cell.cellSelected = NO;
        }
        
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == _tableViewSort) {
    
        NSDictionary *item = [_sortByArray objectAtIndex:indexPath.row];
        
        _orderBy = [[item objectForKey:@"sortBy"] intValue];
        
        [tableView reloadData];
        
        [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
        
        [_delegate selectSortOption:_orderBy title:[item objectForKey:@"title"]];
        
    } else {
        
        TVMenuItem *item = [_filterByArray objectAtIndex:indexPath.row];
        
        if ([item isKindOfClass:[TVMenuItem class]]) {
            
            if (![item.children count]) {
                
                [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
                
            }
            
            [_delegate selectFilterOption:item];
            
        } else {
            
            [_delegate selectFilterBack];
            
        }
        
    }
    
}


#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
