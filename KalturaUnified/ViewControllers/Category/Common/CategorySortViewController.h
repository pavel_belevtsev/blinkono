//
//  CategorySortViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 17.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CategorySortViewControllerDelegate <NSObject>

- (void)selectSortOption:(TVOrderBy)orderBy title:(NSString *)title;
- (void)selectFilterOption:(TVMenuItem *)filterItem;
- (void)selectFilterBack;

@end

@interface CategorySortViewController : UIViewController {
    
    CGFloat maxFilterTableHeight;
    
}


@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (weak, nonatomic) IBOutlet UILabel *labelSortBy;
@property (weak, nonatomic) IBOutlet UITableView *tableViewSort;

@property (weak, nonatomic) IBOutlet UILabel *labelFilterBy;
@property (weak, nonatomic) IBOutlet UITableView *tableViewFilter;

@property (strong, nonatomic) NSArray *sortByArray;
@property (strong, nonatomic) NSArray *filterByArray;

@property (nonatomic, weak) id <CategorySortViewControllerDelegate> delegate;

@property (strong, nonatomic) TVMenuItem *menuItem;
@property (nonatomic) TVOrderBy orderBy;
@property (strong, nonatomic) NSArray *filters;

- (void)updateWithMenuItem:(TVMenuItem *)menuItem orderBy:(TVOrderBy)orderBy filters:(NSArray *)filters;
-(void)updateSortArrayAndReloadData:(NSArray*)sortArray;

@end
