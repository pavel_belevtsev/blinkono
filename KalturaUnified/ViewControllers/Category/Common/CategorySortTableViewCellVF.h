//
//  CategorySortTableViewCellVF.h
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CategorySortTableViewCell.h"

@interface CategorySortTableViewCellVF : CategorySortTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewIsSelectedMark;
@property (strong, nonatomic) UIColor* defaultTextColor;

@end
