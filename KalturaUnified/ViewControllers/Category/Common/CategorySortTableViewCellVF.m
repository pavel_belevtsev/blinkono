//
//  CategorySortTableViewCellVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CategorySortTableViewCellVF.h"

@implementation CategorySortTableViewCellVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    self.defaultTextColor = [UIColor whiteColor];

    // set seperator color between cells:
    self.viewDivider.backgroundColor = [VFTypography instance].grayScale1Color;
    self.labelSort.textColor = [UIColor whiteColor];
    
    self.contentView.backgroundColor = [VFTypography instance].darkGrayColor;
    
    [Utils setLabelFont: self.labelSort bold:YES];
}

/* "PSC : Vodafone Grup : change selected to V (not BG color) : JIRA ticket. */

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    // override the colors that were set in super's setHighlighted
    self.labelSort.textColor = self.defaultTextColor;
    // show/hide the "√" icon
    self.imageViewIsSelectedMark.hidden = !(highlighted || self.cellSelected);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    self.labelSort.textColor = (selected || self.cellSelected) ?  self.defaultTextColor : self.defaultTextColor;
    self.imageViewIsSelectedMark.hidden = !(selected || self.cellSelected);

}
@end
