//
//  CategorySortTableViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 19.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategorySortTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelSort;
@property (weak, nonatomic) IBOutlet UIView *viewDivider;

@property BOOL cellSelected;
@property (strong, nonatomic) UIColor* defaultTextColor;

@end
