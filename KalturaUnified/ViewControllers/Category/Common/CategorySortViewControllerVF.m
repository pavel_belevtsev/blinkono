//
//  CategorySortViewControllerVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CategorySortViewControllerVF.h"
#import "CategorySortTableViewCellVF.h"

@interface CategorySortViewControllerVF ()

@end

@implementation CategorySortViewControllerVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.labelSortBy.textColor = [VFTypography instance].grayScale7Color;
    self.labelFilterBy.textColor = [VFTypography instance].grayScale7Color;
    [Utils setLabelFont:self.labelSortBy bold:YES];
    [Utils setLabelFont:self.labelFilterBy bold:YES];

    self.viewContent.backgroundColor = [VFTypography instance].grayScale1Color;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/* "PSC : Vodafone Grup : change selected to V (not BG color) : JIRA ticket. */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CategorySortTableViewCellVF";
    
    CategorySortTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:tableView options:nil] objectAtIndex:0];
    }
    
    if (tableView == self.tableViewSort) {
        
        NSDictionary *item = [self.sortByArray objectAtIndex:indexPath.row];
        
        cell.labelSort.text = [item objectForKey:@"title"];
        int sortBy = [[item objectForKey:@"sortBy"] intValue];
        
        cell.cellSelected = (sortBy == self.orderBy);
        
    } else {
        
        cell.selected = NO;
        
        TVMenuItem *menuItem = [self.filterByArray objectAtIndex:indexPath.row];
        
        if ([menuItem isKindOfClass:[TVMenuItem class]]) {
            
            cell.labelSort.text = menuItem.name;
            
            if ([self.filters count] && [menuItem isEqual:[self.filters lastObject]]) {
                cell.cellSelected = YES;
                
            }
            
        } else {
            
            cell.labelSort.text = (NSString *)menuItem;
            
        }
        
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    // set the text color in the tables to be GS 6
    CategorySortTableViewCellVF* vfCell = (CategorySortTableViewCellVF *) cell;
    if(vfCell){
        vfCell.defaultTextColor = [VFTypography instance].grayScale6Color;
    }
    return cell;
    
}

@end
