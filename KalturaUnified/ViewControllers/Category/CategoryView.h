//
//  CategoryView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"
#import "CategoryViewController.h"
#import "TVMediaItem+PSTags.h"

@interface CategoryView : UINibView

@property (nonatomic, weak) CategoryViewController *delegate;

- (void)reloadCategoryData;
- (void)updateCategoryData;
- (void)reloadCollectionView;

@end
