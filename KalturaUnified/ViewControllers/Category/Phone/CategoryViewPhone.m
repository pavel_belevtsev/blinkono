//
//  CategoryViewPhone.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 19.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CategoryViewPhone.h"
#import "CategoryItemCollectionViewCell.h"
#import "MediaItemView.h"

const NSInteger categoryViewPhoneItemsCount = 40;

@implementation CategoryViewPhone

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [_collectionViewCategory registerClass:[CategoryItemCollectionViewCell class] forCellWithReuseIdentifier:@"CategoryItemCollectionViewCell2_3"];
    [_collectionViewCategory registerClass:[CategoryItemCollectionViewCell class] forCellWithReuseIdentifier:@"CategoryItemCollectionViewCell16_9"];
    
}

- (void)reloadCollectionView {
    [_collectionViewCategory reloadData];
}

- (void)reloadCategoryData {
    
    loadedPages = 0;
    numberOfPages = 0;

    [_collectionViewCategory reloadData];
    
    [self showHUD];
    [super.delegate loadCategoryData:categoryViewPhoneItemsCount pageIndex:0];
    
}

- (void)updateCategoryData {
    
    [self hideHUD];
    
    if (numberOfPages == 0) _collectionViewCategory.alpha = 0.0;
    NSInteger count = super.delegate.categoryMediaListCount;
    numberOfPages = (count / categoryViewPhoneItemsCount) + ((count % categoryViewPhoneItemsCount) ? 1 : 0);
    
    count = [super.delegate.categoryMediaList count];
    loadedPages = (count / categoryViewPhoneItemsCount) + ((count % categoryViewPhoneItemsCount) ? 1 : 0);
    
    [_collectionViewCategory reloadData];
    [_collectionViewCategory layoutIfNeeded];
    
    
    if (_collectionViewCategory.alpha == 0.0) [self performSelector:@selector(movingCollectionViewDownAnimation) withObject:nil afterDelay:0.01];

}


- (void)movingCollectionViewDownAnimation {
    
    _collectionViewCategory.alpha = 1.0;
    
    for (CategoryItemCollectionViewCell *cell in _collectionViewCategory.visibleCells) {
        NSIndexPath *indexPath = [_collectionViewCategory indexPathForCell:cell];
        
        cell.alpha = 0.0;
        
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y - 50.0, cell.frame.size.width, cell.frame.size.height);
        
        NSInteger index = indexPath.row;
        
        [UIView animateWithDuration:0.2 delay:(0.15 * index) options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            cell.alpha = 1.0;
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y + 50.0, cell.frame.size.width, cell.frame.size.height);
            
        } completion:^(BOOL finished) {
            
        }];
        //NSLog(@"%d %d  %@", indexPath.section, indexPath.row, cell.mediaItemView.mediaItem.name);
    }
}

#pragma mark - Collection View Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) {
        return CGSizeMake(148, 256);
    } else {
        return CGSizeMake(304, 220);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) {
        return UIEdgeInsetsMake(4, 8, 0, 8);
    } else {
        return UIEdgeInsetsMake(8, 8, 0, 8);
    }
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) {
        return 4;
    } else {
        return -4;
    }
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [super.delegate.categoryMediaList count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"CategoryItemCollectionViewCell2_3";
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_16_9) {
        identifier = @"CategoryItemCollectionViewCell16_9";
    }
    
    CategoryItemCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cellView.mediaItemView.delegateController = self.delegate;
    
    NSInteger index = indexPath.row;
    
    if (index < [super.delegate.categoryMediaList count]) {
        
        [cellView updateCellWithMediaItem:[super.delegate.categoryMediaList objectAtIndex:index]];
        
    } else {
        
        [cellView clearMediaCell];
        
    }
    
    return cellView;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

#pragma mark -

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if ((loadedPages > 0) && (loadedPages < numberOfPages)) {
        
        NSInteger categoryViewPhoneItemHeight = (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) ? 260 : 216;
        int divider = (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) ? 1 : 1;
        
        CGFloat pageHeight = categoryViewPhoneItemsCount * categoryViewPhoneItemHeight / divider;
        if (scrollView.contentOffset.y > ((loadedPages - 1) * pageHeight + (pageHeight / 2))) {
            
            [super.delegate loadCategoryData:categoryViewPhoneItemsCount pageIndex:loadedPages++];
            
        }
    }
    
}

@end
