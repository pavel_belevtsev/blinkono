//
//  CategoryViewPhone.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 19.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CategoryView.h"

@interface CategoryViewPhone : CategoryView {
    
    NSInteger loadedPages;
    NSInteger numberOfPages;
    
}

@property (nonatomic, weak) IBOutlet UICollectionView *collectionViewCategory;


@end
