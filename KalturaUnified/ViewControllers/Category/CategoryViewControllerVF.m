//
//  CategoryViewControllerVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 2/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CategoryViewControllerVF.h"

@interface CategoryViewControllerVF ()

@end

@implementation CategoryViewControllerVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.labelSortOpen.backgroundColor = [VFTypography instance].grayScale2Color;
    self.labelSortOpen.textColor = [VFTypography instance].grayScale6Color;
    
}
@end
