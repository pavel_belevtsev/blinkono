//
//  CategoryView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CategoryView.h"

@implementation CategoryView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)reloadCategoryData {
    
}

- (void)updateCategoryData {
    
}

- (void)reloadCollectionView {
    
}

@end
