//
//  CategoryItemCollectionViewCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CategoryItemCollectionViewCell.h"
#import "MediaItemView.h"

@implementation CategoryItemCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        
        self.mediaItemView = [[MediaItemView alloc] initWithNib];
        _mediaItemView.frame = self.bounds;
        [self addSubview:_mediaItemView];
        
    }
    return self;
}

- (void)updateCellWithMediaItem:(TVMediaItem *)mediaItem {
    
    _mediaItemView.hidden = NO;
    [_mediaItemView updateData:mediaItem];
    
}

- (void)clearMediaCell {
    
    _mediaItemView.hidden = YES;
    
}

- (void)setEpisodeNumber:(int)number {
    
    [_mediaItemView setEpisodeNumber:number];
    
    
}

@end
