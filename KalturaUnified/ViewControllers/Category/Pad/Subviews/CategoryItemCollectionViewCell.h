//
//  CategoryItemCollectionViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVMediaItem+PSTags.h"

@class MediaItemView;

@interface CategoryItemCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) MediaItemView *mediaItemView;

- (void)updateCellWithMediaItem:(TVMediaItem *)mediaItem;
- (void)clearMediaCell;
- (void)setEpisodeNumber:(int)number;

@end
