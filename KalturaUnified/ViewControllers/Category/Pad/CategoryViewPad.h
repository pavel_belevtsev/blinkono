//
//  CategoryViewPad.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryView.h"
#import "PageControlView.h"

@interface CategoryViewPad : CategoryView {
    
    NSInteger loadedPages;
    
}

@property (nonatomic, weak) IBOutlet UICollectionView *collectionViewCategory;
@property (nonatomic, weak) IBOutlet UIView *viewNavigation;
@property (nonatomic, strong) PageControlView *viewPageControl;

@end
