//
//  CategoryViewPad.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CategoryViewPad.h"
#import "CategoryItemCollectionViewCell.h"
#import "MediaItemView.h"

const NSInteger categoryViewPadRowsCount2_3 = 2;
const NSInteger categoryViewPadColsCount2_3 = 4;
const NSInteger categoryViewPadRowsCount16_9 = 3;
const NSInteger categoryViewPadColsCount16_9 = 3;


@implementation CategoryViewPad

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self addingPagerView];
        
    [_collectionViewCategory registerClass:[CategoryItemCollectionViewCell class] forCellWithReuseIdentifier:@"CategoryItemCollectionViewCell2_3"];
    [_collectionViewCategory registerClass:[CategoryItemCollectionViewCell class] forCellWithReuseIdentifier:@"CategoryItemCollectionViewCell16_9"];

}

- (void)addingPagerView {
    CGRect pageControlFrame = _viewNavigation.bounds;
    self.viewPageControl = [[PageControlView alloc] initWithFrame:pageControlFrame];
    [self.viewPageControl.control addTarget:self action:@selector(pageControlChanged:) forControlEvents:UIControlEventValueChanged];
    self.viewPageControl.numberOfPages = 0;
    [_viewNavigation addSubview:self.viewPageControl];
}

- (NSInteger)rowsCount {
    
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) {
        return categoryViewPadRowsCount2_3;
    } else {
        return categoryViewPadRowsCount16_9;
    }
    
}

- (NSInteger)colsCount {
    
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) {
        return categoryViewPadColsCount2_3;
    } else {
        return categoryViewPadColsCount16_9;
    }
    
}

- (NSInteger)itemsCount {

    return ([self rowsCount] * [self colsCount]);
    
}

- (void)reloadCollectionView {
    [_collectionViewCategory reloadData];
}

- (void)reloadCategoryData {
    
    loadedPages = 0;
    self.viewPageControl.currentPage = 0;
    self.viewPageControl.numberOfPages = 0;
    
    [_collectionViewCategory reloadData];
    
    [self showHUD];
    [super.delegate loadCategoryData:([self itemsCount] * 3) pageIndex:0];
}

- (void)updateCategoryData {
    
    [self hideHUD];
    
    if (_viewPageControl.numberOfPages == 0) {
        
        NSInteger itemsCount = [self itemsCount];
        
        NSInteger count = super.delegate.categoryMediaListCount;
        self.viewPageControl.numberOfPages = (count / itemsCount) + ((count % itemsCount) ? 1 : 0);
        
        count = [super.delegate.categoryMediaList count];
        loadedPages = (count / itemsCount) + ((count % itemsCount) ? 1 : 0);
        
        _collectionViewCategory.alpha = 0.0;
        [self performSelector:@selector(movingCollectionViewDownAnimation) withObject:nil afterDelay:0.01];

    }
    
    [_collectionViewCategory reloadData];
    [_collectionViewCategory layoutIfNeeded];
}

- (void)movingCollectionViewDownAnimation {
    
    _collectionViewCategory.alpha = 1.0;
    
    for (CategoryItemCollectionViewCell *cell in _collectionViewCategory.visibleCells) {
        NSIndexPath *indexPath = [_collectionViewCategory indexPathForCell:cell];
        
        cell.alpha = 0.0;
        
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y - 50.0, cell.frame.size.width, cell.frame.size.height);
        
        NSInteger index = indexPath.row;
        index = (index % [self rowsCount]) * [self colsCount] + (index / [self rowsCount]);
    
        /*
        if (index % [self rowsCount] == 0) {
            index = index / [self rowsCount];
        } else {
            index = index / [self rowsCount];
            index += [self colsCount];
        }
         */
        
        [UIView animateWithDuration:0.2 delay:(0.15 * index) options:UIViewAnimationOptionCurveEaseIn animations:^{
        
            cell.alpha = 1.0;
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y + 50.0, cell.frame.size.width, cell.frame.size.height);
        
            
        } completion:^(BOOL finished) {
            
        }];
        
        //NSLog(@"%d %d  %@", indexPath.section, indexPath.row, cell.mediaItemView.mediaItem.name);
    }
}

#pragma mark - Collection View Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return loadedPages;//self.viewPageControl.numberOfPages;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self itemsCount];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) {
        return CGSizeMake(180, 306);
    } else {
        return CGSizeMake(288, 208);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) {
        return UIEdgeInsetsMake(24, 56, 4, 56); // top left bottom right
    } else {
        return UIEdgeInsetsMake(24, 48, 0, 48);
    }
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) {
        return 64;
    } else {
        return 32;
    }
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_2_3) {
        return 10;
    } else {
        return 0;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"CategoryItemCollectionViewCell2_3";
    if (super.delegate.menuCategoryTypeSize == MenuCategoryTypeSize_16_9) {
        identifier = @"CategoryItemCollectionViewCell16_9";
    }
    
    CategoryItemCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cellView.mediaItemView.delegateController = self.delegate;
    
    NSInteger index = indexPath.row;
    index = (index % [self rowsCount]) * [self colsCount] + (index / [self rowsCount]);
    /*
    if (index % [self rowsCount] == 0) {
        index = index / [self rowsCount];
    } else {
        index = index / [self rowsCount];
        index += [self colsCount];
    }
    */
    
   // NSLog(@"index %d = %d", indexPath.row, index);
    
    index += (indexPath.section * [self itemsCount]);
    
    if (index < [super.delegate.categoryMediaList count]) {
        
        [cellView updateCellWithMediaItem:[super.delegate.categoryMediaList objectAtIndex:index]];
        //NSLog(((TVMediaItem *)[super.delegate.categoryMediaList objectAtIndex:index]).name);
        
    } else {
        
        [cellView clearMediaCell];
        
    }
    return cellView;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark -

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = CGRectGetWidth(scrollView.frame);
    int page = scrollView.contentOffset.x / pageWidth;
    [self.viewPageControl setCurrentPage:page];
    
    if (loadedPages < self.viewPageControl.numberOfPages) {
        if (page == loadedPages - 2) {
            [super.delegate loadCategoryData:[self itemsCount] pageIndex:loadedPages++];
        }
    }
}

- (void)pageControlChanged:(id)sender {
    UIPageControl *pageControl = sender;
    CGFloat pageWidth = _collectionViewCategory.frame.size.width;
    CGPoint scrollTo = CGPointMake(pageWidth * pageControl.currentPage, 0);
    [_collectionViewCategory setContentOffset:scrollTo animated:YES];
}

@end
