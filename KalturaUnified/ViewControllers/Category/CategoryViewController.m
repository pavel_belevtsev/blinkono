//
//  CategoryViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CategoryViewController.h"
#import "CategoryView.h"
#import "CategorySortViewController.h"
#import <TvinciSDK/TVRental.h>
#import "AnalyticsManager.h"
#import "EUPermittedItems.h"

@interface CategoryViewController ()

@end

const NSInteger categoryViewPadMaxItemsCount2_3 = 96;
const NSInteger categoryViewPadMaxItemsCount16_9 = 108;

@implementation CategoryViewController

- (void)viewDidLoad {
    
    self.loadNoInternetView = YES;

    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    [Utils setLabelFont:_labelTitle bold:YES];
    
    if (_viewNoRelatedItems) {
        _viewNoRelatedItems.hidden = YES;
    }
    
    self.labelAutomation.isAccessibilityElement = YES;
    self.labelAutomation.accessibilityLabel = @"Label Automation";
    self.labelAutomation.accessibilityValue = @"";

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    super.panModeDefault = MFSideMenuPanModeDefault;
    [self menuContainerViewController].panMode = (MFSideMenuPanMode)super.panModeDefault;
    
    CategorySortViewController *sortController = [[ViewControllersFactory defaultFactory] categorySortViewController];
    sortController.delegate = self;
    sortController.menuItem = _categoryMenuItem;
    sortController.orderBy = _orderBy;
    sortController.filters = _filterMenuItemList;
    
    [sortController updateWithMenuItem:_categoryMenuItem orderBy:_orderBy filters:_filterMenuItemList];
    
    [self menuContainerViewController].rightMenuViewController = sortController;
    [[self menuContainerViewController] setRightMenuWidth:270];
    
    if (!isInitialized) {
        isInitialized = YES;
        
        _orderBy = TVOrderByDateAdded;
        
        self.categoryMediaList = [[NSMutableArray alloc] init];
        CategoryView *categoryView = [[CategoryView alloc] initWithNib];
        categoryView.frame = _viewCategory.bounds;
        [_viewCategory addSubview:categoryView];
        
        categoryView.delegate = self;
        
        self.filterMenuItemList = [[NSMutableArray alloc] init];
        
        [self updateSortByButton];
    }
    else {
        [_viewCategory performBlockOnSubviewsKindOfClass:[CategoryView class] block:^(UIView *subview) {
            [(CategoryView*)subview reloadCollectionView];
        }];
    }
    
    //if (_initialMenuItem) [self updateDataForMenuItem:_initialMenuItem];
    
    if (_labelNoRelatedTitle) {
        _labelNoRelatedTitle.text = LS(@"purchases_empty_title");
        [Utils setLabelFont:_labelNoRelatedTitle bold:YES];
    }
    
    if (_labelNoRelatedSubtitle) {
        _labelNoRelatedSubtitle.text = @"";//LS(@"player_related_no_items_subtitle");
        [Utils setLabelFont:_labelNoRelatedSubtitle bold:NO];
    }
}

- (void)viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    
    if (_initialMenuItem) [self updateDataForMenuItem:_initialMenuItem];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self menuContainerViewController].rightMenuViewController = nil;
    
}

#pragma mark - 

- (void)skinChanged {

    if (_labelSortOpen) {
        
        _labelSortOpen.backgroundColor = APST.brandColor;
        _labelSortOpen.textColor = APST.brandTextColor;
        
    } else if (_buttonSortOpen) {
        
        [self initSelectedBrandButtonUI:_buttonSortOpen];
        
    }
    
}

- (void)updateSortByButton {
    
    if (_labelSortOpen) {
        
        [Utils setLabelFont:_labelSortOpen bold:YES];
        _labelSortOpen.text = LS(@"category_page_sort_by");
        _viewSortOpen.frame = CGRectMake(_viewSortOpen.frame.origin.x, _viewSortOpen.frame.origin.y, [_labelSortOpen.text sizeWithAttributes:@{NSFontAttributeName:_labelSortOpen.font}].width + _viewSortOpen.frame.size.height, _viewSortOpen.frame.size.height);
        _viewSortOpen.transform = CGAffineTransformMakeRotation(-M_PI / 2);
        _viewSortOpen.center = CGPointMake(self.view.frame.size.width - (_viewSortOpen.frame.size.width / 2.0), (self.view.frame.size.height / 2.0) - 20.0);
        
    } else if (_buttonSortOpen) {
        
        [_buttonSortOpen setTitle:LS(@"category_page_sort_by") forState:UIControlStateNormal];
        
    }
    
    [self skinChanged];
}

- (IBAction)tapSortByButton:(UITapGestureRecognizer *)recognizer {

    [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
    
}

- (IBAction)buttonSortOpenPressed:(UIButton *)button {
    
    [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
        
}

- (void)updateDataForMenuItem:(TVMenuItem *)menuItem {
    
    if (_categoryMenuItem && [_categoryMenuItem isEqual:menuItem]) return;
    
    if (_searchBlankView && [_searchBlankView superview]) {
        
        [_searchBlankView removeFromSuperview];
        
    }
        
    self.categoryMenuItem = menuItem;
    [_filterMenuItemList removeAllObjects];
    
    NSString *type = [menuItem.layout objectOrNilForKey:ConstKeyMenuItemType];
    searchMode = [type isEqualToString:MainMenuItemsSEARCH];

    onlinePurchasesMode = [type isEqualToString:MainMenuItemsONLINE_PURCHASES];
    
    _orderBy = TVOrderByDateAdded;
    
    if (searchMode) {
        
        NSString *searchText = [menuItem.layout objectForKey:@"Text"];
        
        if (isPad) {
            
            _labelTitle.text = [NSString stringWithFormat:@"%@ \"%@\"", LS(@"search_page_search_results"), searchText];
            
        } else {
        
            UIFont *font = _labelTitle.font;
            
            NSString *sText = [NSString stringWithFormat:@"\"%@\"", searchText];
            
            float width = [sText sizeWithAttributes:@{NSFontAttributeName:font}].width;
            
            while (width > (_labelTitle.frame.size.width - 10.0) && [searchText length] > 2) {
                
                searchText = [searchText substringToIndex:[searchText length] - 2];
                
                sText = [NSString stringWithFormat:@"\"%@...\"", searchText];
                
                width = [sText sizeWithAttributes:@{NSFontAttributeName:font}].width;
            }
            
            _labelTitle.text = sText;
        }
        
    } else {
        _labelTitle.text = _categoryMenuItem.name;
    }
    
    _initialMenuItem = nil;
    
    [self categoryListInitialization];
    
}

#pragma mark - Category List

- (void)categoryListInitialization {
    
    if (searchMode && _searchBlankView && [_searchBlankView superview]) {
        
        return;

    }
        
    [_categoryMediaList removeAllObjects];
    _categoryMediaListCount = 0;
    
    CategoryView *categoryView = [[_viewCategory subviews] lastObject];
    [categoryView reloadCategoryData];
    
    CategorySortViewController *sortController = (CategorySortViewController *)[self menuContainerViewController].rightMenuViewController;
    [sortController updateWithMenuItem:_categoryMenuItem orderBy:_orderBy filters:_filterMenuItemList];
    
    if (_buttonSortOpen) {
        _buttonSortOpen.selected = [_filterMenuItemList count];
    }
}

- (void)loadCategoryData:(NSInteger)pageSize pageIndex:(NSInteger)pageIndex {
    
    _viewNoRelatedItems.hidden = YES;
    if (searchMode) {
        
        NSString *searchText = [_categoryMenuItem.layout objectForKey:@"Text"];
        
        
        NSArray *array = @[@{@"Name":searchText}];
        
        [self requestForSearchMediaOrList:@[] AndList:array mediaType:@"0" pageSize:100 pageIndex:0 orderBy:TVOrderByAndOrList_ID exact:NO completionBlock:^(id jsonResponse){
            
            [self fillData:jsonResponse];
            
        } failedBlock:^(NSString* localizedError) {
            
            [self fillData:[NSArray array]];
            
        }];
        
        /*
        __weak TVPAPIRequest *request = [TVPMediaAPI requestForSearchMediaWithText:searchText mediaType:TVMediaTypeAny pictureSize:[TVPictureSize iPadItemCellPictureSize] pageSize:pageSize pageIndex:pageIndex orderBy:_orderBy delegate:nil];
        
        
        [request setCompletionBlock:^{
            
            NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
            
            
            [self fillData:result];
            
            
        }];
        
        [request setFailedBlock:^{
            
            
        }];
        
        [self sendRequest:request];
        */

    } else if (onlinePurchasesMode) {
        
        
        EUPermittedItems *userSubscription = [[EUPermittedItems alloc] initWithQueue:self.networkQueue];
        
        [userSubscription getDomainOnlinePurchasesSubscriptionOrRentalIsSubscription:YES WithCompletionBlock:^(NSArray *rentalsArray, NSError *error)
         {
             
             NSMutableArray *array = [[NSMutableArray alloc] init];
             
             for (TVRental *item in rentalsArray) {
                 [array addObject:@(item.mediaID)];
             }
             
             if ([array count]) {
                 
                 __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetMediasInfoWithMediaIDs:array pictureSize:[TVPictureSize iPadItemCellPictureSize] includeDynamicInfo:YES delegate:nil];
                 
                 [request setCompletionBlock:^{
                     NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
                     
                     [self fillData:result];
                     if (![result count]) {
                         _viewNoRelatedItems.hidden = NO;
                     }else {
                         _viewNoRelatedItems.hidden = YES;
                     }
                 }];
                 
                 [request setFailedBlock:^{}];
                 [self sendRequest:request];
                 
             } else {
                 [self fillData:nil];
                 _viewNoRelatedItems.hidden = NO;
             }
             
             
         }];

        
    } else {
        
        
        TVMenuItem *item = ([_filterMenuItemList count] ? [_filterMenuItemList lastObject] : _categoryMenuItem);
        
        [self requestForGetChannelMediaList:[[item.layout objectForKey:ConstKeyMenuItemChannel] integerValue] pageSize:pageSize pageIndex:pageIndex orderBy:_orderBy completion:^(NSArray *result) {
            
            [self fillData:result];
            
        }];
        
        /*
        
        TVMultiFilterOrderBy newOrder = TVMFOrderByNone;
        TVChannelMFOrderDirection orderDir = TVChannelMFOrderDirectionDescending;
        
        if (_orderBy == TVOrderByDateAdded) {
            newOrder = TVMFOrderByCreateDate;
        } else if (_orderBy == TVOrderByNumberOfViews) {
            newOrder = TVMFOrderByViews;
        } else if (_orderBy == TVOrderByRating) {
            newOrder = TVMFOrderByLikeCounter;
        }
        
        
        [self requestForGetChannelMediaList:[[item.layout objectForKey:ConstKeyMenuItemChannel] integerValue] pageSize:pageSize pageIndex:pageIndex orderBy:newOrder orderDir:orderDir completion:^(NSArray *result) {
            
            [self fillData:result];
            
        }];
        */
        
    }
    
    
}

- (void)fillData:(NSArray *)result {
    
    [_searchBlankView removeFromSuperview];
    _viewNoRelatedItems.hidden = YES;

    if ([result count] && (searchMode || onlinePurchasesMode)) {
        for (NSDictionary *dic in result) {
            TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dic];
            [_categoryMediaList addObject:item];
        }
        _categoryMediaListCount = [_categoryMediaList count];
        
        if (searchMode) self.labelAutomation.accessibilityValue = [NSString stringWithFormat:@"%ld Items Found", (long)_categoryMediaListCount];

    }
    else if ([result count]) {


        for (NSDictionary *dic in result)
        {
            TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dic];
            _categoryMediaListCount = item.totalItems;
            
            NSInteger maxItemsCount = (_menuCategoryTypeSize == MenuCategoryTypeSize_2_3) ? categoryViewPadMaxItemsCount2_3 : categoryViewPadMaxItemsCount16_9;
            
            if (isPad && (_categoryMediaListCount > maxItemsCount) && (!onlinePurchasesMode) && (!searchMode)) {
                _categoryMediaListCount = maxItemsCount;
            }
            [_categoryMediaList addObject:item];
            
        }
        
        
    } else {
        
        if (searchMode) {
            
            CategoryView *categoryView = [[_viewCategory subviews] lastObject];
            [categoryView updateCategoryData];
            
            if (!_searchBlankView){
                _searchBlankView = [[SearchBlankView alloc] initWithNib];
            }
            
            _searchBlankView.center = self.viewCategory.center;
            _searchBlankView.frame = CGRectOffset(_searchBlankView.frame, 0, -_labelTitle.frame.size.height);
            
            _searchBlankView.labelTitleText = [NSString stringWithFormat:@"%@ \"%@\"", LS(@"search_page_search_no_results_without_param"), [_categoryMenuItem.layout objectForKey:@"Text"]];
            [self.viewCategory addSubview:_searchBlankView];

        }

    }
    
    if (_searchBlankView && [_searchBlankView superview]) {
        
    } else {
        CategoryView *categoryView = [[_viewCategory subviews] lastObject];
        [categoryView updateCategoryData];
        if (categoryView.delegate != self) {
          //  int asd = 0;
        }
    }
}

#pragma mark - CategorySortViewControllerDelegate

- (void)selectSortOption:(TVOrderBy)orderBy title:(NSString *)title {
    
    if (_orderBy != orderBy) {
        _orderBy = orderBy;
        
        [self categoryListInitialization];
        
        if (isPad && !searchMode) _labelTitle.text = [NSString stringWithFormat:@"%@ (%@)", _categoryMenuItem.name, title];
        
    }
    
}

- (void)selectFilterOption:(TVMenuItem *)filterItem {
    
    if (![_filterMenuItemList count] || ![[_filterMenuItemList lastObject] isEqual:filterItem]) {
        [_filterMenuItemList addObject:filterItem];

        [self categoryListInitialization];
        [AnalyticsM trackScreenCategory:_categoryMenuItem withFilters:_filterMenuItemList];
        
        if (isPad && !searchMode) _labelTitle.text = [NSString stringWithFormat:@"%@ (%@)", _categoryMenuItem.name, filterItem.name];
        
    }
    
    
}

- (void)selectFilterBack {
    
    if ([_filterMenuItemList count]) {
        [_filterMenuItemList removeLastObject];
        
        [self categoryListInitialization];
        [AnalyticsM trackScreenCategory:_categoryMenuItem withFilters:_filterMenuItemList];
        
        if (!searchMode) _labelTitle.text = _categoryMenuItem.name;
        
    }
    
}

#pragma mark -

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{
    
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Player View

- (void)openMediaItem:(TVMediaItem *)mediaItem {
    [super openMediaItem:mediaItem];
    [AnalyticsM trackScreenSelectedItem:_categoryMenuItem.name item:mediaItem.name];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
