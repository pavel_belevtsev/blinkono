//
//  CategoryViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategorySortViewController.h"
#import "HomeNavProfileView.h"
#import "SearchBlankView.h"

@interface CategoryViewController : BaseViewController <CategorySortViewControllerDelegate, HomeNavProfileViewDelegate> {
    
    BOOL isInitialized;
    BOOL searchMode;
    BOOL onlinePurchasesMode;

}

- (void)updateDataForMenuItem:(TVMenuItem *)menuItem;
- (void)loadCategoryData:(NSInteger)pageSize pageIndex:(NSInteger)pageIndex;

@property (strong, nonatomic) TVMenuItem *initialMenuItem;
@property (strong, nonatomic) TVMenuItem *categoryMenuItem;

@property (weak, nonatomic) IBOutlet UIView *viewCategory;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@property (nonatomic, strong) NSMutableArray *categoryMediaList;
@property (nonatomic) NSInteger categoryMediaListCount;

@property (nonatomic, weak) IBOutlet UIView *viewSortOpen;
@property (nonatomic, weak) IBOutlet UILabel *labelSortOpen;

@property (nonatomic, weak) IBOutlet UIButton *buttonSortOpen;

@property (nonatomic) TVOrderBy orderBy;
@property (nonatomic, strong) NSMutableArray *filterMenuItemList;

@property (weak, nonatomic) IBOutlet UIView *viewNoRelatedItems;
@property (weak, nonatomic) IBOutlet UILabel *labelNoRelatedTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelNoRelatedSubtitle;

@property (strong, nonatomic) SearchBlankView *searchBlankView;

@property MenuCategoryTypeSize menuCategoryTypeSize;

@property (nonatomic, weak) IBOutlet UILabel *labelAutomation;

@end
