//
//  EPGSelectDateViewPadVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/13/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGSelectDateViewPadVF.h"

#import "UIImage+Tint.h"

const NSInteger weekDayButtonWidth = 110;
const NSInteger weekDayButtonHeight = 50;

@implementation EPGSelectDateViewPadVF


/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)initUI
{
    self.scrollViewSelectDay.contentSize = CGSizeMake((APST.numberOfWeekDays + APST.numberOfRetroDays) * (weekDayButtonWidth + 1), weekDayButtonHeight);
    
    for (NSInteger i = 0; i < (APST.numberOfWeekDays + APST.numberOfRetroDays); i++)
    {
        // create a new button
        UIButton *weekDayButton = [self buildWeekDayButtonToIndex:i];
        
        // set selected state
        if (i == APST.numberOfRetroDays)
        {
            self.todayButton = weekDayButton;
            [weekDayButton setSelected:YES];
        }
        
        UIImage *imgCorner = [UIImage imageNamed:@"epg_search_corner.png"];
        UIView *viewCorner = [[UIView alloc] initWithFrame:CGRectMake(weekDayButton.frame.size.width - imgCorner.size.width, 1, imgCorner.size.width, imgCorner.size.height)];
        viewCorner.backgroundColor = [UIColor clearColor];
        UIImageView *imgViewCorner = [[UIImageView alloc] initWithImage:imgCorner];
        imgViewCorner.alpha = 0.5;
        
        [viewCorner addSubview:imgViewCorner];
        [weekDayButton addSubview:viewCorner];
        viewCorner.userInteractionEnabled = NO;
        
        UILabel *labelCount = [self buildLabelCountWithImageCorner:imgCorner];
        [viewCorner addSubview:labelCount];
        
        viewCorner.tag = 200 + i;
        viewCorner.hidden = YES;
        [self.scrollViewSelectDay addSubview:weekDayButton];
    }
    
    [self setupAndUpdateSelectDayScrollView];
}

/* "PSC : Vodafone Grup : change to keep VF EPG : JIRA ticket. */
- (void)changeScrollOffset:(float)offset {
    
    if (offset < 0) offset = 0;
    if (offset > self.scrollViewSelectDay.contentSize.width - self.scrollViewSelectDay.frame.size.width) offset = self.scrollViewSelectDay.contentSize.width - self.scrollViewSelectDay.frame.size.width;
    
    self.scrollViewSelectDay.contentOffset = CGPointMake(offset, 0);
    
}

- (void)skinChanged {
    // override - do nothing here!
}

-(UIButton *)buildWeekDayButtonToIndex:(NSInteger)index
{
    UIButton *weekDayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    weekDayButton.tag = index;
    weekDayButton.frame = CGRectMake(index*(weekDayButtonWidth + 1), 0, weekDayButtonWidth, weekDayButtonHeight);
    
    // set background image - line for selected state
    [weekDayButton setBackgroundImage:[UIImage imageNamed:@"tab_highlight_background"] forState:UIControlStateSelected];
    
    // set target
    [weekDayButton addTarget:self action:@selector(weekDayPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    // set the title
    weekDayButton.titleLabel.font = Font_Bold(14.0);
    weekDayButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [weekDayButton setTitleColor:[VFTypography instance].grayScale5Color forState:UIControlStateNormal];
    [weekDayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [weekDayButton setTitle:[[self dateTitle:index] uppercaseString] forState:UIControlStateNormal];
    
    return weekDayButton;
}

-(UILabel *)buildLabelCountWithImageCorner:(UIImage *)imgCorner
{
    UILabel *labelCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgCorner.size.width - 2, 16)];
    labelCount.backgroundColor = [UIColor clearColor];
    labelCount.textColor = [UIColor blackColor];
    labelCount.font = Font_Reg(12.0);
    labelCount.text = @"99";
    labelCount.textAlignment = NSTextAlignmentRight;
    return labelCount;
}

-(void)setupAndUpdateSelectDayScrollView
{
    self.scrollViewSelectDay.delegate = nil;
    if (APST.numberOfRetroDays > 0)
    {
        [self changeScrollOffset:APST.numberOfRetroDays * weekDayButtonWidth + (weekDayButtonWidth / 2) - (self.scrollViewSelectDay.frame.size.width / 2)];
    }
    self.scrollViewSelectDay.delegate = self;
}

@end
