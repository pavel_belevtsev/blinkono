//
//  EPGCollectionViewCellPadVF.h
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGCollectionViewCellPad.h"

@interface EPGCollectionViewCellPadVF : EPGCollectionViewCellPad

@property (strong, nonatomic) UIImageView * favImageView;

@end
