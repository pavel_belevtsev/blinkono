//
//  EPGCollectionViewCellPad.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 05.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGCollectionViewCellPad.h"
#import "UIImageView+WebCache.h"

@implementation EPGCollectionViewCellPad

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
}

- (void)updateCellWithMediaItem:(TVMediaItem *)mediaItem {
    
    [super updateCellWithMediaItem:mediaItem];
    
    [super.imgThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:mediaItem andSize:CGSizeMake(super.imgThumb.frame.size.width * 2, super.imgThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"epg_channel_placeholder"]];
    if (APST.showChannelNumber) {
        super.labelChanelNumber.text = [mediaItem.metaData objectForKey:@"Channel number"];
    } else {
        super.labelChanelNumber.text = @"";
    }
    super.labelChanelName.text = [mediaItem.name uppercaseString];

    /*
    EPGProgramItemViewPad *itemView = [[super.viewEPG subviews] objectAtIndex:0];
    itemView.alpha = 0.0;
    itemView.frame = CGRectMake(0, itemView.frame.origin.y, itemView.frame.size.width, itemView.frame.size.height);
    
    [itemView setChannelData:mediaItem];
    */
}


@end
