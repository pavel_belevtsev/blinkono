//
//  EPGProgramItemViewPadVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGProgramItemViewPadVF.h"

#import "UIImage+Tint.h"


@implementation EPGProgramItemViewPadVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */

- (void) awakeFromNib
{
    [super awakeFromNib];    
    
    labelProgram.textColor = [UIColor whiteColor];
    labelDescription.textColor = [VFTypography instance].grayScale6Color;
    labelTime.textColor = [VFTypography instance].grayScale6Color;
    self.viewContainer.backgroundColor = [VFTypography instance].grayScale1Color;

    //TODO: aviv - expose the following as outlet in product: use viewInfoBody
//    UIView* v1 = [[infoPopup subviews] objectAtIndex:0];
//    v1.backgroundColor = [VFTypography instance].darkGrayColor;
//    self.imgLikeCount.superview.hidden = NO;
    
//    self.likeButton.hidden = YES;
//    self.likeCount.hidden = YES;
    //self.viewInfoBubble.backgroundColor = [VFTypography instance].darkGrayColor;
   // self.imgLikeCount.superview.hidden = NO;

}

-(void)viewInfoBubbleBgView:(UIView *)bgView
{
    bgView.backgroundColor = [VFTypography instance].darkGrayColor;
}

-(void)insertArrowImage:(UIImageView *)arrowImage
{
    arrowImage.image = [[UIImage imageNamed:@"epg_arrow"] imageTintedWithColor:[VFTypography instance].darkGrayColor];
}


- (void)updateProgramData:(APSTVProgram *)program {
    [super updateProgramData:program];

    labelProgram.textColor = [UIColor whiteColor];
    labelDescription.textColor = [VFTypography instance].grayScale6Color;
    labelTime.textColor = [VFTypography instance].grayScale6Color;
}

- (void)skinChanged {
    [super skinChanged];
   // [self.imgLikeCount setImage:[[UIImage imageNamed:@"epg_arrow"] imageTintedWithColor:[VFTypography instance].darkGrayColor]];
   // [self.imgLikeCount setHighlightedImage:nil]; //override EPGProgramItemViewPad
}


@end
