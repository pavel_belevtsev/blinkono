//
//  EPGCollectionViewCellPadVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGCollectionViewCellPadVF.h"

@implementation EPGCollectionViewCellPadVF

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */

- (void) awakeFromNib
{
    [super awakeFromNib];
    self.labelChanelName.textColor = [UIColor whiteColor];
    self.viewLoading.backgroundColor = [VFTypography instance].darkGrayColor;
    
    self.viewEPG.backgroundColor = [VFTypography instance].grayScale1Color;
    self.backgroundColor = [VFTypography instance].grayScale2Color;
    self.viewLoading.backgroundColor = [VFTypography instance].grayScale1Color;
    self.imgFavBg.backgroundColor = [UIColor whiteColor];
    
    self.favImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"epg_ribon_icon"]];
    self.favImageView.center = self.imgFavBg.center;
    [self.imgFavBg addSubview:self.favImageView];
    
}

-(void)addSwipeGestureRecognizer
{
    ASLogInfo(@"addSwipeGestureRecognizer");
}
@end
