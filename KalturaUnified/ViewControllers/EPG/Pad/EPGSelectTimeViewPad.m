//
//  EPGSelectTimeViewPad.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSelectTimeViewPad.h"
#import "UIImage+Tint.h"

const NSInteger timeSliderOriginX = 18;
const NSInteger timeSliderWidth = 650;
const NSInteger timeSliderHeight = 50;
const NSInteger timeSliderButtonWidth = 72;

@implementation EPGSelectTimeViewPad

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.timeSlider = [[TVtimeSlider alloc] initWithFrame:CGRectMake(timeSliderOriginX, 0, timeSliderWidth, timeSliderHeight) buttonWidth:timeSliderButtonWidth];
    
    self.timeSlider.rightSideColor = [UIColor clearColor];
    self.timeSlider.leftSideColor = [UIColor clearColor];
    self.timeSlider.hoursTextColor = CS(@"999999");
    self.timeSlider.hoursForeGroundTextColor = [UIColor whiteColor];
    self.timeSlider.hoursTextFont = Font_Bold(13);
    self.timeSlider.buttonTextFont = Font_Bold(15);
    self.timeSlider.backGroundImage = nil;
    self.timeSlider.myDelegate = self;
    
    [self addSubview:self.timeSlider];
    
    [self.timeSlider now];

    [self skinChanged];
    
}

- (void)skinChanged {
    
    self.timeSlider.buttonTextColor = APST.brandTextColor;
    self.timeSlider.buttonLargeImage = [[UIImage imageNamed:@"epg_timeline_picker_clicked_brand"] imageTintedWithColor:APST.brandColor];
    self.timeSlider.buttonSmallImage = [[UIImage imageNamed:@"epg_timeline_picker"] imageTintedWithColor:APST.brandColor];
    
}

- (void)tvSliderValueChangedToTime:(NSDate *)time {
    
    super.currentInterval = [time timeIntervalSince1970];
    
    [super.delegate updateTimeLabel];
    
}

- (void)updateSelectedTime:(NSTimeInterval)currentInterval {
    
    self.currentInterval = currentInterval;

    [_timeSlider setSliderTime:[NSDate dateWithTimeIntervalSince1970:currentInterval]];
    
}

- (void)updateForSearchMode:(BOOL)searchMode {
    
    if (searchMode) {
        
        [self.timeSlider hideUIElements];
    } else {
        [self.timeSlider showUIElements];

    }
}

@end
