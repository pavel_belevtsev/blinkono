//
//  EPGSelectDateViewPad.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSelectDateView.h"

@interface EPGSelectDateViewPad : EPGSelectDateView <UIScrollViewDelegate> {
    
}

@property (nonatomic, weak) IBOutlet UIView *viewTapToday;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewSelectDay;
@property (nonatomic, weak) IBOutlet UIImageView *imgBubbleDate;
@property (nonatomic, weak) IBOutlet UILabel *labelSelectDateBubble;
@property (nonatomic, strong) UIButton *todayButton;

// the following declertion is used by customized profuct:
- (void)weekDayPressed:(UIButton *)button;

@end
