//
//  EPGSelectTimeViewPadVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/13/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGSelectTimeViewPadVF.h"

@implementation EPGSelectTimeViewPadVF
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)awakeFromNib {
    
    [super awakeFromNib];
  
    self.timeSlider.hoursForeGroundTextColor = [UIColor whiteColor];
    self.timeSlider.hoursTextColor = [VFTypography instance].grayScale4Color;
}
@end
