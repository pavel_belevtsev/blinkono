//
//  EPGProgramItemViewPad.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.09.13.
//  Copyright (c) 2013 Quickode Ltd. All rights reserved.
//

#import "EPGProgramItemViewPad.h"
#import "UIImage+Tint.h"

@implementation EPGProgramItemViewPad

@synthesize recoredHelper = _recoredHelper;

#define kInfoBubbleLabelTag    101
#define kInfoBubbleLabelTag    101

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [Utils setLabelFont:labelProgram bold:YES];
    [Utils setLabelFont:labelTime bold:YES];
    
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
}

- (void)skinChanged {
    viewProgress.backgroundColor = APST.brandColor;
    
    if (self.item) {
        [self updateProgramData:self.item];
    }
}

- (void)updateProgramData:(APSTVProgram *)program {
    
    [self showHideInfoPopup:NO];
    

    self.recoredIconImage.hidden =YES;
    self.buttonRecored.selected = NO;
    
    self.item = program;

    BOOL canStartOver = ([program isStartOverEnabled] && [program programIsOn] && [NU(UIUnit_live_startOver_buttons) isUIUnitExist]);
    BOOL canCatchup = ([program isCathupEnabled] && [program programFinished] && [NU(UIUnit_live_catchup_buttons) isUIUnitExist]);
    BOOL canRecord = ([self.item isRecordingEnabled] && [NU(UIUnit_live_record_buttons) isUIUnitExist]);

    labelTime.textColor = [program programIsOn] ? APST.brandColor : CS(@"999999");
    viewProgress.hidden = ![program programIsOn];
    
    if ([program programIsOn]) {
    
        int timeFull = ([program.endDateTime timeIntervalSince1970] - [program.startDateTime timeIntervalSince1970]);
        int timeCurrent = ([[NSDate date] timeIntervalSince1970] - [program.startDateTime timeIntervalSince1970]);
        
        float fullWidth = [viewProgress superview].frame.size.width;
        
        viewProgress.frame = CGRectMake(viewProgress.frame.origin.x, viewProgress.frame.origin.y, fullWidth * timeCurrent / timeFull, viewProgress.frame.size.height);
        
    }
    else if ([program.endDateTime timeIntervalSince1970] < [[NSDate date] timeIntervalSince1970]) {
        
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];
    labelTime.text = [NSString stringWithFormat:@"%@ - %@", [df stringFromDate:program.startDateTime], [df stringFromDate:program.endDateTime]];
    
    labelProgram.text = program.name;
    labelDescription.text = @"";

    if ([program.metaData objectOrNilForKey:@"episode name"]) {
        labelDescription.text = [program.metaData objectOrNilForKey:@"episode name"];
    }

    NSMutableArray *arrButtons = [NSMutableArray array];
    UIButton *button = nil;

    //add StartOver or Catchup button
    button = nil;
    if (canStartOver) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.UIUnitID = NU(UIUnit_live_startOver_buttons);
        [button activateAppropriateRule];
        [button setTitle:[LS(@"starover_btn_txt") uppercaseString] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"startover_icon_tablet"] forState:UIControlStateNormal<<UIControlStateHighlighted];
        [button setTag:ActionButtonsTypeStartOver];
    }
    else if (canCatchup) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.UIUnitID = NU(UIUnit_live_catchup_buttons);
        [button activateAppropriateRule];
        [button setTitle:[LS(@"catchup_btn_txt") uppercaseString]  forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"catchup_icon_tablet"] forState:UIControlStateNormal<<UIControlStateHighlighted];
        [button setTag:ActionButtonsTypeCatchup];
    }
    if (button) {
        [button addTarget:self action:@selector(playProgram:) forControlEvents:UIControlEventTouchUpInside];
        [arrButtons addObject:button];
    }

    //add record button
    if (canRecord) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTag:ActionButtonsTypeRecord];
        button.UIUnitID = NU(UIUnit_live_record_buttons);
        [button activateAppropriateRule];
        [button setImage:[UIImage imageNamed:@"epg_rec_button_ipad"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"epg_rec_button_stop_ipad"] forState:UIControlStateSelected];
        [button setTitle:[LS(@"epg_buttonRecored_rec_text") uppercaseString] forState:UIControlStateNormal];
        [button setTitle:[LS(@"epg_buttonRecoredc_cancel_text") uppercaseString] forState:UIControlStateSelected];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        self.buttonRecored = button;
        [button setTouchUpInsideAction:^(UIButton *btn) {
            BOOL isSeries = NO;
            NSDictionary *dic = self.item.metaData;
            NSString * seriesValue = [dic objectForKey:@"series ID"];
            NSLog(@"seriesName = %@",seriesValue);
            
            if ([seriesValue length]>0){
                isSeries = YES;
            }
            if (btn.selected) // cancel recording
            {
                [self.delegate programItemView:self didSelectCancelRecoredProgram:self.item mediaItem:self.mediaItem recoredItem:self.recordItem isSeries:isSeries];
//                if (isSeries == NO)
//                {
                    self.recoredIconImage.hidden =YES;
                    btn.selected = NO;
                    [self updateActionButtons];
//                }
            }
            else //record
            {
                [self.delegate programItemView:self didSelectRecoredProgram:self.item mediaItem:self.mediaItem isSeries:isSeries];
            }
        }];
        [arrButtons addObject:button];
    }

    //add info button
    if ([program.epgChannelID integerValue] >= 0) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:[LS(@"info") uppercaseString] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"epg_icon_info"] forState:UIControlStateNormal<<UIControlStateHighlighted];
        [button setTag:ActionButtonsTypeInfo];
        [button addTarget:self action:@selector(infoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

        [arrButtons addObject:button];
    }

    [self addMoreButtonsToArray: arrButtons];
    [self addActionButtonsToButtonsContainer:arrButtons];
}

- (void) addMoreButtonsToArray:(NSMutableArray*) array {
    //implement in child classes
}

- (void) updateButtonUI:(UIButton*) button {
    [button setTitleColor:CS(@"cccccc") forState:UIControlStateNormal];
    button.titleLabel.font = Font_Bold(10);
}

- (void) updateButtonEdgeInsets:(UIButton*) button {
    
    CGSize titleSize = [button.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: button.titleLabel.font}];

    CGFloat spacing = 4.0;
    CGSize imageSize = button.imageView.image.size;
    button.titleEdgeInsets = UIEdgeInsetsMake(0.0, - imageSize.width, - (imageSize.height + spacing), 0.0);
    button.imageEdgeInsets = UIEdgeInsetsMake( - (titleSize.height + spacing), 0.0, 0.0, - titleSize.width);
}

- (void) addActionButtonsToButtonsContainer:(NSMutableArray*) arrButtons {
    
    [_viewActionButtons setBackgroundColor:[UIColor clearColor]];
    _viewActionButtons.translatesAutoresizingMaskIntoConstraints = NO;
    [_viewActionButtons removeAllSubviews];
    
    if (_constraintsActionButtonsHeight!=nil){[self.viewContainer removeConstraints:_constraintsActionButtonsHeight];}
    if (_constraintsActionButtonsWidth!=nil){[self.viewContainer removeConstraints:_constraintsActionButtonsWidth];}
    
    
    [self.viewContainer addConstraints: _constraintsActionButtonsHeight = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[_viewActionButtons]-3-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_viewActionButtons)]];
    
    
    if (!arrButtons.count) {
        return;
    }
    
    CGFloat buttonHeight = 59.0f;
    CGFloat buttonWidth = 53.0f;
    CGFloat horizontalSpaceBetweenButtons = 7.0f;

    CGFloat totalWidth = 0;
    for (int i=0; i<arrButtons.count; i++) {
        UIButton *button = arrButtons[i];
        button.translatesAutoresizingMaskIntoConstraints = NO;
        [self updateButtonUI:button];
        [self updateButtonEdgeInsets:button];
        
//        CGSize titleSize = [[button titleForState:UIControlStateNormal] sizeWithAttributes:@{NSFontAttributeName: button.titleLabel.font}];
//        CGSize titleSizeSelected = [[button titleForState:UIControlStateSelected] sizeWithAttributes:@{NSFontAttributeName: button.titleLabel.font}];
//        buttonWidth = MAX(titleSize.width, titleSizeSelected.width) + 24;
        buttonWidth = 80.0;
        
        [_viewActionButtons addSubview:button];
        
        [_viewActionButtons addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                                       attribute:NSLayoutAttributeHeight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0f constant:buttonHeight]];
        [_viewActionButtons addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0f constant:buttonWidth]];
        
        UIButton *previousButton = (i>0) ? [arrButtons objectAtIndex:(i-1)] : nil;
        
        // horizontal position: right from previous button
        CGFloat constant = (i > 0 && arrButtons.count > i+1) ? horizontalSpaceBetweenButtons : 0;
        NSLayoutConstraint *horizontalConstraint = [NSLayoutConstraint constraintWithItem:button
                                                                                attribute:NSLayoutAttributeLeft
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:(previousButton) ? previousButton : [button superview]
                                                                                attribute:(previousButton) ? NSLayoutAttributeRight : NSLayoutAttributeLeft
                                                                               multiplier:1.0f
                                                                                 constant:constant];
        [_viewActionButtons addConstraint:horizontalConstraint];
        [_viewActionButtons addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[button]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)]];
        
        [button layoutIfNeeded];
        totalWidth += buttonWidth;
    }
    NSDictionary *metrics = @{@"width":@(totalWidth)};
    [self.viewContainer addConstraints: _constraintsActionButtonsWidth = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_viewActionButtons(width)]-7-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings( _viewActionButtons)]];
    
}

- (void) updateActionButtons {
    [self addActionButtonsToButtonsContainer:(NSMutableArray*)_viewActionButtons.subviews];
    [_viewActionButtons layoutIfNeeded];
}

-(void)infoButtonPressed:(id)sender{
    
    UIButton *button = (sender) ?: [_viewActionButtons viewWithTag:ActionButtonsTypeInfo];
    BOOL show = [self.viewInfoBubble isHidden];
    
    button.enabled = NO;

    UILabel *label = (UILabel*)[self.viewInfoBubble viewWithTag:kInfoBubbleLabelTag];
    if (self.item.programDescription.length == 0){
        label.textAlignment = NSTextAlignmentCenter;
        label.text = LS(@"tv_guide_empty_item_msg");
    }
    else {
        label.textAlignment = NSTextAlignmentLeft;
        label.text = self.item.programDescription;
    }
    
    [self showHideInfoPopup:show];
    
}

-(void)viewInfoBubbleBgView:(UIView *)bgView
{
    bgView.backgroundColor = CS(@"5A5A5A");
}

-(void)insertArrowImage:(UIImageView *)arrowImage
{
    arrowImage.image = [UIImage imageNamed:@"epg_arrow"];
}

- (UIView *) viewInfoBubble {
    if (!_viewInfoBubble) {
        _viewInfoBubble = [UIView new];
        _viewInfoBubble.translatesAutoresizingMaskIntoConstraints = NO;
        [self.viewContainer addSubview:_viewInfoBubble];
        [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_viewInfoBubble]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_viewInfoBubble)]];
        [self.viewContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_viewInfoBubble]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_viewInfoBubble)]];
        
        [self.viewContainer addConstraint: _constraintViewInfoBubbleWidth =[NSLayoutConstraint constraintWithItem:_viewInfoBubble attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:600]];
        
        UIImageView *arrowImage = [UIImageView new];
        arrowImage.translatesAutoresizingMaskIntoConstraints = NO;
        [self insertArrowImage:arrowImage];
        
        [_viewInfoBubble addSubview:arrowImage];
        [_viewInfoBubble addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[arrowImage]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(arrowImage)]];
        
        UIView *bgView = [UIView new];
        bgView.translatesAutoresizingMaskIntoConstraints = NO;
        [self viewInfoBubbleBgView:bgView];
        [_viewInfoBubble addSubview:bgView];
        [_viewInfoBubble addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[bgView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(bgView)]];
        [_viewInfoBubble addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[bgView][arrowImage(66)]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(bgView, arrowImage)]];

        UILabel *label = [UILabel new];
        label.tag = kInfoBubbleLabelTag;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentLeft;
        label.text = @"";
        label.textColor = APST.brandTextColor;
        [_viewInfoBubble addSubview:label];
        [_viewInfoBubble addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
        [_viewInfoBubble addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-30-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
        [Utils setLabelFont:label size:14 bold:NO];
        
        UITapGestureRecognizer *infoPopupTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(infoButtonPressed:)];
        [_viewInfoBubble addGestureRecognizer:infoPopupTapGestureRecognizer];
        
        [_viewInfoBubble layoutIfNeeded];
        _viewInfoBubble.left = -_viewInfoBubble.width;
        _viewInfoBubble.hidden = YES;
    }

    CGFloat width = self.viewContainer.width - MIN(self.viewActionButtons.width, 80);
    if (_constraintViewInfoBubbleWidth.constant != width) {
        _constraintViewInfoBubbleWidth.constant = width;
    }
        
    return _viewInfoBubble;
}

-(void)showHideInfoPopup:(BOOL)show{
    
    float alpha = (show) ? 1.0 : 0.0;
    BOOL hidden = (show) ? NO : YES;
    float left = 0 ;
    if (show) {
        self.viewInfoBubble.left = -self.viewInfoBubble.frame.size.width;
    }
    else {
        left = -self.viewInfoBubble.frame.size.width;
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.viewInfoBubble.left = left;
        self.viewInfoBubble.alpha = alpha;
        if (show) {
            self.viewInfoBubble.hidden = hidden;
            for (UITapGestureRecognizer *gr in self.viewInfoBubble.gestureRecognizers) {
                gr.enabled = YES;
            }
        }
    } completion:^(BOOL finished) {
        UIButton *button = (UIButton*)[_viewActionButtons viewWithTag:ActionButtonsTypeInfo];
        button.enabled = YES;
        if (!show)
            self.viewInfoBubble.hidden = hidden;
    }];
}

- (IBAction)playProgram:(id)sender
{
    if (([self.item programFinished] && [NU(UIUnit_Play_Catchup) isUIUnitExist] && [self.item isCathupEnabled]) || ([self.item programIsOn] && [NU(UIUnit_Play_StartOver) isUIUnitExist] && [self.item isStartOverEnabled]))
    {
          [self.delegate programItemView:self didSelectPlayProgram:self.item mediaItem:self.mediaItem];
    }
     else if  ([self.item programIsOn])
    {
         [self.delegate programItemView:self didSelectPlayProgram:nil mediaItem:self.mediaItem];
    }
    else
    {
        [self infoButtonPressed:nil];
    }
}

-(void)showRecoredItem:(BOOL)isSeries
{
    if (isSeries)
    {
        self.recoredIconImage.image = [UIImage imageNamed:@"record_series_icon"];
    }
    else
    {
        self.recoredIconImage.image = [UIImage imageNamed:@"record_singleItem_icon"];
    }
    self.recoredIconImage.hidden = NO;
    CGSize textSize = [[labelProgram text] sizeWithAttributes:@{NSFontAttributeName:[labelProgram font]}];
    CGFloat strikeWidth = textSize.width;
    
    CGRect frame = self.recoredIconImage.frame;
    frame.origin.x = labelProgram.frame.origin.x + strikeWidth + 5;
    self.recoredIconImage.frame = frame;
}

-(void)setRecoredHelper:(RecoredHelper *)recoredHelper
{
    _recoredHelper = recoredHelper;
    
    self.recoredIconImage.hidden =YES;
    UIButton *button = (UIButton*)[_viewActionButtons viewWithTag:ActionButtonsTypeRecord];

    if (!button.selected && (recoredHelper.recordingItemStatus == RecoredHelperRecoreded || recoredHelper.recordingItemStatus == RecoredHelperCurrenlyRecordeing)) {
        [self showRecoredItem:recoredHelper.isSeries];
        button.selected = YES;
        [self updateActionButtons];
    }
    else if (button.selected) {
        button.selected = NO;
        [self updateActionButtons];
    }

}

- (IBAction)buttonProgramPressed:(id)sender {

    
    [self playProgram:sender];
    
////1. For current programs – user tap on program area will launch the live channel player page with the current stream point of the live channel. Program buttons (start over, info) functionality will stay the same and user tap on the buttons area will trigger the applicable action as today.
//    BOOL isCurrentProgram = [self.item programIsOn];
//    if(isCurrentProgram){
//        
//        return;
//    }
//
////2. For past programs - user tap on program area will launch the catch-up player page. Program buttons (catch-Up, info) functionality will stay the same and user tap on the buttons area will trigger the applicable action as today. Please note - the catch-up icon should be removed from past programs in the EPG page.
////3. For future programs – will keep the current behavior – user tap on program area will flip the program area into the info panel. The info button will remain as well.
//   
//    BOOL shouldLaunchCatchup = [self.item programFinished];
//    
//    NSString * catchupFlag = [self.item.metaData objectForKey:kEPGFlagCatchup];
//    shouldLaunchCatchup &= [LoginM isSignedIn];
//    shouldLaunchCatchup &= ([catchupFlag length]>0);
//    shouldLaunchCatchup &= ( [catchupFlag caseInsensitiveCompare:@"yes"]== NSOrderedSame );
//    
//    if (shouldLaunchCatchup) {
//        [self playProgram:sender];
//    }else {
//        [self infoButtonPressed:sender];
//    }
}

@end
