//
//  EPGSelectDateViewPad.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSelectDateViewPad.h"
#import "UIImage+Tint.h"
#import "NSDate+Utilities.h"

const NSInteger weekDayButtonWidth = 110;
const NSInteger weekDayButtonHeight = 50;

@implementation EPGSelectDateViewPad

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initUI];
    
}

- (void)initUI {
    
    _scrollViewSelectDay.contentSize = CGSizeMake((APST.numberOfWeekDays + APST.numberOfRetroDays) * (weekDayButtonWidth + 1), weekDayButtonHeight);
    
    for (NSInteger i = 0; i < (APST.numberOfWeekDays + APST.numberOfRetroDays); i++) {
        
        // create a new button
        UIButton *weekDayButton = [UIButton buttonWithType:UIButtonTypeCustom];
        weekDayButton.tag = i;
        weekDayButton.frame = CGRectMake(i*(weekDayButtonWidth + 1), 0, weekDayButtonWidth, weekDayButtonHeight);
        
        // set background image - line for selected state
        [weekDayButton setBackgroundImage:[[UIImage imageNamed:@"tab_highlight_background"] imageTintedWithColor:APST.brandColor]  forState:UIControlStateSelected];

        // set target
        [weekDayButton addTarget:self action:@selector(weekDayPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        // set the title
        weekDayButton.titleLabel.font = Font_Bold(14.0);
        weekDayButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [weekDayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [weekDayButton setTitleColor:CS(@"a8a8a8") forState:UIControlStateNormal];
        [weekDayButton setTitleColor:CS(@"616161") forState:UIControlStateDisabled];
        [weekDayButton setTitle:[self dateTitle:i] forState:UIControlStateNormal];
        
        if (i == APST.numberOfRetroDays) {
            self.todayButton = weekDayButton;
            [weekDayButton setSelected:YES];
        }
        
        
        [weekDayButton setTitle:[weekDayButton.titleLabel.text uppercaseString] forState:UIControlStateNormal];
        
        UIImage *imgCorner = [[UIImage imageNamed:@"epg_search_corner"] imageTintedWithColor:CS(@"616161")];
        UIView *viewCorner = [[UIView alloc] initWithFrame:CGRectMake(weekDayButton.frame.size.width - imgCorner.size.width, 1, imgCorner.size.width, imgCorner.size.height)];
        viewCorner.backgroundColor = [UIColor clearColor];
        UIImageView *imgViewCorner = [[UIImageView alloc] initWithImage:imgCorner];
        imgViewCorner.alpha = 0.5;
        [viewCorner addSubview:imgViewCorner];
        [weekDayButton addSubview:viewCorner];
        viewCorner.userInteractionEnabled = NO;
        
        UILabel *labelCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgCorner.size.width - 2, 16)];
        labelCount.backgroundColor = [UIColor clearColor];
        labelCount.textColor = CS(@"C1C1C1");
        labelCount.font = Font_Reg(12.0);
        labelCount.text = @"99";
        labelCount.textAlignment = NSTextAlignmentRight;
        [viewCorner addSubview:labelCount];
        
        viewCorner.tag = 200 + i;
        viewCorner.hidden = YES;
        
        [_scrollViewSelectDay addSubview:weekDayButton];
    }
    
    _scrollViewSelectDay.delegate = nil;
    if (APST.numberOfRetroDays > 0) {
        
        [self changeScrollOffset:APST.numberOfRetroDays * weekDayButtonWidth + (weekDayButtonWidth / 2) - (_scrollViewSelectDay.frame.size.width / 2)];
    
    }
    _scrollViewSelectDay.delegate = self;
    
}

- (void)changeScrollOffset:(float)offset {
    
    if (offset < 0) offset = 0;
    if (offset > _scrollViewSelectDay.contentSize.width - _scrollViewSelectDay.frame.size.width) offset = _scrollViewSelectDay.contentSize.width - _scrollViewSelectDay.frame.size.width;
    
    _scrollViewSelectDay.contentOffset = CGPointMake(offset, 0);
    
}

- (void)weekDayPressed:(UIButton *)button {
    
    [self selectWeekdayButtonForDayOffset:button.tag];
    
    super.selectedDay = button.tag - APST.numberOfRetroDays;
    
    [super.delegate updateDateLabel];

}

- (void)selectWeekdayButtonForDayOffset:(NSInteger)dayOffset {
    
    if (dayOffset >= 0 && dayOffset < APST.numberOfRetroDays + APST.numberOfWeekDays) {
        
        [_scrollViewSelectDay performBlockOnSubviewsKindOfClass:[UIButton class] block:^(UIView *subview) {
            UIButton *tempButton = (UIButton*)subview;
            [tempButton setSelected:(tempButton.tag == dayOffset)];
            
            if (tempButton.tag == dayOffset) {
                [_scrollViewSelectDay scrollRectToVisible:tempButton.frame animated:YES];
            }
        }];
    }
}

- (NSString *)dateBubbleTitle {
    
    NSString *title = [_labelSelectDateBubble.text lowercaseString];
    return [title capitalizedString];
    
}

- (void)setToday {
    
    [self clearSearchResults];
    
    [self changeScrollOffset:APST.numberOfRetroDays * weekDayButtonWidth + (weekDayButtonWidth / 2) - (_scrollViewSelectDay.frame.size.width / 2)];
    
    if (_todayButton) {
        [self weekDayPressed:_todayButton];
        
    }
}

- (void)setDay:(NSInteger)day {

   [self changeScrollOffset:(APST.numberOfRetroDays + day) * weekDayButtonWidth + (weekDayButtonWidth / 2) - (_scrollViewSelectDay.frame.size.width / 2)];
    
    [_scrollViewSelectDay performBlockOnSubviewsKindOfClass:[UIButton class] block:^(UIView *subview) {
        UIButton *button = (UIButton*)subview;
        if (button.tag == (APST.numberOfRetroDays + day)) {
            [self weekDayPressed:button];
        }
    }];
}

- (void)updateSearchResults:(NSArray *)days {
    
    [_scrollViewSelectDay performBlockOnSubviewsKindOfClass:[UIButton class] block:^(UIView *subview) {
        UIButton *button = (UIButton*)subview;

        if (button.tag < [days count]) {
   
            NSDictionary *day = [days objectAtIndex:button.tag];
            
            UIView *viewCorner = [button viewWithTag:button.tag + 200];
            if (viewCorner) {
                
                int count = [[day objectForKey:@"count"] intValue];
                if (count) {
                    viewCorner.hidden = NO;
                    UILabel *label = [[viewCorner subviews] lastObject];
                    label.text = [NSString stringWithFormat:@"%d", count];
                    
                    button.enabled = YES;
                    
                } else {
                    viewCorner.hidden = YES;
                    
                    //button.enabled = NO;
                }
            }
        }
    }];
    
}

- (void)clearSearchResults {
    
    [_scrollViewSelectDay performBlockOnSubviewsKindOfClass:[UIButton class] block:^(UIView *subview) {
        UIButton *button = (UIButton*)subview;

        UIView *viewCorner = [button viewWithTag:button.tag + 200];
        if (viewCorner) {
            viewCorner.hidden = YES;
        }
        
        button.enabled = YES;
    }];
}

- (void)clearSearchResultsAndDisableButtons {
    /*
    [_scrollViewSelectDay performBlockOnSubviewsKindOfClass:[UIButton class] block:^(UIView *subview) {
        UIButton *button = (UIButton*)subview;

        UIView *viewCorner = [button viewWithTag:button.tag + 200];
        if (viewCorner) {
            viewCorner.hidden = YES;
        }

        button.selected = NO;
        button.enabled = NO;
     }];
     */
}


- (void)restoreButtons {

    [_scrollViewSelectDay performBlockOnSubviewsKindOfClass:[UIButton class] block:^(UIView *subview) {
        UIButton *button = (UIButton*)subview;
        if (button.tag == self.selectedDay + APST.numberOfRetroDays){
            button.selected = YES;
        }
        
        button.enabled = YES;
    }];
}

@end
