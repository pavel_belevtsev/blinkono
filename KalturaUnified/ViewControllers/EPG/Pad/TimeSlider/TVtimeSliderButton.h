//
//  TVtimeSliderButton.h
//  TVinciTimeSlider
//
//  Created by Nimrod Shai on 7/24/13.
//  Copyright (c) 2013 Nimrod Shai. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol TVtimeSliderButtonDelegate <NSObject>

@required
-(void)tvSliderButtonDidShrink;

@end


@interface TVtimeSliderButton : UIView

@property (nonatomic) id <TVtimeSliderButtonDelegate> myDelegate;

@property (nonatomic) NSString *time;
@property (nonatomic) NSInteger buttonWidth;
@property (nonatomic) BOOL isEnlarged;
@property (nonatomic) BOOL isTextEnlarged;
@property (nonatomic,strong) UILabel *buttonViewLabel;
@property (nonatomic,strong) UILabel *buttonViewLargeLabel;
@property (nonatomic,strong) UIImageView *buttonImageView;
@property (nonatomic,strong) UIImageView *buttonSmallImageView;
@property (nonatomic,strong) UIImageView *buttonLargeImageView;
@property (nonatomic) float fontOriginSize;


-(void)enlarge;
-(void)shrink;
-(void)enlargeText;
-(void)shrinkText;

@end
