//
//  TVtimeSliderScrollView.m
//  TVinciTimeSlider
//
//  Created by Nimrod Shai on 7/24/13.
//  Copyright (c) 2013 Nimrod Shai. All rights reserved.
//

#import "TVtimeSliderScrollView.h"

#define ANIMATION_SPEED 0.2

@implementation TVtimeSliderScrollView
@synthesize wasTouched,buttonWidth;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    wasTouched = YES;
    
    UITouch *touch = [touches anyObject];
    CGPoint locationInSelf = [touch locationInView:self];

    CGPoint newOffset;
    
    float realX;
    if (locationInSelf.x <= (self.frame.size.width-buttonWidth)) { // touched left to the button
        realX  = (self.contentOffset.x + ((self.frame.size.width-buttonWidth) - locationInSelf.x) + buttonWidth/2);
    }else{ // touched right to the button
        realX  = (self.contentOffset.x - ((self.frame.size.width-buttonWidth) - (self.contentSize.width - locationInSelf.x)) - buttonWidth/2); // working
    }
    locationInSelf.x = realX;
    
    if ((locationInSelf.x) > self.frame.size.width - buttonWidth) { // touched left end
        newOffset = CGPointMake(self.frame.size.width - buttonWidth, 0);

    }else if((locationInSelf.x) < buttonWidth/2){ // touched right end
        newOffset = CGPointMake(0, 0);

    }else{ // touched in the middle
        newOffset = CGPointMake(locationInSelf.x, 0);
    }
    
    [self setContentOffset:newOffset animated:NO];
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.buttonView shrink];
    [self.buttonView shrinkText];

}


@end
