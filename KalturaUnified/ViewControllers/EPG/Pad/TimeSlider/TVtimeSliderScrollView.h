//
//  TVtimeSliderScrollView.h
//  TVinciTimeSlider
//
//  Created by Nimrod Shai on 7/24/13.
//  Copyright (c) 2013 Nimrod Shai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVtimeSliderButton.h"

@interface TVtimeSliderScrollView : UIScrollView

@property (nonatomic) NSInteger buttonWidth;

@property (nonatomic,strong) TVtimeSliderButton *buttonView;
@property (nonatomic) BOOL wasTouched;

@end
