//
//  TVtimeSliderButton.m
//  TVinciTimeSlider
//
//  Created by Nimrod Shai on 7/24/13.
//  Copyright (c) 2013 Nimrod Shai. All rights reserved.
//

#import "TVtimeSliderButton.h"
#import "TVtimeSlider.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Tint.h"

#define ANIMATION_SPEED 0//0.2
#define BUTTON_HEIGHT_MULTIPLIER 1.2
#define TWO_THIRDS 0.66666667

const CGFloat TVTimeSliderButtonImageViewHeight = 30.0f;

@implementation TVtimeSliderButton
{
    float currentFontSize;
}
@synthesize buttonViewLabel,buttonImageView,buttonWidth,myDelegate,buttonLargeImageView,buttonSmallImageView;


- (id)initWithFrame:(CGRect)frame
{
    buttonWidth = frame.size.width;
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.autoresizesSubviews = YES;
        
        buttonImageView = [[UIImageView alloc] init];
        buttonImageView.frame = CGRectMake(0.0,
                                           (frame.size.height - TVTimeSliderButtonImageViewHeight) / 2.0,
                                           self.frame.size.width,
                                           TVTimeSliderButtonImageViewHeight);
        buttonImageView.image = [UIImage imageNamed:@"epg_timeline_picker_fx"];
        
        buttonSmallImageView = [[UIImageView alloc]initWithFrame:buttonImageView.bounds];
        [buttonImageView addSubview:buttonSmallImageView];
        [self addSubview:buttonImageView];
        
        buttonLargeImageView = [[UIImageView alloc] init];
        [self addSubview:buttonLargeImageView];
        [buttonLargeImageView setHidden:YES];
        
        
       
        
        buttonViewLabel = [[UILabel alloc] init];
        buttonViewLabel.frame = CGRectMake(0, (self.frame.size.height/2)-buttonViewLabel.font.pointSize/2, buttonWidth, buttonViewLabel.font.pointSize);
        
        [buttonViewLabel setCenter:CGPointMake(buttonWidth/2, self.frame.size.height/2)];
        buttonViewLabel.backgroundColor = [UIColor clearColor];
        buttonViewLabel.textAlignment = NSTextAlignmentCenter;
        buttonViewLabel.textColor = [UIColor whiteColor];
        [self addSubview:buttonViewLabel];
        
        self.buttonViewLargeLabel = [[UILabel alloc] init];
        self.buttonViewLargeLabel.textAlignment = NSTextAlignmentCenter;
        self.buttonViewLargeLabel.backgroundColor = [UIColor clearColor];
        self.buttonViewLargeLabel.font = [UIFont boldSystemFontOfSize:25];
        [self addSubview:self.buttonViewLargeLabel];
        [self.buttonViewLargeLabel setHidden:YES];

    }
    return self;
}


-(void)enlarge{
    if (!self.isEnlarged) {
        self.isEnlarged = YES;
        
        // *** handles putting different image
        [buttonLargeImageView setHidden:NO];
        [self.buttonViewLargeLabel setHidden:NO];
        
        [buttonImageView setHidden:YES];
        [self.buttonViewLabel setHidden:YES];
    }
}



-(void)shrink{
    if (self.isEnlarged) {
        self.isEnlarged = NO;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:ANIMATION_SPEED];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [buttonViewLabel setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
        [UIView commitAnimations];
        
        // *** handles putting different image
        [buttonLargeImageView setHidden:YES];
        [self.buttonViewLargeLabel setHidden:YES];

        [buttonImageView setHidden:NO];
        [self.buttonViewLabel setHidden:NO];
    }
    
    [self.myDelegate tvSliderButtonDidShrink];
}



-(void)enlargeText{
    
    if (!self.isTextEnlarged) {
        self.isTextEnlarged = YES;
        [self performSelector:@selector(switchToBiggerText) withObject:nil afterDelay:ANIMATION_SPEED];
        currentFontSize = self.buttonViewLabel.font.pointSize;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:ANIMATION_SPEED];
        self.buttonViewLabel.transform = CGAffineTransformMakeScale(1.5,1.5);
        [UIView commitAnimations];
        
    }
    
}

-(void)shrinkText{
    
    if (self.isTextEnlarged) {
        
        self.isTextEnlarged = NO;
        [self performSelector:@selector(switchToSmallerText) withObject:nil afterDelay:ANIMATION_SPEED];
        currentFontSize = self.buttonViewLabel.font.pointSize;
        
        if (buttonViewLabel.transform.a == 1) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:ANIMATION_SPEED];
            self.buttonViewLabel.transform = CGAffineTransformMakeScale(TWO_THIRDS,TWO_THIRDS);
            [UIView commitAnimations];
            
        }else{

            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:ANIMATION_SPEED];
            self.buttonViewLabel.transform = CGAffineTransformMakeScale(1,1);
            [UIView commitAnimations];
            
        }
        
        

    }
}

-(void)switchToBiggerText{
    if (buttonViewLabel.transform.a == 1.5) {
                
        self.buttonViewLabel.transform = CGAffineTransformMakeScale(1,1);
        
        NSString *fontName =buttonViewLabel.font.fontName;
        self.buttonViewLabel.font = [UIFont fontWithName:fontName size:currentFontSize*1.5];
        self.buttonViewLabel.frame = CGRectMake(buttonViewLabel.frame.origin.x, (self.frame.size.height/2)-buttonViewLabel.font.pointSize/2, buttonWidth, buttonViewLabel.font.pointSize);
        

    }
    currentFontSize = currentFontSize * 1.5;
}

-(void)switchToSmallerText{
    if (currentFontSize > self.fontOriginSize) {

        self.buttonViewLabel.transform = CGAffineTransformMakeScale(1,1);
        NSString *fontName =buttonViewLabel.font.fontName;
        self.buttonViewLabel.font = [UIFont fontWithName:fontName size:currentFontSize*TWO_THIRDS];
        self.buttonViewLabel.frame = CGRectMake(0, (self.frame.size.height/2)-buttonViewLabel.font.pointSize/2, buttonWidth, buttonViewLabel.font.pointSize);
    
        currentFontSize = currentFontSize * TWO_THIRDS;
    }
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{    
    [self enlarge];
    [self enlargeText];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self shrink];
    [self shrinkText];
}






@end
