//
//  TVtimeSlider.m
//  TVinciTimeSlider
//
//  Created by Nimrod Shai on 7/23/13.
//  Copyright (c) 2013 Nimrod Shai. All rights reserved.
//

#import "TVtimeSlider.h"

#define BUTTON_HEIGHT_MULTIPLIER 1.2
#define ANIMATION_SPEED 0.2
#define NUMBER_OF_LABELS 9
#define MINUTES_IN_HOUR 60
#define MINUTES_IN_DAY 1440

NSString * const TVNAnalyticsEventEPGTimeChangeFormat = @"HH:mm";

@interface TVtimeSlider()

@property (nonatomic,strong) UIImageView *leftResizeableImageView;
@property (nonatomic,strong) UIImageView *rightResizeableImageView;

@end

@implementation TVtimeSlider
{
    TVtimeSliderScrollView *buttonScrollView;
    TVtimeSliderButton *buttonView;
    UIImageView *backGroundImageView;
    UILabel *buttonViewLabel;
    float touchedStartX;
    UIView *fillColorView;
    
    UIView *timeGridView;
    UIView *timeGridViewForeGround;
    UILabel *hourLabel;
    BOOL isNowMethod;
}
@synthesize value,buttonWidth;

#pragma mark init stuff

- (id)initWithFrame:(CGRect)frame buttonWidth:(int)abuttonWidth {
    self = [super initWithFrame:frame];
    if (self) {
        isNowMethod = NO;
        
        buttonWidth = abuttonWidth;
        self.backgroundColor = [UIColor clearColor];
        buttonScrollView = self.buttonScrollView;
        [self addSubview:buttonScrollView];
        
        
        self.leftResizeableImageView = [[UIImageView alloc] init];
        [self.leftResizeableImageView setImage:[[UIImage imageNamed:@"epg_timeline_progress"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
        
        self.rightResizeableImageView = [[UIImageView alloc] init];
        [self.rightResizeableImageView setImage:[[UIImage imageNamed:@"epg_timeline_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
        
        
        [self.leftResizeableImageView setFrame:
         CGRectMake(0,(self.frame.size.height - self.rightResizeableImageView.image.size.height)/2,
                    buttonScrollView.frame.origin.x + buttonWidth/2,
                    self.leftResizeableImageView.image.size.height)];
        
        [self addSubview:self.leftResizeableImageView];
        [self sendSubviewToBack:self.leftResizeableImageView];
        
        [self.rightResizeableImageView setFrame:
         CGRectMake(buttonScrollView.frame.origin.x + buttonWidth/2,
                    (self.frame.size.height - self.rightResizeableImageView.image.size.height)/2,
                    self.frame.size.width - self.leftResizeableImageView.frame.size.width,
                    self.rightResizeableImageView.image.size.height)];
        
        [self addSubview:self.rightResizeableImageView];
        [self sendSubviewToBack:self.rightResizeableImageView];
        [self setupInitialTime];
    }
    return self;
}

-(void)setupInitialTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate: [NSDate date]];
    _currentYear = [components year];
    _currentMonth  = [components month];
    _currentDay = [components day];
}

- (TVtimeSliderScrollView *)buttonScrollView {
    // scrollview part
    TVtimeSliderScrollView *scrollView = [[TVtimeSliderScrollView alloc] initWithFrame:CGRectMake(0, -((BUTTON_HEIGHT_MULTIPLIER - 1)/2) * self.frame.size.height, self.frame.size.width, self.frame.size.height * BUTTON_HEIGHT_MULTIPLIER)];
    scrollView.buttonWidth = buttonWidth;
    scrollView.delegate = self;
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.contentSize = CGSizeMake((self.frame.size.width*2)-buttonWidth, self.frame.size.height * BUTTON_HEIGHT_MULTIPLIER);
    scrollView.contentOffset = CGPointMake(self.frame.size.width-buttonWidth, 0);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.clipsToBounds = NO;
    scrollView.bounces = NO;
    
    // time grid part
    [self addSubview:[self getBackGroundTimeGridView]];
    
    // fill color view part
    fillColorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, buttonWidth/2, self.frame.size.height)];
    fillColorView.backgroundColor = [UIColor blueColor];
    fillColorView.clipsToBounds = YES;
    [self addSubview:fillColorView];
    [fillColorView addSubview:[self getForeGroundTimeGridView]];
    
    backGroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:backGroundImageView];
    
    // button part
    CGFloat offsetYForTempFixAssetSize = 1;
    buttonView = [[TVtimeSliderButton alloc] initWithFrame:CGRectMake(self.frame.size.width-buttonWidth, 0.0 + offsetYForTempFixAssetSize, buttonWidth, scrollView.frame.size.height)];
    buttonView.isEnlarged = NO;
    buttonView.myDelegate = self;
    buttonView.buttonWidth = buttonWidth;
    buttonView.backgroundColor = [UIColor clearColor];
    scrollView.buttonView = buttonView;

    [scrollView addSubview:buttonView];
    
    // label part
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[self getTime]];
    NSInteger hour = [components hour];
    NSString *timeString = [NSString stringWithFormat:@"%02ld:%02ld", (long)hour, (long)components.minute];
    buttonView.buttonViewLabel.text = timeString;
    buttonView.buttonViewLargeLabel.text = timeString;
    
    return scrollView;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    CGPoint offset = scrollView.contentOffset;
    [scrollView setContentOffset:offset animated:NO];
    [buttonView shrink];
    [buttonView shrinkText];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    [scrollView setContentOffset:scrollView.contentOffset animated:NO];
    [buttonView shrink];
    [buttonView shrinkText];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    // handle two imageviews
    [self.leftResizeableImageView setFrame:
     CGRectMake(0,
                (self.frame.size.height - self.rightResizeableImageView.image.size.height)/2,
                ((buttonScrollView.frame.size.width-buttonWidth)-buttonScrollView.contentOffset.x) + buttonWidth/2 ,
                self.leftResizeableImageView.image.size.height)];
    
    [self.rightResizeableImageView setFrame:
     CGRectMake(self.leftResizeableImageView.frame.size.width,
                (self.frame.size.height - self.rightResizeableImageView.image.size.height)/2,
                self.frame.size.width - self.leftResizeableImageView.frame.size.width,
                self.rightResizeableImageView.image.size.height)];
    
    
    value = ABS(((scrollView.contentOffset.x/(scrollView.frame.size.width-buttonWidth))-1));
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:[self getTime]];
	NSInteger hour = [components hour] + ([components day] != self.currentDay ? 24 : 0);
    NSString *timeString = [NSString stringWithFormat:@"%02ld:%02ld", (long)hour, (long)components.minute];
    buttonView.buttonViewLabel.text = timeString;
    buttonView.buttonViewLargeLabel.text = timeString;

    fillColorView.frame = CGRectMake(0, 0, buttonWidth/2 + ((buttonScrollView.frame.size.width-buttonWidth)-buttonScrollView.contentOffset.x), self.frame.size.height);
    
    if (!isNowMethod) {

        [buttonView enlarge];
        [buttonView enlargeText];
    }
}

- (NSDate *)getTime {
    if (!self.currentDate) {
        self.currentDate = [NSDate date];
    }
    NSDate *time = self.timeFromSlider ? self.timeFromSlider : self.currentDate;
    
    NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:unitFlags fromDate:time];
	
	[components setYear:self.currentYear];
	[components setMonth:self.currentMonth];
    [components setDay:self.currentDay];
    
    NSInteger oneMinute = 1;
	NSInteger minutes = value * MINUTES_IN_DAY;
    if (value >= 1.0) {
        minutes = MINUTES_IN_DAY - oneMinute;
    }
    if (minutes < MINUTES_IN_HOUR) {
        [components setHour:0];
        [components setMinute:minutes];
    } else {
        [components setHour:(minutes/MINUTES_IN_HOUR)];
        [components setMinute:(minutes%MINUTES_IN_HOUR)];
    }
    time = [[NSCalendar currentCalendar] dateFromComponents:components];
    return time;
}

- (UIView *)getBackGroundTimeGridView {
    timeGridView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - buttonWidth, self.frame.size.height)];
    timeGridView.backgroundColor = [UIColor clearColor];

    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
        
    for (int i = 0; i < NUMBER_OF_LABELS; i++) {
        NSString *hour = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:(i*3)]];
        
        float leftPadding = (((self.frame.size.width)/(NUMBER_OF_LABELS-1))/2)+3;
        
        hourLabel = [[UILabel alloc] init];
        hourLabel.font = [UIFont systemFontOfSize:12];
        hourLabel.textAlignment = NSTextAlignmentCenter;
        hourLabel.frame = CGRectMake(leftPadding + i * ((self.frame.size.width-buttonWidth)/(NUMBER_OF_LABELS-1)), (self.frame.size.height/2)-hourLabel.font.pointSize/2, 40, hourLabel.font.pointSize);
        
        hourLabel.center = CGPointMake(buttonWidth/2 + i * ((self.frame.size.width-buttonWidth)/(NUMBER_OF_LABELS-1)), (self.frame.size.height/2));
        
        hourLabel.backgroundColor = [UIColor clearColor];
        hourLabel.text = [NSString stringWithFormat:@"%@:00",hour];
        [timeGridView addSubview:hourLabel];
    }
    
    return timeGridView;
}

- (UIView *)getForeGroundTimeGridView {
    timeGridViewForeGround = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - buttonWidth, self.frame.size.height)];
    timeGridViewForeGround.backgroundColor = [UIColor clearColor];
    
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    
    for (int i = 0; i < NUMBER_OF_LABELS; i++) {
        NSString *hour = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:(i*3)]];
        
        float leftPadding = (((self.frame.size.width)/(NUMBER_OF_LABELS-1))/2)+3;
        
        hourLabel = [[UILabel alloc] init];
        hourLabel.font = [UIFont systemFontOfSize:12];
        hourLabel.textAlignment = NSTextAlignmentCenter;
        hourLabel.frame = CGRectMake(leftPadding + i * ((self.frame.size.width-buttonWidth)/(NUMBER_OF_LABELS-1)), (self.frame.size.height/2)-hourLabel.font.pointSize/2, 40, hourLabel.font.pointSize);
        
        hourLabel.center = CGPointMake(buttonWidth/2 + i * ((self.frame.size.width-buttonWidth)/(NUMBER_OF_LABELS-1)), (self.frame.size.height/2));
        
        hourLabel.backgroundColor = [UIColor clearColor];
        hourLabel.text = [NSString stringWithFormat:@"%@:00",hour];
        [timeGridViewForeGround addSubview:hourLabel];
    }
    
    return timeGridViewForeGround;
}

- (CGRect)resizeToStretch:(UILabel *)label {
    float width = [self expectedWidth:label];
    CGRect newFrame = [self frame];
    newFrame.size.width = width;
    
    return newFrame;
}

- (float)expectedWidth:(UILabel *)label {
    [label setNumberOfLines:1];
    
    CGSize maximumLabelSize = CGSizeMake(9999, self.frame.size.height);
    
    //CGSize expectedLabelSize = [[label text] sizeWithFont:[label font]
    //                                   constrainedToSize:maximumLabelSize
    //                                       lineBreakMode:[label lineBreakMode]];
    
    CGSize expectedLabelSize = [[label text] boundingRectWithSize:maximumLabelSize
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:@{NSFontAttributeName:[label font]}
                                                          context:nil].size;
    
    return expectedLabelSize.width;
}

#pragma mark SET methods

- (void)setLeftSideColor:(UIColor *)color {
    fillColorView.backgroundColor = color;
}

- (void)setRightSideColor:(UIColor *)color {
    self.backgroundColor = color;
}

- (void)setButtonTextColor:(UIColor *)color {
    buttonView.buttonViewLabel.textColor = color;
    buttonView.buttonViewLargeLabel.textColor = color;
}

- (void)setHoursTextColor:(UIColor *)color {
    for (UILabel *label in timeGridView.subviews) {
        label.textColor = color;
    }    
}

- (void)setHoursForeGroundTextColor:(UIColor *)color {
    for (UILabel *label in timeGridViewForeGround.subviews) {
        label.textColor = color;
    }
}

- (void)setButtonTextFont:(UIFont *)font {
    buttonView.buttonViewLabel.font = font;
    buttonView.fontOriginSize = font.pointSize;
}

- (void)setHoursTextFont:(UIFont *)font {
    for (UILabel *label in timeGridView.subviews) {
        label.font = font;
    }
    for (UILabel *label in timeGridViewForeGround.subviews) {
        label.font = font;
    }
}

- (void)setButtonImage:(UIImage *)image {
    buttonView.buttonImageView.image = image;
}

-(void)setButtonLargeImage:(UIImage *)largeImage {
    float buttonViewX = (buttonView.buttonImageView.frame.size.width - largeImage.size.width) / 2;
    float buttonViewY = CGRectGetMaxY(self.leftResizeableImageView.frame) - largeImage.size.height + 9;
    
    buttonView.buttonLargeImageView.frame = CGRectMake(buttonViewX, buttonViewY, largeImage.size.width, largeImage.size.height);
    buttonView.buttonViewLargeLabel.frame = CGRectMake(buttonViewX, buttonViewY + 12, buttonView.buttonLargeImageView.frame.size.width, 25);
    buttonView.buttonLargeImageView.image = largeImage;
}

-(void)setButtonSmallImage:(UIImage *)smallImage {
    buttonView.buttonSmallImageView.image = smallImage;
}

- (void)setBackGroundImage:(UIImage *)image {
    backGroundImageView.image = image;
}

- (void)now {
    isNowMethod = YES;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit |NSMinuteCalendarUnit) fromDate: [NSDate date]];
    
    NSInteger year = [components year];
    NSInteger month = [components month];
    NSInteger day = [components day];
    NSInteger hour = [components hour];
    NSInteger minutes = [components minute];
    
    NSInteger overallTimeInMinutes = hour * MINUTES_IN_HOUR + minutes;
    
    float ratio = ((float)overallTimeInMinutes / MINUTES_IN_DAY);
    if (ratio > value + 0.01) {
        while (ratio>value) {
            [buttonScrollView setContentOffset:CGPointMake(buttonScrollView.contentOffset.x-1, buttonScrollView.contentOffset.y)];
        }
    }else if (ratio < value - 0.01){
        while (ratio<value) {
            [buttonScrollView setContentOffset:CGPointMake(buttonScrollView.contentOffset.x+1, buttonScrollView.contentOffset.y)];
        }
    }
    
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString *hoursString = [numberFormatter stringFromNumber:@(hour)];
    NSString *minutesString = [numberFormatter stringFromNumber:@(minutes)];
    
    buttonView.buttonViewLabel.text = [NSString stringWithFormat:@"%@:%@",hoursString,minutesString];
    buttonView.buttonViewLargeLabel.text = [NSString stringWithFormat:@"%@:%@",hoursString,minutesString];

    isNowMethod = NO;
    
    NSDate *time = self.currentDate;
    components = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit) fromDate: time];
    [components setHour:year];
    [components setHour:month];
    [components setHour:day];
    [components setHour:hour];
    [components setMinute:minutes];
    time = [calendar dateFromComponents:components];
    
    self.timeFromSlider = time;
    [self.myDelegate tvSliderValueChangedToTime:time];
}

- (void)setSliderTime:(NSDate *)time {
    isNowMethod = YES;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate: time];

    self.currentYear = [components year];
    self.currentMonth = [components month];
    self.currentDay = [components day];
    
    NSInteger year = self.currentYear;
    NSInteger month = self.currentMonth;
    NSInteger day = self.currentDay;
    

    NSInteger hour = [components hour];
    NSInteger minutes = [components minute];
    
    NSInteger overallTimeInMinutes = hour * MINUTES_IN_HOUR + minutes;
    
    float ratio = ((float)overallTimeInMinutes / MINUTES_IN_DAY);
    if (ratio > value + 0.01) {
        while (ratio>value) {
            [buttonScrollView setContentOffset:CGPointMake(buttonScrollView.contentOffset.x-1, buttonScrollView.contentOffset.y)];
        }
    } else if (ratio < value - 0.01) {
        while (ratio<value) {
            [buttonScrollView setContentOffset:CGPointMake(buttonScrollView.contentOffset.x+1, buttonScrollView.contentOffset.y)];
        }
    }
    
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString *hoursString = [numberFormatter stringFromNumber:@(hour)];
    NSString *minutesString = [numberFormatter stringFromNumber:@(minutes)];
    
    buttonView.buttonViewLabel.text = [NSString stringWithFormat:@"%@:%@",hoursString,minutesString];
    buttonView.buttonViewLargeLabel.text = [NSString stringWithFormat:@"%@:%@",hoursString,minutesString];
    
    isNowMethod = NO;
    
    
    components = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit) fromDate: time];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    [components setHour:hour];
    [components setMinute:minutes];
    time = [calendar dateFromComponents:components];
    
    self.timeFromSlider = time;
    //[self.myDelegate tvSliderValueChangedToTime:time];
}

// This method is called when we choose a new day and the time slider goes to 00:00
- (void)startOfDay {
    
    NSDate *time = self.currentDate;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit) fromDate: time];
    
    isNowMethod = YES;
    NSInteger hour = 0;
    NSInteger minutes = 0;
    
    [buttonScrollView setContentOffset:CGPointMake(buttonScrollView.frame.size.width-buttonWidth, buttonScrollView.contentOffset.y) animated:NO];

    buttonView.buttonViewLabel.text = [NSString stringWithFormat:@"00:00"];
    buttonView.buttonViewLargeLabel.text = [NSString stringWithFormat:@"00:00"];

    isNowMethod = NO;
    
    [components setHour:hour];
    [components setMinute:minutes];
    time = [calendar dateFromComponents:components];
    
    self.timeFromSlider = time;
    [self.myDelegate tvSliderValueChangedToTime:time];
}

- (void)hideUIElements {

    buttonView.hidden = YES;
    buttonScrollView.hidden = YES;
    
    self.leftResizeableImageView.hidden = YES;
    self.rightResizeableImageView.frame = CGRectMake(0.0, self.rightResizeableImageView.frame.origin.y, self.frame.size.width, self.rightResizeableImageView.frame.size.height);
    
    fillColorView.hidden = YES;
    
}

- (void)showUIElements {
    
    buttonView.hidden = NO;
    buttonScrollView.hidden = NO;
    
    self.leftResizeableImageView.hidden = NO;
    
    fillColorView.hidden = NO;
    
}

#pragma mark TVtimeSliderButtonDelegates

- (void)tvSliderButtonDidShrink {
    self.timeFromSlider = [self getTime];
    [self.myDelegate tvSliderValueChangedToTime:self.timeFromSlider];
    
}



@end
