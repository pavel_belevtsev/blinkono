//
//  TVtimeSlider.h
//  TVinciTimeSlider
//
//  Created by Nimrod Shai on 7/23/13.
//  Copyright (c) 2013 Nimrod Shai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVtimeSliderButton.h"
#import "TVtimeSliderScrollView.h"

@protocol TVTimeSliderDelegate <NSObject>

@required
-(void)tvSliderValueChangedToTime:(NSDate *)time;

@end


@interface TVtimeSlider : UIView <UIScrollViewDelegate,TVtimeSliderButtonDelegate>

@property (nonatomic) id <TVTimeSliderDelegate> myDelegate;
@property (nonatomic) CGFloat value;
@property (nonatomic) NSInteger buttonWidth;
@property (nonatomic, strong) NSDate *currentDate;
@property (nonatomic, strong) NSDate *timeFromSlider;
@property (nonatomic) NSInteger currentDay;
@property (nonatomic) NSInteger currentMonth;
@property (nonatomic) NSInteger currentYear;

// *** Colors ***

@property (nonatomic,strong) UIColor *leftSideColor;
@property (nonatomic,strong) UIColor *rightSideColor;
@property (nonatomic,strong) UIColor *buttonTextColor;
@property (nonatomic,strong) UIColor *hoursTextColor;
@property (nonatomic,strong) UIColor *hoursForeGroundTextColor;


// *** fonts ***

@property (nonatomic,strong) UIFont *buttonTextFont;
@property (nonatomic,strong) UIFont *hoursTextFont;

// *** images ***

@property (nonatomic,strong) UIImage *buttonImage;
@property (nonatomic,strong) UIImage *buttonLargeImage;
@property (nonatomic,strong) UIImage *buttonSmallImage;
@property (nonatomic,strong) UIImage *backGroundImage;

- (id)initWithFrame:(CGRect)frame buttonWidth:(int)buttonWidth;
-(void)now;
-(void)startOfDay;
- (void)setSliderTime:(NSDate *)time;
- (void)hideUIElements;
- (void)showUIElements;

@end
