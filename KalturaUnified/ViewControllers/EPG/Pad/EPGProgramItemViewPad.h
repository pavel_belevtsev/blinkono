//
//  EPGProgramItemViewPad.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.09.13.
//  Copyright (c) 2013 Quickode Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPGProgramItemView.h"
#import "LikeAdapter.h"

@interface EPGProgramItemViewPad : EPGProgramItemView {
    
    IBOutlet UIView *viewProgress;
    
    IBOutlet UILabel *labelTime;
    IBOutlet UILabel *labelProgram;
    IBOutlet UILabel *labelDescription;
}
@property (nonatomic, retain) NSArray *constraintsActionButtonsWidth, *constraintsActionButtonsHeight;
@property (nonatomic, weak) IBOutlet UIView *viewActionButtons;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic, weak) IBOutlet UIImageView *recoredIconImage;
@property (nonatomic, strong) UIView *viewInfoBubble;
@property (nonatomic, strong) NSLayoutConstraint *constraintViewInfoBubbleWidth;

-(void)showRecoredItem:(BOOL)isSeries;
- (void)skinChanged;

@end
