//
//  EPGSelectTimeViewPad.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSelectTimeView.h"
#import "TVtimeSlider.h"

@interface EPGSelectTimeViewPad : EPGSelectTimeView <TVTimeSliderDelegate>

@property (nonatomic, strong) TVtimeSlider *timeSlider;
               
@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewSelectTime;
@property (nonatomic, weak) IBOutlet UIView *viewSelectedTime;
@property (nonatomic, weak) IBOutlet UILabel *labelSelectTime;
@property (nonatomic, weak) IBOutlet UILabel *labelSelectTimeBubble;
@property (nonatomic, weak) IBOutlet UIImageView *imgBubbleTime;

@end
