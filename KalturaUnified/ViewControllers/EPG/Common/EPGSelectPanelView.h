//
//  EPGSelectPanelView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 05.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"

@protocol EPGSelectPanelViewDelegate <NSObject>

- (void)updateTimeShowBar;
- (void)updateTimeLabel;
- (void)updateDateLabel;
- (void)hideBarProcess;
- (void)hideBarCompleteProcess;

@end

@interface EPGSelectPanelView : UINibView


@property (nonatomic, weak) id <EPGSelectPanelViewDelegate> delegate;

@end
