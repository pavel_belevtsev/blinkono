//
//  EPGSortViewVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/14/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGSortViewVF.h"
#import "CategorySortTableViewCell.h"

/* "PSC : Vodafone Grup : change select cell to be mark by V and not by BG color : JIRA ticket. */

@implementation EPGSortViewVF

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //TODO: aviv hack - change class name - we can simplify the class if we move the
    // cellIdentifier string to a property in the product - this way we won't need to duplicate this func...
    static NSString *cellIdentifier = @"CategorySortTableViewCellVF";
    
    CategorySortTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:tableView options:nil] objectAtIndex:0];
    }
    
    cell.selected = NO;
    
    if (sortOnlyMode) {
        
        NSDictionary *item = [self.filterByArray objectAtIndex:indexPath.row];
        
        cell.labelSort.text = [item objectForKey:@"title"];
        int sortBy = [[item objectForKey:@"sortBy"] intValue];
        
        cell.cellSelected = (sortBy == self.orderBy);
        //cell.labelSort.text = [[_filterByArray objectAtIndex:indexPath.row] objectForKey:@"title"];
        
    } else {
        
        TVMenuItem *menuItem = [self.filterByArray objectAtIndex:indexPath.row];
        
        if ([menuItem isKindOfClass:[TVMenuItem class]]) {
            
            cell.labelSort.text = menuItem.name;
            
            if ([self.filters count] && [menuItem isEqual:[self.filters lastObject]]) {
                cell.cellSelected = YES;
                
            }
            
        } else {
            
            cell.labelSort.text = (NSString *)menuItem;
            
        }
        
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

@end
