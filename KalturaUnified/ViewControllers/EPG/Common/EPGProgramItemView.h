//
//  EPGProgramItemView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 08.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"
#import <TvinciSDK/TVNPVRRecordItem.h>
#import "RecoredHelper.h"

@class EPGProgramItemView;
@protocol EPGProgramItemViewDelegate <NSObject>

-(void) programItemView:(EPGProgramItemView *) sender didSelectPlayProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem;
-(void) programItemView:(EPGProgramItemView *) sender didSelectRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem isSeries:(BOOL)isSeries;
-(void) programItemView:(EPGProgramItemView *) sender didSelectCancelRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem recoredItem:(TVNPVRRecordItem *) recoredItem isSeries:(BOOL)isSeries;

@end


@interface EPGProgramItemView : UINibView

- (void)setChannelData:(TVMediaItem *)data;
- (void)updateProgramData:(APSTVProgram *)program;

@property (nonatomic, strong) APSTVProgram *item;
@property (nonatomic, strong) TVMediaItem *mediaItem;
@property (nonatomic, strong) TVNPVRRecordItem * recordItem;

@property (nonatomic, weak) IBOutlet UIButton *buttonSelect;
@property (weak, nonatomic) IBOutlet UIButton *buttonRecored;

@property (weak, nonatomic) id<EPGProgramItemViewDelegate> delegate;

@property (strong, nonatomic) RecoredHelper * recoredHelper;
@end
