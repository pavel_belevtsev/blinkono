//
//  EPGSelectDateView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSelectDateView.h"
#import "NSDate+Utilities.h"

@implementation EPGSelectDateView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    todayWeekday = [comps weekday];
    
}

- (void)updateSelectDateBubble {
    
    
}

- (NSString *)dateBubbleTitle {
    
    return nil;
    
}

- (void)setToday {
    
}

- (void)setDay:(NSInteger)day {
    
    
}

- (NSString *)dateTitle:(NSInteger)day {
    
    if (day == APST.numberOfRetroDays) {
        return [LS(@"today") uppercaseString];
    } else {
        
        if (day == APST.numberOfRetroDays - 1) {
            
            return [LS(@"yesterday") uppercaseString];
            
        } else if (day == APST.numberOfRetroDays + 1) {
            
            return [LS(@"tomorrow") uppercaseString];
            
        } else {
            NSInteger wDay = todayWeekday + day - APST.numberOfRetroDays;
            
            while (wDay < 1) wDay += 7;
            while (wDay > 7) wDay -= 7;
            
            NSString *str = [@"wday" stringByAppendingString:[Utils getStringFromInteger:wDay]];
            NSString *text = LS(str);
            
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:24 * 60 * 60 * (day - APST.numberOfRetroDays)];
            
            return [NSString stringWithFormat:@"%@ %@", text, [date stringFromDateWithDateFormat:@"d/M"]];
            
        }
        
    }
    
}

- (NSString *)selectedDateTitle {
    
    if (_selectedDay == 0) {
        return LS(@"today");
    } else {
        
        if (_selectedDay == - 1) {
            
            return LS(@"yesterday");
            
        } else if (_selectedDay == +1) {
            
            return LS(@"tomorrow");
            
        } else {
            
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:24 * 60 * 60 * (_selectedDay)];
            
            return [date stringFromDateWithDateFormat:@"EEEE d/M"];
            
        }
        
    }

}

- (void)updateSearchResults:(NSArray *)days {
    
}

- (void)clearSearchResults {
    
}

- (void)clearSearchResultsAndDisableButtons{
    
}

- (void)skinChanged {
    
}

- (void)restoreButtons {
}


@end
