//
//  EPGSelectTimeView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"

@protocol EPGSelectTimeViewDelegate <NSObject>

- (void)updateTimeShowBar;
- (void)updateTimeLabel;
- (void)hideBarProcess;
- (void)hideBarCompleteProcess;

@end

@interface EPGSelectTimeView : UINibView

- (void)updateSelectedTime:(NSTimeInterval)currentInterval;
- (NSString *)timeBubbleTitle;
- (void)stopScrollView;
- (void)updateForSearchMode:(BOOL)searchMode;
- (void)skinChanged;

@property (nonatomic, weak) IBOutlet UIView *viewSelectedTimeBubble;

@property (nonatomic, weak) id <EPGSelectTimeViewDelegate> delegate;

@property NSTimeInterval currentInterval;

@end
