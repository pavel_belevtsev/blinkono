//
//  EPGSortView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSortView.h"
#import "CategorySortTableViewCell.h"

@implementation EPGSortView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.tableViewFilter.layer.cornerRadius = 6;
    self.tableViewFilter.layer.masksToBounds = YES;
    
    _labelFilterBy.text = [LS(@"browse") uppercaseString];
 
    maxFilterTableHeight = _tableViewFilter.frame.size.height;
    
}

- (void)updateSizes {
    
    if (maxFilterTableHeight == 0) {
        maxFilterTableHeight = _tableViewFilter.frame.size.height;
        
        [self changeTableSize];
        
    }
    
}

- (void)updateWithMenuItem:(TVMenuItem *)menuItem filters:(NSArray *)filters sortOnly:(BOOL)sortOnly orderBy:(TVOrderBy)orderBy {
    
    _menuItem = menuItem;
    _filters = filters;
    
    sortOnlyMode = sortOnly;
    _orderBy = orderBy;
    
    if (sortOnlyMode) {
        
        _labelFilterBy.text = LS(@"category_page_sort_by");
        
        _filterByArray = @[@{@"title":LS(@"category_page_newest"), @"sortBy":@(TVOrderByDateAdded)},
                           @{@"title":LS(@"category_page_most_popular"), @"sortBy":@(TVOrderByNumberOfViews)},
                           @{@"title":LS(@"category_page_most_liked"), @"sortBy":@(TVOrderByRating)}];
        
    } else {
        
        if ([filters count]) {
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            [array addObject:LS(@"all_filters")];
            [array addObject:[_filters lastObject]];
            
            _filterByArray = array;
            
        } else {
            
            //_filterByArray = _menuItem.children;
            
            BOOL isSignIn = [LoginM isSignedIn];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            if (isSignIn) [array addObject:LS(@"my_channels")];
            
            for (id item in _menuItem.children) {
                [array addObject:item];
            }
            
            _filterByArray = array;
        }
    }

    _labelFilterBy.hidden = ![_filterByArray count];
    
    [_tableViewFilter reloadData];
    
    [self changeTableSize];
}

- (void)changeTableSize {
    
    CGFloat height = [_filterByArray count] * sortRowHeight;
    if (height > maxFilterTableHeight) height = maxFilterTableHeight;
    
    [UIView animateWithDuration:0.3 animations:^{
        _tableViewFilter.frame = CGRectMake(_tableViewFilter.frame.origin.x, _tableViewFilter.frame.origin.y, _tableViewFilter.frame.size.width, height);
        if (isPad) {
            
            [_tableViewFilter superview].frame = CGRectMake([_tableViewFilter superview].frame.origin.x, [_tableViewFilter superview].frame.origin.y, [_tableViewFilter superview].frame.size.width, height + 85.0);
            
        }
    }];
}

#pragma mark - UITableViewCell

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_filterByArray count];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CategorySortTableViewCell";
    
    CategorySortTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:tableView options:nil] objectAtIndex:0];
    }
    
    cell.selected = NO;
    
    if (sortOnlyMode) {
        
        NSDictionary *item = [_filterByArray objectAtIndex:indexPath.row];
        
        cell.labelSort.text = [item objectForKey:@"title"];
        int sortBy = [[item objectForKey:@"sortBy"] intValue];
        
        cell.cellSelected = (sortBy == _orderBy);
        //cell.labelSort.text = [[_filterByArray objectAtIndex:indexPath.row] objectForKey:@"title"];
        
    } else {
        
        TVMenuItem *menuItem = [_filterByArray objectAtIndex:indexPath.row];
        
        if ([menuItem isKindOfClass:[TVMenuItem class]]) {
            
            cell.labelSort.text = menuItem.name;
            
            if ([_filters count] && [menuItem isEqual:[_filters lastObject]]) {
                cell.cellSelected = YES;
                
            }
            
        } else {
            
            cell.labelSort.text = (NSString *)menuItem;
            
        }
        
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (sortOnlyMode) {
        
        NSDictionary *item = [_filterByArray objectAtIndex:indexPath.row];
        
        _orderBy = [[item objectForKey:@"sortBy"] intValue];
        
        [tableView reloadData];
        
        [_delegate selectSortBy:_orderBy];
        
        [_delegate closeSelectSortScreen];
        
    } else {
        
        
        TVMenuItem *item = [_filterByArray objectAtIndex:indexPath.row];
        
        if ([item isKindOfClass:[TVMenuItem class]]) {
            
            if (![item.children count]) {
                
                //[[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
                [_delegate closeSelectSortScreen];
            }
            
            [_delegate selectFilterOption:item];
            
        } else {
            
            if ([_filters count]) {
                if (indexPath.row == 0) {
                    [_delegate selectFilterBack];
                } else {
                    
                    //[[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
                    [_delegate closeSelectSortScreen];
                    
                }
            } else {
                [_delegate selectMyChannel:((NSString *)item)];
                
                //[[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
                [_delegate closeSelectSortScreen];
                
            }
            
        }
        
    }
    
    
}


@end
