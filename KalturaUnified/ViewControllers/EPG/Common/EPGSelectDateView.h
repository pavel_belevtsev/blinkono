//
//  EPGSelectDateView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"

@protocol EPGSelectDateViewDelegate <NSObject>

- (void)updateTimeShowBar;
- (void)updateDateLabel;
- (void)hideBarProcess;
- (void)hideBarCompleteProcess;

@end

@interface EPGSelectDateView : UINibView {
        
    NSInteger todayWeekday;

}

- (void)updateSelectDateBubble;
- (NSString *)dateBubbleTitle;
- (NSString *)selectedDateTitle;
- (void)setToday;
- (void)setDay:(NSInteger)day;
- (NSString *)dateTitle:(NSInteger)day;
- (void)updateSearchResults:(NSArray *)days;
- (void)clearSearchResults;
- (void)clearSearchResultsAndDisableButtons;
- (void)skinChanged;
- (void)restoreButtons;

@property (nonatomic, weak) id <EPGSelectDateViewDelegate> delegate;

@property (nonatomic, weak) IBOutlet UIImageView *imgSelectDayShadow;
@property (nonatomic, weak) IBOutlet UIView *viewSelectedDateBubble;

@property NSInteger selectedDay;

@end
