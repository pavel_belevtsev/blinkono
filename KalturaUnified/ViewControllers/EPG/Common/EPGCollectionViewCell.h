//
//  EPGCollectionViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 05.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPGProgramItemView.h"
#import <TvinciSDK/TVNPVRRecordItem.h>
#import "RecoredHelper.h"

@class EPGCollectionViewCell;




@protocol EPGCollectionViewCellDelegate <NSObject>

- (void)openMediaItem:(TVMediaItem *)mediaItem;
- (void)addMediaToFavorites:(TVMediaItem *)mediaItem;
- (void)removeMediaFromFavorites:(TVMediaItem *)mediaItem;
- (void)openChannelViewController:(UICollectionViewCell *)collectionViewCell;
- (void) epgCell:(EPGCollectionViewCell *) cell didSelectPlayProgrma:(APSTVProgram *) program channel:(TVMediaItem *) mediaItem;
- (void) epgCell:(EPGCollectionViewCell *) cell programItemView:(EPGProgramItemView *) programItemView didSelectRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem isSeries:(BOOL)isSeries;

- (void) epgCell:(EPGCollectionViewCell *) cell programItemView:(EPGProgramItemView *) programItemView didSelectCancelRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem recoredItem:(TVNPVRRecordItem *) recoredItem isSeries:(BOOL)isSeries;

//- (void) epgCell:(EPGCollectionViewCell *) cell didSelectPlayProgrma:(APSTVProgram *) program channel:(TVMediaItem *) mediaItem;


@end

@interface EPGCollectionViewCell : UICollectionViewCell <UIGestureRecognizerDelegate,EPGProgramItemViewDelegate> {
    
    BOOL swipeEnable;
    
    BOOL dragEnable;
    BOOL canChangeFavorite;
    
    CFAbsoluteTime lastSwipeTime;
    
    float lastScrollValue;
    
}

- (void)updateCellWithMediaItem:(TVMediaItem *)mediaItem;
- (void)updateProgramItem:(NSInteger)index;
- (void)updateProgramItem:(NSInteger)index animated:(BOOL)animated;

@property (nonatomic, strong) TVMediaItem *channel;
@property (nonatomic, strong) NSArray *epgList;
@property (nonatomic, strong) APSTVProgram *program;
@property (nonatomic) NSInteger currentProgramIndex;

@property (nonatomic, weak) IBOutlet UIImageView *imgThumb;

@property (nonatomic, weak) IBOutlet UIView *viewFavSign;
@property (nonatomic, weak) IBOutlet UIImageView *imgFavBg;
@property (nonatomic, weak) IBOutlet UIImageView *imgFavSign;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollChannelLogo;

@property (nonatomic, weak) IBOutlet UIView *viewEPG;
@property (nonatomic, weak) IBOutlet UIView *viewLoading;

@property (nonatomic, weak) IBOutlet UILabel *labelChanelNumber;
@property (nonatomic, weak) IBOutlet UILabel *labelChanelName;
@property (nonatomic, weak) IBOutlet UILabel *labelNoEPG;

@property (nonatomic, weak) id <EPGCollectionViewCellDelegate> delegate;

@property (nonatomic) BOOL isFavorite;

@property (nonatomic, strong)  NSMutableDictionary * recoredsOfChannel;
@property (strong, nonatomic) RecoredHelper * currRecoredHelper;

@property (strong, nonatomic) NSString *epgIdentifier;

-(void)addSwipeGestureRecognizer;
@end
