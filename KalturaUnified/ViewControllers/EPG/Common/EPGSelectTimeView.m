//
//  EPGSelectTimeView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSelectTimeView.h"

@implementation EPGSelectTimeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)updateSelectedTime:(NSTimeInterval)currentInterval {
    
}

- (void)stopScrollView {
    
}

- (void)updateForSearchMode:(BOOL)searchMode {
    
    
}

- (NSString *)timeBubbleTitle {
    
    return nil;
    
}

- (void)skinChanged {
    
}

@end
