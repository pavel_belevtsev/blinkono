//
//  EPGSortView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"

@protocol EPGSortViewDelegate <NSObject>

- (void)selectFilterOption:(TVMenuItem *)filterItem;
- (void)selectMyChannel:(NSString *)filterItem;
- (void)selectFilterBack;
- (void)selectSortBy:(TVOrderBy)orderBy;
- (void)closeSelectSortScreen;

@end

@interface EPGSortView : UINibView  {
    
    CGFloat maxFilterTableHeight;
    
    BOOL sortOnlyMode;
    
}

@property (weak, nonatomic) IBOutlet UILabel *labelFilterBy;
@property (weak, nonatomic) IBOutlet UITableView *tableViewFilter;

@property (strong, nonatomic) NSArray *filterByArray;

@property (nonatomic, weak) id <EPGSortViewDelegate> delegate;

@property (weak, nonatomic) TVMenuItem *menuItem;
@property (weak, nonatomic) NSArray *filters;

@property (nonatomic) TVOrderBy orderBy;

- (void)updateSizes;
- (void)updateWithMenuItem:(TVMenuItem *)menuItem filters:(NSArray *)filters sortOnly:(BOOL)sortOnly orderBy:(TVOrderBy)orderBy;

@end
