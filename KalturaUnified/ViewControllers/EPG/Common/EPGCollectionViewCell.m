//
//  EPGCollectionViewCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 05.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGCollectionViewCell.h"
#import "UIImage+Tint.h"

@implementation EPGCollectionViewCell

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    
}

- (void)awakeFromNib {
    // Initialization code
    
    self.currentProgramIndex = -1;

    [Utils setLabelFont:self.labelChanelNumber bold:YES];
    
    [Utils setLabelFont:self.labelNoEPG];
    self.labelNoEPG.text = LS(@"tv_guide_empty_item_msg");
    self.labelNoEPG.hidden = YES;
    
    EPGProgramItemView *itemView = [[EPGProgramItemView alloc] initWithNib];
    itemView.delegate = self;
    
    itemView.alpha = 0;
    [self.viewEPG addSubview:itemView];
    if (isPhone)  [itemView.buttonSelect addTarget:self action:@selector(tapProgram) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSwipeGestureRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];

    [self skinChanged];
    
}

-(void)addSwipeGestureRecognizer
{
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft)];
    recognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    recognizer.delegate = self;
    [self.viewEPG addGestureRecognizer:recognizer];
    
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    recognizer.direction = UISwipeGestureRecognizerDirectionRight;
    recognizer.delegate = self;
    [self.viewEPG addGestureRecognizer:recognizer];
}

- (void)skinChanged {
    
    self.imgFavSign.image = [[UIImage imageNamed:@"epg_fav_sign"] imageTintedWithColor:APST.brandColor];
    self.imgFavBg.image = [[UIImage imageNamed:@"epg_fav_bg"] imageTintedWithColor:APST.brandColor];

}

- (void)tapProgram {
    
    if (_epgList && _currentProgramIndex < [_epgList count]) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(openChannelViewController:)]) {
            [_delegate openChannelViewController:self];
        }
    }
    
}

- (void)updateCellWithMediaItem:(TVMediaItem *)mediaItem {
    
    BOOL isSignIn = [LoginM isSignedIn];
    
    swipeEnable = NO;
    
    dragEnable = YES;
    canChangeFavorite = YES;
    
    _scrollChannelLogo.scrollEnabled = isSignIn;
    _viewFavSign.hidden = !isSignIn;
    
    self.isFavorite = mediaItem.isFavorite;
    _viewFavSign.frame = CGRectMake(_viewFavSign.frame.origin.x, (self.isFavorite ? 0 : -_viewFavSign.frame.size.height), _viewFavSign.frame.size.width, _viewFavSign.frame.size.height);
    
}

- (void)updateProgramItem {
    
}

- (BOOL)sameDate:(APSTVProgram *)program1 program2:(APSTVProgram *)program2 {
    
    return YES;
    /*
     NSDate *date1 = program1.startDateTime;
     NSDate *date2 = program2.startDateTime;
     
     NSDateFormatter *df = [[NSDateFormatter alloc] init];
     [df setDateFormat:@"d"];
     
     int day1 = [[df stringFromDate:date1] intValue];
     int day2 = [[df stringFromDate:date2] intValue];
     
     return (day1 == day2);
     */
}

- (void)swipeLeft
{
    
    NSArray *epgList = self.epgList;
    
    if (epgList && swipeEnable && self.currentProgramIndex < [epgList count] && self.currentProgramIndex < [epgList count] - 1) {
        
        APSTVProgram *programC = [epgList objectAtIndex:self.currentProgramIndex];
        
        APSTVProgram *program = [epgList objectAtIndex:self.currentProgramIndex + 1];
        
        if ([self sameDate:programC program2:program])
        {
            self.program = program;
            self.currRecoredHelper = [self.recoredsOfChannel objectForKey:self.program.epgId];
            [self updateProgramItem:self.currentProgramIndex + 1 animated:YES];
        }
        
    }
    
}

- (void)swipeRight {
    
    NSArray *epgList = self.epgList;
    
    if (epgList && swipeEnable && self.currentProgramIndex < [epgList count] && self.currentProgramIndex > 0) {
        
        APSTVProgram *programC = [epgList objectAtIndex:self.currentProgramIndex];
        
        APSTVProgram *program = [epgList objectAtIndex:self.currentProgramIndex - 1];
        
        if ([self sameDate:programC program2:program]) {
            self.program = program;
            self.currRecoredHelper = [self.recoredsOfChannel objectForKey:self.program.epgId];
            [self updateProgramItem:self.currentProgramIndex - 1 animated:YES];
        }
    }
    
}


- (void)updateProgramItem:(NSInteger)index {
    
    [self updateProgramItem:index animated:NO];
    
}

- (void)updateProgramItem:(NSInteger)index animated:(BOOL)animated {
    
    if ([_epgList count] == 0) {
        
        for (EPGProgramItemView *subview in [self.viewEPG subviews]) {
            
            subview.alpha = 0;
            
        }
        self.labelNoEPG.hidden = NO;
        
        return;
        
    }
    self.labelNoEPG.hidden = YES;
    
    swipeEnable = NO;
    
    NSInteger pIndex = self.currentProgramIndex;
    
    self.currentProgramIndex = index;
    
    EPGProgramItemView *itemCurrentView = nil;
    
    EPGProgramItemView *itemView = nil;
    EPGProgramItemView *itemNextView = nil;
    
    //NSLog(@"count subview %d", [[self.viewEPG subviews] count]);
    
    for (EPGProgramItemView *subview in [self.viewEPG subviews]) {
        [subview setChannelData:self.channel];
        
        if (subview.alpha == 0) {
            itemView = subview;
        } else {
            
            if (subview.tag == 10) {
                itemCurrentView = subview;
            } else if (subview.tag == 100) {
                itemNextView = subview;
            }
            
        }
        
    }
    
  if (itemCurrentView && itemCurrentView.item && self.program && ( [itemCurrentView.item.epgIdentifier isEqualToString:self.program.epgIdentifier] || (itemCurrentView.item.epgIdentifier == nil && self.program.epgIdentifier ==nil)))
  {
        [itemCurrentView setRecoredHelper:self.currRecoredHelper];
        swipeEnable = YES;
      
      if (self.program)
      {
          [itemCurrentView updateProgramData:self.program];
          [itemCurrentView setRecoredHelper:self.currRecoredHelper];
          return;
      }
  }
    
    if (itemView == nil) {
        
        itemView = [[EPGProgramItemView alloc] initWithNib];
        itemView.delegate = self;
        
        [self.viewEPG addSubview:itemView];
        if (isPhone)  [itemView.buttonSelect addTarget:self action:@selector(tapProgram) forControlEvents:UIControlEventTouchUpInside];
        if (isPad)  [itemView.buttonSelect addTarget:self action:@selector(buttonChannelPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    [itemView setChannelData:self.channel];
    
    if (self.program) [itemView updateProgramData:self.program];
    self.currRecoredHelper = [self.recoredsOfChannel objectForKey:self.program.epgId];

    [itemView setRecoredHelper:self.currRecoredHelper];
    itemView.alpha = 1.0;
    itemCurrentView.alpha = 1.0;
    itemView.frame = CGRectMake(pIndex <= self.currentProgramIndex ? itemView.frame.size.width + 20.0 : - itemView.frame.size.width, itemView.frame.origin.y, itemView.frame.size.width, itemView.frame.size.height);
    
    [self.viewEPG bringSubviewToFront:itemView];
    
    float time = (animated ? 0.5 : 0.0);
    
    self.epgIdentifier = self.program.epgIdentifier;
    
    if (animated) {
    
        [UIView animateWithDuration:time animations:^{
            
            itemView.alpha = 1.0;
            itemView.tag = 10;
            itemView.frame = CGRectMake(0, itemView.frame.origin.y, itemView.frame.size.width, itemView.frame.size.height);
            
            if (itemCurrentView) {
                
                itemCurrentView.alpha = 1.0;
                itemCurrentView.frame = CGRectMake(pIndex <= self.currentProgramIndex ? -itemCurrentView.frame.size.width : itemCurrentView.frame.size.width, itemCurrentView.frame.origin.y, itemCurrentView.frame.size.width, itemCurrentView.frame.size.height);
                
            }
            
            if (itemNextView) {
                
                itemNextView.frame = CGRectMake(pIndex <= self.currentProgramIndex ? 0 : ((itemNextView.frame.size.width * 2) + 20.0), itemNextView.frame.origin.y, itemNextView.frame.size.width, itemNextView.frame.size.height);
                
            }
            
        } completion:^(BOOL finished) {
            
            swipeEnable = YES;
            
            itemCurrentView.alpha = 0.0;
            if (itemNextView) itemNextView.alpha = 0.0;
            
            [self showNextProgram];
            
        }];
        
    } else {
        
        
        itemView.alpha = 1.0;
        itemView.tag = 10;
        itemView.frame = CGRectMake(0, itemView.frame.origin.y, itemView.frame.size.width, itemView.frame.size.height);
        swipeEnable = YES;
        
        itemCurrentView.alpha = 0.0;
        if (itemNextView) itemNextView.alpha = 0.0;
        
        [self showNextProgram];
        
    }
    
}

- (void)showNextProgram {
    
    NSArray *epgList = self.epgList;
    
    if (isPad && epgList && self.currentProgramIndex < [epgList count] - 1) {
        
        APSTVProgram *program = [epgList objectAtIndex:self.currentProgramIndex + 1];
        RecoredHelper * nextRecoredHelper = [self.recoredsOfChannel objectForKey:program.epgId];
        EPGProgramItemView *itemView = nil;
        
        for (EPGProgramItemView *subview in [self.viewEPG subviews]) {
            
            if (subview.alpha == 0) {
                itemView = subview;
             }
            
        }
        
        if (itemView == nil) {
            
            itemView = [[EPGProgramItemView alloc] initWithNib];
            itemView.delegate = self;
            [self.viewEPG addSubview:itemView];
            
            
        }
        [itemView setChannelData:self.channel];
        
        [itemView updateProgramData:program];
        
        if (nextRecoredHelper) [itemView setRecoredHelper:nextRecoredHelper];

        itemView.alpha = 1.0;
        itemView.tag = 100;
        itemView.frame = CGRectMake(itemView.frame.size.width + 20.0, itemView.frame.origin.y, itemView.frame.size.width, itemView.frame.size.height);
        
        
    }
    
}

#pragma mark - Buttons

- (IBAction)buttonChannelPressed:(UIButton *)button {
    
    if (_channel && _delegate) {
        [_delegate openMediaItem:_channel];
    }
}

#pragma mark - Scroll Favorite

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView.contentOffset.x > 0) {
        scrollView.contentOffset = CGPointZero;
    } else if (scrollView.contentOffset.x < 0) {
        
        float maxOfset = _imgFavBg.frame.size.width * 0.9;
        
        if (scrollView.contentOffset.x < -maxOfset) {
            scrollView.contentOffset = CGPointMake(-maxOfset, 0);
        }
        
        if (lastScrollValue < scrollView.contentOffset.x && !dragEnable) {
            dragEnable = YES;
        }
        
        float offset = -scrollView.contentOffset.x;
        
        float dOffset = (dragEnable ? self.isFavorite : !self.isFavorite) ? (offset / maxOfset) : (1.0 - offset / maxOfset);
        
        if (lastScrollValue > scrollView.contentOffset.x || canChangeFavorite) {
            
            _viewFavSign.frame = CGRectMake(_viewFavSign.frame.origin.x, -_viewFavSign.frame.size.height * dOffset, _viewFavSign.frame.size.width, _viewFavSign.frame.size.height);
            
        }
        
        if (offset > maxOfset * 0.9 && canChangeFavorite) {
            
            self.isFavorite = !self.isFavorite;
            dragEnable = NO;
            canChangeFavorite = NO;
            self.channel.isFavorite = self.isFavorite;
            if (self.isFavorite) {
        
                [_delegate addMediaToFavorites:self.channel];
                
            } else {
                
                [_delegate removeMediaFromFavorites:self.channel];
                
            }
            //[self.delegateView favoriteIsChanged:self];
            
            scrollView.userInteractionEnabled = NO;
            
        }
        
        
    } else {
        
        dragEnable = YES;
        canChangeFavorite = YES;
        _viewFavSign.frame = CGRectMake(_viewFavSign.frame.origin.x, (self.isFavorite ? 0 : -_viewFavSign.frame.size.height), _viewFavSign.frame.size.width, _viewFavSign.frame.size.height);
        
        scrollView.userInteractionEnabled = YES;
        
    }
    
    lastScrollValue = scrollView.contentOffset.x;
    
}


#pragma mark -  EPGProgramItemViewDelegate protocol

-(void) programItemView:(EPGProgramItemView *) sender didSelectPlayProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem
{
    if ([self.delegate respondsToSelector:@selector(epgCell:didSelectPlayProgrma:channel:)])
    {
        [self.delegate epgCell:self didSelectPlayProgrma:program channel:mediaItem];
    }
}

-(void) programItemView:(EPGProgramItemView *) sender didSelectRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem isSeries:(BOOL)isSeries
{
    if ([self.delegate respondsToSelector:@selector(epgCell:programItemView:didSelectRecoredProgram:mediaItem:isSeries:)])
    {
        [self.delegate epgCell:self programItemView:sender didSelectRecoredProgram:program mediaItem:mediaItem isSeries:isSeries];
    }
}

-(void) programItemView:(EPGProgramItemView *) sender didSelectCancelRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem recoredItem:(TVNPVRRecordItem *) recoredItem isSeries:(BOOL)isSeries
{
    if ([self.delegate respondsToSelector:@selector(epgCell:programItemView:didSelectCancelRecoredProgram:mediaItem:recoredItem:isSeries:)])
    {
        [self.delegate epgCell:self programItemView:sender didSelectCancelRecoredProgram:program mediaItem:mediaItem recoredItem:recoredItem isSeries:isSeries];
    }
}


@end
