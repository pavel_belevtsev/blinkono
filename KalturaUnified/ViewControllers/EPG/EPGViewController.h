//
//  EPGViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPGSelectDateView.h"
#import "EPGSelectTimeView.h"
#import "EPGSortView.h"
#import "SearchListView.h"
#import "EPGCollectionViewCell.h"
#import "HomeNavProfileView.h"
#import "SearchBlankView.h"
#import "EpgRecoredPopUp.h"
#import "CancelSeriesRecordingPopUp.h"
#import "EPGRcoredStatusView.h"
#import "RecoredAdapter.h"


@class SocialManagement;

@interface EPGViewController : BaseViewController <EPGSelectDateViewDelegate, EPGSelectTimeViewDelegate, EPGSortViewDelegate, UITextFieldDelegate, EPGCollectionViewCellDelegate, HomeNavProfileViewDelegate> {
    
    BOOL isInitialized;
    
    CFAbsoluteTime timeShowBar;
    
    NSTimeInterval currentInterval;
    
    BOOL sortOnly;
    
    BOOL searchMode;

    BOOL useMyChannels;
    
    BOOL buttonDaySelected, buttonTimeSelected;
    
}

@property (nonatomic, strong) TVMenuItem *epgMenuItem;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;


@property (nonatomic, weak) IBOutlet UIView *viewButtonsPhone;
@property (nonatomic, weak) IBOutlet UIButton *buttonNow;
@property (nonatomic, weak) IBOutlet UIButton *buttonDate;
@property (nonatomic, weak) IBOutlet UIButton *buttonTime;

@property (nonatomic, weak) IBOutlet UIButton *buttonSearch;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionViewEPG;

@property (nonatomic, weak) IBOutlet UIView *viewDatePad;
@property (nonatomic, weak) IBOutlet UIImageView *imgViewBottom;
@property (nonatomic, weak) IBOutlet UIView *viewTimePad;

@property (nonatomic, strong) EPGSelectDateView *selectDateView;
@property (nonatomic, strong) EPGSelectTimeView *selectTimeView;
@property (nonatomic, strong) NSTimer *timerHideBar;

@property (nonatomic, readwrite) NSInteger selectedDay;
@property (nonatomic, strong) NSDate *epgDate;

@property (nonatomic, strong) NSMutableDictionary *dictionaryCurrentProgramByChannelID;
@property (nonatomic, strong) NSMutableDictionary *dictionaryRequest;

@property (nonatomic, weak) IBOutlet UIButton *buttonSortOpen;

@property (nonatomic, strong) NSArray *channelsList;
@property (nonatomic, strong) NSMutableArray *filterMenuItemList;
@property (nonatomic, strong) NSArray *searchDays;
@property (nonatomic, strong) NSArray *searchFullListChannels;
@property (nonatomic, weak) IBOutlet UIView *viewSearchForm;
@property (nonatomic, weak) IBOutlet UITextField *textFieldSearch;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewSearchBackground;
@property (nonatomic, strong) SearchListView *searchListView;

@property (nonatomic) TVOrderBy orderBy;

@property (nonatomic, strong) SocialManagement *socialManager;
@property (nonatomic, strong) EPGSortView *sortView;

@property (weak, nonatomic) IBOutlet UIView *viewToolbarBg;
@property (weak, nonatomic) IBOutlet UIButton *buttonSegmentChannels;

@property (strong, nonatomic) SearchBlankView *searchBlankView;

@property (weak, nonatomic) IBOutlet UIView *emptyChannelView;
@property (weak, nonatomic) IBOutlet UILabel *emptyChannelTitle;
@property (weak, nonatomic) IBOutlet UILabel *emptyChannelText;
@property (weak, nonatomic) IBOutlet UIView *leftSideBGView;

@property (nonatomic, strong) UIImageView *imgArrow;

@property (strong, nonatomic) IBOutlet UIView *viewBgPhone;

@property (weak, nonatomic) IBOutlet UIView *recoredStatusView;

@property (strong, nonatomic) EpgRecoredPopUp * epgRecoredPopUp;
@property (strong, nonatomic) CancelSeriesRecordingPopUp* cancelSeriesRecordingPopUp;
@property (strong, nonatomic) EPGRcoredStatusView * recoredStatusPopupView;

@property (strong,nonatomic) NSMutableDictionary * recoredDicByChannels;
@property (nonatomic, strong) NSMutableDictionary *dictionaryNpvrRequest;

@property (strong,nonatomic) RecoredAdapter * recoredAdapter;

@property (nonatomic, weak) IBOutlet UILabel *labelAutomation;

- (APSTVProgram *)programInEPGDate:(NSArray *)array;
- (NSString *)channelSelectedDayKey:(TVMediaItem *)mediaItem;
- (void)loadProgramForChannel:(TVMediaItem *) channel;

- (void)searchProcessComplete:(NSArray *)channelItems;
- (void)updateSearchListResult;
- (void)reloadCollection;
-(void)loadRecoredByChannel:(TVMediaItem *)epgChannel;

- (void)initButtonsPhoneBar;
- (void)epgListInitialization;

@end
