//
//  RecoredHelper.h
//  KalturaUnified
//
//  Created by Israel Berezin on 2/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TvinciSDK/TVNPVRRecordItem.h>
#import <TvinciSDK/APSTVProgram.h>


typedef enum
{
    RecoredHelperNone,
    RecoredHelperCurrenlyRecordeing,
    RecoredHelperRecoreded,
    RecoredHelperCanceling,
    RecoredHelperCancelled
}RecoredHelperStatus;

@interface RecoredHelper : NSObject

@property (strong, nonatomic) APSTVProgram * program;
@property (strong, nonatomic) TVMediaItem * mediaItem;
@property (strong, nonatomic) TVNPVRRecordItem * recordItem;
@property BOOL isSeries;
@property (assign, nonatomic) RecoredHelperStatus recordingItemStatus;

@property (strong,nonatomic ) NSString * recoredId;
@property (strong,nonatomic ) NSString * recoredSereisId;

@end
