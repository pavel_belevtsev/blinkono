//
//  CancelSeriesRecordingPopUp.m
//  KalturaUnified
//
//  Created by Israel Berezin on 2/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CancelSeriesRecordingPopUp.h"
#import "UIImage+Tint.h"

@implementation CancelSeriesRecordingPopUp

- (id)initWithNibWithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView *) linkageView
{
    self = [super initWithNib];
    if (self)
    {
        self.recoredHelper = recoredHelper;
        self.mediaItem = recoredHelper.mediaItem;
        self.program = recoredHelper.program;
        self.recordItem = recoredHelper.recordItem;
        self.linkageView = linkageView;
        
        [self updateTextLabels];
        
        
        [self updateUILocateAndFrameSize];
    }
    return self;
}

-(void)awakeFromNib
{
    [self setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
    [self.mainView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.mainView.layer setShadowOpacity:0.4];
    [self.mainView.layer setShadowRadius:6.0];
    self.mainView.layer.cornerRadius = 3.0;
    [self.mainView.layer setShadowOffset:CGSizeMake(0, 1)];
    [self.mainView setBackgroundColor:CS(@"252525")];
    
    [self updateButtons];
}


-(void)updateButtons
{
    if (isPad)
    {
        [self.btnClose setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"575757")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
        [Utils setButtonFont:self.btnClose bold:YES];
        [self addShadowToButton:self.btnClose];
        [self.btnClose setTitle:LS(@"alert_dialog_close") forState:UIControlStateNormal];
    }
    
    [self.btnDeleteAllRecording setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"575757")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    [Utils setButtonFont:self.btnDeleteAllRecording bold:YES];
    [self addShadowToButton:self.btnDeleteAllRecording];
    
    [self.btnDeleteOnlyThisRecored setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    [Utils setButtonFont:self.btnDeleteOnlyThisRecored bold:YES];
    [self addShadowToButton:self.btnDeleteOnlyThisRecored];
    
    [self.btnDeleteAllRecording setTitle:LS(@"cancelSeriesRecordingPopUp_deleteAllBtn") forState:UIControlStateNormal];
    [self.btnDeleteOnlyThisRecored setTitle:LS(@"cancelSeriesRecordingPopUp_deleteEpisodeBtn") forState:UIControlStateNormal];
    [self updateButtonsTitleColor];
}

-(void)updateButtonsTitleColor
{
    if (isPad)
    {
        [self.btnClose setTitleColor:CS(@"B2B2B2") forState:UIControlStateNormal];
    }
    [self.btnDeleteAllRecording setTitleColor:CS(@"B2B2B2") forState:UIControlStateNormal];
    [self.btnDeleteOnlyThisRecored setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    
}
-(void)updateTextLabels
{
    self.lblTitle.text = LS(@"cancelSeriesRecordingPopUp_Title");
    NSString * discriptionStart = LS(@"cancelSeriesRecordingPopUp_StartDiscriptin");
    NSString * discriptionEnd = LS(@"cancelSeriesRecordingPopUp_EndDiscriptin");
    NSString * name = self.program.name;
    NSString * discription = [NSString stringWithFormat:@"%@ \"%@\" %@",discriptionStart,name,discriptionEnd];
    self.lblDiscription.text = discription;
    if (isPad)
    {
        [Utils setLabelFont:self.lblDiscription size:16 bold:NO];
    }
    else
    {
        [Utils setLabelFont:self.lblDiscription size:15 bold:NO];
    }
    self.lblDiscription.textColor = CS(@"959595");
    
    self.lblDiscription.numberOfLines=0;
    NSInteger programNameLineHeight = 20.f;
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = programNameLineHeight;
    style.maximumLineHeight = programNameLineHeight;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    if (self.lblDiscription.text)
    {
        self.lblDiscription.attributedText = [[NSAttributedString alloc] initWithString:self.lblDiscription.text
                                                                             attributes:attributtes];
    }
    [self.lblDiscription sizeToFit];
    self.lblDiscription.textAlignment = NSTextAlignmentCenter;
    
    if (isPad)
    {
        [Utils setLabelFont:self.lblTitle size:18 bold:YES];
    }
    else
    {
        [Utils setLabelFont:self.lblTitle size:16 bold:YES];
    }
    self.lblTitle.textColor = [UIColor whiteColor];
    
    [self.lblTitle sizeToFit];
}


-(void)updateUILocateAndFrameSize
{
    CGRect titleFrame = self.lblTitle.frame;
    titleFrame.origin.x = self.mainView.frame.size.width/2- self.lblTitle.width/2;
    titleFrame.origin.y = 20;
    self.lblTitle.frame = titleFrame;
    
    CGRect discriptionframe = self.lblDiscription.frame;
    discriptionframe.origin.x = self.mainView.frame.size.width/2 - self.lblDiscription.width/2;
    discriptionframe.origin.y = self.lblTitle.frame.origin.y + self.lblTitle.frame.size.height + 30;
    self.lblDiscription.frame = discriptionframe;
    
    CGRect downViewFrame = self.downView.frame;
    downViewFrame.origin.x = 0;
    downViewFrame.origin.y = self.lblDiscription.frame.origin.y + self.lblDiscription.frame.size.height + 38;
    self.downView.frame = downViewFrame;
    
    CGRect mainViewFrame = self.mainView.frame;
    mainViewFrame.size.height = downViewFrame.origin.y
    + downViewFrame.size.height;
    self.mainView.frame = mainViewFrame;
    
}

-(void)addShadowToButton:(UIButton*)btn
{
    [btn.layer setShadowColor:[UIColor blackColor].CGColor];
    [btn.layer setShadowOpacity:0.4];
    [btn.layer setShadowRadius:2.0];
    [btn.layer setShadowOffset:CGSizeMake(0, 1)];
}

- (IBAction)CloseView:(id)sender
{
    
    if (sender != nil && [self.delegate respondsToSelector:@selector(cancelSeriesRecordingPopUp:didCancelViewWithRecoredHelper:andLinkageView:)])
    {
        [self.delegate cancelSeriesRecordingPopUp:self didCancelViewWithRecoredHelper:self.recoredHelper andLinkageView:self.linkageView];
    }
    self.alpha = 1.0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha= 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];

    }];
}

- (IBAction)deleteAllRecoreds:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(cancelSeriesRecordingPopUp:cnacelRecoredType:WithRecoredHelper:andLinkageView:)])
    {
        [self.delegate cancelSeriesRecordingPopUp:self cnacelRecoredType:CnacelRecoredTypeSeires WithRecoredHelper:self.recoredHelper andLinkageView:self.linkageView];
    }
    [self CloseView:nil];
}

- (IBAction)DeleteOnlyThisRecored:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(cancelSeriesRecordingPopUp:cnacelRecoredType:WithRecoredHelper:andLinkageView:)])
    {
        [self.delegate cancelSeriesRecordingPopUp:self cnacelRecoredType:CnacelRecoredTypeOnlyOldEpisodes WithRecoredHelper:self.recoredHelper andLinkageView:self.linkageView];
    }
    [self CloseView:nil];
}

-(void)updateViewFrameAndCentetMainView:(CGRect ) frame
{
    self.frame = frame;
    self.mainView.center = self.center;
}
@end
