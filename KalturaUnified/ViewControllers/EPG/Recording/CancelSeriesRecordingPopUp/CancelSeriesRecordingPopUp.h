//
//  CancelSeriesRecordingPopUp.h
//  KalturaUnified
//
//  Created by Israel Berezin on 2/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "UINibView.h"
#import <TvinciSDK/TVNPVRRecordItem.h>
#import "RecoredHelper.h"
#import "TVMediaItem+PSTags.h"


typedef enum
{
    CnacelRecoredTypeOnlyOldEpisodes =0,
    CnacelRecoredTypeSeires
}CnacelRecoredType;

@class CancelSeriesRecordingPopUp;

@protocol CancelSeriesRecordingPopUpDelegate <NSObject>

-(void) cancelSeriesRecordingPopUp:(CancelSeriesRecordingPopUp *)epgRecoredPopUp cnacelRecoredType:(CnacelRecoredType)recoredType WithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView*)linkageView;

-(void) cancelSeriesRecordingPopUp:(CancelSeriesRecordingPopUp *)epgRecoredPopUp didCancelViewWithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView*)linkageView;

@end


@interface CancelSeriesRecordingPopUp : UINibView

@property (assign,nonatomic) id<CancelSeriesRecordingPopUpDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscription;
@property (weak, nonatomic) IBOutlet UIView *downView;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteOnlyThisRecored;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteAllRecording;

@property (nonatomic,strong) RecoredHelper * recoredHelper;

@property (nonatomic, strong) UIView * linkageView;
@property (strong, nonatomic) APSTVProgram * program;
@property (strong, nonatomic) TVMediaItem * mediaItem;
@property (strong, nonatomic) TVNPVRRecordItem * recordItem;


- (IBAction)CloseView:(id)sender;
- (IBAction)deleteAllRecoreds:(id)sender;
- (IBAction)DeleteOnlyThisRecored:(id)sender;

- (id)initWithNibWithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView *) linkageView;

-(void)updateViewFrameAndCentetMainView:(CGRect ) frame;

-(void)updateButtonsTitleColor;
-(void)updateTextLabels;
@end
