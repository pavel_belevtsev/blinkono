//
//  CancelSeriesRecordingPopUpVF.m
//  Vodafone
//
//  Created by Israel Berezin on 3/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CancelSeriesRecordingPopUpVF.h"
#import "UIImage+Tint.h"

@implementation CancelSeriesRecordingPopUpVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)updateButtonsTitleColor
{
    [super updateButtonsTitleColor];
    if (isPad)
    {
        [self.btnClose setTitleColor:[VFTypography instance].grayScale5Color forState:UIControlStateNormal];
    }
    [self.btnDeleteAllRecording setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnDeleteOnlyThisRecored setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if (isPad)
    {
        [self.btnDeleteAllRecording setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:[VFTypography instance].grayScale2Color] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    }
    else
    {
        [self.btnDeleteAllRecording setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:[VFTypography instance].grayScale3Color] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];

    }
}

-(void)updateTextLabels
{
    [super updateTextLabels];
    self.lblDiscription.textColor = [VFTypography instance].grayScale6Color;
}

@end
