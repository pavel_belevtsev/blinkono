//
//  RecoredHelper.m
//  KalturaUnified
//
//  Created by Israel Berezin on 2/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "RecoredHelper.h"

@interface RecoredHelper()

@property (strong, nonatomic) NSNumber * status;

@end

@implementation RecoredHelper

@synthesize recordingItemStatus = _recordingItemStatus;

-(void)setRecordingItemStatus:(RecoredHelperStatus)recordingStatus
{
    _recordingItemStatus = recordingStatus;
    _status = [NSNumber numberWithInteger:recordingStatus];
}

-(RecoredHelperStatus)recordingItemStatus
{
    return [self.status integerValue];
}

@end
