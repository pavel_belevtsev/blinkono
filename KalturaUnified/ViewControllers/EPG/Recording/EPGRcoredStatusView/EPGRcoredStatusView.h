//
//  EPGRcoredStatusView.h
//  KalturaUnified
//
//  Created by Israel Berezin on 2/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "UINibView.h"
typedef enum
{
    EPGRcoredStatusViewRcoredingSecsses =0,
    EPGRcoredStatusViewRcoredingFailed,
    EPGRcoredStatusViewDeleteRecoredSecsses,
    EPGRcoredStatusViewDeleteRecoredFailed
}EPGRcoredStatusViewType;

@interface EPGRcoredStatusView : UINibView


@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (nonatomic, strong) NSTimer * timerForDismiss;
@property (nonatomic, assign) EPGRcoredStatusViewType  rcoredStatusViewType;
@property (strong, nonatomic) APSTVProgram * program;

@property (nonatomic, weak) UIView * showOnView;

- (IBAction)closeStatusView:(id)sender;
-(void)addEPGRcored:(APSTVProgram *) program status:(EPGRcoredStatusViewType)rcoredStatusViewType ViewOnView:(UIView*)superView isSeries:(BOOL)isSeries;


-(void)updateViewBgAndImageWithRcoredStatusViewType:(EPGRcoredStatusViewType)rcoredStatusViewType;

@end
