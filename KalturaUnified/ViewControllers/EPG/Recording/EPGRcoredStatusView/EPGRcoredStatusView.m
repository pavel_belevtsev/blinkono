//
//  EPGRcoredStatusView.m
//  KalturaUnified
//
//  Created by Israel Berezin on 2/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#define Spase 14

#import "EPGRcoredStatusView.h"

const CGFloat EPGRcoredStatusViewTimerTimeInterval = 5.0f;

@implementation EPGRcoredStatusView

-(void)resetTimer
{
    [self stopTimer];
    
    self.timerForDismiss = [NSTimer scheduledTimerWithTimeInterval:EPGRcoredStatusViewTimerTimeInterval target:self selector:@selector(dismissPopUpAutomatically) userInfo:nil repeats:NO];
}

- (void)stopTimer
{
    NSLog(@"PopUp stop");

    [self.timerForDismiss invalidate];
    self.timerForDismiss = nil;
}

- (void)dismissPopUpAutomatically
{
    [self dismissPopUp];
}

#pragma mark - public functions

- (void)dismissPopUp
{
    [self closeStatusView:nil];
}

- (IBAction)closeStatusView:(id)sender
{
    [self stopTimer];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.showOnView.hidden = YES;

    }];
}

-(void)addEPGRcored:(APSTVProgram *) program status:(EPGRcoredStatusViewType)rcoredStatusViewType ViewOnView:(UIView*)superView isSeries:(BOOL)isSeries
{
    [self buildUIAndTextWithProgram:program status:rcoredStatusViewType isSeries:isSeries];
    
    self.alpha = 0;
    self.showOnView = superView;
    self.showOnView.hidden = NO;
    
    [self.showOnView addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1.0;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)buildUIAndTextWithProgram:(APSTVProgram *) program status:(EPGRcoredStatusViewType)rcoredStatusViewType isSeries:(BOOL)isSeries
{
    //  <string name="toaster_recording_failed">Recording %1$s %2$s\nhas failed\. Please try again...</string>

    
    [self resetTimer];
    [self.mainView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.mainView.layer setShadowOpacity:0.3];
    [self.mainView.layer setShadowRadius:4.0];
    [self.mainView.layer setShadowOffset:CGSizeMake(0, 3)];
    
    [self updateViewBgAndImageWithRcoredStatusViewType:rcoredStatusViewType];
    [self updateFontsAndColors];
    
    NSString * titleStart = LS(@"rcoredStatusView_secsses_StartTitle");
    if (isSeries)
    {
        titleStart = LS(@"rcoredStatusView_secsses_series_StartTitle");
    }
    NSString * titleEnd = LS(@"rcoredStatusView_secsses_EndTitle");
    NSString * name = program.name;
    
    if (rcoredStatusViewType == EPGRcoredStatusViewRcoredingFailed)
    {
        titleStart = LS(@"rcoredStatusView_secsses_StartTitle");
        if (isSeries)
        {
            titleStart = LS(@"rcoredStatusView_secsses_series_StartTitle");
        }
        titleEnd = LS(@"rcoredStatusView_failed_EndTitle");
    }
    if (rcoredStatusViewType == EPGRcoredStatusViewDeleteRecoredSecsses)
    {
        titleStart = LS(@"rcoredStatusView_failed_StartTitle");
        if (isSeries)
        {
            titleStart = LS(@"rcoredStatusView_failed_series_StartTitle");
        }
        titleEnd = LS(@"rcoredStatusView_secssesDelete_EndTitle");
    }

    if (rcoredStatusViewType == EPGRcoredStatusViewDeleteRecoredFailed)
    {
        titleStart = LS(@"rcoredStatusView_failed_StartTitle");
        if (isSeries)
        {
            titleStart = LS(@"rcoredStatusView_failed_series_StartTitle");
        }
        titleEnd = LS(@"rcoredStatusView_failed_EndTitle");
    }

    NSString * discription = [NSString stringWithFormat:@"%@ \"%@\" %@",titleStart,name,titleEnd];
    self.lblTitle.text = discription;
    if (isPad)
    {
        [self.lblTitle sizeToFit];

        CGRect titleFrame = self.lblTitle.frame;
        titleFrame.size.height = self.frame.size.height;
        titleFrame.origin.x = (self.frame.size.width / 2 -  self.lblTitle.frame.size.width/2) + (Spase + self.iconImage.frame.size.width)/2;
        self.lblTitle.frame = titleFrame;

        CGRect iconImageFrame = self.iconImage.frame;
        iconImageFrame.origin.x = self.lblTitle.frame.origin.x - (Spase + self.iconImage.frame.size.width);
        self.iconImage.frame = iconImageFrame;
    }
    
}

-(void)updateViewBgAndImageWithRcoredStatusViewType:(EPGRcoredStatusViewType)rcoredStatusViewType
{
    [self.mainView setBackgroundColor:CS(@"67AB57")];
    self.iconImage.image = [UIImage imageNamed:@"check_icon"];
    if (rcoredStatusViewType == EPGRcoredStatusViewRcoredingFailed || rcoredStatusViewType == EPGRcoredStatusViewDeleteRecoredFailed )
    {
        [self.mainView setBackgroundColor:CS(@"D64A4A")];
        self.iconImage.image = [UIImage imageNamed:@"alert_icon"];
    }
}

-(void)updateFontsAndColors
{
    [Utils setLabelFont:self.lblTitle size:14 bold:YES];
    [self.lblTitle setTextColor:[UIColor whiteColor]];
}

-(void)dealloc
{
    [self stopTimer];
}
@end
