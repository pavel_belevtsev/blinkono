//
//  EPGRcoredStatusViewVF.m
//  Vodafone
//
//  Created by Israel Berezin on 3/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGRcoredStatusViewVF.h"

@implementation EPGRcoredStatusViewVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)updateViewBgAndImageWithRcoredStatusViewType:(EPGRcoredStatusViewType)rcoredStatusViewType
{
    [super updateViewBgAndImageWithRcoredStatusViewType:rcoredStatusViewType];
    [self.mainView setBackgroundColor:CS(@"67AB57")];
    if (rcoredStatusViewType == EPGRcoredStatusViewRcoredingFailed || rcoredStatusViewType == EPGRcoredStatusViewDeleteRecoredFailed )
    {
        [self.mainView setBackgroundColor:CS(@"D64A4A")];
    }

}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)updateFontsAndColors
{
    [Utils setLabelFont:self.lblTitle size:14 bold:YES];
    [self.lblTitle setTextColor:[UIColor whiteColor]];
}
@end
