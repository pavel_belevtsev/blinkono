//
//  EpgRecoredPopUp.h
//  KalturaUnified
//
//  Created by Israel Berezin on 2/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "ToolTipView.h"
#import "RecoredHelper.h"
#import "APSComplexButton.h"

typedef enum
{
    RecoredTypeEpisode =0,
    RecoredTypeSeires
}RecoredType;

@class EpgRecoredPopUp;

@protocol EpgRecoredPopUpDelegate <NSObject>

-(void) epgRecoredPopUp:(EpgRecoredPopUp *)epgRecoredPopUp recredType:(RecoredType)recoredType recoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem andLinkageView:(UIView*)linkageView;

-(void)setupButtonUI:(APSComplexButton *)button withText:(NSString*)title andImage:(UIImage*)image;
-(void)insertCloseButtonIfNeed;
-(void)setupLineColor:(UIView*)line;
-(void)setupBGColor;
-(void)insertMarginsBgColor;

@end



@interface EpgRecoredPopUp : ToolTipView

@property (assign,nonatomic) id<EpgRecoredPopUpDelegate> delegate;

@property (strong, nonatomic) APSTVProgram * program;
@property (strong, nonatomic) TVMediaItem * mediaItem;

@property (strong, nonatomic) RecoredHelper * recoredHelper;
@property (strong,nonatomic ) UIView * internalView;

-(void)buildWithOnLinkageView:(UIView *)linkageView withRecoredProgram:(APSTVProgram *) program andMediaItem:(TVMediaItem *) mediaItem toolTipArrowType:(ToolTipArrowType)toolTipArrowType
                toolCloseType:(ToolCloseType)toolCloseType;

-(void)buildWithOnLinkageView:(UIView *)linkageView withRecoredHelper:(RecoredHelper *) recoredHelper toolTipArrowType:(ToolTipArrowType)toolTipArrowType
                toolCloseType:(ToolCloseType)toolCloseType;


-(void)setupButtonUI:(APSComplexButton *)button withText:(NSString*)title andImage:(UIImage*)image;
@end
