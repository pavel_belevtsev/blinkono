//
//  EpgRecoredPopUp.m
//  KalturaUnified
//
//  Created by Israel Berezin on 2/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EpgRecoredPopUp.h"
#import "APSComplexButton.h"

#define  statusBarHigh 20
#define  TopBarHigh 0
#define  BottomBarHigh 0
#define ButtonCloseWidht 23

@implementation EpgRecoredPopUp


-(void)buildWithOnLinkageView:(UIView *)linkageView withRecoredHelper:(RecoredHelper *) recoredHelper toolTipArrowType:(ToolTipArrowType)toolTipArrowType
                toolCloseType:(ToolCloseType)toolCloseType
{
    self.recoredHelper = recoredHelper;
    [self buildWithOnLinkageView:linkageView withRecoredProgram:recoredHelper.program andMediaItem:recoredHelper.mediaItem toolTipArrowType:toolTipArrowType toolCloseType:toolCloseType];
}

-(void)setupBGColor
{
    [self.internalView setBackgroundColor:CS(@"232323")]; // :CS(@"232323")
}

-(void)setupLineColor:(UIView*)line
{
    [line setBackgroundColor:CS(@"353535")];
}

-(void)buildWithOnLinkageView:(UIView *)linkageView withRecoredProgram:(APSTVProgram *) program andMediaItem:(TVMediaItem *) mediaItem toolTipArrowType:(ToolTipArrowType)toolTipArrowType
                toolCloseType:(ToolCloseType)toolCloseType
{
    self.program = program;
    self.mediaItem = mediaItem;
    self.internalView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 230, 125)];
    self.internalView.clipsToBounds = NO;
    [self setupBGColor]; // :CS(@"232323")
    
    APSComplexButton * recoredEpisode = [[APSComplexButton alloc] initWithFrame:CGRectMake(0,0,230,62)];
    [recoredEpisode addTarget:self action:@selector(recoredEpisode:) forControlEvents:UIControlEventTouchUpInside];
    APSComplexButton * recoredSeries = [[APSComplexButton alloc] initWithFrame:CGRectMake(0,67,230,62)];
    [recoredSeries addTarget:self action:@selector(recoredSeries:) forControlEvents:UIControlEventTouchUpInside];

    UIView * line = [[UIView alloc] initWithFrame:CGRectMake(0, 66, 230, 1)];
    [self setupLineColor:line];
    
    NSString *title = LS(@"epgRecoredPopUp_RecoredEpisode");//LS(@"epgRecoredPopUp_RecoredEpisode");
    [self setupButtonUI:recoredEpisode withText:title andImage:[UIImage imageNamed:@"record_singleItem_icon"]];
    
    NSString *title1 = LS(@"epgRecoredPopUp_RecoredSeries");//LS(@"epgRecoredPopUp_RecoredSeries");
    [self setupButtonUI:recoredSeries withText:title1 andImage:[UIImage imageNamed:@"record_series_icon"]];

    recoredSeries.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;;
    recoredEpisode.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;;
    [recoredSeries setTitleEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
    [recoredEpisode setTitleEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
    
    [self.internalView addSubview:recoredEpisode];
    [self.internalView addSubview:line];
    [self.internalView addSubview:recoredSeries];
    
    NSInteger extarnel_MarginRight =150;
    if (isPad)
    {
        if (linkageView.frame.origin.x >555)
        {
            extarnel_MarginRight -= 40;
        }
    }
    NSInteger internalMarginTop = 7;
    NSInteger otherMargin = 7;

    if (isPhone)
    {
        otherMargin = 13;
        internalMarginTop= 40;
    }
    
    [self buildToolTipViewWithToolTipBoxView:self.internalView
                                 linkageView:linkageView toolTipArrowType:toolTipArrowType
                               toolCloseType:toolCloseType
                      andInternalMarginRight:otherMargin
                       andInternalMarginLeft:otherMargin
                        andInternalMarginTop:internalMarginTop
                       andInternalMarginDown:otherMargin
                      andExtarnelMarginRight:extarnel_MarginRight
                       andEnternalMarginLeft:20];
}

-(void)setupButtonUI:(APSComplexButton *)button withText:(NSString*)title andImage:(UIImage*)image
{
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
    button.titleLabel.font = Font_Bold(16);
    [self addImage:image toButton:button withTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
}

-(void)addImage:(UIImage*)image toButton:(APSComplexButton *)button withTitleEdgeInsets:(UIEdgeInsets)titleEdgeInsets
{
    UIImageView * imgFilter = [[UIImageView alloc] initWithImage:image];
    imgFilter.center = CGPointMake(button.frame.origin.x + 30.0, button.frame.size.height / 2.0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [button addSubview:imgFilter];
}

-(IBAction)recoredSeries:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(epgRecoredPopUp:recredType:recoredProgram:mediaItem:andLinkageView:)])
    {
        [self.delegate epgRecoredPopUp:self recredType:RecoredTypeSeires recoredProgram:self.program mediaItem:self.mediaItem andLinkageView:self.linkageView];
    }
 
    [self close:nil];
}

-(IBAction)recoredEpisode:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(epgRecoredPopUp:recredType:recoredProgram:mediaItem:andLinkageView:)])
    {
        [self.delegate epgRecoredPopUp:self recredType:RecoredTypeEpisode recoredProgram:self.program mediaItem:self.mediaItem andLinkageView:self.linkageView];
    }
    [self close:nil];
}

-(void)locateToolTip
{
    if (self.toolTipArrowType == ToolTipTypeArrowNone)
    {
        self.frame = self.linkageView.frame;
        self.center =  self.linkageView.center;
        self.toolShowView.center =  self.center;
    }
    else
    {
        NSInteger linkageViewWidth= self.linkageView.frame.size.width;
        NSInteger toolTipWidth = self.frame.size.width;
        
        //“global” position of object in ios
        UIViewController * viewC = (UIViewController *) NavM.container.centerViewController;
        CGPoint globalOrigenPoint = [self.linkageView convertPoint:self.linkageView.bounds.origin toView:viewC.view];
        
        NSInteger toolTipX = 0;
        NSInteger mainWidth = viewC.view.bounds.size.width;
        NSInteger mainHeight = viewC.view.bounds.size.height;
        
        if ((globalOrigenPoint.x +linkageViewWidth) < toolTipWidth) // right point
        {
            toolTipX = self.extarnelMarginLeft;
        }
        else if ((globalOrigenPoint.x + toolTipWidth) > mainWidth) // left point
        {
            toolTipX = mainWidth - (toolTipWidth + self.extarnelMarginRight);
        }
        else  // center
        {
            toolTipX = globalOrigenPoint.x + linkageViewWidth/2 - toolTipWidth/2;
        }
        
        NSInteger toolTipY = 0;
        NSInteger toolTipYArrowUp = globalOrigenPoint.y + self.linkageView.frame.size.height - TopBarHigh - 10;
        NSInteger toolTipYArrowDown = globalOrigenPoint.y - self.frame.size.height - (BottomBarHigh + statusBarHigh);
        
        if (self.toolTipArrowType ==ToolTipTypeArrowDynamic)
        {
            if ((toolTipYArrowUp + self.frame.size.height) > (mainHeight - (statusBarHigh + BottomBarHigh)))
            {
                toolTipY = toolTipYArrowDown + statusBarHigh +10;
                self.toolTipArrowType = ToolTipTypeArrowDown;
            }
            else if ((toolTipYArrowDown + statusBarHigh - self.frame.size.height) < 0)
            {
                toolTipY = toolTipYArrowUp;
                self.toolTipArrowType = ToolTipTypeArrowUp;
            }
            else
            {
                toolTipY = toolTipYArrowDown + statusBarHigh +10;
                self.toolTipArrowType = ToolTipTypeArrowDown;
            }
        }
        else if (self.toolTipArrowType == ToolTipTypeArrowUp)
        {
            toolTipY = toolTipYArrowUp;
        }
        else if (self.toolTipArrowType == ToolTipTypeArrowDown)
        {
            toolTipY = toolTipYArrowDown;
        }
        self.frame = CGRectMake(toolTipX, toolTipY, self.frame.size.width, self.frame.size.height);
    }
    [self addSubview:self.toolShowView];
}

-(void)insertCloseButtonIfNeed
{
    if (isPhone)
    {
        UIButton * closeFull = [[UIButton alloc] initWithFrame:self.frame];
        [closeFull setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]];
        [closeFull addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
        [self insertSubview:closeFull atIndex:0];
        
        UIButton * close = [[UIButton alloc] initWithFrame:CGRectMake(self.toolTipBoxView.frame.origin.x + self.toolTipBoxView.frame.size.width-ButtonCloseWidht, 5, 25, 25)];
        [close setBackgroundColor:[UIColor clearColor]];
        [close setImage:[UIImage imageNamed:@"pin_close_btn"] forState:UIControlStateNormal];
        [close addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
        [self.toolShowView addSubview:close];
        
        UILabel * lab = [[UILabel alloc] init];
        [lab setBackgroundColor:[UIColor clearColor]];
        [lab setText:LS(@"Select Option")];
        [self updateSelectOptionFontSizeAndColor:lab];
        CGRect frame = lab.frame;
        frame.origin.y = close.frame.origin.y;
        frame.origin.x = self.toolTipBoxView.frame.origin.x;
        frame.size.height = close.frame.size.height;
        frame.size.width = self.toolTipBoxView.frame.size.width -ButtonCloseWidht;
        lab.frame = frame;
        [self.toolShowView addSubview:lab];
        
    }
}

-(void)updateSelectOptionFontSizeAndColor:(UILabel *) lab
{
    [Utils setLabelFont:lab size:14 bold:NO];
    [lab setTextColor:CS(@"959595")];
}

-(void)insertMarginsBgColor
{
    self.toolShowView.layer.cornerRadius = 5.0;
    self.toolShowView.layer.borderWidth = 1.0f;
    self.toolShowView.layer.borderColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1.0].CGColor;
    [self.toolShowView setBackgroundColor:[UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1.0]];
}

-(void)updateUI
{
    
    if (isPad)
    {
        [super updateUI];
        return;
    }
//    self.internalMarginTop = 40;

    NSInteger internalMarginWidth = self.internalMarginRight + self.internalMarginLeft;
    NSInteger internalMarginHigh = self.internalMarginTop + self.internalMarginDown;
    
    
    self.toolShowView = [[UIView alloc] init];
    self.toolShowView.frame = CGRectMake(0, 0, self.toolTipBoxView.frame.size.width + internalMarginWidth, self.toolTipBoxView.frame.size.height + internalMarginHigh);
    [self.toolShowView addSubview:self.toolTipBoxView];
    self.toolTipBoxView.center = self.toolShowView.center;
    
    
    CGRect frame1 = self.toolTipBoxView.frame;
    frame1.origin.y = self.internalMarginTop;
    self.toolTipBoxView.frame = frame1;
    
    
    [self.toolShowView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.toolShowView.layer setShadowOpacity:0.4];
    [self.toolShowView.layer setShadowRadius:6.0];
    [self.toolShowView.layer setShadowOffset:CGSizeMake(0, 1)];
    
    [self insertMarginsBgColor];
    
    CGRect frame = self.frame;
    frame.size.width = self.toolShowView.frame.size.width+10;
    frame.size.height = self.toolShowView.frame.size.height+20;
    self.frame = frame;
    self.toolShowView.center = self.center;

}

@end
