//
//  EPGViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGViewController.h"
#import "UIImage+Tint.h"
#import "NSDate+Utilities.h"
#import "UIImageView+WebCache.h"
#import <TvinciSDK/APSBlockOperation.h>
#import <TvinciSDK/APSTVProgram+Additions.h>
#import "EPGSortViewControllerPhone.h"
#import "SocialManagement.h"
#import "EPGChannelViewController.h"
#import <TvinciSDK/TVNPVRRecordItem.h>
#import "RecoredHelper.h"
#import "APSBackgroundColorButton.h"
#import <TvinciSDK/TVDataRefreshHandler.h>


#define seriesSourceKey @"season"
#define seasonIdKey @"seasonId"

@interface EPGViewController ()<EpgRecoredPopUpDelegate,CancelSeriesRecordingPopUpDelegate,RecoredAdapterDelegate>

@end

@implementation EPGViewController

- (void)viewDidLoad {
    
  

    
    self.loadNoInternetView = YES;

    [super viewDidLoad];

    self.recoredDicByChannels = [[NSMutableDictionary alloc] init];
    self.dictionaryNpvrRequest = [[NSMutableDictionary alloc]init];
    self.recoredAdapter = [[RecoredAdapter alloc] init];
    self.recoredAdapter.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _viewBgPhone.hidden = YES;
    
    [Utils setLabelFont:_labelTitle bold:YES];
    
    [_collectionViewEPG registerNib:[UINib nibWithNibName:[Utils nibName:@"EPGCollectionViewCell"] bundle:nil] forCellWithReuseIdentifier:@"EPGCollectionViewCell"];
    [_collectionViewEPG setBackgroundColor:CS(@"232323")];
    
    self.dictionaryCurrentProgramByChannelID = [[NSMutableDictionary alloc] init];
    self.dictionaryRequest = [[NSMutableDictionary alloc] init];
    
    self.filterMenuItemList = [[NSMutableArray alloc] init];
    
    self.searchListView = [[SearchListView alloc] initWithNib];
    
    CGFloat searchY = _viewSearchForm.frame.origin.y + _viewSearchForm.frame.size.height;
    CGFloat height = super.viewContent.frame.size.height - searchY;
    if (isPad) {
        height -= _viewTimePad.frame.size.height;
    }else {
        height -=_viewButtonsPhone.frame.size.height;
    }
    _searchListView.frame = CGRectMake(_viewSearchForm.frame.origin.x, searchY, _viewSearchForm.frame.size.width, height);
    
    _searchListView.alpha = 0.0;
    _searchListView.forEPG = YES;
    _searchListView.delegate = self;
    [super.viewContent addSubview:_searchListView];
    
    self.socialManager = [[SocialManagement alloc] init];

    if (isPad) {
        self.sortView = [[EPGSortView alloc] initWithNib];
        _sortView.frame = CGRectMake(_viewSearchForm.frame.origin.x, _viewSearchForm.frame.origin.y + _viewSearchForm.frame.size.height, _sortView.frame.size.width, _sortView.frame.size.height);
        _sortView.delegate = self;
        [[_viewSearchForm superview] addSubview:_sortView];
        
        _viewToolbarBg.layer.cornerRadius = 6;
        _viewToolbarBg.layer.masksToBounds = YES;
        
        [_buttonSegmentChannels setTitle:LS(@"tv_guide_channel_sort") forState:UIControlStateNormal];
        _buttonSegmentChannels.titleLabel.numberOfLines = 2;
        _buttonSegmentChannels.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        _buttonSegmentChannels.selected = NO;
        _sortView.alpha = 0.0;
        
        [Utils setButtonFont:_buttonSegmentChannels bold:YES];
        [_buttonSegmentChannels addTarget:self action:@selector(buttonSegmentSort:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        UIImage *img = [UIImage imageNamed:@"details_down_arrow"];
        self.imgArrow = [[UIImageView alloc] initWithImage:[img imageTintedWithColor:CS(@"bebebe")] highlightedImage:[img imageTintedWithColor:APST.brandTextColor]];
        
        [self updateImgArrowPosition];
        [_buttonSegmentChannels addSubview:_imgArrow];
        
        [_buttonSegmentChannels addTarget:self action:@selector(buttonChannelsTouchDown:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragInside];
        [_buttonSegmentChannels addTarget:self action:@selector(buttonChannelsTouchCancel:) forControlEvents:UIControlEventTouchCancel | UIControlEventTouchDragOutside];

        
        
    }
    
    _emptyChannelView.hidden = YES;
    
    [Utils setLabelFont:_emptyChannelTitle bold:YES];
    [Utils setLabelFont:_emptyChannelText bold:NO];
    
    _emptyChannelTitle.text = LS(@"epg_no_favorites_header_short");
    _emptyChannelText.text = LS(@"epg_no_favorites_instruction");
    
    if(isPad){
        APSBackgroundColorButton* button = (APSBackgroundColorButton*) self.buttonNow;
        // set the colors
        button.selectedColor = APST.brandColor;
        button.highlightedColor = APST.brandColor;
        button.defaultColor = CS(@"666666");
        // set the button shape & shadow:
        button.layer.cornerRadius = 4;
        self.buttonNow.selected = YES;
        
        self.buttonSearch.layer.cornerRadius = 4;
        [self initSelectedBrandButtonUI:self.buttonSearch];
//        self.buttonSearch.backgroundColor = CS(@"666666");
    }
    
    self.labelAutomation.isAccessibilityElement = YES;
    self.labelAutomation.accessibilityLabel = @"Label Automation";
    self.labelAutomation.accessibilityValue = @"";

}

- (void)updateImgArrowPosition {
    
    float titleWidth = [_buttonSegmentChannels.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:_buttonSegmentChannels.titleLabel.font}].width + 20.0;
    int arrowX = (_buttonSegmentChannels.frame.size.width - titleWidth) / 2;
    arrowX = _buttonSegmentChannels.frame.size.width - arrowX;
    
    self.imgArrow.frame = CGRectMake(arrowX, (int)((_buttonSegmentChannels.frame.size.height - _imgArrow.image.size.height) / 2.0), _imgArrow.image.size.width, _imgArrow.image.size.height);
}

- (void)buttonChannelsTouchDown:(UIButton *)button {
    _imgArrow.highlighted = !button.selected;
    
    [self rotateImgArrow];
}

- (void)buttonChannelsTouchCancel:(UIButton *)button {
    _imgArrow.highlighted = NO;
    
    [self rotateImgArrow];
}

- (void)rotateImgArrow {
    
    [UIView animateWithDuration:0.2f animations:^{
        [_imgArrow setTransform:CGAffineTransformMakeRotation(_imgArrow.highlighted ? M_PI : 0)];
    }];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    super.panModeDefault = MFSideMenuPanModeNone;
    [self menuContainerViewController].panMode = (MFSideMenuPanMode)super.panModeDefault;
    
    _labelTitle.text = _epgMenuItem.name;
    
    if (!isInitialized) {
        
        sortOnly = ([_epgMenuItem.children count] == 0);
        
    }
    
    if (isPhone) {
        
        self.timerHideBar = [NSTimer scheduledTimerWithTimeInterval:1.0 / 2.0 target:self selector:@selector(hideBarProcess) userInfo:nil repeats:YES];
        
        EPGSortViewControllerPhone *sortController = [[ViewControllersFactory defaultFactory] epgSortViewControllerPhone];
        sortController.delegate = self;
        
        sortController.sortView = [[EPGSortView alloc] initWithNib];
        [sortController.sortView updateWithMenuItem:_epgMenuItem filters:_filterMenuItemList sortOnly:sortOnly orderBy:_orderBy];
        
        [self menuContainerViewController].rightMenuViewController = sortController;
        [[self menuContainerViewController] setRightMenuWidth:270];
        
        if (!APST.searchEPG && !self.viewSearchForm.hidden){
            
            self.viewSearchForm.hidden = YES;
            self.collectionViewEPG.frame = CGRectMake(self.collectionViewEPG.frame.origin.x, self.collectionViewEPG.frame.origin.y - self.viewSearchForm.frame.size.height, self.collectionViewEPG.frame.size.width, self.collectionViewEPG.frame.size.height + self.viewSearchForm.frame.size.height);
            
        }
        
    } else {
        
        if (APST.searchEPG){
            
            if (sortOnly || [_filterMenuItemList count]) {
                [_buttonSegmentChannels setTitle:LS(@"category_page_sort_by") forState:UIControlStateNormal];
            } else {
                [_buttonSegmentChannels setTitle:LS(@"browse") forState:UIControlStateNormal];
            }
            [self updateImgArrowPosition];
            
            [_sortView updateWithMenuItem:_epgMenuItem filters:_filterMenuItemList sortOnly:sortOnly orderBy:_orderBy];
            
            [self.buttonSearch setTitle:LS(@"search") forState:UIControlStateNormal];
            
        } else{
            
            self.buttonSearch.hidden = YES;
            
        }
        
    }

    if (!isInitialized) {
        //isInitialized = YES;
        
        sortOnly = ([_epgMenuItem.children count] == 0);
        
        self.selectedDay = 0;
        self.epgDate = [NSDate date];
        currentInterval = [self.epgDate timeIntervalSince1970];
                
        [self updateSortByButton];

        if (_viewButtonsPhone) {
            [self initButtonsPhoneBar];
        } else {
            [self initButtonsPadBar];
        }
            
        //[self epgListInitialization];
        
    }
    
    [self skinChanged];
    
    timeShowBar = CFAbsoluteTimeGetCurrent();
    
    
    if (!_searchBlankView){
        _searchBlankView = [[SearchBlankView alloc] initWithNib];
        [self.viewContent addSubview:_searchBlankView];
        _searchBlankView.center = self.collectionViewEPG.center;
        
        if (isPad){
            _searchBlankView.frame = CGRectOffset(_searchBlankView.frame, _viewSearchForm.frame.size.width/2, 0);//self.viewNavigationBar.frame.size.height);
        }

    }
    
    _searchBlankView.alpha = 0.0;
    _collectionViewEPG.alpha = 1.0;
    
    [_collectionViewEPG reloadData];
    
    
    [EPGM refreshUserRelatedActionsWithCompletion:^(NSMutableArray *result) {
        [_collectionViewEPG reloadData];
    } delegate:self];
    
    if (_buttonNow.selected) {
        [self buttonNowPressed:_buttonNow];
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (!isInitialized) {
        isInitialized = YES;
        
        
        [self epgListInitialization];
        
    }

    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if (_timerHideBar) {
        [_timerHideBar invalidate];
        _timerHideBar = nil;
        
        
        [self menuContainerViewController].rightMenuViewController = nil;
        
    }
    
    // clean memory to load then back to page.
    [self.recoredAdapter cleanAllReoredsDataFromMemory];
    NSArray * keys =[self.dictionaryNpvrRequest allKeys];
    for (NSString * key in keys)
    {
        TVPAPIRequest * req = [self.dictionaryNpvrRequest objectForKey:key];
        [req cancel];
    }
    [self.dictionaryNpvrRequest removeAllObjects];
    
    isInitialized = NO;
}

#pragma mark - Segment Buttons

- (void)buttonSegmentSort:(UIButton *)button {
    
    button.selected = !button.selected;
    _imgArrow.highlighted = button.selected;
    
    [self rotateImgArrow];

    if (button.selected) {
        _emptyChannelView.hidden = YES;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        
        _sortView.alpha = (button.selected ? 1.0 : 0.0);
        
    }];
    
}

#pragma mark - Search

- (IBAction)buttonSearchPressed:(UIButton *)button {
    
    if (_buttonSegmentChannels.selected) {
        [self buttonSegmentSort:_buttonSegmentChannels];
    }
    
    _emptyChannelView.hidden = YES;
    
    button.selected = !button.selected;
    
    if (button.selected) {
        
        _viewSearchForm.hidden = NO;
        [_textFieldSearch becomeFirstResponder];
        
    } else {
        
        if (searchMode) {
            
            [self buttonNowPressed:_buttonNow];
        }
        [self restoreSearchButtons];

        _viewSearchForm.hidden = YES;
        
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == _textFieldSearch) {
        
        _emptyChannelView.hidden = YES;
        _searchListView.alpha = 1.0;
        _searchListView.textFieldSearch = _textFieldSearch;
        _textFieldSearch.delegate = _searchListView;
        _searchListView.imageNotActiveBackground = [UIImage imageNamed:@"epg_search_bg"];
        _searchListView.imageViewSearchBackground = _imageViewSearchBackground;
        _imageViewSearchBackground.image = [UIImage imageNamed:@"epg_search_bg_active"];
        [_searchListView initSearchList];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == _textFieldSearch) {
        _imageViewSearchBackground.image = [UIImage imageNamed:@"epg_search_bg"];
    }
}

- (void)searchProcess:(NSString *)searchStr {
    
    _textFieldSearch.delegate = self;
    
    _searchListView.alpha = 0.0;

    [_textFieldSearch resignFirstResponder];
    
    NSLog(@"Search %@", searchStr);
    
    if ([searchStr length] > 1) {
    
        [self showHUDBlockingUI:YES];
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        [self requestForSearchEPGProgramsWithText:searchStr pageIndex:0 array:array];
        
        
    } else {
        
        [self restoreSearchButtons];
        
    }
    
}

- (void)requestForSearchEPGProgramsWithText:(NSString *)searchStr pageIndex:(int)pageIndex array:(NSMutableArray *)array {

    __weak EPGViewController *weakSelf = self;
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForSearchEPGProgramsWithText:searchStr pageSize:maxPageSize pageIndex:pageIndex delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
        
        for (id item in result) {
            [array addObject:item];
        }
        
        if (([result count] == maxPageSize) && ([array count] < 200)) {
            
            [weakSelf requestForSearchEPGProgramsWithText:searchStr pageIndex:(pageIndex + 1) array:array];
            
        } else {
            
            [weakSelf searchProcessComplete:array];
            if (!(array && array.count > 0)) {
                _searchListView.buttonSearch.hidden = YES;
            }
            
        }
    
        
    }];
    
    [request setFailedBlock:^{
        
        [weakSelf searchProcessComplete:nil];
        
    }];
    
    [self sendRequest:request];
    
}


- (void)searchProcessComplete:(NSArray *)channelItems
{
    [self hideHUD];
    _buttonTime.enabled = NO;
    _buttonNow.selected = NO;
    
    buttonDaySelected = NO;
    buttonTimeSelected = NO;
    
    _buttonDate.selected = NO;
    _buttonTime.selected = NO;
    
    _searchBlankView.alpha = 0.0;
    _emptyChannelView.hidden = YES;
    _collectionViewEPG.alpha = 1.0;
    
    searchMode = YES;

    [_selectTimeView updateForSearchMode:searchMode];
    
    _searchBlankView.labelDescriptionText = LS(@"search_page_search_no_results_sub_title1");
    _searchBlankView.labelTitleText = [NSString stringWithFormat:@"%@ \"%@\"", LS(@"search_page_search_no_results_without_param"), _searchListView.searchStr];
    
    self.searchDays = [NSArray array];
    self.searchFullListChannels = [NSArray array];
    self.channelsList = [NSArray array];
    
    [_selectDateView clearSearchResults];
    
    if (channelItems)
    {
        if (channelItems.count > 0)
        {
            
            NSLog(@"channelItems %lu", (unsigned long)[channelItems count]);
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"M/d/yyyy h:mm:ss a"];
            [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
           

            NSMutableArray *data = [[NSMutableArray alloc] init];
            
            NSMutableArray *dataDays = [[NSMutableArray alloc] init];
            NSMutableArray *dataChannels = [[NSMutableArray alloc] init];
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            
            for (NSInteger i = 0; i < (APST.numberOfWeekDays + APST.numberOfRetroDays); i++)
            {
                
                NSDate *now = [NSDate date];
                NSDate *newDate = [now dateByAddingTimeInterval:60*60*24*(i - APST.numberOfRetroDays)];
                
                NSMutableDictionary *dayItem = [[NSMutableDictionary alloc] init];
                [dayItem setObject:newDate forKey:@"newDate"];
                
                NSDateComponents *components = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit) fromDate:newDate];
                
                [dayItem setObject:@(components.day) forKey:@"day"];
                [dayItem setObject:@(components.month) forKey:@"month"];
                [dayItem setObject:@(0) forKey:@"count"];
                
                [dataDays addObject:dayItem];
            }
            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"APSTVProgram" inManagedObjectContext:self.privateContext];
            
            for (NSDictionary *item in channelItems)
            {
                APSTVProgram * program = (APSTVProgram *)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
                [program setdictionary:item];
                [data addObject:program];
            }
            
            [data sortUsingComparator:^NSComparisonResult(TVEPGProgram *program1, TVEPGProgram *program2) {
                
                return [program1.startDateTime compare:program2.startDateTime];
                
            }];
            
            NSMutableArray *searchChannels = [[NSMutableArray alloc] init];
            
            for (TVMediaItem *channel in EPGM.channelsList)
            {
                NSMutableArray *epgList = [[NSMutableArray alloc] init];
                NSMutableArray *channelProgramms = [[NSMutableArray alloc] init];
                
                for (int i = 0; i < [data count]; i++)
                {
                    APSTVProgram *program = [data objectAtIndex:i];
                    if ([program.epgChannelID intValue] == [channel.epgChannelID intValue])
                    {
                        [channelProgramms addObject:program];
                        
                        [epgList addObject:program];
                        
                        [data removeObjectAtIndex:i--];
                    }
                }
                
                [searchChannels addObject:channel];
                
                if ([channelProgramms count] > 0)
                {
                    NSMutableDictionary *channelData = [[NSMutableDictionary alloc] init];
                    
                    BOOL canAddChannelToList = NO;
                    
                    for (APSTVProgram *program in channelProgramms)
                    {
                        NSDateComponents *components = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit) fromDate:program.startDateTime];
                        
                        if (components) {
                            
                            for (NSMutableDictionary *dayData in dataDays) {
                                if ([[dayData objectForKey:@"day"] intValue] == components.day && [[dayData objectForKey:@"month"] intValue] == components.month) {
                                    
                                    canAddChannelToList = YES;
                                    
                                    NSString *key = [NSString stringWithFormat:@"%ld_%ld", (long)components.day, (long)components.month];
                                    
                                    NSMutableArray *dayArray = [channelData objectForKey:key];
                                    if (!dayArray) {
                                        
                                        dayArray = [[NSMutableArray alloc] init];
                                        [channelData setObject:dayArray forKey:key];
                                        
                                    }
                                    
                                    [dayArray addObject:program];
                                    
                                    int count = [[dayData objectForKey:@"count"] intValue] + 1;
                                    [dayData setObject:@(count) forKey:@"count"];
                                    
                                }
                            }
                        }
                    }
                    
                    if (canAddChannelToList) {
                        
                        [channelData setObject:channel forKey:@"channel"];
                        [dataChannels addObject:channelData];
                        
                    }
                }
                
            }
            
            [_selectDateView updateSearchResults:dataDays];
            
            for (int i = 0; i < [dataDays count]; i++) {
                
                //NSMutableDictionary *dayData = [dataDays objectAtIndex:i];
                
                //NSLog(@"%@", [dayData description]);
                
            }
            

            self.searchDays = dataDays;
            self.searchFullListChannels = dataChannels;
            
            
            if ([self.searchFullListChannels count] > 0) {
                
                
                NSInteger firstButtonTag = APST.numberOfRetroDays;
                
                
                if (firstButtonTag >= 0) {
                    
                    NSInteger indexToday = firstButtonTag;
                    
                    NSInteger maxOffsetTag = MAX(APST.numberOfRetroDays, APST.numberOfWeekDays) + 1;
                    
                    BOOL ready = NO;
                    
                    for (int i = 0; i < maxOffsetTag; i++) {
                        
                        if (!ready) {
                            
                            NSInteger indexTag = indexToday + i;
                            
                            if (i < APST.numberOfWeekDays && indexTag < [dataDays count]) {
                                
                                NSMutableDictionary *dayData = [dataDays objectAtIndex:indexTag];
                                
                                int count = [[dayData objectForKey:@"count"] intValue];
                                
                                if (count > 0) {
                                    
                                    self.selectedDay = indexTag - APST.numberOfRetroDays;
                                    
                                    [_selectDateView setDay:_selectedDay];
                                    [self updateSearchListResult];
                                    
                                    ready = YES;
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                    
                    for (int i = 0; i < maxOffsetTag; i++) {
                        
                        if (!ready && i > 0) {
                            
                            NSInteger indexTag = indexToday - i;
                            
                            if (indexTag >= 0) {
                                
                                NSMutableDictionary *dayData = [dataDays objectAtIndex:indexTag];
                                
                                NSInteger count = [[dayData objectForKey:@"count"] intValue];
                                
                                if (count > 0) {
                                    
                                    self.selectedDay = indexTag - APST.numberOfRetroDays;
                                    [_selectDateView setDay:_selectedDay];
                                    [self updateSearchListResult];
                                    
                                    ready = YES;
                                    
                                }
                                
                            }
                        }
                        
                    }
                    
                }
                
                _buttonDate.enabled = YES;
                
            } else {
                
                _buttonDate.enabled = NO;
             
                _channelsList = [NSArray array];
                
                [self reloadCollection];
                
            }
        
            
        } else {
          
            _searchBlankView.alpha = 1.0;
            _collectionViewEPG.alpha = 0.0;
            _emptyChannelView.hidden = YES;
            [_selectDateView  clearSearchResultsAndDisableButtons];
            _viewBgPhone.hidden = YES;
            
        }
    }
    
}

- (void)updateSearchListResult {
    
    if (_selectedDay + APST.numberOfRetroDays < [_searchDays count]) {
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        NSDictionary *item = [_searchDays objectAtIndex:_selectedDay + APST.numberOfRetroDays];
        
        //NSLog([item description]);
        
        NSString *key = [NSString stringWithFormat:@"%d_%d", [[item objectForKey:@"day"] intValue], [[item objectForKey:@"month"] intValue]];
        //NSLog(key);
        
        for (NSDictionary *channel in _searchFullListChannels) {
            
            if ([channel objectForKey:key]) {
          //      NSLog([channel description]);
                [array addObject:[channel objectForKey:@"channel"]];
            }
            
        }
        
        _channelsList = array;
        
        [self reloadCollection];
        
        if ([array count]) {
            _searchBlankView.alpha = 0.0;
            _collectionViewEPG.alpha = 1.0;
            _viewBgPhone.hidden = NO;
            
        } else {
            _searchBlankView.alpha = 1.0;
            _collectionViewEPG.alpha = 0.0;
            _viewBgPhone.hidden = YES;
            
        }
        
    }
    
}


#pragma mark - Sorting

- (void)updateSortByButton {
    
    if (_buttonSortOpen) {
        
        [_buttonSortOpen setTitle:LS(@"category_page_sort_by") forState:UIControlStateNormal];
        [self initSelectedBrandButtonUI:_buttonSortOpen];
        
    }
}

- (IBAction)buttonSortOpenPressed:(UIButton *)button {
    
    [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
    
}

#pragma mark - Inititalization

- (void)skinChanged {
    
    [self updateSortByButton];
    
    if (_selectDateView) {
        [_selectDateView skinChanged];
    }
    if (_selectTimeView) {
        [_selectTimeView skinChanged];
    }
    if (isPhone) {
        
        UIImage *imgDate = [[UIImage imageNamed:@"epg_icon_date"] imageTintedWithColor:APST.brandColor];
        [_buttonDate setImage:imgDate forState:UIControlStateHighlighted];
        [_buttonDate setImage:imgDate forState:UIControlStateSelected];
        
        UIImage *imgTime = [[UIImage imageNamed:@"epg_icon_time"] imageTintedWithColor:APST.brandColor];
        [_buttonTime setImage:imgTime forState:UIControlStateHighlighted];
        [_buttonTime setImage:imgTime forState:UIControlStateSelected];

        
        [_buttonNow setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
        [_buttonNow setTitleColor:APST.brandColor forState:UIControlStateSelected];
        [_buttonDate setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
        [_buttonDate setTitleColor:APST.brandColor forState:UIControlStateSelected];
        [_buttonTime setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
        [_buttonTime setTitleColor:APST.brandColor forState:UIControlStateSelected];
    }
    
    
    if (isPad) {
        
        UIImage *imgChannelSortBtn = [UIImage imageNamed:@"epg_segment_button"];
        [_buttonSegmentChannels setBackgroundImage:[imgChannelSortBtn imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
        [_buttonSegmentChannels setBackgroundImage:[imgChannelSortBtn imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
        
        [_buttonSegmentChannels setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
        [_buttonSegmentChannels setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
        
        UIImage *img = [UIImage imageNamed:@"details_down_arrow"];
        self.imgArrow.highlightedImage = [img imageTintedWithColor:APST.brandTextColor];
        
        if (APST.searchEPG){
            
            if (sortOnly || [_filterMenuItemList count]) {
                [_buttonSegmentChannels setTitle:LS(@"category_page_sort_by") forState:UIControlStateNormal];
            } else {
                [_buttonSegmentChannels setTitle:LS(@"browse") forState:UIControlStateNormal];
            }
            [self updateImgArrowPosition];
            
            [_sortView updateWithMenuItem:_epgMenuItem filters:_filterMenuItemList sortOnly:sortOnly orderBy:_orderBy];
            
            [self.buttonSearch setTitle:LS(@"search") forState:UIControlStateNormal];
            [self.buttonSearch setImage:[[UIImage imageNamed:@"epg_search_icon"] imageTintedWithColor:APST.brandTextColor] forState:UIControlStateHighlighted];
            [self.buttonSearch setImage:[[UIImage imageNamed:@"epg_search_icon"] imageTintedWithColor:APST.brandTextColor] forState:UIControlStateSelected];
        }
    }
}

- (void)initButtonsPhoneBar {
    
    
    [Utils setButtonFont:_buttonNow bold:YES];
    [Utils setButtonFont:_buttonDate bold:YES];
    [Utils setButtonFont:_buttonTime bold:YES];
    
    [_buttonNow setTitle:LS(@"now") forState:UIControlStateNormal];
    [_buttonDate setTitle:LS(@"today") forState:UIControlStateNormal];
    
    
    self.selectTimeView = [[EPGSelectTimeView alloc] initWithNib];
    _selectTimeView.delegate = self;
    _selectTimeView.frame = CGRectMake(0.0, _viewButtonsPhone.frame.origin.y, _selectTimeView.frame.size.width, _selectTimeView.frame.size.height);
    [super.viewContent insertSubview:_selectTimeView belowSubview:_viewButtonsPhone];
    
    self.selectDateView = [[EPGSelectDateView alloc] initWithNib];
    _selectDateView.delegate = self;
    _selectDateView.frame = CGRectMake(0.0, _viewButtonsPhone.frame.origin.y, _selectDateView.frame.size.width, _selectDateView.frame.size.height);
    [super.viewContent insertSubview:_selectDateView belowSubview:_viewButtonsPhone];
   
    
    [_buttonTime setTitle:[[NSDate date] stringFromDateWithDateFormat:@"HH:mm"] forState:UIControlStateNormal];
    
}

- (void)initButtonsPadBar {

    if (self.selectDateView!= nil) {
        [self.selectDateView removeFromSuperview];
    }
    self.selectDateView = [[EPGSelectDateView alloc] initWithNib];
    _selectDateView.delegate = self;
    _selectDateView.frame = _viewDatePad.bounds;
    [_viewDatePad addSubview:_selectDateView];
    
    [_imgViewBottom setImage:[[UIImage imageNamed:@"epg_bottom_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)]];
    
    if (self.selectTimeView!= nil) {
        [self.selectTimeView removeFromSuperview];
    }
    self.selectTimeView = [[EPGSelectTimeView alloc] initWithNib];
    _selectTimeView.delegate = self;
    _selectTimeView.frame = _viewTimePad.bounds;
    [_viewTimePad addSubview:_selectTimeView];
    
}

#pragma mark - EPGSortViewControllerDelegate

- (void)epgListInitialization {
    
    _emptyChannelView.hidden = YES;
    
    BOOL useFilters = [_filterMenuItemList count];
    
    NSInteger channelId = [[_epgMenuItem.layout objectForKey:ConstKeyMenuItemChannel] integerValue];
    
    _searchBlankView.alpha = 0.0;
    _collectionViewEPG.alpha = 1.0;
    _viewBgPhone.hidden = NO;
    
    useMyChannels = NO;
    
    if (useFilters) {
        
        TVMenuItem *item = [_filterMenuItemList lastObject];
        if ([item isKindOfClass:[TVMenuItem class]]) {
            channelId = item.channelID;
        } else {
            useMyChannels = YES;
        }
        
    }
    
    if (!EPGM.channelsList || useFilters) {
        
        if (useMyChannels) {
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            for (TVMediaItem *item in EPGM.channelsList) {
                if (item.isFavorite) {
                    [array addObject:item];
                }
            }
            
            _channelsList = array;
            
            [self reloadCollection];
            [_collectionViewEPG scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            
            if (_channelsList.count == 0){
                _emptyChannelView.hidden = NO;
                _searchBlankView.alpha = 0.0;
                [_selectDateView  clearSearchResultsAndDisableButtons];
            }
        
        } else {
            
            [self showHUDBlockingUI:YES];
            
             __weak EPGViewController *weakSelf = self;

            //if (channelId == 340846) channelId = 111111; // Test no Drama channels
            [self requestForGetChannelMediaList:channelId orderBy:TVOrderByName completion:^(NSArray *result) {
                
                NSMutableArray *array = [[NSMutableArray alloc] init];
                if ([result count]) {
                    
                    for (NSDictionary *dic in result) {
                        TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dic];
                        
                        if ([item isLive]) {
                            
                            for (TVMediaItem *itemOld in weakSelf.channelsList){
                                if ([item.mediaID isEqualToString:itemOld.mediaID]){
                                    item.isFavorite = itemOld.isFavorite;
                                    break;
                                }
                            }
                            [array addObject:item];
                            
                        }
                        
                    }
                    
                }
                
                [EPGM sortChannelsList:array];
                
                if (useFilters) {
                    _channelsList = array;
                    
                    [self hideHUD];
                    
                    [self reloadCollection];
                    [_collectionViewEPG scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    
                    if ([_channelsList count] == 0) {
                        
                        TVMenuItem *item = [_filterMenuItemList lastObject];
                        if ([item isKindOfClass:[TVMenuItem class]]) {
        
                            _searchBlankView.labelDescriptionText = LS(@"search_page_search_no_results_sub_title2");
                            _searchBlankView.labelTitleText = [NSString stringWithFormat:@"%@ \"%@\"", LS(@"search_page_search_no_results_without_param1"), item.name];
                            
                            _searchBlankView.alpha = 1.0;
                            _collectionViewEPG.alpha = 0.0;
                            _emptyChannelView.hidden = YES;
                            [_selectDateView  clearSearchResultsAndDisableButtons];
                            _viewBgPhone.hidden = YES;
                            
                        }

                        
                    }

                    
                } else {
                    
                    _channelsList = array;
                    
                    [EPGM updateEPGList:array delegate:self completion:^(NSMutableArray *result) {
                        
                        [self hideHUD];
                        
                        [self reloadCollection];
                        [_collectionViewEPG scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                        
                        
                    }];
                    
                }
                
                
            }];

        }

    } else {
        
        if (sortOnly && _orderBy > 0) {
         
            //_filterByArray = @[@{@"title":LS(@"category_page_newest"), @"sortBy":@(TVOrderByDateAdded)},
            //                   @{@"title":LS(@"category_page_most_popular"), @"sortBy":@(TVOrderByNumberOfViews)},
            //                   @{@"title":LS(@"category_page_most_liked"), @"sortBy":@(TVOrderByRating)}];
            
            if (_orderBy == TVOrderByDateAdded) {
                
            }
            
            if (_orderBy == TVOrderByDateAdded) {
                
                _channelsList = [EPGM.channelsList sortedArrayUsingComparator:^NSComparisonResult(TVMediaItem *p1, TVMediaItem *p2) {
                    
                    if ([p1.creationDate timeIntervalSince1970] < [p2.creationDate timeIntervalSince1970]) {
                        return NSOrderedDescending;
                    }  else {
                        return NSOrderedAscending;
                    }
                    
                }];
                
            } else if (_orderBy == TVOrderByNumberOfViews) {
                
                _channelsList = [EPGM.channelsList sortedArrayUsingComparator:^NSComparisonResult(TVMediaItem *p1, TVMediaItem *p2) {
                    
                    if (p1.viewCounter < p2.viewCounter) {
                        return NSOrderedDescending;
                    }  else {
                        return NSOrderedAscending;
                    }
                    
                }];
                 
            } else if (_orderBy == TVOrderByRating) {
                
                _channelsList = [EPGM.channelsList sortedArrayUsingComparator:^NSComparisonResult(TVMediaItem *p1, TVMediaItem *p2) {
                    
                    if (p1.likeCounter < p2.likeCounter) {
                        return NSOrderedDescending;
                    }  else {
                        return NSOrderedAscending;
                    }
                    
                }];
                
                
            }
         
        } else {
            _channelsList = EPGM.channelsList;
        }
        
        
        /* "PSC : ONO : SYN-4030 VFGTVONES-200 - iPhone/EPG/ Bad behaviour when going back to the EPG from the information screen of a future event*/
        searchMode=NO;
        [self buttonNowPressed:nil];
        [self reloadCollection];
        [_collectionViewEPG scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
    }

    if (_buttonSortOpen) {
        
        if (!sortOnly) {
            if (isPhone) {
                EPGSortViewControllerPhone *sortController = (EPGSortViewControllerPhone *)[self menuContainerViewController].rightMenuViewController;
                if ([sortController isKindOfClass:[EPGSortViewControllerPhone class]]) {
                    [sortController.sortView updateWithMenuItem:_epgMenuItem filters:_filterMenuItemList sortOnly:sortOnly orderBy:_orderBy];
                }
            }
        }
        
        _buttonSortOpen.selected = [_filterMenuItemList count];
    }
    
    if (isPad) {
    
        [_sortView updateWithMenuItem:_epgMenuItem filters:_filterMenuItemList sortOnly:sortOnly orderBy:_orderBy];
        
    }
}

- (void)selectFilterOption:(TVMenuItem *)filterItem {
    
    if (![_filterMenuItemList count] || ![[_filterMenuItemList lastObject] isEqual:filterItem]) {
        [_filterMenuItemList addObject:filterItem];
        
        [self epgListInitialization];
        
    }
    
}

- (void)selectMyChannel:(NSString *)filterItem {
    
    [_filterMenuItemList addObject:filterItem];
    
    [self epgListInitialization];
    
}

- (void)selectFilterBack {
    
    if ([_filterMenuItemList count]) {
        [_filterMenuItemList removeLastObject];
        
        [self epgListInitialization];
        
    }
}

- (void)selectSortBy:(TVOrderBy)orderBy {
    
    _orderBy = orderBy;
    
    [self epgListInitialization];
    
}

- (void)closeSelectSortScreen {

    if (isPhone) {
        [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
    } else {
        [self buttonSegmentSort:_buttonSegmentChannels];
    }
    
}

- (void)reloadCollection {
    
    [_collectionViewEPG reloadData];
    _viewBgPhone.hidden = ([_channelsList count] == 0);
    
    if (_channelsList.count > 0){
        [_selectDateView  restoreButtons];
    } else {
        [_selectDateView  clearSearchResultsAndDisableButtons];
    }
}

#pragma mark - EPGSelectDateViewDelegate;

- (void)updateTimeShowBar {
    
    timeShowBar = CFAbsoluteTimeGetCurrent();
    
}

- (void)updateDateLabel {
    
    [self.recoredAdapter cleanAllReoredsDataFromMemory];
    NSArray * keys =[self.dictionaryNpvrRequest allKeys];
    for (NSString * key in keys)
    {
        TVPAPIRequest * req = [self.dictionaryNpvrRequest objectForKey:key];
        [req cancel];
    }
    [self.dictionaryNpvrRequest removeAllObjects];
    
    if (isPhone) {
        _buttonDate.selected = YES;
    }
    
    if(isPad){
        self.buttonNow.selected = NO;
    }
    
    NSString *title = [_selectDateView selectedDateTitle];
    [self setButtonTitle:title button:_buttonDate];
    
    if (_selectedDay != _selectDateView.selectedDay) {
        
        NSInteger dDay = _selectDateView.selectedDay - _selectedDay;
        self.epgDate = [self.epgDate dateByAddingDays:dDay];
        //NSLog(@"updateDateLabel %@", self.epgDate);
        
        currentInterval = [self.epgDate timeIntervalSince1970];
        
        [_selectTimeView updateSelectedTime:currentInterval];
        
        _selectedDay = _selectDateView.selectedDay;
        
        if (searchMode) [self updateSearchListResult];
        
        [self reloadCollection];
        
    }
    
}

- (void)updateTimeLabel {
    
    if (isPhone) {
        _buttonTime.selected = YES;
    }
    
    if(isPad){
        self.buttonNow.selected = NO;
    }
    
    currentInterval = _selectTimeView.currentInterval;
    
    self.epgDate = [NSDate dateWithTimeIntervalSince1970:currentInterval];
    //NSLog(@"updateTimeLabel %@", self.epgDate);
    
    NSString *title = [_selectTimeView timeBubbleTitle];
    [self setButtonTitle:title button:_buttonTime];

    [self reloadCollection];
    
}

- (void)setButtonTitle:(NSString *)title button:(UIButton *)button {
    
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateHighlighted];
    [button setTitle:title forState:UIControlStateSelected];
    
}

- (void)hideBarCompleteProcess {
    
    timeShowBar = 0;
    
    [self hideBarProcess];
    
}

- (void)hideBarProcess {
    
    if (isPhone) {
        if (buttonDaySelected) {
            if (CFAbsoluteTimeGetCurrent() - timeShowBar > 6.0) {
                [self buttonDatePressed:_buttonDate];
            }
        } else if (buttonTimeSelected) {
            if (CFAbsoluteTimeGetCurrent() - timeShowBar > 6.0) {
                [self buttonTimePressed:_buttonTime];
            }
        } else {
            timeShowBar = CFAbsoluteTimeGetCurrent();
        }
    
    }
}

#pragma mark - Button actions

- (IBAction)buttonNowPressed:(UIButton *)button {
    
    if (isPad && _buttonSearch.selected) {
    
        [self buttonSearchPressed:_buttonSearch];
        return;
        
    }
    
    if (searchMode) {
        
        [self epgListInitialization];
    }
    
    if (![_textFieldSearch isFirstResponder]) {
        
        [self restoreSearchButtons];
        
    }
    
    searchMode = NO;
    _buttonDate.enabled = YES;
    _buttonTime.enabled = YES;
    
    [_selectTimeView updateForSearchMode:searchMode];
    
    
    self.selectedDay = 0;
    self.epgDate = [NSDate date];
    currentInterval = [self.epgDate timeIntervalSince1970];
    
    if (isPhone) {
        
        if (buttonDaySelected) {
            [self buttonDatePressed:_buttonDate];
        }
        
        if (buttonTimeSelected) {
            [self buttonTimePressed:_buttonTime];
        }
        
    } else {
        
        [_selectTimeView updateSelectedTime:currentInterval];
        
    }
    
    [_selectDateView setToday];
    
    buttonDaySelected = NO;
    buttonTimeSelected = NO;
    
    _buttonDate.selected = NO;
    _buttonTime.selected = NO;
    
    [self setButtonTitle:LS(@"today") button:_buttonDate];
    [self setButtonTitle:[[NSDate date] stringFromDateWithDateFormat:@"HH:mm"] button:_buttonTime];
    
    [self reloadCollection];
    
    self.buttonNow.selected = YES;
/*
    if (isPad && _buttonSearch.selected) {
        _buttonSearch.selected = NO;
        _viewSearchForm.hidden = YES;
    }
*/
}

- (IBAction)buttonDatePressed:(UIButton *)button {
    
    [self updateTimeShowBar];

    if (![_textFieldSearch isFirstResponder]) {
        
        [self restoreSearchButtons];

    }
    _buttonNow.selected = NO;
    
    if (buttonTimeSelected) {
        [self buttonTimePressed:_buttonTime];
    }
    
    _buttonTime.selected = NO;
    
    buttonDaySelected = !buttonDaySelected;
    if (buttonDaySelected) _buttonDate.selected = buttonDaySelected;
    
    if (buttonDaySelected) {
        [_selectDateView updateSelectDateBubble];
    }
    
    _selectDateView.imgSelectDayShadow.alpha = 1.0;
    _selectDateView.viewSelectedDateBubble.alpha = 0.0;
    
    [UIView animateWithDuration:0.2 animations:^{
        _selectDateView.frame = CGRectMake(_selectDateView.frame.origin.x, _viewButtonsPhone.frame.origin.y - (buttonDaySelected ? _selectDateView.frame.size.height : 0), _selectDateView.frame.size.width, _selectDateView.frame.size.height);
    }];
    
    
}


- (IBAction)buttonTimePressed:(UIButton *)button {
    
    [self restoreSearchButtons];

    [self updateTimeShowBar];
    
    _buttonNow.selected = NO;
    
    if (buttonDaySelected) {
        [self buttonDatePressed:_buttonDate];
    }
    
    _buttonDate.selected = NO;
    
    buttonTimeSelected = !buttonTimeSelected;
    if (buttonTimeSelected) _buttonTime.selected = buttonTimeSelected;
    
    if (buttonTimeSelected) {
        
        [_selectTimeView updateSelectedTime:currentInterval];
        
    } else {
        
        [_selectTimeView stopScrollView];
        
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        _selectTimeView.frame = CGRectMake(_selectTimeView.frame.origin.x, _viewButtonsPhone.frame.origin.y - (buttonTimeSelected ? _selectTimeView.frame.size.height : 0), _selectTimeView.frame.size.width, _selectTimeView.frame.size.height);
        
    }];
    
}

- (NSString *)selectedDayKey {

    NSDate *date = [NSDate date];
    
    date = [date dateByAddingDays:self.selectedDay];
    
    return [date stringFromDateWithDateStyle:NSDateFormatterShortStyle];
    
}

- (NSString *)channelSelectedDayKey:(TVMediaItem *)mediaItem {
    
    return [NSString stringWithFormat:@"%@_%@", [self selectedDayKey], mediaItem.epgChannelID];
    
}

- (void)loadProgramForChannel:(TVMediaItem *) channel {
    
    NSDate *date = [NSDate date];
    
    date = [self dateWithOutTime:[date dateByAddingDays:self.selectedDay]];
    NSDate *dateLast = [date dateByAddingDays:1];
    //NSLog(@"%@  %@", date, dateLast);
    
    //__weak NSString *weakKey = [self channelSelectedDayKey:channel];
    
    __weak EPGViewController * weakSelf = self;
    __weak TVMediaItem * weakChannel = channel;
    
    //NSLog(@"%@  %@", date, dateLast);
    APSBlockOperation * request = [[APSEPGManager sharedEPGManager] programsByRangeForChannel:channel.epgChannelID fromDate:date toDate:dateLast actualFromDate:date actualToDate:dateLast includeProgramsDuringfromDate:YES includeProgramsDuringToDate:YES privateContext:self.privateContext starterBlock:^{
    } failedBlock:^{
    
    } completionBlock:^(NSArray *programs) {
        
        
        NSString *key = [weakSelf channelSelectedDayKey:weakChannel];
        if (programs != nil)
        {
            NSArray *array = [programs  sortedArrayUsingComparator:^NSComparisonResult(APSTVProgram *obj1, APSTVProgram *obj2) {
                
                NSDate *date1 = obj1.startDateTime;
                NSDate *date2 = obj2.startDateTime;
                
                return [date1 compare:date2];
                
            }];
            
            NSMutableArray *fillArray = [EPGM addEmptyProgramsForEmptySpacesInProgramsArray:[array mutableCopy] forDate:_epgDate privateContext:self.privateContext];
            NSLog(@"array count %d %@", [fillArray count], key);
            /*
            for (APSTVProgram *obj in fillArray) {
                NSLog(@"'%@' - '%@'", obj.startDateTime, obj.endDateTime);
            }
            */
            [weakSelf.dictionaryCurrentProgramByChannelID setObject:fillArray forKey:key];
        }
        else
        {
            [weakSelf.dictionaryCurrentProgramByChannelID setObject:[NSArray array] forKey:key];
        }
        
        [weakSelf.collectionViewEPG reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:[_channelsList indexOfObject:weakChannel] inSection:0]]];
        
    }];
    
    if (request != nil)
    {
        NSString *key = [weakSelf channelSelectedDayKey:weakChannel];
        [self.dictionaryRequest setObject:request forKey:key];
    }
}

- (APSTVProgram *)programInEPGDate:(NSArray *)array {

    for (APSTVProgram *program in array) {
        if ([program programInTime:_epgDate]) {
            return program;
        }
    }
    
    return nil;
}

- (NSInteger)programIndexInEPGDate:(NSArray *)array {
    
    if (array) {
    
        for (NSInteger i = 0; i < [array count]; i++) {
            APSTVProgram *program = [array objectAtIndex:i];
            if ([program programInTime:_epgDate]) {
                return i;
            }
        }
        
    }
    
    return -1;
}

#pragma mark - Collection View Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if ([_channelsList count]) {
        
        NSInteger channelsCount = 0;
        NSInteger programmsCount = 0;
        
        for (TVMediaItem *channel in _channelsList) {
            NSString *key = [self channelSelectedDayKey:channel];
            NSArray *array = [self.dictionaryCurrentProgramByChannelID objectForKey:key];
            if (array && [array count]) {
                channelsCount++;
                programmsCount += [array count];
            }
        }
        
        if ((channelsCount > 0) && (programmsCount > 0)) {
            self.labelAutomation.accessibilityValue = [NSString stringWithFormat:@"%lu channels in EPG, %lu programms found", (long)channelsCount, (long)programmsCount];
        }
        
    }
    
    return [_channelsList count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    EPGCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:@"EPGCollectionViewCell" forIndexPath:indexPath];
    
    
    TVMediaItem *channel = [_channelsList objectAtIndex:indexPath.row];
    
    [cellView updateCellWithMediaItem:channel];
    
    cellView.channel = channel;
    
    
    if (searchMode) {
        
        if (_selectedDay + APST.numberOfRetroDays < [_searchDays count]) {
            
            NSDictionary *item = [_searchDays objectAtIndex:_selectedDay + APST.numberOfRetroDays];
            
            NSString *key = [NSString stringWithFormat:@"%d_%d", [[item objectForKey:@"day"] intValue], [[item objectForKey:@"month"] intValue]];
            
            for (NSDictionary *dic in _searchFullListChannels) {
                
                if ([dic objectForKey:key]) {
                    
                    TVMediaItem *channelSearch = [dic objectForKey:@"channel"];
                    
                    if ([channel.mediaID isEqualToString:channelSearch.mediaID]) {
                        
                        cellView.viewEPG.hidden = NO;
                        cellView.viewLoading.hidden = YES;
                        cellView.currentProgramIndex = 0;

                        NSArray *array = [dic objectForKey:key];
                        APSTVProgram *program = nil;
                        if (array && [array count]) {
                            program = [array objectAtIndex:cellView.currentProgramIndex];
                        }
                        
                        cellView.program = program;
                        if (program)
                        {
                            cellView.epgList = array;
                        }
                        [cellView updateProgramItem:cellView.currentProgramIndex];
                        

                    }
                    
                }
                
            }
            
        }

        
    } else {
        
        
        NSString *key = [self channelSelectedDayKey:channel];
        NSArray *array = [self.dictionaryCurrentProgramByChannelID objectForKey:key];
        if (array == nil && [self.dictionaryRequest objectForKey:key] == nil) {
            [self loadProgramForChannel:channel];
            cellView.viewEPG.hidden = YES;
            cellView.viewLoading.hidden = NO;
        } else {
            cellView.viewEPG.hidden = NO;
            cellView.viewLoading.hidden = YES;
        }
        
        APSTVProgram *program = nil;
        if (array && [array count])
        {
            program = [self programInEPGDate:array];
            // check nPVR
            if (isPad)
            {
                NSString *key = [self channelSelectedDayKey:channel];
                NSMutableDictionary * recoredsTOChannel = [self.recoredAdapter takeAllChannelRecoreds:channel];

                if (recoredsTOChannel == nil && [self.dictionaryNpvrRequest objectForKey:key] == nil)
                {
                    [self loadRecoredByChannel:channel];
                }
            }
        }
        
        cellView.program = program;
        cellView.recoredsOfChannel = [self.recoredAdapter takeAllChannelRecoreds:channel];
        if (program)
        {
            cellView.epgList = array;
        } else {
            cellView.epgList = [NSArray array];
        }
        cellView.currentProgramIndex = [self programIndexInEPGDate:array];
        [cellView updateProgramItem:cellView.currentProgramIndex animated:YES];
    }
    
    cellView.delegate = self;
    
    return cellView;
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (![collectionView.indexPathsForVisibleItems containsObject:indexPath]) {
        
        if (indexPath.item < _channelsList.count) {

            TVMediaItem * channel = _channelsList[indexPath.item];
            NSString *key = [self channelSelectedDayKey:channel];

            APSBlockOperation * request = [self.dictionaryRequest objectForKey:key];
            [request cancel];
            [self.dictionaryRequest removeObjectForKey:key];
            
            TVPAPIRequest * npvrRequest = [self.dictionaryNpvrRequest objectForKey:key];
            [npvrRequest cancel];
            [self.dictionaryNpvrRequest removeObjectForKey:key];
        }
        
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
  //  self.searchListView.alpha = 0.0;

}

- (NSDate *)dateWithOutTime:(NSDate *)datDate {
    if( datDate == nil ) {
        datDate = [NSDate date];
    }
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:datDate];
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

#pragma mark - EPGCollectionViewCellDelegate

- (void)addMediaToFavorites:(TVMediaItem *)mediaItem {
    [self.socialManager addMediaToFavorites:mediaItem];
}

- (void)removeMediaFromFavorites:(TVMediaItem *)mediaItem {
    [self.socialManager removeMediaFromFavorites:mediaItem];
    
    if (useMyChannels) {
        
        int index = -1;
        int i = 0;
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for (TVMediaItem *item in _channelsList) {
            if ([item.mediaID isEqualToString:mediaItem.mediaID]) {
                index = i;
            } else {
                [array addObject:item];
            }
            
            i++;
        }
        
        _channelsList = array;
        if (index >= 0) {
            
            [_collectionViewEPG deleteItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]]];
            if (_channelsList.count == 0){
                _emptyChannelView.hidden = NO;
                _viewBgPhone.hidden = YES;
                [_selectDateView  clearSearchResultsAndDisableButtons];

            }
        }
        
    }
    
}


- (void)openChannelViewController:(EPGCollectionViewCell *)collectionViewCell {
    
    EPGChannelViewController *controller = [[ViewControllersFactory defaultFactory] epgChannelViewController];
    controller.channel = collectionViewCell.channel;
    controller.epgList = collectionViewCell.epgList;
    controller.currentProgramIndex = collectionViewCell.currentProgramIndex;
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

#pragma mark - EPGCollectionViewCell Delegate -

- (void) epgCell:(EPGCollectionViewCell *) cell didSelectPlayProgrma:(APSTVProgram *) program channel:(TVMediaItem *) mediaItem
{
    [self openMediaItem:mediaItem withProgram:program];
}

- (void) epgCell:(EPGCollectionViewCell *) cell programItemView:(EPGProgramItemView *) programItemView didSelectRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem isSeries:(BOOL)isSeries
{
    RecoredHelper * recHelp = [self.recoredAdapter takeOrMakeRecoredHelperWithMediaItem:mediaItem andProgram:program isSeries:isSeries];
    if (isSeries)
    {
        self.epgRecoredPopUp = [[CustomViewFactory defaultFactory] epgRecoredPopUp];// [[EpgRecoredPopUp alloc] init];
        [self.epgRecoredPopUp buildWithOnLinkageView:programItemView.buttonRecored withRecoredHelper:recHelp toolTipArrowType:ToolTipTypeArrowDynamic toolCloseType:ToolTipCloseFullScreen];
         self.epgRecoredPopUp.delegate = self;
        [self.view addSubview:self.epgRecoredPopUp];
    }
    else
    {
        [self recordAsset:program mediaItem:mediaItem];
    }
}

- (void) epgCell:(EPGCollectionViewCell *) cell programItemView:(EPGProgramItemView *) programItemView didSelectCancelRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem recoredItem:(TVNPVRRecordItem *) recoredItem isSeries:(BOOL)isSeries
{
    if (isSeries) // programItemView
    {
        RecoredHelper * rec = [self.recoredAdapter recoredHelperToepgProgram:program mediaItem:mediaItem];
        self.cancelSeriesRecordingPopUp = [[CustomViewFactory defaultFactory] cancelSeriesRecordingPopUpWithRecoredHelper:rec andLinkageView:programItemView];
       // [[CancelSeriesRecordingPopUp alloc] initWithNibWithRecoredHelper:rec andLinkageView:programItemView];
        self.cancelSeriesRecordingPopUp.delegate = self;
        [self.view addSubview:self.cancelSeriesRecordingPopUp];
    }
    else
    {
        [self.recoredAdapter deleteOneRecored:recoredItem Program: program mediaItem: mediaItem];
    }
}

#pragma mark - EpgRecoredPopUp Delegate -

-(void) epgRecoredPopUp:(EpgRecoredPopUp *)epgRecoredPopUp recredType:(RecoredType)recoredType recoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem andLinkageView:(UIView *)linkageView;
{
    if (recoredType == RecoredTypeEpisode)
    {
        [self recordAsset:program mediaItem: mediaItem];
    }
    else
    {
        [self recordSeriesByProgramId:program mediaItem:mediaItem];
    }
}

#pragma mark - cancelSeriesRecordingPopUp Delegate -

-(void) cancelSeriesRecordingPopUp:(CancelSeriesRecordingPopUp *)epgRecoredPopUp cnacelRecoredType:(CnacelRecoredType)cnacelRecoredType WithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView*)linkageView
{
    [self.recoredAdapter cancelOrDeleteSeriesRecordingWithCnacelRecoredType:cnacelRecoredType WithRecoredHelper:recoredHelper];
}

-(void) cancelSeriesRecordingPopUp:(CancelSeriesRecordingPopUp *)epgRecoredPopUp didCancelViewWithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView*)linkageView
{
    
    [self reloadAfterActionProgram:recoredHelper.program mediaItem:recoredHelper.mediaItem recordAdapterStatus:RecoredAdapterStatusOk];
}

#pragma mark - recoredAdapter Calls -

- (void)recordSeriesByProgramId:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    [self.recoredAdapter recordSeriesByProgram:epgProgram mediaItem:mediaItem];
}

- (void)recordAsset:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    [self.recoredAdapter recordAsset:epgProgram mediaItem:mediaItem];
}

-(void)reloadAfterActionProgram:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem recordAdapterStatus:(RecoredAdapterStatus)recordAdapterStatus {
    
    NSInteger index = -1;
    for ( TVMediaItem * item in _channelsList)
    {
        if ([item.mediaID isEqualToString:mediaItem.mediaID])
        {
            index++;
            break;
        }
        index++;
    }
    if (index > -1)
    {
        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        EPGCollectionViewCell * cell = (EPGCollectionViewCell *)[self.collectionViewEPG cellForItemAtIndexPath:indexPath];
        if (cell)
        {
            RecoredHelper * rec = [self.recoredAdapter recoredHelperToepgProgram:epgProgram mediaItem:mediaItem];
            cell.currRecoredHelper = (recordAdapterStatus == RecoredAdapterStatusOk) ? rec : nil;
            [cell updateProgramItem:cell.currentProgramIndex animated:NO];
        }
    }
}

-(void)loadRecoredByChannel:(TVMediaItem *)epgChannel
{
    NSString *key = [self channelSelectedDayKey:epgChannel];
    NSArray *array = [self.dictionaryCurrentProgramByChannelID objectForKey:key];
    
    TVPAPIRequest * request =   [self.recoredAdapter loadRecoredByChannel:epgChannel andPrograms:array scrollToProgram:nil];
    
    if (request != nil)
    {
        NSString *key = [self channelSelectedDayKey:epgChannel];
        [self.dictionaryNpvrRequest setObject:request forKey:key];
   }
}

#pragma mark - Recored Adapter delegate -

-(void)recoredAdapter:(RecoredAdapter *)recoredAdapter complateLoadRecoredItemsToChannel:(TVMediaItem*)mediaItem isChannelHeveRecoredItems:(BOOL)isChannelHeveRecoredItems scrollToProgram:(APSTVProgram *)scrolledProgram
{
    if (isChannelHeveRecoredItems == YES)
    {
        __weak EPGViewController * weakSelf = self;
        __weak TVMediaItem * weakChannel = mediaItem;
        NSInteger ind = [weakSelf.channelsList indexOfObject:weakChannel];
        EPGCollectionViewCell * cellView = (EPGCollectionViewCell *) [weakSelf.collectionViewEPG cellForItemAtIndexPath:[NSIndexPath indexPathForItem:ind inSection:0]];
        NSInteger currentProgramIndex = cellView.currentProgramIndex;
        
        // see: http://stackoverflow.com/questions/14094684/avoid-animation-of-uicollectionview-after-reloaditemsatindexpaths
        [UIView performWithoutAnimation:^{
            
            NSInteger ind = [weakSelf.channelsList indexOfObject:weakChannel];
            [weakSelf.collectionViewEPG reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:ind inSection:0]]];
            if (scrolledProgram)
            {
                cellView.recoredsOfChannel = [recoredAdapter takeAllChannelRecoreds:mediaItem];
                [cellView updateProgramItem:currentProgramIndex animated:NO];
            }
        }];
    }
}

-(void) recoredAdapter:(RecoredAdapter *)recoredAdapter  complateCancelOrDeleateAssetRecording:(TVNPVRRecordItem *) recordItem  Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus
{
    
    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
          [self reloadAfterActionProgram:epgProgram mediaItem:mediaItem recordAdapterStatus:recoredAdapterStatus];
    }
    RecoredHelper * rec = [self.recoredAdapter recoredHelperToepgProgram:epgProgram mediaItem:mediaItem];
    
    if (self.recoredStatusPopupView ==  nil)
    {
        self.recoredStatusPopupView = [[CustomViewFactory defaultFactory] epgRcoredStatusView];
    }
    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewDeleteRecoredSecsses ViewOnView:self.recoredStatusView isSeries:rec.isSeries];
    }
    else
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewDeleteRecoredFailed ViewOnView:self.recoredStatusView isSeries:rec.isSeries];
    }

}

-(void) recoredAdapter:(RecoredAdapter *)recoredAdapter  complateCancelOrDeleateSeriesRecording:(RecoredHelper*)recoredHelper recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus
{
    RecoredHelper * rec = [self.recoredAdapter recoredHelperToepgProgram:recoredHelper.program mediaItem:recoredHelper.mediaItem];

    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
        [self.recoredStatusPopupView addEPGRcored:recoredHelper.program status:EPGRcoredStatusViewDeleteRecoredSecsses ViewOnView:self.recoredStatusView isSeries:rec.isSeries];
    }
    else
    {
        [self.recoredStatusPopupView addEPGRcored:recoredHelper.program status:EPGRcoredStatusViewDeleteRecoredFailed ViewOnView:self.recoredStatusView isSeries:rec.isSeries];
    }
}

-(void)recoredAdapter:(RecoredAdapter *)recoredAdapter complateRecordAssetOrSeriesToToChannel:(TVMediaItem*)mediaItem Program:(APSTVProgram *) epgProgram recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus
{
    [self reloadAfterActionProgram:epgProgram mediaItem:mediaItem recordAdapterStatus:recoredAdapterStatus];
    

        
    RecoredHelper * rec = [self.recoredAdapter recoredHelperToepgProgram:epgProgram mediaItem:mediaItem];
    
    if (self.recoredStatusPopupView ==  nil)
    {
        self.recoredStatusPopupView = [[CustomViewFactory defaultFactory] epgRcoredStatusView];
    }
    // [[EPGRcoredStatusView alloc] initWithNib];
    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewRcoredingSecsses ViewOnView:self.recoredStatusView isSeries:rec.isSeries];
    }
    else
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewRcoredingFailed ViewOnView:self.recoredStatusView isSeries:rec.isSeries];
    }
}

-(NSArray*) currentProgramsToChannel:(TVMediaItem*)channel {
    return [self.dictionaryCurrentProgramByChannelID objectForKey:[self channelSelectedDayKey:channel]];
}

#pragma mark -

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self restoreSearchButtons];
    
    [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{}];
    
}

-(void) restoreSearchButtons{
    
    _textFieldSearch.text = @"";
    _searchListView.buttonSearch.hidden = NO;
    _searchListView.searchStr = @"";
    [_searchListView updateSearchTable];
    _searchListView.alpha = 0.0;
    if ([_channelsList count]) {
        _searchBlankView.alpha = 0.0;
        _collectionViewEPG.alpha = 1.0;
    }
    
    if ([_textFieldSearch isFirstResponder]){
        
        [_textFieldSearch resignFirstResponder];
    }
}

#pragma mark -
#pragma mark - UIKeyboardWillHideNotification Selectors

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    CGRect kbRect = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardRect = [_searchListView.superview convertRect:kbRect fromView:nil];
    
    CGFloat searchY = _viewSearchForm.frame.origin.y + _viewSearchForm.frame.size.height;
    _searchListView.frame = CGRectMake(_viewSearchForm.frame.origin.x, searchY, _viewSearchForm.frame.size.width, keyboardRect.origin.y - searchY);
}


- (void)keyboardWillHide:(NSNotification*)aNotification {
    CGFloat searchY = _viewSearchForm.frame.origin.y + _viewSearchForm.frame.size.height;
    CGFloat height = super.viewContent.frame.size.height - searchY;
    if (isPad) {
        height -= _viewTimePad.frame.size.height;
    }else {
        height -=_viewButtonsPhone.frame.size.height;
    }
    _searchListView.frame = CGRectMake(_viewSearchForm.frame.origin.x, searchY, _viewSearchForm.frame.size.width, height);
}


- (void)dealloc {

    //NSLog(@"DEALLOC %d", [_dictionaryRequest count]);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
