//
//  EPGSelectDateViewPhoneVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 2/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGSelectDateViewPhoneVF.h"

@implementation EPGSelectDateViewPhoneVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */

- (void)awakeFromNib {
    
    self.seperatorColor = [UIColor colorWithWhite:169.0/255.0 alpha:1];;
    
    [super awakeFromNib];
}

@end
