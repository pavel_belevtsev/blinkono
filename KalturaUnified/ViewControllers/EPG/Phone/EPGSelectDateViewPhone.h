//
//  EPGSelectDateViewPhone.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSelectDateView.h"

@interface EPGSelectDateViewPhone : EPGSelectDateView <UIScrollViewDelegate> {
    
}

@property (nonatomic, weak) IBOutlet UIView *viewTapToday;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewSelectDay;
@property (nonatomic, weak) IBOutlet UIImageView *imgBubbleDate;
@property (nonatomic, weak) IBOutlet UILabel *labelSelectDateBubble;

// the following viewTopEdge is a 1 px high view at the top of the frame
// the outlet is used by customized products (vodafone) to change the bg color
@property (weak, nonatomic) IBOutlet UIView *viewTopEdge;

@property (nonatomic, strong) UIColor* seperatorColor;
@end
