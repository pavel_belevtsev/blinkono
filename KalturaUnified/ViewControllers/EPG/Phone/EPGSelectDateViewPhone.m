//
//  EPGSelectDateViewPhone.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSelectDateViewPhone.h"
#import "UIImage+Tint.h"
#import "NSDate+Utilities.h"

@implementation EPGSelectDateViewPhone

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self initUI];
    
}

- (void)initUI {
    
    [Utils setLabelFont:_labelSelectDateBubble bold:YES];
    
    int epgDayWidth = _scrollViewSelectDay.frame.size.width;
    
    _scrollViewSelectDay.contentSize = CGSizeMake(epgDayWidth * (APST.numberOfRetroDays + APST.numberOfWeekDays), _scrollViewSelectDay.frame.size.height);
    
    UIView *viewSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, _scrollViewSelectDay.frame.size.height)];
    viewSeparator.backgroundColor = [UIColor blackColor];
    viewSeparator.alpha = 0.3;
    [_scrollViewSelectDay addSubview:viewSeparator];
    
    for (int i = 0; i < (APST.numberOfRetroDays + APST.numberOfWeekDays); i++) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((epgDayWidth * i) + 5, 0, epgDayWidth - 10, _scrollViewSelectDay.frame.size.height)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:FONT_BOLD size:13.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.adjustsFontSizeToFitWidth = YES;
        
        label.text = [self dateTitle:i];
        
        if (i == APST.numberOfRetroDays) {
            label.alpha = 1.0;
        } else {
            label.alpha = 0.3;
        }
        
        label.tag = 100 + i;
        
        [_scrollViewSelectDay addSubview:label];
        
        viewSeparator = [[UIView alloc] initWithFrame:CGRectMake(_scrollViewSelectDay.frame.size.width * (i + 1), 0, 1, _scrollViewSelectDay.frame.size.height)];
        viewSeparator.backgroundColor = APST.brandTextColor;
        viewSeparator.alpha = 0.3;
        [_scrollViewSelectDay addSubview:viewSeparator];
        
        UIImage *imgCorner = [UIImage imageNamed:@"epg_search_corner"];
        UIView *viewCorner = [[UIView alloc] initWithFrame:CGRectMake((_scrollViewSelectDay.frame.size.width * (i + 1)) - imgCorner.size.width, 0, imgCorner.size.width, imgCorner.size.height)];
        viewCorner.backgroundColor = [UIColor clearColor];
        UIImageView *imgViewCorner = [[UIImageView alloc] initWithImage:imgCorner];
        imgViewCorner.alpha = 0.5;
        [viewCorner addSubview:imgViewCorner];
        [_scrollViewSelectDay addSubview:viewCorner];
        viewCorner.userInteractionEnabled = NO;
        
        UILabel *labelCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, imgCorner.size.width - 2, 9 + 4)];
        labelCount.backgroundColor = [UIColor clearColor];
        labelCount.textColor = [UIColor blackColor];
        labelCount.font = Font_Reg(9);
        labelCount.text = @"99";
        labelCount.textAlignment = NSTextAlignmentRight;
        [viewCorner addSubview:labelCount];
        
        viewCorner.tag = 200 + i;
        viewCorner.hidden = YES;
        
        
    }
    
    _scrollViewSelectDay.delegate = nil;
    [_scrollViewSelectDay setContentOffset:CGPointMake(APST.numberOfRetroDays * epgDayWidth, 0)];
    _scrollViewSelectDay.delegate = self;
    
    UITapGestureRecognizer *tapDateRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDateRecognize:)];
    [_viewTapToday addGestureRecognizer:tapDateRecognizer];
    
    [self skinChanged];
    
    
}

- (void)skinChanged {
 
    _imgBubbleDate.image = [[UIImage imageNamed:@"epg_buble"] imageTintedWithColor:APST.brandColor];
    _labelSelectDateBubble.textColor = APST.brandTextColor;
    
    for (int i = 0; i < (APST.numberOfRetroDays + APST.numberOfWeekDays); i++) {
        
        UILabel *label = (UILabel *)[_scrollViewSelectDay viewWithTag:100 + i];
        if ([label isKindOfClass:[UILabel class]]) {
            label.textColor = APST.brandTextColor;
        }
        
    }
    
    self.backgroundColor = APST.brandColor;
    ((UIView *)[[self subviews] objectAtIndex:0]).backgroundColor = APST.brandColor;
    
}

- (void)tapDateRecognize:(UITapGestureRecognizer *)recognizer {
    
    CGPoint point = [recognizer locationInView:_viewTapToday];
    
    int epgDayWidth = 80;
    
    int dPage = (((int)(point.x + (epgDayWidth / 2))) / epgDayWidth) - 2;
    
    if (dPage != 0) {
        
        
        int page = ((int)_scrollViewSelectDay.contentOffset.x) / ((int)_scrollViewSelectDay.frame.size.width) + dPage;
        
        if (page >= 0 && page < (APST.numberOfRetroDays + APST.numberOfWeekDays)) {
            
            [_scrollViewSelectDay setContentOffset:CGPointMake(_scrollViewSelectDay.frame.size.width * page, 0) animated:YES];
        }
        
    }
    
}

- (NSString *)dateBubbleTitle {
    
    NSString *title = [_labelSelectDateBubble.text lowercaseString];
    return [title capitalizedString];
    
}

- (void)setToday {
    
    [self clearSearchResults];

    _scrollViewSelectDay.contentOffset = CGPointMake(_scrollViewSelectDay.frame.size.width * APST.numberOfRetroDays, 0);
    
}

- (void)setDay:(NSInteger)day {
    
    _scrollViewSelectDay.contentOffset = CGPointMake(_scrollViewSelectDay.frame.size.width * (APST.numberOfRetroDays + day), 0);
    [self updateLabelsForPage:day];
    
}

#pragma mark - UIScrollView

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    [UIView animateWithDuration:0.2 animations:^{
        super.imgSelectDayShadow.alpha = 1.0;
        super.viewSelectedDateBubble.alpha = 0.0;
    } completion:^(BOOL finished){
        [super.delegate hideBarCompleteProcess];

        
    }];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (!decelerate) {
        
        [UIView animateWithDuration:0.2 animations:^{
            super.imgSelectDayShadow.alpha = 1.0;
            super.viewSelectedDateBubble.alpha = 0.0;
        } completion:^(BOOL finished){
            
            //[self buttonDatePressed:buttonDate];
            
        }];
        
    }
    
}

- (void)updateLabelsForPage:(NSInteger)page {
    
    if (page != super.selectedDay) {
        
        super.selectedDay = page;
        
    }
    
    UILabel *label = (UILabel *)[_scrollViewSelectDay viewWithTag:(APST.numberOfRetroDays + page) + 100];
    
    if (label) _labelSelectDateBubble.text = label.text;
    
    [UIView animateWithDuration:0.2 animations:^{
        super.imgSelectDayShadow.alpha = 1.0;
        super.viewSelectedDateBubble.alpha = 0.0;
    } completion:^(BOOL finished){
//        [super.delegate hideBarCompleteProcess];
    }];
    
    [super.delegate updateDateLabel];
    
    for (int i = 0; i < (APST.numberOfRetroDays + APST.numberOfWeekDays); i++) {
        UILabel *label = (UILabel *)[_scrollViewSelectDay viewWithTag:100 + i];
        if (label) {
            label.alpha = (i == (page + APST.numberOfRetroDays)) ? 1.0 : 0.5;
        }
    }

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [super.delegate updateTimeShowBar];
    
    if (((int)scrollView.contentOffset.x) % ((int)scrollView.frame.size.width) == 0) {
        
        NSInteger page = ((NSInteger)scrollView.contentOffset.x) / ((NSInteger)scrollView.frame.size.width) - APST.numberOfRetroDays;
        [self updateLabelsForPage:page];
        
    } else {
        
        if (super.imgSelectDayShadow.alpha == 1.0) {
            
            [UIView animateWithDuration:0.2 animations:^{
                super.imgSelectDayShadow.alpha = 0.0;
                super.viewSelectedDateBubble.alpha = 1.0;
            }];
            
        }
        
        int page = (scrollView.contentOffset.x + scrollView.frame.size.width / 2) / scrollView.frame.size.width;
        
        UILabel *label = (UILabel *)[scrollView viewWithTag:page + 100];
        
        if (label) _labelSelectDateBubble.text = label.text;
    }
    
}

- (void)updateSelectDateBubble {

    int page = (_scrollViewSelectDay.contentOffset.x + _scrollViewSelectDay.frame.size.width / 2) / _scrollViewSelectDay.frame.size.width;
    UILabel *label = (UILabel *)[_scrollViewSelectDay viewWithTag:page + 100];
    if (label) _labelSelectDateBubble.text = label.text;

    
}

- (void)updateSearchResults:(NSArray *)days {
    
    for (int i = 0; i < (APST.numberOfRetroDays + APST.numberOfWeekDays); i++) {
    
        if (i < [days count]) {
            
            NSDictionary *day = [days objectAtIndex:i];
            
            UIView *viewCorner = [_scrollViewSelectDay viewWithTag:i + 200];
            if (viewCorner) {
                
                int count = [[day objectForKey:@"count"] intValue];
                if (count) {
                    viewCorner.hidden = NO;
                    UILabel *label = [[viewCorner subviews] lastObject];
                    label.text = [NSString stringWithFormat:@"%d", count];
                } else {
                    viewCorner.hidden = YES;
                }
            }
            
        }
    }
    
}

- (void)clearSearchResults {
    
    for (int i = 0; i < (APST.numberOfRetroDays + APST.numberOfWeekDays); i++) {
        
        UIView *viewCorner = [_scrollViewSelectDay viewWithTag:i + 200];
        if (viewCorner) {
            
            viewCorner.hidden = YES;
            
        }
    }
    
}

- (void)clearSearchResultsAndDisableButtons {
    /*
    for (UIButton *button in _scrollViewSelectDay.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            
            UIView *viewCorner = [button viewWithTag:button.tag + 200];
            if (viewCorner) {
                viewCorner.hidden = YES;
            }
            button.selected = NO;
            button.enabled = NO;
            
        }
    }
    */
}

- (void)restoreButtons {
    
    for (UIButton *button in _scrollViewSelectDay.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            
            if (button.tag == self.selectedDay + APST.numberOfRetroDays){
                button.selected = YES;
            }
            
            button.enabled = YES;
            
        }
    }
}

@end
