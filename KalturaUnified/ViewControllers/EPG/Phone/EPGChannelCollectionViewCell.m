//
//  EPGChannelCollectionViewCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGChannelCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "APSTVProgram+Time.h"
#import "UIImage+Tint.h"

#define  TVNButtonGrayColor  0x575757

@interface EPGChannelCollectionViewCell () {
    void(^blockRecordButton)(UIButton *btn);
}
@end

@implementation EPGChannelCollectionViewCell

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)awakeFromNib {
    // Initialization code
    
    [Utils setLabelFont:labelTime bold:YES];
    [Utils setLabelFont:labelTitle bold:YES];
    
    [Utils setLabelFont:labelDescription];
    
    [Utils setLabelFont:labelSubDescription bold:YES];
    
    imgThumb.image = [UIImage imageNamed:@"placeholder_2_3.png"];
                      
    [self.firstActionButton.titleLabel setFont:Font_Bold(15)];
    [self.secondActionButton.titleLabel setFont:Font_Bold(15)];
    [Utils setButtonFont:self.firstActionButton bold:YES];
    [Utils setButtonFont:self.secondActionButton bold:YES];
    [self createRecordButtonBlock];
    
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
}

- (void)skinChanged {

    if (self.item) {
        [self updateData:self.item];
    }
}

-(void)updateProgramImage
{
    NSURL *pictureURL = [NSURL URLWithString:self.item.pictureURL];
    
    if (!pictureURL)
    {
        pictureURL = [Utils getBestPictureURLForMediaItem:_mediaItem andSize:CGSizeMake(465.0, 262.0)];
    }
    
    if (pictureURL)
    {
        UIImage *imgDefault = [UIImage imageNamed:@"placeholder_2_3.png"];
        
        [imgThumb sd_setImageWithURL:pictureURL placeholderImage:imgDefault];
    }
    else
    {
        imgThumb.image = [UIImage imageNamed:@"placeholder_2_3.png"];
    }

}

- (void)updateData:(APSTVProgram *)program {
    
    self.item = program;
    
    self.buttonRecored.selected = NO;
    self.recoredIconImage.hidden =YES;
        
    [self updateProgramImage];
    
    UIColor *frameColor = ([program programIsOn] ? APST.brandColor : CS(@"a2a2a2"));
    
    viewBar.backgroundColor = frameColor;
    viewBarTime.backgroundColor = frameColor;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];
    labelTime.text = [NSString stringWithFormat:@"%@ - %@", [df stringFromDate:program.startDateTime], [df stringFromDate:program.endDateTime]];

    labelTime.hidden = NO;
    
    labelTime.textColor = ([program programIsOn] ? APST.brandTextColor : [UIColor blackColor]);
    
    labelTitle.numberOfLines = 0;
    labelTitle.text = program.name;
    labelTitle.frame = [Utils getLabelFrame:labelTitle maxHeight:100.0 minHeight:0];
    
    
    BOOL canStartOver = ([program isStartOverEnabled] && [program programIsOn] && [NU(UIUnit_live_startOver_buttons) isUIUnitExist]);
    BOOL canCatchup = ([program isCathupEnabled] && [program programFinished] && [NU(UIUnit_live_catchup_buttons) isUIUnitExist]);
    BOOL canRecord = ([self.item isRecordingEnabled] && [NU(UIUnit_live_record_buttons) isUIUnitExist]);
    
    self.firstActionButton.hidden = NO;
    self.secondActionButton.hidden = NO;
    self.constraintActionButtonsHeight.constant = 32.0f;
    self.constraintActionButton1Width.constant = 110.f;
    self.constraintActionButton2Width.constant = 110.f;
    self.constraintActionButtonsSeparatorWidth.constant = 7;
    
    CGFloat oldWidthFirstActionButton = self.firstActionButton.width;
    if (canStartOver) {
        self.firstActionButton.UIUnitID = NU(UIUnit_live_startOver_buttons);
        [self.firstActionButton activateAppropriateRule];
        [self.firstActionButton setTitle:LS(@"starover_btn_txt") forState:UIControlStateNormal];
        [self.firstActionButton setImage:[UIImage imageNamed:@"epg_icon_startover"] forState:UIControlStateNormal<<UIControlStateHighlighted];
        [self.firstActionButton sizeToFit];
    }
    else if (canCatchup) {
        self.firstActionButton.UIUnitID = NU(UIUnit_live_catchup_buttons);
        [self.firstActionButton activateAppropriateRule];
        [self.firstActionButton setTitle:LS(@"catchup_btn_txt") forState:UIControlStateNormal];
        [self.firstActionButton setImage:[UIImage imageNamed:@"epg_icon_catchup"] forState:UIControlStateNormal<<UIControlStateHighlighted];
        [self.firstActionButton sizeToFit];
    }
    else {
        self.firstActionButton.hidden = YES;
        self.constraintActionButton1Width.constant = 0;
        self.constraintActionButtonsSeparatorWidth.constant = 0;
    }
    if (oldWidthFirstActionButton != self.firstActionButton.width) {
        self.constraintActionButton1Width.constant = MAX(oldWidthFirstActionButton, self.firstActionButton.width);
        [self.firstActionButton setNeedsLayout];
    }
    
    if (canRecord) {
        self.secondActionButton.UIUnitID = NU(UIUnit_live_record_buttons);
        [self.secondActionButton activateAppropriateRule];
        [self.secondActionButton setTitle:LS(@"epg_buttonRecored_rec_text") forState:UIControlStateNormal];
        [self.secondActionButton setTitle:LS(@"epg_buttonRecoredc_cancel_text") forState:UIControlStateSelected];
        [self.secondActionButton setImage:[UIImage imageNamed:@"s&s_rec_icon_iphone"] forState:UIControlStateNormal];
        [self.secondActionButton setImage:[UIImage imageNamed:@"s&s_stop_rec_icon_iphone"] forState:UIControlStateSelected];
        [self.secondActionButton setTouchUpInsideAction:blockRecordButton];
        [self.secondActionButton setBackgroundImage:[UIImage imageNamed:@"details_button"] forState:UIControlStateNormal];
        [self.secondActionButton setBackgroundImage:[[[UIImage imageNamed:@"details_button"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
        
        CGFloat oldWidthSecondActionButton = self.secondActionButton.width;
        [self.secondActionButton sizeToFit];
        if (oldWidthFirstActionButton != self.firstActionButton.width) {
            self.constraintActionButton2Width.constant = MAX(oldWidthSecondActionButton, self.secondActionButton.width);
            [self.secondActionButton setNeedsLayout];
        }
    }
    else {
        self.secondActionButton.hidden = YES;
        self.constraintActionButton2Width.constant = 0;
    }
    
    if (self.firstActionButton.hidden && self.secondActionButton.hidden)
        self.constraintActionButtonsHeight.constant = 0;
    
    self.constraintActionDescriptionHeight.constant = 160;
    labelDescription.text = program.programDescription;
    [labelDescription sizeToFit];
    if (labelDescription.height+10 < self.constraintActionDescriptionHeight.constant)
        self.constraintActionDescriptionHeight.constant = labelDescription.height+10;
    
    labelSubDescription.text = [NSString stringWithFormat:@"%d %@", (int)([program.endDateTime timeIntervalSince1970] - [program.startDateTime timeIntervalSince1970]) / 60, LS(@"min")];
    
    if ([program.metaData objectForKey:@"Release year"] && [[program.metaData objectForKey:@"Release year"] length] > 0) {
        labelSubDescription.text = [NSString stringWithFormat:@"%@  |  %@", labelSubDescription.text, [program.metaData objectForKey:@"Release year"]];
    }
    /*”PSC : ONO : OS-91 Ono - CR - Rating needs ot be presented in each media page*/
    if ([program.metaData objectForKey:@"rating"] && [[program.metaData objectForKey:@"rating"] length] > 0) {
        labelSubDescription.text = [NSString stringWithFormat:@"%@  |  %@", labelSubDescription.text, LS([program.metaData objectForKey:@"rating"])];
    }
    [self layoutIfNeeded];
}

-(BOOL)isAllowRecording
{
    NSString * allowRecording = [[self.item metaData] objectForKey:@"allowRecording"];
    return (APST.supportRecordings && [allowRecording isEqualToString:@"yes"]);
}

- (void) createRecordButtonBlock {
    __block EPGChannelCollectionViewCell *weakSelf = self;
    blockRecordButton = ^(UIButton *btn) {
        
        BOOL isSeries = NO;
        NSDictionary *dic = weakSelf.item.metaData;
        NSString * seriesValue = [dic objectForKey:@"series ID"];
        NSLog(@"seriesName = %@",seriesValue);
        
        if ([seriesValue length]>0)
        {
            isSeries = YES;
        }
        
        if (btn.selected) // cancel recored
        {
            [weakSelf.delegate channelCollectionViewCell:weakSelf didSelectCancelRecoredProgram:weakSelf.item mediaItem:weakSelf.mediaItem recoredItem:weakSelf.recordItem isSeries:isSeries];
            weakSelf.secondActionButton.selected = NO;
            weakSelf.recoredIconImage.hidden =YES;
        }
        else // make recored
        {
            [weakSelf.delegate channelCollectionViewCell:weakSelf didSelectRecoredProgram:weakSelf.item mediaItem:weakSelf.mediaItem isSeries:isSeries];
        }
    };
}

- (IBAction)playProgram:(id)sender
{
    [self.delegate  channelCollectionViewCell:self didSelectPlayProgram:self.item  forChannel:self.mediaItem];
}

-(void)setRecoredHelper:(RecoredHelper *)recoredHelper
{
    self.secondActionButton.selected = NO;
    self.recoredIconImage.hidden =YES;
    _recoredHelper = recoredHelper;
    if (recoredHelper.recordingItemStatus == RecoredHelperRecoreded)
    {
        self.secondActionButton.selected = YES;
        [self showRecoredItem:recoredHelper.isSeries];
    }
    if (recoredHelper.recordingItemStatus == RecoredHelperCancelled)
    {
        self.secondActionButton.selected = NO;
    }
}

-(void)showRecoredItem:(BOOL)isSeries
{
    if (isSeries)
    {
        self.recoredIconImage.image = [UIImage imageNamed:@"record_series_icon"];
    }
    else
    {
        self.recoredIconImage.image = [UIImage imageNamed:@"record_singleItem_icon"];
    }
    self.recoredIconImage.hidden = NO;
    CGSize textSize = [[labelTitle text] sizeWithAttributes:@{NSFontAttributeName:[labelTitle font]}];
    CGFloat strikeWidth = textSize.width;
    
    CGRect frame = self.recoredIconImage.frame;
    frame.origin.x = labelTitle.frame.origin.x + strikeWidth +5;
    self.recoredIconImage.frame = frame;
}
@end
