//
//  EPGChannelViewControllerVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 1/29/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGChannelViewControllerVF.h"

@interface EPGChannelViewControllerVF ()

@end

@implementation EPGChannelViewControllerVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.viewContent.backgroundColor = [VFTypography instance].grayScale2Color;
    self.labelTilt.textColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
