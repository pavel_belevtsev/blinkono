//
//  EPGCollectionViewCellPhoneVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/14/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGCollectionViewCellPhoneVF.h"

@implementation EPGCollectionViewCellPhoneVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    self.backgroundColor = [VFTypography instance].grayScale2Color;
    self.imgFavBg.backgroundColor = [UIColor whiteColor];
    self.viewChannelBG.backgroundColor = [VFTypography instance].darkGrayColor;
}


-(void)addSwipeGestureRecognizer
{
    ASLogInfo(@"addSwipeGestureRecognizer");
}
@end
