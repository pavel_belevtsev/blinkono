//
//  EPGChannelCollectionViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TvinciSDK/TVNPVRRecordItem.h>
#import "RecoredHelper.h"
#import "TVMediaItem+PSTags.h"

@class EPGChannelCollectionViewCell;

@protocol EPGChannelCollectionViewCellDelegate <NSObject>

-(void) channelCollectionViewCell:(EPGChannelCollectionViewCell *) sender didSelectPlayProgram:(APSTVProgram *) program forChannel:(TVMediaItem *) channel;

-(void) channelCollectionViewCell:(EPGChannelCollectionViewCell *) sender didSelectRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem isSeries:(BOOL)isSeries;

-(void) channelCollectionViewCell:(EPGChannelCollectionViewCell *) sender didSelectCancelRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem recoredItem:(TVNPVRRecordItem *) recoredItem isSeries:(BOOL)isSeries;
@end


@interface EPGChannelCollectionViewCell : UICollectionViewCell {

    
    IBOutlet UILabel *labelTime;
    
    IBOutlet UILabel *labelTitle;
    IBOutlet UILabel *labelSubTitle;
    
    IBOutlet UILabel *labelDescription;
    IBOutlet UILabel *labelSubDescription;
    
    IBOutlet UIImageView *imgThumb;
    
    IBOutlet UIScrollView *scrollDescription;
    
    IBOutlet UIView *viewBar;
    IBOutlet UIView *viewBarTime;
    
}

@property (nonatomic, weak) IBOutlet UIButton *firstActionButton, *secondActionButton;
@property (nonatomic, weak) IBOutlet UIView *viewTopLine, *viewTimes;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *constraintActionButtonsHeight, *constraintActionButton1Width, *constraintActionButton2Width, *constraintActionButtonsSeparatorWidth, *constraintActionDescriptionHeight;

- (void)updateData:(APSTVProgram *)program;

@property (weak, nonatomic) id<EPGChannelCollectionViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic, strong) APSTVProgram *item;
@property (nonatomic, strong) TVMediaItem *mediaItem;

@property (weak, nonatomic) IBOutlet UIButton *buttonCatchup;
@property (weak, nonatomic) IBOutlet UIButton *buttonStartover;
@property (nonatomic, strong) UIButton * buttonRecored;
@property (nonatomic, strong) TVNPVRRecordItem * recordItem;
@property (strong, nonatomic) RecoredHelper * recoredHelper;
@property (weak, nonatomic) IBOutlet UIImageView *recoredIconImage;

@property CGRect orginalStartOverButtonFrame;
-(void)updateProgramImage;
-(BOOL)isAllowRecording;
@end
