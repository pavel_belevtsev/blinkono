//
//  EPGCollectionViewCellPhone.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 05.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPGCollectionViewCell.h"

@interface EPGCollectionViewCellPhone : EPGCollectionViewCell

@end
