//
//  EPGChannelCollectionViewCellVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 1/29/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGChannelCollectionViewCellVF.h"
#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"

@implementation EPGChannelCollectionViewCellVF

- (void)awakeFromNib {

    [super awakeFromNib];
    
    //2 (item number as described in Vodafone rebranding 2.0 app.pdf page 15)
    self.viewContainer.backgroundColor = [VFTypography instance].grayScale1Color;
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)updateData:(APSTVProgram *)program {
    [super updateData:program];

    //3
    labelTitle.textColor = [UIColor whiteColor];
    //4
    labelSubTitle.textColor = [UIColor whiteColor];
    //5
    labelDescription.textColor = [VFTypography instance].grayScale6Color;
    //6
    labelSubDescription.textColor = [VFTypography instance].grayScale5Color;
    
    [self.self.firstActionButton setBackgroundImage:[[[UIImage imageNamed:@"details_button"] imageTintedWithColor:[VFTypography instance].grayScale3Color] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];

    labelTime.textColor = ([program programIsOn] ? APST.brandTextColor : [UIColor whiteColor]);
    
    BOOL canRecord = ([self isAllowRecording] && [LoginM isSignedIn]);
    if (canRecord)
    {
        [self.secondActionButton setBackgroundImage:[[[UIImage imageNamed:@"details_button"] imageTintedWithColor:[VFTypography instance].grayScale3Color] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
        self.secondActionButton.titleLabel.textColor = [UIColor whiteColor];
    }
}

/* "PSC : Vodafone Grup : change Picture size to best size : JIRA ticket. */
-(void)updateProgramImage
{
    NSURL *pictureURL = [self.item pictureURLForSize:CGSizeMake(568, 320)];
    if (!pictureURL)
    {
        pictureURL = [Utils getBestPictureURLForMediaItem:self.mediaItem andSize:CGSizeMake(465.0, 262.0)];
    }
    
    if (pictureURL)
    {
        UIImage *imgDefault = [UIImage imageNamed:@"placeholder_2_3.png"];
        
        [imgThumb sd_setImageWithURL:pictureURL placeholderImage:imgDefault];
    }
    else
    {
        imgThumb.image = [UIImage imageNamed:@"placeholder_2_3.png"];
    }
    
}
@end
