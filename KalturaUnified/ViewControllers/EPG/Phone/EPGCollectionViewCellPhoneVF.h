//
//  EPGCollectionViewCellPhoneVF.h
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/14/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGCollectionViewCellPhone.h"

@interface EPGCollectionViewCellPhoneVF : EPGCollectionViewCellPhone
@property (weak, nonatomic) IBOutlet UIView *viewChannelBG;

@end
