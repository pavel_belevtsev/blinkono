//
//  EPGChannelCollectionViewCellVF.h
//  Vodafone
//
//  Created by Aviv Alluf on 1/29/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGChannelCollectionViewCell.h"

@interface EPGChannelCollectionViewCellVF : EPGChannelCollectionViewCell

@end
