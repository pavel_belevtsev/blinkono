//
//  EPGChannelViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 19.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGChannelViewController.h"
#import "EPGChannelCollectionViewCell.h"

@interface EPGChannelViewController ()<EPGChannelCollectionViewCellDelegate,RecoredAdapterDelegate,EpgRecoredPopUpDelegate,CancelSeriesRecordingPopUpDelegate>

@end

@implementation EPGChannelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Utils setLabelFont:_labelTitle bold:YES];
    [Utils setLabelFont:_labelTilt];
    
    _labelTilt.text = [LS(@"channel_tilt_device_to_watch") uppercaseString];
    
    [_collectionViewChannel registerNib:[UINib nibWithNibName:@"EPGChannelCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"EPGChannelCollectionViewCell"];
    
    _collectionViewChannel.panGestureRecognizer.enabled = NO;
    [_collectionViewChannel addGestureRecognizer:_scrollViewChannel.panGestureRecognizer];
    
    _scrollViewChannel.contentSize = _scrollViewChannel.bounds.size;
   
    
    [_buttonWatch setTitle:[LS(@"watch") uppercaseString] forState:UIControlStateNormal];
    
    [self skinChanged];
    
    [self allocRecoredrItems];
}

- (void)skinChanged {
    
    [self initBrandButtonUI:_buttonWatch];

}

- (void)viewWillAppear:(BOOL)animated {

    if (!isInitialized) {
        
        isInitialized = YES;
        
        _labelTitle.text = _channel.name;
        
        _scrollViewChannel.contentSize = CGSizeMake(_scrollViewChannel.bounds.size.width * [_epgList count], _scrollViewChannel.bounds.size.height);
        
        
        _scrollViewChannel.contentOffset = CGPointMake(_scrollViewChannel.bounds.size.width * _currentProgramIndex, 0);
        CGPoint contentOffset = CGPointMake(_scrollViewChannel.contentOffset.x - 20.0, 0);
        _collectionViewChannel.contentOffset = contentOffset;
        
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)  name:UIDeviceOrientationDidChangeNotification  object:nil];

}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [self removeOrientationObserver];
    [self clearRequest];
}

- (void)removeOrientationObserver {

    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];

}

- (void)orientationChanged:(NSNotification *)notification {
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if (orientation == UIInterfaceOrientationLandscapeRight) {
        [self buttonWatchPressed:_buttonWatch];
    }
    
}

#pragma mark hiddenScrollView delegate

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == _scrollViewChannel) {
        
        CGPoint contentOffset = CGPointMake(scrollView.contentOffset.x - 20.0, 0);
        _collectionViewChannel.contentOffset = contentOffset;
        
    }
}


#pragma mark - UICollectionView

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.epgList count];
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(_scrollViewChannel.frame.size.width, _scrollViewChannel.frame.size.height);
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    EPGChannelCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:@"EPGChannelCollectionViewCell" forIndexPath:indexPath];
    
    cellView.delegate = self;
    cellView.mediaItem = _channel;
    [cellView updateData:[_epgList objectAtIndex:indexPath.row]];
    
    RecoredHelper * recHelp = [self.recoredAdapter recoredHelperToepgProgram:[_epgList objectAtIndex:indexPath.row] mediaItem:self.channel];
    
    cellView.recoredHelper = recHelp;

    return cellView;
    
}

#pragma mark -

- (IBAction)buttonBackPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)buttonWatchPressed:(UIButton *)button {

    [self openMediaItem:_channel];
    
    [self removeOrientationObserver];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - EPGChannelCollectionViewCellDelegate methods 

-(void) channelCollectionViewCell:(EPGChannelCollectionViewCell *) sender didSelectPlayProgram:(APSTVProgram *) program forChannel:(TVMediaItem *) channel
{
    [self openMediaItem:channel withProgram:program];
}

-(void) channelCollectionViewCell:(EPGChannelCollectionViewCell *) sender didSelectRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem isSeries:(BOOL)isSeries
{
    RecoredHelper * recHelp = [self.recoredAdapter takeOrMakeRecoredHelperWithMediaItem:mediaItem andProgram:program isSeries:isSeries];
    
    if (isSeries)
    {
        self.epgRecoredPopUp = [[CustomViewFactory defaultFactory]epgRecoredPopUp];// [[EpgRecoredPopUp alloc] init];
       [self.epgRecoredPopUp buildWithOnLinkageView:self.view withRecoredHelper:recHelp toolTipArrowType:ToolTipTypeArrowNone toolCloseType:ToolTipCloseButtonOnly];
        self.epgRecoredPopUp.delegate = self;
        [self.view addSubview:self.epgRecoredPopUp];
    }
    else
    {
        [self recordAsset:program mediaItem:mediaItem];
    }

}

-(void) channelCollectionViewCell:(EPGChannelCollectionViewCell *) sender didSelectCancelRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem recoredItem:(TVNPVRRecordItem *) recoredItem isSeries:(BOOL)isSeries
{
    if (isSeries)
    {
        RecoredHelper * rec = [self.recoredAdapter recoredHelperToepgProgram:program mediaItem:mediaItem];
        self.cancelSeriesRecordingPopUp = [[CustomViewFactory defaultFactory] cancelSeriesRecordingPopUpWithRecoredHelper:rec andLinkageView:self.view];
        //[[CancelSeriesRecordingPopUp alloc] initWithNibWithRecoredHelper:rec andLinkageView:self.view];
        self.cancelSeriesRecordingPopUp.delegate = self;
        [self.view addSubview:self.cancelSeriesRecordingPopUp];
    }
    else
    {
        [self.recoredAdapter deleteOneRecored:recoredItem Program: program mediaItem: mediaItem];
    }

}

- (void)clearRequest
{
    TVPAPIRequest * req = [self.dictionaryNpvrRequest objectForKey:@"NpvrRequest"];
    if (req)
    {
        [req cancel];
        [self.dictionaryNpvrRequest removeObjectForKey:@"NpvrRequest"];
    }
}


-(void)setEpgList:(NSArray *)epgList
{
    _epgList = epgList;
    [self allocRecoredrItems];
    if ([epgList count]>0)
    {
        [self.recoredAdapter cleanAllReoredsDataFromMemory];
        TVPAPIRequest * req =  [self.recoredAdapter loadRecoredByChannel:_channel andPrograms:_epgList scrollToProgram:nil];
        if (req)
        {
            [self.dictionaryNpvrRequest setObject:req forKey:@"NpvrRequest"];
        }
 
    }
}

-(NSArray*) currentProgramsToChannel:(TVMediaItem*)channel {
    return self.epgList;
}

-(void)allocRecoredrItems
{
    if (self.recoredAdapter == nil)
    {
        self.recoredAdapter = [[RecoredAdapter alloc] init];
        self.recoredAdapter.delegate = self;
    }
    
    if (self.dictionaryNpvrRequest == nil)
    {
        self.dictionaryNpvrRequest = [[NSMutableDictionary alloc] init];
    }
}

-(void)dealloc
{
    [self clearRequest];
}

#pragma mark - recoredAdapter delegate -

-(void)recoredAdapter:(RecoredAdapter *)epgRecoredPopUp complateRecordAssetOrSeriesToToChannel:(TVMediaItem*)mediaItem Program:(APSTVProgram *) epgProgram recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus
{
     [self.collectionViewChannel reloadData];
    RecoredHelper * rec = [self.recoredAdapter recoredHelperToepgProgram:epgProgram mediaItem:mediaItem];
    if (self.recoredStatusPopupView ==  nil)
    {
        self.recoredStatusPopupView = [[CustomViewFactory defaultFactory] epgRcoredStatusView];
    }
   // [[EPGRcoredStatusView alloc] initWithNib];
    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewRcoredingSecsses ViewOnView:self.recordInfoPlaceholder isSeries:rec.isSeries];
    }
    else
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewRcoredingFailed ViewOnView:self.recordInfoPlaceholder isSeries:rec.isSeries];
    }
    [self scrollToProgram:epgProgram animated:NO];

}

-(void) recoredAdapter:(RecoredAdapter *)recoredAdapter  complateCancelOrDeleateAssetRecording:(TVNPVRRecordItem *) recordItem  Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus;
{
    [self scrollToProgram:epgProgram animated:NO];
    
    RecoredHelper * rec = [self.recoredAdapter recoredHelperToepgProgram:epgProgram mediaItem:mediaItem];
    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewDeleteRecoredSecsses ViewOnView:self.recordInfoPlaceholder isSeries:rec.isSeries];
    }
    else
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewDeleteRecoredFailed ViewOnView:self.recordInfoPlaceholder isSeries:rec.isSeries];
    }
}

-(void)recoredAdapter:(RecoredAdapter *)recoredAdapter complateLoadRecoredItemsToChannel:(TVMediaItem*)mediaItem isChannelHeveRecoredItems:(BOOL)isChannelHeveRecoredItems  scrollToProgram:(APSTVProgram *)scrolledProgram
{
    [UIView performWithoutAnimation:^{

        [self.collectionViewChannel reloadData];
        if (scrolledProgram)
        {
            [self scrollToProgram:scrolledProgram animated:NO];
        }
    }];
}

-(void) recoredAdapter:(RecoredAdapter *)recoredAdapter  complateCancelOrDeleateSeriesRecording:(RecoredHelper*)recoredHelper recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus
{
    RecoredHelper * rec = [self.recoredAdapter recoredHelperToepgProgram:recoredHelper.program mediaItem:recoredHelper.mediaItem];
    
    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
        [self.recoredStatusPopupView addEPGRcored:recoredHelper.program status:EPGRcoredStatusViewDeleteRecoredSecsses ViewOnView:self.recordInfoPlaceholder isSeries:rec.isSeries];
    }
    else
    {
        [self.recoredStatusPopupView addEPGRcored:recoredHelper.program status:EPGRcoredStatusViewDeleteRecoredFailed ViewOnView:self.recordInfoPlaceholder isSeries:rec.isSeries];
    }
}

-(void) scrollToProgram:(APSTVProgram *) scrolledProgram animated:(BOOL) animated
{
    int index = -1;
    for (int i = 0; i < [_epgList count]; i++) {
        APSTVProgram *program = [_epgList objectAtIndex:i];
        if ([program.epgId isEqualToString:scrolledProgram.epgId])
        {
            index = i;
        }
    }
    if (index >= 0) {
        
        [self.collectionViewChannel performBatchUpdates:^{
            
            [self.collectionViewChannel reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:index inSection:0]]];
            
        }
                                             completion:^(BOOL finished)
        {
            CGPoint contentOffset = CGPointMake(_scrollViewChannel.contentOffset.x - 20.0, 0);
            _collectionViewChannel.contentOffset = contentOffset;
        }];
        
//        [_collectionViewChannel reloadData];
//        CGPoint contentOffset = CGPointMake(_collectionViewChannel.contentOffset.x - 20.0, 0);
//        _collectionViewChannel.contentOffset = contentOffset;
//        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:index inSection:0];
//        [self.collectionViewChannel scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
    }
    
}
#pragma mark - EpgRecoredPopUp Delegate -

-(void) epgRecoredPopUp:(EpgRecoredPopUp *)epgRecoredPopUp recredType:(RecoredType)recoredType recoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem andLinkageView:(UIView *)linkageView;
{
    if (recoredType == RecoredTypeEpisode)
    {
        [self recordAsset:program mediaItem: mediaItem];
    }
    else
    {
        [self recordSeriesByProgramId:program mediaItem:mediaItem];
    }
}

#pragma mark - cancelSeriesRecordingPopUp Delegate -

-(void) cancelSeriesRecordingPopUp:(CancelSeriesRecordingPopUp *)epgRecoredPopUp cnacelRecoredType:(CnacelRecoredType)cnacelRecoredType WithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView*)linkageView
{
    [self.recoredAdapter cancelOrDeleteSeriesRecordingWithCnacelRecoredType:cnacelRecoredType WithRecoredHelper:recoredHelper];
}

-(void) cancelSeriesRecordingPopUp:(CancelSeriesRecordingPopUp *)epgRecoredPopUp didCancelViewWithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView*)linkageView
{
    [self scrollToProgram:recoredHelper.program animated:NO];
    
//    RecoredHelper * rec = [self.recoredAdapter recoredHelperToepgProgram:recoredHelper.program mediaItem:recoredHelper.mediaItem];
}

#pragma mark - recoredAdapter Calls -

- (void)recordSeriesByProgramId:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    [self.recoredAdapter recordSeriesByProgram:epgProgram mediaItem:mediaItem];
}

- (void)recordAsset:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem
{
    [self.recoredAdapter recordAsset:epgProgram mediaItem:mediaItem];
}

@end
