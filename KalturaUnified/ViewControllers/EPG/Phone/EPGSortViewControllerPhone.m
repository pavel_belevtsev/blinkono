//
//  EPGSortViewControllerPhone.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 08.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSortViewControllerPhone.h"
#import "CategorySortTableViewCell.h"
#import "EPGSortView.h"

@interface EPGSortViewControllerPhone ()

@end

@implementation EPGSortViewControllerPhone

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewContent.layer.cornerRadius = 6;
    self.viewContent.layer.masksToBounds = YES;
    
    //self.sortView = [[EPGSortView alloc] initWithNib];
    [_viewContent addSubview:_sortView];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.sortView.frame = [_viewContent bounds];
    [self.sortView updateSizes];
    self.sortView.delegate = self.delegate;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
