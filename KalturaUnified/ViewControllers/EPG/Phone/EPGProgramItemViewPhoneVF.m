//
//  EPGProgramItemViewPhoneVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/14/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EPGProgramItemViewPhoneVF.h"

@implementation EPGProgramItemViewPhoneVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void) awakeFromNib{
    [super awakeFromNib];
    
    self.viewContainer.backgroundColor = [VFTypography instance].grayScale1Color;
}

- (void)updateProgramData:(APSTVProgram *)program {
    [super updateProgramData:program];

//    labelTime.textColor = ([program programIsOn] ? APST.brandTextColor : [UIColor blackColor]);
    labelTime.textColor = [UIColor whiteColor];
    labelChannel.textColor = [UIColor whiteColor];
    labelProgram.textColor = [UIColor whiteColor];
}

@end
