//
//  EPGChannelViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 19.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseViewController.h"
#import "RecoredHelper.h"
#import "CancelSeriesRecordingPopUp.h"
#import "EPGRcoredStatusView.h"
#import "EpgRecoredPopUp.h"

@interface EPGChannelViewController : BaseViewController {
    
    BOOL isInitialized;
    
    
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTilt;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewChannel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewChannel;
@property (weak, nonatomic) IBOutlet UIButton *buttonWatch;

@property (nonatomic, strong) TVMediaItem *channel;
@property (nonatomic, strong) NSArray *epgList;
@property (nonatomic) NSInteger currentProgramIndex;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewTiltIcon;

@property (weak, nonatomic) IBOutlet UIView *recordInfoPlaceholder;
@property (strong, nonatomic) EPGRcoredStatusView * recoredStatusPopupView;
@property (strong, nonatomic) EpgRecoredPopUp * epgRecoredPopUp;
@property (nonatomic, strong) RecoredAdapter * recoredAdapter;
@property (strong, nonatomic) CancelSeriesRecordingPopUp* cancelSeriesRecordingPopUp;
@property (nonatomic, strong) NSMutableDictionary *dictionaryNpvrRequest;

@end
