//
//  EPGSelectTimeViewPhone.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSelectTimeViewPhone.h"
#import "UIImage+Tint.h"

@implementation EPGSelectTimeViewPhone

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    int scrollTimeOffset = _scrollViewSelectTime.frame.size.width / 2;
    int scrollTimeWidth = _viewSelectedTime.frame.size.width;
    
    
    _scrollViewSelectTime.contentSize = CGSizeMake(scrollTimeWidth * 24 + scrollTimeOffset * 2 - (scrollTimeWidth / 60), _scrollViewSelectTime.frame.size.height);
    
    for (int i = 0; i < 25; i++) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(scrollTimeWidth * i + scrollTimeOffset - scrollTimeWidth / 2, 0, scrollTimeWidth, _scrollViewSelectTime.frame.size.height)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:FONT_BOLD size:13.0];
        label.textAlignment = NSTextAlignmentCenter;
        
        label.text = [NSString stringWithFormat:@"%d:00", i];
        
        [_scrollViewSelectTime addSubview:label];
        
    }
    
    UIView *viewTap = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _scrollViewSelectTime.contentSize.width, _scrollViewSelectTime.contentSize.height)];
    viewTap.backgroundColor = [UIColor clearColor];
    [_scrollViewSelectTime addSubview:viewTap];
    
    UITapGestureRecognizer *tapTimeRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTimeRecognize:)];
    [viewTap addGestureRecognizer:tapTimeRecognizer];
    
    viewTap.tag = 100;

    
    [Utils setLabelFont:_labelSelectTime bold:YES];
    [Utils setLabelFont:_labelSelectTimeBubble bold:YES];
    
    
    [self skinChanged];

}

- (void)skinChanged {
    
    _imgBubbleTime.image = [[UIImage imageNamed:@"epg_buble"] imageTintedWithColor:APST.brandColor];

    for (UILabel *label in [_scrollViewSelectTime subviews]) {
        if ([label isKindOfClass:[UILabel class]]) {
            label.textColor = APST.brandTextColor;
        }
    }
    
    _labelSelectTime.textColor = APST.brandTextColor;
    _labelSelectTimeBubble.textColor = APST.brandTextColor;
    
    self.backgroundColor = APST.brandColor;
    
    ((UIView *)[[self subviews] objectAtIndex:0]).backgroundColor = APST.brandColor;
    
    _viewSelectedTime.backgroundColor = APST.brandColor;
    
    
}

- (void)tapTimeRecognize:(UITapGestureRecognizer *)recognizer {
    
    UIView *viewTap = [_scrollViewSelectTime viewWithTag:100];
    CGPoint point = [recognizer locationInView:viewTap];
    
    float x = point.x - (_scrollViewSelectTime.frame.size.width / 2);
    
    if (x < 0.0) x = 0;
    
    if (x > _scrollViewSelectTime.contentSize.width - _scrollViewSelectTime.frame.size.width) {
        x = _scrollViewSelectTime.contentSize.width - _scrollViewSelectTime.frame.size.width;
    }
    
    [_scrollViewSelectTime setContentOffset:CGPointMake(x, 0) animated:YES];
    
    [self performSelector:@selector(updateTimeFromScroll) withObject:nil afterDelay:1.0];
}

- (void)updateTimeFromScroll {
    
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 1.0;
        super.viewSelectedTimeBubble.alpha = 0.0;
    } completion:^(BOOL finished){
        
        [super.delegate hideBarCompleteProcess];
        
    }];
    
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    int offset = _scrollViewSelectTime.contentOffset.x;
    int scrollTimeWidth = _viewSelectedTime.frame.size.width;
    
    NSDateComponents *components = [gregorian components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate dateWithTimeIntervalSince1970:super.currentInterval]];
    
    components.hour = offset / scrollTimeWidth;
    components.minute = (offset % scrollTimeWidth) * 60 / scrollTimeWidth;
    
    NSDate *newDate = [gregorian dateFromComponents:components];
    
    super.currentInterval = [newDate timeIntervalSince1970];
    
    [super.delegate updateTimeLabel];
    
}

- (void)updateSelectedTime:(NSTimeInterval)currentInterval {
    
    self.currentInterval = currentInterval;
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [gregorian components:NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:[NSDate dateWithTimeIntervalSince1970:currentInterval]];
    
    NSInteger h = components.hour;
    NSInteger min = components.minute;
    
    _labelSelectTime.text = [NSString stringWithFormat:@"%ld:%.2ld", (long)h, (long)min];
    _labelSelectTimeBubble.text = _labelSelectTime.text;
    
    int scrollTimeWidth = _viewSelectedTime.frame.size.width;
    
    _scrollViewSelectTime.contentOffset = CGPointMake(scrollTimeWidth * h + scrollTimeWidth * min / 60, 0);
    
    _viewSelectedTime.alpha = 1.0;
    super.viewSelectedTimeBubble.alpha = 0.0;
    
}

- (void)stopScrollView {
    
    CGPoint offset = _scrollViewSelectTime.contentOffset;
    [_scrollViewSelectTime setContentOffset:offset animated:NO];
    
}

- (NSString *)timeBubbleTitle {
    
    return _labelSelectTimeBubble.text;
    
}

#pragma mark - UIScrollView

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    
    [self updateTimeFromScroll];
    
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    
    [self updateTimeFromScroll];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [super.delegate updateTimeShowBar];
    
    int offset = _scrollViewSelectTime.contentOffset.x;
    int scrollTimeWidth = _viewSelectedTime.frame.size.width;
    
    int h = offset / scrollTimeWidth;
    int min = (offset % scrollTimeWidth) * 60 / scrollTimeWidth;
    
    _labelSelectTime.text = [NSString stringWithFormat:@"%d:%.2d", h, min];
    _labelSelectTimeBubble.text = _labelSelectTime.text;
    
    if (_viewSelectedTime.alpha == 1.0) {
        
        [UIView animateWithDuration:0.2 animations:^{
            _viewSelectedTime.alpha = 0.0;
            super.viewSelectedTimeBubble.alpha = 1.0;
        }];
        
    }
 
}

@end
