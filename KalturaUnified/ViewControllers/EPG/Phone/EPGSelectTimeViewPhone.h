//
//  EPGSelectTimeViewPhone.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGSelectTimeView.h"

@interface EPGSelectTimeViewPhone : EPGSelectTimeView

@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewSelectTime;
@property (nonatomic, weak) IBOutlet UIView *viewSelectedTime;
@property (nonatomic, weak) IBOutlet UILabel *labelSelectTime;
@property (nonatomic, weak) IBOutlet UILabel *labelSelectTimeBubble;
@property (nonatomic, weak) IBOutlet UIImageView *imgBubbleTime;
@property (weak, nonatomic) IBOutlet UIView *viewTopEdge;

@end
