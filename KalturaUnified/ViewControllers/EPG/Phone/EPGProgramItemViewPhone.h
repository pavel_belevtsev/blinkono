//
//  EPGProgramItemViewPhone.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.09.13.
//  Copyright (c) 2013 Quickode Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EPGProgramItemView.h"

@interface EPGProgramItemViewPhone : EPGProgramItemView {
    
    IBOutlet UIView *viewTime;
    IBOutlet UIView *viewProgress;
    
    IBOutlet UILabel *labelTime;
    IBOutlet UILabel *labelChannel;
    IBOutlet UILabel *labelProgram;
    
}
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

- (void)setChannelData:(TVMediaItem *)data;
- (void)updateProgramData:(APSTVProgram *)program;

@end
