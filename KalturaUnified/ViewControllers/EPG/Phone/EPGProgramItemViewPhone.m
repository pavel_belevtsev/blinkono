//
//  EPGProgramItemViewPhone.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.09.13.
//  Copyright (c) 2013 Quickode Ltd. All rights reserved.
//

#import "EPGProgramItemViewPhone.h"

@implementation EPGProgramItemViewPhone

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (void)awakeFromNib {

    [super awakeFromNib];
    
    labelProgram.text = @"";
    labelTime.text = @"";
    
    [Utils setLabelFont:labelChannel bold:YES];
    [Utils setLabelFont:labelProgram];
    [Utils setLabelFont:labelTime bold:YES];
    
    
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];

}

- (void)skinChanged {
    
    viewProgress.backgroundColor = APST.brandColor;
    
    if (super.item) {
        [self updateProgramData:super.item];
    }
}

- (void)setChannelData:(TVMediaItem *)data {
    
    [super setChannelData:data];
    labelChannel.text = data.name;
    
}

- (void)updateProgramData:(APSTVProgram *)program {
    
    labelProgram.text = @"";
    labelTime.text = @"";
    
    super.item = program;
    
    viewTime.backgroundColor = [program programIsOn] ? APST.brandColor : CS(@"a2a2a2");
    viewProgress.hidden = ![program programIsOn];
    
    if ([program programIsOn]) {
    
        int timeFull = ([program.endDateTime timeIntervalSince1970] - [program.startDateTime timeIntervalSince1970]);
        int timeCurrent = ([[NSDate date] timeIntervalSince1970] - [program.startDateTime timeIntervalSince1970]);
        
        float fullWidth = [viewProgress superview].frame.size.width;
        
        viewProgress.frame = CGRectMake(viewProgress.frame.origin.x, viewProgress.frame.origin.y, fullWidth * timeCurrent / timeFull, viewProgress.frame.size.height);
        
    } else if ([program.endDateTime timeIntervalSince1970] < [[NSDate date] timeIntervalSince1970]) {
        
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];
    labelTime.text = [NSString stringWithFormat:@"%@ - %@", [df stringFromDate:program.startDateTime], [df stringFromDate:program.endDateTime]];
    
    labelTime.textColor = ([program programIsOn] ? APST.brandTextColor : [UIColor blackColor]);
    
    labelProgram.text = program.name;
    labelProgram.frame = [Utils getLabelFrame:labelProgram maxHeight:40.0 minHeight:0];

}

@end
