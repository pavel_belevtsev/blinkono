//
//  EPGSortViewControllerPhone.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 08.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

@class EPGSortView;

@interface EPGSortViewControllerPhone : UIViewController {
    
}

@property (strong, nonatomic) EPGSortView *sortView;

@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (nonatomic, weak) id delegate;

@end
