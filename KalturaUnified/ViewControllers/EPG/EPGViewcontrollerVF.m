//
//  EPGViewcontroller_Vodafone.m
//  KalturaUnified
//
//  Created by Rivka Peleg on 12/29/14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EPGViewcontrollerVF.h"
#import <TvinciSDK/APSBlockOperation.h>

#import "UIImage+Tint.h"
#import "EPGSelectTimeViewPad.h"
#import "EPGSelectTimeViewPhone.h"
#import "EPGSelectDateViewPhone.h"

@interface EPGViewcontrollerVF ()

@property (assign, nonatomic) NSInteger swipeBalanceOffset;
@property (retain, nonatomic) NSMutableDictionary * minimumOffsetForChannel;
@property (retain, nonatomic) NSMutableDictionary * maximumOffsetForChannel;
@property (assign, nonatomic) BOOL buttonsPhoneBarCreated;

@end

@implementation EPGViewcontrollerVF

/* "PSC : Vodafone Grup : change to VF EPG : JIRA ticket. */

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft)];
    recognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.collectionViewEPG addGestureRecognizer:recognizer];
    
    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    recognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.collectionViewEPG addGestureRecognizer:recognizer];
    
    
    self.minimumOffsetForChannel = [NSMutableDictionary dictionary];
    self.maximumOffsetForChannel = [NSMutableDictionary dictionary];
    

    UIImage *imgChannelSortBtn = [UIImage imageNamed:@"epg_segmentself.button"];
    [self.buttonSegmentChannels setBackgroundImage:[imgChannelSortBtn imageTintedWithColor:[VFTypography instance].grayScale2Color] forState:UIControlStateNormal];
    
    [self.buttonSegmentChannels setTitleColor:[VFTypography instance].grayScale2Color forState:UIControlStateNormal];
    [self.buttonSegmentChannels setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.buttonSegmentChannels setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    self.viewContent.backgroundColor = [VFTypography instance].grayScale2Color;

    self.collectionViewEPG.backgroundColor = [UIColor clearColor];
    
    self.viewContent.backgroundColor = [VFTypography instance].darkGrayColor;
    self.view.backgroundColor = [UIColor blackColor];
    
    [self.leftSideBGView setBackgroundColor:[VFTypography instance].darkGrayColor];
    self.leftSideBGView.hidden = NO;
    //TODO: aviv - the "epg_sidebar_nowself.button" image is used as bg image for buttonNow - is currently
    // gray but should be brand color - should get new one from product
        self.buttonsPhoneBarCreated = NO;
}


- (void) rebrandDateView
{
    if(isPhone){
        EPGSelectDateViewPhone *selectDateView = (EPGSelectDateViewPhone*)self.selectDateView;
//        selectDateView.scrollViewSelectDay.backgroundColor = [VFTypography instance].grayScale2Color;
        selectDateView.backgroundColor = [VFTypography instance].grayScale4Color;
        //TODO: aviv - hack - should use viewTopEdge instead!!!.
        UIView* targerView =   ((UIView *)[[selectDateView subviews] objectAtIndex:0]);
        targerView.backgroundColor = [VFTypography instance].grayScale4Color;
    }
}

- (void) rebrandTimeView
{
    if(isPhone){
        EPGSelectTimeViewPhone* timeView = (EPGSelectTimeViewPhone*)self.selectTimeView;
        timeView.backgroundColor = [VFTypography instance].grayScale4Color;
        //TODO: aviv - hack - should expose an outlet for the bg view.
        ((UIView *)[[timeView subviews] objectAtIndex:0]).backgroundColor = [VFTypography instance].grayScale4Color;
        timeView.viewSelectedTime.backgroundColor = [VFTypography instance].grayScale2Color;
    }
}

-(void) updateDateLabel
{
    [super updateDateLabel];
    [self resetSwipeOffset];
}

-(void) updateTimeLabel
{
    [super updateTimeLabel];
    [self resetSwipeOffset];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self resetSwipeOffset];
    [self.collectionViewEPG reloadData];
    
    if (isPhone)
    {
        [self rebrandTimeView];
        [self rebrandDateView];
        
        UIImage *imgDate = [[UIImage imageNamed:@"epg_icon_date"] imageTintedWithColor:[UIColor whiteColor]];
        [self.buttonDate setImage:imgDate forState:UIControlStateHighlighted];
        [self.buttonDate setImage:imgDate forState:UIControlStateSelected];
        
        UIImage *imgTime = [[UIImage imageNamed:@"epg_icon_time"] imageTintedWithColor:[UIColor whiteColor]];
        [self.buttonTime setImage:imgTime forState:UIControlStateHighlighted];
        [self.buttonTime setImage:imgTime forState:UIControlStateSelected];
        
        
        [self.buttonNow setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [self.buttonNow setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self.buttonDate setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [self.buttonDate setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self.buttonTime setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [self.buttonTime setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

    }
    
    if(isPad)
    {
        [self.buttonNow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    

//    if (isPad) {
//        // set the "select time view" button color:
//        EPGSelectTimeViewPad* epgSelectTimeView = (EPGSelectTimeViewPad*) self.selectTimeView;
//        TVtimeSlider *timeSlider = epgSelectTimeView.timeSlider;
//    }
}

/* "PSC : Vodafone Group : SYN-4030 : VFGTVONES-200 - iPhone/EPG/ Bad behaviour when going back to the EPG from the information screen of a future event. */
- (void)initButtonsPhoneBar {
    if (!self.buttonsPhoneBarCreated) {
        [super initButtonsPhoneBar];
        self.buttonsPhoneBarCreated = YES;
    }
}



- (void)searchProcessComplete:(NSArray *)channelItems
{
    [super searchProcessComplete:channelItems];
    [self resetSwipeOffset];
}


-(void) resetSwipeOffset
{
    //[_selectTimeView updateForSearchMode:NO];
    self.swipeBalanceOffset =0;
    [self.maximumOffsetForChannel removeAllObjects];
    [self.minimumOffsetForChannel removeAllObjects];
}

-(void)makeNowButtonGrey
{
    if(isPad)
    {
        self.buttonNow.selected = NO;
    }
}

-(void) swipeRight
{
    NSInteger possibleMinimumOffset = [self possibleMinimumOffset];
    self.swipeBalanceOffset = MAX(self.swipeBalanceOffset, possibleMinimumOffset);
    NSInteger possibleMaximumOffset = [self possibleMaximumOffset];
    self.swipeBalanceOffset = MIN(self.swipeBalanceOffset, possibleMaximumOffset);
    
    if (self.swipeBalanceOffset != possibleMinimumOffset)
    {
        // [_selectTimeView updateForSearchMode:YES];
        
        self.swipeBalanceOffset--;
        
        for (EPGCollectionViewCell * cell in [self.collectionViewEPG visibleCells])
        {
            [self updateCellToCurrentOffset:cell];
        }
        [self makeNowButtonGrey];
    }
}

-(void) swipeLeft
{
    NSInteger possibleMinimumOffset = [self possibleMinimumOffset];
    self.swipeBalanceOffset = MAX(self.swipeBalanceOffset, possibleMinimumOffset);
    NSInteger possibleMaximumOffset = [self possibleMaximumOffset];
    self.swipeBalanceOffset = MIN(self.swipeBalanceOffset, possibleMaximumOffset);
    
    if (self.swipeBalanceOffset != possibleMaximumOffset)
    {
        
        // [_selectTimeView updateForSearchMode:YES];
        
        self.swipeBalanceOffset++;
        
        for (EPGCollectionViewCell * cell in [self.collectionViewEPG visibleCells])
        {
            [self updateCellToCurrentOffset:cell];
        }
        [self makeNowButtonGrey];
    }
}

-(void) updateCellToCurrentOffset:(EPGCollectionViewCell *) cell
{
    NSArray * array = cell.epgList;
    
    APSTVProgram *program = nil;
    if (array && [array count])
    {
        if (searchMode)
        {
            program = array[0];
        }
        else
        {
            program = [self programInEPGDate:array];
        }
        
    }
    
    NSInteger programIndex = [array indexOfObject:program];
    
    NSInteger indexWithOffset = programIndex+self.swipeBalanceOffset;
    indexWithOffset = MAX(0, indexWithOffset);
    indexWithOffset = MIN(indexWithOffset, array.count-1);
    cell.program = array[indexWithOffset];
    [cell updateProgramItem:(int)indexWithOffset animated:YES];
    
    
    
}



-(NSInteger ) possibleMinimumOffset
{
    NSInteger minimum = NSIntegerMax;
    for (NSNumber * number in self.minimumOffsetForChannel.allValues)
    {
        if (number.integerValue<minimum)
        {
            minimum = number.integerValue;
        }
    }
    
    
    return minimum;
}

-(NSInteger ) possibleMaximumOffset
{
    NSInteger maximum = NSIntegerMin;
    for (NSNumber * number in self.maximumOffsetForChannel.allValues)
    {
        if (number.integerValue>maximum)
        {
            maximum = number.integerValue;
        }
    }
    
    
    return maximum;
    
}

- (void)reloadCollection {
    
    [self resetSwipeOffset];
    [self.collectionViewEPG reloadData];
    self.viewBgPhone.hidden = ([self.channelsList count] == 0);
    
    if (self.channelsList.count > 0){
        [self.selectDateView  restoreButtons];
    } else {
        [self.selectDateView  clearSearchResultsAndDisableButtons];
    }
}
#pragma mark - uitable view methods

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    EPGCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:@"EPGCollectionViewCell" forIndexPath:indexPath];
    cellView.backgroundColor = [VFTypography instance].grayScale2Color;
    
    TVMediaItem *channel = [self.channelsList objectAtIndex:indexPath.row];
    
    [cellView updateCellWithMediaItem:channel];
    
    cellView.channel = channel;
        
    if (searchMode) {
        
        if (self.selectedDay + APST.numberOfRetroDays < [self.searchDays count]) {
            
            NSDictionary *item = [self.searchDays objectAtIndex:self.selectedDay + APST.numberOfRetroDays];
            
            NSString *key = [NSString stringWithFormat:@"%d_%d", [[item objectForKey:@"day"] intValue], [[item objectForKey:@"month"] intValue]];
            
            for (NSDictionary *dic in self.searchFullListChannels) {
                
                if ([dic objectForKey:key]) {
                    
                    TVMediaItem *channelSearch = [dic objectForKey:@"channel"];
                    
                    if ([channel.mediaID isEqualToString:channelSearch.mediaID]) {
                        
                        cellView.viewEPG.hidden = NO;
                        cellView.viewLoading.hidden = YES;
                        
                        cellView.currentProgramIndex = 0;
                        
                        NSArray *array = [dic objectForKey:key];
                        APSTVProgram *program = nil;
                        if (array && [array count]) {
                            program = [array objectAtIndex:cellView.currentProgramIndex];
                        }
                        NSInteger programIndex = [array indexOfObject:program];
                        
                        [self.minimumOffsetForChannel setObject:[NSNumber numberWithInteger:-programIndex]  forKey:channel.epgChannelID];
                        [self.maximumOffsetForChannel setObject:[NSNumber numberWithInteger:array.count-programIndex-1] forKey:channel.epgChannelID];
                        
                        
                        program = array[programIndex+self.swipeBalanceOffset];
                        cellView.program = program;
                        if (program) {
                            cellView.epgList = array;
                        }
                        else
                        {
                            cellView.epgList = nil;
                        }
                        
                        cellView.currentProgramIndex = programIndex+self.swipeBalanceOffset;
                        [cellView updateProgramItem:(int)cellView.currentProgramIndex];
                        
                        
                    }
                    
                }
                
            }
            
        }
        
        
    } else {
        
        
        NSString *key = [self channelSelectedDayKey:channel];
        NSArray *array = [self.dictionaryCurrentProgramByChannelID objectForKey:key];
        
        if (array == nil && [self.dictionaryRequest objectForKey:key] == nil)
        {
            [self loadProgramForChannel:channel];
            cellView.viewEPG.hidden = YES;
            cellView.viewLoading.hidden = NO;
        }
        else
        {
            cellView.viewEPG.hidden = NO;
            cellView.viewLoading.hidden = YES;
        }
        
        APSTVProgram *program = nil;
        if (array && [array count])
        {
            program = [self programInEPGDate:array];
        }
        
        cellView.program = program;
        
            // check nPVR
        
        

        if (program)
        {
            if (isPad)
            {
                NSString *key = [self channelSelectedDayKey:channel];
                NSMutableDictionary * recoredsTOChannel = [self.recoredAdapter takeAllChannelRecoreds:channel];
                
                if (recoredsTOChannel == nil && [self.dictionaryNpvrRequest objectForKey:key] == nil)
                {
                    [self loadRecoredByChannel:channel];
                }
            }
            cellView.recoredsOfChannel = [self.recoredAdapter takeAllChannelRecoreds:channel];

            NSInteger programIndex = [array indexOfObject:program];
            
            if (channel.epgChannelID)
            {
                [self.minimumOffsetForChannel setObject:[NSNumber numberWithInteger:-programIndex]      forKey:channel.epgChannelID];
                [self.maximumOffsetForChannel setObject:[NSNumber numberWithInteger:array.count-programIndex-1] forKey:channel.epgChannelID];
            }
            else
            {
                ASLogInfo(@"!!!! channel %@ ,id= %@ not have epgChannelID !!!!", channel.name,channel.mediaID);
            }

            
            NSInteger indexWithOffset = programIndex+self.swipeBalanceOffset;
            indexWithOffset = MAX(0, indexWithOffset);
            indexWithOffset = MIN(indexWithOffset, array.count-1);
            cellView.program = array[indexWithOffset];
            cellView.epgList = array;
            
            cellView.currentProgramIndex = indexWithOffset;
        }
        else
        {
            cellView.currentProgramIndex = -1;
            cellView.epgList = nil;
        }
            //[self programIndexInEPGDate:array];
        [cellView updateProgramItem:(int)cellView.currentProgramIndex];
    }
    
    
    cellView.delegate = self;
    
    return cellView;
    
}


- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (![collectionView.indexPathsForVisibleItems containsObject:indexPath])
    {
        
        if (indexPath.item < self.channelsList.count)
        {
            
            
            TVMediaItem * channel = self.channelsList[indexPath.item];
            
            if (channel.epgChannelID)
            {
                [self.minimumOffsetForChannel removeObjectForKey:channel.epgChannelID];
                [self.maximumOffsetForChannel removeObjectForKey:channel.epgChannelID];
            }
            else
            {
                ASLogInfo(@"!!!! channel %@ ,id= %@ not have epgChannelID !!!!", channel.name,channel.mediaID);
            }

            
            NSString *key = [self channelSelectedDayKey:channel];
            
            APSBlockOperation * request = [self.dictionaryRequest objectForKey:key];
            [request cancel];
            [self.dictionaryRequest removeObjectForKey:key];
            
            TVPAPIRequest * npvrRequest = [self.dictionaryNpvrRequest objectForKey:key];
            [npvrRequest cancel];
            [self.dictionaryNpvrRequest removeObjectForKey:key];
        }
    }
    
}

@end
