//
//  SettingsAccountViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SettingsAccountViewController.h"
#import "SettingsCell.h"
#import "SettingsListViewController.h"
#import "SettingsEditViewController.h"
#import "AnalyticsManager.h"
#import "SettingsPushAnimator.h"

@interface SettingsAccountViewController ()

@end

@implementation SettingsAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    [Utils setLabelFont:_labelTitle bold:YES];
    _labelTitle.text = LS(@"screen_title_settings_account");
    
    
    [Utils setButtonFont:_buttonFacebook bold:YES];
    _buttonFacebook.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _buttonFacebook.hidden = YES;
    _buttonFacebook.userInteractionEnabled = YES;
    
    if (isPad) self.navigationController.delegate = self;
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self menuContainerViewController].panMode = MFSideMenuPanModeNone;
    
    TVUser *user = [TVSessionManager sharedTVSessionManager].currentUser;
    
    NSString *accountName = [NSString stringWithFormat:@"%@ %@ (%@)", user.firstName, user.lastName, user.email];
    [_buttonFacebook setTitle:[NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName] forState:UIControlStateNormal];
    
    self.contentList = [NSArray arrayWithObjects:accountName, LS(@"available_subscription"), nil];
    
    if (APST.billingHistorySettingsEnabled)
    {
        self.contentList = [self.contentList arrayByAddingObject:LS(@"screen_title_settings_billing_history")];
    }
    [_tableViewContent reloadData];
    
}


#pragma mark - Button

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)buttoAccountPressed:(UIButton *)button {

    SettingsEditViewController *controller = [[ViewControllersFactory defaultFactory] settingsEditViewController];
    [self.navigationController pushViewController:controller animated:YES];
    
    NSArray *components = @[LS(@"action_settings"), LS(@"screen_title_settings_account"), LS(@"screen_title_settings_edit_details")];
    [AnalyticsM trackScreen:[components componentsJoinedByString:@"->"]];
}

#pragma mark - TableView dataSource implementation

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_contentList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //disable billing history if inapp is active
    if (APST.hasInAppPurchases && section == 2)
        return 0;
    
    return 1;
    
}

- (UIView *)createHeaderView:(NSString *)title {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableViewContent.frame.size.width, 40.f)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, _tableViewContent.frame.size.width, 40.f)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor lightGrayColor];
    label.text = title;
    [Utils setLabelFont:label size:11 bold:YES];
    [headerView addSubview:label];
    
    return headerView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //set footer height for last section
    return (APST.hasInAppPurchases && (section == [self numberOfSectionsInTableView:tableView]-1)) ? 80 : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    //set footer for last section
    if (APST.hasInAppPurchases &&  (section == [self numberOfSectionsInTableView:tableView]-1)) {
        CGFloat height = [self tableView:tableView heightForFooterInSection:section];
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.width, height)];
        footerView.backgroundColor = [UIColor clearColor];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.translatesAutoresizingMaskIntoConstraints = NO;
        button.titleLabel.font = Font_Bold(14);
        [button setBackgroundColor:CS(@"444444")];
        [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal<<UIControlStateHighlighted<<UIControlStateSelected];
        [button setTitle:LS(@"fragment_account_restore_purchases_button_title") forState:UIControlStateNormal<<UIControlStateHighlighted<<UIControlStateSelected];
        button.layer.cornerRadius = 5;
        button.layer.masksToBounds = YES;
        [button setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];

        [button setTouchUpInsideAction:^(UIButton *btn) {
            BaseViewController *base = (isPad) ? (BaseViewController*)self.parentViewController.parentViewController : self;
            [base showHUDBlockingUI:YES];
            [IAP restoreSubscriptions:^(SubscriptionRequestParams *reqParams) {
                reqParams.delegate = self;
            } completion:^(SubscriptionResponseParams *resParams) {
                [base hideHUD];
                if (resParams.success) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"fragment_account_restore_purchases_button_title")
                                                                        message:LS(@"fragment_account_restore_purchases_success_description")
                                                                       delegate:nil
                                                              cancelButtonTitle:nil
                                                              otherButtonTitles:LS(@"Continue"), nil];
                    
                    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
                        [self tableView:tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
                    }];
                }
                else {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"fragment_account_restore_purchases_button_title")
                                                                        message:LS(@"fragment_account_restore_purchases_error_description")
                                                                       delegate:nil
                                                              cancelButtonTitle:nil
                                                              otherButtonTitles:LS(@"ok"), nil];
                    
                    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
                    }];
                }
            }];
        }];
        [footerView addSubview:button];
        NSDictionary *metrics = @{@"height": @(height-40),
                                  @"trailing": (isPhone) ? @15 : @0};

        [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(trailing)-[button]-(>=10)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(button)]];
        [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-30-[button(height)]-10-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(button)]];
        
        return footerView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 20.f;
            break;
        case 1:
            return 40.0f;
            break;
        case 2: //billing history
            //disable billing history if inapp is active
            if (APST.hasInAppPurchases)
                return .0f;
            return 40.0f;
        default:
            break;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0: {
            
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableViewContent.frame.size.width, 20.f)];
            headerView.backgroundColor = [UIColor clearColor];
            return  headerView;
            break;
        }
        case 1: {
            
            return [self createHeaderView:[LS(@"subscription") uppercaseString]];
            break;
        }
        case 2: //billing history
        {
            //disable billing history if inapp is active
            if (APST.hasInAppPurchases)
                return nil;
            
            return [self createHeaderView:[LS(@"screen_title_settings_billing_history") uppercaseString]] ;
            break;
        }
        default:
            break;
    }
    
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * const cellIdentificator = @"SettingsCell";
    
    SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentificator];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentificator owner:tableView options:nil] objectAtIndex:(isPhone ? 0 : 1)];
        
    }
    
    [cell updateBgImage];
    
    cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, tableView.frame.size.width, cell.frame.size.height);
    
    cell.textLabel.textColor = [UIColor lightGrayColor];
    cell.textLabel.text = [_contentList objectAtIndex:indexPath.section];
    [Utils setLabelFont:cell.textLabel size:16 bold:YES];
    
    UIImageView *accesoryImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"settings_accesory_icon"]];
    accesoryImage.frame = CGRectMake((cell.frame.size.width - accesoryImage.frame.size.width) - 5.f, 0.f, accesoryImage.frame.size.width, accesoryImage.frame.size.width);
    [cell addSubview:accesoryImage];
    
    if (indexPath.section == 0 && LoginM.isFacebookUser)
    {
        cell.hidden = YES;
        _buttonFacebook.hidden = NO;
    }
    else if (indexPath.section == 0)
    {
        cell.hidden = NO;
        _buttonFacebook.hidden = YES;
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        [self buttoAccountPressed:_buttonFacebook];
        
    }  else if (indexPath.section == 1) {
        
        SettingsListViewController *controller = [[ViewControllersFactory defaultFactory] settingsListViewController];
        controller.settingsListType = kSettingsListAvailableSubscriptions;
        [self.navigationController pushViewController:controller animated:YES];

        NSArray *components = @[LS(@"action_settings"), LS(@"screen_title_settings_account"), LS(@"screen_title_settings_available_subscription")];
        [AnalyticsM trackScreen:[components componentsJoinedByString:@"->"]];
        
    } else if (indexPath.section == 2) {
        
        SettingsListViewController *controller = [[ViewControllersFactory defaultFactory] settingsListViewController];
        controller.settingsListType = kSettingsListBillingHistory;
        [self.navigationController pushViewController:controller animated:YES];

        NSArray *components = @[LS(@"action_settings"), LS(@"screen_title_settings_account"), LS(@"screen_title_settings_billing_history")];
        [AnalyticsM trackScreen:[components componentsJoinedByString:@"->"]];
        
    }
}

#pragma mark - UINavigationDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    
    if (operation == UINavigationControllerOperationPush) {
            return [SettingsPushAnimator new];
        }
    
    return nil;
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
