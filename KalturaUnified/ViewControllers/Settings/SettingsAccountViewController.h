//
//  SettingsAccountViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsAccountViewController : BaseViewController<UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableViewContent;

@property (strong, nonatomic) IBOutlet UIButton *buttonFacebook;

@property (strong, nonatomic) NSArray *contentList;

@end
