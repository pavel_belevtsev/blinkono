//
//  SettingsPrivacyViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SettingsPrivacyViewController.h"
#import "SliderCell.h"

@interface SettingsPrivacyViewController ()

@end

@implementation SettingsPrivacyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    [Utils setLabelFont:_labelTitle bold:YES];
    _labelTitle.text = LS(@"screen_title_settings_privacy");
    
    UILabel *titleLabel = [[_viewHeaderActivity subviews] objectAtIndex:0];
    titleLabel.text = [LS(@"settings_privacy_activity_sharing") uppercaseString];
    [Utils setLabelFont:titleLabel bold:YES];
    
    UILabel *textLabel = [[_viewHeaderActivity subviews] objectAtIndex:1];
    textLabel.text = LS(@"settings_privacy_title");
    [Utils setLabelFont:textLabel bold:NO];
    
    titleLabel = [[_viewHeaderFacebook subviews] objectAtIndex:0];
    titleLabel.text = [LS(@"settings_privacy_facebook_title") uppercaseString];
    [Utils setLabelFont:titleLabel bold:YES];
    
    textLabel = [[_viewHeaderFacebook subviews] objectAtIndex:1];
    textLabel.text = LS(@"settings_privacy_post");
    [Utils setLabelFont:textLabel bold:NO];

    [_viewHeaderActivity removeFromSuperview];
    [_viewHeaderFacebook removeFromSuperview];
    
    self.contentList = [NSArray arrayWithObjects:LS(@"like"), LS(@"watch"), nil];

    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (!isInitialized) {
        isInitialized = YES;
        
        [self showHUDBlockingUI:YES];
        _requestsCount = 4;
        
        [self requestForGetUserInternalActionsShare:TVUserSocialActionLIKE];
        [self requestForGetUserInternalActionsShare:TVUserSocialActionWATCHES];
        
        [self requestForGetUserExternalActionsShare:TVUserSocialActionLIKE];
        [self requestForGetUserExternalActionsShare:TVUserSocialActionWATCHES];

        
    }
    
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    } else {
        /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
        if (!APST.enableFBSharePrivacy) {
            return 0;
        }else{
            return 1;
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!APST.enableFBSharePrivacy) {
        return 1;
    }else{
        return 2;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return _viewHeaderActivity.frame.size.height;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return _viewHeaderActivity;
    } else {
        /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
        if (!APST.enableFBSharePrivacy) {
            return nil;
        }else{
            return _viewHeaderFacebook;
        }
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * const st_cellId = @"SliderCell";
    
    SliderCell *cell = [tableView dequeueReusableCellWithIdentifier:st_cellId];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:st_cellId owner:tableView options:nil] objectAtIndex:(isPhone ? 0 : 1)];
    
    }
    
    cell.slider.tag = indexPath.row + (indexPath.section * 100);
    
    if (indexPath.section == 0) {
        
        cell.label.text = [_contentList objectAtIndex:indexPath.row];
        
    } else {
        
        cell.label.text = LS(@"post_to_Facebook");
        
    }
    
    [cell.slider addTarget:self action:@selector(changeState:) forControlEvents:UIControlEventValueChanged];
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
            [cell.slider setOn:_isLikeInternal];
            
        } else {
            
            [cell.slider setOn:_isWatchesInternal];
            
        }
    } else {
        
        if (!_isLikeInternal && !_isWatchesInternal) {
            [cell.slider setOn:NO];
            cell.slider.userInteractionEnabled = NO;
            cell.slider.alpha = 0.7;
        } else {
            [cell.slider setOn:(_isLikeExternal || _isWatchesExternal)];
            cell.slider.userInteractionEnabled = YES;
            cell.slider.alpha = 1.0;
        }
    }
    
    if (isPhone) {
        cell.backgroundColor = CS(@"232323");
    } else {
        cell.backgroundColor = [UIColor clearColor];
        
        if (indexPath.section == 0) {
            
            cell.imgBg.image = [UIImage imageNamed:((indexPath.row == 0) ? @"settings_list_item_top" : @"settings_list_item_bottom")];
            cell.viewSeparator.hidden = (indexPath.row == 1);
        }
    
    }
    
    cell.label.textColor = [UIColor lightGrayColor];
    [Utils setLabelFont:cell.label size:16 bold:YES];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (void)changeState:(id)sender {
    UISwitch *state = sender;
    TVUserPrivacy privacy = state.on ? TVUserPrivacy_ALLOW : TVUserPrivacy_DONT_ALLOW;
    if (state.tag < 100) {
        TVUserSocialActionType actionType = state.tag ? TVUserSocialActionWATCHES : TVUserSocialActionLIKE;
        
        if (actionType == TVUserSocialActionLIKE) {
            _isLikeInternal = !_isLikeInternal;
            if (!_isLikeInternal && _isLikeExternal) {
                _isLikeExternal = NO;
                [self setUserExternalActionStatus:TVUserPrivacy_DONT_ALLOW forAction:actionType];
                
            } else if (_isLikeInternal && !_isLikeExternal && _isWatchesExternal) {
                
                _isLikeExternal = YES;
                [self setUserExternalActionStatus:TVUserPrivacy_ALLOW forAction:actionType];
                
            }
        } else {
            _isWatchesInternal = !_isWatchesInternal;
            if (!_isWatchesInternal && _isWatchesExternal) {
                _isWatchesExternal = NO;
                [self setUserExternalActionStatus:TVUserPrivacy_DONT_ALLOW forAction:actionType];
                
            } else if (_isWatchesInternal && !_isWatchesExternal && _isLikeExternal) {
                
                _isWatchesExternal = YES;
                [self setUserExternalActionStatus:TVUserPrivacy_ALLOW forAction:actionType];
                
            }
        }
        
        [self.tableViewContent reloadData];
        [self setUserInternalActionStatus:privacy forAction:actionType];
        
    } else {
        
        if (state.on) {
            if (_isLikeInternal && !_isLikeExternal) {
                _isLikeExternal = YES;
                [self.tableViewContent reloadData];
                [self setUserExternalActionStatus:TVUserPrivacy_ALLOW forAction:TVUserSocialActionLIKE];
            }
            if (_isWatchesInternal && !_isWatchesExternal) {
                _isWatchesExternal = YES;
                [self.tableViewContent reloadData];
                [self setUserExternalActionStatus:TVUserPrivacy_ALLOW forAction:TVUserSocialActionWATCHES];
            }
        } else {
            
            if (_isLikeExternal) {
                _isLikeExternal = NO;
                [self.tableViewContent reloadData];
                [self setUserExternalActionStatus:TVUserPrivacy_DONT_ALLOW forAction:TVUserSocialActionLIKE];
            }
            if (_isWatchesExternal) {
                _isWatchesExternal = NO;
                [self.tableViewContent reloadData];
                [self setUserExternalActionStatus:TVUserPrivacy_DONT_ALLOW forAction:TVUserSocialActionWATCHES];
            }
            
        }
        
    }
    
}


#pragma mark - Network

- (void)requestForGetUserExternalActionsShare:(TVUserSocialActionType)action {
    //[self.fullScreenProgress show];
    __weak TVPAPIRequest *request = [TVPSocialAPI requestForGetUserExternalActionShare:action
                                                                        socialPlatform:TVSocialPlatformFacebook
                                                                              delegate:nil];
    [request setCompletionBlock:^{
        
        
        NSString *response = [request JSONResponse];
        BOOL userActionStatus;
        if ([response isEqualToString:@"DONT_ALLOW"]) {
            userActionStatus = NO;
            if (action == TVUserSocialActionWATCHES) {
                LoginM.externalShareWatches = @"DONT_ALLOW";
            }
        } else if ([response isEqualToString:@"ALLOW"] || [response isEqualToString:@"UNKNOWN"]){
            userActionStatus = YES;
            if (action == TVUserSocialActionLIKE) {
                _isLikeExternal = YES;
            } else {
                _isWatchesExternal = YES;
                LoginM.externalShareWatches = @"ALLOW";
                
            }
        } else {
            [self showAlertWithTitle:LS(@"alert_general_error")
                             message:LS(@"shareActions_FailedGetStatus")];
        }
        
        [self.tableViewContent reloadData];
        if (--_requestsCount == 0) {
            [self hideHUD];
        }
    }];
    
    [request setFailedBlock:^{
        
        [self showAlertWithTitle:LS(@"alert_general_error")
                         message:LS(@"shareActions_FailedGetStatus")];
        if (--_requestsCount == 0) {
            [self hideHUD];
        }
    }];
    
    [request startAsynchronous];
}


- (void)requestForGetUserInternalActionsShare:(TVUserSocialActionType)action {

    
    __weak TVPAPIRequest *request = [TVPSocialAPI requestforGetUserInternalActionPrivacyWithUserAction:action
                                                                                        socialPlatform:TVSocialPlatformFacebook
                                                                                              delegate:nil];
    [request setCompletionBlock:^{
        
        
        NSString *response = [request JSONResponse];
        BOOL userActionStatus;
        if ([response isEqualToString:@"DONT_ALLOW"]) {
            userActionStatus = NO;
        } else if ([response isEqualToString:@"ALLOW"] || [response isEqualToString:@"UNKNOWN"]){
            userActionStatus = YES;
            if (action == TVUserSocialActionLIKE) {
                _isLikeInternal = YES;
            } else {
                _isWatchesInternal = YES;
            }
        } else {
            [self showAlertWithTitle:LS(@"alert_general_error")
                             message:LS(@"shareActions_FailedGetStatus")];
        }
        
        [self.tableViewContent reloadData];
        
        if (--_requestsCount == 0) {
            [self hideHUD];
        }
    }];
    
    [request setFailedBlock:^{

        [self showAlertWithTitle:LS(@"alert_general_error")
                         message:LS(@"shareActions_FailedGetStatus")];
        if (--_requestsCount == 0) {
            [self hideHUD];
        }
    }];
    
    [request startAsynchronous];
}

- (void)setUserExternalActionStatus:(TVUserPrivacy)userPrivacy forAction:(TVUserSocialActionType)action {
    __weak TVPAPIRequest *request = [TVPSocialAPI requestForSetUserExternalActionShare:action
                                                                        socialPlatform:TVSocialPlatformFacebook
                                                                         actionPrivacy:userPrivacy
                                                                              delegate:nil];
    [request setCompletionBlock:^{
        if (![[request JSONResponse] boolValue]) {
            [self.tableViewContent reloadData];
            [self showAlertWithTitle:LS(@"alert_general_error")
                             message:LS(@"shareActions_FailedSetStatus")];
        } else {
            
            if (action == TVUserSocialActionWATCHES) {
                if (userPrivacy == TVUserPrivacy_ALLOW) {
                    LoginM.externalShareWatches = @"ALLOW";
                } else {
                    LoginM.externalShareWatches = @"DONT_ALLOW";
                }
            }
            
        }
    }];
    
    [request setFailedBlock:^{

        [self.tableViewContent reloadData];
        [self showAlertWithTitle:LS(@"alert_general_error")
                         message:LS(@"shareActions_FailedSetStatus")];
    }];
    
    [request startAsynchronous];
}

- (void)setUserInternalActionStatus:(TVUserPrivacy)userPrivacy forAction:(TVUserSocialActionType)action {
    
    __weak TVPAPIRequest *request = [TVPSocialAPI requestForSetUserInternalActionPrivacyWithUserAction:action
                                                                                        socialPlatform:TVSocialPlatformFacebook
                                                                                         privacyAction:userPrivacy
                                                                                              delegate:nil];
    [request setCompletionBlock:^{
        if (![[request JSONResponse] boolValue]) {
            [self.tableViewContent reloadData];
            [self showAlertWithTitle:LS(@"alert_general_error")
                             message:LS(@"shareActions_FailedSetStatus")];
        }
    }];
    
    [request setFailedBlock:^{

        [self.tableViewContent reloadData];
        [self showAlertWithTitle:LS(@"alert_general_error")
                         message:LS(@"shareActions_FailedSetStatus")];
    }];
    
    [request startAsynchronous];
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    
    [self hideHUD];
    
    NSString *okButtonTitle = LS(@"ok");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:okButtonTitle otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - Button

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
