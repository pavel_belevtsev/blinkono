//
//  SettingsAboutUsViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsAboutUsViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@end
