//
//  SettingsAboutViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SettingsAboutViewController.h"
#import "SettingsCell.h"
#import "SettingsAboutUsViewController.h"
#import "AnalyticsManager.h"


@interface SettingsAboutViewController ()
@end

@implementation SettingsAboutViewController

-(void)buildContentList
{
    self.contentList = [NSArray array];
    
    if (APST.contactUsPageSettingsEnabled)
    {
        self.contentList = [self.contentList arrayByAddingObject:LS(@"settings_about_contact_us")];
    }
    
    self.contentList = [self.contentList arrayByAddingObject:LS(@"settings_about_about_us")];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Utils setLabelFont:_labelTitle bold:YES];
    _labelTitle.text = LS(@"screen_title_settings_about");
    
    [self buildContentList];

    [_viewFooter removeFromSuperview];
    
    UILabel *footerLabelVer = [[_viewFooter subviews] objectAtIndex:0];
    UILabel *footerLabelNum = [[_viewFooter subviews] objectAtIndex:1];
    
    
    [Utils setLabelFont:footerLabelNum bold:YES];
    
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
//    NSString *versionBundle = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
    NSNumber * rcVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ReleaseCandidate"];
    footerLabelNum.text = [NSString stringWithFormat:@"%@ (%@%@)", version, LS(@"rc"), rcVersion/*, LS(@"Time Stamp"),versionBundle*/];
    [Utils setLabelFont:footerLabelVer bold:YES];
    footerLabelVer.text = LS(@"settings_about_version");

    _tableViewContent.tableFooterView = _viewFooter;
    
    //_labelIcons.text = LS(@"settings_about_icons_text");
    _labelIcons.text = LS(@"");
    [Utils setLabelFont:_labelIcons];
    
    UIView *viewLogos = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _poweredByView.frame.size.width, _poweredByView.frame.size.height / 2)];
    
    NSArray *array = [NSArray arrayWithObjects:@"settings_about_powered_by_text", @"settings_about_logo", nil];
    
    int logoX = 0;
    
    for (int i = 0; i < [array count]; i++) {
        NSString *str = [array objectAtIndex:i];
        
        if (i % 2 == 0) {
            
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(logoX, 11, viewLogos.frame.size.width, viewLogos.frame.size.height - 11)];
            label.text = LS(str);
            label.backgroundColor = [UIColor clearColor];
            label.textColor = _labelIcons.textColor;
            label.font = _labelIcons.font;
            label.textAlignment = NSTextAlignmentCenter;
            label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, [label.text sizeWithAttributes:@{NSFontAttributeName:label.font}].width + 10, label.frame.size.height);
            [viewLogos addSubview:label];
            
            logoX += label.frame.size.width;
            
        } else {
            
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:str]];
            imgView.frame = CGRectMake(logoX, 4, imgView.frame.size.width, imgView.frame.size.height);
            [viewLogos addSubview:imgView];
            
            logoX += imgView.frame.size.width;
        }
    }
    
    CGRect frameLogos = viewLogos.frame;
    frameLogos.origin.x = (int)((_poweredByView.frame.size.width - logoX) / 2);
    frameLogos.size.width = logoX;
    
    viewLogos.frame = frameLogos;
    
    [_poweredByView addSubview:viewLogos];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self menuContainerViewController].panMode = MFSideMenuPanModeNone;
 
    [_tableViewContent reloadData];
    
}


#pragma mark - Button

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - TableView dataSource implementation

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_contentList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 20.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20.0)];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * const cellIdentificator = @"SettingsCell";
    
    SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentificator];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentificator owner:tableView options:nil] objectAtIndex:(isPhone ? 0 : 1)];
        
    }
    
    [cell updateBgImage];
    
    cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, tableView.frame.size.width, cell.frame.size.height);
    cell.textLabel.textColor = [UIColor lightGrayColor];
    cell.textLabel.text = [_contentList objectAtIndex:indexPath.section];
    [Utils setLabelFont:cell.textLabel size:16 bold:YES];
    
    UIImageView *accesoryImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"settings_accesory_icon"]];
    accesoryImage.frame = CGRectMake((cell.frame.size.width - accesoryImage.frame.size.width) - 5.f, 0.f, accesoryImage.frame.size.width, accesoryImage.frame.size.width);
    [cell addSubview:accesoryImage];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString * componenet = [self.contentList objectAtIndex:indexPath.row];

    if ([componenet isEqualToString:LS(@"settings_about_contact_us")]/*indexPath.section == 0*/) {
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
        [self sendEmail];
        
    }  else if ([componenet isEqualToString:LS(@"settings_about_about_us")]) {
        
        SettingsAboutUsViewController *controller = [[ViewControllersFactory defaultFactory] settingsAboutUsViewController];
        [self.navigationController pushViewController:controller animated:YES];
        
        NSArray *components = @[LS(@"action_settings"), LS(@"settings_about"), LS(@"settings_about_about_us")];
        [AnalyticsM trackScreen:[components componentsJoinedByString:@"->"]];
    }
    
}

- (void)sendEmail {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *email = [[MFMailComposeViewController alloc] init];
        email.mailComposeDelegate = self;
        
        NSString *emailStr = APST.mailAddress;

        [email setToRecipients:[NSArray arrayWithObject:emailStr]];
        
        [self presentViewController:email animated:YES completion:nil];
        
        NSArray *components = @[LS(@"action_settings"), LS(@"settings_about"), LS(@"settings_about_contact_us")];
        [AnalyticsM trackScreen:[components componentsJoinedByString:@"->"]];
    } else {
        UIAlertView *emailAlert = [[UIAlertView alloc] initWithTitle:LS(@"email_Failure") message:LS(@"shareMessage_CantSentMail") delegate:self cancelButtonTitle:[LS(@"ok") uppercaseString] otherButtonTitles: nil];
        [emailAlert show];
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
