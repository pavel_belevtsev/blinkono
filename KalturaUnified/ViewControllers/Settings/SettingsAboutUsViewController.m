//
//  SettingsAboutUsViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SettingsAboutUsViewController.h"

@interface SettingsAboutUsViewController ()<UIWebViewDelegate>

@end

@implementation SettingsAboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Utils setLabelFont:_labelTitle bold:YES];
    _labelTitle.text = LS(@"settings_about_about_us");

    if (isPad) {
        [Utils setButtonFont:_buttonBack bold:YES];
        [_buttonBack setTitle:LS(@"back") forState:UIControlStateNormal];
    }
    
    [_webView loadRequest:[NSURLRequest requestWithURL:APST.aboutUsUrl]];
    
}

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if (inType == UIWebViewNavigationTypeLinkClicked) {
        
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        
        return NO;
    }
    
    return YES;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self showHUDBlockingUI:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hideHUD];
}

- (void)dealloc {
    
    _webView.delegate = nil;
    [_webView stopLoading];
    
}


#pragma mark - Button

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
