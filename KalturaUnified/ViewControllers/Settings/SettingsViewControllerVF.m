//
//  SettingsViewControllerVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 2/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingsViewControllerVF.h"
#import "GroupedStyleTableView.h"
#import "SettingsCellVF.h"

@interface SettingsViewControllerVF ()

@end

@implementation SettingsViewControllerVF

// This is Israel changes .. but the class was not use. so i comment it out and add my change.


///* "PSC : VOdafone Grup : change Location to GroupedStyleTableView  : JIRA ticket. */
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//
//    // table view should have margins on left/right:
//    float xDelta = 15.0;
//    CGRect frame = self.tableViewContent.frame;
//    frame.size.width -= xDelta*2;
//    frame.origin.x = xDelta;;
//    frame.origin.y += 15.0;;
//    self.tableViewContent.frame = frame;
//
//
//    GroupedStyleTableView* table = (GroupedStyleTableView*)self.tableViewContent;
//    table.cornerRadius = 8.0;
//    
//    self.viewContent.backgroundColor = [VFTypography instance].darkGrayColor;
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
///* "PSC : VOdafone Grup : change Location to GroupedStyleTableView  : JIRA ticket. */
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    SettingsCellVF *cell = (SettingsCell*)[super tableView:tableView cellForRowAtIndexPath:indexPath ];
//    
//    [cell updateSepratorLocate];
//    
//    return cell;
//}


// "PSC : ONO :  OPF-857 - settings - error appears in privacy and the app is stuck */
// "PSC : ONO :  OPF-1597VFGTVONES-559 - iOS-Remove section "Sobre nosotros" (about us) */

-(void)updateContentList{

    
    NSMutableArray * arr = [NSMutableArray arrayWithArray:@[[SettingsObject createWithName:LS(@"settings_account") type:SettingsTypeAccount],
                                                            [SettingsObject createWithName:LS(@"settings_about") type:SettingsTypeAbout]]];
    
    if ([NU(UIUnit_Settings_Downloads) isUIUnitExist])
    {
        [arr insertObject: [SettingsObject createWithName:LS(@"settings_downloads") type:SettingsTypeDownloads] atIndex:2];
    }
    self.contentList = arr;


}
@end
