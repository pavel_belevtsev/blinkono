//
//  SettingsAboutViewControllerVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 2/5/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingsAboutViewControllerVF.h"
#import "GroupedStyleTableView.h"
#import "SettingsAboutUsViewController.h"
#import "AnalyticsManager.h"

@interface SettingsAboutViewControllerVF ()

@end

@implementation SettingsAboutViewControllerVF

- (void) awakeFromNib
{
    [super awakeFromNib];
}

/* "PSC : Vodafone Grup : change Location to GroupedStyleTableView  : JIRA ticket. */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (isPhone)
    {
    // table view should have margins on left/right:
        float xDelta = 15.0;
        CGRect frame = self.tableViewContent.frame;
        frame.size.width -= xDelta*2;
        frame.origin.x = xDelta;;
        frame.origin.y += 15.0;;
        self.tableViewContent.frame = frame;

        GroupedStyleTableView* table = (GroupedStyleTableView*)self.tableViewContent;
        table.cornerRadius = 8.0;

        self.viewContent.backgroundColor = [VFTypography instance].darkGrayColor;
        self.tableViewContent.backgroundColor = [UIColor clearColor];

        UILabel *footerLabelNum = [[self.viewFooter subviews] objectAtIndex:1];
        CGRect footerLabelNumFrame = footerLabelNum.frame;
        footerLabelNumFrame.origin.x-=xDelta*2;
        footerLabelNum.frame = footerLabelNumFrame;
    }
    self.poweredByView.hidden = YES;
    self.labelIcons.hidden= YES;
    // "PSC : Vodafone Group : SYN-3574 -> About - the version number is unclear
    UILabel *footerLabelNum = [[self.viewFooter subviews] objectAtIndex:1];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *versionBundle = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
    footerLabelNum.text = [NSString stringWithFormat:@"%@ (%@)", version, versionBundle];
}

/* "PSC : Vodafone Grup : change Location to GroupedStyleTableView  : JIRA ticket. */
#pragma mark - TableView dataSource implementation

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (isPhone)
    {
        return 1;
    }
    return [super numberOfSectionsInTableView:tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isPhone)
    {
        return [self.contentList count];
    }
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (isPhone)
    {
        return 0;
    }
    return [super tableView:tableView heightForHeaderInSection:section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (isPhone)
    {
        return nil;
    }
    return [super tableView:tableView viewForHeaderInSection:section];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isPhone)
    {
        UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
        cell.textLabel.text = [self.contentList objectAtIndex:indexPath.row];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];

        return cell;
    }
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (isPhone)
    {
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
    else
    {
        NSString * componenet = [self.contentList objectAtIndex:indexPath.section];

        if ([componenet isEqualToString:LS(@"settings_about_contact_us")]/*indexPath.section == 0*/) {
            [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
            [self sendEmail];
            
        }  else if ([componenet isEqualToString:LS(@"settings_about_about_us")]) {
            
            SettingsAboutUsViewController *controller = [[ViewControllersFactory defaultFactory] settingsAboutUsViewController];
            [self.navigationController pushViewController:controller animated:YES];
            
            NSArray *components = @[LS(@"action_settings"), LS(@"settings_about"), LS(@"settings_about_about_us")];
            [AnalyticsM trackScreen:[components componentsJoinedByString:@"->"]];
        }
    }
        
}


@end
