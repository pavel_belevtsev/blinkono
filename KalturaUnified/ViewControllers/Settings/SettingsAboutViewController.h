//
//  SettingsAboutViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>

@interface SettingsAboutViewController : BaseViewController <MFMailComposeViewControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableViewContent;

@property (strong, nonatomic) NSArray *contentList;

@property (strong, nonatomic) IBOutlet UIView *viewFooter;

@property (weak, nonatomic) IBOutlet UIView *poweredByView;
@property (weak, nonatomic) IBOutlet UILabel *labelIcons;

- (void)sendEmail;
-(void)buildContentList;
@end
