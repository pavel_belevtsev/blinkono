//
//  EditCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditCell : UIView

- (void)setPasswordMode;

@property (nonatomic, retain) IBOutlet UIButton *buttonCheck;
@property (nonatomic, retain) IBOutlet UILabel *cellLabel;
@property (nonatomic, retain) IBOutlet UITextField *textField;


@end
