//
//  EditCell.h
//  KalturaUnified
//
//  Created by Synergetica LLC on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditTableCell : UITableViewCell

- (void)setPasswordMode;

@property (nonatomic, retain) IBOutlet UIButton *buttonCheck;
@property (nonatomic, retain) IBOutlet UILabel *cellLabel;
@property (nonatomic, retain) IBOutlet UITextField *textField;

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

+(EditTableCell *) editTableCell;

@end
