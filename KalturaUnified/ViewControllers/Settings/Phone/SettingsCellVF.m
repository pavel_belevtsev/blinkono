//
//  SettingsCellVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 2/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingsCellVF.h"

@implementation SettingsCellVF

/* "PSC : VOdafone Grup : change color : JIRA ticket. */
- (void) awakeFromNib
{
    [super awakeFromNib];
    
    self.defaultBGColor = [UIColor colorWithWhite:35.0/255.0 alpha:1.0];
    
    CGRect frame = self.frame;
    frame.size.width = 290;
    self.frame = frame;
    
}

/* "PSC : VOdafone Grup : change Location to GroupedStyleTableView  : JIRA ticket. */
-(void)updateSepratorLocate
{
    CGRect sepFrame = self.sepretaorView.frame;
    sepFrame.origin.y -=1;
    self.sepretaorView.frame = sepFrame;
}
@end
