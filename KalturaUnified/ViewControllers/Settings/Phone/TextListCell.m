//
//  TextListCell.m
//  
//
//  Created by Eremenko Vladimir on 20.09.13.
//  Copyright (c) 2013 Quickode Ltd. All rights reserved.
//

#import "TextListCell.h"

@implementation TextListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
