//
//  BillingHistoryCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//


#import "BillingHistoryCell.h"

@implementation BillingHistoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)customCellWithName:(NSString *)name price:(NSString *)price type:(NSString *)type date:(NSString *)date {
    
    nameLabel.text= name;
    [Utils setLabelFont:nameLabel bold:YES];
    
    priceLabel.text = [NSString stringWithFormat:@"%@", price];
    priceLabel.textColor = APST.brandColor;
    [Utils setLabelFont:priceLabel bold:YES];
    
    typeLabel.text= type;
    typeLabel.textColor = [UIColor blackColor];
    [Utils setLabelFont:typeLabel bold:YES];
    CGSize maximumLabelSize = CGSizeMake(300, 20);
    //CGSize expectedLabelSize = [type sizeWithFont:typeLabel.font constrainedToSize:maximumLabelSize lineBreakMode:typeLabel.lineBreakMode];
    CGSize expectedLabelSize = [type boundingRectWithSize:maximumLabelSize
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:[typeLabel font]}
                                                  context:nil].size;
    
    CGRect frame = typeLabel.frame;
    frame.size.width= expectedLabelSize.width + 8;
    typeLabel.frame= frame;
    typeLabel.backgroundColor = APST.brandColor;
    
    dateLabel.frame= CGRectMake(typeLabel.frame.origin.x + typeLabel.frame.size.width + 6.f, dateLabel.frame.origin.y, dateLabel.frame.size.width, dateLabel.frame.size.height);
    dateLabel.text= date;
}

@end
