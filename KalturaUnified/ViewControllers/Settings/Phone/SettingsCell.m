//
//  SettingsCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SettingsCell.h"
#import "UIImage+Tint.h"

@implementation SettingsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    _defaultBGColor = [UIColor orangeColor];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if (_imgBg) {
        
        self.backgroundColor = [UIColor clearColor];
        _imgBg.highlighted = selected;
        
    } else {
        
        if (selected)
        {
            self.backgroundColor = APST.brandColor;
        }
        else
        {
            self.backgroundColor = [UIColor colorWithWhite:0.1568f alpha:1.f];
            self.backgroundColor = _defaultBGColor;
        }
    }
    
    if (selected)
    {
        self.textLabel.textColor = APST.brandTextColor;
    }
    else
    {
        self.textLabel.textColor = [UIColor lightGrayColor];
    }

}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (_imgBg) {
        
        self.backgroundColor = [UIColor clearColor];
        _imgBg.highlighted = highlighted;
        
    } else {
        
        if (highlighted)
        {
            self.backgroundColor = APST.brandColor;
        }
        else
        {
            self.backgroundColor = [UIColor colorWithWhite:0.1568f alpha:1.f];
            self.backgroundColor = _defaultBGColor;
        }
        
    }

    if (highlighted)
    {
        self.textLabel.textColor = APST.brandTextColor;
    }
    else
    {
        self.textLabel.textColor = [UIColor lightGrayColor];
    }
    
}

- (void)updateBgImage {
    
    if (_imgBg) {
        
        _imgBg.highlightedImage = [_imgBg.image imageTintedWithColor:APST.brandColor];
    }
    
}


- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.textLabel.frame = CGRectMake(self.textLabel.frame.origin.x, self.textLabel.frame.origin.y, [self.textLabel superview].frame.size.width - 50.0, self.textLabel.frame.size.height);
    
}

@end
