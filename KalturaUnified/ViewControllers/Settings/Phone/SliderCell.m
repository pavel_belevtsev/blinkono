//
//  SliderCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SliderCell.h"


@implementation SliderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib {
    
    [self.slider setOnTintColor:APST.brandColor];
    [self.slider setTintColor:CS(@"bebebe")];
    [self.slider setThumbTintColor:[UIColor whiteColor]];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
/*
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (highlighted)
    {
        self.backgroundColor= APST.brandColor;
        self.textLabel.textColor= [UIColor blackColor];
    }
    else
    {
        self.backgroundColor= [UIColor colorWithWhite:0.1568f alpha:1.f];
        self.textLabel.textColor= [UIColor lightGrayColor];
    }
}
*/
@end
