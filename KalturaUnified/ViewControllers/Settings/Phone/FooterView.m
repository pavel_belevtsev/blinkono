//
//  FooterView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "FooterView.h"
#import <QuartzCore/QuartzCore.h>

@implementation FooterView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {

    [self.footerButton setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
    [self.footerButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    self.footerButton.backgroundColor = APST.brandColor;
    self.footerButton.layer.cornerRadius = 5.0f;

}

@end
