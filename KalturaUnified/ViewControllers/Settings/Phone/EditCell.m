//
//  EditCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "EditCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation EditCell

- (void)awakeFromNib
{

    [Utils setButtonFont:_buttonCheck bold:YES];
    [_buttonCheck setTitle:LS(@"create_account_check") forState:UIControlStateNormal];

    _buttonCheck.hidden = YES;
    
}

- (IBAction)buttonCheckPressed:(UIButton *)button {
    
    //[self closeKeyboard];
    BOOL wasFirstResponder;
    if ((wasFirstResponder = [_textField isFirstResponder])) {
        [_textField resignFirstResponder];
    }
    
    [_textField setSecureTextEntry:![_textField isSecureTextEntry]];
    if (wasFirstResponder) {
        [_textField becomeFirstResponder];
    }
    
    [button setTitle:([_textField isSecureTextEntry] ? LS(@"create_account_check") : LS(@"create_account_hide")) forState:UIControlStateNormal];
    
}

- (void)setPasswordMode {
    
    _buttonCheck.hidden = NO;
    
    _textField.secureTextEntry = YES;
    _textField.frame = CGRectMake(_textField.frame.origin.x, _textField.frame.origin.y, _buttonCheck.frame.origin.x - _textField.frame.origin.x - 2.0, _textField.frame.size.height);
    
}

@end
