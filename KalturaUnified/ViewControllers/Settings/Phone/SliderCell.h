//
//  SliderCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SliderCell : UITableViewCell {
    
}

@property (nonatomic, strong) IBOutlet UILabel *label;
@property (nonatomic, strong) IBOutlet UISwitch *slider;
@property (nonatomic, assign) IBOutlet UIImageView *imgBg;
@property (nonatomic, assign) IBOutlet UIView *viewSeparator;

@end
