//
//  SettingsCellVF.h
//  Vodafone
//
//  Created by Aviv Alluf on 2/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingsCell.h"

@interface SettingsCellVF : SettingsCell

-(void)updateSepratorLocate;

@end
