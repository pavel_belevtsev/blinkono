//
//  BillingHistoryCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillingHistoryCell : UITableViewCell {
    
    IBOutlet UILabel *priceLabel;
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *typeLabel;
    IBOutlet UILabel *dateLabel;
}

-(void)customCellWithName:(NSString *)name price:(NSString *)price type:(NSString *)type date:(NSString *)date;

@end
