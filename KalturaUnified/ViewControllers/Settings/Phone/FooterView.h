//
//  FooterView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterView : UIView

@property (retain, nonatomic) IBOutlet UIButton *footerButton;

@end
