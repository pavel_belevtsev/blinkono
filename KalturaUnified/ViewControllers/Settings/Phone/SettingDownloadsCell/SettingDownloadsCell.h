//
//  SettingDownloadsCell.h
//  KalturaUnified
//
//  Created by Israel Berezin on 5/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingDownloadsCell : UITableViewCell
@property (nonatomic, assign) IBOutlet UIImageView *imgBg;
@property (nonatomic, assign) IBOutlet UIView *viewSeparator;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkMarkImage;

+(SettingDownloadsCell *) settingDownloadsCell;

@end
