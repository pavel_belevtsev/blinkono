//
//  SettingDownloadsCell.m
//  KalturaUnified
//
//  Created by Israel Berezin on 5/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingDownloadsCell.h"
#import "UIImage+Tint.h"

@implementation SettingDownloadsCell

- (void)awakeFromNib {
    // Initialization code
    UIImage *brandImage = [UIImage imageNamed:@"v_icon"];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:APST.brandColor];
    self.checkMarkImage.image = coloredBrandImage;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(SettingDownloadsCell *) settingDownloadsCell
{
    SettingDownloadsCell *view = nil;
    NSString *nibName = @"SettingDownloadsCell";
    if (isPad)
    {
         nibName = @"SettingDownloadsCellPad";
    }
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

@end
