//
//  SettingsCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell

- (void)updateBgImage;

@property (nonatomic, assign) IBOutlet UIImageView *imgBg;
@property (nonatomic, strong) UIColor* defaultBGColor;
@property (weak, nonatomic) IBOutlet UIView *sepretaorView;
@property NSInteger settingCellType;

@end
