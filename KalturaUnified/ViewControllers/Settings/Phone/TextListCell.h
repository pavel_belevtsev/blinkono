//
//  TextListCell.h
//  
//
//  Created by Eremenko Vladimir on 20.09.13.
//  Copyright (c) 2013 Quickode Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextListCell : UITableViewCell
{
    
}

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *date;

@end
