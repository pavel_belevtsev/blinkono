//
//  SettingsListViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SettingsListViewController.h"
#import "BillingHistoryCell.h"
#import "TextListCell.h"
#import "NSDate+Utilities.h"

@interface SettingsListViewController ()

@end

@implementation SettingsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_viewHeader removeFromSuperview];
    
    UILabel *headerLable0 = [[_viewHeader subviews] objectAtIndex:0];
    UILabel *headerLable1 = [[_viewHeader subviews] objectAtIndex:1];
    
    [Utils setLabelFont:headerLable0 bold:YES];
    [Utils setLabelFont:headerLable1 bold:YES];
    [Utils setLabelFont:_labelTitle bold:YES];
    [Utils setLabelFont:_labelBlank bold:YES];
    _viewBlank.hidden = YES;
    
    switch (self.settingsListType) {
        case kSettingsListBillingHistory:
            _labelTitle.text = LS(@"screen_title_settings_billing_history");
            headerLable0.text = [LS(@"fragment_account_billing_item") uppercaseString];
            headerLable1.text = LS(@"fragment_account_billing_price");
            _labelBlank.text = LS(@"settings_billing_history_no_items");
            break;
        case kSettingsListAvailableSubscriptions:
            _labelTitle.text = LS(@"screen_title_settings_available_subscription");
            headerLable0.text = [LS(@"transaction_type_subscription") uppercaseString];
            headerLable1.text = [LS(@"fragment_account_billing_expiry_date") uppercaseString];
            _labelBlank.text = LS(@"no_available_subscriptions");
            break;
        default:
            break;
    }
    
    _tableViewContent.tableHeaderView = _viewHeader;
    _tableViewContent.hidden = YES;
    
    if (isPad) {
        [Utils setButtonFont:_buttonBack bold:YES];
        [_buttonBack setTitle:LS(@"back") forState:UIControlStateNormal];
    }
}

- (NSString *)formatDate:(NSString *)dateStr {
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS"];
    NSDate *date= [formatter dateFromString:dateStr];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *result= [formatter stringFromDate:date];
    
    return result;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (!isInitialized) {
        isInitialized = YES;
        
        [self showHUDBlockingUI:YES];
        
        switch (self.settingsListType) {
            case kSettingsListBillingHistory:
                [self initBillingHistory];
                break;
            case kSettingsListAvailableSubscriptions:
                [self initAvailableSubscriptions];
                break;
            default:
                break;
        }
    }
}

- (void) initAvailableSubscriptions {

    [SubscriptionM getAllSubscriptionDataFromVirtualMedias:self appendIDs:nil needPurchasingInfo:YES completion:^(NSArray *arrSubscriptionsData) {
        [self hideHUD];

        arrSubscriptionsData = [arrSubscriptionsData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"purchasingInfo != nil"]];
        self.contentList = arrSubscriptionsData;
        
        [self.tableViewContent reloadData];
        self.tableViewContent.hidden = NO;
        
        if (!arrSubscriptionsData || !arrSubscriptionsData.count) {
            self.tableViewContent.tableHeaderView = nil;
            self.viewBlank.hidden = NO;
        }
    } withInApp:NO];
}

- (void) initBillingHistory {
    __weak SettingsListViewController *selfWeak = self;
    __weak TVPAPIRequest *request= [TVPMediaAPI requestForGetUserTransactionHistory:0 numberOfItemsToFetch:100 delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSDictionary *response = [[request JSONResponse] dictionaryByRemovingNSNulls];
        
        if ([response count] == 0) {
            
            selfWeak.tableViewContent.tableHeaderView = nil;
            selfWeak.viewBlank.hidden = NO;
            
        } else {
            NSArray *transactions = [response objectForKey:@"m_Transactions"];
            NSInteger trCount = [[response objectForKey:@"m_nTransactionsCount"] integerValue];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            for (NSInteger i = 0; i < trCount; i++)
            {
                NSDictionary *transaction= [transactions objectAtIndex:i];
                
                TVTransaction *transact = [[TVTransaction alloc] initWithDictionary:transaction];
                
                NSDictionary *price= [transaction objectForKey:@"m_Price"];
                NSString *name= [transaction objectForKey:@"m_sPurchasedItemName"];
                NSString *itemPrice= [NSString stringWithFormat:@"%@ %@", [price objectForKey:@"m_dPrice"], [[price objectForKey:@"m_oCurrency"] objectForKey:@"m_sCurrencySign"]];
                
                // how to know what the type?
                NSString *type= LS(@"transaction_type_unknown");
                
                if (transact.itemType == TVBillTypePPV) {
                    type= LS(@"billType_PPV");
                } else if (transact.itemType == TVBillTypeSubscription) {
                    type= LS(@"billType_Subscription");
                } else if (transact.itemType == TVBillTypePrePaid) {
                    type= LS(@"billType_PrePaid");
                } else if (transact.itemType == TVBillTypePrePaidExpired) {
                    type= LS(@"billType_PrePaidExpired");
                }
                
                NSString *date = @"";
                NSString *date1 = [selfWeak formatDate:[transaction objectForKey:@"m_dtActionDate"]];
                NSString *date2 = [selfWeak formatDate:[transaction objectForKey:@"m_dtEndDate"]];
                
                NSLog(@"%@ - %@", [transaction objectForKey:@"m_dtActionDate"], [transaction objectForKey:@"m_dtEndDate"]);
                
                if (date1) {
                    date = date1;
                }
                if (date2) {
                    if ([date length] > 0) {
                        date = [NSString stringWithFormat:@"%@ - ", date];
                    }
                    date = [NSString stringWithFormat:@"%@%@", date, date2];
                    
                }
                
                NSArray *cellData= [[NSArray alloc] initWithObjects:name, itemPrice, type, date, nil];
                [array addObject:cellData];
                
            }
            
            selfWeak.contentList = array;
            
            selfWeak.viewBlank.hidden = ([array count] > 0);
            if (!selfWeak.viewBlank.hidden) selfWeak.tableViewContent.tableHeaderView = nil;
            
            [selfWeak.tableViewContent reloadData];
            selfWeak.tableViewContent.hidden = NO;
            
        }
        [selfWeak hideHUD];
    }];
    
    [request setFailedBlock:^{
        [selfWeak hideHUD];
    }];
    
    [self sendRequest:request];
}

#pragma mark - Button

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_contentList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat retValue = 0;
    switch (self.settingsListType) {
        case kSettingsListBillingHistory:
            retValue = 52.f;
            break;
        case kSettingsListAvailableSubscriptions:
            retValue = 44.f;
            break;
        default:
            break;
    }
    return retValue;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *retCell = nil;
    switch (self.settingsListType) {
        case kSettingsListBillingHistory:
        {
            static NSString * const cellIdentificator = @"BillingHistoryCell";
            
            BillingHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentificator];
            
            if (!cell) {
                
                cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentificator owner:tableView options:nil] objectAtIndex:(isPhone ? 0 : 1)];
                
            }
            
            cell.backgroundColor = [UIColor clearColor];
            
            NSArray *celldata = [_contentList objectAtIndex:indexPath.row];
            
            [cell customCellWithName:[celldata objectAtIndex:0] price:[celldata objectAtIndex:1] type:[celldata objectAtIndex:2] date:[celldata objectAtIndex:3]];
            
            if (indexPath.row != [tableView numberOfRowsInSection:indexPath.section] - 1) {
                
                [self addBottomSeparatorForCell:cell];
            }
            retCell = cell;

        }
            break;
        case kSettingsListAvailableSubscriptions:
        {
            static NSString * const cellIdentificator = @"TextListCell";
            
            TextListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentificator];
            
            if (!cell) {
                cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentificator owner:tableView options:nil] objectAtIndex:(isPhone ? 0 : 1)];
            }
            
            TVSubscriptionData *dataObject = _contentList[indexPath.row];
            [cell.contentView setBackgroundColor:[UIColor clearColor]];
            [cell setBackgroundColor:[UIColor clearColor]];

            cell.name.text = dataObject.virtualMediaItem.name;
            NSDate *date = dataObject.purchasingInfo.endDate;
            cell.date.text = [date stringFromDateWithDateFormat:@"dd/MM/yyyy"];
            [Utils setLabelFont:cell.date bold:YES];
            
            if (indexPath.row != [tableView numberOfRowsInSection:indexPath.section] - 1)
            {
                [self addBottomSeparatorForCell:cell];
            }
            retCell = cell;
        }
            break;
        default:
            break;
    }

    return retCell;
}

- (void)addBottomSeparatorForCell:(UITableViewCell *)cell {
    
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(20, cell.frame.size.height - 1, cell.frame.size.width - 40, 1)];
    separator.backgroundColor = CS(@"565656");
    [cell addSubview:separator];
}

@end
