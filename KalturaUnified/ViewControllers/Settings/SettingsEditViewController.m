//
//  SettingsEditViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 26.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SettingsEditViewController.h"
#import "EditCell.h"
#import "FooterView.h"
#import "SettingFBUnmargeViewController.h"


@interface SettingsEditViewController ()

@end

@implementation SettingsEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _labelTitle.text = LS(@"screen_title_settings_edit_details");
    [Utils setLabelFont:_labelTitle bold:YES];
    
    if (isPad)
    {
        [Utils setButtonFont:_buttonBack bold:YES];
        [_buttonBack setTitle:LS(@"back") forState:UIControlStateNormal];
    }
    
    [Utils setButtonFont:_btnunmargeFB bold:NO];
    
    [self.btnunmargeFB setTitleColor:CS(@"999999") forState:UIControlStateNormal];

}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (!isInitialized) {
        isInitialized = YES;
        
        TVUser *user = [TVSessionManager sharedTVSessionManager].currentUser;
        
        [self createTableWithFirstName:user.firstName LastName:user.lastName andEmail:user.email];
        
    }
    if (LoginM.isFacebookUser)
    {
        self.btnunmargeFB.hidden = NO;
    }
    else
    {
        self.btnunmargeFB.hidden = YES;
    }

}

- (void)createTableWithFirstName:(NSString *)userFirstName LastName:(NSString *)userLastName andEmail:(NSString *)userEmail {
    
    NSString *name = userFirstName;
    NSString *last = userLastName;
    NSString *email = userEmail;
    NSString *header1 = [LS(@"personal_details") uppercaseString];
    NSString *button1 = LS(@"update_details");
    button1 = LS(@"edit");
    NSArray *arr1 = [[NSArray alloc] initWithObjects:name, last, email, header1, button1, nil];
    
    NSString *old = @"";
    NSString *new = @"";
    NSString *header2 = [LS(@"change_password") uppercaseString];
    NSString *button2 = LS(@"change_password_btn");
    NSArray *arr2 = [[NSArray alloc] initWithObjects:old, new, header2, button2, nil];
    
    NSArray *arr = [[NSArray alloc] initWithObjects:arr1, arr2, nil];
    
    NSString *oldPass = LS(@"old_password");
    NSString *newPass = LS(@"new_password");
    
    NSArray *arr3 = [[NSArray alloc] initWithObjects:oldPass, newPass, nil];
    
    NSString *firstName = LS(@"first_name");
    NSString *lastName = LS(@"last_name");
    NSString *address = LS(@"create_account_email_hint");
    NSArray *arr4 = [[NSArray alloc] initWithObjects:firstName, lastName, address, nil];
    
    NSArray *array = [[NSArray alloc] initWithObjects:arr4, arr3, nil];
    
    BOOL editMode = !LoginM.isFacebookUser;
    
    for (UIView *subview in [viewContentInner subviews]) {
        
        if ([subview isKindOfClass:[UILabel class]]) {
            
            UILabel *label = (UILabel *)subview;
            int section = label.tag % 10;
            NSInteger index = [[arr objectAtIndex:section] count] - 2;
            label.text = [[arr objectAtIndex:section] objectAtIndex:index];
            label.adjustsFontSizeToFitWidth = YES;
            [Utils setLabelFont:label bold:YES];
            
            label.hidden = (section == 1) && !editMode;
            
        } else if ([subview isKindOfClass:[UIImageView class]]) {
            
            NSInteger section = (subview.tag / 20) - 1;
            NSInteger row = subview.tag % 20;
            
            if ((section == 1) && !editMode) {
                
                subview.hidden = YES;
                
            } else {
             
                EditCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"EditCell" owner:self options:nil] objectAtIndex:(isPhone ? 0 : 1)];
                cell.frame = subview.frame;
                cell.tag = subview.tag + 100;
                
                cell.cellLabel.text = [[array objectAtIndex:section] objectAtIndex:row];
                [Utils setLabelFont:cell.cellLabel bold:YES];
                
                cell.textField.text = [[arr objectAtIndex:section] objectAtIndex:row];
                cell.textField.delegate = self;
                if (row == 2) {
                    cell.textField.keyboardType = UIKeyboardTypeEmailAddress;
                }
                if (section == 1) {
                    [cell setPasswordMode];
                } else {
                    cell.textField.enabled = NO;
                }
                cell.textField.tag = cell.tag;
                [Utils setTextFieldFont:cell.textField bold:YES];
                cell.textField.userInteractionEnabled = editMode;
                
                [viewContentInner addSubview:cell];

            }
            
        } else if ([subview isKindOfClass:[UIView class]]) {
            
            int section = subview.tag % 30;
            if(editMode){
                [self addFooterButtonToSection:section view:subview titleArray:arr];
                
            }

        }
        
    }
    
    scrollViewContent.contentSize = viewContentInner.frame.size;
    
}
-(void)addFooterButtonToSection :(int)section view:(UIView *)subview titleArray:(NSArray*)arr{
    
    FooterView *footerView= [[[NSBundle mainBundle] loadNibNamed:@"FooterView" owner:self options:nil] objectAtIndex:(isPhone ? 0 : 1)];
    [footerView.footerButton setTitle:[[arr objectAtIndex:section] lastObject] forState:UIControlStateNormal];
    [Utils setButtonFont:footerView.footerButton bold:YES];
    footerView.backgroundColor = CS(@"303030");
    [footerView.footerButton addTarget:self action:@selector(footerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [footerView.footerButton addTarget:self action:@selector(footerButtonTouchDown:) forControlEvents:UIControlEventTouchDown];
    [footerView.footerButton addTarget:self action:@selector(footerButtonTouchUpOutside:) forControlEvents:UIControlEventTouchUpOutside];
    footerView.footerButton.tag= section;
    
        [subview addSubview:footerView];
}
- (void)updateScrollViewOffset {
    
    if (currentTextField == nil) {
        
        [scrollViewContent setContentOffset:CGPointZero animated:YES];
        
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    currentTextField = nil;
    
    [self performSelector:@selector(updateScrollViewOffset) withObject:nil afterDelay:0.1];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    currentTextField = textField;
    
    if (textField.tag >= 140) {
        
        [scrollViewContent setContentOffset:CGPointMake(0, (isPhone ? 240 : 140)) animated:YES];
        
    }
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    EditCell *cell = nil;
    if (textField.tag == 120) {
        cell = (EditCell *)[viewContentInner viewWithTag:121];
    } else if (textField.tag == 121) {
        cell = (EditCell *)[viewContentInner viewWithTag:122];
    } else if (textField.tag == 140) {
        cell = (EditCell *)[viewContentInner viewWithTag:141];
    }
    
    if (cell) {
        [cell.textField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)footerButtonTapped:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    button.backgroundColor = APST.brandColor;
    
    if (button.tag == 0 && !editUserMode) {
        
        editUserMode = YES;
        [button setTitle:LS(@"update_details") forState:UIControlStateNormal];
        
        EditCell *firstNameCell = (EditCell *)[viewContentInner viewWithTag:120];
        EditCell *lastNameCell = (EditCell *)[viewContentInner viewWithTag:121];
        EditCell *emailCell = (EditCell *)[viewContentInner viewWithTag:122];
        
        firstNameCell.textField.enabled = YES;
        lastNameCell.textField.enabled = YES;
        emailCell.textField.enabled = YES;
        
        [firstNameCell.textField becomeFirstResponder];
        
        return;
        
    }
    
    if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    if (button.tag == 0)
    {
        __block EditCell *firstNameCell = (EditCell *)[viewContentInner viewWithTag:120];
        __block EditCell *lastNameCell = (EditCell *)[viewContentInner viewWithTag:121];
        __block EditCell *emailCell = (EditCell *)[viewContentInner viewWithTag:122];
        
        [firstNameCell.textField resignFirstResponder];
        [lastNameCell.textField resignFirstResponder];
        [emailCell.textField resignFirstResponder];
        
        NSString *firstName = firstNameCell.textField.text;
        NSString *lastName = lastNameCell.textField.text;
        NSString *email = emailCell.textField.text;
        
        
        @try
        {
            
            if (![firstName isEqualToString:@""] && ![lastName isEqualToString:@""])
            {
                if (email.length > 0)
                {
                    if([Utils NSStringIsValidEmail:email])
                    {
                        
                        
                        [TVSessionManager sharedTVSessionManager].currentUser.firstName = firstName;
                        [TVSessionManager sharedTVSessionManager].currentUser.lastName = lastName;
                        [TVSessionManager sharedTVSessionManager].currentUser.email = email;
                        
                        [self showHUDBlockingUI:YES];
                        
                        __weak TVPAPIRequest *request= [TVPSiteAPI requestForSetUserDataWithUser:[TVSessionManager sharedTVSessionManager].currentUser delegate:nil];
                        
                        [request setCompletionBlock:^{
                            
                            [self hideHUD];
                            [self.navigationController popViewControllerAnimated:YES];
                            [[NSNotificationCenter defaultCenter] postNotificationName:KRenameNotification
                                                                                object:nil];
                            
                        }];
                        
                        [request setFailedBlock:^{
                            
                            [self hideHUD];
                            
                        }];
                        
                        [request startAsynchronous];
                        
                        
                    }
                    else
                    {
                        [emailCell.textField becomeFirstResponder];
                        [NSException raise:NSInvalidArgumentException format:LS(@"edit_incorectEmail"), nil];
                    }
                }
                else
                {
                    [emailCell.textField becomeFirstResponder];
                    [NSException raise:NSInvalidArgumentException format:LS(@"edit_EnterEmail"), nil];
                }
            }
            else
            {
                if ([firstName isEqualToString:@""])
                {
                    [firstNameCell.textField becomeFirstResponder];
                    [NSException raise:NSInvalidArgumentException format:LS(@"edit_insetFirstName"), nil];
                }
                else
                {
                    [firstNameCell.textField resignFirstResponder];
                    [NSException raise:NSInvalidArgumentException format: LS(@"edit_insetLastName"), nil];
                }
            }
        }
        @catch (NSException *exception)
        {
            [self presentAlertWithException:exception];
        }
        
        
        if (firstName.length > 0 && lastName.length > 0 && email.length > 0 && [Utils NSStringIsValidEmail:email]) {
            
            
        }
        
    }
    else
    {
        __block EditCell *oldPassCell= (EditCell *)[viewContentInner viewWithTag:140];
        __block EditCell *newPassCell= (EditCell *)[viewContentInner viewWithTag:141];
        
        NSString *oldPass= oldPassCell.textField.text;
        NSString *newPass= newPassCell.textField.text;
        
        [oldPassCell.textField resignFirstResponder];
        [newPassCell.textField resignFirstResponder];
        
        
        if (oldPass.length > 0 && newPass.length > 0) {
            
            [self showHUDBlockingUI:YES];
            
            TVUser *user = [TVSessionManager sharedTVSessionManager].currentUser;
            
            __weak TVPAPIRequest *request = [TVPUsersAPI requestForChangeUserPasswordWithUserName:user.username oldPassword:oldPass newPassword:newPass delegate:nil];
            
            [request setCompletionBlock:^{
                
                [self hideHUD];
                
                oldPassCell.textField.text = @"";
                newPassCell.textField.text = @"";
                
                NSDictionary *userDict = [[request JSONResponse] objectOrNilForKey:@"m_user"];
                NSInteger responseStatus = [[[request JSONResponse] objectOrNilForKey:@"m_RespStatus"] integerValue];
                if (userDict && !responseStatus) {
                    
                    [self presentAlertWithTitle:LS(@"alert_password_successfully_changed") message:nil];
                    
                } else {
                    NSString *message;
                    if (responseStatus) {
                        message = LS(@"edit_OldPasswordIncorrect");
                    } else {
                        message = LS(@"edit_FailedChangePassword");
                    }
                    
                    [self presentAlertWithTitle:message message:nil];
                    
                }

                
            }];
            
            [request setFailedBlock:^{
                
                [self hideHUD];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(@"edit_FailedChangePassword") message:nil delegate:nil cancelButtonTitle:[LS(@"ok") uppercaseString] otherButtonTitles:nil];
                [alert show];
                
            }];
            
            [request startAsynchronous];
            
        }
        
    }
}

- (void)footerButtonTouchDown:(id)sender {
    UIButton *button = (UIButton *)sender;
    button.backgroundColor = APST.brandDarkColor;
}

- (void)footerButtonTouchUpOutside:(id)sender {
    UIButton *button = (UIButton *)sender;
    button.backgroundColor = APST.brandColor;
}


#pragma mark - Button

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)openUnmaregeFBView:(id)sender
{
    
    SettingFBUnmargeViewController *controller = [[ViewControllersFactory defaultFactory] settingFBUnmargeViewController];
    [self.navigationController pushViewController:controller animated:YES];
}
@end
