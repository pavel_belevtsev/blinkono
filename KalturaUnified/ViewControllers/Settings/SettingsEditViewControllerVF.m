//
//  SettingsEditViewControllerVF.m
//  Vodafone
//
//  Created by Brana Fainer on 8/25/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingsEditViewControllerVF.h"
#import "EditCell.h"
#import "FooterView.h"




@interface SettingsEditViewControllerVF ()

@end

@implementation SettingsEditViewControllerVF

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)createTableWithFirstName:(NSString *)userFirstName LastName:(NSString *)userLastName andEmail:(NSString *)userEmail {
    NSString *name = userFirstName;
    NSString *last = userLastName;
    NSString *email = userEmail;
    NSString *header1 = [LS(@"personal_details") uppercaseString];
    NSString *button1 = LS(@"update_details");
    button1 = LS(@"edit");
    NSArray *arr1 = [[NSArray alloc] initWithObjects:name, last, email, header1, button1, nil];
    
    NSString *old = @"";
    NSString *new = @"";
    NSString *header2 = [LS(@"change_password") uppercaseString];
    NSString *button2 = LS(@"change_password_btn");
    NSArray *arr2 = [[NSArray alloc] initWithObjects:old, new, header2, button2, nil];
    
    NSArray *arr = [[NSArray alloc] initWithObjects:arr1, arr2, nil];
    
    NSString *oldPass = LS(@"old_password");
    NSString *newPass = LS(@"new_password");
    
    NSArray *arr3 = [[NSArray alloc] initWithObjects:oldPass, newPass, nil];
    
    NSString *firstName = LS(@"first_name");
    NSString *lastName = LS(@"last_name");
    NSString *address = LS(@"create_account_email_hint");
    NSArray *arr4 = [[NSArray alloc] initWithObjects:firstName, lastName, address, nil];
    
    NSArray *array = [[NSArray alloc] initWithObjects:arr4, arr3, nil];
    
    BOOL editMode = !LoginM.isFacebookUser;
    
    for (UIView *subview in [viewContentInner subviews]) {
        
        if ([subview isKindOfClass:[UILabel class]]) {
            
            UILabel *label = (UILabel *)subview;
            int section = label.tag % 10;
            NSInteger index = [[arr objectAtIndex:section] count] - 2;
            label.text = [[arr objectAtIndex:section] objectAtIndex:index];
            label.adjustsFontSizeToFitWidth = YES;
            [Utils setLabelFont:label bold:YES];
            
            label.hidden = (section == 1) && !editMode;
            
        } else if ([subview isKindOfClass:[UIImageView class]]) {
            
            NSInteger section = (subview.tag / 20) - 1;
            NSInteger row = subview.tag % 20;
            
            if ((section == 1) && !editMode) {
                
                subview.hidden = YES;
                
            } else {
                
                EditCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"EditCell" owner:self options:nil] objectAtIndex:(isPhone ? 0 : 1)];
                cell.frame = subview.frame;
                cell.tag = subview.tag + 100;
                
                cell.cellLabel.text = [[array objectAtIndex:section] objectAtIndex:row];
                [Utils setLabelFont:cell.cellLabel bold:YES];
                
                cell.textField.text = [[arr objectAtIndex:section] objectAtIndex:row];
                cell.textField.delegate = self;
                if (row == 2) {
                    cell.textField.keyboardType = UIKeyboardTypeEmailAddress;
                }
                if (section == 1) {
                    [cell setPasswordMode];
                } else {
                    cell.textField.enabled = NO;
                }
                cell.textField.tag = cell.tag;
                [Utils setTextFieldFont:cell.textField bold:YES];
                cell.textField.userInteractionEnabled = editMode;
                
                [viewContentInner addSubview:cell];
                
            }
            
        } else if ([subview isKindOfClass:[UIView class]]) {
            
            int section = subview.tag % 30;
            
            if (editMode && section!=0) {
                [self addFooterButtonToSection:section view:subview titleArray:arr];

            }
            
        }
        
    }
    
    scrollViewContent.contentSize = viewContentInner.frame.size;
    

}

/* "PSC : ONO : SYN-4043 VFGTVONES-260 - iPad/Settings/ No error message in change password button */
- (void)footerButtonTapped:(id)sender {
    UIButton *button = (UIButton *)sender;
    if (button.tag != 0) {
        button.backgroundColor = APST.brandColor;
        if (LoginM.internetReachability.currentReachabilityStatus == NotReachable){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"alert_no_internet_connection") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
            [alertView show];
            return;
        }
        
        __block EditCell *oldPassCell= (EditCell *)[viewContentInner viewWithTag:140];
        __block EditCell *newPassCell= (EditCell *)[viewContentInner viewWithTag:141];
        
        NSString *oldPass= oldPassCell.textField.text;
        NSString *newPass= newPassCell.textField.text;
        
        [oldPassCell.textField resignFirstResponder];
        [newPassCell.textField resignFirstResponder];
        
        
        if (oldPass.length > 0 && newPass.length >= 6 && newPass.length < 16) {
            
            [self showHUDBlockingUI:YES];
            
            TVUser *user = [TVSessionManager sharedTVSessionManager].currentUser;
            
            __weak TVPAPIRequest *request = [TVPUsersAPI requestForChangeUserPasswordWithUserName:user.username oldPassword:oldPass newPassword:newPass delegate:nil];
            
            [request setCompletionBlock:^{
                
                [self hideHUD];
                
                oldPassCell.textField.text = @"";
                newPassCell.textField.text = @"";
                
                NSDictionary *userDict = [[request JSONResponse] objectOrNilForKey:@"m_user"];
                NSInteger responseStatus = [[[request JSONResponse] objectOrNilForKey:@"m_RespStatus"] integerValue];
                if (userDict && !responseStatus) {
                    
                    [self presentAlertWithTitle:LS(@"alert_password_successfully_changed") message:nil];
                    
                } else {
                    NSString *message;
                    if (responseStatus) {
                        message = LS(@"edit_OldPasswordIncorrect");
                    } else {
                        message = LS(@"edit_FailedChangePassword");
                    }
                    
                    [self presentAlertWithTitle:message message:nil];
                    
                }
                
                
            }];
            
            [request setFailedBlock:^{
                
                [self hideHUD];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(@"edit_FailedChangePassword") message:nil delegate:nil cancelButtonTitle:[LS(@"ok") uppercaseString] otherButtonTitles:nil];
                [alert show];
                
            }];
            
            [request startAsynchronous];
            
        } else if (oldPass.length == 0 || newPass.length == 0){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(@"create_account_new_password_error_empty") message:nil delegate:nil cancelButtonTitle:[LS(@"ok") uppercaseString] otherButtonTitles:nil];
            [alert show];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(@"create_account_new_password_error_length") message:nil delegate:nil cancelButtonTitle:[LS(@"ok") uppercaseString] otherButtonTitles:nil];
            [alert show];
        }
    } else {
        [super footerButtonTapped:sender];
    }
}

@end
