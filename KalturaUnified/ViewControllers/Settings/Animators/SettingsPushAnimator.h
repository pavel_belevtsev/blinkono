//
//  SettingsPushAnimator.h
//  KalturaUnified
//
//  Created by Dmytro Rozumeyenko on 1/23/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingsPushAnimator : NSObject<UIViewControllerAnimatedTransitioning>


@end
