//
//  SettingsPushAnimator.m
//  KalturaUnified
//
//  Created by Dmytro Rozumeyenko on 1/23/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingsPushAnimator.h"

@implementation SettingsPushAnimator

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return kViewNavigationDuration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    [[transitionContext containerView] addSubview:toViewController.view];
    toViewController.view.frame = CGRectMake(toViewController.view.frame.size.width, toViewController.view.frame.origin.y, toViewController.view.frame.size.width, toViewController.view.frame.size.height);
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        fromViewController.view.frame = CGRectMake(-fromViewController.view.frame.size.width, fromViewController.view.frame.origin.y,  fromViewController.view.frame.size.width, fromViewController.view.frame.size.height);
        toViewController.view.frame = CGRectMake(0, toViewController.view.frame.origin.y, toViewController.view.frame.size.width, toViewController.view.frame.size.height);

    } completion:^(BOOL finished) {
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
}

@end
