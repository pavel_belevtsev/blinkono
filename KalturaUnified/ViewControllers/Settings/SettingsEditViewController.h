//
//  SettingsEditViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 26.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsEditViewController : BaseViewController <UITextFieldDelegate> {
    
    IBOutlet UIView *viewContentInner;
    IBOutlet UIScrollView *scrollViewContent;
    
    UITextField *currentTextField;
    
    BOOL isInitialized;
    BOOL editUserMode;
    
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
@property (weak, nonatomic) IBOutlet UIButton *btnunmargeFB;
- (IBAction)openUnmaregeFBView:(id)sender;

/* "PSC : ONO :  OPF-1260 VFGTVONES-150 - iPhone/Información Personal/Editing the email address is possible*/
//- (void)createTableWithFirstName:(NSString *)userFirstName LastName:(NSString *)userLastName andEmail:(NSString *)userEmail;
-(void)addFooterButtonToSection :(int)section view:(UIView *)subview titleArray:(NSArray*)arr;
- (void)footerButtonTapped:(id)sender;


@end
