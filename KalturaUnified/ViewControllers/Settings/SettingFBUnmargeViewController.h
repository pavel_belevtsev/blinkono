//
//  MyZoneFBUnmargeViewController.h
//  KalturaUnified
//
//  Created by Israel Berezin on 1/5/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SettingFBUnmargeViewController : LoginBaseViewController

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelFirstDiscriptiod;
@property (weak, nonatomic) IBOutlet UILabel *labelSecDiscriptiod;
@property (weak, nonatomic) IBOutlet UIButton *buttonDisconnectFacebook;
@property (weak, nonatomic) IBOutlet UIButton *buttonForgotPassword;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewContent;
@property (weak, nonatomic)IBOutlet UIView *viewContentInner;

@property (weak, nonatomic) IBOutlet UITableView *unmaegeSettingtableView;

@end
