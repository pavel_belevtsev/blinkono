//
//  SettingsPrivacyViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsPrivacyViewController : BaseViewController {
    
    BOOL isInitialized;
    
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableViewContent;

@property (strong, nonatomic) IBOutlet UIView *viewHeaderActivity;
@property (strong, nonatomic) IBOutlet UIView *viewHeaderFacebook;

@property (strong, nonatomic) NSArray *contentList;

@property BOOL isLikeInternal;
@property BOOL isWatchesInternal;
@property BOOL isLikeExternal;
@property BOOL isWatchesExternal;
@property NSInteger requestsCount;

@end
