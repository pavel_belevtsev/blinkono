//
//  SettingsListViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 25.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

enum {
    kSettingsListBillingHistory,
    kSettingsListAvailableSubscriptions
};
typedef NSInteger SettingsListType;

@interface SettingsListViewController : BaseViewController {
    
    BOOL isInitialized;

}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableViewContent;
@property (weak, nonatomic) IBOutlet UIView *viewBlank;
@property (weak, nonatomic) IBOutlet UILabel *labelBlank;

@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (strong, nonatomic) IBOutlet UIView *viewHeader;

@property (strong, nonatomic) NSArray *contentList;

@property (nonatomic, readwrite) NSInteger settingsListType;

@end
