//
//  SettingsPrivacyViewControllerVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 2/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingsPrivacyViewControllerVF.h"

@interface SettingsPrivacyViewControllerVF ()

@end

@implementation SettingsPrivacyViewControllerVF

/* "PSC : Vodafone Grup : change Location to GroupedStyleTableView  : JIRA ticket. */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(isPad){
        // according to VF rebranding all the labels should be aligned with teh tables in this VC:
        [self normelizeAllLabels:self.viewHeaderFacebook];
        [self normelizeAllLabels:self.viewHeaderActivity];
    }
}

- (void) normelizeAllLabels:(UIView*) view
{
    
    for (UIView* subView  in [view subviews]) {
        if ([subView isKindOfClass:[UILabel class]]) {
            // we found a label - make sure the x value in the frame is zero
            CGRect frame =  subView.frame;
            frame.origin.x = 0;
            subView.frame = frame;
        }
    }
}



@end
