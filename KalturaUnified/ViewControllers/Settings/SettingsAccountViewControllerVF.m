//
//  SettingsAccountViewControllerVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 2/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingsAccountViewControllerVF.h"
#import "GroupedStyleTableView.h"
#import "SettingsCellVF.h"


@interface SettingsAccountViewControllerVF ()

@end

@implementation SettingsAccountViewControllerVF

/* "PSC : Vodafone Grup : change Location to GroupedStyleTableView  : JIRA ticket. */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // table view should have margins on left/right:
    float xDelta = 15.0;
    CGRect frame = self.tableViewContent.frame;
    frame.size.width -= xDelta*2;
    frame.origin.x = xDelta;;
    frame.origin.y += 15.0;;
    self.tableViewContent.frame = frame;
    
    GroupedStyleTableView* table = (GroupedStyleTableView*)self.tableViewContent;
    table.cornerRadius = 8.0;
    
    self.viewContent.backgroundColor = [VFTypography instance].darkGrayColor;
    self.tableViewContent.backgroundColor = [UIColor clearColor];
}

/* "PSC : Vodafone Grup : change Location to GroupedStyleTableView  : JIRA ticket. */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsCellVF *cell =  (SettingsCell *)[super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    cell.sepretaorView.hidden = YES;

    return cell;
    
}
@end
