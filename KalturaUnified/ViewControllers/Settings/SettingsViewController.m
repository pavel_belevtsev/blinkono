//
//  SettingsViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsCell.h"
#import "SettingsAccountViewController.h"
#import "SettingsPrivacyViewController.h"
#import "SettingsAboutViewController.h"
#import "SettingDownloadsViewController.h"

#import "AnalyticsManager.h"

@interface SettingsViewController ()

@property UINavigationController  *currentDetailViewController;

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    self.loadNoInternetView = YES;

    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [Utils setLabelFont:_labelTitle bold:YES];
    _labelTitle.text = LS(@"screen_title_settings");
    
    [self updateContentList];
    
    _tableViewContent.tableFooterView = [[UIView alloc] init];
    
    selectedRow = -1;
}

-(void)updateContentList
{
    NSMutableArray * arr = [NSMutableArray arrayWithArray:@[[SettingsObject createWithName:LS(@"settings_account") type:SettingsTypeAccount],
                                                            [SettingsObject createWithName:LS(@"settings_privacy") type:SettingsTypePrivacy],
                                                            [SettingsObject createWithName:LS(@"settings_about") type:SettingsTypeAbout]]];
    
    if ([NU(UIUnit_Settings_Downloads) isUIUnitExist])
    {
        [arr insertObject: [SettingsObject createWithName:LS(@"settings_downloads") type:SettingsTypeDownloads] atIndex:2];
    }
    self.contentList = arr;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    super.panModeDefault = MFSideMenuPanModeDefault;
    [self menuContainerViewController].panMode = (MFSideMenuPanMode)super.panModeDefault;
    
    [self menuContainerViewController].rightMenuViewController = nil;
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (isPad && (selectedRow < 0)) {
        selectedRow = 0;
        
        [_tableViewContent selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
        
        [self showAccountScreen];
        
    }
}

#pragma mark - Button

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{}];
    
}


#pragma mark - TableView dataSource implementation

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_contentList count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * const cellIdentificator = @"SettingsCell";
    
    SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentificator];
    
    if (!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentificator owner:tableView options:nil] objectAtIndex:0];
    }
    
    cell.textLabel.textColor = [UIColor lightGrayColor];
    SettingsObject * obj = _contentList[indexPath.row];
    if (obj)
    {
        cell.textLabel.text = obj.name;
        cell.settingCellType = obj.type;
    }
    [Utils setLabelFont:cell.textLabel size:16 bold:YES];
    
    if (isPhone) {
        UIImageView *accesoryImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"settings_accesory_icon"]];
        accesoryImage.frame = CGRectMake((cell.frame.size.width - accesoryImage.frame.size.width) - 5.f, (cell.frame.size.height - accesoryImage.frame.size.height) / 2.0, accesoryImage.frame.size.width, accesoryImage.frame.size.height);
        [cell addSubview:accesoryImage];
    }
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (isPhone)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    SettingsCell *cell = (SettingsCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    SettingsType selectedIndex = (int)cell.settingCellType;
   
    BaseViewController * selectedViewContorller = nil;
    
    switch (selectedIndex)
    {
        case SettingsTypeAccount:
        {
            selectedViewContorller = [[ViewControllersFactory defaultFactory] settingsAccountViewController];
            NSArray *components = @[LS(@"settings_account"), LS(@"settings_account")];
            [AnalyticsM trackScreen:[components componentsJoinedByString:@"->"]];
        }
            break;
            
        case SettingsTypePrivacy:
        {
            selectedViewContorller = [[ViewControllersFactory defaultFactory] settingsPrivacyViewController];
            NSArray *components = @[LS(@"settings_account"), LS(@"settings_privacy")];
            [AnalyticsM trackScreen:[components componentsJoinedByString:@"->"]];
        }
            break;
        case SettingsTypeDownloads:
        {
            selectedViewContorller = [[ViewControllersFactory defaultFactory] settingDownloadsViewController];
            NSArray *components = @[LS(@"settings_account"), LS(@"settings_downloads")];
            [AnalyticsM trackScreen:[components componentsJoinedByString:@"->"]];

        }
            break;
        case SettingsTypeAbout:
        {
            selectedViewContorller = [[ViewControllersFactory defaultFactory] settingsAboutViewController];
            NSArray *components = @[LS(@"settings_account"), LS(@"settings_about")];
            [AnalyticsM trackScreen:[components componentsJoinedByString:@"->"]];
        }
            break;
        default:
            break;
    }
    
    if (isPad)
    {
        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:selectedViewContorller];
        navigation.navigationBarHidden = YES;
        [self presentDetailController:navigation];
    }
    else
    {
        [self.navigationController pushViewController:selectedViewContorller animated:YES];
    }
}

- (void)showAccountScreen
{
    SettingsAccountViewController *controller = [[ViewControllersFactory defaultFactory] settingsAccountViewController];
    if (isPad)
    {
        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:controller];
        navigation.navigationBarHidden = YES;
        [self presentDetailController:navigation];
    }
    else
    {
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - Navigation

- (void)presentDetailController:(UINavigationController*)detailVC {
    
    if (self.currentDetailViewController){
        [self removeCurrentDetailViewController];
    }
    
    [self addChildViewController:detailVC];
    
    detailVC.view.frame = [self frameForDetailController];
    
    [self.detailView addSubview:detailVC.view];
    self.currentDetailViewController = detailVC;
    
    [detailVC didMoveToParentViewController:self];
    
}

- (void)removeCurrentDetailViewController {
    
    [self.currentDetailViewController willMoveToParentViewController:nil];
    [self.currentDetailViewController.view removeFromSuperview];
    [self.currentDetailViewController removeFromParentViewController];
    
}

- (CGRect)frameForDetailController {
    
    CGRect detailFrame = self.detailView.bounds;
    return detailFrame;
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

@implementation SettingsObject

- (instancetype)initWithName:(NSString*) name type:(NSInteger) type
{
    self = [super init];
    if (self) {
        _name = name;
        _type = type;
    }
    return self;
}

+(SettingsObject*) createWithName:(NSString*) name type:(NSInteger) type {
    return [[SettingsObject alloc] initWithName:name type:type];
}

- (id)copyWithZone:(NSZone *)zone {
    SettingsObject *obj = [[self class] allocWithZone:zone];
    obj.name = [_name copyWithZone:zone];
    obj.type = _type;
    return obj;
}

@end
