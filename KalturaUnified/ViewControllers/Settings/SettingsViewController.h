//
//  SettingsViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeNavProfileView.h"


typedef enum
{
    SettingsTypeAccount,
    SettingsTypePrivacy,
    SettingsTypeDownloads,
    SettingsTypeAbout
}
SettingsType;


@interface SettingsViewController : BaseViewController <HomeNavProfileViewDelegate, UITableViewDataSource> {
    int selectedRow;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableViewContent;
@property (strong, nonatomic) NSArray *contentList;
@property (weak, nonatomic) IBOutlet UIView *detailView;

@end


@interface SettingsObject : NSObject
@property (nonatomic, retain) NSString *name;
@property (nonatomic, readwrite) NSInteger type;

+(SettingsObject*) createWithName:(NSString*) name type:(NSInteger) type;

/* "PSC : ONO :  OPF-857 - settings - error appears in privacy and the app is stuck */
-(void)updateContentList;

@end