//
//  SettingsAboutViewControllerVF.h
//  Vodafone
//
//  Created by Aviv Alluf on 2/5/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingsAboutViewController.h"

@interface SettingsAboutViewControllerVF : SettingsAboutViewController

@end
