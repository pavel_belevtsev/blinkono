//
//  MyZoneFBUnmargeViewController.m
//  KalturaUnified
//
//  Created by Israel Berezin on 1/5/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingFBUnmargeViewController.h"
#import "EditTableCell.h"
#import "SettingsAccountViewController.h"
#import "SocialManagement.h"

#define NumberOfCellsInTableView 2

@interface SettingFBUnmargeViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic) NSArray * titelsArr;
@property (strong, nonatomic) NSArray * dataArr;

@end

@implementation SettingFBUnmargeViewController

#pragma mark - Memeory -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [self unregisterToKeyborardNotifications];
}

#pragma mark - Life -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.socialManager = [SocialManagement new];
    self.socialManager.delegateForProvider = self;
  
    [self updateUI];
    
    [self updateData];
    
    [self registerToKeyborardNotifications];
    
    [self.unmaegeSettingtableView reloadData];
}

-(void)updateData
{
    TVUser *myUser = [TVSessionManager sharedTVSessionManager].currentUser;
    NSString *userEmail = myUser.username;
    NSString *userPass = @"";
    self.dataArr = [[NSArray alloc] initWithObjects:userEmail, userPass, nil];
    
    NSString *user = LS(@"user");
    NSString *pass = LS(@"password");
    self.titelsArr = [[NSArray alloc] initWithObjects:user, pass, nil];
}

-(void)updateUI
{
    self.labelTitle.text =LS(@"facebook_unmerge_title");
    
    [self.buttonDisconnectFacebook setTitle:LS(@"facebook_unmerge_title") forState:UIControlStateNormal];
    [self.buttonForgotPassword setTitle:LS(@"facebook_unmerge_forgot_password_link") forState:UIControlStateNormal];
    self.buttonDisconnectFacebook.backgroundColor = APST.brandColor;
    self.buttonDisconnectFacebook.layer.cornerRadius = 5.0f;
    
    [Utils setButtonFont:self.buttonDisconnectFacebook bold:YES];
    [Utils setButtonFont:self.buttonForgotPassword bold:YES];
    
    [Utils setLabelFont: self.labelTitle bold:YES];
    [Utils setLabelFont: self.labelFirstDiscriptiod bold:NO];
    [Utils setLabelFont: self.labelSecDiscriptiod bold:NO];
    
    [self.buttonForgotPassword setTitleColor:CS(@"999999") forState:UIControlStateNormal];
    
    self.labelFirstDiscriptiod.text = LS(@"disconnectFacebook_FirstDiscriptiod");
    self.labelSecDiscriptiod.text = LS(@"disconnectFacebook_SecDiscriptiod");
}


#pragma mark - Unmerge  -

-(IBAction)FBUserUnmerge:(id)sender
{
    EditTableCell *userCell = (EditTableCell *)[self.unmaegeSettingtableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];;
    EditTableCell *passCell = (EditTableCell *)[self.unmaegeSettingtableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];;

    NSString * name = userCell.textField.text;
    NSString * pass = passCell.textField.text;
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForGetSiteGuidWithUserName:name password:pass delegate:nil];
    
    [request setStartedBlock:^{
        [self showHUDBlockingUI:YES];
    }];
    
    [request setFailedBlock:^{
         [self hideHUD];
    }];
    
    [request setCompletionBlock:^{
        
        id jsonResponse = [request JSONResponse];
        if ([jsonResponse intValue] > 0)
        {
            [[super.socialManager socialProviderById:SocialProviderTypeFacebook] aquirePermissions:^(BOOL success) {
                if (success){
                    [FBM requestForGetFBUserDataWithToken:^(TVFacebookUser *user) {
                        [self unmergeWithFacebookUser:user];
                    }];
                }
                else {
                    [self hideHUD];
                }
            }];
        }
        else
        {
            [self hideHUD];
            [self presentAlertWithTitle:LS(@"disconnectFacebook_email_or_password_incorrect") message:nil];
        }
    }];
    
    [self sendRequest:request];
}

-(void)returnToAccountViewController
{
    UIViewController * accountViewController = nil;
    
    for (UIViewController * tempview in [self.navigationController viewControllers]) {
        
        if ([tempview  isKindOfClass:[SettingsAccountViewController class]])
        {
            accountViewController = tempview;
            break;
        }
    }
    if (accountViewController)
    {
        [self.navigationController popToViewController:accountViewController animated:YES];
    }
    
}

#pragma mark - Action -

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - FBManagementDelegate -

- (void)unmergeWithFacebookUser:(TVFacebookUser *)user
{
    NSString *facebookToken = [[FBSession.activeSession accessTokenData] accessToken];

    __weak TVPAPIRequest *request = [TVPSiteAPI requestForFBUserUnmergeWithToken:facebookToken username:[user.facebookUser objectForKey:@"email"] password:user.data delegate:nil];
    
    [request setFailedBlock:^{
        [self hideHUD];
        [[SocialManagement socialProviderClassById:SocialProviderTypeFacebook] logout];
    }];
    
    [request setCompletionBlock:^{
        
        [[SocialManagement socialProviderClassById:SocialProviderTypeFacebook] logout];
        LoginM.facebookScope = nil;
        
        ASLogInfo(@"req = %@",request);
        
        [[TVSessionManager sharedTVSessionManager] getCurrentUserDetailsWithStartBlock:^{
            
        } failedBlock:^{
            
            [self hideHUD];
            
        } completionBlock:^{
            
            [super.userManager loadDomainUsers:^{
                [self hideHUD];
                [[NSNotificationCenter defaultCenter] postNotificationName:KRenameNotification object:nil];
                [self returnToAccountViewController];
            }];
        }];
    }];
    
    [self sendRequest:request];
}

#pragma mark - forgot password -

- (IBAction)buttonSendPressed:(UIButton *)button {
    

    [self closeKeyboard];
    
    NSString * userName = [[[TVSessionManager sharedTVSessionManager] currentUser] username];
            
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForSendNewPassword:userName delegate:nil];
    
    [request setCompletionBlock:^{
        
        if ([[request JSONResponse] intValue] == 0) {
            
            [self presentAlertWithTitle:LS(@"forgot_pass_wrong_username_error") message:nil];
            
        }
        else
        {
            [self presentAlertWithTitle:LS(@"forgot_pass_email_sent_successfully") message:nil];
        }
    }];
    
    [request setFailedBlock:^{
        
    }];
    
    [self sendRequest:request];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return NumberOfCellsInTableView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"EditTableCell";
    
    EditTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [EditTableCell editTableCell];
    }
    
    if (indexPath.row == 0)
    {
        if (isPad)
        {
            cell.bgImage.image = [UIImage imageNamed:@"settings_text_field_top"]; //
        }
        else
        {
            cell.bgImage.image = [UIImage imageNamed:@"settings_cell_top"]; //
        }
    }
    else if (indexPath.row == (NumberOfCellsInTableView-1))
    {
        if (isPad)
        {
            cell.bgImage.image = [UIImage imageNamed:@"settings_text_field_bottom"];
        }
        else
        {
            cell.bgImage.image = [UIImage imageNamed:@"settings_cell_bottom"];
        }
    }
    else
    {
        if (isPad)
        {
            cell.bgImage.image = [UIImage imageNamed:@"settings_text_field_middle"];
        }
        else
        {
            cell.bgImage.image = [UIImage imageNamed:@"settings_cell_middle"];
        }
    }
    
    cell.cellLabel.text = [self.titelsArr objectAtIndex:indexPath.row];
    cell.textField.text = [self.dataArr objectAtIndex:indexPath.row];
    cell.textField.userInteractionEnabled = FALSE;
    cell.textField.textColor = CS(@"999999");
    cell.textField.tag = indexPath.row;
    cell.textField.delegate = self;
    
    if (indexPath.row == 1) {
        [cell setPasswordMode];
    }
    return cell;
}


#pragma mark - TextField delegate -

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    EditTableCell *cell = nil;
    if (textField.tag == 0)
    {
        cell = (EditTableCell *)[self.unmaegeSettingtableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    }
    if (cell)
    {
        [cell.textField becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}


#pragma mark - Keyborard Notifications -

- (void)registerToKeyborardNotifications
{
    [self unregisterToKeyborardNotifications];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)unregisterToKeyborardNotifications
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)keyboardWillShow
{
    [self.scrollViewContent setContentOffset:CGPointMake(0, 110) animated:YES];
}

-(void)keyboardWillHide
{
    [self.scrollViewContent setContentOffset:CGPointZero animated:YES];
}

@end
