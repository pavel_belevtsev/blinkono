//
//  SettingDownloadsQuotaViewControllerPhone.m
//  KalturaUnified
//
//  Created by Israel Berezin on 5/31/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingDownloadsQuotaViewControllerPhone.h"
#import "SettingDownloadsCell.h"

@interface SettingDownloadsQuotaViewControllerPhone ()<UITableViewDelegate,UITableViewDataSource>

@property NSInteger topHeaderHigh;
@property NSInteger foteerHeaderHigh;

@end

@implementation SettingDownloadsQuotaViewControllerPhone

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableViewContent.delegate = self;
    self.tableViewContent.dataSource = self;
    
    self.headerlabelTitle.text = LS(@"dtgs_Quota_page_heder_title");
    [Utils setLabelFont:self.headerlabelTitle bold:YES];
    
    self.topHeaderHigh = self.viewHeader.frame.size.height;
    self.foteerHeaderHigh = self.viewFotter.frame.size.height;
    
    [self.viewHeader removeFromSuperview];
    [self.viewFotter removeFromSuperview];

    self.selectedIndex = 0;
    [self updateContentList];
}

-(void)updateContentList
{
    self.contentList = [NSArray arrayWithObjects:LS(@"5%"), LS(@"10%"), LS(@"20%"),(@"30%"), LS(@"40%"), LS(@"50%"),LS(@"60%"),nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contentList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return  self.topHeaderHigh;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return _viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return self.foteerHeaderHigh;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return _viewFotter;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * const st_SettingDownloadsCell = @"SettingDownloadsCell";
    
    SettingDownloadsCell *cell = [tableView dequeueReusableCellWithIdentifier:st_SettingDownloadsCell];
    if (!cell)
    {
        cell = [SettingDownloadsCell settingDownloadsCell];
    }
    
    [self updateDownloadsCellUI:cell withIndexPath:indexPath];
    
    return cell;
}

-(void)updateDownloadsCellUI:(SettingDownloadsCell *)cell withIndexPath:(NSIndexPath*)indexPath
{
   
    cell.backgroundColor = CS(@"232323");
    cell.imgBg.hidden = YES;
  
    cell.rightLabel.hidden = YES;
    cell.viewSeparator.hidden = (indexPath.row == ([self.contentList count]-1));
    
    if (indexPath.row == self.selectedIndex)
    {
        cell.checkMarkImage.hidden = NO;
    }
    else
    {
        cell.checkMarkImage.hidden = YES;
    }
    
    cell.leftLabel.text = [self.contentList objectAtIndex:indexPath.row];
    
    cell.leftLabel.textColor = [UIColor lightGrayColor];
    [Utils setLabelFont:cell.leftLabel size:16 bold:YES];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != self.selectedIndex)
    {
        SettingDownloadsCell * oldCell = (SettingDownloadsCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0]];
        SettingDownloadsCell * newCell = (SettingDownloadsCell *)[tableView cellForRowAtIndexPath:indexPath];
        oldCell.checkMarkImage.hidden = YES;
        newCell.checkMarkImage.hidden = NO;
        self.selectedIndex = indexPath.row;
    }
}




@end
