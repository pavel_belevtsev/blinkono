//
//  SettingDownloadsViewController.m
//  KalturaUnified
//
//  Created by Israel Berezin on 5/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingDownloadsViewController.h"
#import "SliderCell.h"
#import "SettingDownloadsCell.h"
#import "SettingDownloadsQualityViewController.h"
#import "SettingDownloadsQuotaViewController.h"
#import "UIDevice+Network.h"

typedef enum
{
    SettingsDownloadCellular,
    SettingsDownloadAutoResume,
    SettingsDownloadQuota,
    SettingsDownloadQuality
}
SettingsDownloadType;

@interface SettingDownloadsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property NSInteger topHeaderHigh;
@property NSInteger secHeaderHigh;

@end

@implementation SettingDownloadsViewController

#define kSwitchTagStartFrom 1000
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableViewContent.delegate = self;
    self.tableViewContent.dataSource = self;
    
    [Utils setLabelFont:_labelTitle bold:YES];
    _labelTitle.text = LS(@"dtgs_dp_main_titel_downloads");
    
    self.headerNetworkAndStoragelabelTitle.text = [LS(@"dtgs_dp_titel_network_and_storage") uppercaseString];
    [Utils setLabelFont:self.headerNetworkAndStoragelabelTitle bold:YES];
    
    self.audiolabelTitle.text = LS(@"dtgs_dp_audio");
    [Utils setLabelFont:self.audiolabelTitle bold:NO];
    
    self.topHeaderHigh = _viewHeaderNetworkAndStorage.frame.size.height;
    self.secHeaderHigh = _viewHeaderAudio.frame.size.height;
    
    [_viewHeaderNetworkAndStorage removeFromSuperview];
    [_viewHeaderAudio removeFromSuperview];
    
    [self updateContentList];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[DTGManager sharedInstance] getAllSettings:^(NSMutableDictionary *dict)
     {
         self.settingsDefaultValues = [NSDictionary dictionaryWithDictionary:dict];
         [self.tableViewContent reloadData];
     }];
}

-(void)updateContentList
{
    NSString *autoResumeTitle = (isPad) ? @"dtps_dp_auto_resume_downloads_tablet" : @"dtps_dp_auto_resume_downloads_smartphone";
    self.contentList = [NSMutableArray arrayWithObjects:[SettingsObject createWithName:LS(autoResumeTitle) type:SettingsDownloadAutoResume],
                                                        [SettingsObject createWithName:LS(@"dtps_dp_download_quota") type:SettingsDownloadQuota],
                                                        [SettingsObject createWithName:LS(@"dtps_dp_quality") type:SettingsDownloadQuality],
                                                        nil];
    
    if ([[UIDevice currentDevice] supportsCellularNetwork]) {
        [self.contentList insertObject:[SettingsObject createWithName:LS(@"dtps_dp_download_on_cell_data") type:SettingsDownloadCellular] atIndex:0];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [_contentList count];
    }
    else {
        return 1;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return  self.topHeaderHigh;
    }
    else {
        return self.secHeaderHigh;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return _viewHeaderNetworkAndStorage;
    }
    else {
        return _viewHeaderAudio;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * const st_SettingDownloadsCell = @"SettingDownloadsCell";
    static NSString * const st_cellId = @"SliderCell";

    UITableViewCell *cell = nil;
    SettingsObject *obj = [_contentList objectAtIndex:indexPath.row];
    switch (obj.type) {
        case SettingsDownloadAutoResume:
        case SettingsDownloadCellular: {
            cell = [tableView dequeueReusableCellWithIdentifier:st_cellId];
            
            if (!cell) {
                cell = [[[NSBundle mainBundle] loadNibNamed:st_cellId owner:tableView options:nil] objectAtIndex:(isPhone ? 0 : 1)];
            }
            
            [self updateSliderCellUI:(SliderCell *)cell withIndexPath:indexPath];
        }
            break;
        default: {
            cell = [tableView dequeueReusableCellWithIdentifier:st_SettingDownloadsCell];
            if (!cell) {
                cell = [SettingDownloadsCell settingDownloadsCell];
            }
            
            [self updateDownloadsCellUI:(SettingDownloadsCell*)cell withIndexPath:indexPath];
        }
            break;
    }
    return cell;
}

-(void)updateSliderCellUI:(SliderCell *)cell withIndexPath:(NSIndexPath*)indexPath
{
    SettingsObject *obj = [_contentList objectAtIndex:indexPath.row];
    cell.label.text = obj.name;
    
    [cell.slider addTarget:self action:@selector(changeState:) forControlEvents:UIControlEventValueChanged];
    
    switch (obj.type) {
        case SettingsDownloadAutoResume: {
            BOOL isOn = ([[self.settingsDefaultValues objectForKey:[KADownloadSettings autoResumeDownloads]]  isEqual: @"true"]);
            [cell.slider setOn:isOn];
            cell.slider.tag = kSwitchTagStartFrom + obj.type;
        }
            break;
        case SettingsDownloadCellular: {
            BOOL isOn = ([[self.settingsDefaultValues objectForKey:[KADownloadSettings enableDownloadOnCellularData]]  isEqual: @"true"]);
            [cell.slider setOn:isOn];
            cell.slider.tag = kSwitchTagStartFrom + obj.type;
        }
            break;
        default:
            break;
    }
    
    if (isPhone) {
        cell.backgroundColor = CS(@"232323");
    }
    else {
        cell.backgroundColor = [UIColor clearColor];
        cell.imgBg.image = [UIImage imageNamed:((indexPath.row == 0) ? @"settings_list_item_top" : @"settings_list_item_middle")];
    }
    cell.viewSeparator.hidden = NO;

    cell.label.textColor = [UIColor lightGrayColor];
    [Utils setLabelFont:cell.label size:16 bold:YES];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)updateDownloadsCellUI:(SettingDownloadsCell *)cell withIndexPath:(NSIndexPath*)indexPath
{
    if (isPhone) {
        cell.backgroundColor = CS(@"232323");
        cell.imgBg.hidden = YES;
    }
    else {
        cell.imgBg.hidden = NO;
        cell.backgroundColor = [UIColor clearColor];
        
        if (indexPath.section == 0)
        {
            cell.imgBg.image = [UIImage imageNamed:((indexPath.row == ([_contentList count]-1)) ?@"settings_list_item_bottom": @"settings_list_item_middle")];
        }
    }
    cell.viewSeparator.hidden = (indexPath.row == ([_contentList count]-1));

    if (indexPath.section == 0) {
        SettingsObject *obj = [_contentList objectAtIndex:indexPath.row];
        switch (obj.type) {
            case SettingsDownloadQuality: {
                NSString * srt = [self.settingsDefaultValues objectForKey:[KADownloadSettings downloadQuality]];
                cell.rightLabel.text = srt;
            }
                break;
            case SettingsDownloadQuota: {
                NSString * srt = [self.settingsDefaultValues objectForKey:[KADownloadSettings downloadQuota]];
                cell.rightLabel.text = [srt stringByAppendingString:@"%"];
            }
            default:
                break;
        }
        cell.leftLabel.text = obj.name;
    }
    
    UIImageView *accesoryImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"settings_accesory_icon"]];
    accesoryImage.frame = CGRectMake((cell.frame.size.width - accesoryImage.frame.size.width) - 5.f, (cell.frame.size.height - accesoryImage.frame.size.height) / 2.0, accesoryImage.frame.size.width, accesoryImage.frame.size.height);
    [cell addSubview:accesoryImage];
    
    cell.rightLabel.textColor = [UIColor lightGrayColor];
    [Utils setLabelFont:cell.rightLabel size:16 bold:YES];
    
    cell.leftLabel.textColor = [UIColor lightGrayColor];
    [Utils setLabelFont:cell.leftLabel size:16 bold:YES];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsObject *obj = [_contentList objectAtIndex:indexPath.row];
    switch (obj.type) {
        case SettingsDownloadQuality: {
            SettingDownloadsQualityViewController *controller = [[ViewControllersFactory defaultFactory] settingDownloadsQualityViewController];
            [self.navigationController pushViewController:controller animated:YES];
        }
            break;
        case SettingsDownloadQuota: {
            SettingDownloadsQuotaViewController *controller = [[ViewControllersFactory defaultFactory] settingDownloadsQuotaViewController];
            [self.navigationController pushViewController:controller animated:YES];
        }
        default:
            break;
    }
}
#pragma mark - Button

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)changeState:(id)sender {
    UISwitch *obj = sender;
    
    NSString * strValue = obj.isOn ? @"true" : @"false";
    
    NSInteger type = obj.tag - kSwitchTagStartFrom;
    NSString * key = @"";

    switch (type) {
        case SettingsDownloadAutoResume:
            key=  [KADownloadSettings autoResumeDownloads];
            break;
        case SettingsDownloadCellular:
            key=  [KADownloadSettings enableDownloadOnCellularData];
            break;
        default:
            break;
    }

    [[DTGManager sharedInstance] setSettingsKey:key value:strValue completion:^(BOOL success) {
        ASLogInfo(@"is success = %d",success);
    }];
}

@end
