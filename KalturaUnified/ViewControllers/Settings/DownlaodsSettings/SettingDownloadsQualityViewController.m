//
//  SettingDownloadsQualityViewController.m
//  KalturaUnified
//
//  Created by Israel Berezin on 5/31/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingDownloadsQualityViewController.h"
#import "SettingDownloadsCell.h"

typedef enum
{
    SettingsDownloadQualityHD,
    SettingsDownloadQualitySD
}
SettingsDownloadQuality;

@interface SettingDownloadsQualityViewController ()<UITableViewDataSource,UITableViewDelegate>
@property NSInteger topHeaderHigh;

@end

@implementation SettingDownloadsQualityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableViewContent.delegate = self;
    self.tableViewContent.dataSource = self;
    
    [Utils setLabelFont:_labelTitle bold:YES];
    _labelTitle.text = LS(@"dtps_quality_page_main_bit_rate");
    
    self.headerLabelTitle.text = LS(@"dtps_quality_page_header_title");
    [Utils setLabelFont:self.headerLabelTitle bold:YES];

    self.topHeaderHigh = self.viewHeader.frame.size.height;
    [_viewHeader removeFromSuperview];
    
    self.selectedIndex = 0;
    [self updateContentList];
}

-(void)updateContentList
{
    
    self.contentList = [NSArray arrayWithObjects:   [SettingsObject createWithName:LS(@"dtps_quality_page_HD") type:SettingsDownloadQualityHD],
                                                    [SettingsObject createWithName:LS(@"dtps_quality_page_SD") type:SettingsDownloadQualitySD],
                                                    nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString * key = [KADownloadSettings downloadQuality];

    [[DTGManager sharedInstance] getSettingsKey:key completion:^(NSString * value) {
        if ([value isEqualToString:@"high"])
        {
            self.selectedIndex = 0;
        }
        else
        {
            self.selectedIndex = 1;
        }
        [self.tableViewContent reloadData];
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     return [_contentList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return  self.topHeaderHigh;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
        return _viewHeader;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * const st_SettingDownloadsCell = @"SettingDownloadsCell";
    
    SettingDownloadsCell *cell = [tableView dequeueReusableCellWithIdentifier:st_SettingDownloadsCell];
    if (!cell)
    {
        cell = [SettingDownloadsCell settingDownloadsCell];
    }

    [self updateDownloadsCellUI:cell withIndexPath:indexPath];

    return cell;
}

-(void)updateDownloadsCellUI:(SettingDownloadsCell *)cell withIndexPath:(NSIndexPath*)indexPath
{
    if (isPhone)
    {
        cell.backgroundColor = CS(@"232323");
        cell.imgBg.hidden = YES;
    }
    else
    {
        cell.imgBg.hidden = NO;
        cell.backgroundColor = [UIColor clearColor];
        
        NSInteger arrSize =([_contentList count]-1);
        
        if (indexPath.row == 0)
        {
            cell.imgBg.image = [UIImage imageNamed:@"settings_list_item_top"];
        }
        else if (indexPath.row == arrSize)
        {
            cell.imgBg.image = [UIImage imageNamed:@"settings_list_item_bottom"];
        }
        else
        {
            cell.imgBg.image = [UIImage imageNamed:@"settings_list_item_middle"];
        }
    }
    
    cell.rightLabel.hidden = YES;
    cell.viewSeparator.hidden = (indexPath.row == ([_contentList count]-1));

    if (indexPath.row == self.selectedIndex)
    {
        cell.checkMarkImage.hidden = NO;
    }
    else
    {
        cell.checkMarkImage.hidden = YES;
    }
    
    SettingsObject *obj = [_contentList objectAtIndex:indexPath.row];
    cell.leftLabel.text = obj.name;
    
    cell.leftLabel.textColor = [UIColor lightGrayColor];
    [Utils setLabelFont:cell.leftLabel size:16 bold:YES];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != self.selectedIndex)
    {
        SettingDownloadsCell * oldCell = (SettingDownloadsCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0]];
        SettingDownloadsCell * newCell = (SettingDownloadsCell *)[tableView cellForRowAtIndexPath:indexPath];
        oldCell.checkMarkImage.hidden = YES;
        newCell.checkMarkImage.hidden = NO;
        self.selectedIndex = indexPath.row;
        
        SettingsObject *obj = [_contentList objectAtIndex:indexPath.row];
        NSString * key = [KADownloadSettings downloadQuality];
        NSString * value = obj.name;
      
        [[DTGManager sharedInstance] setSettingsKey:key value:value completion:^(BOOL success) {
            ASLogInfo(@"is success = %d",success);
        }];
    }
}

#pragma mark - Button

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
