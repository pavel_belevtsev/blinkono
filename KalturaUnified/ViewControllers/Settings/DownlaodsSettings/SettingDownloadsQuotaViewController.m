//
//  SettingDownloadsQuotaViewController.m
//  KalturaUnified
//
//  Created by Israel Berezin on 5/31/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingDownloadsQuotaViewController.h"
#include <sys/param.h>
#include <sys/mount.h>

@interface SettingDownloadsQuotaViewController ()

@property NSInteger topHeaderHigh;
@property NSInteger topHeaderFotter;

@end

@implementation SettingDownloadsQuotaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Utils setLabelFont:self.labelTitle bold:YES];
    self.labelTitle.text = LS(@"dtgs_Quota_page_Downloads_Quota");
    
    self.headerLabelTitle.text = LS(@"dtgs_Quota_page_header_title");
    [Utils setLabelFont:self.headerLabelTitle bold:YES];
    
    [self calculateFreeDiskSize];
    NSString * baseString =  LS(@"dtgs_Quota_page_available_stronge_on_your_device");
    NSString * descriptionText = [NSString stringWithFormat:baseString, [@(self.freeDiskSpace) stringValue], self.freeDiskSizeString,[@(self.freeDiskPercent) stringValue] ];
    self.footerLabelTitle.text = descriptionText;
    [Utils setLabelFont:self.footerLabelTitle bold:YES];
    [self.quotaSlider addTarget:self action:@selector(quotaSliderChangeValue:) forControlEvents:UIControlEventValueChanged];

//    self.quotaSlider.thumbTintColor = [UIColor redColor];
    self.quotaSlider.minimumTrackTintColor =  APST.brandColor;
//    [self.quotaSlider setThumbImage:[UIImage imageNamed:@"settings_quota_handle"] forState:UIControlStateNormal];

    self.sliderView.layer.cornerRadius = 5.0;
    [self.sliderView setBackgroundColor:[UIColor colorWithRed:35.0/225.0 green:35.0/225.0 blue:35.0/225.0 alpha:1.0]];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString * key = [KADownloadSettings downloadQuota];
    [[DTGManager sharedInstance] getSettingsKey:key completion:^(NSString * value) {
        NSInteger intValue = [value integerValue];
        [self.quotaSlider setValue:intValue];
        self.lblQuotaValue.text = [NSString stringWithFormat:@"%d%%",(int)self.quotaSlider.value];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)calculateFreeDiskSize
{
    ASLogInfo(@"freeDiskspaceInMB = %lld",[self freeDiskSpaceInMB]);
    ASLogInfo(@"totalDiskspaceInMB = %lld",[self totalDiskSpaceInMB]);

    uint64_t totalSpace =[self totalDiskSpaceInMB];
    uint64_t totalFreeSpace =[self freeDiskSpaceInMB];
    
    self.freeDiskPercent = 100 * totalFreeSpace / totalSpace;
    
    if (totalFreeSpace > 1024)
    {
        totalFreeSpace = (totalFreeSpace/1024ll);
        self.freeDiskSizeString = LS(@"GB");
    }
    else
    {
        self.freeDiskSizeString = LS(@"MB");

    }
    self.freeDiskSpace = totalFreeSpace;
    
    ASLogInfo(@"totalDiskspaceIn %@ = %lld and it %ld Percent Free",self.freeDiskSizeString, self.freeDiskSpace,(long)self.freeDiskPercent );

}



- (uint64_t)freeDiskSpaceInMB{
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    
    __autoreleasing NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
        totalFreeSpace =((totalFreeSpace/1024ll)/1024ll);

    }
    else {
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

- (uint64_t)totalDiskSpaceInMB{
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    
    __autoreleasing NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
        totalSpace =((totalSpace/1024ll)/1024ll);
        
    }
    else {
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalSpace;
}

#pragma mark - Button -

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)onQuotaSliderEnd:(id)sender
{
    NSString * key = [KADownloadSettings downloadQuota];
    NSString * value = [NSString stringWithFormat:@"%d",(int)self.quotaSlider.value];
    
    [[DTGManager sharedInstance] setSettingsKey:key value:value completion:^(BOOL success) {
        ASLogInfo(@"is success = %d",success);
    }];
}

- (IBAction)quotaSliderChangeValue:(id)sender
{
    self.lblQuotaValue.text = [NSString stringWithFormat:@"%d%%",(int)self.quotaSlider.value];
}


@end
