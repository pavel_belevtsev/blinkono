//
//  SettingDownloadsViewController.h
//  KalturaUnified
//
//  Created by Israel Berezin on 5/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingDownloadsViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableViewContent;

@property (weak, nonatomic) IBOutlet UILabel *headerNetworkAndStoragelabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *audiolabelTitle;

@property (strong, nonatomic) IBOutlet UIView *viewHeaderNetworkAndStorage;
@property (strong, nonatomic) IBOutlet UIView *viewHeaderAudio;

@property (strong, nonatomic) NSMutableArray *contentList;

@property (strong, nonatomic) NSDictionary * settingsDefaultValues;
@end
