//
//  SettingDownloadsQuotaViewControllerPhone.h
//  KalturaUnified
//
//  Created by Israel Berezin on 5/31/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SettingDownloadsQuotaViewController.h"

@interface SettingDownloadsQuotaViewControllerPhone : SettingDownloadsQuotaViewController
@property (weak, nonatomic) IBOutlet UITableView *tableViewContent;
@property (weak, nonatomic) IBOutlet UILabel *headerlabelTitle;

@property (strong, nonatomic) IBOutlet UIView *viewHeader;
@property (strong, nonatomic) IBOutlet UIView *viewFotter;


@end
