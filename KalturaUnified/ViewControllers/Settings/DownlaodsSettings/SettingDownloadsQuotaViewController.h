//
//  SettingDownloadsQuotaViewController.h
//  KalturaUnified
//
//  Created by Israel Berezin on 5/31/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingDownloadsQuotaViewController : BaseViewController
@property (strong, nonatomic) NSArray *contentList;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
@property  (nonatomic) NSInteger  selectedIndex;
@property (weak, nonatomic) IBOutlet UILabel *footerLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *headerLabelTitle;
@property (weak, nonatomic) IBOutlet UISlider *quotaSlider;
@property (weak, nonatomic) IBOutlet UILabel *lblQuotaValue;

@property (weak, nonatomic) IBOutlet UIView *sliderView;
@property (nonatomic) uint64_t freeDiskSpace;
@property (nonatomic) uint64_t freeDiskPercent;
@property (strong, nonatomic)  NSString * freeDiskSizeString; // GB or MB

- (IBAction)onQuotaSliderEnd:(id)sender;

- (uint64_t) freeDiskSpaceInMB;
- (uint64_t) totalDiskSpaceInMB;

@end
