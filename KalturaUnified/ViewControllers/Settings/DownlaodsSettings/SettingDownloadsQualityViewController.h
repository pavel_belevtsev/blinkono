//
//  SettingDownloadsQualityViewController.h
//  KalturaUnified
//
//  Created by Israel Berezin on 5/31/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingDownloadsQualityViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
@property (weak, nonatomic) IBOutlet UITableView *tableViewContent;

@property (weak, nonatomic) IBOutlet UILabel *headerLabelTitle;
@property (strong, nonatomic) IBOutlet UIView *viewHeader;

@property (strong, nonatomic) NSArray *contentList;

@property  (nonatomic) NSInteger  selectedIndex;
@end
