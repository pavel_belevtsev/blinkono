//
//  MainMenuHeaderCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuHeaderCell : UIView {

}

@property (strong, nonatomic) IBOutlet UILabel *labelTitle;

@end
