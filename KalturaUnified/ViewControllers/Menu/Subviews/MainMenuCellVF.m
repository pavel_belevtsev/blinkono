//
//  MainMenuCellVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/5/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MainMenuCellVF.h"

@implementation MainMenuCellVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */

- (void)updateColor {
    [super updateColor];
    
    [self.cellButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.cellButton setTitleColor:[VFTypography instance].grayScale6Color forState:UIControlStateNormal];
    colorView.clipsToBounds = YES;
}

@end
