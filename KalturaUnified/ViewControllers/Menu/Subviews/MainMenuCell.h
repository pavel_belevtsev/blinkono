//
//  MainMenuCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuCell : UIView {
    
    IBOutlet UIView *colorView;
    
}

- (void)updateColor;

@property (strong, nonatomic) IBOutlet UIButton *cellButton;
@property (weak, nonatomic) TVMenuItem *menuItem;

@end
