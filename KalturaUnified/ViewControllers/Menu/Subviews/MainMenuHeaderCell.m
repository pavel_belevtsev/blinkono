//
//  MainMenuHeaderCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MainMenuHeaderCell.h"

@implementation MainMenuHeaderCell

- (void)awakeFromNib {

    [Utils setLabelFont:_labelTitle bold:YES];

}


@end
