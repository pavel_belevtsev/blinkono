//
//  MainMenuCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MainMenuCell.h"

@implementation MainMenuCell

-(void)awakeFromNib {
    
    [Utils setButtonFont:_cellButton bold:YES];
    [self updateColor];

}

- (void)updateColor {

    colorView.backgroundColor = APST.brandColor;
    [self.cellButton setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [self.cellButton setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    
}

@end
