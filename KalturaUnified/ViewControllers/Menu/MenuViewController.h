//
//  MenuViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchListView.h"

@interface MenuViewController : BaseViewController <UITextFieldDelegate>{
    
    BOOL isMenuButtonPressed;
    BOOL searchMode;
    NSString *menuButtonType;

    BOOL menuDisabled;

}

- (void)selectMenuItem:(TVMenuItem *)menuItem;
- (void)pressMenuItem:(TVMenuItem *)menuItem activityFeedLoadOnMyZoneStart:(BOOL) activityFeedLoadOnStart;
- (IBAction)buttonSettingsPressed:(UIButton *)button;
- (IBAction)buttonAvatarPressed:(UIButton *)button;
- (void)buttonLivePressed:(UIButton *)button;
- (void) buttonSubscriptionsPressed:(TVMenuItem *)menuItem;
- (void)updateMenu;
- (void)deepLinkingAction:(NSNotification *)notification;

@property (weak, nonatomic) IBOutlet UIView *viewMenuUser;
@property (weak, nonatomic) IBOutlet UILabel *labelMenuUser;
@property (weak, nonatomic) IBOutlet UIButton *buttonMenuUser;
@property (weak, nonatomic) IBOutlet UIButton *buttonAvatar;
@property (weak, nonatomic) IBOutlet UIScrollView *menuScrollView;
@property (weak, nonatomic) IBOutlet UIView *viewMenuFooter;
@property (weak, nonatomic) IBOutlet UIView *viewMenuTvScanFooter;

@property (weak, nonatomic) IBOutlet UIView *viewMenuBg;

@property (weak, nonatomic) IBOutlet UILabel *labelNoInternetTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelNoInternetMessage;

@property (weak, nonatomic) TVMenuItem *selectedMenuItem;

@property (nonatomic, weak) IBOutlet UIView *viewSearchForm;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewSearchBackground;
@property (nonatomic, weak) IBOutlet UITextField *textFieldSearch;
@property (nonatomic, strong) SearchListView *searchListView;

@property (weak, nonatomic) IBOutlet UIButton *buttonSettings;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogout;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewFooterBg;
@property (weak, nonatomic) IBOutlet UIButton *buttonCompanion;
@property (weak, nonatomic) IBOutlet UIView * companionFooterPlaceHolder;

-(void)updateUserName;
@property (weak, nonatomic) IBOutlet UIButton *btnSettings;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

#pragma mark - DownloadToDo
@property (weak, nonatomic) IBOutlet UIView *offlineOnlineFooter;
@property (weak, nonatomic) IBOutlet UIView *viewOnlineMenuHolder;
@property (weak, nonatomic) IBOutlet UIView *viewOfflineMenuHolder;
@property (weak, nonatomic) IBOutlet UIButton *buttonGoOnlineOffline;
@property (weak, nonatomic) IBOutlet UILabel *labelPoint;
@property (weak, nonatomic) IBOutlet UILabel *labelGoOnlineOffline;

@property (weak, nonatomic) IBOutlet UIButton *BtnCompanion;

@end
