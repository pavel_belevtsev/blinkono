//
//  MenuViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MenuViewController.h"
#import "MainMenuHeaderCell.h"
#import "MainMenuCell.h"
#import "UIImage+Tint.h"
#import "UIAlertView+Blocks.h"
#import "UIButton+Additions.h"
#import "HomeViewController.h"
#import "CategoryViewController.h"
#import "EPGViewController.h"
#import "PlayerViewController.h"
#import "MyZoneViewController.h"
#import "SettingsViewController.h"
#import "LoginManager.h"
#import "CompanionView.h"
#import <TvinciSDK/TVNetworkTracker.h>
#import <TvinciSDK/TVCommercializationManager.h>


NSString * const TVNMainMenuTypeMenuSubscriptionsKey = @"Subscriptions";
NSString * const TVNMainMenuTypeMenuSettingKey = @"Settings";
NSString * const TVNMainMenuTypeMenuLoginKey = @"Login"; //             URL = "[''Type'':''Login'',''Icon'':''Login'']";
NSString * const TVNMainMenuTypeMenuLogoutKey = @"Logout"; //

@interface MenuViewController ()<CompanionViewDelegate>

//@property (strong, nonatomic) CompanionView * companionView;
@property (nonatomic, strong) NSDictionary *menuIcons;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isMenuButtonPressed = NO;

    self.viewContent.layer.cornerRadius = 4;
    self.viewContent.layer.masksToBounds = YES;

    [self initIconsList];
    [self updateMenu];

    [self registerForNotification];
    
    self.searchListView = [[SearchListView alloc] initWithNib];

    _searchListView.frame = _menuScrollView.frame;
    _searchListView.alpha = 0.0;
    _searchListView.delegate = self;
    [[_viewSearchForm superview] addSubview:_searchListView];

    searchMode = NO;
    menuButtonType = @"";

    self.btnLogin.hidden = [APST settingsAndLoginFromMenu];
    self.btnSettings.hidden = [APST settingsAndLoginFromMenu];

    self.buttonLogout.hidden = [APST settingsAndLoginFromMenu];
    self.buttonSettings.hidden = [APST settingsAndLoginFromMenu];

    [self setUpOnlineOfflineUI];

    [self setupFooter];

    self.labelNoInternetTitle.text =  LS(@"stg_menu_offline_title");
    self.labelNoInternetMessage.text = LS(@"stg_menu_offline_message");
    [self registerTVCompanionMixedNotification];
    
    [self.BtnCompanion setImage:[UIImage imageNamed:@"chromecast_icon_not-connected"] forState:UIControlStateNormal];
    [self.BtnCompanion setImage:[[UIImage imageNamed:@"chromecast_icon_connected"]imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];

    self.textFieldSearch.isAccessibilityElement = YES;
    self.textFieldSearch.accessibilityLabel = @"Text Field Search";

}

-(void) setupFooter
{
    BOOL downloadIsOn = [NU(UIUnit_Download_buttons) isUIUnitExist];
    BOOL showCompanionFooter = APST.menuFotterTV  == YES && [NU(UIUnit_Companion_buttons) isUIUnitExist];

    self.offlineOnlineFooter.hidden = !downloadIsOn;
    self.viewMenuFooter.hidden = downloadIsOn || showCompanionFooter;
    self.viewMenuTvScanFooter.hidden = downloadIsOn || !showCompanionFooter;

}

- (void)viewDidAppear:(BOOL)animated
{
    [self avatarPictureUpdate];
    [self deepLinkingAction:nil];
    if ([[TVMixedCompanionManager sharedInstance] isConnected])
    {
        [self devicePaired:nil];
    }
    else
    {
        [self deviceUnPaired:nil];
    }
}

- (void)deepLinkingAction:(NSNotification *)notification {

    if (DeepLinkingM.deepLinkingAction) {

        NSArray *controllers = [NavM.container.centerViewController viewControllers];
        UIViewController *controller = [controllers lastObject];

        if (![controller isKindOfClass:[LoginBaseViewController class]]) {
            /*
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"deepLinkingAction %d %@", DeepLinkingM.deepLinkingAction.deepLinkingType, DeepLinkingM.deepLinkingAction.mediaId] message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
             [alert show];
             */

            DeepLinkingAction *action = DeepLinkingM.deepLinkingAction;

            if (action.deepLinkingType == DeepLinkingTypeMedia) {

                [self requestForGetMediaInfoWithMediaID:[action.mediaId integerValue] completion:^(NSDictionary *result) {

                    if (result) {

                        TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:result];
                        [self openMediaItem:item];

                    } else {

                        [self pressMenuItem:[MenuM menuItem:TVPageLayoutHome] animated:YES];

                    }
                }];

            } else if (action.deepLinkingType == DeepLinkingTypeCategory) {

                [self pressMenuItem:[MenuM menuCategoryItem:action.categoryName] animated:YES];

            } else {

                switch (action.deepLinkingType) {
                    case DeepLinkingTypeHome:
                        [self pressMenuItem:[MenuM menuItem:TVPageLayoutHome] animated:YES];
                        break;
                    case DeepLinkingTypeLiveTV:
                        [self pressMenuItem:[MenuM menuItem:TVPageLayoutLiveTV] animated:YES];
                        break;
                    case DeepLinkingTypeEPG:
                        [self pressMenuItem:[MenuM menuItem:TVPageLayoutEPG] animated:YES];
                        break;
                    case DeepLinkingTypeMyZone:
                        [self pressMenuItem:[MenuM menuItem:TVPageLayoutMyZone] activityFeedLoadOnMyZoneStart:NO];
                        break;
                    case DeepLinkingTypeOnlinePurchases:
                        [self pressMenuItem:[MenuM menuItem:MainMenuItemsONLINE_PURCHASES] animated:YES];
                        break;
                    case DeepLinkingTypeRecordings:
                        [self pressMenuItem:[MenuM menuItem:TVPageLayoutRecordings] animated:YES];
                        break;
                    default:
                        break;
                }

            }

            DeepLinkingM.deepLinkingAction = nil;

        }

    }

}

- (void) avatarPictureUpdate {
    UIImage *image = [LoginManager getProfilePicture];
    self.buttonAvatar.imageView.contentMode = UIViewContentModeScaleAspectFill;

    [self.buttonAvatar setImage:image forState:UIControlStateNormal];
    self.buttonAvatar.layer.masksToBounds = YES;
    self.buttonAvatar.layer.cornerRadius = self.buttonAvatar.frame.size.width / 2.f;
    self.buttonAvatar.layer.borderColor = [UIColor grayColor].CGColor;
    self.buttonAvatar.layer.borderWidth = 1.5f;
}

- (void)initIconsList {

    self.menuIcons = @{@"home": @"sidemenu_home_icon",
                       @"MyZone":@"sidemenu_myzone_icon",
                       @"LiveTV":@"sidemenu_live_icon",
                       @"EPG":@"sidemenu_tvguide_icon",
                       @"Recording":@"sidemenu_recording_icon",
                       @"Category": @"sidemenu_movies_icon",
                       @"Shows": @"sidemenu_shows_icon",
                       @"Movies": @"sidemenu_popcorn_icon",
                       @"Series": @"sidemenu_shows_icon",
                       @"Download": @"sidemenu_download_icon",
                       @"Recordings": @"sidemenu_download_icon",
                       @"Popcorn": @"sidemenu_popcorn_icon",
                       @"Subscriptions": @"sidemenu_movies_icon",
                       @"Settings": @"sidemenu_settings_icon",
                       @"Purchases": @"sidemenu_purchases_icon",
                       @"Login": @"sidemenu_login_icon",
                       @"Logout": @"sidemenu_logout_icon"
                       };

}

#pragma mark - Search

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    return !isMenuButtonPressed;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {

    [self disableAllMenuButtons];

    searchMode = YES;

    if (textField == _textFieldSearch) {

        _searchListView.alpha = 1.0;

        _searchListView.textFieldSearch = _textFieldSearch;
        _textFieldSearch.delegate = _searchListView;
        _searchListView.imageViewSearchBackground = _imageViewSearchBackground;
        _searchListView.imageNotActiveBackground = [UIImage imageNamed:@"menu_search_bg"];
        [_imageViewSearchBackground setImage:[UIImage imageNamed:@"menu_search_bg_active"]];
        [_searchListView initSearchList];

    }
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    [self enableAllMenuButtons];
    if (textField == _textFieldSearch) {
        _imageViewSearchBackground.image = [UIImage imageNamed:@"menu_search_bg"];
    }
}

- (void)searchProcess:(NSString *)searchStr {

    [self enableAllMenuButtons];

    _textFieldSearch.delegate = self;

    [_textFieldSearch resignFirstResponder];

    _searchListView.alpha = 0.0;

    NSLog(@"Search %@", searchStr);

    if ([searchStr length] > 1 && searchMode) {

        TVMenuItem *menuItem = [[TVMenuItem alloc] init];
        menuItem.layout = [NSDictionary dictionaryWithObjectsAndKeys:MainMenuItemsSEARCH, ConstKeyMenuItemType, searchStr, @"Text", nil];

        [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{
            [self pressMenuItem:menuItem activityFeedLoadOnMyZoneStart:NO];
        }];

    }

    searchMode = NO;

}

#pragma mark - Main menu

- (BOOL)checkItemMenuSeperatorType:(TVMenuItem *)item {

    NSString *url = [item.URL absoluteString];

    NSRange range = [url rangeOfString:@"MenuSeperator"];
    if ([url length] && range.location != NSNotFound) {
        return YES;
    }
    else {
        return NO;
    }
}

-(void)updateUserName
{
    TVUser *user = [TVSessionManager sharedTVSessionManager].currentUser;
    _labelMenuUser.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
    [_buttonMenuUser setTitle:_labelMenuUser.text forState:UIControlStateNormal];
    _labelMenuUser.text = @"";
}

- (void)updateMenu {

    _selectedMenuItem = nil;

    TVUser *user = [TVSessionManager sharedTVSessionManager].currentUser;

    [_buttonMenuUser setTitleColor:CS(@"b3b3b3") forState:UIControlStateNormal];

    UIImage *image = [LoginManager getProfilePicture];
    [self.buttonAvatar setImage:image forState:UIControlStateNormal];


    if (user && ([user.siteGUID intValue] > 0))
    {

        _viewMenuUser.hidden = NO;
        [self avatarPictureUpdate];

        [self updateUserName];

        _menuScrollView.frame = CGRectMake(_menuScrollView.frame.origin.x, _viewMenuUser.frame.origin.y + _viewMenuUser.frame.size.height, _menuScrollView.frame.size.width, _viewMenuFooter.frame.origin.y - (_viewMenuUser.frame.origin.y + _viewMenuUser.frame.size.height));

    }
    else
    {


        NSLog(@"anonym");
        _viewMenuUser.hidden = YES;

        _menuScrollView.frame = CGRectMake(_menuScrollView.frame.origin.x, _viewMenuUser.frame.origin.y, _menuScrollView.frame.size.width, _viewMenuFooter.frame.origin.y - _viewMenuUser.frame.origin.y);
    }

    _viewMenuBg.frame = _menuScrollView.frame;

    [_menuScrollView removeAllSubviewsKindOfClass:[MainMenuHeaderCell class]];
    [_menuScrollView removeAllSubviewsKindOfClass:[MainMenuCell class]];

    float y = 0.0;

    int section = 0;

    for (TVMenuItem *menuItem in MenuM.menuItems) {

        if ([self checkItemMenuSeperatorType:menuItem] || (section == 0)) {

            MainMenuHeaderCell *mainMenuHeaderCell = [[[NSBundle mainBundle] loadNibNamed:@"MainMenuHeaderCell" owner:self options:nil] objectAtIndex:0];

            mainMenuHeaderCell.labelTitle.text = [((section == 0) ? LS(@"menu") : menuItem.name) uppercaseString];

            mainMenuHeaderCell.frame = CGRectMake(0, y, mainMenuHeaderCell.frame.size.width, mainMenuHeaderCell.frame.size.height);
            [_menuScrollView addSubview:mainMenuHeaderCell];
            y += mainMenuHeaderCell.frame.size.height;

        }

        _menuScrollView.exclusiveTouch = YES;

        if (![self checkItemMenuSeperatorType:menuItem] || (section == 0)) {

            NSString* cellTitle = nil;
            NSString* cellIconKey = nil;

            // load cell from xib:
            MainMenuCell *mainMenuCell = [[[NSBundle mainBundle] loadNibNamed:@"MainMenuCell" owner:self options:nil] objectAtIndex:0];

            // initialize cell text
            cellTitle = menuItem.name;

            // initialize the cell icon key we received from TVM:
            cellIconKey = [menuItem.layout objectForKey:ConstKeyMenuItemIcon];

            // handle special case of login/logout menu item
            if ([self checkItemMenuLoginOrLogoutType:menuItem])
            {
                if (![LoginM isSignedIn])
                {
                    cellTitle = LS(@"login");
                    cellIconKey = @"Login";
                }
                else
                {
                    cellTitle = LS(@"logout");
                    cellIconKey = @"Logout";
                }
            }

            // set the text & icons
            [mainMenuCell.cellButton setTitle:cellTitle forState:UIControlStateNormal];

            // set the icon
            NSString *iconName = [_menuIcons objectForKey:cellIconKey];
            UIImage *imgIcon = [UIImage imageNamed:iconName];
            UIImage *imgIconSelected = [imgIcon imageTintedWithColor:APST.brandTextColor];

            [mainMenuCell.cellButton setImage:imgIcon forState:UIControlStateNormal];
            [mainMenuCell.cellButton setImage:imgIconSelected forState:UIControlStateHighlighted];
            [mainMenuCell.cellButton setImage:imgIconSelected forState:UIControlStateSelected];
            mainMenuCell.cellButton.adjustsImageWhenDisabled = NO;

            mainMenuCell.cellButton.tag = section;
            [mainMenuCell.cellButton addTarget:self action:@selector(buttonMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
            [mainMenuCell.cellButton addTarget:self action:@selector(buttonMenuPressedMultiple:withEvent:) forControlEvents:UIControlEventTouchDownRepeat];

            mainMenuCell.menuItem = menuItem;

            if ([menuButtonType isEqualToString:menuItem.name]){
                mainMenuCell.cellButton.selected = YES;
                _selectedMenuItem = menuItem;
            }

            //[mainMenuCell.cellButton addTarget:self action:@selector(enableAllMenuButtons) forControlEvents:UIControlEventTouchCancel | UIControlEventTouchDragOutside];


            mainMenuCell.frame = CGRectMake(0, y, mainMenuCell.frame.size.width, mainMenuCell.frame.size.height);
            [self.menuScrollView addSubview:mainMenuCell];

            mainMenuCell.cellButton.exclusiveTouch = YES;
            y += mainMenuCell.frame.size.height;

        }

        section++;

    }

    _menuScrollView.contentSize = CGSizeMake(_menuScrollView.frame.size.width, y - 1);

   }


-(IBAction)companionPressed:(id)sender
{
    UIViewController * centerViewController = ((UINavigationController *)[self menuContainerViewController].centerViewController).topViewController;

    if ([centerViewController isKindOfClass:[PlayerViewController class]])
    {
        PlayerViewController * playerViewController = (PlayerViewController *)centerViewController;
        [playerViewController buttonCompanionPressed:nil];
    }
    else
    {
        self.companionView = [[CompanionViewPhone alloc] init];
        [self.companionView updateTexts];
        self.companionView.delegate = self;
        if ([TVMixedCompanionManager sharedInstance].isConnected == NO)
        {
            [self.companionView buttonScanPressed:nil];
        }
        [[centerViewController view] addSubview:self.companionView];
        self.companionView.frame = CGRectMake(0, 20, centerViewController.view.size.width, centerViewController.view.size.height-20);
        [((CompanionViewPhone*)self.companionView) updateScreenSize];
        
        CompanionPlayerState state = (CompanionPlayerState)[TVMixedCompanionManager sharedInstance].currentCompanionPlayerState;
        if ([TVMixedCompanionManager sharedInstance].isConnected == YES && (state == CompanionPlayerStatePlay || state == CompanionPlayerStatePause))
        {
            [self.companionView showCompanionPlayView];
        }

        
    }

    [[self menuContainerViewController] toggleLeftSideMenuCompletion:nil];
}

- (void)deSelectMenuItems {

    _selectedMenuItem = nil;
    menuButtonType = @"";
    [_menuScrollView performBlockOnSubviewsKindOfClass:[MainMenuCell class] block:^(UIView *subview) {
        [(MainMenuCell*)subview cellButton].selected = NO;
    }];
}

- (void)selectMenuItem:(TVMenuItem *)menuItem {

    _selectedMenuItem = menuItem;
    menuButtonType = menuItem.name;

    [_menuScrollView performBlockOnSubviewsKindOfClass:[MainMenuCell class] block:^(UIView *subview) {
        [(MainMenuCell*)subview cellButton].selected = ([[(MainMenuCell*)subview menuItem] isEqual:_selectedMenuItem]);
    }];
}

- (void)pressMenuItem:(TVMenuItem *)menuItem animated:(BOOL) animated{
    [self pressMenuItem:menuItem activityFeedLoadOnMyZoneStart:NO homeSelected:NO animated:animated];
}

- (void)pressMenuItem:(TVMenuItem *)menuItem homeSelected:(BOOL)homeSelected animated:(BOOL) animated {
    [self pressMenuItem:menuItem activityFeedLoadOnMyZoneStart:NO homeSelected:homeSelected animated:animated];
}

- (void)pressMenuItem:(TVMenuItem *)menuItem activityFeedLoadOnMyZoneStart:(BOOL) activityFeedLoadOnStart {
    [self pressMenuItem:menuItem activityFeedLoadOnMyZoneStart:activityFeedLoadOnStart animated:YES];
}

- (void)pressMenuItem:(TVMenuItem *)menuItem activityFeedLoadOnMyZoneStart:(BOOL)activityFeedLoadOnMyZoneStart animated:(BOOL) animated {
    [self pressMenuItem:menuItem activityFeedLoadOnMyZoneStart:activityFeedLoadOnMyZoneStart homeSelected:NO animated:animated];
}

- (void)pressMenuItem:(TVMenuItem *)menuItem activityFeedLoadOnMyZoneStart:(BOOL)activityFeedLoadOnStart homeSelected:(BOOL)homeSelected animated:(BOOL) animated{

    NSArray *controllers = [[self menuContainerViewController].centerViewController viewControllers];
    BOOL showAnimated = animated;
    NSString *type = [menuItem.layout objectOrNilForKey:ConstKeyMenuItemType];

    if ([type isEqualToString:TVPageLayoutHome] || [type isEqualToString:TVPageLayoutCover])
    {
        [NavM setPhonePortrait];

        [self selectMenuItem:menuItem];

        for (HomeViewController *controller in controllers)
        {
            if ([controller isKindOfClass:[HomeViewController class]])
            {
                //if (controller != [controllers lastObject]) {
                ((HomeViewController *)controller).fullReload = homeSelected;
                //}

                if (controller == [controllers lastObject]) {
                    [(HomeViewController *)controller updateMenu];
                } else {
                    [[self menuContainerViewController].centerViewController popToViewController:controller animated:NO];
                }

                return;
            }
        }

        if ([controllers count] && [[controllers lastObject] isKindOfClass:[PlayerViewController class]])
        {
            [[self menuContainerViewController].centerViewController popViewControllerAnimated:NO];
            showAnimated = NO;
        }
        HomeViewController *controller = [[ViewControllersFactory defaultFactory] homeViewController];
        [[self menuContainerViewController].centerViewController pushViewController:controller animated:showAnimated];

    } else if ([type isEqualToString:MainMenuItemsONLINE_PURCHASES] && ![LoginM isSignedIn]) {

        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"buttonOnlinePurchases"
                                                             forKey:@"loginPoint"];
        [[NSNotificationCenter defaultCenter] postNotificationName:KLoginNotification
                                                            object:nil
                                                          userInfo:userInfo];

    } else if ([type isEqualToString:MainMenuItemsCATEGORY_2_3] || [type isEqualToString:MainMenuItemsCATEGORY_16_9] || [type isEqualToString:MainMenuItemsSEARCH] || [type isEqualToString:MainMenuItemsONLINE_PURCHASES]) {

        MenuCategoryTypeSize categoryTypeSize = MenuCategoryTypeSize_2_3;
        if ([type isEqualToString:MainMenuItemsCATEGORY_16_9] || ([type isEqualToString:MainMenuItemsCATEGORY_2_3] && APST.category_16_9)) {
            categoryTypeSize = MenuCategoryTypeSize_16_9;
        }

        [NavM setPhonePortrait];

        if (![type isEqualToString:MainMenuItemsSEARCH])
        {
            [self selectMenuItem:menuItem];
        }

        for (CategoryViewController *controller in controllers)
        {
            if ([controller isKindOfClass:[CategoryViewController class]])
            {
                controller.menuCategoryTypeSize = categoryTypeSize;
                if (controller == [controllers lastObject])
                {
                    [controller updateDataForMenuItem:menuItem];
                }
                else
                {
                    controller.initialMenuItem = menuItem;
                    [[self menuContainerViewController].centerViewController popToViewController:controller animated:NO];
                }
                return;
            }
        }

        if ([controllers count] && [[controllers lastObject] isKindOfClass:[PlayerViewController class]])
        {
            [[self menuContainerViewController].centerViewController popViewControllerAnimated:NO];
            showAnimated = NO;
        }
        CategoryViewController *controller = [[ViewControllersFactory defaultFactory] categoryViewController];
        controller.initialMenuItem = menuItem;
        controller.menuCategoryTypeSize = categoryTypeSize;
        [[self menuContainerViewController].centerViewController pushViewController:controller animated:showAnimated];

    }
    else if ([type isEqualToString:TVPageLayoutEPG])
    {

        [NavM setPhonePortrait];

        [self selectMenuItem:menuItem];

        for (EPGViewController *controller in controllers)
        {
            if ([controller isKindOfClass:[EPGViewController class]])
            {
                if (controller != [controllers lastObject])
                {
                    [[self menuContainerViewController].centerViewController popToViewController:controller animated:NO];
                }
                return;
            }
        }

        if ([controllers count] && [[controllers lastObject] isKindOfClass:[PlayerViewController class]])
        {
            [[self menuContainerViewController].centerViewController popViewControllerAnimated:NO];
            showAnimated = NO;
        }
        EPGViewController *controller = [[ViewControllersFactory defaultFactory] epgViewController];
        controller.epgMenuItem = menuItem;
        [[self menuContainerViewController].centerViewController pushViewController:controller animated:showAnimated];

    }
    else if ([type isEqualToString:TVPageLayoutLiveTV])
    {

        [NavM setPhonePortrait];

        //        if ([LoginM isSignedIn])
        //        {

        [self selectMenuItem:menuItem];

        for (PlayerViewController *controller in controllers)
        {
            if ([controller isKindOfClass:[PlayerViewController class]])
            {
                if (controller == [controllers lastObject])
                {
                    [controller setLivePlayer];
                }
                else
                {
                    controller.initialMediaItem = nil;
                    [controller setLivePlayer];
                    [[self menuContainerViewController].centerViewController popToViewController:controller animated:NO];
                }
                return;
            }
        }

        if (!NavM.playerViewController)
        {
            NavM.playerViewController = [[ViewControllersFactory defaultFactory] playerViewController];
        }

        NavM.playerViewController.initialMediaItem = nil;
        NavM.playerViewController.liveTV = YES;
        [NavM.container.centerViewController pushViewController:NavM.playerViewController animated:showAnimated];


        //        }
        //        else
        //        {
        //            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"buttonLive"
        //                                                                 forKey:@"loginPoint"];
        //            [[NSNotificationCenter defaultCenter] postNotificationName:KLoginNotification
        //                                                                object:nil
        //                                                              userInfo:userInfo];
        //        }

    }
    else if ([type isEqualToString:TVPageLayoutMyZone])
    {

        [NavM setPhonePortrait];

        [self selectMenuItem:menuItem];

        if ([controllers count] && [[controllers lastObject] isKindOfClass:[PlayerViewController class]]) {
            [[self menuContainerViewController].centerViewController popViewControllerAnimated:NO];
            showAnimated = NO;
        }

        MyZoneViewController *controller = nil;
        if (isPad)
        {
            controller = [[ViewControllersFactory defaultFactory] myZoneFullScreenViewController];
        }
        else
        {
            controller = [[ViewControllersFactory defaultFactory] myZoneViewController];
        }

        if (isPhone){

            for (HomeViewController *hController in controllers) {
                if ([hController isKindOfClass:[HomeViewController class]]) {
                    controller.delegateController = hController;
                }
            }
        }

        [[self menuContainerViewController].centerViewController pushViewController:controller animated:showAnimated];

        controller.activityFeedLoadOnStart = activityFeedLoadOnStart;
        controller.titleString = menuItem.name;


    }
    else if ([type isEqualToString:TVNMainMenuTypeMenuLoginKey] || [type isEqualToString:TVNMainMenuTypeMenuLogoutKey])
    {
        [self buttonLogoutPressed:nil];
    }
    else if ([type isEqualToString:TVNMainMenuTypeMenuSettingKey])
    {
        [self buttonSettingsPressed:nil];
        if ([LoginM isSignedIn]) {
            [self selectMenuItem:menuItem];
        }
    }
    else if ([type isEqualToString:TVNMainMenuTypeMenuSubscriptionsKey])
    {
        [self buttonSubscriptionsPressed:menuItem];
    }
    else
    {
        NSLog(@"Type not realized '%@'", type);
    }
}

-(void) menuOpenCenterScreenButtonPressed :(NSNotification*)notification {

    if (_searchListView.alpha == 1) {

        if ([notification.name isEqualToString:MFSideMenuStateNotificationEvent])
        {
            NSDictionary* userInfo = notification.userInfo;
            if (userInfo){
                if ([userInfo objectForKey:@"eventType"] && [[userInfo objectForKey:@"eventType"] integerValue] == MFSideMenuStateEventMenuWillClose){

                    searchMode = NO;
                    [_textFieldSearch resignFirstResponder];

                }
            }
        }
    }
}


#pragma mark - Buttons

- (void) buttonSubscriptionsPressed:(TVMenuItem *)menuItem  {
    if ([LoginM isSignedIn]) {
        if (!menuItem)
            menuItem = [MenuM menuItem:TVNMainMenuTypeMenuSubscriptionsKey];
        [self selectMenuItem:menuItem];

        SubscriptionsListViewController *controller = [[ViewControllersFactory defaultFactory] subscriptionsListViewController];
        [[self menuContainerViewController].centerViewController pushViewController:controller animated:YES];
    }
    else {

        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"buttonSubscriptions"
                                                             forKey:@"loginPoint"];
        [[NSNotificationCenter defaultCenter] postNotificationName:KLoginNotification
                                                            object:nil
                                                          userInfo:userInfo];
    }
}

-(void)buttonMenuPressedMultiple:(id)sender withEvent:(UIEvent*)event {
    UITouch* touch = [[event allTouches] anyObject];
    UIButton *button = sender;
    button.clickEnabled = FALSE;
    if (touch.tapCount == 2) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            button.clickEnabled = TRUE;
        });
    }
}

- (void)buttonMenuPressed:(UIButton *)button {

//    NSLog(@"buttonMenuPressed, button.clickEnabled: %d", button.clickEnabled);
    if (button.clickEnabled && button.tag < [MenuM.menuItems count]) {
        [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{ }];
        TVMenuItem *menuItem = [MenuM.menuItems objectAtIndex:button.tag];

        NSString *type = [menuItem.layout objectOrNilForKey:ConstKeyMenuItemType];

        [self pressMenuItem:menuItem homeSelected:[type isEqualToString:TVPageLayoutHome] animated:YES];
    }

    [self enableAllMenuButtons];
}

- (void)buttonLivePressed:(UIButton *)button {

    [self pressMenuItem:[MenuM menuItem:TVPageLayoutLiveTV] activityFeedLoadOnMyZoneStart:NO];
}

- (IBAction)buttonSettingsPressed:(UIButton *)button {

    if ([LoginM isSignedIn]) {

        if (button && ([self menuContainerViewController].menuState == MFSideMenuStateLeftMenuOpen)) {
            [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{}];
        }

        NSArray *controllers = [[self menuContainerViewController].centerViewController viewControllers];

        [NavM setPhonePortrait];

        [self deSelectMenuItems];

        for (SettingsViewController *controller in controllers) {
            if ([controller isKindOfClass:[SettingsViewController class]]) {
                if (controller == [controllers lastObject]) {

                } else {
                    [[self menuContainerViewController].centerViewController popToViewController:controller animated:NO];
                }
                return;
            }
        }

        SettingsViewController *controller = [[ViewControllersFactory defaultFactory] settingsViewController];
        [[self menuContainerViewController].centerViewController pushViewController:controller animated:YES];

    } else {

        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"buttonSettigs"
                                                             forKey:@"loginPoint"];
        [[NSNotificationCenter defaultCenter] postNotificationName:KLoginNotification
                                                            object:nil
                                                          userInfo:userInfo];
    }

    [self enableAllMenuButtons];

}

- (IBAction)buttonLogoutPressed:(UIButton *)button {

    if ([LoginM isSignedIn]) {


        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"logout")
                                                            message:LS(@"log_out_msg")
                                                           delegate:nil
                                                  cancelButtonTitle:LS(@"cancel")
                                                  otherButtonTitles:LS(@"logout"), nil];

        [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {

            if (buttonIndex == 1) {

                [LoginM logout];
                [[NSNotificationCenter defaultCenter] postNotificationName:KLogoutNotification object:nil];

            }


        }];


    } else {

        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"buttonLogout"
                                                             forKey:@"loginPoint"];
        [[NSNotificationCenter defaultCenter] postNotificationName:KLoginNotification
                                                            object:nil
                                                          userInfo:userInfo];

    }

    [self enableAllMenuButtons];


}

- (IBAction)buttonAvatarPressed:(UIButton *)button {

    if (menuDisabled) return;

    if ([self menuContainerViewController].menuState == MFSideMenuStateLeftMenuOpen) {

        menuDisabled = YES;

        [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{

            menuDisabled = NO;

        }];
    }

    if (isPhone) {
        [self pressMenuItem:[MenuM menuItem:TVPageLayoutMyZone] activityFeedLoadOnMyZoneStart:NO];
    } else {

        NSArray *controllers = [[self menuContainerViewController].centerViewController viewControllers];

        if ([controllers count]) {
            BaseViewController *controller = [controllers lastObject];
            [controller openMyZone];

        }
    }


    [self enableAllMenuButtons];

}

-(void) enableAllMenuButtons {

    isMenuButtonPressed = NO;
    [_menuScrollView performBlockOnSubviewsKindOfClass:[MainMenuCell class] block:^(UIView *subview) {
        [(MainMenuCell*)subview cellButton].enabled = YES;
    }];
}

- (void) disableAllMenuButtons {
    [_menuScrollView performBlockOnSubviewsKindOfClass:[MainMenuCell class] block:^(UIView *subview) {
        [(MainMenuCell*)subview cellButton].enabled = NO;
    }];
}

#pragma mark -
#pragma mark TextField

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    return YES;

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    [textField resignFirstResponder];

    return YES;

}

#pragma mark -
#pragma mark - UIKeyboardWillHideNotification Selectors

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    CGRect kbRect = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect convertedRect = [_searchListView.superview convertRect:kbRect fromView:nil];

    CGFloat searchY = _viewSearchForm.frame.origin.y + _viewSearchForm.frame.size.height;
    _searchListView.frame = CGRectMake(_viewSearchForm.frame.origin.x, searchY, _viewSearchForm.frame.size.width, convertedRect.origin.y - searchY);
}


- (void)keyboardWillHide:(NSNotification*)aNotification {
    _searchListView.frame = _menuScrollView.frame;
}


#pragma mark -

- (void)dealloc {

    [self unregisterForNotification];
    [self unregisterTVCompanionMixedNotification];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma  mark -

- (BOOL)checkItemMenuLoginOrLogoutType:(TVMenuItem *)item
{
    NSString *url = [item.URL absoluteString];

    NSRange range = [url rangeOfString:TVNMainMenuTypeMenuLogoutKey];
    if ([url length] && range.location != NSNotFound)
    {
        return YES;
    }
    else
    {
        NSRange range = [url rangeOfString:TVNMainMenuTypeMenuLoginKey];
        if ([url length] && range.location != NSNotFound) {
            return YES;
        }
        else
        {
            return NO;
        }
    }
}

#pragma mark - companion
-(void) hideCompanionScreen
{
    [self.companionView removeFromSuperview];
}

-(void) showLoginView
{

}


#pragma mark - download to go


-(void) setUpOnlineOfflineUI
{
    [self offlineModeChanged:nil];
}
- (IBAction)toggleOfflineMode:(UIButton *)sender
{
    if (sender.selected == YES)
    {
        [[OfflineManager sharedInstance] setIsOffline:NO];
    }
    else
    {
        [[OfflineManager sharedInstance] setIsOffline:YES];

    }
}




-(void) offlineModeChanged:(NSNotification *) notification
{
    BOOL lastModeIsOffline = self.buttonGoOnlineOffline.selected;
    BOOL isOffline = [[OfflineManager sharedInstance] isOffline];
    BOOL noInternetConnection = LoginM.internetReachability.currentReachabilityStatus == NotReachable;
    BOOL downloadUnitExist = [NU(UIUnit_Device_Downloads) isUIUnitExist];

    self.viewOfflineMenuHolder.hidden = !(isOffline && downloadUnitExist);
    self.viewOnlineMenuHolder.hidden = isOffline && downloadUnitExist;
    self.buttonGoOnlineOffline.selected = isOffline;
    self.buttonGoOnlineOffline.enabled = !(noInternetConnection && self.buttonGoOnlineOffline.selected);

    self.labelPoint.text = isOffline?@"\u25CF":@"\u26AC";
    self.labelPoint.textColor = isOffline?(!noInternetConnection?[APST brandColor]:[UIColor darkGrayColor]):[UIColor lightGrayColor];
    self.labelGoOnlineOffline.text = isOffline?LS(@"dtg_Go_Online"):LS(@"dtg_Go_Offline");


    self.labelGoOnlineOffline.textColor = isOffline?(!noInternetConnection?[UIColor lightGrayColor]:[UIColor darkGrayColor]):[UIColor lightGrayColor];

    self.companionFooterPlaceHolder.UIUnitID = NU(UIUnit_Companion_buttons);
    [self.companionFooterPlaceHolder activateAppropriateRule];
    self.companionFooterPlaceHolder.hidden = [APP_SET isOffline]?YES: self.companionFooterPlaceHolder.hidden;

    if (isOffline == NO &&[MenuM menuItem:TVPageLayoutMyZone] == Nil && downloadUnitExist)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationNameRestartApp object:Nil];
    }

    if (lastModeIsOffline != isOffline )
    {
        isOffline? [self pressMenuItem:[MenuM menuItem:TVPageLayoutMyZone] activityFeedLoadOnMyZoneStart:NO animated:YES]:[self pressMenuItem:[MenuM menuItem:isPad?TVPageLayoutHome:TVPageLayoutCover] animated:YES];
    }
}

#pragma mark - notification

-(void) registerForNotification
{
    [self unregisterForNotification];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenu) name:KMenuItemsUpdatedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenu) name:KAuthenticationCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenu) name:KRenameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenu) name:TVSessionManagerLogoutCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuOpenCenterScreenButtonPressed:) name:MFSideMenuStateNotificationEvent object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deepLinkingAction:) name:KDeepLinkingActionNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(offlineModeChanged:) name:NotificationNameApplicationSettingsOfflineModeChanged object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(offlineModeChanged:) name:kReachabilityChangedNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userStatusChanged:) name:NotificationNameUserStatusChanged object:nil];
}

-(void) unregisterForNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

-(void) userStatusChanged:(NSNotification *) notification
{
    [self setupFooter];
    [self setUpOnlineOfflineUI];
}

-(void) devicePaired:(NSNotification *) notification
{
    self.BtnCompanion.selected = YES;
}

-(void) deviceUnPaired:(NSNotification *) notification
{
    self.BtnCompanion.selected = NO;
}

-(void)registerTVCompanionMixedNotification
{
    [self unregisterTVCompanionMixedNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(devicePaired:) name:TVCompanionMixedNotificationDeviceConnected object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceUnPaired:) name:TVCompanionMixedNotificationDeviceDisconnected object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chromeCastForceDisconnect:) name:@"chromeCastForceDisconnectNotifcation" object:nil];

}

-(void)unregisterTVCompanionMixedNotification
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVCompanionMixedNotificationDeviceConnected object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVCompanionMixedNotificationDeviceDisconnected object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"chromeCastForceDisconnectNotifcation" object:nil];

}
-(void)chromeCastForceDisconnect:(NSNotification *)notifcation
{
    if (isPhone)
    {
        if ([[TVMixedCompanionManager sharedInstance] isConnected])
        {
            [[TVMixedCompanionManager sharedInstance] disconnect];
        }
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // do work here
//            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:LS(@"chromecast") message:LS(@"chromecast_casting_otherAppTakeover") delegate:nil cancelButtonTitle:LS(@"close") otherButtonTitles: nil];
//            [alert show];
//            [self deviceUnPaired:nil];
//            
//        });
    }
}
@end
