//
//  MenuViewControllerVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MenuViewControllerVF.h"

@interface MenuViewControllerVF ()

@end

@implementation MenuViewControllerVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.textFieldSearch.textColor = [VFTypography instance].grayScale6Color;
    self.textFieldSearch.font = Font_RegItalic(self.textFieldSearch.font.pointSize);

}

-(void)updateUserName
{
    [super updateUserName];
    [Utils setLabelFont:self.labelMenuUser bold:NO];
    [Utils setButtonFont:self.buttonMenuUser bold:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
