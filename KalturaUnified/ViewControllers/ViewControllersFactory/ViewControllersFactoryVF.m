//
//  ViewControllersFactoryVF.m
//  KalturaUnified
//
//  Created by Rivka Peleg on 12/31/14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "ViewControllersFactoryVF.h"
#import "EPGViewcontrollerVF.h"
#import "PlayerViewControllerVF.h"
#import "LoginStartViewControllerVF.h"
#import "LoginForgotViewControllerVF.h"
#import "LoginMultiViewControllerVF.h"
#import "MyZoneViewControllerVF.h"
#import "SettingsPrivacyViewControllerVF.h"
#import "CategoryViewControllerVF.h"
#import "LoginRegisterViewControllerVF.h"
#import "LoginAssociateViewControllerVF.h"
#import "SettingsViewControllerVF.h"
#import "SettingsEditViewControllerVF.h"

@implementation ViewControllersFactoryVF

-(EPGViewController *) epgViewController
{
    return [[EPGViewcontrollerVF alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:EPG_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (PlayerViewController *)playerViewController {
    
    return [[PlayerViewControllerVF alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:PLAYER_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
    
}

- (LoginStartViewController*) loginStartViewController{
    return [[LoginStartViewControllerVF alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
}

- (LoginForgotViewController*) loginForgotViewController{
    return [[LoginForgotViewControllerVF alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
}

- (LoginMultiViewController*) loginMultiViewController{
    return [[LoginMultiViewControllerVF  alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
}

- (MyZoneViewController*) myZoneViewController
{
    return [[MyZoneViewControllerVF  alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:MYZONE_STORYBOARD_NAME] bundle:nil]];
}

- (MyZoneViewController*) myZoneFullScreenViewController
{
    return [[MyZoneViewControllerVF  alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:MYZONE_STORYBOARD_FULL_NAME] bundle:nil]];
}

- (SettingsPrivacyViewController*) settingsPrivacyViewController {
    return [[SettingsPrivacyViewControllerVF alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (SettingsEditViewController*) settingsEditViewController {
    return [[SettingsEditViewControllerVF alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (CategoryViewController*) categoryViewController {
    if (isPhone) {
        return [[CategoryViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:MAIN_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
    } else{
        return [[CategoryViewControllerVF alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:MAIN_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
        
    }
}

- (LoginRegisterViewController*) loginRegisterViewController
{

       return [[LoginRegisterViewControllerVF alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];

}

- (LoginAssociateViewController*) loginAssociateViewController
{
    if (isPhone)
    {
        return  [super loginAssociateViewController];
    }
    else
    {
        return [[LoginAssociateViewControllerVF alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
    }
}

#pragma - Settings ViewControllers

- (SettingsViewController*) settingsViewController{
    return [[SettingsViewControllerVF alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

@end
