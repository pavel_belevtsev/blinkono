//
//  PageControlView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PageControlView.h"

const NSInteger PageControlBottomSpace = 28;
const CGFloat PageControlDotColor = 119.0;
const CGFloat PageControlCurrentDotColor = 223.0;
const CGFloat PageControlDotSize = 10.0;
const CGFloat PageControlDotSpace = 11.0;

@implementation PageControlView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self pageControlConfig];
    }
    return self;
}

- (void)awakeFromNib {
    [self pageControlConfig];
}

- (void)setNumberOfPages:(NSInteger)numberOfPages {
    _numberOfPages = numberOfPages;
    self.control.numberOfPages = _numberOfPages;
    if (numberOfPages <= 1) {
        self.control.hidden = YES;
    } else {
        self.control.hidden = NO;
    }
}

- (void)pageControlConfig {
    self.control = [[DDPageControl alloc] init];
    [self.control setCenter:CGPointMake(CGRectGetWidth(self.bounds) / 2, CGRectGetHeight(self.bounds) - PageControlBottomSpace)];
    self.control.numberOfPages = _numberOfPages;
    self.currentPage = 0;
    [self.control setDefersCurrentPageDisplay:YES];
    self.control.type = DDPageControlTypeOnFullOffFull;
    self.control.onColor = [UIColor colorWithRed:PageControlCurrentDotColor/255.0f
                                           green:PageControlCurrentDotColor/255.0f
                                            blue:PageControlCurrentDotColor/255.0f
                                           alpha:1.0f];
    self.control.offColor = [UIColor colorWithRed:PageControlDotColor/255.0f
                                            green:PageControlDotColor/255.0f
                                             blue:PageControlDotColor/255.0f
                                            alpha:1.0f];
    [self.control setIndicatorDiameter:PageControlDotSize];
    [self.control setIndicatorSpace:PageControlDotSpace];
    [self addSubview:_control];
}

- (void)setCurrentPage:(NSInteger)page {
    [self.control setCurrentPage:page];
    [self.control updateCurrentPageDisplay];
}

@end
