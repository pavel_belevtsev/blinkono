//
//  BaseViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"
#import "UIImage+Tint.h"
#import "PlayerViewController.h"
#import <TvinciSDK/APSCoreDataStore.h>
#import "MyZoneViewController.h"
#import "SeriesViewController.h"
#import "LoginViewController.h"
#import "MenuViewController.h"
#import "CompanionViewPad.h"
#import "HomeNavProfileView.h"
#import <TvinciSDK/TVMixedCompanionManager.h>
#import "NPVRPlayerAdapter.h"
#import "UIView+RemoveConstraints.h"
#import "AnalyticsManager.h"
#import "TVMixedCompanionManager+Additions.h"

@interface BaseViewController ()<CompanionNavButtonDelegate,HomeNavProfileViewDelegate>



@end

@implementation BaseViewController

- (id)init:(UIStoryboard *)storyboard {
    
    if ((self = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])])) {
        
    }
    return self;
}

- (BOOL)showViewNavigation {
    return YES;
}

- (BOOL)showLogoNavigation {
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.viewContent) {
        self.viewContent.layer.cornerRadius = 4;
        self.viewContent.layer.masksToBounds = YES;
    }
    
    self.privateContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    self.privateContext.persistentStoreCoordinator = [[APSCoreDataStore defaultStore] persistentStoreCoordinator];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
    
    if (isPad && self.viewNavigationBar ) {
        self.homeNavProfileView = [[[NSBundle mainBundle] loadNibNamed:@"HomeNavProfileView" owner:self options:nil] objectAtIndex:0];
        self.homeNavProfileView.delegate = self;
        self.homeNavProfileView.frame = CGRectMake(kViewMyZoneWidth - _homeNavProfileView.frame.size.width, 0, _homeNavProfileView.frame.size.width, _homeNavProfileView.frame.size.height);
        [self.viewNavigationBar addSubview:_homeNavProfileView];
    }

    
    if ([NU(UIUnit_Companion_buttons) isUIUnitExist] && self.viewNavigationBar && isPad)
    {
        self.companionNavButton = [CompanionNavButton companionNavButton];
        self.companionNavButton.delegate = self;
        self.companionNavButton.UIUnitID = NU(UIUnit_Companion_buttons);
        [self.companionNavButton activateAppropriateRule];
        self.companionNavButton.frame = CGRectMake(_homeNavProfileView.frame.origin.x-self.companionNavButton.size.width, 0, self.companionNavButton.frame.size.width, _homeNavProfileView.frame.size.height);
        [_viewNavigationBar addSubview:self.companionNavButton];
    }
    
//    [self.view layoutIfNeeded];
    
    if (self.loadNoInternetView){
        [self noInternetViewSetup];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (isPad && self.myZoneViewController){
        
        [self closeMyZone];
    }


    self.homeNavProfileView.hidden = [APP_SET isOffline];
    [self.companionNavButton activateAppropriateRule];
    self.companionNavButton.hidden = [APP_SET isOffline]?YES:self.companionNavButton.hidden;

}

- (void)skinChanged {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self clearQueueOfRequest];
    self.networkQueue = nil;
}

- (NSUInteger)supportedInterfaceOrientations {
    
    return (isPhone ? UIInterfaceOrientationMaskPortrait : UIInterfaceOrientationMaskLandscape);
    
}

#pragma mark - TVNetworkQueue

- (TVNetworkQueue *)networkQueue {
    if (_networkQueue == nil) {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

- (void)sendRequest:(ASIHTTPRequest *)request {
    if (request != nil) {
        [self.networkQueue sendRequest:request];
    }
}

- (void)clearQueueOfRequest {
    [self.networkQueue cleen];
}

#pragma mark - init Buttons

- (void)initBrandButtonUI:(UIButton *)brandButton {
    UIImage *brandImage = [UIImage imageNamed:@"brand_button"];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:APST.brandColor];
    UIImage *sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateNormal];
    
    coloredBrandImage = [brandImage imageTintedWithColor:CS(@"4e4e4e")];
    sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateHighlighted];
    
    [brandButton setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
    [brandButton setTitleColor:CS(@"b2b2b2") forState:UIControlStateHighlighted];
    [Utils setButtonFont:brandButton bold:YES];
}

- (void)initClearAllButtonUI:(UIButton *)brandButton {
    UIImage *brandImage = [UIImage imageNamed:@"brand_button"];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:CS(@"d84c4b")];
    UIImage *sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateNormal];
    
    [brandButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [brandButton setTitleColor:CS(@"b2b2b2") forState:UIControlStateHighlighted];
    [Utils setButtonFont:brandButton bold:YES];
    
}

- (void)initSelectedBrandButtonUI:(UIButton *)brandButton {
    
    UIImage *img = brandButton.imageView.image;
    if (img) {
        
        UIImage *coloredBrandImage = [img imageTintedWithColor:APST.brandTextColor];
        [brandButton setImage:coloredBrandImage forState:UIControlStateHighlighted];
        [brandButton setImage:coloredBrandImage forState:UIControlStateSelected];
    }
    
    UIImage *brandImage = [UIImage imageNamed:@"brand_button"];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:APST.brandColor];
    UIImage *sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateHighlighted];
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateSelected];
    
    coloredBrandImage = [brandImage imageTintedWithColor:CS(@"4e4e4e")];
    sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateNormal];
    
    [brandButton setTitleColor:CS(@"b2b2b2") forState:UIControlStateNormal];
    [brandButton setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [brandButton setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    [Utils setButtonFont:brandButton bold:YES];
}

- (void)initOneColorBrandButtonUI:(UIButton *)brandButton {
    
    UIImage *img = brandButton.imageView.image;
    if (img) {
        
        UIImage *coloredBrandImage = [img imageTintedWithColor:APST.brandTextColor];
        [brandButton setImage:coloredBrandImage forState:UIControlStateNormal];
        [brandButton setImage:coloredBrandImage forState:UIControlStateHighlighted];
        [brandButton setImage:coloredBrandImage forState:UIControlStateSelected];
    }
    
    UIImage *brandImage = [UIImage imageNamed:@"brand_button"];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:APST.brandColor];
    UIImage *sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateNormal];
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateHighlighted];
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateSelected];

    [brandButton setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
    [brandButton setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [brandButton setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    [Utils setButtonFont:brandButton bold:YES];
}

- (void)initFacebookButtonUI:(UIButton *)brandButton {
    UIImage *brandImage = [UIImage imageNamed:@"brand_button"];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:APST.facebookColor];
    UIImage *sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateNormal];
    [brandButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [brandButton setTitleColor:CS(@"b2b2b2") forState:UIControlStateHighlighted];
    [Utils setButtonFont:brandButton bold:YES];
    
}

- (TVUnderlineButton *)createUnderlineButtonUI:(UIView *)viewButton font:(UIFont *)font {
    
    TVUnderlineButton *button = [[TVUnderlineButton alloc] initWithView:viewButton];
    
    [button setUnderlineNormal:YES];
    
    [button.titleLabel setFont:font];
    [Utils setButtonFont:button bold:YES];
    [button setTitleColor:CS(@"b2b2b2") forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    return button;
    
}

- (void)initSwitchUI:(UISwitch *)switchRemeber {
    
    [switchRemeber setOnTintColor:APST.brandColor];
    [switchRemeber setTintColor:CS(@"bebebe")];
    [switchRemeber setThumbTintColor:[UIColor whiteColor]];
    
}

- (void)initTextFieldUI:(UITextField *)textField {
    
    [Utils setTextFieldFont:textField bold:YES];
    
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:textField.placeholder attributes:@{NSForegroundColorAttributeName: CS(@"4c4c4c")}];
    
}

#pragma mark -
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self closeKeyboard];

}

- (void)closeKeyboard {
    
}

#pragma mark - HUD

- (void)showHUDBlockingUI:(BOOL) blockingUI {
    
    if (self.activityView == nil) {
        
        self.activityView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityView" owner:self options:nil] objectAtIndex:0];
        self.activityView.frame = self.view.bounds;
        self.activityView.viewBackground.hidden = YES; // SYN-3122
        [self.view addSubview:self.activityView];
        self.activityView.userInteractionEnabled = blockingUI?YES:NO;
    }
}


- (void)restartHUD {
    
    if (self.activityView) {
        [_activityView restartHUD];
    }
    
}

- (void)hideHUD {
    
    if (self.activityView) {
        
        [self.activityView removeFromSuperview];
        self.activityView = nil;
        
    }
    
}

#pragma mark - Request

- (void)requestForGetChannelMediaList:(NSInteger)channelID
                             pageSize:(NSInteger)pageSize
                            pageIndex:(NSInteger)pageIndex
                              orderBy:(TVOrderBy)orderBy
                           completion:(void(^)(NSArray *result))completion {
    
    [self requestForGetChannelMediaList:channelID pageSize:pageSize pageIndex:pageIndex orderBy:orderBy array:nil completion:completion];
}

- (void)requestForGetChannelMediaList:(NSInteger)channelID orderBy:(TVOrderBy)orderBy completion:(void(^)(NSArray *result))completion {
    
    NSMutableArray *array = [NSMutableArray new];
    [self requestForGetChannelMediaList:channelID pageSize:maxPageSize pageIndex:0 orderBy:orderBy array:array completion:completion];
    
}

- (void)requestForGetChannelMediaList:(NSInteger)channelID pageSize:(NSInteger)pageSize pageIndex:(NSInteger)pageIndex orderBy:(TVOrderBy)orderBy array:(NSMutableArray *)array completion:(void(^)(NSArray *result))completion {
    
    TVChannelMFOrderDirection orderDir = TVChannelMFOrderDirectionAscending;
    TVMultiFilterOrderBy newOrder = TVMFOrderByNone;
    
    switch (orderBy) {
        case TVOrderByDateAdded:
            newOrder = TVMFOrderByCreateDate;
            /* "PSC :OPF-2132 Order - Newest items are shown last (at the bottom instead of the top) */
            orderDir = TVChannelMFOrderDirectionDescending;
            break;
        case TVOrderByNumberOfViews:
            newOrder = TVMFOrderByViews;
            break;
        case TVOrderByRating:
            newOrder = TVMFOrderByLikeCounter;
            orderDir = TVChannelMFOrderDirectionDescending;
            break;
        default:
            break;
    }
    
    [self requestForGetChannelMediaList:channelID pageSize:pageSize pageIndex:pageIndex orderBy:newOrder orderDir:orderDir array:array completion:completion];
    
}
- (void)requestForGetChannelMediaList:(NSInteger)channelID
                             pageSize:(NSInteger)pageSize
                            pageIndex:(NSInteger)pageIndex
                              orderBy:(TVMultiFilterOrderBy)orderBy
                             orderDir:(TVChannelMFOrderDirection)orderDir
                                array:(NSMutableArray *)array
                           completion:(void(^)(NSArray *result))completion {

    if (!array)
    {
        array = [NSMutableArray new];
    }
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetOrderedChannelMultiFilterChannelID:channelID
                                                                                    pictureSize : [TVPictureSize iPadItemCellPictureSize]
                                                                                       pageSize : pageSize
                                                                                      pageIndex : pageIndex
                                                                                        orderBy : orderBy
                                                                                       orderDir : orderDir
                                                                                     orderValue : @""
                                                                                           tags : @[]
                                                                                        catWith : OR
                                                                                       delegate : nil];
    
    [request setCompletionBlock:^{
    
        id responseData = [request JSONResponse];
        if ([responseData isKindOfClass:[NSArray class]]) {
            NSArray *result = [responseData arrayByRemovingNSNulls];
            
            [array addObjectsFromArray:result];
            
            if ([result count] == pageSize)
            {
                [self requestForGetChannelMediaList:channelID pageSize:pageSize pageIndex:(pageIndex + 1) orderBy:orderBy orderDir:orderDir array:array completion:completion];
            }
            else
            {
                completion(array);
            }
        }
        else {
            completion([NSArray array]);
        }
    }];
    
    [request setFailedBlock:^{
        
        completion([NSArray array]);
        
    }];
    
    [self sendRequest:request];
}

- (void)requestForGetAreMediasFavorites:(NSArray *)mediaIds completion:(void(^)(NSArray *result))completion {
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetAreMediasFavorites:mediaIds delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
        
        completion(result);
        
    }];
    
    [request setFailedBlock:^{
        
        completion([NSArray array]);
        
    }];
    
    [self sendRequest:request];
}

- (void)requestForGetMediaInfoWithMediaID:(NSInteger)mediaID completion:(void(^)(NSDictionary *result))completion {
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetMediaInfoWithMediaID:mediaID pictureSize:[TVPictureSize iPadItemCellPictureSize] includeDynamicInfo:YES delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSDictionary *result = [[request JSONResponse] dictionaryByRemovingNSNulls];
        
        completion(result);
        
    }];
    
    [request setFailedBlock:^{
        
        completion(nil);
        
    }];
    
    
    [self sendRequest:request];
}

- (void)requestForSearchMediaOrList: (NSArray *)orList
                            AndList: (NSArray *)andList
                          mediaType: (NSString *)mediaType
                          pageSize : (NSInteger) pageSize
                         pageIndex : (NSUInteger) pageIndex
                           orderBy : (TVOrderByAndOrList) orderBy
                             exact : (BOOL) exact
                   completionBlock : (void(^)(id jsonResponse))completionBlock
                       failedBlock : (void(^)(NSString* localizedError))failedBlock {
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForSearchMediaByAndOrList:orList AndList:andList mediaType:mediaType pageSize:pageSize pageIndex:pageIndex orderBy:orderBy orderDir:TVOrderDirectionByAndOrList_Ascending exact:exact orderMeta:@"" delegate:nil];
    
    [request setFailedBlock:^{
        
        completionBlock([NSArray array]);
        NSLog(@"requestForSearchMediaOrList setFailedBlock");
        NSLog(@"%@",[request.error localizedDescription]);
        
    }];
    
    [request setCompletionBlock:^{
        
        NSLog(@"requestForSearchMediaOrList setCompletionBlock");
        //NSString * responseString = request.responseString;
        //NSLog(@"%@",responseString);
        
        completionBlock([request JSONResponse]);
        
    }];
    
    [self sendRequest:request];
}

- (void)requestForGetMediaInfoWithEPGChannelID:(NSString *)epgChannelID completion:(void(^)(NSDictionary *result))completion {
    
    NSArray *array = @[@{@"EPG Channel ID meta":epgChannelID}];
    
    [self requestForSearchMediaOrList:array AndList:@[] mediaType:@"0" pageSize:1 pageIndex:0 orderBy:TVOrderByAndOrList_ID exact:NO completionBlock:^(id jsonResponse){
        
        NSArray *array = [jsonResponse arrayByRemovingNSNulls];
        
        if ([array count]) {
            
            completion([array objectAtIndex:0]);
            
        } else {
            
            completion(nil);
            
        }
    } failedBlock:^(NSString* localizedError) {
        
        completion(nil);
        
    }];
}

- (void)requestForGetUserStartedWatchingMediasWithNumOfItems:(NSInteger)numOfItems completion:(void(^)(NSArray *result))completion {
    
    if ([LoginM isSignedIn]) {
        
        __weak TVPAPIRequest *request = [TVPSiteAPI requestForGetUserStartedWatchingMediasWithNumOfItems:numOfItems
                                                                                                delegate:nil];
        [request setCompletionBlock:^{
            
            NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
            
            completion(result);
            
        }];
        
        [request setFailedBlock:^{
            
            completion([NSArray array]);
            
        }];
        
        [self sendRequest:request];
        
    } else {
        completion([NSArray array]);
    }
}

- (void)requestForGetUserExternalActionsShareWatches:(TVUserSocialActionType)action completion:(void(^)(BOOL result))completion {
    
    __weak TVPAPIRequest *request = [TVPSocialAPI requestForGetUserExternalActionShare:action
                                                                        socialPlatform:TVSocialPlatformFacebook
                                                                              delegate:nil];
    [request setCompletionBlock:^{
        
        NSString *response = [request JSONResponse];
        
        if ([response isEqualToString:@"DONT_ALLOW"]) {
            LoginM.externalShareWatches = @"DONT_ALLOW";
            completion(NO);
        } else if ([response isEqualToString:@"ALLOW"] || [response isEqualToString:@"UNKNOWN"]){
            LoginM.externalShareWatches = @"ALLOW";
            completion(YES);
        } else {
            LoginM.externalShareWatches = @"DONT_ALLOW";
            completion(NO);
        }
        
    }];
    
    [request setFailedBlock:^{
        
        completion(NO);
        
    }];
    
    [self sendRequest:request];
}

- (void)requestForEpgHightlightsFromValue:(int)from toValue:(int)to withPageSize:(NSInteger) pageSize andPageIndex:(NSInteger) pageIndex withCompletionBlock:(void (^)(NSArray* programsArray, NSError* error)) completion {
    
    NSArray *andList        = [NSArray array];
    NSMutableArray *orList  = [NSMutableArray array];
    
    for (int i = from; i <= to; i++) {
        NSString* valueStr = [NSString stringWithFormat:@"%d",i];
        NSDictionary* item = [NSDictionary dictionaryWithObjects:
                              [NSArray arrayWithObjects:@"Highlight", valueStr, nil] forKeys:
                              [NSArray arrayWithObjects:@"m_sKey",@"m_sValue", nil]];
        [orList addObject:item];
    }
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForSearchEPGByAndOrList:orList AndList:andList pageSize:pageSize pageIndex:pageIndex delegate:nil];
    
    [request setFailedBlock:^{
        completion(nil, request.error);
    }];
    
    [request setCompletionBlock:^{
        
        NSLog(@"getEPGMultiChannelProgram = %@",request );
        NSArray * channelItems = [request JSONResponse];
        NSMutableArray * channelPrograms = [NSMutableArray array];
        for(NSDictionary * programDictionary in channelItems)
        {
            TVEPGProgram * programItem = [[TVEPGProgram alloc] initWithDictionary:programDictionary];
            [channelPrograms addObject:programItem];
        }
        completion(channelPrograms, nil);
        
    }];
    
    [self sendRequest:request];
}

#pragma mark - My Zone

- (void)openMyZoneWithActivityLoad {

    [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) pressMenuItem:[MenuM menuItem:TVPageLayoutMyZone] activityFeedLoadOnMyZoneStart:YES];    
}

- (void)initMyZone {
    if (_myZoneViewController) {
        [self clearMyZone];
    }
    
    [self menuContainerViewController].panMode = MFSideMenuPanModeNone;
    self.myZoneViewController = [[ViewControllersFactory defaultFactory] myZoneViewController];
    self.myZoneViewController.view.frame = CGRectMake(0, kViewMyZoneHeight * 0.2, kViewMyZoneWidth, kViewMyZoneHeight);
    self.myZoneViewController.view.alpha = 0.5;
    self.myZoneViewController.delegateController = self;
    [self.view insertSubview:self.myZoneViewController.view atIndex:0];
    
}

- (void)clearMyZone {
    
    NSLog(@"clearMyZone");
    
    UIView *viewTap = [[_viewContent subviews] lastObject];
    if (viewTap.tag == kViewCloseMyZoneTag) [viewTap removeFromSuperview];
    
    [self.myZoneViewController.view removeFromSuperview];
    self.myZoneViewController = nil;
}

- (void)openMyZone {
    
    if( [self isKindOfClass:[MyZoneViewController class]])
    {// if we are in the my zone , we should not open new one.
        return;
    }
    
    if ([APSTypography instance].myZoneFullPageEnabled && ![self isKindOfClass:[PlayerViewController class]])
    {
        [((MenuViewController *)[self menuContainerViewController].leftMenuViewController) pressMenuItem:[MenuM menuItem:TVPageLayoutMyZone] activityFeedLoadOnMyZoneStart:NO];
    }
    else
    {
        if (isPad && (_viewContent.frame.origin.y == kViewMyZoneHeight)) {
            
            [self closeMyZone];
            return;
            
        }
        
        [self initMyZone];
        
        if (isPad) {
            
            [UIView animateWithDuration:kViewAnimationDuration animations:^{
                
                self.myZoneViewController.view.frame = CGRectMake(0, 0.0, kViewMyZoneWidth, kViewMyZoneHeight);
                self.myZoneViewController.view.alpha = 1.0;
                _viewContent.frame = CGRectMake(0, kViewMyZoneHeight, _viewContent.frame.size.width, _viewContent.frame.size.height);
                
            } completion:^(BOOL finished) {
                
                UIView *viewTap = [[UIView alloc] initWithFrame:_viewContent.bounds];
                viewTap.tag = kViewCloseMyZoneTag;
                [_viewContent addSubview:viewTap];
                
                UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeMyZone)];
                [tapRecognizer setCancelsTouchesInView:NO];
                [viewTap addGestureRecognizer:tapRecognizer];
                
            }];
        }
    }
}

- (void)closeMyZone
{
    if (![APSTypography instance].myZoneFullPageEnabled || [self isKindOfClass:[PlayerViewController class]] || APST.companion)
    {
        if (self.isCompanionDiscoveryOpned)
        {
            [self toggleComapnionDiscovery];
            return;
        }
        
        [self menuContainerViewController].panMode = (MFSideMenuPanMode)self.panModeDefault;
        
        [UIView animateWithDuration:kViewAnimationDuration animations:^{
            
            self.viewContent.frame = CGRectMake(0, 20, self.viewContent.frame.size.width, self.viewContent.frame.size.height);
            
            self.myZoneViewController.view.frame = CGRectMake(0, kViewMyZoneHeight * 0.2, kViewMyZoneWidth, kViewMyZoneHeight);
            self.myZoneViewController.view.alpha = 0.0;
            
        } completion:^(BOOL finished) {
            
            [self clearMyZone];
            CompanionPlayerState state = (CompanionPlayerState)[TVMixedCompanionManager sharedInstance].currentCompanionPlayerState;
            if (state != CompanionPlayerStateUnknown)
            {
                return;
            }
            else if ([self isKindOfClass:[PlayerViewController class]]){
                PlayerViewController *playerVC = (PlayerViewController*)self;
                [playerVC startTimer];
                
                if (playerVC.itemsPricesWithCoupons && ![playerVC playerIsPlaying]) {
                    [playerVC updateItemMainActionState];
                }
            }
        }];
    }
}


#pragma mark - Player View
-(void) loadEPGChannelsWithCompletion:(void(^)(void))completion {
    
    if (!EPGM.channelsList) {
        
        [self showHUDBlockingUI:YES];
        
        [EPGM loadEPGChannels:self completion:^(NSMutableArray *result) {
            
            [EPGM updateEPGList:result delegate:self completion:^(NSMutableArray *result) {
                [self hideHUD];
                if (completion)
                {
                    completion();
                }
            }];
            
        }];
    }
    else
    {
        if (completion)
        {
            completion();
        }
        
    }
}

-(void) playChannelWithEPGChannelID:(NSString *) channelID
{
    [self loadEPGChannelsWithCompletion:^{
        
        NSUInteger index = [EPGM.channelsList indexOfObjectPassingTest:^BOOL(TVMediaItem * obj, NSUInteger idx, BOOL *stop)
                            {
                                if ([obj.epgChannelID isEqualToString:channelID])
                                {
                                    return YES;
                                };
                                
                                return NO;
                                
                            }];
        
        TVMediaItem * mediaItem = nil;
        
        if (index != NSNotFound)
        {
            
        mediaItem = EPGM.channelsList[index];
        [self openMediaItem:mediaItem];
            

        }


    }];
    
}
- (void)playRecordedItem:(TVNPVRRecordItem *) recordedItem
{
    [self loadEPGChannelsWithCompletion:^{
        [self playRecordedItemAfterChannelListIsloaded:recordedItem];

    }];
}

-(void) playRecordedItemAfterChannelListIsloaded:(TVNPVRRecordItem *) recordedItem
{
    NPVRPlayerAdapter * playerNPVRAdapter = [[NPVRPlayerAdapter alloc] init];
    playerNPVRAdapter.recordedAsset = recordedItem;
    
    TVMediaItem *media = playerNPVRAdapter.relatedMediaItem;
    if (media) {
        [self openMediaItem:media  withProgram:nil withNPVR:playerNPVRAdapter];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"my_zone_items_recorded_no_channel_error_title") message:LS(@"my_zone_items_recorded_no_channel_error_description") delegate:self cancelButtonTitle:nil otherButtonTitles:LS(@"ok"), nil];
        [alertView show];
    }
}

-(void) openMediaItem:(TVMediaItem *)mediaItem withProgram:(APSTVProgram *) program
{
    [self openMediaItem:mediaItem withProgram:program withNPVR:nil];
}

-(void) openMediaItem:(TVMediaItem *)mediaItem withProgram:(APSTVProgram *) program withNPVR:(NPVRPlayerAdapter *) npvrPlayerAdapter
{
    if (NavM.container.centerViewController) {
        
        if (mediaItem.isSeries && isPad) {
            
            SeriesViewController *controller = [[ViewControllersFactory defaultFactory] seriesViewController];
            controller.seriesMediaItem = mediaItem;
            [NavM.container.centerViewController pushViewController:controller animated:YES];
            
        } else if (mediaItem.isMovie || mediaItem.isLive || mediaItem.isEpisode || (mediaItem.isSeries && isPhone) || npvrPlayerAdapter) {
            
            NSArray *controllers = [NavM.container.centerViewController viewControllers];
            
            for (PlayerViewController *controller in controllers) {
                if ([controller isKindOfClass:[PlayerViewController class]]) {
                    controller.liveTV = NO;
                    if (controller == [controllers lastObject]) {
                        
                        controller.programToPlay = program;
                        controller.npvrPlayerAdapter = npvrPlayerAdapter;
                        [controller selectMediaItem:mediaItem];
                        
                    } else {
                        controller.programToPlay = program;
                        controller.initialMediaItem = mediaItem;
                        controller.npvrPlayerAdapter = npvrPlayerAdapter;
                        [[self menuContainerViewController].centerViewController popToViewController:controller animated:NO];
                    }
                    return;
                }
            }
            
            if (!NavM.playerViewController) {
                NavM.playerViewController = [[ViewControllersFactory defaultFactory] playerViewController];
            }
            
            NavM.playerViewController.liveTV = NO;
            
            NavM.playerViewController.programToPlay = program;
            NavM.playerViewController.initialMediaItem = mediaItem;
            NavM.playerViewController.npvrPlayerAdapter = npvrPlayerAdapter;
            
            //fix for getting series name on ipad episide, to be able to check if there are more episodes in series
            if (isPad) {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self isKindOfClass: %@", [SeriesViewController class]];
                SeriesViewController *vc = [[controllers filteredArrayUsingPredicate:predicate] firstObject];
                NavM.playerViewController.seriesName = vc.seriesMediaItem.name;
            }
            
            [NavM.container.centerViewController pushViewController:NavM.playerViewController animated:YES];
            
        }
    }
}

-(void) openMediaItemWithMediaId:(NSString *) mediaID
{
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetMediaInfoWithMediaID:mediaID.integerValue mediaType:0 pictureSize:nil includeDynamicInfo:NO delegate:nil];
    
    [request setCompletionBlock:^{
    
        NSDictionary * mediaDictionary = [request JSONResponse];
        TVMediaItem * mediaItem = [[TVMediaItem alloc] initWithDictionary:mediaDictionary];
        [self openMediaItem:mediaItem];
    }];
    
    [request setFailedBlock:^{
        
    }];
    
    [self sendRequest:request];
}

- (void)openMediaItem:(TVMediaItem *)mediaItem {
    
    [self openMediaItem:mediaItem withProgram:nil];
}

#pragma mark - Search

- (void)searchProcess:(NSString *)searchStr {
    
}

- (void) intermediateLogin:(NSString*)segue {
    
    if ([segue isEqualToString:@"fbLogin"]) {
        SocialManagement *sm = [SocialManagement new];
        sm.delegateForProvider = self;
        
        [[sm socialProviderById:SocialProviderTypeFacebook] aquirePermissions:^(BOOL success) {
            if (success){
                [FBM requestForGetFBUserDataWithToken:^(TVFacebookUser *user) {
                    LoginViewController *controller = [[ViewControllersFactory defaultFactory] loginViewController];
                    controller.isNotRoot = YES;
                    controller.segue = segue;
                    controller.fbUser = user;
                    [self.navigationController pushViewController:controller animated:YES];
                }];
            }
            
            if (![AnalyticsM.currentEvent.category length]) {
                [AnalyticsM.currentEvent clearFields];
                AnalyticsM.currentEvent.category = @"Login and registration";
                AnalyticsM.currentEvent.action = @"Facebook User Sign in";
                AnalyticsM.currentEvent.label = @"Login screen";
            }
        }];
    }
    else {
        LoginViewController *controller = [[ViewControllersFactory defaultFactory] loginViewController];
            controller.isNotRoot = YES;
        controller.segue = segue;
        controller.fbUser = nil;
        [NavM.container.centerViewController pushViewController:controller animated:YES];
    }
}


-(void) noInternetViewSetup{
    self.noInternetView = [NoInternetView new];
    self.noInternetView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.noInternetView];
    
    CGFloat yLoc = [self.noInternetView convertPoint:CGPointZero fromView:[UIApplication sharedApplication].keyWindow].y;
    if (isPad) {
        if ([[self.view superview] superview]) {
            yLoc = -1;
        }
        else {
            yLoc = 64;
        }
    }
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_noInternetView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_noInternetView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_noInternetView(>=35)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_noInternetView)]];
    
    CGFloat constant = (yLoc < 0) ? 0 : 64;
    [self.view addConstraint: _noInternetView.constraintTopSpace = [NSLayoutConstraint constraintWithItem:_noInternetView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:constant]];
 
}

#pragma mark - companion

-(IBAction) openCompanionDiscovery:(id) sender
{
    [self toggleComapnionDiscovery];
}

-(void) toggleComapnionDiscovery
{
    if (isPad)
    {
        if (self.isCompanionDiscoveryOpned)
        {//close companion
            [self menuContainerViewController].panMode = (MFSideMenuPanMode)_panModeDefault;
            
            [UIView animateWithDuration:kViewAnimationDuration animations:^{
                
                _viewContent.frame = CGRectMake(0, 20, _viewContent.frame.size.width, _viewContent.frame.size.height);
                
                self.companionView.position = CGPointMake(0, self.companionView.height * 0.2);
                self.companionView.alpha = 0.0;
                
            } completion:^(BOOL finished) {
                
                self.isCompanionDiscoveryOpned = NO;
                self.companionView.companionPlayView.delegate = nil;
                UIView *viewTap = [[_viewContent subviews] lastObject];
                if (viewTap.tag == kViewCloseMyZoneTag) [viewTap removeFromSuperview];
                [self.companionView removeFromSuperview];
                self.companionView = nil;
                if (self.companionView != nil)
                {
                    NSLog(@"");
                }
            }];
        }
        else
        {
            if( self.companionView == nil )
            {
                self.companionView =  [CompanionView companionView];
                self.companionView.position = CGPointMake(0, self.companionView.height * 0.2);
                self.companionView.alpha = 1;
                if ([TVMixedCompanionManager sharedInstance].isConnected == NO)
                {
                    [self.companionView buttonScanPressed:nil];
                }
                self.companionView.tag = 800;
                [self.view insertSubview:self.companionView atIndex:0];
            }
//            else
//            {
//                [self.companionView updateUIAccordingToConnectionMode];
//            }
            
            CompanionPlayerState state = (CompanionPlayerState)[TVMixedCompanionManager sharedInstance].currentCompanionPlayerState;
            if ([TVMixedCompanionManager sharedInstance].isConnected == YES && (state == CompanionPlayerStatePlay || state == CompanionPlayerStatePause))
            {
                [self.companionView showCompanionPlayView];
            }

            [UIView animateWithDuration:kViewAnimationDuration animations:^{
                
                self.companionView.position = CGPointZero;
                self.companionView.alpha = 1.0;
                self.viewContent.position= CGPointMake(0, self.companionView.height);
                
            } completion:^(BOOL finished) {
                
                UIView *viewTap = [[UIView alloc] initWithFrame:_viewContent.bounds];
                viewTap.tag = kViewCloseMyZoneTag;
                [_viewContent addSubview:viewTap];
                UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeMyZone)];
                [tapRecognizer setCancelsTouchesInView:NO];
                [viewTap addGestureRecognizer:tapRecognizer];
                self.isCompanionDiscoveryOpned = YES;
            }];
        }
    }
}

#pragma mark - CompanionNavButtonDelegate

-(void) companionNavButtonPressed:(CompanionNavButton *)sender
{
    [self toggleComapnionDiscovery];
}

#pragma mark - TopBanners

-(UILabel *) createBannerWithText:(NSString *) text
{
    UILabel * banner = [[UILabel alloc] init];
    banner.font = [UIFont boldSystemFontOfSize:isPhone?12:14];
    banner = banner;
    banner.numberOfLines = 0;
    banner.text = text;
    banner.textColor = [UIColor whiteColor];
    CGSize size =[banner sizeThatFits:CGSizeMake(self.view.width,self.view.height)];
    NSInteger high = isPad? 42:(size.height+8);
    banner.size = CGSizeMake(self.view.size.width, high);
    banner = banner;
    banner.backgroundColor = APST.brandColor;
    banner.textAlignment = NSTextAlignmentCenter;
    banner.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    return banner;
}

- (void) showTopBannerWithText:(NSString *) text duration:(NSTimeInterval) timeInterval
{
    self.topBanner = [self createBannerWithText:text];
    self.topBanner.position = CGPointMake(0,isPad?self.viewNavigationBar.height+20:64);
    
    [self.view addSubview:self.topBanner];
    self.topBanner.alpha=0;
    [UIView animateWithDuration:0.3 animations:^{
        self.topBanner.alpha=1;
    } completion:^(BOOL finished)
    {
        if (timeInterval!= DBL_MAX)
        {
            [self performSelector:@selector(hideTopBanner) withObject:nil afterDelay:timeInterval];
        }
    }];
}

- (void) hideTopBanner
{
    [UIView animateWithDuration:0.3 animations:^{
        
        self.topBanner.alpha = 0;
    } completion:^(BOOL finished)
    {
        [self.topBanner removeFromSuperview];
    }];
}

-(NSString *) getCompanionBannerText
{
    TVAbstractCompanionDevice * connectedDevice = [[TVMixedCompanionManager sharedInstance] getCurrentConnectedDevice];
    return [NSString stringWithFormat:@"Connected to %@",connectedDevice.name];
}

-(void) addConstrainsForlinearLayoutRightToLeft:(NSArray *) array inSuperView:(UIView *) superView space:(CGFloat) space;
{

    for (UIView * view in array)
    {
        [view removeAllConstraints];
    }

    for (int i =0; i<array.count; i++)
    {
        ((UIView *)array[i]).translatesAutoresizingMaskIntoConstraints = NO;

        [superView addConstraint:[NSLayoutConstraint constraintWithItem:array[i] attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0]];

        if (i==0)
        {
            UIView * firstView = array[i];
            [superView addConstraint:[NSLayoutConstraint constraintWithItem:superView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:firstView attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:space]];
            
            [superView addConstraint:[NSLayoutConstraint constraintWithItem:firstView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:firstView.width]];
        }
        else
        {
            UIView * firstView = array[i-1];
            UIView * secondView =  array[i];

            [superView addConstraint:[NSLayoutConstraint constraintWithItem:firstView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:secondView attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:space]];
        }
    }
}

@end
