//
//  PageControlView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDPageControl.h"

extern const NSInteger PageControlBottomSpace;

@interface PageControlView : UIView

@property (nonatomic, assign) NSInteger numberOfPages;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) DDPageControl *control;

- (void)setCurrentPage:(NSInteger)page;

@end
