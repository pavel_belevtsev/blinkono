//
//  NoInternetView.h
//  KalturaUnified
//
//  Created by Dmytro Rozumeyenko on 1/27/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

@interface NoInternetView : UIView

@property (nonatomic, retain) NSLayoutConstraint *constraintTopSpace;

@end
