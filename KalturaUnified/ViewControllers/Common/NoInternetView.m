//
//  NoInternetView.m
//  KalturaUnified
//
//  Created by Dmytro Rozumeyenko on 1/27/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "NoInternetView.h"
#import <TvinciSDK/Reachability.h>

@implementation NoInternetView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        self.hidden = YES;
        [self showHide:[Reachability reachabilityForInternetConnection]];
    }
    return self;
}

- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* reachability = [note object];
    NSParameterAssert([reachability isKindOfClass:[Reachability class]]);
    
    [self showHide:reachability];
}

-(void) showHide:(Reachability* )reachability {
    [self layoutIfNeeded];
    if (reachability.currentReachabilityStatus == NotReachable) {
        if ([APP_SET isOffline])
           return;
        self.hidden = NO;
        
        NSString *message = LS(@"alert_no_internet_connection");
        void(^block)(UIButton *btn) = nil;
        if ([NU(UIUnit_Download_buttons) isUIUnitExist]) {
            message = LS(@"dtg_no_connection_title");
            block = ^(UIButton *btn) {
                self.hidden = YES;
                [[ErrorView sharedInstance] removeMe: self];
                [self removeAllSubviews];
                [[OfflineManager sharedInstance] setIsOffline:YES];
            };

        }
        
        [[ErrorView sharedInstance] showMessageOnTarget:message image:nil target:self dismissAfter:10 btnBlock:block];
    }
    else {
        self.hidden = YES;
        [self removeAllSubviews];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
