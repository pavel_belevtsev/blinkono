//
//  ViewControllersFactory.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EPGViewController.h"
#import "PlayerViewController.h"
#import "LoginViewController.h"
#import "LoginStartViewController.h"
#import "LoginForgotViewController.h"
#import "LoginMultiViewController.h"
#import "LoginRegisterViewController.h"
#import "LoginAssociateViewController.h"
#import "LoginInfoViewController.h"
#import "MyZoneViewController.h"
#import "SeriesViewController.h"
#import "HomeViewController.h"
#import "CategoryViewController.h"
#import "SettingsViewController.h"
#import "SettingsAboutUsViewController.h"
#import "SettingsEditViewController.h"
#import "SettingsListViewController.h"
#import "SettingFBUnmargeViewController.h"
#import "SettingsPrivacyViewController.h"
#import "SettingsAboutViewController.h"
#import "SettingsAccountViewController.h"
#import "CategorySortViewController.h"
#import "EPGSortViewControllerPhone.h"
#import "EPGChannelViewController.h"
#import "MyZoneInnerViewController.h"
#import "PlayerRelatedViewController.h"
#import "PlayerLiveChannelsViewController.h"
#import "PlayerEpisodesViewController.h"
#import "PlayerSeasonsViewController.h"
#import "MenuViewController.h"
#import "ViewController.h"
#import "LoginNavigationController.h"
#import "LoginAccountViewController.h"
#import "SubscriptionsListViewController.h"
#import "SettingDownloadsViewController.h"
#import "SettingDownloadsQualityViewController.h"
#import "SettingDownloadsQuotaViewController.h"

#define MAIN_STORYBOARD_NAME    @"Main"
#define LOGIN_STORYBOARD_NAME   @"Login"
#define EPG_STORYBOARD_NAME     @"EPG"
#define PLAYER_STORYBOARD_NAME  @"Player"
#define MYZONE_STORYBOARD_NAME  @"MyZone"
#define MYZONE_STORYBOARD_FULL_NAME @"MyZoneFull"
#define SETTINGS_STORYBOARD_NAME @"Settings"

@interface ViewControllersFactory : NSObject

+ (instancetype)defaultFactory;
- (void)becomeDefaultFactory;

- (ViewController *)initialViewController;
- (SeriesViewController*) seriesViewController;
- (HomeViewController*) homeViewController;
- (MenuViewController*) menuViewController;

- (UINavigationController *)contentViewController;
- (LoginNavigationController *)loginNavigationController:(UIViewController *)rootViewController;

- (EPGViewController *)epgViewController;
- (EPGSortViewControllerPhone*) epgSortViewControllerPhone;
- (EPGChannelViewController*) epgChannelViewController;

- (PlayerViewController *)playerViewController;
- (PlayerRelatedViewController*) playerRelatedViewController;
- (PlayerLiveChannelsViewController*) playerLiveChannelsViewController;
- (PlayerEpisodesViewController*) playerEpisodesViewController;
- (PlayerSeasonsViewController*) playerSeasonsViewController;

- (LoginViewController*) loginViewController;
- (LoginStartViewController*) loginStartViewController;
- (LoginForgotViewController*) loginForgotViewController;
- (LoginMultiViewController*) loginMultiViewController;
- (LoginRegisterViewController*) loginRegisterViewController;
- (LoginAssociateViewController*) loginAssociateViewController;
- (LoginInfoViewController*) loginInfoViewController;
- (LoginAccountViewController*) loginAccountViewController;

- (MyZoneViewController*) myZoneViewController;
- (MyZoneViewController*) myZoneFullScreenViewController;
- (MyZoneInnerViewController*) myZoneInnerViewControllerWithId:(NSString *)item  andMyZoneType:(NSString*)myZoneType;

- (CategoryViewController*) categoryViewController;
- (CategorySortViewController*) categorySortViewController;

- (SubscriptionsListViewController*) subscriptionsListViewController;

- (SettingsViewController*) settingsViewController;
- (SettingsAboutUsViewController*) settingsAboutUsViewController;
- (SettingsEditViewController*) settingsEditViewController;
- (SettingsListViewController*) settingsListViewController;
- (SettingFBUnmargeViewController*) settingFBUnmargeViewController;
- (SettingsPrivacyViewController*) settingsPrivacyViewController;
- (SettingsAboutViewController*) settingsAboutViewController;
- (SettingsAccountViewController*) settingsAccountViewController;
- (SettingDownloadsViewController*) settingDownloadsViewController;
- (SettingDownloadsQualityViewController*)settingDownloadsQualityViewController;
- (SettingDownloadsQuotaViewController*)settingDownloadsQuotaViewController;


@end
