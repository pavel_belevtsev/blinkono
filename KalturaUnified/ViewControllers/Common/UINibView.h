//
//  UINibView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 15.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityView.h"

@interface UINibView : UIView

- (id)initWithNib;
- (void)showHUD;
- (void)showHUDNoBack;
- (void)hideHUD;
- (void)updateSkin;

@property (nonatomic, strong) ActivityView *activityView;

@end
