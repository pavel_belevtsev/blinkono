//
//  ViewControllersFactory.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "ViewControllersFactory.h"
#import "SettingDownloadsQuotaViewController.h"


#define makeViewController(storyboard, viewController) [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([viewController class])]

@implementation ViewControllersFactory

static ViewControllersFactory *_sharedInstance = nil;

+ (instancetype)defaultFactory {
    
    @synchronized(self)
    {
        if (_sharedInstance == nil)
        {
            _sharedInstance = [[ViewControllersFactory alloc] init];
        }
    }
    
    return _sharedInstance;
}

- (void)becomeDefaultFactory {
    
    @synchronized (self)
    {
        if (_sharedInstance != self)
        {
            _sharedInstance = self;
        }
    }
}


#pragma - Uncategorized ViewControllers

- (ViewController *)initialViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[Utils nibName:MAIN_STORYBOARD_NAME] bundle:nil];
    return [storyboard instantiateInitialViewController];
}

- (SeriesViewController*) seriesViewController {
    return [[SeriesViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:MAIN_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (HomeViewController*) homeViewController {
    return [[HomeViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:MAIN_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (MenuViewController*) menuViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[Utils nibName:MAIN_STORYBOARD_NAME] bundle:nil];
    return makeViewController(storyboard, MenuViewController);
}

#pragma - Navigation Controllers

- (UINavigationController *)contentViewController {
   
    return [[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"ContentViewController"];
    
}

- (LoginNavigationController *)loginNavigationController:(UIViewController *)rootViewController {
    return [[LoginNavigationController alloc] initWithRootViewController:rootViewController];
}

#pragma - EPG ViewControllers

- (EPGViewController *)epgViewController {
    
    return [[EPGViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:EPG_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];

}

- (EPGSortViewControllerPhone*) epgSortViewControllerPhone {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[Utils nibName:EPG_STORYBOARD_NAME] bundle:[NSBundle mainBundle]];
    return makeViewController(storyboard, EPGSortViewControllerPhone);
}

- (EPGChannelViewController*) epgChannelViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[Utils nibName:EPG_STORYBOARD_NAME] bundle:[NSBundle mainBundle]];
    return makeViewController(storyboard, EPGChannelViewController);
}

#pragma - Player ViewControllers

- (PlayerViewController *)playerViewController {
    
    return [[PlayerViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:PLAYER_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (PlayerRelatedViewController*) playerRelatedViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[Utils nibName:PLAYER_STORYBOARD_NAME] bundle:[NSBundle mainBundle]];
    return makeViewController(storyboard, PlayerRelatedViewController);
}

- (PlayerLiveChannelsViewController*) playerLiveChannelsViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[Utils nibName:PLAYER_STORYBOARD_NAME] bundle:[NSBundle mainBundle]];
    return makeViewController(storyboard, PlayerLiveChannelsViewController);
}

- (PlayerEpisodesViewController*) playerEpisodesViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[Utils nibName:PLAYER_STORYBOARD_NAME] bundle:[NSBundle mainBundle]];
    return makeViewController(storyboard, PlayerEpisodesViewController);
}

- (PlayerSeasonsViewController*) playerSeasonsViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[Utils nibName:PLAYER_STORYBOARD_NAME] bundle:[NSBundle mainBundle]];
    return makeViewController(storyboard, PlayerSeasonsViewController);
}

#pragma - Login ViewControllers

- (LoginViewController*) loginViewController {
    return [[LoginViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (LoginStartViewController*) loginStartViewController{
    return [[LoginStartViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
}

- (LoginAccountViewController*) loginAccountViewController {
    return [[LoginAccountViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
}

- (LoginForgotViewController*) loginForgotViewController{
    return [[LoginForgotViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
}

- (LoginMultiViewController*) loginMultiViewController{
    return [[LoginMultiViewController  alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
}

- (LoginRegisterViewController*) loginRegisterViewController {
    return [[LoginRegisterViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
}

- (LoginAssociateViewController*) loginAssociateViewController {
    return [[LoginAssociateViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
}

- (LoginInfoViewController*) loginInfoViewController {
    return [[LoginInfoViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:LOGIN_STORYBOARD_NAME] bundle:nil]];
}

#pragma - My Zone ViewControllers

- (MyZoneViewController*) myZoneViewController
{
    return [[MyZoneViewController  alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:MYZONE_STORYBOARD_NAME] bundle:nil]];
}

- (MyZoneViewController*) myZoneFullScreenViewController
{
    return [[MyZoneViewController  alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:MYZONE_STORYBOARD_FULL_NAME] bundle:nil]];
}

- (MyZoneInnerViewController*) myZoneInnerViewControllerWithId:(NSString *)item andMyZoneType:(NSString*)myZoneType
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[Utils nibName:MYZONE_STORYBOARD_NAME] bundle:nil];
    if ([myZoneType isEqualToString:@"MyZoneFullPad"])
    {
        storyboard = [UIStoryboard storyboardWithName:[Utils nibName:MYZONE_STORYBOARD_FULL_NAME] bundle:nil];
    }
    return [storyboard instantiateViewControllerWithIdentifier:item];
}

#pragma - Category ViewControllers

- (CategoryViewController*) categoryViewController {
    return [[CategoryViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:MAIN_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (CategorySortViewController*) categorySortViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[Utils nibName:MAIN_STORYBOARD_NAME] bundle:[NSBundle mainBundle]];
    return makeViewController(storyboard, CategorySortViewController);
}

#pragma - Subscriptions List ViewController

- (SubscriptionsListViewController*) subscriptionsListViewController {
    return [[SubscriptionsListViewController alloc] init:[UIStoryboard storyboardWithName:@"SubscriptionsList" bundle:[NSBundle mainBundle]]];
}

//- (DTGViewController*) dtgViewController {
//    return [[DTGViewController alloc] init:[UIStoryboard storyboardWithName:@"DTGViewController" bundle:[NSBundle mainBundle]]];
//}

#pragma - Settings ViewControllers

- (SettingsViewController*) settingsViewController{
    return [[SettingsViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (SettingsAboutUsViewController*) settingsAboutUsViewController {
    return [[SettingsAboutUsViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (SettingsEditViewController*) settingsEditViewController {
    return [[SettingsEditViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (SettingsListViewController*) settingsListViewController {
    return [[SettingsListViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (SettingFBUnmargeViewController*) settingFBUnmargeViewController {
    return [[SettingFBUnmargeViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (SettingsPrivacyViewController*) settingsPrivacyViewController {
    return [[SettingsPrivacyViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (SettingsAboutViewController*) settingsAboutViewController {
    return [[SettingsAboutViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

- (SettingsAccountViewController*) settingsAccountViewController {
    return [[SettingsAccountViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

// SettingDownloadsViewController
- (SettingDownloadsViewController*) settingDownloadsViewController {
    return [[SettingDownloadsViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

-(SettingDownloadsQualityViewController*)settingDownloadsQualityViewController
{
    return [[SettingDownloadsQualityViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}

-(SettingDownloadsQuotaViewController*)settingDownloadsQuotaViewController
{
    return [[SettingDownloadsQuotaViewController alloc] init:[UIStoryboard storyboardWithName:[Utils nibName:SETTINGS_STORYBOARD_NAME] bundle:[NSBundle mainBundle]]];
}
@end
