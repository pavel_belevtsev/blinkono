//
//  ViewHelper.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 15.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewHelper : NSObject

+ (void)addBlockViewWithDurationAnimation:(CGFloat)duration;
+ (void)addBlockViewWithDurationAnimation:(CGFloat)duration delegate:(UIView *)view controller:(UIViewController *)controller;
+ (void)removeBlockViewWithDurationAnimation:(CGFloat)duration;

@end