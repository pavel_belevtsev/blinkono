//
//  ErrorView.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 6/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import UIKit

class ErrorView: UIView {

    let isPhone = (UIDevice.currentDevice().userInterfaceIdiom == .Phone)
    
    static var instance: ErrorView!
    var statusText : UILabel = UILabel()
    var statusImage : UIImageView = UIImageView()
    var btnClick : UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
    
    class func sharedInstance() -> ErrorView {
        self.instance = (self.instance ?? ErrorView())
        return self.instance
    }
    
    init() {
        super.init(frame: CGRectZero)
        self.tag = 1999
        self.createSubviews()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    func showMessageOnTarget(message: String, image: UIImage?, target: UIView, shouldDismiss: Bool, btnBlock: ((UIButton!) -> Void)?) {
        self.showMessageOnTarget(message, image: image, target: target, dismissAfter: (shouldDismiss) ? 5 : -1, btnBlock: btnBlock)
    }
    
    func showMessageOnTarget(message: String, image: UIImage?, target: UIView, shouldDismiss: Bool) {
        self.showMessageOnTarget(message, image: image, target: target, dismissAfter: (shouldDismiss) ? 5 : -1, btnBlock: nil)
    }
    
    func showMessageOnTarget(message: String, image: UIImage?, target: UIView) {
        self.showMessageOnTarget(message, image: image, target: target, dismissAfter: 5)
    }
    
    func showMessageOnTarget(message: String, image: UIImage?, target: UIView, dismissAfter: Double) {
        self.showMessageOnTarget(message, image: image, target: target, dismissAfter: dismissAfter, btnBlock: nil)
    }
    
    func showMessageOnTarget(message: String, image: UIImage?, target: UIView?, dismissAfter: Double, btnBlock: ((UIButton!) -> Void)?) {
        
        if statusText.text == message && self.alpha == 1 && self.superview == target {
            return
        }
        
        if let target = target {
            
            //update text
            statusText.text = message;
            
            //update image
            if let image = image {
                statusImage.image = image
            }
            else {
                statusImage.image = UIImage(named: "no_internet")
            }
            
            if self.superview != target {
                target.layoutIfNeeded()
                self.alpha = 0;
                self.setTranslatesAutoresizingMaskIntoConstraints(false)
                target.addSubview(self)
                target.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[me(target)]|",
                    options: NSLayoutFormatOptions.allZeros,
                    metrics: nil,
                    views: ["me": self, "target": target]))
                
                target.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[me]",
                    options: NSLayoutFormatOptions.allZeros,
                    metrics: nil,
                    views: ["me": self]))

                
                if let block = btnBlock {
                    btnClick.setTouchUpInsideAction(block)
                }
                
                target.hidden = false
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.alpha = 1;
                    
                })
                
                if dismissAfter > 0 {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(dismissAfter * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
                        self.removeMe()
                    }
                }
                self.layoutIfNeeded()
                
                target.superview?.bringSubviewToFront(target)
            }
        }
    }
    
    func createSubviews() {
        statusImage.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.addSubview(statusImage)
        
        let metrics = ["imageSize": NSNumber(integer: 33), "smallImageSize": NSNumber(integer: 18), "actionButtonRight": NSNumber(integer: 28)]
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[statusImage(imageSize)]",
            options: NSLayoutFormatOptions.allZeros,
            metrics: metrics,
            views: ["statusImage": statusImage]))

        statusText.setTranslatesAutoresizingMaskIntoConstraints(false)
        statusText.text = "";
        statusText.textColor = APSTypography.instance().brandTextColor;

        
        statusText.font = (isPhone) ? UIFont(name: "ProximaNova-Regular", size: 12) : UIFont(name: "ProximaNova-Bold", size: 14)
        statusText.numberOfLines = 10
        self.addSubview(statusText)
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[statusText]|",
            options: NSLayoutFormatOptions.allZeros,
            metrics: nil,
            views: ["statusText": statusText]))

        btnClick.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.addSubview(btnClick)
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[btn]-(actionButtonRight)-|",
            options: NSLayoutFormatOptions.allZeros,
            metrics: metrics,
            views: ["btn": btnClick]))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[btn]|",
            options: NSLayoutFormatOptions.allZeros,
            metrics: nil,
            views: ["btn": btnClick]))
        
        var side1 : UIView = UIView()
        side1.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.addSubview(side1)
        
        var side2 : UIView = UIView()
        side2.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.addSubview(side2)

        var btnClose : UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        btnClose.setTranslatesAutoresizingMaskIntoConstraints(false)
        btnClose.setImage(UIImage(named: "x_btn"), forState: UIControlState.Normal)
        self.addSubview(btnClose)
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[btnClose(smallImageSize)]",
            options: NSLayoutFormatOptions.allZeros,
            metrics: metrics,
            views: ["btnClose": btnClose]))
        btnClose.setTouchUpInsideAction { (btn: UIButton!) -> Void in
            self.removeMe()
        }
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[side1(side2)]-[statusImage(imageSize)]-10-[statusText]-[side2(>=side1)]-[btnClose(smallImageSize)]-10-|",
            options: NSLayoutFormatOptions.allZeros,
            metrics: metrics,
            views: ["statusImage": statusImage, "statusText": statusText, "side1": side1, "side2": side2, "btnClose": btnClose]))
        
        self.addConstraint(NSLayoutConstraint(item: statusImage, attribute: .CenterY, relatedBy: .Equal, toItem: statusText, attribute: .CenterY, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: btnClose, attribute: .CenterY, relatedBy: .Equal, toItem: statusText, attribute: .CenterY, multiplier: 1.0, constant: 0))

        self.backgroundColor = Utils.colorFromString("D64A4A")
    }
    
    func removeMe() {
        self.removeMe(self.superview)
    }
    
    func removeMe(target: UIView?) {
        //remove all actions for button
        self.btnClick.actions = NSMutableDictionary()
        
        if let superview = target {
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.alpha = 0;
                }, completion: { (sccess: Bool) -> Void in
                    superview.viewWithTag(self.tag)?.removeFromSuperview()
                    if superview.isKindOfClass(NSClassFromString("NoInternetView")) {
                        superview.hidden = true;
                    }
            })
        }
        

    }
}
