//
//  UINibView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 15.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"

@implementation UINibView

- (id)initWithNib {
    
    NSString *nibName = [Utils nibName:NSStringFromClass([self class])];
    
    if (![[NSBundle mainBundle] pathForResource:nibName ofType:@"nib"]) {
        nibName = NSStringFromClass([self class]);
    }
    
    if ((self = [[[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil] objectAtIndex:0])) {
        
    }
    
    return self;
    
}

#pragma mark - HUD

- (void)showHUD {
    
    if (self.activityView == nil) {
        
        self.activityView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityView" owner:self options:nil] objectAtIndex:0];
        self.activityView.frame = self.bounds;
        self.activityView.viewBackground.hidden = YES; // SYN-3122
        [self addSubview:self.activityView];
        
    }
    
}

- (void)showHUDNoBack {
    
    if (self.activityView == nil) {
        
        self.activityView = [[[NSBundle mainBundle] loadNibNamed:@"ActivityView" owner:self options:nil] objectAtIndex:0];
        self.activityView.frame = self.bounds;
        self.activityView.viewBackground.hidden = YES;
        [self addSubview:self.activityView];
        
    }
    
}

- (void)hideHUD {
    
    if (self.activityView) {
        
        [self.activityView removeFromSuperview];
        self.activityView = nil;
        
    }
    
}

#pragma mark - Skin

- (void)updateSkin {
    
    
}

@end
