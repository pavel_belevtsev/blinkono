//
//  ActivityView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "ActivityView.h"
#import "UIImageView+Animation.h"
#import "UIView+LoadNib.h"

@implementation ActivityView

- (void)awakeFromNib {

    [super awakeFromNib];
    
    [_imgSpinner startBrandAnimation];
    
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
    
}

- (void)skinChanged {

    if (!self.hidden) {
        [self restartHUD];
    }
}

- (void)dealloc {
    
    [_imgSpinner stopBrandAnimation];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)restartHUD {
    
    [_imgSpinner startBrandAnimation];
    
}



-(void) startAnimating
{
    self.hidden = NO;
    [_imgSpinner startBrandAnimation];
}

-(void) stopAnimating
{
    self.hidden = YES;
    [_imgSpinner stopAnimating];
    
}

+(ActivityView *) ActivityView
{
   return  [ActivityView loadNibName:nil];
}
@end
