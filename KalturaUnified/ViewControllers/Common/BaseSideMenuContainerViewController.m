//
//  BaseSideMenuContainerViewController.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 2/24/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseSideMenuContainerViewController.h"

@interface BaseSideMenuContainerViewController ()

@end

@interface MFSideMenuContainerViewController ()

- (void) handlePan:(UIPanGestureRecognizer *)recognizer;

@end

@implementation BaseSideMenuContainerViewController

+ (BaseSideMenuContainerViewController *)containerWithCenterViewController:(id)centerViewController
                                                  leftMenuViewController:(id)leftMenuViewController
                                                 rightMenuViewController:(id)rightMenuViewController {
    BaseSideMenuContainerViewController *controller = [BaseSideMenuContainerViewController new];
    controller.leftMenuViewController = leftMenuViewController;
    controller.centerViewController = centerViewController;
    controller.rightMenuViewController = rightMenuViewController;
    
    [controller setLeftMenuWidth:255];
    
    return controller;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    // If the gesture is on a UIControl instance we do not want to slide the menu, rather let the control
    // handle the event.: https://github.com/mikefrederick/MFSideMenu/issues/115
    
    
    //
    if ([touch.view isKindOfClass:[UISlider class]]) {
        return NO;
    }
    return [super gestureRecognizer:gestureRecognizer shouldReceiveTouch:touch];
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    UIView *view = [self.centerViewController view];
    
    
    CGPoint translatedPoint = [recognizer translationInView:view];
    if (translatedPoint.x > 0) {
        
        if (isPhone && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) && (self.menuState == MFSideMenuStateClosed)) {
            return;
        }
    }

    
    [super handlePan:recognizer];
    
}

@end
