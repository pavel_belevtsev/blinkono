//
//  BaseSideMenuContainerViewController.h
//  KalturaUnified
//
//  Created by Aviv Alluf on 2/24/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MFSideMenuContainerViewController.h"

// BaseSideMenuContainerViewController overrides the way MFSideMenuContainerViewController
// handles gestures, allowing controls to handle them before the view controller does, this
// allows for example a horizontal UISlider to function properly on it.
@interface BaseSideMenuContainerViewController : MFSideMenuContainerViewController

+ (BaseSideMenuContainerViewController *)containerWithCenterViewController:(id)centerViewController
                                                    leftMenuViewController:(id)leftMenuViewController
                                                   rightMenuViewController:(id)rightMenuViewController;

@end
