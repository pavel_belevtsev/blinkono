//
//  ActivityView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityView : UIView

- (void)restartHUD;

@property (nonatomic, weak) IBOutlet UIImageView *imgSpinner;
@property (nonatomic, weak) IBOutlet UIView *viewBackground;


+(ActivityView *) ActivityView;

-(void) startAnimating;
-(void) stopAnimating;

@end
