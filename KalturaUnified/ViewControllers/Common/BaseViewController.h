//
//  BaseViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TvinciSDK/TVNetworkQueue.h>
#import "TVUnderlineButton.h"
#import "ActivityView.h"
#import "NoInternetView.h"
#import "CompanionNavButton.h"
#import "CustomViewFactory.h"
#import "TVMediaItem+PSTags.h"

@class MyZoneViewController;
@class APSTVProgram;
@class CompanionView;
@class HomeNavProfileView;
@class TVNPVRRecordItem;

@interface BaseViewController : UIViewController

- (id)init:(UIStoryboard *)storyboard;
- (BOOL)showViewNavigation;
- (BOOL)showLogoNavigation;
- (void)sendRequest:(ASIHTTPRequest *)request;

- (void)initBrandButtonUI:(UIButton *)brandButton;
- (void)initOneColorBrandButtonUI:(UIButton *)brandButton;
- (void)initSelectedBrandButtonUI:(UIButton *)brandButton;
- (void)initClearAllButtonUI:(UIButton *)brandButton;
- (void)initFacebookButtonUI:(UIButton *)brandButton;
- (TVUnderlineButton *)createUnderlineButtonUI:(UIView *)viewButton font:(UIFont *)font;
- (void)initSwitchUI:(UISwitch *)switchRemeber;
- (void)initTextFieldUI:(UITextField *)textField;

- (void)closeKeyboard;

- (void)showHUDBlockingUI:(BOOL) blockingUI;
- (void)hideHUD;

- (void)requestForGetChannelMediaList:(NSInteger)channelID pageSize:(NSInteger)pageSize pageIndex:(NSInteger)pageIndex orderBy:(TVOrderBy)orderBy completion:(void(^)(NSArray *result))completion;
- (void)requestForGetChannelMediaList:(NSInteger)channelID orderBy:(TVOrderBy)orderBy completion:(void(^)(NSArray *result))completion;
- (void)requestForGetChannelMediaList:(NSInteger)channelID pageSize:(NSInteger)pageSize pageIndex:(NSInteger)pageIndex orderBy:(TVOrderBy)orderBy array:(NSMutableArray *)array completion:(void(^)(NSArray *result))completion;
- (void)requestForGetChannelMediaList:(NSInteger)channelID pageSize:(NSInteger)pageSize pageIndex:(NSInteger)pageIndex orderBy:(TVMultiFilterOrderBy)orderBy orderDir:(TVChannelMFOrderDirection)orderDir array:(NSMutableArray *)array completion:(void(^)(NSArray *result))completion;

- (void)requestForGetAreMediasFavorites:(NSArray *)mediaIds completion:(void(^)(NSArray *result))completion;
- (void)requestForGetMediaInfoWithMediaID:(NSInteger)mediaID completion:(void(^)(NSDictionary *result))completion;
- (void)requestForGetMediaInfoWithEPGChannelID:(NSString *)epgChannelID completion:(void(^)(NSDictionary *result))completion;
- (void)requestForGetUserStartedWatchingMediasWithNumOfItems:(NSInteger)numOfItems completion:(void(^)(NSArray *result))completion;
- (void)requestForGetUserExternalActionsShareWatches:(TVUserSocialActionType)action completion:(void(^)(BOOL result))completion;
- (void)requestForSearchMediaOrList:(NSArray *)orList AndList:(NSArray *)andList mediaType:(NSString *)mediaType pageSize:(NSInteger)pageSize pageIndex:(NSUInteger)pageIndex orderBy:(TVOrderByAndOrList)orderBy exact:(BOOL) exact completionBlock:(void(^)(id jsonResponse))completionBlock failedBlock:(void(^)(NSString* localizedError))failedBlock;
- (void)requestForEpgHightlightsFromValue:(int)from toValue:(int)to withPageSize:(NSInteger) pageSize andPageIndex:(NSInteger) pageIndex withCompletionBlock:(void (^)(NSArray* programsArray, NSError* error)) completion;

- (void)playRecordedItem:(TVNPVRRecordItem *) recordedItem;
- (void)openMediaItem:(TVMediaItem *)mediaItem;
- (void)openMediaItem:(TVMediaItem *)mediaItem withProgram:(APSTVProgram *) program;
- (void)openMediaItemWithMediaId:(NSString *) mediaID;
-(void) playChannelWithEPGChannelID:(NSString *) channelID;



- (void)searchProcess:(NSString *)searchStr;

- (void)initMyZone;
- (void)clearMyZone;

- (void)openMyZone;
- (void)openMyZoneWithActivityLoad;

- (void)closeMyZone;

- (void)intermediateLogin:(NSString*)segue;

- (void)restartHUD;

- (void)skinChanged;

-(void) toggleComapnionDiscovery;

- (void) showTopBannerWithText:(NSString *) text duration:(NSTimeInterval) timeInterval;
- (void) hideTopBanner;
- (UILabel *) createBannerWithText:(NSString *) text;
- (NSString *) getCompanionBannerText;

-(void) addConstrainsForlinearLayoutRightToLeft:(NSArray *) array inSuperView:(UIView *) superView space:(CGFloat) space;
-(void) reopenComapnionDiscovery;

@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (nonatomic, readwrite, strong) TVNetworkQueue *networkQueue;
@property (nonatomic, strong) ActivityView *activityView;
@property (nonatomic, strong) NSManagedObjectContext *privateContext;

@property (nonatomic, strong) NoInternetView *noInternetView;
@property (nonatomic, retain) NSLayoutConstraint *constraintTopSpace;

@property (strong, nonatomic) MyZoneViewController *myZoneViewController;
@property (nonatomic, readwrite) NSInteger panModeDefault;
@property (nonatomic,assign) BOOL loadNoInternetView;

@property (assign, nonatomic) BOOL isCompanionDiscoveryOpned;
@property (strong, nonatomic) CompanionView * companionView;
@property (strong, nonatomic) CompanionNavButton * companionNavButton;
@property (strong, nonatomic) HomeNavProfileView *homeNavProfileView;
@property (weak, nonatomic) IBOutlet UIView *viewNavigationBar;

@property (strong, nonatomic) UILabel * topBanner;


@end
