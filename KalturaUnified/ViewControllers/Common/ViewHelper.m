//
//  ViewHelper.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 15.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "ViewHelper.h"

UIView *blockView;
UIView *delegateView;

@implementation ViewHelper

+ (void)addBlockViewWithDurationAnimation:(CGFloat)duration {
    
//    [self addBlockViewWithDurationAnimation:duration delegate:nil];
    
}

+ (void)addBlockViewWithDurationAnimation:(CGFloat)duration delegate:(UIView *)view controller:(UIViewController *)controller {
    
    //UIApplication *appDelegate = [UIApplication sharedApplication];
    //UIView *rootView = [[[[appDelegate windows] objectAtIndex:0] subviews] lastObject];
    
    UIView *rootView = controller.view;
    CGRect rect = rootView.bounds;
    //if (isPad && (rect.size.width < rect.size.height)) {
    //    rect = CGRectMake(0, 0, rect.size.height, rect.size.width);
    //}
    blockView = [[UIView alloc] initWithFrame:rect];
    blockView.backgroundColor = [UIColor blackColor];
    
    [rootView addSubview:blockView];
	blockView.alpha = 0.0;
	
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBlockView:)];
    [blockView addGestureRecognizer:recognizer];
    
    delegateView = view;
    
    float alpha = 0.5;
    if (view) {
        alpha = 0.05;
    }
    
	[UIView animateWithDuration:duration animations:^{
        blockView.alpha = alpha;
	}];
}

+ (void)removeBlockViewWithDurationAnimation:(CGFloat)duration {	
	[UIView animateWithDuration:duration animations:^{
        blockView.alpha = 0;
    } completion:^(BOOL finished) {
		[blockView removeFromSuperview];
	}];
}

+ (void)tapBlockView:(UITapGestureRecognizer *)recognizer {
    
    if (delegateView) {
        
        [ViewHelper removeBlockViewWithDurationAnimation:0.3];
        [delegateView removeFromSuperview];
        
    }
}


@end
