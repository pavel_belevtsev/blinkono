//
//  SeriesViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 13.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SeriesViewController.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Tint.h"
#import "CategoryItemCollectionViewCell.h"
#import "MediaItemView.h"
#import "SeriesPopUpView.h"
#import "UIView+Additions.h"
#import "AnalyticsManager.h"

const NSInteger seriesViewPadItemsCount = 6;
const NSInteger seriesViewPadRowsCount = 2;
const NSInteger seriesViewPadColsCount = 3;

const CGFloat   popUpViewYTranslation = 30.0;
const CGFloat   popUpViewXOrigin = 352.0;
const CGFloat   popUpViewYOrigin = 132.0;

const CGFloat popUpShareSeriesViewXOrigin = 674.0;
const CGFloat popUpShareSeriesViewYOrigin = 90.0;

@interface SeriesViewController ()

@property (nonatomic, strong) SeriesPopUpView *episodePopUpView;
@property (nonatomic, strong) BasePopUp *popUpToShow;
@property (nonatomic, strong) BasePopUp *currentPopUp;
@property (nonatomic, strong) UIButton *btnOfPopUpToShow;
@property (nonatomic, strong) UIButton *currentbtnOfPopUp;

@end

@implementation SeriesViewController

- (void)viewDidLoad {
    
    self.loadNoInternetView = YES;

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Utils setLabelFont:_labelTitle bold:YES];
    
    [Utils setButtonFont:_buttonBack bold:YES];
    [_buttonBack setTitle:LS(@"back") forState:UIControlStateNormal];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [Utils setLabelFont:_labelTitle bold:YES];
    
    [Utils setLabelFont:_labelDescription];
    [Utils setLabelFont:_labelTags];
    
    [self addingPagerView];
    
    [_collectionViewEpisodes registerClass:[CategoryItemCollectionViewCell class] forCellWithReuseIdentifier:@"CategoryItemCollectionViewCell"];
    
    [self initializeEpisodePopUpView];

}

- (void)skinChanged {
    
    _labelLikeCounter.textColor = APST.brandTextColor;
    
    [_buttonLike setImage:[[UIImage imageNamed:@"details_like_icon"] imageTintedWithColor:APST.brandTextColor] forState:UIControlStateSelected];

    [_buttonLike setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    
    [_buttonLike setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    
    _imgLikeCounter.image = [[UIImage imageNamed:@"btn_like_counter_base"] imageTintedWithColor:APST.brandColor];

    UIImage *img = [UIImage imageNamed:@"details_down_arrow"];
    self.imgArrow.highlightedImage = [img imageTintedWithColor:APST.brandTextColor];
    
    [self updateButtonImages:_buttonFavorite];
    
    [self updateButtonImages:_buttonSeasons];
    
    [self updateButtonImages:_buttonShare];
    
    [_collectionViewEpisodes reloadData];
    
}

- (void)addingPagerView {
    CGRect pageControlFrame = _viewNavigation.bounds;
    self.viewPageControl = [[PageControlView alloc] initWithFrame:pageControlFrame];
    [self.viewPageControl.control addTarget:self action:@selector(pageControlChanged:) forControlEvents:UIControlEventValueChanged];
    self.viewPageControl.numberOfPages = 0;
    [_viewNavigation addSubview:self.viewPageControl];
}

- (void)initializeEpisodePopUpView {
    self.episodePopUpView =  [[SeriesPopUpView alloc] init];
    self.episodePopUpView.frame = CGRectMake(popUpViewXOrigin, popUpViewYOrigin, CGRectGetWidth(self.episodePopUpView.frame), CGRectGetHeight(self.episodePopUpView.frame));
    self.episodePopUpView.popUpRectView = CGRectMake(popUpViewXOrigin, popUpViewYOrigin, CGRectGetWidth(self.episodePopUpView.frame), CGRectGetHeight(self.episodePopUpView.frame));
    self.episodePopUpView.popUpSuperView = self.view;
    self.episodePopUpView.seasonTableView.delegate = self;
    self.episodePopUpView.seasonTableView.dataSource = self;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    super.panModeDefault = MFSideMenuPanModeNone;
    [self menuContainerViewController].panMode = (int)super.panModeDefault;
    
    if (!isInitialized) {
        isInitialized = YES;
        
        _labelTitle.text = _seriesMediaItem.name;
        
        [_imgViewThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:_seriesMediaItem andSize:CGSizeMake(_imgViewThumb.frame.size.width * 2, _imgViewThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_2_3.png"]];
        
        _labelLikeCounter.highlightedTextColor = CS(@"bebebe");
        
        [Utils setLabelFont:_labelLikeCounter bold:YES];
        _labelLikeCounter.text = [Utils getStringFromInteger: _seriesMediaItem.likeCounter];
        
        [_buttonLike setImage:[[UIImage imageNamed:@"details_like_icon"] imageTintedWithColor:CS(@"bebebe")] forState:UIControlStateNormal];
        
        [_buttonLike setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"575757")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
        
        [_buttonLike.titleLabel setFont:_labelLikeCounter.font];
        
        _imgLikeCounter.highlightedImage = [[UIImage imageNamed:@"btn_like_counter_base"] imageTintedWithColor:CS(@"575757")];
        
        self.likeAdapter.hideButtonTitleForLikes = NO;
        [self.likeAdapter setupMediaItem:_seriesMediaItem];
        [self.favoriteAdapter setupMediaItem:_seriesMediaItem];
        
        [_buttonFavorite setTitle:LS(@"favorite") forState:UIControlStateNormal];
        [self updateButtonImages:_buttonFavorite];
        
        [_buttonSeasons setTitle:LS(@"seasons") forState:UIControlStateNormal];
        [self updateButtonImages:_buttonSeasons];
        
        [_buttonShare setTitle:LS(@"share") forState:UIControlStateNormal];
        [self updateButtonImages:_buttonShare];
        _buttonShare.UIUnitID = NU(UIUnit_Social_buttons);
        [_buttonShare activateAppropriateRule];

        
        _labelDescription.numberOfLines = 0;
        _labelDescription.text = _seriesMediaItem.mediaDescription;
        
        _labelTags.text = @"";
        
        if ([_seriesMediaItem.tags objectForKey:@"Genre"] && [[_seriesMediaItem.tags objectForKey:@"Genre"] count] > 0) {
            
            if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
            
            _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text, [[_seriesMediaItem.tags objectForKey:@"Genre"] firstObject]];
        }

        if ([_seriesMediaItem.metaData objectForKey:@"Release year"] && [[_seriesMediaItem.metaData objectForKey:@"Release year"] length] > 0) {
            
            if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
            
            _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text, [_seriesMediaItem.metaData objectForKey:@"Release year"]];
        }
        
        if ([_seriesMediaItem.metaData objectForKey:@"Rating"] && [[_seriesMediaItem.metaData objectForKey:@"Rating"] length] > 0) {
            
            if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
            
            _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text, [_seriesMediaItem.metaData objectForKey:@"Rating"]];
        }
        
        if ([_seriesMediaItem.tags objectForKey:@"Country"] && [[_seriesMediaItem.tags objectForKey:@"Country"] count] > 0) {
            
            if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
            
            _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text, [[_seriesMediaItem.tags objectForKey:@"Country"] firstObject]];
        }
        /*”PSC : ONO : OS-91 Ono - CR - Rating needs ot be presented in each media page*/

        if ([_seriesMediaItem.tags objectForKey:@"Parental Rating"]){// && [[_item.tags objectForKey:@"Parental Rating"] count] > 0) {
            
            if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
            
            _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text,  LS([Utils ratingToShow:_seriesMediaItem.tags[@"Parental Rating"]])];
        }

        CGRect descFrame = [_labelDescription textRectForBounds:CGRectMake(0, 0, _scrollViewContent.frame.size.width, CGFLOAT_MAX) limitedToNumberOfLines:0];
        _labelDescription.frame = CGRectMake(0.0, 0.0, _labelDescription.frame.size.width, descFrame.size.height + 5);
        
        _labelTags.frame = CGRectMake(_labelTags.frame.origin.x, _labelDescription.frame.origin.y + _labelDescription.frame.size.height + 10.0, _labelTags.frame.size.width, _labelTags.frame.size.height);
        
        _scrollViewContent.contentSize = CGSizeMake(_scrollViewContent.frame.size.width, _labelTags.frame.origin.y + _labelTags.frame.size.height + 10.0);
        
        [self showHUDBlockingUI:YES];
        
        self.fullList = [[NSMutableArray alloc] init];
        [self loadEpisodes:0];
        
        
        UIImage *img = [UIImage imageNamed:@"details_down_arrow"];
        self.imgArrow = [[UIImageView alloc] initWithImage:[img imageTintedWithColor:[UIColor lightGrayColor]]];
        
        self.imgArrow.frame = CGRectMake(_buttonSeasons.frame.size.width - 24.0, (int)((_buttonSeasons.frame.size.height - img.size.height) / 2.0), img.size.width, img.size.height);
        [_buttonSeasons addSubview:_imgArrow];
        
        [_buttonSeasons addTarget:self action:@selector(buttonSeasonsTouchDown:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragInside];
        [_buttonSeasons addTarget:self action:@selector(buttonSeasonsTouchCancel:) forControlEvents:UIControlEventTouchCancel | UIControlEventTouchDragOutside];
        
        [self initializeSocialFeedPopUpView];
        
        self.socialManager = [[SocialManagement alloc] init];
        _socialManager.delegate = self;
        [_socialManager updateWithMedia:_seriesMediaItem];
        
        [self skinChanged];
    }
}

- (LikeAdapter*) likeAdapter {
    if (!_likeAdapter)
        _likeAdapter = [[LikeAdapter alloc] initWithMediaItem:_seriesMediaItem control:_buttonLike label:nil imgLike:_imgLikeCounter labelCounter:_labelLikeCounter];
    return _likeAdapter;
}

- (FavoriteAdapter*) favoriteAdapter {
    if (!_favoriteAdapter)
        _favoriteAdapter = [[FavoriteAdapter alloc] initWithMediaItem:_seriesMediaItem control:_buttonFavorite];
    return _favoriteAdapter;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    _buttonShare.userInteractionEnabled  = [LoginM isSignedIn];
    _buttonFavorite.userInteractionEnabled  = [LoginM isSignedIn];
    _buttonLike.userInteractionEnabled  = [LoginM isSignedIn];
}

- (void)initializeSocialFeedPopUpView {
    
    self.shareContentView = [[ShareContentView alloc] initWithNib];
    _shareContentView.delegate = self;
    
    self.sharePopUpView =  [[SharePopUpView alloc] initWithArrowDirection:kSharePopUpViewArrowDirectionUp contentView:_shareContentView];
    self.sharePopUpView.frame = CGRectMake(popUpShareSeriesViewXOrigin, popUpShareSeriesViewYOrigin, CGRectGetWidth(self.sharePopUpView.frame), CGRectGetHeight(self.sharePopUpView.frame));

    self.sharePopUpView.frame = CGRectMake(self.sharePopUpView.frame.origin.x, self.sharePopUpView.frame.origin.y + _shareContentView.viewPrivately.frame.size.height, self.sharePopUpView.frame.size.width, self.sharePopUpView.frame.size.height - _shareContentView.viewPrivately.frame.size.height);
    
    self.sharePopUpView.popUpRectView = CGRectMake(self.sharePopUpView.frame.origin.x, self.sharePopUpView.frame.origin.y, CGRectGetWidth(self.sharePopUpView.frame), CGRectGetHeight(self.sharePopUpView.frame));
    self.sharePopUpView.popUpSuperView = self.view;
    self.sharePopUpView.isDismissAutomatically = YES;
    self.sharePopUpView.popUpAnimationType = TVNPopUpAnimationTypeUpToDown;
}

- (void)buttonSeasonsTouchDown:(UIButton *)button {
    
    if (_imgArrow.highlighted == button.selected) {
        _imgArrow.highlighted = !button.selected;
    
        [self rotateImgArrow];
    }
}

- (void)buttonSeasonsTouchCancel:(UIButton *)button {
    
    if (_imgArrow.highlighted) {
        _imgArrow.highlighted = NO;
    
        [self rotateImgArrow];
    }
}

- (void)rotateImgArrow {
    
    [UIView animateWithDuration:0.2f animations:^{
        [_imgArrow setTransform:CGAffineTransformMakeRotation(_imgArrow.highlighted ? M_PI : 0)];
    }];
}

- (void)updateButtonImages:(UIButton *)button {

    [Utils setButtonFont:button bold:YES];
    
    UIImage *img = button.imageView.image;
    
    if (img) {
        [button setImage:[img imageTintedWithColor:CS(@"bebebe")] forState:UIControlStateNormal];
        [button setImage:[img imageTintedWithColor:APST.brandTextColor] forState:UIControlStateHighlighted];
        [button setImage:[img imageTintedWithColor:APST.brandTextColor] forState:UIControlStateSelected];
    }
    
    [button setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"4e4e4e")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    [button setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateHighlighted];
    [button setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    
    
    [button setTitleColor:CS(@"bebebe") forState:UIControlStateNormal];
    [button setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [button setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
}

#pragma mark - Episodes

- (void)loadEpisodes:(int)page {
    
    NSString *episodeTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeEpisode];
    NSDictionary *tagsDictionary = [NSDictionary  dictionaryWithObjectsAndKeys:_seriesMediaItem.name, @"Series Name", nil];
    if (![_seriesMediaItem.name length]) tagsDictionary = [NSDictionary  dictionaryWithObjectsAndKeys:@"Californication", @"Series Name", nil];
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForSearchMediaByAndOrList:@[tagsDictionary] AndList:@[] mediaType:episodeTypeID pageSize:maxPageSize pageIndex:page orderBy:TVOrderByAndOrList_NAME orderDir:TVOrderDirectionByAndOrList_Ascending exact:NO orderMeta:@"" delegate:nil];
//    __weak TVPAPIRequest *request = [TVPMediaAPI requestForSearchMediaByMetaDataPairs:nil orTagPairs:tagsDictionary mediaType:episodeTypeID pictureSize:[TVPictureSize iPadItemCellPictureSize] pageSize:maxPageSize pageIndex:page orderBy:TVOrderByName delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
        
        if ([result count]) {
            
            for (id item in result) {
                [_fullList addObject:item];
            }
            
        }
        
        if ([result count] == maxPageSize) {
            
            [self loadEpisodes:page + 1];
            
        } else {
            
            [self sortEpisodes];
            
        }
        
    }];
    
    [request setFailedBlock:^{
        
        
    }];
    
    [self sendRequest:request];
}

- (void)sortEpisodes {
    
    self.dictionarySeasonsBySeasonNumber = [Utils mapEpisodesBySeasonsNumber:_fullList];
    self.seasons = [Utils sortSeasonsNumbers:_dictionarySeasonsBySeasonNumber.allKeys];

    [self updateEpisodes];
}

- (NSArray *)episodesArray {
    
    if ([_seasons count]) {
        
        if (_currentSeason < 0 || _currentSeason >= [_seasons count])  {
            _currentSeason = 0;
        }
        
        NSString *season = [_seasons objectAtIndex:_currentSeason];
        
        return [_dictionarySeasonsBySeasonNumber objectForKey:season];
        
    } else {
        
        return [NSArray array];
        
    }
}

- (void)updateEpisodes {
    
    [self hideHUD];
    
    _viewPageControl.numberOfPages = 0;
    _viewPageControl.currentPage = 0;
    
    if (_viewPageControl.numberOfPages == 0) {
        NSInteger count = [[self episodesArray] count];
        self.viewPageControl.numberOfPages = (count / seriesViewPadItemsCount) + ((count % seriesViewPadItemsCount) ? 1 : 0);
        [_collectionViewEpisodes scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    }
    
    [_collectionViewEpisodes reloadData];
    
    if ([_seasons count]) {
        [_buttonSeasons superview].hidden = NO;
        
        if (_currentSeason < 0 || _currentSeason >= [_seasons count])  {
            _currentSeason = 0;
        }
        
        [_buttonSeasons setTitle:[NSString stringWithFormat:LS(@"season"), [_seasons objectAtIndex:_currentSeason]] forState:UIControlStateNormal];
    }
}

#pragma mark - UITableView data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return TVNEpisodePopUpViewHeightCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _seasons.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        UIImage *bgimg = [[UIImage imageNamed:@"settings_sidebar_shadow"] stretchableImageWithLeftCapWidth:6 topCapHeight:4];
        cell.backgroundView = [[UIImageView alloc] initWithImage:bgimg];
        
    }
    
    UIView *selectedBGView = [[UIView alloc] init];
    selectedBGView.backgroundColor = APST.brandColor;
    cell.selectedBackgroundView = selectedBGView;

    cell.textLabel.textColor = CS(@"bebebe");
    cell.textLabel.highlightedTextColor = APST.brandTextColor;
    cell.textLabel.font = Font_Bold(18);
    cell.textLabel.text = [NSString stringWithFormat:LS(@"season"), [_seasons objectAtIndex:indexPath.row]];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if (indexPath.row == _currentSeason){
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:YES];
    }
    
    return cell;
}

#pragma mark - UITableView delegate
// Every time the user selects a season in the popup section

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row != _currentSeason) {
        
        _currentSeason = indexPath.row;
        
        [self updateEpisodes];
        
    }

    [self buttonSeasonsPressed:_buttonSeasons];
}


#pragma mark - Collection View Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return self.viewPageControl.numberOfPages;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return seriesViewPadItemsCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CategoryItemCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryItemCollectionViewCell" forIndexPath:indexPath];
    cellView.mediaItemView.delegateController = self;
    
    NSInteger index = indexPath.row;
    if (index % seriesViewPadRowsCount == 0) {
        index = index / seriesViewPadRowsCount;
    } else {
        index = index / seriesViewPadRowsCount;
        index += seriesViewPadColsCount;
    }
    //NSLog(@"index %d", index);
    
    index += (indexPath.section * seriesViewPadItemsCount);
    
    NSArray *array = [self episodesArray];
    
    if (index < [array count]) {
        
        TVMediaItem *item = [array objectAtIndex:index];
        [cellView updateCellWithMediaItem:item];
        [cellView setEpisodeNumber:[[item getMediaItemEpisodeNumber] intValue]];
    } else {
        
        [cellView clearMediaCell];
        
    }
    return cellView;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}


#pragma mark -

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = CGRectGetWidth(scrollView.frame);
    int page = scrollView.contentOffset.x / pageWidth;
    [self.viewPageControl setCurrentPage:page];
}

- (void)pageControlChanged:(id)sender {
    UIPageControl *pageControl = sender;
    CGFloat pageWidth = _collectionViewEpisodes.frame.size.width;
    CGPoint scrollTo = CGPointMake(pageWidth * pageControl.currentPage, 0);
    [_collectionViewEpisodes setContentOffset:scrollTo animated:YES];
}

#pragma mark - Buttons actions

- (IBAction)buttonBackPressed:(UIButton *)button {

    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)buttonFavoritePressed:(UIButton *)button {
    
    NSLog(@"");
}

- (IBAction)buttonSeasonsPressed:(UIButton *)button {
    
    if (self.episodePopUpView.superview) {
        button.selected = NO;
        [self.episodePopUpView dismissPopUp];
        
    }
    else {
        if (![self.view.subviews containsObject:self.currentPopUp]) {//there is no popup in the screen
            button.selected = YES;
            self.currentPopUp = self.episodePopUpView;
            self.currentPopUp.popUpDelegate = self;
            self.currentbtnOfPopUp = button;
            [self.episodePopUpView showPopUp];
        } else {//there is a popup in the screen, first remove than add other
            [self.currentPopUp dismissPopUp];
            self.popUpToShow = self.episodePopUpView;
            self.btnOfPopUpToShow = button;
        }
    }
    
    if (_imgArrow.highlighted != button.selected) {
        _imgArrow.highlighted = button.selected;
        
        [self rotateImgArrow];
    }
}

- (IBAction)buttonSharePressed:(UIButton *)button {
    
    if (_imgArrow.highlighted) {
        _imgArrow.highlighted = NO;
        [self rotateImgArrow];
    }
    
    [_shareContentView setupWithMedia:_seriesMediaItem];
    
    [self showSocialFeedPopUp:button];
}

- (void)openMyZone {

    if (_buttonShare.selected) {
        [self buttonSharePressed:_buttonShare];
    }
    if (_buttonSeasons.selected) {
        [self buttonSeasonsPressed:_buttonSeasons];
    }
    
    [super openMyZone];
}

#pragma mark - Social

- (void)showSocialFeedPopUp:(UIButton *)button {
    
    if (self.sharePopUpView.superview) {
        button.selected = NO;
        [self.sharePopUpView dismissPopUp];
    }
    else {
        if (![self.view.subviews containsObject:self.currentPopUp]) {//there is no popup in the screen
            button.selected = YES;
            self.currentPopUp = self.sharePopUpView;
            self.currentPopUp.popUpDelegate = self;
            self.currentbtnOfPopUp = button;
            
            [self.sharePopUpView showPopUp];
            
        } else {//there is a popup in the screen, first remove than add other
            [self.currentPopUp dismissPopUp];
            self.popUpToShow = self.sharePopUpView;
            self.btnOfPopUpToShow = button;
            
        }
    }
}

#pragma mark - TVNBasePopUpDelegate

- (void)didAddPopUp:(BasePopUp *)popUp {
    //[self bringSubviewToFront:self.middleNavigationBar];
}

- (void)didEndAddPopUp:(BasePopUp*)popUp {
}

- (void)didDismissPopUp:(BasePopUp*)popUp autoDismiss:(BOOL)autoDismiss{
    self.currentbtnOfPopUp.selected = NO;
    self.currentPopUp.popUpDelegate = nil;
    self.currentPopUp = nil;
    if (self.popUpToShow) { //threre is a popup to show
        [self.popUpToShow showPopUp];
        self.btnOfPopUpToShow.selected = YES;
        
        self.currentPopUp = self.popUpToShow;
        self.currentbtnOfPopUp = self.btnOfPopUpToShow;
        self.currentPopUp.popUpDelegate = self;
        self.popUpToShow = nil;
        self.btnOfPopUpToShow = nil;
    }
}

#pragma mark - ShareContentViewDelegate

- (void)shareAction:(NSInteger)socialAction {
    
    [_socialManager shareAction:socialAction];
}

#pragma mark - SocialManagementDelegate

- (void)dismissSocialShareView {
    
    [self.sharePopUpView dismissPopUp];
    self.btnOfPopUpToShow.selected = !self.btnOfPopUpToShow.selected;
    
}

- (void)showCommentView:(UIView *)viewComment {
    //[self stopFullScreenModeTimer];
    
    [UIView animateWithDuration:kViewAnimationDuration animations:^{
        [ViewHelper addBlockViewWithDurationAnimation:kViewAnimationDuration delegate:viewComment controller:self];
    } completion:^(BOOL finished) {
        //UIApplication *appDelegate = [UIApplication sharedApplication];
        //UIView *rootView = [[[[appDelegate windows] objectAtIndex:0] subviews] lastObject];
        //[viewComment setFrame:CGRectMake((rootView.width - viewComment.width) / 2, (rootView.height - viewComment.height) / 2,viewComment.width,viewComment.height)];
        
        viewComment.center = CGPointMake(self.view.width / 2, self.view.height / 2);
        [self.view addSubview:viewComment];
        
    }];
}

- (void)removeCommentView {
    //[self shareAction:nil];
    //[self resetFullScreenModeTimer];
    [ViewHelper removeBlockViewWithDurationAnimation:kViewAnimationDuration];
}

- (void)presentViewController:(UIViewController *)controller {
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)dismissViewController {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)loginWithFB
{
    
}


#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Player View

- (void)openMediaItem:(TVMediaItem *)mediaItem {
    [super openMediaItem:mediaItem];
    [AnalyticsM trackEpisodeScreen:_seriesMediaItem episode:mediaItem];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
