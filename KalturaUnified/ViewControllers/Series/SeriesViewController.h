//
//  SeriesViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 13.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseViewController.h"
#import "LikeAdapter.h"
#import "FavoriteAdapter.h"
#import "PageControlView.h"
#import "HomeNavProfileView.h"
#import "ShareContentView.h"
#import "SharePopUpView.h"
#import "SocialManagement.h"

@interface SeriesViewController : BaseViewController <HomeNavProfileViewDelegate, UITableViewDelegate, UITableViewDataSource, BasePopUpDelegate, ShareContentViewDelegate, SocialManagementDelegate> {
    BOOL isInitialized;
}

@property (strong, nonatomic) TVMediaItem *seriesMediaItem;
@property (nonatomic, strong) LikeAdapter *likeAdapter;
@property (nonatomic, strong) FavoriteAdapter *favoriteAdapter;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewThumb;

@property (nonatomic, weak) IBOutlet UIButton *buttonLike;
@property (nonatomic, weak) IBOutlet UIImageView *imgLikeCounter;
@property (nonatomic, weak) IBOutlet UILabel *labelLikeCounter;

@property (nonatomic, weak) IBOutlet UIButton *buttonFavorite;

@property (nonatomic, weak) IBOutlet UIButton *buttonSeasons;
@property (nonatomic, weak) IBOutlet UIButton *buttonShare;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewContent;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UILabel *labelTags;

@property (nonatomic, strong) NSMutableArray *fullList;
@property (nonatomic, strong) NSDictionary *dictionarySeasonsBySeasonNumber;
@property (nonatomic, strong) NSArray *seasons;
@property NSInteger currentSeason;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionViewEpisodes;
@property (nonatomic, weak) IBOutlet UIView *viewNavigation;
@property (nonatomic, strong) PageControlView *viewPageControl;

@property (nonatomic, strong) UIImageView *imgArrow;

@property (nonatomic, strong) ShareContentView *shareContentView;
@property (nonatomic, strong) SharePopUpView *sharePopUpView;

@property (nonatomic, strong) SocialManagement *socialManager;


@end
