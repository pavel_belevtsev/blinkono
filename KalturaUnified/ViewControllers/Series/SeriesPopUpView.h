//
//  SeriesPopUpView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 13.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePopUp.h"

extern const CGFloat TVNEpisodePopUpViewHeightCell;

@interface SeriesPopUpView : BasePopUp
@property (assign) BOOL isShowing;

@property (weak, nonatomic) IBOutlet UITableView *seasonTableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBg;
@property (weak, nonatomic) IBOutlet UIImageView *imgLeft;
@property (weak, nonatomic) IBOutlet UIImageView *imgRight;
@property (weak, nonatomic) IBOutlet UIImageView *imgMiddle;

- (void)setViewForNumItems:(int)numItems;

@end
