//
//  SeriesPopUpView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 13.01.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SeriesPopUpView.h"

const CGFloat TVNEpisodePopUpViewHeightCell = 44.0f;
const int TVNEpisodePopUpViewNumberOfDisplayItems = 5;
NSString * const TVNEpisodePopUpViewBubbleLeft =  @"showpage_share_bubble_left";
NSString * const TVNEpisodePopUpViewBubbleRight =  @"showpage_share_bubble_right";
NSString * const TVNEpisodePopUpViewBubbleMiddle =  @"showpage_share_bubble_mid";
const CGFloat TVNEpisodePopUpViewBottomSpace= 16.0f;


@implementation SeriesPopUpView

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    if (self) {
        self.isDismissAutomatically = NO;
    }
    return self;
}

- (void)awakeFromNib {
    UIImage *image = [UIImage imageNamed:TVNEpisodePopUpViewBubbleLeft];
    UIImage *sizeableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(40, 18, 20, 0)];
    self.imgLeft.image =sizeableImage;
    
    image = [UIImage imageNamed:TVNEpisodePopUpViewBubbleMiddle];
    sizeableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(40, 0, 20, 0)];
    self.imgMiddle.image =sizeableImage;
    
    image = [UIImage imageNamed:TVNEpisodePopUpViewBubbleRight];
    sizeableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(40, 0, 20, 18)];
    self.imgRight.frame = CGRectMake(CGRectGetMaxX(self.imgMiddle.frame), CGRectGetMinY(self.imgRight.frame), CGRectGetWidth(self.imgRight.frame), CGRectGetHeight(self.imgRight.frame));
    self.imgRight.image =sizeableImage;
    
    self.seasonTableView.layer.cornerRadius = 3.0f;
    [self.seasonTableView setTableFooterView:[UIView new]];
}

- (void)setViewForNumItems:(int)numItems{
    CGFloat heightTable;
    if (numItems<TVNEpisodePopUpViewNumberOfDisplayItems) {
        heightTable = numItems*TVNEpisodePopUpViewHeightCell;
    } else{
        heightTable = TVNEpisodePopUpViewHeightCell*TVNEpisodePopUpViewNumberOfDisplayItems;
    }

    self.seasonTableView.frame = CGRectMake(CGRectGetMinX(self.seasonTableView.frame), CGRectGetMinY(self.seasonTableView.frame), CGRectGetWidth(self.seasonTableView.frame),heightTable-2);
   
    CGFloat totalHeightPopUp = CGRectGetMaxY(self.seasonTableView.frame) + TVNEpisodePopUpViewBottomSpace;
    self.imgRight.frame = CGRectMake(CGRectGetMinX(self.imgRight.frame), CGRectGetMinY(self.imgRight.frame), CGRectGetWidth(self.imgRight.frame), totalHeightPopUp);
    self.imgLeft.frame = CGRectMake(CGRectGetMinX(self.imgLeft.frame), CGRectGetMinY(self.imgLeft.frame), CGRectGetWidth(self.imgLeft.frame), totalHeightPopUp);
    self.imgMiddle.frame = CGRectMake(CGRectGetMinX(self.imgMiddle.frame), CGRectGetMinY(self.imgMiddle.frame), CGRectGetWidth(self.imgMiddle.frame), totalHeightPopUp);
    self.frame = CGRectMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame), CGRectGetWidth(self.frame), totalHeightPopUp);
    self.popUpRectView = self.frame;
}

@end