//
//  CompanionNavButton.m
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CompanionNavButton.h"
#import "UIView+LoadNib.h"
#import <TvinciSDK/TVMixedCompanionManager.h>
#import "UIImage+Tint.h"

@implementation CompanionNavButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [self.buttonCompanion setImage:[UIImage imageNamed:@"chromecast_icon_not-connected"] forState:UIControlStateNormal];
    [self.buttonCompanion setImage:[[UIImage imageNamed:@"chromecast_icon_connected"]imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
    
    [self registerTVCompanionMixedNotification];
    
    if ([[TVMixedCompanionManager sharedInstance] isConnected])
    {
        [self devicePaired:nil];
    }
    else
    {
        [self deviceUnPaired:nil];
    }
}

+(CompanionNavButton *) companionNavButton
{
    return [CompanionNavButton loadNibName:nil];
}

- (IBAction)toggleDiscoveryView:(id)sender {
    [self.delegate companionNavButtonPressed:self];
}

-(void) devicePaired:(NSNotification *) notification
{
    self.buttonCompanion.selected = YES;
}

-(void) deviceUnPaired:(NSNotification *) notification
{
    self.buttonCompanion.selected = NO;
}

-(void)registerTVCompanionMixedNotification
{
    [self unregisterTVCompanionMixedNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(devicePaired:) name:TVCompanionMixedNotificationDeviceConnected object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceUnPaired:) name:TVCompanionMixedNotificationDeviceDisconnected object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chromeCastForceDisconnect:) name:@"chromeCastForceDisconnectNotifcation" object:nil];

}

-(void)unregisterTVCompanionMixedNotification
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVCompanionMixedNotificationDeviceConnected object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVCompanionMixedNotificationDeviceDisconnected object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"chromeCastForceDisconnectNotifcation" object:nil];
}

-(void)dealloc
{
    [self unregisterTVCompanionMixedNotification];
}


-(void)chromeCastForceDisconnect:(NSNotification *)notifcation
{
    if ([[TVMixedCompanionManager sharedInstance] isConnected])
    {
        [[TVMixedCompanionManager sharedInstance] disconnect];
    }
//    dispatch_async(dispatch_get_main_queue(), ^{
//        // do work here
//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:LS(@"chromecast") message:LS(@"chromecast_casting_otherAppTakeover") delegate:nil cancelButtonTitle:LS(@"close") otherButtonTitles: nil];
//        [alert show];
//        [self deviceUnPaired:nil];
//       
//    });
}
@end
