//
//  CompanionDiscoveryViewIPad.h
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CompanionDiscoveryView.h"

@interface CompanionDiscoveryViewIPad : CompanionDiscoveryView

@end
