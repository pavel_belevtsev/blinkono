//
//  CompanionViewBase.h
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum CompanionDiscoveryViewState
{
    CompanionDiscoveryViewState_InitialView,
    CompanionDiscoveryViewState_Dicovery,
    CompanionDiscoveryViewState_paired,
    
} CompanionDiscoveryViewState;

@interface CompanionDiscoveryView : UIView

@property (assign, nonatomic) CompanionDiscoveryViewState state;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewDevices;
@property (strong, nonatomic) IBOutlet UIView * viewUser;
@property (strong, nonatomic) IBOutlet UIView * viewDevices;
@property (strong, nonatomic) IBOutlet UIView *viewDrag;

@property (strong, nonatomic) IBOutlet UIView *viewWhatThis;
@property (strong, nonatomic) IBOutlet UIView *viewServer;


@property (assign, nonatomic) int selectedDevice;
@property (strong, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (strong, nonatomic) IBOutlet UIView *viewXLeft;
@property (strong, nonatomic) IBOutlet UIView *viewXCenter;
@property (strong, nonatomic) IBOutlet UIView *viewXRight;

@property (assign, nonatomic) BOOL canDragViewUser;
@property (nonatomic, strong) CompanionItemView *selectedCompanion;


+(CompanionDiscoveryView *) compantionDiscoveryView;

@end
