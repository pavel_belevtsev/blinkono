//
//  CompanionItemView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 02.01.15.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface CompanionItemView : UIView

@property (nonatomic, strong) IBOutlet UILabel *labelName;
@property (nonatomic, strong) IBOutlet UIView *viewUser;
@property (nonatomic, strong) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblDeviceType;

@end
