//
//  CompanionView.m
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CompanionView.h"
#import <TvinciSDK/TVMixedCompanionManager.h>
#import "UIView+LoadNib.h"
#import "UIImageView+WebCache.h"
#import <TvinciSDK/TVNetworkTracker.h>



@interface CompanionView ()<UIAlertViewDelegate,CompanionPlayViewDelegate>


@end


@implementation CompanionView



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
+ (CompanionView *) companionView
{
    return [CompanionView loadNibName:[Utils nibName:NSStringFromClass([super class])]];
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self registerToChromeCastConnectNotifcation];
}

-(BOOL) canDrag
{
    return [[TVCompanionManager sharedInstance].m_devices count] >0;
}


-(void) scanForDevices
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVCompanionMixedNotificationDeviceDiscovery object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDiscovery:) name:TVCompanionMixedNotificationDeviceDiscovery object:nil];
    
    [[TVMixedCompanionManager sharedInstance] scan];
    
}


- (IBAction)buttonScanPressed:(UIButton *)button
{
    [self scanForDevices];
    TVNetworkTracker* networkTracker = [TVNetworkTracker sharedNetworkStatusManager];
    [networkTracker updateStatus];
    
    if (networkTracker.status != TVNetworkStatus_WiFi)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"companion_NoWiFiText") message:nil delegate:self cancelButtonTitle:LS(@"info_btn_close") otherButtonTitles:nil];
        [alertView show];
        
        return;
    }
    
    
    if (![LoginM isSignedIn]) {
        
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"companion_LoginText") message:nil delegate:self cancelButtonTitle:LS(@"info_btn_close") otherButtonTitles:LS(@"login"), nil];
        [alertView show];
        
        return;
        
    }
    
    
    viewWhatThis.hidden = YES;
    
}


-(void) onDiscovery:(NSNotification *) notification
{
    [self updateDevicesList];
}


-(void) updateDevicesList
{
    // to implement on subclass
}


-(void) connectDevice:(TVAbstractCompanionDevice *) device
{
    [[TVMixedCompanionManager sharedInstance] connect:device];
}



-(void)dealloc
{
    self.companionPlayView.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVCompanionMixedNotificationDeviceDiscovery object:nil];
    [self unregisterToChromeCastConnectNotifcation];
}

-(void) updateTexts
{
    labelTitle.text = LS(@"companion_CompanionTitle");
    labelWhatThatText.text = LS(@"companion_WhatThis_description");
    
    [self updateButton:buttonScan title:LS(@"companion_Scan") tint:NO];
    [self updateButton:buttonWhatThis title:LS(@"companion_WhatsThis_button_title") tint:NO];
    [self updateButton:buttonDisconnect title:LS(@"companion_Disconnect") tint:YES];
    [self updateButton:buttonTryAgain title:LS(@"companion_TryAgain") tint:NO];
    labelNoDevice.text = LS(@"companion_NoDevice");
    
    labelScanning.text = LS(@"companion_scanning");
    labelDrag.text = LS(@"companion_Drag");
    labelDrag.font = Font_Bold(labelDrag.font.pointSize);
    labelDrag.hidden = NO;
    
    UIView *viewImgAvatar = [imgAvatar superview];
    viewImgAvatar.layer.cornerRadius = imgAvatar.frame.size.width / 2.0;
    viewImgAvatar.clipsToBounds = YES;
    
}


- (void)updateButton:(UIButton *)button title:(NSString *)title tint:(BOOL)tint
{
    
}

-(void)updateUIAccordingToConnectionMode
{
    
}

- (void)initWhatThisView {
    
    viewWhatThis.hidden = NO;
    viewServer.hidden = YES;
    self.viewMainServer.hidden = YES;
    buttonScan.hidden = NO;
    buttonWhatThis.hidden = YES;
    buttonDisconnect.hidden = YES;
}

-(void) initCompanionView
{
    [self registerToChromeCastConnectNotifcation];

    canDragViewUser = [self canDrag];
    
    viewDrag.hidden = !canDragViewUser;
    
    viewUser.hidden = NO;
    viewUser.center = viewXLeft.center;
    viewUser.userInteractionEnabled = YES;
    imgAvatar.image = [UIImage imageNamed:@"companion_user"];
    
    if ([[TVSessionManager sharedTVSessionManager] isUserConnectedToFacebook]) {
        [imgAvatar sd_setImageWithURL:[[[TVSessionManager sharedTVSessionManager]currentUser] facebookPictureURLForSizeType:TVUserFacebookPictureTypeNormal] placeholderImage:[UIImage imageNamed:@"companion_user"]];
    }
    
    self.selectedCompanion.imgAvatar.image = [UIImage imageNamed:@"companion_user"];
    self.selectedCompanion.hidden = YES;
    
    
    buttonScan.hidden = YES;
    buttonWhatThis.hidden = NO;
    buttonDisconnect.hidden = YES;
}


#pragma mark - UIAlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        
        //        [self.delegate hideCompanionScreen];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:KLoginNotification
                                                            object:nil
                                                          userInfo:nil];
    }
    
}

#pragma mark - CompanionPlayView

- (void)showCompanionPlayView
{
    self.mediaItem = [TVMixedCompanionManager sharedInstance].casstingMediaItem;
    self.companionPlayView = [CompanionPlayView companionPlayView];
    self.companionPlayView.delegate = self;
    [self.companionPlayView configureWithItem:self.mediaItem];
    
    //TODO add to view in Subview !!!
}

-(void)removeCompanionPlayView
{
  // implament in sub class!
}

- (void)companionPlayView:(CompanionPlayView *)view playPauseButtonPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    BOOL needPlay = button.selected;
    [[NSNotificationCenter defaultCenter] postNotificationName:(needPlay) ? @"userMakePlayNotification" : @"userMakePauseNotification" object:nil];
}

- (void)companionPlayView:(CompanionPlayView *)view stopButtonPressed:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userMakeStopNotification" object:nil];
    [self removeCompanionPlayView];
}

-(void)registerToChromeCastConnectNotifcation
{
    [self unregisterToChromeCastConnectNotifcation];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chromeCastForceDisconnect:) name:@"chromeCastForceDisconnectNotifcation" object:nil];

    
    
}

-(void)unregisterToChromeCastConnectNotifcation
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"chromeCastForceDisconnectNotifcation" object:nil];

}

-(void)chromeCastForceDisconnect:(NSNotification *)notifcation
{
    dispatch_async(dispatch_get_main_queue(), ^{
    // do work here
        [self buttonDisconnectPressed:nil];
    });
    if ([notifcation.userInfo objectForKey:@"error"]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Fallo en la conexión. Inténtalo de nuevo"
                                                         message:nil
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles: nil];
        [alert show];
    }
    

}

-(IBAction)buttonDisconnectPressed:(UIButton *)button
{
    // impament in subclass
}
@end


