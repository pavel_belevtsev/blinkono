//
//  TVCompanionManager.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 02.01.15.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "TVCompanionManager.h"
#import <TvinciSDK/TVSessionManager.h>
#ifdef PLAYER

#ifdef PLAYER_DX

#import <TVCPlayerStaticLibrary/TVCplayer.h>

#else

#import <TVCPlayerWV/TVCplayer.h>

#endif

#endif

#import <TvinciSDK/TVCompanionAPI.h>
#import <TvinciSDK/TVCompanionHandler.h>
#import <TvinciSDK/TVCompanionDevice.h>


NSString * const TVNotificationCompanionDevicesList = @"TVNotificationCompanionDevicesList";

@implementation TVCompanionManager

+ (TVCompanionManager*)sharedInstance {
    
    static dispatch_once_t p = 0;
    
    __strong static id sharedObject = nil;
    
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    
    return sharedObject;
}

- (id)init {
    if (self = [super init]) {
        
        self.devices = [NSArray array];
        
    }
    return self;
}

-(void)registerNotifications
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self selector:@selector(scanProcess) name:TVSessionManagerSignInFailedNotification object:nil];
    [center addObserver:self selector:@selector(scanProcess) name:TVSessionManagerDeviceStateAvailableNotification object:nil];
}

-(void)unregisterNotifications
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center removeObserver:self name:TVSessionManagerSignInFailedNotification object:nil];
    [center removeObserver:self name:TVSessionManagerDeviceStateAvailableNotification object:nil];
    
}

- (void)scan {
    
    [self registerNotifications];
    [[TVSessionManager sharedTVSessionManager]forceCheckDeviceState];
    
}

- (void)scanProcess {
    
    [self unregisterNotifications];
    
    CompanionManager * manager = [CompanionManager sharedManager];
    manager.delegate = self;
    [manager searchForDevices];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    [array addObject:@"SetTopBox 1 Living Room"];
    [array addObject:@"SetTopBox 2 Bed Room"];
    [array addObject:@"SetTopBox 3 Kitchen"];
    [array addObject:@"SetTopBox 4 No Room"];
    [array addObject:@"SetTopBox 5 Bed Room"];
    [array addObject:@"SetTopBox 6 No Room"];
    [array addObject:@"SetTopBox 7 Kitchen"];
    [array addObject:@"SetTopBox 8 Living Room"];
    [array addObject:@"SetTopBox 9 Bed Room"];
    [array addObject:@"SetTopBox 10 Kitchen"];
    
    self.devices = array;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TVNotificationCompanionDevicesList object:nil userInfo:nil];
    
    
}

- (NSString *)getDeviceName:(id)device {
    
    if ([device isKindOfClass:[NSString class]]) {
        return (NSString *)device;
    }
    
    if ([device isKindOfClass:[TVCompanionDevice class]]) {
        NSString *name = ((TVCompanionDevice *)device).friendlyName;
        
        if ([name isKindOfClass:[NSString class]] && [name length]) {
            return name;
        } else {
            
            return LS(@"nameless_stb");
            
        }
    }
    
    return @"";
    
}

- (void)pairDevice:(id)device {
    
    self.isPaired = YES;
    
    
    if ([[[NSBundle mainBundle] objectForInfoDictionaryKey:@"DebugCompanionMode"] boolValue]) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:TVNotificationCompanionDevicePaired object:nil userInfo:nil];
        
    }
    [[CompanionManager sharedManager] pairDevice:device];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:TVNotificationCompanionDevicePaired object:nil userInfo:nil];
    
    
}

- (void)unPairCurrentDevice {
    
    if (self.isPaired) {
        
        self.isPaired = NO;
        if ([[[NSBundle mainBundle] objectForInfoDictionaryKey:@"DebugCompanionMode"] boolValue]) {
            
            [[NSNotificationCenter defaultCenter]postNotificationName:TVNotificationCompanionDeviceUnPaired object:nil userInfo:nil];
            
        }
        [[CompanionManager sharedManager] unPairCurrentDevice];
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:TVNotificationCompanionDeviceUnPaired object:nil userInfo:nil];
        
        
    }
    
}

#pragma mark CompanionModeProtocol methods


#ifdef PLAYER
- (void)sendSwooshRequestWithmediaID : (TVMediaToPlayInfo *)mediaToPlay
                   withMediaMarkRate : (NSInteger)seconds
                       playback_type : (NSString *)playback_type
                        epgProgramId : (NSString *)epgProgramId
                            itemType : (NSString *)itemType
                        doRequestPin : (BOOL)requestPin {
    
    TVCompanionDevice* selectedDevice = [CompanionManager sharedManager].devicePaired;
    
    [TVCompanionHandler sendMediaToSTB : selectedDevice
                               mediaID : mediaToPlay.mediaItem.mediaID
                     withMediaMarkRate : seconds
                              siteGuid : [TVSessionManager sharedTVSessionManager].currentUser.siteGUID
                         playback_type : playback_type
                          epgProgramId : epgProgramId
                              itemType : itemType
                                fileId : [mediaToPlay currentFile].fileID
                              domainID : [TVSessionManager sharedTVSessionManager].currentDomain.domainID
                          doRequestPin : NO
     
                   withCompletionBlock : ^(TVCompanionSwooshResult SwooshResult) {
                       
                       
                       switch (SwooshResult) {
                           case TVCompanionSwoosh_Fail:
                               NSLog(@"TVCompanionSwoosh_Fail");
                               break;
                               
                           case TVCompanionSwoosh_Success:
                               NSLog(@"TVCompanionSwoosh_Success");
                               break;
                               
                           case TVCompanionSwoosh_UknownFormat:
                               NSLog(@"TVCompanionSwoosh_UknownFormat");
                               break;
                               
                           default:
                               break;
                       }
                   }];
    
}

- (void)swoosh:(TVMediaToPlayInfo *)mediaToPlay epgId:(TVEPGProgram *)epgProgram time:(NSTimeInterval) time
{
    NSString* itemType      = nil;
    NSString* playbackType  = nil;
    NSInteger seconds = 0;
    
    if (mediaToPlay.mediaItem.isLive)
    {
        playbackType  = @"live";
        itemType      = @"live";
        
        if ([mediaToPlay.fileTypeFormatKey isEqualToString:TVCMediaFormat_CatchUp]) {
            playbackType = @"catch_up";
            
        }else if ([mediaToPlay.fileTypeFormatKey isEqualToString:TVCMediaFormat_StartOver]) {
            playbackType = @"start_over";
            
        }else if ([mediaToPlay.fileTypeFormatKey isEqualToString:TVCMediaFormat_PauseAndPlay]) {
            playbackType = @"pltv";
        }
    }
    else
    {
        itemType = @"vod";
    }
    
    seconds = time;
    [self sendSwooshRequestWithmediaID:mediaToPlay withMediaMarkRate:seconds playback_type:playbackType epgProgramId:epgProgram.EPGId itemType:itemType doRequestPin:NO];
    
}
#endif
- (NSArray *)getKnownDevicesIds {
    
    return [TVSessionManager sharedTVSessionManager].currentDomain.deviceFamilies;
}

- (void)companionSearchFinishedWithDevices:(NSArray *)devices {
    
    //  Get all UDIDs of registered STBs
    NSArray *knownDevices = [TVSessionManager sharedTVSessionManager].currentDomain.deviceFamilies;
    
    //  Merge nearby devices with the registered STBs, means "registeredSTBs" array will have only registered STBs that are positioned nearby.
    NSArray* registeredSTBs = [TVCompanionHandler crossDevicesArray:devices withUDIDsArray:knownDevices];
    
    //registeredSTBs = [NSArray arrayWithObject:devices[1]];
    
    if ([registeredSTBs count]) {
        
        //  Download essentail XML data for the list of STBs
        
        [[CompanionManager sharedManager] downloadXMLDataForDevices:registeredSTBs WithCompletionBlock:^(NSArray *downloadedDevices) {
            
            //  XML data was downloaded, just refresh the UI
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            for (TVCompanionDevice *device in [CompanionManager sharedManager].devicesItems) {
                NSLog(@"device name %@", device.friendlyName);
                
                [array addObject:device];
                
            }
            
            self.devices = array;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:TVNotificationCompanionDevicesList object:nil userInfo:nil];
            
            
        }];
        
    } else {
        
        self.devices = [NSArray array];
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        [array addObject:@"SetTopBox 1 Living Room"];
        [array addObject:@"SetTopBox 2 Bed Room"];
        [array addObject:@"SetTopBox 3 Kitchen"];
        [array addObject:@"SetTopBox 4 No Room"];
        [array addObject:@"SetTopBox 5 Bed Room"];
        [array addObject:@"SetTopBox 6 No Room"];
        [array addObject:@"SetTopBox 7 Kitchen"];
        [array addObject:@"SetTopBox 8 Living Room"];
        [array addObject:@"SetTopBox 9 Bed Room"];
        [array addObject:@"SetTopBox 10 Kitchen"];
        
        self.devices = array;
        
        [[NSNotificationCenter defaultCenter]postNotificationName:TVNotificationCompanionDevicesList object:nil userInfo:nil];
        
        
    }
    
}

- (void)companionSearchFinishedWithError:(NSError *)error {
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:LS(@"alert_general_error") message:LS(@"general_Error_Discovery_was_interrupted") delegate:nil cancelButtonTitle:[LS(@"ok") uppercaseString] otherButtonTitles:nil];
    [alert show];
}


-(void)saveDevices :(NSArray *)devices{
    self.m_devices = [NSMutableArray arrayWithArray:devices];
}



@end
