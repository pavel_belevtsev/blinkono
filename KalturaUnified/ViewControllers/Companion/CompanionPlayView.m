//
//  CompanionPlayView.m
//  Vodafone
//
//  Created by Admin on 24.10.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CompanionPlayView.h"
#import "UIView+LoadNib.h"
#import "Utils.h"
#import "UIImageView+WebCache.h"

@interface CompanionPlayView ()

@property (assign, nonatomic) CGRect seriesThumbRect;
@property (assign, nonatomic) CGRect movieThumbRect;
@property (assign, nonatomic) CGRect channelThumbRect;

@property (assign, nonatomic) CGFloat initialTitleMargin;
@property (assign, nonatomic) CGFloat initialViewWidth;

@end

@implementation CompanionPlayView

#pragma mark -
#pragma mark - View Setup

+(CompanionPlayView *) companionPlayView
{
    return [CompanionPlayView loadNibName:nil];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self assignThumbnailsValues];
    
    self.initialTitleMargin = self.labelMediaTitle.frame.origin.x - CGRectGetMaxX(self.viewThumbnailContainer.frame);
    self.initialViewWidth = self.width;
    
    self.layer.cornerRadius = 2;
    self.layer.masksToBounds = YES;
    self.viewInner.layer.borderColor = [Utils colorFromString:@"1e1e1e"].CGColor;
    self.viewInner.layer.borderWidth = 1.0f;
}

- (void)assignThumbnailsValues {
    self.seriesThumbRect = CGRectMake(13, 29, 99, 56);
    self.movieThumbRect = CGRectMake(21, 8, 65, 98);
    self.channelThumbRect = CGRectMake(26, 15, 83, 83);
}

#pragma mark -
#pragma mark - Configuring

- (void)configureWithItem:(TVMediaItem *)item {
    self.labelMediaTitle.text = item.name;
    NSMutableString *subTitle = [@"" mutableCopy];
    if ([item isMovie]) {
        
        NSArray *genres = [item.tags objectForKey:@"Genre"];
        if ([genres count]) {
            [subTitle appendString:[genres firstObject]];
        }
        
        NSString *releaseYear = item.releaseYearDescription;
        if (releaseYear.length && [genres count]) {
            [subTitle appendString:@" | "];
            [subTitle appendString:releaseYear];
        } else if (releaseYear.length) {
            [subTitle appendString:releaseYear];
        }
        self.buttonPlayPause.hidden = NO;
        [self setupThumbnailWith:self.movieThumbRect];
        
    } else if ([item isEpisode]) {
        subTitle = [[NSString stringWithFormat:@"%@ %@, %@ %@", LS(@"Season"), [item getMediaItemSeasonNumber], LS(@"Episode"), [item getMediaItemEpisodeNumber]] mutableCopy];
        self.buttonPlayPause.hidden = NO;
        [self setupThumbnailWith:self.seriesThumbRect];
    } else if ([item isLive]) {
        self.buttonPlayPause.hidden = YES;
        [self setupThumbnailWith:self.channelThumbRect];
    } else {
        self.buttonPlayPause.hidden = NO;
        [self setupThumbnailWith:self.movieThumbRect];
    }
    self.labelMediaSubtitle.text = subTitle;
    [self.imageViewThumbnail sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:item andSize:CGSizeMake(self.imageViewThumbnail.frame.size.width * 2, self.imageViewThumbnail.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"]];
    [self updatePasuePlayImage];
}

- (void)setupThumbnailWith:(CGRect)frame {
    self.viewImageBorder.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
    self.imageViewThumbnail.frame = CGRectMake(1.f, 1.f, self.viewImageBorder.frame.size.width - 2.f, self.viewImageBorder.frame.size.height - 2.f);
  
}

-(void)updatePasuePlayImage
{
    CompanionPlayerState state = (CompanionPlayerState)[TVMixedCompanionManager sharedInstance].currentCompanionPlayerState;
    switch (state) {
        case CompanionPlayerStatePlay:
            self.buttonPlayPause.selected = NO;
            break;
        case CompanionPlayerStatePause:
            self.buttonPlayPause.selected = YES;
            break;
        default:
            break;
    }
}

- (void)relayout {
    CGRect containerFrame = self.viewControlsContainer.frame;
    containerFrame.origin.x = self.frame.size.width - containerFrame.size.width;
    self.viewControlsContainer.frame = containerFrame;
    [self relayoutLabel:self.labelMediaTitle];
    [self relayoutLabel:self.labelMediaSubtitle];
}

- (void)relayoutLabel:(UILabel *)label {
    CGRect titleFrame = label.frame;
    CGFloat margin = CGRectGetWidth(self.frame) / self.initialViewWidth * self.initialTitleMargin;
    titleFrame.origin.x = CGRectGetMaxX(self.viewThumbnailContainer.frame) + margin;
    CGFloat marginX = (!self.buttonPlayPause.hidden) ? CGRectGetMinX(self.viewControlsContainer.frame) : CGRectGetMidX(self.viewControlsContainer.frame) ;
    titleFrame.size.width = marginX - margin - titleFrame.origin.x;
    label.frame = titleFrame;
}

#pragma mark -
#pragma mark - Drawing borders

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self drawBottomPaleLine];
}

- (void)drawBottomPaleLine {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [Utils colorFromString:@"373737"].CGColor);
    CGContextSetLineWidth(context, 1.f);
    CGContextMoveToPoint(context, 0.f, self.frame.size.height - 1.f);
    CGContextAddLineToPoint(context, self.frame.size.width - 1.f, self.frame.size.height - 1.f);
    CGContextStrokePath(context);
}

#pragma mark -
#pragma mark - Actions

- (IBAction)playPauseButtonPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(companionPlayView:playPauseButtonPressed:)]) {
        [self.delegate companionPlayView:self playPauseButtonPressed:sender];
    }
    self.buttonPlayPause.selected = !self.buttonPlayPause.selected;
}

- (IBAction)stopButtonPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(companionPlayView:stopButtonPressed:)]) {
        [self.delegate companionPlayView:self stopButtonPressed:sender];
    }
}

@end
