//
//  CompanionViewBase.m
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CompanionDiscoveryView.h"
#import "CompanionItemView.h"
#import "UIImageView+WebCache.h"

@implementation CompanionDiscoveryView

- (IBAction)discovery:(id)sender
{
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) setup
{
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragUserAction:)] ;
    [self.viewUser addGestureRecognizer:recognizer];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDevicesList) name:TVNotificationCompanionDevicesList object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(companionDevicePaired) name:TVNotificationCompanionDevicePaired object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(companionDeviceUnPaired) name:
     TVNotificationCompanionDeviceUnPaired object:nil];
    
    
    self.selectedCompanion = [[[NSBundle mainBundle] loadNibNamed:@"CompanionItemView" owner:self options:nil] objectAtIndex:0];
    self.selectedCompanion.viewUser.hidden = NO;
    [self.viewXRight.superview addSubview:self.selectedCompanion];
    self.selectedCompanion.center = self.viewXRight.center;
    self.selectedCompanion.hidden = YES;
    self.selectedCompanion.imgAvatar.image = [UIImage imageNamed:@"companion_user"];
    
}


- (void)initWhatThisView {
    
    self.viewWhatThis.hidden = NO;
    self.viewServer.hidden = YES;
    
//    buttonScan.hidden = NO;
//    buttonWhatThis.hidden = YES;
//    buttonDisconnect.hidden = YES;
//    
//    labelScanning.hidden = YES;
//    labelNoDevice.hidden = YES;
//    buttonScanAgain.hidden = YES;
    
}

- (void)initCompanionView {
    
    self.canDragViewUser = [[TVCompanionManager sharedInstance].devices count];
    
    self.viewDrag.hidden = YES;
    self.viewDevices.hidden = YES;
    
    self.viewUser.hidden = YES;
    self.viewUser.center = self.viewXLeft.center;
    self.viewUser.userInteractionEnabled = YES;
    self.imgAvatar.image = [UIImage imageNamed:@"companion_user.png"];
    
    if ([[TVSessionManager sharedTVSessionManager] isUserConnectedToFacebook])
    {
        [self.imgAvatar sd_setImageWithURL:[[[TVSessionManager sharedTVSessionManager]currentUser] facebookPictureURLForSizeType:TVUserFacebookPictureTypeNormal] placeholderImage:[UIImage imageNamed:@"companion_user.png"]];
    }
    
    self.selectedCompanion.frame = CGRectMake(self.viewDevices.frame.origin.x, self.scrollViewDevices.frame.origin.y, self.scrollViewDevices.frame.size.width, self.scrollViewDevices.frame.size.height);
    self.selectedCompanion.hidden = YES;
    self.selectedCompanion.imgAvatar.image = [UIImage imageNamed:@"companion_user.png"];
    
//    buttonScan.hidden = YES;
//    buttonWhatThis.hidden = NO;
//    buttonDisconnect.hidden = YES;
//    
//    labelScanning.hidden = YES;
//    
//    labelNoDevice.hidden = YES;
//    buttonScanAgain.hidden = YES;
    
    // [imgAvatar setImageWithURL:[NSURL URLWithString:@"http://img.terrikon.info/i/coach/ukr/h/th-100-rahaev.jpg"] placeholderImage:[UIImage imageNamed:@"companion_user.png"]];
    
}





+(CompanionDiscoveryView *) compantionDiscoveryView
{
   NSArray * views =  [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class])  owner:nil options:nil];
    
    for (UIView * view in views)
    {
        if ([view isKindOfClass:[CompanionDiscoveryView class]])
        {
            CompanionDiscoveryView* discoveryView = (CompanionDiscoveryView*)view;
            [discoveryView setup];
            return discoveryView;
            
            
        }
    }
    
    return nil;
}



-(IBAction)discoveryPressed:(id)sender
{
    [[TVCompanionManager sharedInstance] performSelector:@selector(scan) withObject:nil afterDelay:0.5];
}


#pragma mark - notification

-(void) updateDevicesList
{
    NSLog(@"updateDevicesList");
    
    for (UIView *subview in [self.scrollViewDevices subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            subview.hidden = YES;
        }
    }
    
    self.scrollViewDevices.contentSize = self.scrollViewDevices.frame.size;
    NSArray *devices = [TVCompanionManager sharedInstance].devices;
    
    if ([devices count] > 1) {
        
        self.scrollViewDevices.contentSize = CGSizeMake(self.scrollViewDevices.frame.size.width, self.scrollViewDevices.frame.size.height * [devices count] * 3);
        
        [self initScrollViewDevices:0];
        
    } else if ([devices count] == 1) {
        
        self.scrollViewDevices.contentSize = CGSizeMake(self.scrollViewDevices.frame.size.width, self.scrollViewDevices.frame.size.height);
        CompanionItemView *companionItem = [self getFreeCompanionItem];
        companionItem.frame = CGRectMake(companionItem.frame.origin.x, 0, companionItem.frame.size.width, companionItem.frame.size.height);
        companionItem.labelName.text = [[TVCompanionManager sharedInstance] getDeviceName:[devices objectAtIndex:0]];
        
    }
    
//    canDragViewUser = [[TVCompanionManager sharedInstance].devices count];
//    
    self.viewDrag.hidden = !self.canDragViewUser;
//    viewDragP.hidden = !canDragViewUser;
    
//    viewDevices.hidden = !canDragViewUser;
//    viewDevicesP.hidden = !canDragViewUser;
//    viewUser.hidden = !canDragViewUser;
//    viewUserP.hidden = !canDragViewUser;
    
//    viewNoDevices.hidden = canDragViewUser;
    
//    [self hideCompanionHUD];
    
}

-(void) companionDevicePaired
{
    
}

-(void) companionDeviceUnPaired
{
    
}

#pragma mark - queue
- (CompanionItemView *)getFreeCompanionItem {
    
    for (UIView *subview in [self.scrollViewDevices subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            if (subview.hidden) {
                subview.hidden = NO;
                
                return (CompanionItemView *)subview;
                
            }
        }
    }
    
    CompanionItemView *companionItem = [[[NSBundle mainBundle] loadNibNamed:@"CompanionItemView" owner:self options:nil] objectAtIndex:0];
    companionItem.viewUser.hidden = YES;
    [self.scrollViewDevices addSubview:companionItem];
    
    return companionItem;
    
}


- (void)initScrollViewDevices:(int)device {
    
    self.selectedDevice = device;
    
    for (UIView *subview in [self.scrollViewDevices subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            subview.hidden = YES;
        }
    }
    
    NSArray *devices = [TVCompanionManager sharedInstance].devices;
    
    int y = self.scrollViewDevices.frame.size.height * ([devices count] + self.selectedDevice);
    
    self.scrollViewDevices.contentOffset = CGPointMake(0, y);
    
    for (int i = -2; i < 3; i++) {
        
        CompanionItemView *companionItem = [self getFreeCompanionItem];
        companionItem.frame = CGRectMake(companionItem.frame.origin.x, y + (i * companionItem.frame.size.height), companionItem.frame.size.width, companionItem.frame.size.height);
        
        int index = self.selectedDevice + i;
        
        while (index < 0) index += [devices count];
        while (index >= [devices count]) index -= [devices count];
        
        companionItem.labelName.text = [[TVCompanionManager sharedInstance] getDeviceName:[devices objectAtIndex:index]];
        
    }
}

#pragma mark - pan gesture

- (void)dragUserAction:(UIPanGestureRecognizer *)recognizer {
    
    CGPoint translation = [recognizer translationInView:recognizer.view];
    
    float x = self.viewUser.center.x + translation.x;
    
    float checkCenterX = self.viewXCenter.center.x;
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        if (x < checkCenterX) {
            
            float time = ((self.viewUser.center.x - self.viewXLeft.center.x) / (checkCenterX - self.viewXLeft.center.x)) * 0.7;
            
            [UIView animateWithDuration:time animations:^{
                
                self.viewUser.center = self.viewXLeft.center;
               
                
            } completion:^(BOOL finished){
                
                self.canDragViewUser = [[TVCompanionManager sharedInstance].devices count];
                self.viewDrag.hidden = !self.canDragViewUser;

                
            }];
            
        }
        
        
    } else if (self.canDragViewUser) {
        
        
        if (x < self.viewXLeft.center.x) {
            x = self.viewXLeft.center.x;
        }
        if (x >= checkCenterX) {
            
            self.viewUser.center = CGPointMake(checkCenterX, self.viewUser.center.y);
            
            self.canDragViewUser = NO;
            self.viewDrag.hidden = !self.canDragViewUser;
            
            [UIView animateWithDuration:0.7 animations:^{
                
                self.viewUser.center = self.viewXRight.center;
                
            } completion:^(BOOL finished){
                
                self.selectedCompanion.center = self.viewXRight.center;
                
                self.selectedCompanion.imgAvatar.image = self.imgAvatar.image;
                self.selectedCompanion.hidden = NO;
                self.viewUser.hidden = YES;
                
                int index = self.selectedDevice;
                
                NSArray *devices = [TVCompanionManager sharedInstance].devices;
                while (index < 0) index += [devices count];
                while (index >= [devices count]) index -= [devices count];
                
                self.selectedCompanion.labelName.text = [[TVCompanionManager sharedInstance] getDeviceName:[devices objectAtIndex:index]];

                
                [TVCompanionManager sharedInstance].currentDeviceName = self.selectedCompanion.labelName.text;
                [[TVCompanionManager sharedInstance] pairDevice:[devices objectAtIndex:index]];
                
                self.viewDevices.hidden = YES;

                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.selectedCompanion.center = self.viewXCenter.center;

                } completion:^(BOOL finished){
                    
//                    buttonScan.hidden = YES;
//                    buttonWhatThis.hidden = YES;
//                    buttonDisconnect.hidden = NO;
                    
                }];
                
            }];
        } else {
            self.viewUser.center = CGPointMake(x, self.viewUser.center.y);
        }
        
    }
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self];
    
}


#pragma mark - deallocation

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
