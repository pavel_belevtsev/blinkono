//
//  TVNCompanionView.m
//  Tvinci
//
//  Created by Pavel Belevtsev on 28.04.14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import "CompanionViewPad.h"
#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"
#import <TvinciSDK/TVNetworkTracker.h>
#import <TvinciSDK/TVMixedCompanionManager.h>
//#import "TVNLoginManager.h"
//#import "TVNAppDelegate.h"
//#import "TVNMainViewController.h"

@implementation CompanionViewPad



- (id)init {
    self = [super init];
    
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    if (self) {
        ASLogInfo(@"init CompanionViewPad");
    }
    return self;
    
}

-(void)awakeFromNib
{
    ASLogInfo(@"awakeFromNib CompanionViewPad");

    [super awakeFromNib];
    [self updateTexts];
}


- (void)updateTexts {
    
    ASLogInfo(@"updateTexts CompanionViewPad");

    [super updateTexts];
    
    labelTitle.font = Font_Bold(labelTitle.font.pointSize);
    labelWhatThatText.font = Font_Reg(labelWhatThatText.font.pointSize);
    labelWhatThatText.textColor = CS(@"#cccccc");
    
    buttonTryAgain.frame = CGRectMake((self.frame.size.width - buttonTryAgain.frame.size.width) / 2.0, buttonTryAgain.frame.origin.y, buttonTryAgain.frame.size.width, buttonTryAgain.frame.size.height);
    
    labelDrag.textColor = CS(@"#cccccc");
    
    labelScanning.font = Font_Reg(labelScanning.font.pointSize);
    labelTitle.textColor = CS(@"#cccccc");
    
    
    labelNoDevice.font = Font_Bold(labelNoDevice.font.pointSize);
    labelNoDevice.textColor =  CS(@"#cccccc");
    
    self.selectedCompanion = [[[NSBundle mainBundle] loadNibNamed:@"CompanionItemView" owner:self options:nil] objectAtIndex:0];
    self.selectedCompanion.viewUser.hidden = NO;
    [self.viewMainServer addSubview:self.selectedCompanion];
    
    [self initCompanionView];
    
    [self initWhatThisView];
    
    
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragUserAction:)];
    [viewUser addGestureRecognizer:recognizer];
    
    self.fullScreenProgress =  [ActivityView ActivityView];
    self.fullScreenProgress.viewBackground.hidden = YES;
    self.fullScreenProgress.center = CGPointMake(self.viewMainServer.size.width/2, self.viewMainServer.size.height/2.5);
    [self.fullScreenProgress stopAnimating];
    [self.viewMainServer addSubview:self.fullScreenProgress];
    
    
    [self updateUIAccordingToConnectionMode];
    
    
}

- (void)updateButton:(UIButton *)button title:(NSString *)title tint:(BOOL)tint {
    
    UIImage *btnImage = [UIImage imageNamed:@"gray_btn.png"];

    if (tint) {
        btnImage = [btnImage imageTintedWithColor:APST.brandColor];
    }
    int btnWidth = [title sizeWithFont:Font_Bold(14)].width + 32;
    if ((btnWidth % 2) == 1) btnWidth++;
    
    button.frame = CGRectMake(self.frame.size.width - btnWidth - 18, button.frame.origin.y, btnWidth, button.frame.size.height);
    [button setBackgroundImage:[btnImage stretchableImageWithLeftCapWidth:btnImage.size.width / 2.0 topCapHeight:btnImage.size.height / 2.0] forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    if (tint)
    {
        [button setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
        [button setTitleColor:CS(@"#424242") forState:UIControlStateHighlighted];
    }
    else
    {
        [button setTitleColor:CS(@"#f4f4f4") forState:UIControlStateNormal];
        [button setTitleColor:CS(@"#8c8c8c") forState:UIControlStateHighlighted];
    }
    [button.titleLabel setFont:Font_Bold(14)];
    
}


- (void)initWhatThisView {
    ASLogInfo(@"initWhatThisView CompanionViewPad");

    [super initWhatThisView];
    labelScanning.hidden = YES;
    labelNoDevice.hidden = YES;
    buttonTryAgain.hidden = YES;
    
}


- (void)initCompanionView {
    ASLogInfo(@"initCompanionView CompanionViewPad");

    [super initCompanionView];
    
    viewDevices.hidden = YES;
    self.selectedCompanion.frame = CGRectMake(viewDevices.frame.origin.x, scrollViewDevices.frame.origin.y, scrollViewDevices.frame.size.width, scrollViewDevices.frame.size.height);
    
    
    labelScanning.hidden = YES;
    labelNoDevice.hidden = YES;
    buttonTryAgain.hidden = YES;
    
    
}

- (void)dragUserAction:(UIPanGestureRecognizer *)recognizer {
    
    
    CGPoint translation = [recognizer translationInView:recognizer.view];
    
    float x = viewUser.center.x + translation.x;
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        if (x < viewXCenter.center.x) {
            
            float time = ((viewUser.center.x - viewXLeft.center.x) / (viewXCenter.center.x - viewXLeft.center.x)) * 0.7;
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:TVCompanionMixedNotificationDeviceDiscovery object:nil];
            
            [UIView animateWithDuration:time animations:^{
                
                viewUser.center = viewXLeft.center;
                
            } completion:^(BOOL finished){
                
                canDragViewUser = [self canDrag];
                //[[TVCompanionManager sharedInstance].devices count];
                viewDrag.hidden = !canDragViewUser;
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDiscovery:) name:TVCompanionMixedNotificationDeviceDiscovery object:nil];
            }];
            
        }
        
        
    } else if (canDragViewUser) {
        
        if (x < viewXLeft.center.x) {
            x = viewXLeft.center.x;
        }
        if (x >= viewXCenter.center.x) {
            
            viewUser.center = viewXCenter.center;
            
            canDragViewUser = NO;
            viewDrag.hidden = !canDragViewUser;
            
            [UIView animateWithDuration:0.7 animations:^{
                
                viewUser.center = viewXRight.center;
                
            } completion:^(BOOL finished){
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:TVCompanionMixedNotificationDeviceDiscovery object:nil];
                
                self.selectedCompanion.imgAvatar.image = imgAvatar.image;
                self.selectedCompanion.hidden = NO;
                viewUser.hidden = YES;
                
                
                NSInteger index = selectedDevice;
                
                NSArray *devices = [TVCompanionManager sharedInstance].m_devices;
                
                while (index < 0) index += [devices count];
                while (index >= [devices count]) index -= [devices count];
                
                TVAbstractCompanionDevice *device = [devices objectAtIndex:index];
                self.selectedCompanion.labelName.text = device.name;
                NSInteger deviceType = [device getType];
                if (deviceType == 0x3015)
                {
                    self.selectedCompanion.lblDeviceType.text = @"Chromecast";
                }
                else
                {
                    self.selectedCompanion.lblDeviceType.text = @"STB";
                }
                
                [self connectDevice:device];
                
                viewDevices.hidden = YES;
                
                buttonScan.hidden = YES;
                buttonWhatThis.hidden = YES;
                buttonDisconnect.hidden = NO;
                
                
                [UIView animateWithDuration:0.7 animations:^{
                    
                    self.selectedCompanion.frame = CGRectMake(viewXCenter.center.x - (self.selectedCompanion.frame.size.height / 2.0), self.selectedCompanion.frame.origin.y,
                                                              self.selectedCompanion.frame.size.width, self.selectedCompanion.frame.size.height);
                    
                } completion:^(BOOL finished){
                    
                    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDiscovery:) name:TVCompanionMixedNotificationDeviceDiscovery object:nil];
                }];
                
            }];
        } else {
            viewUser.center = CGPointMake(x, viewUser.center.y);
        }
        
        
    }
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self];
    
}

- (void)updateDevicesList {
    
    NSLog(@"updateDevicesList");
    ASLogInfo(@"updateDevicesList CompanionViewPad");

    for (UIView *subview in [scrollViewDevices subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            subview.hidden = YES;
        }
    }
    
    scrollViewDevices.contentSize = scrollViewDevices.frame.size;
    
    // Note: this is a workaround to solve the problem, that the SDK is not sending the most updated CC devices .. we will take from the SDK only the STB list and use our CC list from [TVCompanionManager sharedInstance].m_devices
    NSArray *devicesAll = [[TVMixedCompanionManager sharedInstance] getDiscoveredDevices];
    
    for (TVAbstractCompanionDevice *dev in devicesAll) {
        NSInteger deviceType = [dev getType];
        if (deviceType == 0x3015)
        {//chromecast
        }
        else
        {//stb
            [[TVCompanionManager sharedInstance].m_devices addObject:dev ];
            
        }
    }
    
    NSArray *devices = [NSArray arrayWithArray:[TVCompanionManager sharedInstance].m_devices];
    if ([devices count] > 1) {
        
        scrollViewDevices.contentSize = CGSizeMake(scrollViewDevices.frame.size.width, scrollViewDevices.frame.size.height * [devices count] * 3);
        
        [self initScrollViewDevices];
        
    } else if ([devices count] == 1) {
        
        scrollViewDevices.contentSize = CGSizeMake(scrollViewDevices.frame.size.width, scrollViewDevices.frame.size.height);
        CompanionItemView *companionItem = [self getFreeCompanionItem];
        companionItem.frame = CGRectMake(companionItem.frame.origin.x, 0, companionItem.frame.size.width, companionItem.frame.size.height);
        
        TVAbstractCompanionDevice* device = [devices objectAtIndex:0];
        companionItem.labelName.text = device.name;
        NSInteger deviceType = [device getType];
        if (deviceType == 0x3015)
        {
            companionItem.lblDeviceType.text = @"Chromecast";
        }
        else
        {
            companionItem.lblDeviceType.text = @"STB";
        }
    }
    
    canDragViewUser = [self canDrag];
    if([devices count] == 0){
        
        canDragViewUser = NO;
    }
    //[[TVCompanionManager sharedInstance].devices count];
    
    viewDrag.hidden = !canDragViewUser;
    
    labelScanning.hidden = YES;
    
    [self.fullScreenProgress stopAnimating];
    //    [_fullScreenProgress hide];
    
    viewUser.hidden = !canDragViewUser;
    
    viewDevices.hidden = !canDragViewUser;
    
    labelNoDevice.hidden = canDragViewUser;
    buttonTryAgain.hidden = canDragViewUser;
    
}

- (IBAction)buttonWhatThisPressed:(UIButton *)button {
    
    [self initWhatThisView];
    
}

- (void)initScrollViewDevices {
    ASLogInfo(@"initScrollViewDevices CompanionViewPad");

    viewDevices.hidden = NO;
    
    selectedDevice = 0;
    
    for (UIView *subview in [scrollViewDevices subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            subview.hidden = YES;
        }
    }
    
    NSArray *devices = [TVCompanionManager sharedInstance].m_devices;
    
    int y = scrollViewDevices.frame.size.height * [devices count];
    
    scrollViewDevices.contentOffset = CGPointMake(0, y);
    
    for (int i = -2; i < 3; i++) {
        
        CompanionItemView *companionItem = [self getFreeCompanionItem];
        companionItem.frame = CGRectMake(companionItem.frame.origin.x, y + (i * companionItem.frame.size.height), companionItem.frame.size.width, companionItem.frame.size.height);
        
        NSInteger index = selectedDevice + i;
        
        while (index < 0) index += [devices count];
        while (index >= [devices count]) index -= [devices count];
        
        TVAbstractCompanionDevice* device = [devices objectAtIndex:index];
        companionItem.labelName.text = device.name;
        NSInteger deviceType = [device getType];
        if (deviceType == 0x3015)
        {
            companionItem.lblDeviceType.text = @"Chromecast";
        }
        else
        {
            companionItem.lblDeviceType.text = @"STB";
            
        }
    }
    
    
}

- (CompanionItemView *)getFreeCompanionItem {
    ASLogInfo(@"getFreeCompanionItem CompanionViewPad");

    for (UIView *subview in [scrollViewDevices subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            if (subview.hidden) {
                subview.hidden = NO;
                
                return (CompanionItemView *)subview;
                
            }
        }
    }
    
    CompanionItemView *companionItem = [[[NSBundle mainBundle] loadNibNamed:@"CompanionItemView" owner:self options:nil] objectAtIndex:0];
    companionItem.viewUser.hidden = YES;
    [scrollViewDevices addSubview:companionItem];
    
    return companionItem;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    ASLogInfo(@"scrollViewDidScroll CompanionViewPad");
    if (scrollView == scrollViewDevices)
    {
        if (((int)scrollView.contentOffset.y) % ((int)scrollView.frame.size.height) == 0)
        {
            
            NSInteger page = scrollView.contentOffset.y / scrollView.frame.size.height;
            NSArray *devices = [TVCompanionManager sharedInstance].m_devices;
            
            NSInteger newSelectedDevice = page - [devices count];
            if (newSelectedDevice != selectedDevice)
            {
                selectedDevice = newSelectedDevice;
                
                if (selectedDevice == -[devices count] || selectedDevice == [devices count])
                {
                    [self initScrollViewDevices];
                }
                else
                {
                    for (UIView *subview in [scrollViewDevices subviews])
                    {
                        if ([subview isKindOfClass:[CompanionItemView class]])
                        {
                            subview.hidden = YES;
                        }
                    }
                    
                    int y = scrollViewDevices.frame.size.height * ([devices count] + selectedDevice);
                    
                    for (int i = -2; i < 3; i++)
                    {
                        CompanionItemView *companionItem = [self getFreeCompanionItem];
                        companionItem.frame = CGRectMake(companionItem.frame.origin.x, y + (i * companionItem.frame.size.height), companionItem.frame.size.width, companionItem.frame.size.height);
                        
                        NSInteger index = selectedDevice + i;
                        
                        while (index < 0) index += [devices count];
                        while (index >= [devices count]) index -= [devices count];
                        
                        TVAbstractCompanionDevice* device = [devices objectAtIndex:index];
                        
                        companionItem.labelName.text = device.name;
                        NSInteger deviceType = [device getType];
                        if (deviceType == 0x3015)
                        {
                            companionItem.lblDeviceType.text = @"Chromecast";
                        }
                        else
                        {
                            companionItem.lblDeviceType.text = @"STB";
                        }
                    }
                }
            }
        }
    }
}

- (void)companionDevicePaired
{
    
}

- (void)companionDeviceUnPaired
{
    //    if (!buttonDisconnect.hidden) {
    //        [self buttonDisconnectPressed:buttonDisconnect];
    //    }
}

- (IBAction)buttonDisconnectPressed:(UIButton *)button
{
    [[TVMixedCompanionManager sharedInstance] disconnect];
    
    [self removeCompanionPlayView];

    [self initCompanionView];
    
    viewUser.center = viewXCenter.center;
    viewDevices.hidden = NO;
    viewUser.hidden = NO;
    buttonDisconnect.hidden = YES;
    labelDrag.hidden = NO;
    
    [UIView animateWithDuration:0.7 animations:^{
        
        viewUser.center = viewXLeft.center;
        
    } completion:^(BOOL finished){
        
        canDragViewUser = [self canDrag];
        viewDrag.hidden = !canDragViewUser;
        
    }];
}

-(BOOL)isUserConnected
{
    ASLogInfo(@"isUserConnected CompanionViewPad");

    if (buttonDisconnect.hidden == YES)
    {
        return NO;
    }
    return YES;
}

- (IBAction)buttonScanPressed:(UIButton *)button
{
    NSArray *devices =  [TVCompanionManager sharedInstance].m_devices;
    if (button == nil && [devices count] > 0)
    {
        viewWhatThis.hidden = YES;
        
        labelTitle.text = LS(@"companion_CompanionTitle");
        
        self.viewMainServer.hidden = NO;
        
        buttonScan.hidden = YES;
        
        labelNoDevice.hidden = YES;
        buttonTryAgain.hidden = YES;
        
        viewDevices.hidden = YES;
        viewDrag.hidden = YES;
        viewUser.hidden = YES;
        
        buttonWhatThis.hidden = NO;
        
        labelScanning.hidden = NO;
        
        [self.viewMainServer bringSubviewToFront:labelScanning];
        
        [self updateDevicesList];
        
        return;
    }
    
    [super buttonScanPressed:button];
    
    
    if (viewWhatThis.hidden == YES)
    {
        labelTitle.text = LS(@"companion_CompanionTitle");
        
        self.viewMainServer.hidden = NO;
        
        buttonScan.hidden = YES;
        
        labelNoDevice.hidden = YES;
        buttonTryAgain.hidden = YES;
        
        viewDevices.hidden = YES;
        viewDrag.hidden = YES;
        viewUser.hidden = YES;
        
        buttonWhatThis.hidden = NO;
        
        labelScanning.hidden = NO;
        
        [self.fullScreenProgress startAnimating];
        
        
        [self.viewMainServer bringSubviewToFront:labelScanning];
        
        [self scanForDevices];
        
    }
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//
//    if (buttonIndex == 1) {
//
////        [[TVNMainViewController sharedinstance] openCenterPanel];
////
////        TVNAppDelegate * appDelegate = (TVNAppDelegate*)[UIApplication sharedApplication].delegate;
////        [appDelegate showLogin:kRegularLogin delegate:nil];
//
//    }
//
//}

-(void) updateUIAccordingToConnectionMode
{
    ASLogInfo(@"updateUIAccordingToConnectionMode CompanionViewPad");

    if ([[TVMixedCompanionManager sharedInstance] isConnected])
    {
        ASLogInfo(@"updateUIAccordingToConnectionMode -> isConnected CompanionViewPad");

        [self updateDevicesList];
        viewWhatThis.hidden =YES;
        self.viewMainServer.hidden = NO;
        self.selectedCompanion.imgAvatar.image = imgAvatar.image;
        self.selectedCompanion.hidden = NO;
        viewUser.hidden = YES;
        
        labelDrag.hidden = YES;
        TVAbstractCompanionDevice *device = [[TVMixedCompanionManager sharedInstance] getCurrentConnectedDevice];
        self.selectedCompanion.labelName.text = device.name;
        NSInteger deviceType = [device getType];
        if (deviceType == 0x3015)
        {
            self.selectedCompanion.lblDeviceType.text = @"Chromecast";
        }
        else
        {
            self.selectedCompanion.lblDeviceType.text = @"STB";
        }
        
        viewDevices.hidden = YES;
        buttonScan.hidden = YES;
        buttonWhatThis.hidden = YES;
        buttonDisconnect.hidden = NO;
        self.selectedCompanion.frame = CGRectMake(viewXCenter.center.x - (self.selectedCompanion.frame.size.height / 2.0), self.selectedCompanion.frame.origin.y,
                                                  self.selectedCompanion.frame.size.width, self.selectedCompanion.frame.size.height);
    }
    
}

-(void)showCompanionPlayView
{
    ASLogInfo(@"showCompanionPlayView CompanionViewPad");
    
    self.mediaItem = [TVMixedCompanionManager sharedInstance].casstingMediaItem;
    self.companionPlayView = [CompanionPlayView companionPlayView];
    self.companionPlayView.delegate = self;
    [self.companionPlayView configureWithItem:self.mediaItem];
    
    self.playerViewMode.hidden = NO;
    self.companionPlayView.hidden = NO;
    self.companionPlayView.alpha = 1.0;
     self.playerViewMode.alpha = 1.0;

    self.viewMainServer.hidden = YES;
    [self bringSubviewToFront:self.playerViewMode];
    [self.playerViewMode addSubview:self.companionPlayView];
    
    CGFloat width = self.companionPlayView.frame.size.width;
    CGFloat height = self.companionPlayView.frame.size.height;
    CGFloat x = (self.playerViewMode.frame.size.width - width) / 2.f;
    CGFloat y = (self.playerViewMode.frame.size.height - height) / 2.f;
    self.companionPlayView.frame = CGRectMake(x, y, width, height);

    
    ASLogInfo(@"self.companionPlayView = %@",self.companionPlayView);
    ASLogInfo(@"self.playerViewMode = %@",self.playerViewMode);
    ASLogInfo(@"self.viewMainServer = %@",self.viewMainServer);

}

-(void)removeCompanionPlayView
{
    self.viewMainServer.hidden=NO;
    self.viewMainServer.alpha =1.0;
    [UIView animateWithDuration:0.3 animations:^{
        self.companionPlayView.alpha =0;
        self.playerViewMode.alpha = 0;
    } completion:^(BOOL finished) {
        self.companionPlayView = nil;
        self.playerViewMode.hidden=YES;
    }];
}
@end
