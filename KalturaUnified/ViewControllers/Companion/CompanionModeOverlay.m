//
//  CompanionModeOverlay.m
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//



#import "CompanionModeOverlay.h"
#import "TVCompanionManager.h"
#import "UIView+LoadNib.h"
#import "UIImage+Tint.h"

@implementation CompanionModeOverlay

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


+(CompanionModeOverlay *) companionModeOverlay
{
    return [CompanionModeOverlay loadNibName:nil];
}

-(void)awakeFromNib
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)   name:UIDeviceOrientationDidChangeNotification  object:nil];
    
    if (isPad)
    {
        self.companionDescription.text = LS(@"companion_PlayingText");
        self.companionDescription.font = Font_Reg(18);
        [self.buttonStopCompanion.titleLabel setFont:Font_Bold(18)];
        
    }
    
    UIImage *btnImage = [UIImage imageNamed:@"pin_dial_btn.png"];
    btnImage = [btnImage imageTintedWithColor:APST.brandColor];
    [self.buttonStopCompanion setBackgroundImage:[btnImage stretchableImageWithLeftCapWidth:btnImage.size.width / 2.0 topCapHeight:btnImage.size.height / 2.0] forState:UIControlStateNormal];
    
    btnImage = [UIImage imageNamed:@"pin_dial_btn.png"];
    [self.buttonStopCompanion setBackgroundImage:[btnImage stretchableImageWithLeftCapWidth:btnImage.size.width / 2.0 topCapHeight:btnImage.size.height / 2.0] forState:UIControlStateHighlighted];
    
    [self.buttonStopCompanion setTitle:NSLocalizedString(@"companion_StopPlaying", nil) forState:UIControlStateNormal];
    [self.buttonStopCompanion setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.companionDeviceName.text = [[TVCompanionManager sharedInstance] getDeviceName:[CompanionManager sharedManager].devicePaired];
}

-(void)setMediaItem:(TVMediaItem *)mediaItem
{
    _mediaItem = mediaItem;
    if ([mediaItem isLive] == NO)
    {
        self.buttonStopCompanion.hidden = YES;
    }
    else
    {
        self.buttonStopCompanion.hidden = NO;
    }
}

- (IBAction)stopCompanion:(id)sender
{
    
    [[TVMixedCompanionManager sharedInstance] sendMessageToCurrentDevice:@"CompanionMessageStop" withExtras:nil withCompletion:^(bool success, NSDictionary *extras) {
        
    }];
    [TVMixedCompanionManager sharedInstance].currentCompanionPlayerState = CompanionPlayerStateStop;
    [self.delegate companionModeOverlay:self didSelectStopCompanion:sender];
}



-(void) animateInOnView:(UIView *) superView  withCompletion:(void (^)()) completion
{
    self.size = superView.size;
    [superView addSubview:self];
    self.isPresnted = YES;
    if (completion)
    {
        
        completion();
    }
}


-(void) animateOutWithCompletion:(void (^)()) completion
{
    [self removeFromSuperview];
    
    self.isPresnted = NO;
    if (completion)
    {
        completion();
    }
}


- (void)orientationChanged:(NSNotification *)note
{
    
    if (isPhone)
    {
        switch ([[UIApplication sharedApplication] statusBarOrientation])
        {
            case UIInterfaceOrientationPortrait:
            case UIInterfaceOrientationPortraitUpsideDown:
            {
                self.imageViewTV.image = [UIImage imageNamed:@"companion_tv_logo"];
                UIFont * font = [self.buttonStopCompanion.titleLabel.font fontWithSize:11];
                self.buttonStopCompanion.titleLabel.font= font;
            }
                break;
            case UIInterfaceOrientationLandscapeLeft:
            case UIInterfaceOrientationLandscapeRight:
            {
                self.imageViewTV.image = [UIImage imageNamed:@"companion_landscape_computer"];
                UIFont * font = [self.buttonStopCompanion.titleLabel.font fontWithSize:16];
                self.buttonStopCompanion.titleLabel.font= font;
                
            }
                break;
                
            default:
                break;
        }
        
    }
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
