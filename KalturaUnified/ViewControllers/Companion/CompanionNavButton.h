//
//  CompanionNavButton.h
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

@class CompanionNavButton;

@protocol  CompanionNavButtonDelegate <NSObject>
-(void) companionNavButtonPressed:(CompanionNavButton *) sender;
@end

#import <UIKit/UIKit.h>




@interface CompanionNavButton : UIView

@property (weak, nonatomic) id<CompanionNavButtonDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *buttonCompanion;

+(CompanionNavButton *) companionNavButton;

@end
