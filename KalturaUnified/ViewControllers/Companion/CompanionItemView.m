//
//  CompanionItemView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 02.01.15.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CompanionItemView.h"

@implementation CompanionItemView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    
    UIView *viewImgAvatar = [_imgAvatar superview];
    viewImgAvatar.layer.cornerRadius = _imgAvatar.frame.size.width / 2.0;
    viewImgAvatar.clipsToBounds = YES;
    
    self.labelName.font = Font_Bold(self.labelName.font.pointSize);
    
    self.viewUser.hidden = YES;
    
}

@end
