//
//  CompanionView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 02.01.15.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanionItemView.h"
#import "CompanionScrollView.h"
#import "TVCompanionManager.h"
#import "CompanionView.h"



@class PlayerViewController;

@interface CompanionViewPhone : CompanionView {
    
    IBOutlet UIButton *buttonClose;
    IBOutlet UIButton *buttonBack;
    
    IBOutlet UIView *viewServerL;
    IBOutlet UIView *viewServerP;
    
    
    IBOutlet UIView *viewYLeft;
    IBOutlet UIView *viewYCenter;
    IBOutlet UIView *viewYRight;
    
    IBOutlet UIView *viewUserP;
    
    IBOutlet UIImageView *imgAvatarP;
    
    IBOutlet UIView *viewDragP;
    IBOutlet UILabel *labelDragP;
    
    IBOutlet UIView *viewDevicesP;
    IBOutlet UIScrollView *scrollViewDevicesP;
    
    IBOutlet UIImageView *imgToolbar;
    IBOutlet UIImageView *imgWhatBg;
    IBOutlet UIView *viewCompanionActivity;
    IBOutlet UIImageView *imgViewCompanionActivity;
    IBOutlet UIView *viewNoDevices;
    
    BOOL isLandscape;
}

- (id)init;
- (void)updateTexts;
- (void)updateScreenSize;

@property (nonatomic, strong) CompanionItemView *selectedCompanionP;
@property (weak, nonatomic) IBOutlet UIView *playerViewMode;

@end
