//
//  CompanionView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 02.01.15.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CompanionViewPhone.h"
#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+Animation.h"
#import "PlayerViewController.h"
#import <TvinciSDK/TVMixedCompanionManager.h>

@interface CompanionViewPhone ()

@property (assign, nonatomic) CGSize playViewSize;

@end


@implementation CompanionViewPhone



- (id)init {
    self = [super init];
    
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    if (self) {
    }
    return self;
    
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    UIImage *toolbarImage = [UIImage imageNamed:@"companion_toolbar"];
    imgToolbar.image = [toolbarImage stretchableImageWithLeftCapWidth:toolbarImage.size.width / 2.0 topCapHeight:toolbarImage.size.height / 2.0];
    
    [self updateTexts];
    
}


- (void)updateTexts
{
    [super updateTexts];
    
    [Utils setLabelFont:labelTitle];
    
    NSString *str = [labelWhatThatText text];
    NSInteger strLength = [str length];
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:str];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:14];
    [attString addAttribute:NSParagraphStyleAttributeName
                      value:style
                      range:NSMakeRange(0, strLength)];
    
    labelWhatThatText.attributedText = attString;
    
    [self updateButton:buttonClose title:LS(@"close") tint:NO];
    
    UIView *viewImgAvatar = [imgAvatar superview];
    viewImgAvatar.layer.cornerRadius = imgAvatar.frame.size.width / 2.0;
    viewImgAvatar.clipsToBounds = YES;
    
    if (self.selectedCompanion == nil)
    {
        self.selectedCompanion = [[[NSBundle mainBundle] loadNibNamed:@"CompanionItemView" owner:self options:nil] objectAtIndex:1];
    }
    
    self.selectedCompanion.viewUser.hidden = NO;
    [viewServerL addSubview:self.selectedCompanion];
    
    labelDragP.text = labelDrag.text;
    labelDragP.font = Font_Bold(labelDragP.font.pointSize);
    
    viewImgAvatar = [imgAvatarP superview];
    viewImgAvatar.layer.cornerRadius = imgAvatarP.frame.size.width / 2.0;
    viewImgAvatar.clipsToBounds = YES;
    
    if (self.selectedCompanionP== nil)
    {
        self.selectedCompanionP = [[[NSBundle mainBundle] loadNibNamed:@"CompanionItemView" owner:self options:nil] objectAtIndex:1];
        
    }
    self.selectedCompanionP.viewUser.hidden = NO;
    [viewServerP addSubview:self.selectedCompanionP];
    
    [self initCompanionView];
    [self initWhatThisView];
    
    
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragUserAction:)] ;
    [viewUser addGestureRecognizer:recognizer];
    
    recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragUserPAction:)];
    [viewUserP addGestureRecognizer:recognizer];
    [Utils setLabelFont:labelScanning bold:YES];
    viewLoading.hidden = YES;
    [Utils setLabelFont:labelNoDevice bold:YES];
    
    
    buttonTryAgain.center = CGPointMake(viewNoDevices.frame.size.width / 2, buttonTryAgain.center.y);
    viewNoDevices.hidden = YES;
    
    [self updateUIAccordingToConnectionMode];
    
}

- (void)updateButton:(UIButton *)button title:(NSString *)title tint:(BOOL)tint {
    
    [Utils setButtonFont:button bold:YES];
    UIImage *btnImage = [UIImage imageNamed:@"gray_btn"];
    if (tint) {
        btnImage = [btnImage imageTintedWithColor:CS(@"d64a49")];
    }
    
    int btnWidth = [title sizeWithAttributes:@{NSFontAttributeName:button.titleLabel.font}].width + 32;
    button.frame = CGRectMake(button.frame.origin.x, button.frame.origin.y, btnWidth, button.frame.size.height);
    [button setBackgroundImage:[btnImage stretchableImageWithLeftCapWidth:btnImage.size.width / 2.0 topCapHeight:btnImage.size.height / 2.0] forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    if (tint) {
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    } else {
        [button setTitleColor:CS(@"b3b3b3") forState:UIControlStateNormal];
        [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    }
    [button.titleLabel setFont:Font_Bold(14)];
    
}


- (void)updateScreenSize {
    
    isLandscape = self.frame.size.width > self.frame.size.height;
    
    buttonBack.hidden = isLandscape;
    buttonClose.hidden = !isLandscape;
    
    imgWhatBg.hidden = isLandscape;
    
    
    if (isLandscape) {
        
        labelTitle.text = LS(@"companion_CompanionTitle");
        labelTitle.font = Font_Bold(14);
        labelTitle.textAlignment = NSTextAlignmentLeft;
        labelTitle.textColor = CS(@"b5b5b5");
        
        buttonClose.frame = CGRectMake(self.frame.size.width - buttonClose.frame.size.width - 10, buttonClose.frame.origin.y, buttonClose.frame.size.width, buttonClose.frame.size.height);
        
        labelTitle.frame = CGRectMake(labelTitle.frame.origin.x, labelTitle.frame.origin.y, self.frame.size.width - buttonClose.frame.size.width - 10 - labelTitle.frame.origin.x, labelTitle.frame.size.height);
        //viewCompanionActivity.center = CGPointMake(290.0 + viewServerL.frame.origin.x, 24.0 + ([viewServerL superview].frame.size.height / 2.0));
        
    } else {
        
        labelTitle.text = LS(@"companion_title");
        labelTitle.font = Font_Bold(18);
        labelTitle.textAlignment = NSTextAlignmentCenter;
        labelTitle.textColor = [UIColor whiteColor];
        
        labelTitle.frame = CGRectMake(labelTitle.frame.origin.x, labelTitle.frame.origin.y, self.frame.size.width - (labelTitle.frame.origin.x * 2), labelTitle.frame.size.height);
        //viewCompanionActivity.center = CGPointMake([viewServerP superview].frame.size.width / 2.0, 120.0);
        
    }
    self.playerViewMode.frame = viewServer.bounds;
    if (isLandscape) {
        self.companionPlayView.frame = CGRectMake(0.f, 0.f, self.playViewSize.width, self.playViewSize.height);
    } else {
        self.companionPlayView.frame = CGRectMake(0.f, 0.f, self.playerViewMode.frame.size.width, self.playViewSize.height);
    }
    [self.companionPlayView relayout];
    self.companionPlayView.center = self.playerViewMode.center;
    
    viewLoading.center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0);
    viewNoDevices.center = CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0);
    
    [viewServerL superview].hidden = !isLandscape;
    [viewServerP superview].hidden = isLandscape;
    
}

- (void)initWhatThisView {
    
    [super initWhatThisView];
    
    viewCompanionActivity.alpha = 0;
    viewLoading.hidden = YES;
    
}

- (void)initCompanionView {
    
    [super initCompanionView];
    
    self.selectedCompanion.center = viewXRight.center;
    
    viewDragP.hidden = !canDragViewUser;
    viewUserP.hidden = NO;
    viewUserP.center = viewYLeft.center;
    viewUserP.userInteractionEnabled = YES;
    imgAvatarP.image = [UIImage imageNamed:@"companion_user"];
    
    if ([[TVSessionManager sharedTVSessionManager] isUserConnectedToFacebook]) {
        [imgAvatarP sd_setImageWithURL:[[[TVSessionManager sharedTVSessionManager]currentUser] facebookPictureURLForSizeType:TVUserFacebookPictureTypeNormal] placeholderImage:[UIImage imageNamed:@"companion_user"]];
    }
    
    self.selectedCompanionP.center = viewYRight.center;
    self.selectedCompanionP.hidden = YES;
    self.selectedCompanionP.imgAvatar.image = [UIImage imageNamed:@"companion_user"];
    
}

- (void)dragUserAction:(UIPanGestureRecognizer *)recognizer {
    
    
    CGPoint translation = [recognizer translationInView:recognizer.view];
    
    float x = viewUser.center.x + translation.x;
    
    float checkCenterX = viewXCenter.center.x - 70.0;
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        if (x < checkCenterX) {
            
            float time = ((viewUser.center.x - viewXLeft.center.x) / (checkCenterX - viewXLeft.center.x)) * 0.7;
            
            [UIView animateWithDuration:time animations:^{
                
                viewUser.center = viewXLeft.center;
                viewUserP.center = viewYLeft.center;
                
            } completion:^(BOOL finished){
                
                canDragViewUser = [self canDrag];//[[TVCompanionManager sharedInstance].devices count];
                viewDrag.hidden = !canDragViewUser;
                viewDragP.hidden = !canDragViewUser;
                
            }];
            
        }
        
        
    } else if (canDragViewUser) {
        
        
        if (x < viewXLeft.center.x) {
            x = viewXLeft.center.x;
        }
        if (x >= checkCenterX) {
            
            viewUser.center = CGPointMake(checkCenterX, viewUser.center.y);
            
            canDragViewUser = NO;
            viewDrag.hidden = !canDragViewUser;
            viewDragP.hidden = !canDragViewUser;
            
            [UIView animateWithDuration:0.7 animations:^{
                
                viewUser.center = viewXRight.center;
                viewUserP.center = viewYRight.center;
                
            } completion:^(BOOL finished){
                
                self.selectedCompanion.center = viewXRight.center;
                
                self.selectedCompanion.imgAvatar.image = imgAvatar.image;
                self.selectedCompanion.hidden = NO;
                viewUser.hidden = YES;
                
                
                self.selectedCompanionP.imgAvatar.image = imgAvatarP.image;
                self.selectedCompanionP.hidden = NO;
                viewUserP.hidden = YES;
                
                NSInteger index = selectedDevice;
                
                NSArray *devices = [TVCompanionManager sharedInstance].m_devices;
                // [TVCompanionManager sharedInstance].devices;
                while (index < 0) index += [devices count];
                while (index >= [devices count]) index -= [devices count];
                
                TVAbstractCompanionDevice * device = devices[index];
                self.selectedCompanion.labelName.text = device.name;
                self.selectedCompanionP.labelName.text = device.name;
                
                [self connectDevice:device];
                
                viewDevices.hidden = YES;
                viewDevicesP.hidden = YES;
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.selectedCompanion.center = viewXCenter.center;
                    self.selectedCompanionP.center = viewYCenter.center;
                    
                } completion:^(BOOL finished){
                    
                    buttonScan.hidden = YES;
                    buttonWhatThis.hidden = YES;
                    buttonDisconnect.hidden = NO;
                    
                }];
                
            }];
        } else {
            viewUser.center = CGPointMake(x, viewUser.center.y);
        }
        
        
    }
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self];
    
}

- (void)dragUserPAction:(UIPanGestureRecognizer *)recognizer {
    
    
    CGPoint translation = [recognizer translationInView:recognizer.view];
    
    float y = viewUserP.center.y + translation.y;
    
    float checkCenterY = viewYCenter.center.y + 20.0;
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        if (y > checkCenterY) {
            
            float time = ((viewUserP.center.y - viewYLeft.center.y) / (checkCenterY - viewYLeft.center.x)) * 0.7;
            
            [UIView animateWithDuration:time animations:^{
                
                viewUser.center = viewXLeft.center;
                viewUserP.center = viewYLeft.center;
                
            } completion:^(BOOL finished){
                
                canDragViewUser = [self canDrag]; //[[TVCompanionManager sharedInstance].devices count];
                viewDrag.hidden = !canDragViewUser;
                viewDragP.hidden = !canDragViewUser;
                
            }];
            
        }
        
        
    } else if (canDragViewUser) {
        
        
        if (y > viewYLeft.center.y) {
            y = viewYLeft.center.y;
        }
        if (y <= checkCenterY) {
            
            viewUserP.center = CGPointMake(viewUserP.center.x, checkCenterY);
            
            canDragViewUser = NO;
            viewDrag.hidden = !canDragViewUser;
            viewDragP.hidden = !canDragViewUser;
            
            [UIView animateWithDuration:0.7 animations:^{
                
                viewUser.center = viewXRight.center;
                viewUserP.center = viewYRight.center;
                
            } completion:^(BOOL finished){
                
                self.selectedCompanion.center = viewXRight.center;
                
                self.selectedCompanion.imgAvatar.image = imgAvatar.image;
                self.selectedCompanion.hidden = NO;
                viewUser.hidden = YES;
                
                
                self.selectedCompanionP.imgAvatar.image = imgAvatarP.image;
                self.selectedCompanionP.hidden = NO;
                viewUserP.hidden = YES;
                
                NSInteger index = selectedDevice;
                
                NSArray *devices = [TVCompanionManager sharedInstance].m_devices;
                while (index < 0) index += [devices count];
                while (index >= [devices count]) index -= [devices count];
                
                TVAbstractCompanionDevice * device = [devices objectAtIndex:index];
                self.selectedCompanion.labelName.text = device.name;
                self.selectedCompanionP.labelName.text = device.name;
                
                [self connectDevice:device];
                
                viewDevices.hidden = YES;
                viewDevicesP.hidden = YES;
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.selectedCompanion.center = viewXCenter.center;
                    self.selectedCompanionP.center = viewYCenter.center;
                    
                } completion:^(BOOL finished){
                    
                    buttonScan.hidden = YES;
                    buttonWhatThis.hidden = YES;
                    buttonDisconnect.hidden = NO;
                    
                }];
                
            }];
        } else {
            viewUserP.center = CGPointMake(viewUserP.center.x, y);
        }
        
        
    }
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self];
    
}


- (void)updateDevicesList {
    
    NSLog(@"updateDevicesList");
    
    for (UIView *subview in [scrollViewDevices subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            subview.hidden = YES;
        }
    }
    
    scrollViewDevices.contentSize = scrollViewDevices.frame.size;
    scrollViewDevicesP.contentSize = scrollViewDevicesP.frame.size;
    
    // Note: this is a workaround to solve the problem, that the SDK is not sending the most updated CC devices .. we will take from the SDK only the STB list and use our CC list from [TVCompanionManager sharedInstance].m_devices
    NSArray *devicesAll = [[TVMixedCompanionManager sharedInstance] getDiscoveredDevices];
    
    for (TVAbstractCompanionDevice *dev in devicesAll) {
        NSInteger deviceType = [dev getType];
        if (deviceType == 0x3015)
        {//chromecast
        }
        else
        {//stb
            [[TVCompanionManager sharedInstance].m_devices addObject:dev ];
            
        }
    }

    NSArray *devices = [NSArray arrayWithArray:[TVCompanionManager sharedInstance].m_devices];

    
    if ([devices count] > 1) {
        
        scrollViewDevices.contentSize = CGSizeMake(scrollViewDevices.frame.size.width, scrollViewDevices.frame.size.height * [devices count] * 3);
        scrollViewDevicesP.contentSize = CGSizeMake(scrollViewDevicesP.frame.size.width * [devices count] * 3, scrollViewDevicesP.frame.size.height);
        
        [self initScrollViewDevices:0];
        
    } else if ([devices count] == 1) {
        
        
        TVAbstractCompanionDevice * device = [devices objectAtIndex:0];
        
        scrollViewDevices.contentSize = CGSizeMake(scrollViewDevices.frame.size.width, scrollViewDevices.frame.size.height);
        CompanionItemView *companionItem = [self getFreeCompanionItem];
        companionItem.frame = CGRectMake(companionItem.frame.origin.x, 0, companionItem.frame.size.width, companionItem.frame.size.height);
        companionItem.labelName.text = device.name;
        
        scrollViewDevicesP.contentSize = CGSizeMake(scrollViewDevicesP.frame.size.width, scrollViewDevicesP.frame.size.height);
        companionItem = [self getFreeCompanionItemP];
        companionItem.frame = CGRectMake(companionItem.frame.origin.x, 0, companionItem.frame.size.width, companionItem.frame.size.height);
        companionItem.labelName.text = device.name;
        
        
    }
    if (![[TVMixedCompanionManager sharedInstance] isConnected]) {
        canDragViewUser = [self canDrag]; // [[TVCompanionManager sharedInstance].devices count];
        [self needToHideNoDeviceView:canDragViewUser];
       
    }
    if ([devices count]==0){
        [self needToHideNoDeviceView:NO];
    }

    
    [self hideCompanionHUD];
    
}
-(void)needToHideNoDeviceView :(BOOL)needToHide{
    viewDrag.hidden = !needToHide;
    viewDragP.hidden = !needToHide;
    
    viewDevices.hidden = !needToHide;
    viewDevicesP.hidden = !needToHide;
    viewUser.hidden = !needToHide;
    viewUserP.hidden = !needToHide;
    
    viewNoDevices.hidden = needToHide;
}
- (IBAction)buttonWhatThisPressed:(UIButton *)button {
    
    [self initWhatThisView];
    
}

- (void)initScrollViewDevices:(int)device {
    
    selectedDevice = device;
    
    for (UIView *subview in [scrollViewDevices subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            subview.hidden = YES;
        }
    }
    for (UIView *subview in [scrollViewDevicesP subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            subview.hidden = YES;
        }
    }
    
    NSArray *devices = [TVCompanionManager sharedInstance].m_devices;
    
    int y = scrollViewDevices.frame.size.height * ([devices count] + selectedDevice);
    
    scrollViewDevices.contentOffset = CGPointMake(0, y);
    
    for (int i = -2; i < 3; i++) {
        
        CompanionItemView *companionItem = [self getFreeCompanionItem];
        companionItem.frame = CGRectMake(companionItem.frame.origin.x, y + (i * companionItem.frame.size.height), companionItem.frame.size.width, companionItem.frame.size.height);
        
        NSInteger index = selectedDevice + i;
        
        while (index < 0) index += [devices count];
        while (index >= [devices count]) index -= [devices count];
        
        TVAbstractCompanionDevice * device = devices[index];
        companionItem.labelName.text = device.name;
        
    }
    
    int x = scrollViewDevicesP.frame.size.width * ([devices count] + selectedDevice);
    
    scrollViewDevicesP.contentOffset = CGPointMake(x, 0);
    
    for (int i = -2; i < 3; i++) {
        
        CompanionItemView *companionItem = [self getFreeCompanionItemP];
        companionItem.frame = CGRectMake(x + (i * companionItem.frame.size.width), companionItem.frame.origin.y, companionItem.frame.size.width, companionItem.frame.size.height);
        
        NSInteger index = selectedDevice + i;
        
        while (index < 0) index += [devices count];
        while (index >= [devices count]) index -= [devices count];
        
        TVAbstractCompanionDevice * device = devices[index];
        companionItem.labelName.text = device.name;
    }
    
}

- (CompanionItemView *)getFreeCompanionItem {
    
    for (UIView *subview in [scrollViewDevices subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            if (subview.hidden) {
                subview.hidden = NO;
                
                return (CompanionItemView *)subview;
                
            }
        }
    }
    
    CompanionItemView *companionItem = [[[NSBundle mainBundle] loadNibNamed:@"CompanionItemView" owner:self options:nil] objectAtIndex:0];
    companionItem.viewUser.hidden = YES;
    [scrollViewDevices addSubview:companionItem];
    
    return companionItem;
    
}

- (CompanionItemView *)getFreeCompanionItemP {
    
    for (UIView *subview in [scrollViewDevicesP subviews]) {
        if ([subview isKindOfClass:[CompanionItemView class]]) {
            if (subview.hidden) {
                subview.hidden = NO;
                
                return (CompanionItemView *)subview;
                
            }
        }
    }
    
    CompanionItemView *companionItem = [[[NSBundle mainBundle] loadNibNamed:@"CompanionItemView" owner:self options:nil] objectAtIndex:1];
    companionItem.viewUser.hidden = YES;
    [scrollViewDevicesP addSubview:companionItem];
    
    return companionItem;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView == scrollViewDevices) {
        
        if (((int)scrollView.contentOffset.y) % ((int)scrollView.frame.size.height) == 0) {
            
            NSInteger page = scrollView.contentOffset.y / scrollView.frame.size.height;
            
            NSArray *devices = [TVCompanionManager sharedInstance].m_devices;
            
            NSInteger newSelectedDevice = page - [devices count];
            
            if (newSelectedDevice != selectedDevice) {
                
                selectedDevice = newSelectedDevice;
                
                if (selectedDevice == -[devices count] || selectedDevice == [devices count]) {
                    [self initScrollViewDevices:0];
                } else {
                    
                    for (UIView *subview in [scrollViewDevices subviews]) {
                        if ([subview isKindOfClass:[CompanionItemView class]]) {
                            subview.hidden = YES;
                        }
                    }
                    
                    int y = scrollViewDevices.frame.size.height * ([devices count] + selectedDevice);
                    
                    for (int i = -2; i < 3; i++) {
                        
                        CompanionItemView *companionItem = [self getFreeCompanionItem];
                        companionItem.frame = CGRectMake(companionItem.frame.origin.x, y + (i * companionItem.frame.size.height), companionItem.frame.size.width, companionItem.frame.size.height);
                        
                        NSInteger index = selectedDevice + i;
                        
                        while (index < 0) index += [devices count];
                        while (index >= [devices count]) index -= [devices count];
                        
                        TVAbstractCompanionDevice * device = devices[index];
                        companionItem.labelName.text = device.name;
                        
                        
                    }
                    
                    
                }
                
            }
            
            
        }
        
        
    } else if (scrollView == scrollViewDevicesP) {
        
        if (((int)scrollView.contentOffset.x) % ((int)scrollView.frame.size.width) == 0) {
            
            int page = scrollView.contentOffset.x / scrollView.frame.size.width;
            
            NSArray *devices = [TVCompanionManager sharedInstance].m_devices;
            
            NSInteger newSelectedDevice = page - [devices count];
            
            if (newSelectedDevice != selectedDevice) {
                
                selectedDevice = newSelectedDevice;
                
                if (selectedDevice == -[devices count] || selectedDevice == [devices count]) {
                    [self initScrollViewDevices:0];
                } else {
                    
                    for (UIView *subview in [scrollViewDevicesP subviews]) {
                        if ([subview isKindOfClass:[CompanionItemView class]]) {
                            subview.hidden = YES;
                        }
                    }
                    
                    int x = scrollViewDevicesP.frame.size.width * ([devices count] + selectedDevice);
                    
                    for (int i = -2; i < 3; i++) {
                        
                        CompanionItemView *companionItem = [self getFreeCompanionItemP];
                        companionItem.frame = CGRectMake(x + (i * companionItem.frame.size.width), companionItem.frame.origin.y, companionItem.frame.size.width, companionItem.frame.size.height);
                        
                        NSInteger index = selectedDevice + i;
                        
                        while (index < 0) index += [devices count];
                        while (index >= [devices count]) index -= [devices count];
                        
                        TVAbstractCompanionDevice * device = devices[index];
                        companionItem.labelName.text = device.name;
                        
                        
                    }
                }
            }
        }
    }
}

- (void)companionDevicePaired {
    
    
}

- (void)companionDeviceUnPaired {
    if (!buttonDisconnect.hidden) {
        [self buttonDisconnectPressed:buttonDisconnect];
    }
}

- (IBAction)buttonDisconnectPressed:(UIButton *)button {
    
    [[TVMixedCompanionManager sharedInstance] disconnect];
    
    [self initCompanionView];
    
    viewUser.center = viewXCenter.center;
    viewDevices.hidden = NO;
    
    viewUserP.center = viewYCenter.center;
    viewDevicesP.hidden = NO;
    buttonDisconnect.hidden =YES;
    
    if (self.companionPlayView.superview)
    {
        [self removeCompanionPlayView];
    }
    
    [UIView animateWithDuration:0.7 animations:^{
        
        viewUser.center = viewXLeft.center;
        viewUserP.center = viewYLeft.center;
        
    } completion:^(BOOL finished){
        
        canDragViewUser = [self canDrag]; //[[TVCompanionManager sharedInstance].devices count];
        viewDrag.hidden = !canDragViewUser;
        viewDragP.hidden = !canDragViewUser;
        buttonDisconnect.hidden = YES;
        buttonWhatThis.hidden = NO;


        
    }];
}

- (IBAction)buttonScanPressed:(UIButton *)button {
    
    
    [super buttonScanPressed:button];
    
    
    if (viewWhatThis.hidden == YES)
    {
        viewServer.hidden = NO;
        
        buttonScan.hidden = YES;
        
        viewDevices.hidden = YES;
        viewDevicesP.hidden = YES;
        viewDrag.hidden = YES;
        viewDragP.hidden = YES;
        viewUser.hidden = YES;
        viewUserP.hidden = YES;
        
        buttonWhatThis.hidden = NO;
        
        viewNoDevices.hidden = YES;
        
        [self showCompanionHUD];
        
        [self scanForDevices];
        
    }
    
    
}


//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//
//    if (buttonIndex == 1) {
//
//        [self buttonClosePressed:nil];
//    }
//
//}

- (IBAction)buttonClosePressed:(UIButton *)button {
    
    [self.delegate hideCompanionScreen];
    
}

#pragma mark -

- (void)showCompanionHUD {
    
    [imgViewCompanionActivity startBrandAnimation];
    imgViewCompanionActivity.center = CGPointMake(viewCompanionActivity.frame.size.width / 2.0, viewCompanionActivity.frame.size.height / 2.0);
    
    
    viewLoading.hidden = NO;
    
    [viewServer bringSubviewToFront:viewLoading];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         viewCompanionActivity.alpha = 1.0;
                         
                     }];
}

- (void)hideCompanionHUD {
    
    viewLoading.hidden = YES;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         viewCompanionActivity.alpha = 0.0;
                         
                     } completion:^(BOOL finished) {
                         
                         [imgViewCompanionActivity stopBrandAnimation];
                         
                     }];
}

-(void) updateUIAccordingToConnectionMode
{
    
    if ([[TVMixedCompanionManager sharedInstance] isConnected])
    {
        viewWhatThis.hidden =YES;
        viewServer.hidden = NO;
        [self updateDevicesList];
        self.selectedCompanion.center = viewXRight.center;
        
        self.selectedCompanion.imgAvatar.image = imgAvatar.image;
        self.selectedCompanion.hidden = NO;
        viewUser.hidden = YES;
        self.selectedCompanionP.imgAvatar.image = imgAvatarP.image;
        self.selectedCompanionP.hidden = NO;
        viewUserP.hidden = YES;
        TVAbstractCompanionDevice *device = [[TVMixedCompanionManager sharedInstance] getCurrentConnectedDevice];
        
        self.selectedCompanion.labelName.text = device.name;
        self.selectedCompanionP.labelName.text = device.name;
        
        viewDevices.hidden = YES;
        viewDevicesP.hidden = YES;
        
        self.selectedCompanion.center = viewXCenter.center;
        self.selectedCompanionP.center = viewYCenter.center;
        
        buttonScan.hidden = YES;
        buttonWhatThis.hidden = YES;
        buttonDisconnect.hidden = NO;
        
        viewDragP.hidden = YES;
        viewDrag.hidden = YES;
    }
    
}
-(BOOL)isUserConnected
{
    if (buttonDisconnect.hidden == YES)
    {
        return NO;
    }
    return YES;
}

-(void)showCompanionPlayView
{
    [super showCompanionPlayView];
    self.playViewSize = self.companionPlayView.frame.size;
    self.playerViewMode.hidden = NO;
    self.playerViewMode.alpha = 1;
    [self.playerViewMode addSubview:self.companionPlayView];
    [viewServer bringSubviewToFront:self.playerViewMode];
    
    isLandscape = self.frame.size.width > self.frame.size.height;
    if (isLandscape) {
        self.companionPlayView.frame = CGRectMake(0.f, 0.f, self.playViewSize.width, self.playViewSize.height);
    } else {
        self.companionPlayView.frame = CGRectMake(0.f, 0.f, self.playerViewMode.frame.size.width, self.playViewSize.height);
    }
    [self.companionPlayView relayout];
    self.companionPlayView.center = self.playerViewMode.center;
    
    self.playerViewMode.frame = viewServer.bounds;
    
    viewWhatThis.hidden = YES;
    viewServer.hidden = NO;
    
    buttonScan.hidden = YES;    
    viewDevices.hidden = YES;
    viewDevicesP.hidden = YES;
    viewDrag.hidden = YES;
    viewDragP.hidden = YES;
    viewUser.hidden = YES;
    viewUserP.hidden = YES;
    
    CGFloat width = self.companionPlayView.frame.size.width;
    CGFloat height = self.companionPlayView.frame.size.height;
    CGFloat x = (self.playerViewMode.frame.size.width - width) / 2.f;
    CGFloat y = (self.playerViewMode.frame.size.height - height) / 2.f;
    self.companionPlayView.frame = CGRectMake(x, y, width, height);
    buttonDisconnect.hidden = NO;
    [viewServer bringSubviewToFront:buttonDisconnect];
    [self.companionPlayView relayout];
}

-(void)removeCompanionPlayView
{
    [UIView animateWithDuration:0.3 animations:^{
        self.companionPlayView.alpha =0;
        self.playerViewMode.alpha = 0;
    } completion:^(BOOL finished) {
        self.companionPlayView = nil;
        self.playerViewMode.hidden=YES;
//        [self buttonClosePressed:nil];
    }];
}
@end
