//
//  TVNCompanionView.h
//  Tvinci
//
//  Created by Pavel Belevtsev on 28.04.14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanionItemView.h"
#import "CompanionScrollView.h"
#import "TVCompanionManager.h"
#import "CompanionView.h"
#import "ActivityView.h"


@interface CompanionViewPad : CompanionView<CompanionPlayViewDelegate> {
    
}


- (id)init;

@property (nonatomic, strong) ActivityView *fullScreenProgress;

@property (weak, nonatomic) IBOutlet UIView *playerViewMode;

@end
