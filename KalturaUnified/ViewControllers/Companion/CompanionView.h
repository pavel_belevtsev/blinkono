//
//  CompanionView.h
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TvinciSDK/TVMixedCompanionManager.h>
#import "CompanionPlayView.h"

@protocol CompanionViewDelegate <NSObject>
-(void) hideCompanionScreen;
-(void) showLoginView;

@end

@interface CompanionView : UIView
{
    IBOutlet UILabel *labelWhatThatText;
    IBOutlet UIView *viewWhatThis;
    IBOutlet UILabel *labelTitle;
    IBOutlet UIView *viewServer;
    
    IBOutlet UIButton *buttonScan;
    IBOutlet UIButton *buttonWhatThis;
    IBOutlet UIButton *buttonDisconnect;
    IBOutlet UIImageView *imgAvatar;
    
    IBOutlet UIView *viewXLeft;
    IBOutlet UIView *viewXCenter;
    IBOutlet UIView *viewXRight;
    IBOutlet UIView *viewUser;
    
    IBOutlet UIView *viewDrag;
    IBOutlet UILabel *labelDrag;
    
    IBOutlet UIView *viewDevices;
    IBOutlet UIScrollView *scrollViewDevices;
    IBOutlet UIButton *buttonTryAgain;
    IBOutlet UILabel *labelScanning;
    
    
    IBOutlet UIView *viewLoading;
    IBOutlet UILabel *labelNoDevice;
    
    NSInteger selectedDevice;
    BOOL canDragViewUser;


    

}

@property (nonatomic, strong) CompanionItemView *selectedCompanion;
@property (nonatomic, weak) id<CompanionViewDelegate> delegate;
@property (nonatomic, strong) CompanionPlayView * companionPlayView;
@property (nonatomic, strong) TVMediaItem * mediaItem;

+ (CompanionView *) companionView;

@property (weak, nonatomic) IBOutlet UIView *viewMainServer;

-(BOOL) canDrag;
-(void) scanForDevices;
-(void) updateDevicesList;
-(void) connectDevice:(TVAbstractCompanionDevice *) device;
-(void) updateUIAccordingToConnectionMode;
- (void)updateTexts;
-(void) initWhatThisView;
-(void) initCompanionView;
- (IBAction)buttonScanPressed:(UIButton *)button;
-(BOOL)isUserConnected;
-(void)showCompanionPlayView;
-(void)removeCompanionPlayView;
- (IBAction)buttonDisconnectPressed:(UIButton *)button;

@end
