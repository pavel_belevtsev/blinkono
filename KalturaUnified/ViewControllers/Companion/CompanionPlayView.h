//
//  CompanionPlayView.h
//  Vodafone
//
//  Created by Admin on 24.10.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CompanionPlayViewDelegate;

@interface CompanionPlayView : UIView

@property (weak, nonatomic) id<CompanionPlayViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *viewInner;
@property (weak, nonatomic) IBOutlet UIView *viewThumbnailContainer;
@property (weak, nonatomic) IBOutlet UIView *viewImageBorder;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewThumbnail;

@property (weak, nonatomic) IBOutlet UILabel *labelMediaTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelMediaSubtitle;

@property (weak, nonatomic) IBOutlet UIView *viewControlsContainer;
@property (weak, nonatomic) IBOutlet UIView *viewDelimiter;
@property (weak, nonatomic) IBOutlet UIButton *buttonStop;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlayPause;

+ (CompanionPlayView *)companionPlayView;
- (void)configureWithItem:(TVMediaItem *)item;
- (void)relayout;

@end


@protocol CompanionPlayViewDelegate <NSObject>

@optional
- (void)companionPlayView:(CompanionPlayView *)view playPauseButtonPressed:(id)sender;
- (void)companionPlayView:(CompanionPlayView *)view stopButtonPressed:(id)sender;

@end
