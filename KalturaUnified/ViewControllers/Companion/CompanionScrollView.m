//
//  CompanionScrollView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 02.01.15.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CompanionScrollView.h"

@implementation CompanionScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UIView *) hitTest:(CGPoint) point withEvent:(UIEvent *)event {
    if ([self pointInside:point withEvent:event]) {
        return [[self subviews] objectAtIndex:0];
    }
    return nil;
}


@end
