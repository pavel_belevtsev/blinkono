//
//  TVCompanionManager.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 02.01.15.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef PLAYER

#ifdef PLAYER_DX
#import <TVCPlayerStaticLibrary/TVMediaToPlayInfo.h>
#else
#import <TVCPlayerWV/TVMediaToPlayInfo.h>
#endif

#endif


extern NSString * const TVNotificationCompanionDevicesList;

#import <TvinciSDK/CompanionManager.h>

#ifdef PLAYER
@interface TVCompanionManager : NSObject <CompanionModeProtocol>
#else

@interface TVCompanionManager : NSObject

#endif

@property (nonatomic, strong) NSArray *devices;
@property (nonatomic, strong) NSMutableArray *m_devices;


+ (TVCompanionManager*)sharedInstance;

- (void)scan;
- (NSString *)getDeviceName:(id)device;
- (void)pairDevice:(id)device;
- (void)unPairCurrentDevice;

@property (nonatomic, retain) NSString *currentDeviceName;

#ifdef PLAYER
- (void)swoosh:(TVMediaToPlayInfo *)mediaToPlay epgId:(TVEPGProgram *)epgProgram time:(NSTimeInterval) time;
#endif

@property BOOL isPaired;
-(void)saveDevices :(NSArray *)devices;

@end
