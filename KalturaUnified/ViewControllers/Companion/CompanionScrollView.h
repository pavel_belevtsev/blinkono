//
//  CompanionScrollView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 02.01.15.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanionScrollView : UIView

@end
