//
//  CompanionModeOverlay.h
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CompanionModeOverlay;
@protocol CompanionModeOverlayDelegate <NSObject>
-(void) companionModeOverlay:( CompanionModeOverlay *) sender didSelectStopCompanion:(UIButton *) button;
@end


@interface CompanionModeOverlay : UIView

@property (weak, nonatomic) id<CompanionModeOverlayDelegate> delegate;


@property (assign, nonatomic) BOOL isPresnted;
@property (strong, nonatomic) IBOutlet UILabel * companionDeviceName;
@property (strong, nonatomic) IBOutlet UILabel * companionDescription;
@property (strong, nonatomic) IBOutlet UIButton * buttonStopCompanion;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewTV;
@property (nonatomic, strong) TVMediaItem * mediaItem;

+(CompanionModeOverlay *) companionModeOverlay;


-(void) animateInOnView:(UIView *) superView  withCompletion:(void (^)()) completion;
-(void) animateOutWithCompletion:(void (^)()) completion;

@end
