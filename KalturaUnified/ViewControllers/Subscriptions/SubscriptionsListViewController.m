//
//  SubscriptionsList.m
//  KalturaUnified
//
//  Created by Alex Zchut on 2/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SubscriptionsListViewController.h"

@interface SubscriptionsListViewController () {
    BOOL dataLoaded;
    
}
@property (strong, nonatomic) NSMutableDictionary *prototypeCells;

@end

@implementation SubscriptionsListViewController

#define kCellEstimatedHeight (isPad) ? 156.0f : 169.0f

- (void)viewDidLoad {
    
    self.loadNoInternetView = YES;
    
    [super viewDidLoad];
        
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [Utils setLabelFont:_labelTitle bold:YES];
    _labelTitle.text = LS(@"subscriptions");
    _viewNoRelated.hidden = TRUE;
    
    //came from item page
    if (self.arrSubscriptionsIDsContainingSelectedMedia) {
        [_btnBack setImage:nil forState:UIControlStateNormal];
        [_btnBack setBackgroundImage:[UIImage imageNamed:@"nav_bar_back_btn"] forState:UIControlStateNormal];
        [_btnBack setTitle:LS(@"back") forState:UIControlStateNormal];
        [Utils setButtonFont:_btnBack bold:YES];
        self.contraintBackBtnWidth.constant = 70;
        self.contraintBackBtnHeight.constant = 34;
    }
    else {
        [_btnBack setImage:[UIImage imageNamed:@"nav_bar_menu_icon"] forState:UIControlStateNormal];
        [_btnBack setBackgroundImage:nil forState:UIControlStateNormal];
        [_btnBack setTitle:@"" forState:UIControlStateNormal];
        self.contraintBackBtnWidth.constant = 44;
        self.contraintBackBtnHeight.constant = 44;
    }

    [self setNavigationBarBackground];
    
    [self requestSubscriptionsData];
}

- (void) setNavigationBarBackground {
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"nav_bar_bg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.viewNavigationBar setBackgroundColor:[UIColor colorWithPatternImage:image]];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    super.panModeDefault = MFSideMenuPanModeDefault;
    [self menuContainerViewController].panMode = (MFSideMenuPanMode)super.panModeDefault;
    [[self menuContainerViewController] setRightMenuWidth:270];
    
    if (_labelNoRelatedTitle) {
        _labelNoRelatedTitle.text = LS(@"player_related_no_items_title");
        [Utils setLabelFont:_labelNoRelatedTitle bold:YES];
    }
    
    if (_labelNoRelatedSubtitle) {
        _labelNoRelatedSubtitle.text = LS(@"player_related_no_items_subtitle");
        [Utils setLabelFont:_labelNoRelatedSubtitle bold:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self menuContainerViewController].rightMenuViewController = nil;
}

#pragma mark - subscriptions data

- (void) requestSubscriptionsData {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showHUDBlockingUI:YES];
    });
    
    [SubscriptionM getAllSubscriptionDataFromVirtualMedias:self appendIDs:self.arrSubscriptionsIDsContainingSelectedMedia needPurchasingInfo:YES completion:^(NSArray *arrSubscriptionsData) {
        _arrSubscriptionsDataForOthers = arrSubscriptionsData;
        if (self.arrSubscriptionsIDsContainingSelectedMedia) {
            _arrSubscriptionsDataForMedia = [arrSubscriptionsData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"subscriptionCode IN %@", self.arrSubscriptionsIDsContainingSelectedMedia]];
            _arrSubscriptionsDataForOthers = [arrSubscriptionsData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (SELF IN %@)",_arrSubscriptionsDataForMedia]];
        }
        
//#warning REMOVE THESE DEBUG LINES
//#ifdef DEBUG
//        if (self.itemsCount>2 && !_arrSubscriptionsDataForOthers.count)
//            _arrSubscriptionsDataForOthers = @[_arrSubscriptionsDataForMedia[0]];
//#endif
        
//#warning REMOVE THESE DEBUG LINES
//#ifdef DEBUG
//        if (_arrSubscriptionsDataForOthers.count > 0)
//            _arrSubscriptionsDataForOthers = @[_arrSubscriptionsDataForOthers[0]];
//        
//        for (NSInteger i=0;i<_arrSubscriptionsDataForOthers.count; i++) {
//            TVSubscriptionData *subscriptionData = _arrSubscriptionsDataForOthers[i];
//            if (i==0) {
//                //purchased check
//                if (!subscriptionData.purchasingInfo) {
//                    subscriptionData.purchasingInfo = [TVSubscriptionData_purchasingInfo new];
//                    subscriptionData.purchasingInfo.stringPurchaseDate = @"2015-02-15T10:47:00";
//                    subscriptionData.purchasingInfo.stringEndDate = @"2015-04-15T20:47:00";
//                }
//            }
//            subscriptionData.virtualMediaItem.pictureURL = [NSURL URLWithString:@"http://2.bp.blogspot.com/_ZWYQsM2TLBk/TQD8mxZDjDI/AAAAAAAAABY/LYFYoUCY4kA/s640/germany-landscape-605-2.jpg"];
//            subscriptionData.virtualMediaItem.mediaDescription = @"Proin a felis venenatis, aliquam nulla vel, semper augue. Donec rhoncus leo lectus, et semper ante elementum et. Cras in dui gravida, dignissim ipsum sed, elementum sem. Integer sodales arcu quis nibh eleifend hendrerit. Donec aliquam libero ex, sed vehicula quam interdum efficitur. Mauris fringilla nisl et ante pulvinar, nec blandit augue laoreet. Nulla elit urna, hendrerit id finibus et, accumsan sed leo. Duis sollicitudin neque in efficitur sollicitudin. Morbi tempus tempor placerat. In ac diam sed eros ornare gravida. Proin sed lobortis purus. Pellentesque mollis maximus augue. Nulla venenatis tristique porttitor. Ut non erat nec odio hendrerit mattis.";
//        }
//#endif
        
        dataLoaded = TRUE;
        [self.tblMainList reloadData];
        [self hideHUD];
    }];
}

#pragma mark - buttons

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    if (self.arrSubscriptionsIDsContainingSelectedMedia)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [[self menuContainerViewController] toggleLeftSideMenuCompletion:NULL];
}

#pragma mark - table view delegates
- (NSInteger) itemsCount {
    return self.arrSubscriptionsDataForMedia.count + self.arrSubscriptionsDataForOthers.count;
}

- (SubscriptionListCell *)tableView:(UITableView *)tableView prototypeForCellIdentifier:(NSString *)cellIdentifier
{
    SubscriptionListCell *cell;
    if (cellIdentifier) {
        cell = [self.prototypeCells objectForKey:cellIdentifier];
        if (!cell) {
            if (!self.prototypeCells) {
                self.prototypeCells = [[NSMutableDictionary alloc] init];
            }
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [self.prototypeCells setObject:cell forKey:cellIdentifier];
        }
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView cellIdentifierAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = NSStringFromClass([SubscriptionListCell class]);
    if ([self itemsCount] == 1)
        identifier = [identifier stringByAppendingString:@"Single"]; //on single subsciption load specific cell
    return [Utils nibName:identifier];
}

- (TVSubscriptionData*)cellDataObjectAtIndexPath:(NSIndexPath *)indexPath
{
    TVSubscriptionData *object = nil;
    switch (indexPath.section) {
        case 0: //item related subscriptions
            object = self.arrSubscriptionsDataForMedia[indexPath.row];
            break;
        case 1: //all subscriptions
            object = self.arrSubscriptionsDataForOthers[indexPath.row];
            break;
        default:
            break;
    }
    return object;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger retValue = 0;
    
    if (dataLoaded) {
        switch (section) {
            case 0: //item related subscriptions
                retValue = [self.arrSubscriptionsDataForMedia count];
                break;
            case 1: //all subscriptions
                retValue = [self.arrSubscriptionsDataForOthers count];
                break;
            default:
                break;
        }
    }
    return retValue;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return ([self itemsCount] == 1) ? tableView.height : kCellEstimatedHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!dataLoaded)
        return 0;
    
    //single item
    if ([self itemsCount] == 1)
        return tableView.height;
    
    CGFloat retValue = 0;
    TVSubscriptionData *item = [self cellDataObjectAtIndexPath:indexPath];
    NSString *cellId = [self tableView:tableView cellIdentifierAtIndexPath:indexPath];
    if (cellId) {
        SubscriptionListCell *cell = [self tableView:tableView prototypeForCellIdentifier:cellId];
        retValue = [cell heightWithData:item];
    }
    return retValue;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellId = [self tableView:tableView cellIdentifierAtIndexPath:indexPath];
    SubscriptionListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    cell.delegate = self;
    cell.contentView.backgroundColor = cell.backgroundColor = [UIColor clearColor];
    
    [cell setDetails:[self cellDataObjectAtIndexPath:indexPath] indexPath:indexPath];

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (!dataLoaded || [self itemsCount] == 1)
        return 0;
    
    CGFloat height = 0;
    switch (section) {
        case 1: //all subscriptions
            //if there are no media related subscriptions set the header of the related subscriptions to 0 or make it 60 if there are media related and not related subscriptions to show the diff between them
            height = (self.arrSubscriptionsDataForOthers.count && self.arrSubscriptionsDataForMedia.count) ? 60 : 0;
            break;
        default:
            height = 13; //height for header in media related subscriptions
            break;
    }
    return height;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (!dataLoaded || [self itemsCount] == 1)
        return nil;

    CGFloat height = [self tableView:tableView heightForHeaderInSection:section];
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.width, height)];
    viewHeader.backgroundColor = [UIColor clearColor];
    if (section == 1 && height && self.arrSubscriptionsDataForOthers.count) {
        
        UIView *viewDelimeterLine = [UIView new];
        viewDelimeterLine.translatesAutoresizingMaskIntoConstraints = NO;
        viewDelimeterLine.backgroundColor = kDelimiterLineColor;
        [viewHeader addSubview:viewDelimeterLine];
        [viewHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[viewDelimeterLine]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewDelimeterLine)]];
        
        UILabel *lblTitle = [UILabel new];
        lblTitle.numberOfLines = 0;
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.text = (isPad) ? LS(@"other_subscriptions") : LS(@"other_subscriptions_short");
        lblTitle.textColor = CS(@"959595");
        lblTitle.font = (isPad) ? Font_Reg(24) : Font_Reg(20);
        lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
        [viewHeader addSubview:lblTitle];
        CGSize sizeText = [lblTitle.text sizeWithAttributes:@{NSFontAttributeName:lblTitle.font}];
        NSDictionary *metrics = @{@"top": (isPad) ? @(height-(sizeText.height+18)) : @9, @"left": (isPad) ? @30: @20};
        [viewHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[lblTitle]-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(lblTitle)]];
        [viewHeader addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[viewDelimeterLine(1)]-(top)-[lblTitle]-(>=0)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(lblTitle, viewDelimeterLine)]];
    }
    return viewHeader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:FALSE];
}

#pragma mark - delegate for purchasing
- (void) purchaseSubscription:(TVSubscriptionData*) subscriptionData completion:(void (^)(SubscriptionResponseParams *resParams))completion {
    [self showHUDBlockingUI:YES];
    [IAP purchaseSubscription:^(SubscriptionPurchaseRequestParams *reqParams) {
        reqParams.subscriptionData = subscriptionData;
        reqParams.productCode = subscriptionData.productCode;
        reqParams.delegate = self;
    } completion:^(SubscriptionResponseParams *resParams) {
        [self hideHUD]; //remove activity indicator
        completion(resParams);
    }];
}

- (void) renewSubscriptionData:(NSString*) subscriptionCode completion:(void(^)(TVSubscriptionData *subscriptionData))completion {
    [SubscriptionM getSubscriptionsData:@[subscriptionCode] delegate:self needPurchasingInfo:TRUE completion:^(NSArray *arrSubscriptions) {
        if (arrSubscriptions && arrSubscriptions.count == 1)
            completion([arrSubscriptions firstObject]);
    }];
}
@end
