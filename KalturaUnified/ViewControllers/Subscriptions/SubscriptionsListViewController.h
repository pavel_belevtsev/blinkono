//
//  SubscriptionsList.h
//  KalturaUnified
//
//  Created by Alex Zchut on 2/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubscriptionListCell.h"

@interface SubscriptionsListViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, SubscriptionListCellActions>

@property (nonatomic, retain) NSArray *arrSubscriptionsIDsContainingSelectedMedia;
@property (nonatomic, retain) NSArray *arrSubscriptionsDataForOthers, *arrSubscriptionsDataForMedia;
@property (nonatomic, retain) IBOutlet UITableView *tblMainList;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *contraintBackBtnWidth, *contraintBackBtnHeight;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelNoRelatedTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelNoRelatedSubtitle;
@property (weak, nonatomic) IBOutlet UIView *viewNoRelated;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@end
