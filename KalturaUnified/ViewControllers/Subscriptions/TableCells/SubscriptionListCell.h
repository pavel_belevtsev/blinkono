//
//  SubscriptionListCell.h
//  KalturaUnified
//
//  Created by Alex Zchut on 2/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kDelimiterLineColor     CS(@"4E4E4E")

@protocol SubscriptionListCellActions <NSObject>

- (void) purchaseSubscription:(TVSubscriptionData*) subscriptionData completion:(void(^)(SubscriptionResponseParams *resParams))completion;
- (void) renewSubscriptionData:(NSString*) subscriptionCode completion:(void(^)(TVSubscriptionData *subscriptionData))completion;
- (NSInteger) itemsCount;

@end
@interface SubscriptionListCell : UITableViewCell

- (void) setDetails:(TVSubscriptionData*) subscriptionData indexPath:(NSIndexPath*)indexPath;
- (CGFloat)heightWithData:(TVSubscriptionData*)subscriptionData;
@property (nonatomic, retain) IBOutlet UILabel *lblName, *lblNameDelimeter, *lblAutoRenew, *lblSubscriptionPrice, *lblDescription;
@property (nonatomic, retain) IBOutlet UIButton *btnSubscribe;
@property (nonatomic, retain) IBOutlet UIImageView *imgItemPreview;
@property (nonatomic, retain) IBOutlet UIView *viewHD, *viewContentDelimeter, *viewSingleItemContent;
@property (nonatomic, weak) id<SubscriptionListCellActions> delegate;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *contraintAutoRenewHeight, *contraintSubscribeBtnHeight, *contraintDescriptionBottomHeight, *contraintDescriptionWidth;

@end
