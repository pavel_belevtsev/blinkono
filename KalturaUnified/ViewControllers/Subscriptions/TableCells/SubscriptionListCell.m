//
//  SubscriptionListCell.m
//  KalturaUnified
//
//  Created by Alex Zchut on 2/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "SubscriptionListCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+Additions.h"

@implementation SubscriptionListCell

#define kDateFormat             @"dd/MM/yy"
#define kTextColor              CS(@"959595")
#define kWhiteColor             CS(@"ffffff")
#define kButtonColor            CS(@"5ABAF4")
#define kButtonSelectedColor    (_viewSingleItemContent) ? CS(@"4F4F4F") : CS(@"474747")

- (CGFloat)heightWithData:(TVSubscriptionData*)subscriptionData {
    if (isPad)
        return 156;
    else {
        CGFloat descriptionY = _lblDescription.frame.origin.y + _contraintDescriptionBottomHeight.constant;
        _lblDescription.font = Font_Reg(14);
        _lblDescription.text = subscriptionData.virtualMediaItem.mediaDescription;
        [_lblDescription layoutIfNeeded];
        
        //add name height if more than 1 line
        CGFloat titleHeight = _lblName.height;
        _lblName.text =  [subscriptionData.objectVirtualName uppercaseString];
        [_lblName layoutIfNeeded];
        titleHeight = _lblName.height - titleHeight;
        titleHeight = (titleHeight > 0) ? : 0;
        
        return descriptionY + _lblDescription.size.height + titleHeight;
    }
}

- (void) setDetails:(TVSubscriptionData*) subscriptionData indexPath:(NSIndexPath*)indexPath {
    
    _viewSingleItemContent.backgroundColor = CS(@"484848");
    _contraintAutoRenewHeight.constant = 18;
    _contraintDescriptionWidth.constant = self.width;
    if (subscriptionData.isRecurring) {
        _lblAutoRenew.text = LS(@"auto_renewal");
    }
    else {
        [Utils getDurationTimeFromNow:[NSDate dateWithMinutesBeforeNow:subscriptionData.subscriptionUsageModule.maxUsageModuleLifeCycle] completion:^(NSInteger number, NSString *unit, NSString *altValue) {
            _lblAutoRenew.text = [NSString stringWithFormat:LS(@"item_page_buy_subscription_for_period"), @(number), [unit lowercaseString]];
        }];
    }
    _lblAutoRenew.font = Font_Reg(12);
    _lblAutoRenew.textColor = kTextColor;
    
    _viewHD.backgroundColor = kTextColor;
    _viewHD.hidden = YES; //hide the view
    
    _viewContentDelimeter.backgroundColor = kDelimiterLineColor;
    
    _lblNameDelimeter.textColor = kTextColor;
    
    _lblName.text = [subscriptionData.objectVirtualName uppercaseString];
    _lblName.textColor = kWhiteColor;
    
    _lblSubscriptionPrice.attributedText = [self createPriceAttributedText:subscriptionData];
        _lblNameDelimeter.text = @"|";
    _lblDescription.text = subscriptionData.virtualMediaItem.mediaDescription;
    _lblDescription.font = Font_Reg(14);
    _lblDescription.textColor = kTextColor;

    //removing title for selected state
    [_btnSubscribe setTitle:@"" forState:UIControlStateSelected];
    _btnSubscribe.titleLabel.numberOfLines = 0;
    _btnSubscribe.titleLabel.textAlignment = NSTextAlignmentCenter;
    _btnSubscribe.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    if ([subscriptionData.purchasingInfo.endDate timeIntervalSince1970] > [[NSDate date] timeIntervalSince1970]) {
        [self updateSubscribeButton:subscriptionData];
    }
    else {
        _btnSubscribe.layer.cornerRadius = 4.0;
        _btnSubscribe.layer.masksToBounds = YES;
        [_btnSubscribe setTitle:[LS(@"subscribe") capitalizedString] forState:UIControlStateNormal<<UIControlStateHighlighted<<UIControlStateDisabled];
        [_btnSubscribe setTitleColor:kWhiteColor forState:UIControlStateNormal<<UIControlStateHighlighted<<UIControlStateDisabled];
        [_btnSubscribe setUserInteractionEnabled:YES];
        
        __block SubscriptionListCell *sself = self;
        [_btnSubscribe setTouchUpInsideAction:^(UIButton *btn) {
//#warning REMOVE THESE DEBUG LINES
//            BOOL success = TRUE;
//            subscriptionData.purchasingInfo = [TVSubscriptionData_purchasingInfo new];
//            subscriptionData.purchasingInfo.stringPurchaseDate = @"2015-02-15T10:47:00";
//            subscriptionData.purchasingInfo.stringEndDate = @"2016-02-15T20:47:00";
            [sself.delegate purchaseSubscription:subscriptionData completion:^(SubscriptionResponseParams *resParams) {
                if (resParams.success) {
                    [sself.delegate renewSubscriptionData:subscriptionData.subscriptionCode completion:^(TVSubscriptionData *subscriptionNewData) {
                        [sself updateSubscribeButton:subscriptionNewData];
                    }];
                }
                else {
                    [sself updateSubscribeButton:subscriptionData];
                    NSString *errorDesc = (resParams.error) ? [NSString stringWithFormat:LS(@"item_page_buy_subscription_error_description_with_reason"), resParams.error.localizedDescription] : LS(@"item_page_buy_subscription_error_description");
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"item_page_buy_subscription_error_title")
                                                                        message:errorDesc
                                                                       delegate:nil
                                                              cancelButtonTitle:nil
                                                              otherButtonTitles:LS(@"ok"), nil];
                    
                    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
                    }];
                }
                [sself layoutIfNeeded];
            }];
        }];
    }
    
    _imgItemPreview.backgroundColor = [UIColor lightGrayColor];
    _imgItemPreview.layer.borderColor = kWhiteColor.CGColor;
    _imgItemPreview.layer.borderWidth = 1.0f;
    _imgItemPreview.clipsToBounds = YES;
    _imgItemPreview.contentMode = UIViewContentModeScaleAspectFill;
    
    //if no pictureURL, get the link from pictures array with same size of TVImage (obj in array)
    if (!subscriptionData.virtualMediaItem.pictureURL && subscriptionData.virtualMediaItem.pictures.count) {
        subscriptionData.virtualMediaItem.pictureURL = [Utils getBestPictureURLForMediaItem:subscriptionData.virtualMediaItem andSize:_imgItemPreview.size];
    }
    
    [_imgItemPreview sd_setImageWithURL:subscriptionData.virtualMediaItem.pictureURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image)
            _imgItemPreview.image = image;
        else
            _imgItemPreview.image = [UIImage imageNamed:@"default_pic_subscriptions"];
    }];
}

- (void) updateSubscribeButton:(TVSubscriptionData*) data  {
    
    if (data.purchasingInfo.purchaseDate) {
        [self.btnSubscribe setSelected:YES];
        [self.btnSubscribe setAttributedTitle:[self createSubscribeButtonAttributedText:data] forState:UIControlStateSelected];
        [self.btnSubscribe setBackgroundColor:kButtonSelectedColor];
        [self.btnSubscribe setUserInteractionEnabled:NO];
        self.lblAutoRenew.text = @"";
        self.contraintAutoRenewHeight.constant = 5;
        self.contraintSubscribeBtnHeight.constant = 60;
    }
    else {

        [self.btnSubscribe.titleLabel setFont:Font_Bold(16)];
        [self.btnSubscribe setSelected:NO];
        [self.btnSubscribe setBackgroundColor:kButtonColor];
        [self.btnSubscribe setUserInteractionEnabled:YES];
        self.contraintAutoRenewHeight.constant = 18;
        if (data.isRecurring) {
            self.lblAutoRenew.text = LS(@"auto_renewal");
        }
        else {
            [Utils getDurationTimeFromNow:[NSDate dateWithMinutesBeforeNow:data.subscriptionUsageModule.maxUsageModuleLifeCycle] completion:^(NSInteger number, NSString *unit, NSString *altValue) {
                self.lblAutoRenew.text = [NSString stringWithFormat:LS(@"item_page_buy_subscription_for_period"), @(number), [unit lowercaseString]];
            }];
        }
        self.contraintSubscribeBtnHeight.constant = (isPad) ? 40 : 36;
    }
}

- (NSAttributedString*) createSubscribeButtonAttributedText:(TVSubscriptionData*) data {
    NSString *descriptionText = nil;
    NSMutableAttributedString *attributedPrice = nil;
    NSDictionary* attributes = @{NSForegroundColorAttributeName : kWhiteColor, NSFontAttributeName : Font_Reg(14)};

    if (data.isRecurring) {
        NSString *baseString = (_viewSingleItemContent) ? LS(@"item_page_buy_subscribed_single_title") : LS(@"item_page_buy_subscribed_title");
        descriptionText = [NSString stringWithFormat:baseString, [data.purchasingInfo.purchaseDate stringFromDateWithDateFormat:kDateFormat], LS(@"auto_renewal")];
        
        attributedPrice = [[NSMutableAttributedString alloc] initWithString:descriptionText attributes:attributes];
        
        NSRange strRange = [descriptionText rangeOfString:LS(@"auto_renewal")];
        if (strRange.location != NSNotFound) {
            [attributedPrice setAttributes:@{NSForegroundColorAttributeName : kTextColor, NSFontAttributeName : Font_Reg(12)} range:strRange];
        }
    }
    else { //not recurring text
        NSString *timeLeftString = [SubscriptionM getNRSubscriptionLeftPeriod:data];
        NSString *baseString = (_viewSingleItemContent) ? LS(@"item_page_buy_subscribed_single_title_norenew") : LS(@"item_page_buy_subscribed_title_norenew");
        descriptionText = [NSString stringWithFormat:baseString, [data.purchasingInfo.purchaseDate stringFromDateWithDateFormat:kDateFormat], timeLeftString];

        attributedPrice = [[NSMutableAttributedString alloc] initWithString:descriptionText attributes:attributes];
        
        NSRange strRange = [descriptionText rangeOfString:timeLeftString];
        if (strRange.location != NSNotFound) {
            [attributedPrice setAttributes:@{NSForegroundColorAttributeName : kTextColor, NSFontAttributeName : Font_Reg(12)} range:strRange];
        }
    }
    return attributedPrice;
}

- (NSAttributedString*) createPriceAttributedText:(TVSubscriptionData *)data {
    BOOL bS = (_viewSingleItemContent) ?1:0;
    NSString *descriptionText = nil;
    NSMutableAttributedString *attributedPrice = nil;
    NSDictionary* attributes = @{NSForegroundColorAttributeName : APST.brandTextColor, NSFontAttributeName : Font_Bold(bS?30:20)};

    if (data.isRecurring) {
        descriptionText = [NSString stringWithFormat:@"%@/%@",
                                     data.priceCode.prise.name,
                                     [LS(@"dateTimeStr_month") lowercaseString]];
        
        attributedPrice = [[NSMutableAttributedString alloc] initWithString:descriptionText attributes:attributes];
        
        NSRange strRange = [descriptionText rangeOfString:[NSString stringWithFormat:@"/%@", [LS(@"dateTimeStr_month") lowercaseString]]];
        if (strRange.location != NSNotFound) {
            [attributedPrice setAttributes:@{NSForegroundColorAttributeName : APST.brandTextColor, NSFontAttributeName : Font_Reg(bS?20:14)} range:strRange];
        }
    }
    else {
        descriptionText = [NSString stringWithFormat:@"%@",
                           data.priceCode.prise.name];
        attributedPrice = [[NSMutableAttributedString alloc] initWithString:descriptionText attributes:attributes];
    }
    return attributedPrice;
}

@end
