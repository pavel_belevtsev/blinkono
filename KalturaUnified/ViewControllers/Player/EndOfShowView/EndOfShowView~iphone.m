//
//  EndOfShowView~iphone.m
//  KalturaUnified
//
//  Created by Alex Zchut on 3/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EndOfShowView~iphone.h"
#import "UIButton+VerticalLayout.h"
#import "UIImage+Tint.h"

@implementation EndOfShowView_iphone

#define kButtonHeight @70
#define kButtonWidth @93

- (void)showForMediaItem:(TVMediaItem*)mediaItem hasMediaTrailer:(BOOL)mediaTrailer hasNextEpisode:(BOOL)hasNextEpisode {
    //call super init
    [super showForMediaItem:mediaItem hasMediaTrailer:mediaTrailer hasNextEpisode:hasNextEpisode];
        
    if (mediaItem.isLive)
        [self createButtonsForLiveMedia];
    else
    {
        if([APP_SET isOffline]) {
            [self createsButtonsForVODMediaOnOffline:mediaItem];
        }
        else {
            [self createButtonsForVODMedia:mediaItem hasMediaTrailer:mediaTrailer hasNextEpisode:hasNextEpisode];
        }
    }
}

-(void) createsButtonsForVODMediaOnOffline:(TVMediaItem *) mediaItem
{
    UIButton *btnStartOver = nil;
    btnStartOver = [self createButtonOnTarget:self text:LS(@"player_dialog_end_of_show_start_over") image:[UIImage imageNamed:@"endofshow_startover_smartphone"] needSelectionColors:YES clickBlock:^(UIButton *btn) {
        [self.delegate buttonEndOfShowReplayPressed];
    }];



    UIButton *btnRemove = nil;
    btnRemove = [self createButtonOnTarget:self text:LS(@"player_dialog_end_of_show_remove") image:[UIImage imageNamed:@"endofshow_remove_smartphone"] needSelectionColors:YES clickBlock:^(UIButton *btn) {
        if ([self.delegate respondsToSelector:@selector(buttonEndOfShowRemoveDownloadedItem)])
        {
            [self.delegate buttonEndOfShowRemoveDownloadedItem];
        }
    }];

    UIView *middleEmptyView = [self createMiddleEmptyViewOnTatget:self];

    NSDictionary *metricsButtons = @{@"b1width": kButtonWidth,
                                     @"b2width": @15,
                                     @"b3width": kButtonWidth,
                                     @"b1height": kButtonHeight,
                                     @"b2height": kButtonHeight,
                                     @"b3height": kButtonHeight,
                                     @"space": @0};

    [self updateUIWithButtons:@[btnStartOver,middleEmptyView, btnRemove] metrics:metricsButtons onTargetView:self];
}

- (void) createButtonsForVODMedia:(TVMediaItem*)mediaItem hasMediaTrailer:(BOOL)mediaTrailer hasNextEpisode:(BOOL)hasNextEpisode {
    
    UIButton *btnLike = [self createButtonOnTarget:self text:LS(@"like") image:[UIImage imageNamed:@"endofshow_like_smartphone"] needSelectionColors:TRUE clickBlock:NULL];
    btnLike.tag = PlayerActionTypeLike;
    btnLike.UIUnitID = NU(UIUnit_Social_buttons);
    [btnLike activateAppropriateRule];
    
    UIButton *btnFavorite = [self createButtonOnTarget:self text:LS(@"favorite") image:[UIImage imageNamed:@"endofshow_favorite_smartphone"] needSelectionColors:TRUE clickBlock:NULL];
    btnFavorite.tag = PlayerActionTypeFavorite;
    
    btnFavorite.UIUnitID = NU(UIUnit_favorites_buttons);
    [btnFavorite activateAppropriateRule];

    NSDictionary *metrics = @{@"b1width": kButtonWidth,
                              @"b2width": kButtonWidth,
                              @"b3width": kButtonWidth,
                              @"b1height": kButtonHeight,
                              @"b2height": kButtonHeight,
                              @"b3height": kButtonHeight,
                              @"space": @0};
    
    UIButton *btnThirdButton = nil;
    if (mediaTrailer) {
        NSString *text = (mediaItem.isEpisode) ? LS(@"player_watch_full_episode") : LS(@"player_vod_watch_full_movie");
        btnThirdButton = [self createButtonOnTarget:self text:text image:[UIImage imageNamed:@"endofshow_fullmovie_smartphone"] needSelectionColors:TRUE clickBlock:^(UIButton *btn) {
            [self.delegate buttonEndOfShowWatchFullMoviePressed];
        }];
    }
    else if (mediaItem.isEpisode) {
        btnThirdButton = [self createButtonOnTarget:self text:LS(@"next_episode") image:[UIImage imageNamed:@"endofshow_next-episode_smartphone"] needSelectionColors:TRUE clickBlock:^(UIButton *btn) {
            [self endOfShowButton:btn shouldHighlighted:NO];
            [self.delegate buttonNextEpisodePressed];
        }];
        if (!hasNextEpisode)
            [btnThirdButton setEnabled:FALSE];
    }
    else {
        btnThirdButton = [self createButtonOnTarget:self text:LS(@"player_dialog_end_of_show_start_over") image:[UIImage imageNamed:@"endofshow_startover_smartphone"]  needSelectionColors:TRUE clickBlock:^(UIButton *btn) {
            [self.delegate buttonEndOfShowReplayPressed];
        }];
    }

    [self updateUIWithButtons:@[btnLike, btnFavorite, btnThirdButton] metrics:metrics];
    
    self.endOfShowLikeAdapter = [[LikeAdapter alloc] initWithMediaItem:nil control:btnLike label:(UILabel*)[btnLike viewWithTag:kButtonLabelTag]];
    self.endOfShowLikeAdapter.delegate = self;
    self.endOfShowLikeAdapter.hideButtonTitleForLikes = NO;
    self.endOfShowFavoriteAdapter = [[FavoriteAdapter alloc] initWithMediaItem:nil control:btnFavorite label:(UILabel*)[btnFavorite viewWithTag:kButtonLabelTag]];
    self.endOfShowFavoriteAdapter.delegate = self;

    [self.endOfShowLikeAdapter setupMediaItem:mediaItem];
    [self.endOfShowFavoriteAdapter setupMediaItem:mediaItem];
}

- (void) createButtonsForLiveMedia {
    
    UIButton *btnBackToLive= [self createButtonOnTarget:self text:LS(@"player_dialog_end_of_show_back_to_live") image:[UIImage imageNamed:@"endofshow_backtolive"] needSelectionColors:FALSE clickBlock:^(UIButton *btn) {
        [self.delegate buttonBackToLivePressed];
    }];
    UIButton *btnStartOver= [self createButtonOnTarget:self text:LS(@"player_dialog_end_of_show_start_over") image:[UIImage imageNamed:@"endofshow_startover"] needSelectionColors:FALSE clickBlock:^(UIButton *btn) {
        [self.delegate buttonStartOverPressed];
    }];
    /* "PSC : ONO : OPF-1383 Live - return from background display incorect screen */
    btnStartOver.hidden=YES;
    NSDictionary *metrics = @{@"b1width": @([kButtonWidth integerValue]+17),
                              @"b2width": @0,
                              @"b3width": @([kButtonWidth integerValue]+17),
                              @"b1height": kButtonHeight,
                              @"b2height": kButtonHeight,
                              @"b3height": kButtonHeight,
                              @"space": @0};
    UIView *middleEmptyView = [self createMiddleEmptyViewOnTatget:self];
    [self updateUIWithButtons:@[btnBackToLive, middleEmptyView, btnStartOver] metrics:metrics];
}

- (UIButton*) createButtonOnTarget:(UIView*)target text:(NSString*) title image:(UIImage*) image needSelectionColors:(BOOL)needSelectionColors clickBlock:(void(^)(UIButton *btn)) block {

    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    [target addSubview:button];
    
    
    [button setImage:image forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel sizeToFit];
    
    [button.titleLabel setText:title];
    [button.titleLabel setFont:Font_Bold(12)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button sizeToFit];
    
    [button centerVerticallyWithPadding:3];
    
    if (block)
        [button setTouchUpInsideAction:block];
    
    if (needSelectionColors)
        [self updateButtonSelectionState:title button:button label:button.titleLabel image:image];
    
    return button;
    

}

- (void) updateButtonSelectionState:(NSString *)title button:(UIButton *)button label:(UILabel *)label image:(UIImage *) image {
    [button addTarget:self action:@selector(endOfShowButtonTouchDown:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragInside];
    [button addTarget:self action:@selector(endOfShowButtonTouchCancel:) forControlEvents:UIControlEventTouchCancel | UIControlEventTouchDragOutside];
    [button setTitleColor:APST.brandColor forState:UIControlStateSelected];
    [button setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
    
}

@end
