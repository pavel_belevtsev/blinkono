//
//  EndOfShowView.m
//  KalturaUnified
//
//  Updated by Alex Zchut on 15/03/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EndOfShowView.h"
#import "UIImage+Tint.h"

@interface EndOfShowView () {
    BOOL _isLive;
}

@end

@implementation EndOfShowView

- (void)showForMediaItem:(TVMediaItem*)mediaItem hasMediaTrailer:(BOOL)mediaTrailer hasNextEpisode:(BOOL)hasNextEpisode {
    
    self.hidden = NO;
    _isLive = mediaItem.isLive;
    
    //remove all previous ui on view, dont remove backgrounds (like shadow on ipad)
    [self removeAllSubviewsExceptSubviewIsKindOfClass:[UIImageView class]];

    //implemented in child classes
}

- (void) updateUIWithButtons:(NSArray*) arrButtons metrics:(NSDictionary*) metrics {
    [self updateUIWithButtons:arrButtons metrics:metrics onTargetView:self];
}

- (void) updateUIWithButtons:(NSArray*) arrButtons metrics:(NSDictionary*) metrics onTargetView:(UIView*) target {
    
    switch(arrButtons.count) {
        case 3:
        {
            UIView *b1 = arrButtons[0];
            UIView *b2 = arrButtons[1];
            UIView *b3 = arrButtons[2];
            
            //set b2 to y center of self
            [target addConstraint: [NSLayoutConstraint constraintWithItem:b2 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:target attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
            //set b1 to y center of b2
            [target addConstraint: [NSLayoutConstraint constraintWithItem:b1 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:b2 attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
            //set b3 to y center of b2
            [target addConstraint: [NSLayoutConstraint constraintWithItem:b3 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:b2 attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
            //set b2 to x center of self
            [target addConstraint: [NSLayoutConstraint constraintWithItem:b2 attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:target attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
            
            [target addConstraint: [NSLayoutConstraint constraintWithItem:b1 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:[metrics[@"b1height"] floatValue]]];
            [target addConstraint: [NSLayoutConstraint constraintWithItem:b2 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:[metrics[@"b2height"] floatValue]]];
            [target addConstraint: [NSLayoutConstraint constraintWithItem:b3 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:[metrics[@"b3height"] floatValue]]];
            
            [target addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(>=0)-[b1(b1width)]-(space)-[b2(b2width)]-(space)-[b3(b3width)]-(>=0)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(b1, b2, b3)]];
        }
            break;
        default:
            break;
    }
}

- (UIView*) createMiddleEmptyViewOnTatget:(UIView*)target {
    UIView *middleEmptyView = [UIView new];
    middleEmptyView.translatesAutoresizingMaskIntoConstraints = NO;
    [target addSubview:middleEmptyView];
    
    return middleEmptyView;
}

- (void) hide {
    self.hidden = YES;
}

- (void) buttonEndOfShowPressed:(UIButton *)button {
    [self endOfShowButton:button shouldHighlighted:NO];
}

- (void) endOfShowButtonTouchDown:(UIButton *)button {
    [self endOfShowButton:button shouldHighlighted:YES];
}

- (void) endOfShowButtonTouchCancel:(UIButton *)button {
    [self endOfShowButton:button shouldHighlighted:NO];
}

#pragma mark - Logic UI

- (void) endOfShowButton:(UIButton *)button shouldHighlighted:(BOOL)highlighted {
    UILabel *lbl = (UILabel *)[button viewWithTag:kButtonLabelTag];
    lbl.highlighted = highlighted | button.selected;
    
    switch (button.tag) {
        case PlayerActionTypeLike:
            lbl.text = (highlighted | button.selected) ? LS(@"liked") : LS(@"like");
            break;
        case PlayerActionTypeFavorite:
            if (_isLive) {
                lbl.text = (highlighted | button.selected) ? LS(@"bookmarked") : LS(@"bookmark");
            } else {
                lbl.text = LS(@"favorite");
            }
            break;
        default:
            break;
    }
    UIImageView *imgView = (UIImageView*)[button viewWithTag:kButtonImageTag];
    UIImage *image = [button imageForState:UIControlStateDisabled];
    if (imgView && image) {
        if (highlighted) {
            [imgView setImage:[image imageTintedWithColor:APST.brandColor]];
        }
        else {
            [imgView setImage:image];
        }
    }
}

#pragma mark - Like adapter delegates
- (void) likeAdapter:(LikeAdapter*)likeAdapter comleteUpdateLikeStatus:(BOOL) status {
    UIButton *button = likeAdapter.buttonLike;
    UIImageView *imgView = (UIImageView*)[button viewWithTag:kButtonImageTag];
    UIImage *image = [button imageForState:UIControlStateDisabled];
    if (status) {
        [imgView setImage:[image imageTintedWithColor:APST.brandColor]];
    }
    else {
        [imgView setImage:image];
    }
}

#pragma mark - Favorite adapter delegates
- (void) favoriteAdapter:(FavoriteAdapter*)favoriteAdapter comleteUpdateFavoriteStatus:(BOOL) status {
    UIButton *button = favoriteAdapter.buttonFavorite;
    UIImageView *imgView = (UIImageView*)[button viewWithTag:kButtonImageTag];
    UIImage *image = [button imageForState:UIControlStateDisabled];
    if (status) {
        [imgView setImage:[image imageTintedWithColor:APST.brandColor]];
    }
    else {
        [imgView setImage:image];
    }
}
@end
