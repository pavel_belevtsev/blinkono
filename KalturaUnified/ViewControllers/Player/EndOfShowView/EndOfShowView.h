//
//  EndOfShowView.h
//  KalturaUnified
//
//  Updated by Alex Zchut on 15/03/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LikeAdapter.h"
#import "FavoriteAdapter.h"

#define kButtonLabelTag 882
#define kButtonImageTag 883

@protocol PlayerEndOfShowDelegate <NSObject>

- (void)buttonBackToLivePressed;
- (void)buttonStartOverPressed;
- (void)buttonNextEpisodePressed;
- (void)updateButtonBrandUI:(UIButton*)btn sameForAllStates:(BOOL) sameForAllStates;
- (void)buttonEndOfShowReplayPressed;
- (void)buttonEndOfShowWatchFullMoviePressed;
- (void)buttonEndOfShowRemoveDownloadedItem;

@end

@interface EndOfShowView : UIView <LikeAdapterDelegate, FavoriteAdapterDelegate>

@property (nonatomic, strong) LikeAdapter *endOfShowLikeAdapter;
@property (nonatomic, strong) FavoriteAdapter *endOfShowFavoriteAdapter;
@property (nonatomic, strong) id<PlayerEndOfShowDelegate> delegate;

- (void)showForMediaItem:(TVMediaItem*)mediaItem hasMediaTrailer:(BOOL)mediaTrailer hasNextEpisode:(BOOL)hasNextEpisode;
- (void)hide;

- (void) updateUIWithButtons:(NSArray*) arrButtons metrics:(NSDictionary*) metrics;
- (void) updateUIWithButtons:(NSArray*) arrButtons metrics:(NSDictionary*) metrics onTargetView:(UIView*) target;
- (UIView*) createMiddleEmptyViewOnTatget:(UIView*)target;
- (void) endOfShowButton:(UIButton *)button shouldHighlighted:(BOOL)highlighted;
- (void) endOfShowButtonTouchDown:(UIButton *)button;
- (void) endOfShowButtonTouchCancel:(UIButton *)button;

@end
