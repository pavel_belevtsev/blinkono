//
//  EndOfShowView~iphone.h
//  KalturaUnified
//
//  Created by Alex Zchut on 3/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EndOfShowView.h"

@interface EndOfShowView_iphone : EndOfShowView

- (UIButton*) createButtonOnTarget:(UIView*)target text:(NSString*) title image:(UIImage*) image needSelectionColors:(BOOL)needSelectionColors clickBlock:(void(^)(UIButton *btn)) block;

@end
