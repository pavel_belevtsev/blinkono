//
//  EndOfShowView~ipad.m
//  KalturaUnified
//
//  Created by Alex Zchut on 3/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EndOfShowView~ipad.h"
#import "UIButton+Additions.h"
#import <TvinciSDK/TVMediaItem.h>

@implementation EndOfShowView_ipad

#define kButtonHeight @49
#define kButtonWidth @238

- (void)showForMediaItem:(TVMediaItem*)mediaItem hasMediaTrailer:(BOOL)mediaTrailer hasNextEpisode:(BOOL)hasNextEpisode {
    [super showForMediaItem:mediaItem hasMediaTrailer:mediaTrailer hasNextEpisode:hasNextEpisode];

    self.alpha = 1.0;

    [self createButtons:mediaItem hasMediaTrailer:mediaTrailer hasNextEpisode:hasNextEpisode offline:[APP_SET isOffline]];
}

- (void) createButtons:(TVMediaItem*)mediaItem hasMediaTrailer:(BOOL)mediaTrailer hasNextEpisode:(BOOL)hasNextEpisode offline:(BOOL) isOffline
{

    //create title "DONT GO JUST YET"
    UILabel *labelTitle = [UILabel new];
    labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
    labelTitle.textAlignment = NSTextAlignmentCenter;
    labelTitle.textColor = APST.brandTextColor;
    [Utils setLabelFont:labelTitle size:40 bold:YES];

    [self addSubview:labelTitle];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[labelTitle]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(labelTitle)]];
    
    //create buttons line
    UIView *viewButtons = [UIView new];
    viewButtons.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:viewButtons];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[viewButtons]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewButtons)]];


    UILabel *labelFooter = nil;
    if (!isOffline)
    {
        labelFooter = [UILabel new];
        labelFooter.translatesAutoresizingMaskIntoConstraints = NO;
        labelFooter.textAlignment = NSTextAlignmentCenter;
        labelFooter.textColor = APST.brandTextColor;
        [Utils setLabelFont:labelFooter size:18 bold:YES];

        [self addSubview:labelFooter];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[labelFooter]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(labelFooter)]];
    }

    
    if (mediaItem.isLive)
    {
        [self createButtonsForLiveMedia:viewButtons];
        labelTitle.text = LS(@"whats_next");
    }
    else {
        [self createButtonsForVODMedia:mediaItem hasMediaTrailer:mediaTrailer hasNextEpisode:hasNextEpisode target:viewButtons offline:isOffline];
        
        labelTitle.text = LS(@"player_vod_endscreen_title");
        if (mediaTrailer)
            labelFooter.text = [NSString stringWithFormat:@"%@ %@ %@", LS(@"item_page_movie_available"), LS(@"item_page_no_charge"), LS(@"item_page_episode_available_continuation")];
        else
            labelFooter.text = LS(@"player_vod_endscreen_instruction");
    }

    [Utils setLabelFont:labelFooter];

    NSDictionary *metrics = @{@"buttonsHeight": @48, @"space": @0};

    if (!isOffline)
    {
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=0)-[labelTitle]-(20)-[viewButtons(buttonsHeight)]-20-[labelFooter]-30-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(labelTitle, viewButtons, labelFooter)]];
    }
    else
    {
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=0)-[labelTitle]-(20)-[viewButtons(buttonsHeight)]-30-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(labelTitle, viewButtons)]];

    }
}

- (void) createButtonsForVODMedia:(TVMediaItem*)mediaItem hasMediaTrailer:(BOOL)mediaTrailer hasNextEpisode:(BOOL)hasNextEpisode target:(UIView*)target offline:(BOOL) offline{

    if (offline)
    {
        UIButton *btnStartOver = nil;
        btnStartOver = [self createButtonOnTarget:target text:LS(@"player_dialog_end_of_show_start_over") image:[UIImage imageNamed:@"stb_startover"] clickBlock:^(UIButton *btn)
        {
            [self.delegate buttonEndOfShowReplayPressed];
        }];


        UIButton *btnRemove = nil;
        btnRemove = [self createButtonOnTarget:target text:LS(@"Remove") image:[UIImage imageNamed:@"endofshow_remove_tablet"] clickBlock:^(UIButton *btn)
                          {
                              if ([self.delegate respondsToSelector:@selector(buttonEndOfShowRemoveDownloadedItem)])
                              {
                                  [self.delegate buttonEndOfShowRemoveDownloadedItem];
                              }

                          }];

        UIView *middleEmptyView = [self createMiddleEmptyViewOnTatget:target];

        NSDictionary *metricsButtons = @{@"b1width": kButtonWidth,
                                         @"b2width": @15,
                                         @"b3width": kButtonWidth,
                                         @"b1height": kButtonHeight,
                                         @"b2height": kButtonHeight,
                                         @"b3height": kButtonHeight,
                                         @"space": @0};
        [self updateUIWithButtons:@[btnStartOver,middleEmptyView, btnRemove] metrics:metricsButtons onTargetView:target];
    }
    else
    {
        UIButton *btnLike = [self createButtonOnTarget:target text:LS(@"like") image:[UIImage imageNamed:@"endofshow_like_tablet"] clickBlock:NULL];
        btnLike.tag = PlayerActionTypeLike;
        btnLike.UIUnitID = NU(UIUnit_Social_buttons);
        [btnLike activateAppropriateRule];

        UIButton *btnFavorite = [self createButtonOnTarget:target text:LS(@"favorite") image:[UIImage imageNamed:@"endofshow_favorite_tablet"] clickBlock:NULL];
        btnFavorite.tag = PlayerActionTypeFavorite;
        btnFavorite.UIUnitID = NU(UIUnit_favorites_buttons);
        [btnFavorite activateAppropriateRule];


        UIButton *btnThirdButton = nil;
        if (mediaTrailer) {
            NSString *text = (mediaItem.isEpisode) ? LS(@"player_watch_full_episode") : LS(@"player_vod_watch_full_movie");
            btnThirdButton = [self createButtonOnTarget:target text:text image:[UIImage imageNamed:@"endofshow_fullmovie_tablet"] clickBlock:^(UIButton *btn) {
                [self.delegate buttonEndOfShowWatchFullMoviePressed];
            }];
        }
        else if (mediaItem.isEpisode) {
            btnThirdButton = [self createButtonOnTarget:target text:LS(@"next_episode") image:[UIImage imageNamed:@"endofshow_next-episode_tablet"] clickBlock:^(UIButton *btn) {
                [self endOfShowButton:btn shouldHighlighted:NO];
                [self.delegate buttonNextEpisodePressed];
            }];
            if (!hasNextEpisode)
                [btnThirdButton setEnabled:FALSE];
        }
        else {
            btnThirdButton = [self createButtonOnTarget:target text:LS(@"player_dialog_end_of_show_start_over") image:[UIImage imageNamed:@"endofshow_startover_tablet"] clickBlock:^(UIButton *btn) {
                [self.delegate buttonEndOfShowReplayPressed];
            }];
        }

        NSDictionary *metricsButtons = @{@"b1width": kButtonWidth,
                                         @"b2width": kButtonWidth,
                                         @"b3width": kButtonWidth,
                                         @"b1height": kButtonHeight,
                                         @"b2height": kButtonHeight,
                                         @"b3height": kButtonHeight,
                                         @"space": @15};
        [self updateUIWithButtons:@[btnLike, btnFavorite, btnThirdButton] metrics:metricsButtons onTargetView:target];

        self.endOfShowLikeAdapter = [[LikeAdapter alloc] initWithMediaItem:nil control:btnLike label:(UILabel*)[btnLike viewWithTag:kButtonLabelTag]];
        self.endOfShowLikeAdapter.delegate = self;
        self.endOfShowFavoriteAdapter = [[FavoriteAdapter alloc] initWithMediaItem:nil control:btnFavorite label:(UILabel*)[btnFavorite viewWithTag:kButtonLabelTag]];
        self.endOfShowFavoriteAdapter.delegate = self;
        
        [self.endOfShowLikeAdapter setupMediaItem:mediaItem];
        [self.endOfShowFavoriteAdapter setupMediaItem:mediaItem];
    }

}

- (void) createButtonsForLiveMedia:(UIView*)target {
    
    UIButton *btnBackToLive = [self createButtonOnTarget:target text:LS(@"player_dialog_end_of_show_back_to_live") image:[UIImage imageNamed:@"endofshow_backtolive"] sameUIForAllStates:YES clickBlock:^(UIButton *btn) {
        [self.delegate buttonBackToLivePressed];
    }];
    
    
    UIButton *btnStartOver= [self createButtonOnTarget:target text:LS(@"player_dialog_end_of_show_start_over") image:[UIImage imageNamed:@"endofshow_startover"] sameUIForAllStates:YES clickBlock:^(UIButton *btn) {
        [self.delegate buttonStartOverPressed];
    }];
    /* "PSC : ONO : OPF-1383 Live - return from background display incorect screen */
    btnStartOver.hidden=YES;
    UIView *middleEmptyView = [self createMiddleEmptyViewOnTatget:target];
    
    NSDictionary *metricsButtons = @{@"b1width": kButtonWidth,
                                     @"b2width": @15,
                                     @"b3width": kButtonWidth,
                                     @"b1height": kButtonHeight,
                                     @"b2height": kButtonHeight,
                                     @"b3height": kButtonHeight,
                                     @"space": @0};
    [self updateUIWithButtons:@[btnBackToLive, middleEmptyView, btnStartOver] metrics:metricsButtons onTargetView:target];
}

- (UIButton*) createButtonOnTarget:(UIView*)target text:(NSString*) title image:(UIImage*) image clickBlock:(void(^)(UIButton *btn)) block {
    return [self createButtonOnTarget:target text:title image:image sameUIForAllStates:FALSE clickBlock:block];
}

- (UIButton*) createButtonOnTarget:(UIView*)target text:(NSString*) title image:(UIImage*) image sameUIForAllStates:(BOOL) sameUIForAllStates clickBlock:(void(^)(UIButton *btn)) block {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    [target addSubview:button];
    
    [button setTitle:title forState:UIControlStateNormal];
    if (image) {
        [button setImage:image forState:UIControlStateNormal];
        [button centerTitleAndImageWithSpacing:10];
    }
    if (block)
        [button setTouchUpInsideAction:block];
    
    [self updateButtonBrandUI:button sameForAllStates:sameUIForAllStates];

    return button;
}

- (void)updateButtonBrandUI:(UIButton*)btn sameForAllStates:(BOOL) sameForAllStates {
    [self.delegate updateButtonBrandUI:btn sameForAllStates:sameForAllStates];
    
    //update text color to white - ove
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

}

@end
