//
//  ParentalAdapter.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 09.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "ParentalAdapter.h"

@implementation ParentalAdapter

+ (ParentalAdapter *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (id)init {
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

@end
