//
//  MediaGroupRulesAdapter.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 09.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"

@interface MediaGroupRulesAdapter : NSObject {
    NSMutableDictionary  *dictMediaFilesProductCodes;
}

+ (MediaGroupRulesAdapter *)instance;
@property (strong, nonatomic) NSMutableDictionary * dictMediaFilesPrices;

- (TVSingleItemPrice*) getMediaPrice:(NSArray*)files;
- (NSString*) getMediaProductCode:(NSArray*)files;

- (void) getItemsPricesWithCoupons:(TVMediaItem *)mediaItem delegate:(BaseViewController *)delegateController completion:(void(^)(NSDictionary *resultAllowedItems, NSDictionary *resultNotAllowedItems))completion;
- (void) getItemsPricesWithCouponsForDownloadedMedias:(NSArray *)mediaItems delegate:(BaseViewController *)delegateController completion:(void(^)(NSArray *updatedMediaItems))completion;
- (void) getGroupMediaRules:(TVMediaItem *)mediaItem delegate:(BaseViewController *)delegateController completion:(void(^)(NSDictionary *result))completion;
- (void) getAssetsStatsById:(NSString*)assetId assetType:(TVAssetType)assetType delegate:(BaseManagement *)delegate completion:(void(^)(NSInteger likes, NSInteger views, NSInteger votes))completion;
- (BOOL) isPriceReasonPurchased:(TVPriceType) priceReason;
-(NSString*)getFileIdToSingleItemPrice:(TVSingleItemPrice*) singleItemPrice;

@end
