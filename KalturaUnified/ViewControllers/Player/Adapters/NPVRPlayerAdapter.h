//
//  NPVRPlayerAdapter.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 2/23/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseNetworkObject.h"
#import "PlayerViewController.h"

@interface NPVRPlayerAdapter : BaseNetworkObject

@property (assign, nonatomic) BOOL alreadyUsed;
@property (strong, nonatomic) TVNPVRRecordItem * recordedAsset;
@property (strong, nonatomic) TVMediaItem * relatedMediaItem;


-(void) downloadLicencedLinkWithCompletion:(void (^)(NSURL *url , bool isStartOver)) completion failedBlock:(void (^)()) failed;
-(BOOL) isStartOver;







@end
