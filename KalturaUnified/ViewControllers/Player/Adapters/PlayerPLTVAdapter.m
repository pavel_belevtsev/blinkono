//
//  TrickPlayAdapter.m
//  KalturaUnified
//
//  Created by Rivka Schwartz on 3/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerPLTVAdapter.h"

@implementation PlayerPLTVAdapter

#ifdef PLAYER

+ (void)makeMediaReadyforPLTVFormat:(NSString *)format andMediaToPlay:(TVMediaToPlayInfo *)mediaToPlay  withProgram:(TVEPGProgram *)program withCompletionBlock:(void (^)(BOOL succeeded)) completion {
    
    TVFile * currentFile = [mediaToPlay.mediaItem fileForFormat:mediaToPlay.fileTypeFormatKey];
    NSDate * startTime   = program.startDateTime;
    
    EPGLicensedLinkFormatType formatType = EPGLicensedLinkFormatType_Catchup;
    
    __weak TVPAPIRequest * request = nil;
//    if ([[TVPConditionalAccessAPI class] respondsToSelector:@selector(requestForGetEPGLicensedDataWithFileID:EPGItemID:startTime:basicLink:userIP:refferer:countryCd2:languageCode3:formatType:delegate:)])
//    {
        request = [TVPConditionalAccessAPI
                   requestForGetEPGLicensedDataWithFileID:[currentFile.fileID integerValue]
                   EPGItemID:[program.EPGId integerValue]
                   startTime:startTime
                   basicLink:[currentFile.fileURL absoluteString]
                   userIP:nil
                   refferer:nil
                   countryCd2:nil
                   languageCode3:nil
                   formatType:formatType
                   delegate:nil];
        
        [request setCompletionBlock: ^{
            
            NSDictionary * response =[request JSONResponse];
            TVLicencedDataResponse * licencedData = [[TVLicencedDataResponse alloc] initWithDictionary:response];
            NSString* url = [licencedData.licencedLink.mainURL absoluteString];
            [mediaToPlay addPLTVFileWithFormat:format andUrlString:url andBaseFile:currentFile];
            mediaToPlay.fileTypeFormatKey = format;
            completion(YES);
        }];
//    }
//    else
//    {
//        request = [TVPMediaAPI requestForGetEPGLicensedLinkForFileID:[currentFile.fileID integerValue]
//                                                           EPGItemID:[program.EPGId integerValue]
//                                                           startTime:startTime
//                                                           basicLink:[currentFile.fileURL absoluteString]
//                                                              userIP:@""
//                                                            refferer:@""
//                                                          countryCd2:@""
//                                                       languageCode3:@""
//                                                          deviceName:@""
//                                                          formatType:formatType
//                                                            delegate:nil];
//        
//        [request setCompletionBlock: ^{
//            NSString* url      = [request JSONResponse];
//            [mediaToPlay addPLTVFileWithFormat:format andUrlString:url andBaseFile:currentFile];
//            mediaToPlay.fileTypeFormatKey = format;
//            completion(YES);
//        }];
//    }
    

    [request setFailedBlock:^{
        completion(NO);
    }];
    
    [request startAsynchronous];
    
}

#endif

@end
