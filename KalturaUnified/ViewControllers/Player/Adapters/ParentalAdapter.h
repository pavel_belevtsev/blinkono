//
//  ParentalAdapter.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 09.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParentalAdapter : NSObject

+ (ParentalAdapter *)instance;

@end
