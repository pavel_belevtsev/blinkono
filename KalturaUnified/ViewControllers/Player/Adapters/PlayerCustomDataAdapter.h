//
//  PlayerCustomDataAdapter.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 02.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"
#import <TvinciSDK/APSTVProgram.h>

@interface PlayerCustomDataAdapter : NSObject

+ (PlayerCustomDataAdapter *)instance;

- (void)updateSecuredSiteGUID:(BaseViewController *)delegateController;

- (void)requestForGetMediaMark:(TVMediaItem *)mediaItem delegate:(BaseViewController *)delegateController completion:(void(^)(NSDictionary *result))completion;

- (void)requestForGetMediaMarkNPVR:(TVNPVRRecordItem *)recordItem  delegate:(BaseViewController *)delegateController completion:(void(^)(NSDictionary *result))completion ;


- (void)checkLiveParental:(APSTVProgram *)program channel:(TVMediaItem *)channel delegate:(BaseViewController *)delegateController completion:(void(^)(NSArray *result))completion;

- (void)requestForGetDomainLastPosition:(TVMediaItem *)mediaItem delegate:(BaseViewController *)delegateController completion:(void(^)(NSDictionary *result))completion;

@property (nonatomic, strong) NSString *securedSiteGUID;

@end
