//
//  PlayerDTGAdapter.m
//  KalturaUnified
//
//  Created by Alex Zchut on 5/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerDTGAdapter.h"
#import "PlayerVODDetailsView.h"
#import "PlayerVODInfoView.h"
#import "UIImage+Tint.h"

@interface PlayerDTGAdapter () {
    BOOL downloadsPausedByPlayer;
}

@end

@implementation PlayerDTGAdapter
- (instancetype)init {
    self = [super init];
    if (self) {
        downloadsPausedByPlayer = FALSE;
    }
    return self;
}

- (BOOL) isInitialDownloadState {
    return (self.downloadItem.downloadState == KADownloadItemDownloadStateInitial || self.downloadItem.downloadState == KADownloadItemDownloadStateGetInfo);
}

- (BOOL) isItemDownloaded {
    return self.downloadItem.downloadState == KADownloadItemDownloadStateDownloaded;
}

- (void) pauseAllDownloads {
    if (!downloadsPausedByPlayer) {
        downloadsPausedByPlayer = TRUE;
        if (self.downloadItem && ![self isItemDownloaded]) {
            [self updateForDownloadItem];
        }
        [[DTGManager sharedInstance] interruptActiveDownloadTasks];
    }
}

- (void) resumeAllDownloads {
    if (downloadsPausedByPlayer) {
        downloadsPausedByPlayer = FALSE;
        if (self.downloadItem && ![self isItemDownloaded]) {
            [self updateForDownloadItem];
        }
        [[DTGManager sharedInstance] resumeInterruptedDownloadTasks];
    }
}

-(void) removeDownloadItemAllocation {
    [self.downloadItem removeDelegate: self];
}

- (void) getItemDetails {
    
    if (self.downloadItem && [self.delegateController getCurrentMediaState] == kVODButtonPlayngState && self.downloadItem.downloadState == KADownloadItemDownloadStateDownloaded) {
        [self.downloadItem addDelegate:self];
        return;
    }
    
    TVMediaItem *mediaItem = [self.delegateController getInitialMediaItem];
    TVFile *file = [mediaItem.files filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"format == %@", APST.TVCMediaFormatDownload]].firstObject;
    
//    file.fileURL = [NSURL URLWithString:@"http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8"];
//    if (mediaItem.files.count>0/*!file || (file && !file.fileURL)*/) {
//        TVFile *tempFile = [mediaItem.files filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"format == %@", [self.delegateController bestFileformat]]].firstObject;
//
//        TVFile *newFile = [tempFile copyWithZone:(__bridge NSZone *)(tempFile)];
//        newFile.format = APST.TVCMediaFormatDownload;
//        newFile.fileURL = tempFile.fileURL;
//        newFile.fileURL = [NSURL URLWithString:@"http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8"];
//        
//        NSMutableArray *arr = [NSMutableArray arrayWithArray:mediaItem.files];
//        TVFile *file = [arr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"format == %@", APST.TVCMediaFormatDownload]].firstObject;
//        [arr removeObject:file];
//        [arr addObject:newFile];
//        mediaItem.files = [NSArray arrayWithArray:arr];
//    }

    //initial(cleanup) update for item
    [self updateForDownloadItem: nil];
    
    if ([LoginM isSignedIn] && [self.delegateController getCurrentMediaState] != kVODButtonInitial && file)
    {
        [[DTGManager sharedInstance] getItem:mediaItem completion:^(KADownloadItem *downloadItem) {
            self.downloadItem = downloadItem;
            [self.downloadItem addDelegate:self];
            
            [self updateForDownloadItem:self.downloadItem];
        }];
    }
}

- (void) buttonDownloadPressed {
    
    if (!self.downloadItem)
        return;
    
    if (![LoginM isSignedIn] && APST.anonymousUserAllowedToPlayFreeItems) {
        [self.delegateController showLogin];
    }
    else {
        switch (self.downloadItem.downloadState) {
            case KADownloadItemDownloadStateInProgress:
            case KADownloadItemDownloadStateResumed:
            case KADownloadItemDownloadStateInDownloadQueue:
            case KADownloadItemDownloadStateCancelled: {
                [self.downloadItem pause];
            }
                break;
            default:
                [[DTGManager sharedInstance] isConnectionOkToDownload:^(BOOL result) {
                    if (result) {
                        [self buttonDownloadPressedStep2];
                    }
                    else {
                        [[ErrorView sharedInstance] showMessageOnTarget:LS(@"dtg_connection_stopped_no_network_title") image:nil target:[self.delegateController getContainerView]];
                    }
                }];
                break;
        }
    }
}

- (void) buttonDownloadPressedStep2 {
    switch (self.downloadItem.downloadState) {
        case KADownloadItemDownloadStateInitial: {
            [self disableDownloadButton];
            //if initial state - save item data and images
            TVMediaItem *mediaItem = [self.delegateController getMediaItem];
            
            //create pictures folder
            NSString *picturesFolder = [self.downloadItem.metadataFolderPath stringByAppendingPathComponent:@"pictures"];
            [KAFileSavingUtility createFolderAtPath:picturesFolder];
            
            //create new array for local images urls
            NSMutableArray *arrLocalPictures = [NSMutableArray array];
            
            for (TVImage *picture in mediaItem.pictures) {
                
                if (picture.imageUrl) {
                    //get picture name+extension
                    NSString *urlLastComponent = picture.imageUrl.pathComponents.lastObject;
                    
                    //download image
                    [[NSURLSession.sharedSession downloadTaskWithURL:picture.imageUrl completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error){
                        if (!error) {
                            [NSFileManager.defaultManager moveItemAtURL:location toURL:[NSURL fileURLWithPath:[picturesFolder stringByAppendingPathComponent:urlLastComponent]] error:nil];
                        }
                    }] resume];
                    
                    //add image local url to new mediaItem pics array instead of remote url to be able to load image while offline
                    NSURLComponents *uc = self.downloadItem.contentFolderUrlComponents;
                    uc.path = [[uc.path stringByAppendingPathComponent:@"pictures"] stringByAppendingPathComponent:urlLastComponent];
                    
                    picture.imageUrl = uc.URL;
                    [arrLocalPictures addObject:picture];
                }
            }
            //set pictures local urls
            mediaItem.pictures = arrLocalPictures;
            
            //save media item data to local file
            [mediaItem.data writeToFile:[self.downloadItem.metadataFolderPath stringByAppendingPathComponent: [NSStringFromClass([mediaItem class]) stringByAppendingString:@".bin"]] atomically:YES];
            
            //get item info and start download
            if (self.downloadItem.url.length > 0) {
                [self.downloadItem loadInfo:^(KADownloadItem *updatedDownloadItem, BOOL success) {
                    if (success) {
                        self.downloadItem = updatedDownloadItem;
                        [self.downloadItem selectVideoStreams:@[@(0)] completion:^(BOOL success) {
                            if (success) {
                                [self.downloadItem selectAudioStreams:@[@(0)] completion:^(BOOL success) {
                                    if (success) {
                                        [self.downloadItem startDownload];
                                    }
                                }];
                            }
                        }];
                        [self enableDownloadButton];
                    }
                    else {
                        [self enableDownloadButton];
                    }
                }];
            }
            else {
                [self enableDownloadButton];
            }
        }
            break;
        case KADownloadItemDownloadStateGetInfo:
        case KADownloadItemDownloadStateFailed: {
            [self.downloadItem selectVideoStreams:@[@(0)] completion:^(BOOL success) {
                if (success) {
                    [self.downloadItem selectAudioStreams:@[@(0)] completion:^(BOOL success) {
                        if (success) {
                            [self.downloadItem startDownload];
                        }
                    }];
                }
            }];
        }
            break;
        case KADownloadItemDownloadStatePaused:
        case KADownloadItemDownloadStateInterrupted: {
            [self.downloadItem resume];
        }
            break;
        default:
            break;
    }
}

-(void) disableDownloadButton {
    [self.delegateController prepareToUpdateDownloadStateForListeners:^(YLProgressBar *downloadProgress, UIButton *downloadButton, UIImageView *downloadStatusImage, UIView *target) {
        downloadButton.userInteractionEnabled = FALSE;
    }];
}

-(void) enableDownloadButton {
    [self.delegateController prepareToUpdateDownloadStateForListeners:^(YLProgressBar *downloadProgress, UIButton *downloadButton, UIImageView *downloadStatusImage, UIView *target) {
        downloadButton.userInteractionEnabled = TRUE;
    }];
}


#pragma mark - KADownloadItemDelegates
-(void)downloadFinished:(KADownloadItem * __nonnull)item {
    [self updateForDownloadItem:item];
}

-(void)downloadFailed:(KADownloadItem * __nonnull)item error:(NSError * __nullable)error {
    [[ErrorView sharedInstance] showMessageOnTarget:[NSString stringWithFormat:LS(@"dtg_download_failed_title"), [self.delegateController getMediaItem].name] image:nil target:[self.delegateController getContainerView]];
    [self updateForDownloadItem:item];
}

-(void)downloadInterrupted:(KADownloadItem * __nonnull)item {
    switch (item.lastInterruptionReason) {
        case KADownloadItemInterruptionReasonDownloadQuotaLimit: {
            NSString *message = LS(@"dtg_out_of_quota_title");
            [[ErrorView sharedInstance] showMessageOnTarget:message image:nil target:[self.delegateController getContainerView]];
        }
            break;
        case KADownloadItemInterruptionReasonDeviceOutOfFreeSpace: {
            NSString *message = LS(@"dtg_out_of_disk_space_title");
            [[ErrorView sharedInstance] showMessageOnTarget:message image:nil target:[self.delegateController getContainerView]];
        }
            break;
        case KADownloadItemInterruptionReasonConnectivity: {
//            NSString *message = LS(@"dtg_connection_stopped_no_network_title");
//            [[ErrorView sharedInstance] showMessageOnTarget:message image:nil target:[self.delegateController getContainerView]];
        }
            break;
        default:
            break;
    }
    [self updateForDownloadItem:item];
}

-(void)downloadPaused:(KADownloadItem * __nonnull)item {
    [self updateForDownloadItem:item];
}

-(void)downloadResumed:(KADownloadItem * __nonnull)item {
    [self updateForDownloadItem:item];
}

-(void)downloadStarted:(KADownloadItem * __nonnull)item {
    [self updateForDownloadItem:item];
}

-(void)downloadWillStart:(KADownloadItem * __nonnull)item {
    [self updateForDownloadItem:item];
}

-(void)downloadInProgress:(KADownloadItem * __nonnull)item {
    [self updateForDownloadItem:item];
}

#pragma mark - make update
- (void) updateForDownloadItem {
    [self updateForDownloadItem: self.downloadItem];
}
- (void) updateForDownloadItem:(KADownloadItem*)downloadItem {
    
    [self.delegateController prepareToUpdateDownloadStateForListeners:^(YLProgressBar *downloadProgress, UIButton *downloadButton, UIImageView *downloadStatusImage, UIView *target) {
        [self updateForDownloadItem:downloadItem
                           progress:downloadProgress
                     downloadButton:downloadButton
                        statusImage:downloadStatusImage
                             target:target];
    }];
}

- (void) updateForDownloadItem:(KADownloadItem*) downloadItem
                  progress:(YLProgressBar*) downloadProgress
            downloadButton:(UIButton*) downloadButton
               statusImage:(UIImageView*) downloadStatusImage
                    target:(UIView*) target {
    
    if (!downloadItem && !(![LoginM isSignedIn] && APST.anonymousUserAllowedToPlayFreeItems)) { //button initial update without purchasing info
        downloadButton.enabled = FALSE;
        downloadButton.userInteractionEnabled = FALSE;
    }
    else {
        downloadButton.enabled = TRUE;
        downloadButton.userInteractionEnabled = TRUE;
    }
    
    switch (downloadItem.downloadState) {
        default:
        case KADownloadItemDownloadStateInitial:
        case KADownloadItemDownloadStateGetInfo:
            if (![LoginM isSignedIn] || [self.delegateController getCurrentMediaState] == kVODButtonPurchaseState || [self.delegateController getCurrentMediaState] == kVODButtonAddToWatchListState || [self.delegateController getCurrentMediaState] == kVODButtonAddedToWatchListState ||  [self.delegateController getCurrentMediaState] == kVODButtonInitial ) {
                if ([LoginM isSignedIn] && [target isKindOfClass:[PlayerVODInfoView class]]) {
                    [(PlayerVODInfoView*)target setImageThumbDownloadIndicator:downloadItem.downloadState];
                }
                downloadButton.backgroundColor = CS(@"575757");
                downloadStatusImage.image = [[UIImage imageNamed:@"download_icon"] imageTintedWithColor:(isPad) ? CS(@"737373") : CS(@"626161")];
            }
            else {
                downloadButton.backgroundColor = (isPad) ? CS(@"575757") : CS(@"4f4f4f");
                downloadStatusImage.image = [[UIImage imageNamed:@"download_icon"] imageTintedWithColor:APST.brandTextColor];
            }
            downloadProgress.hidden = TRUE;
            break;
        case KADownloadItemDownloadStatePaused:
        case KADownloadItemDownloadStateInterrupted:
        case KADownloadItemDownloadStateFailed:
            downloadProgress.hidden = FALSE;
            downloadProgress.progressTintColors = @[CS(@"747474")];
            downloadButton.backgroundColor = (isPad) ? CS(@"575757") : CS(@"4f4f4f");
            downloadStatusImage.image = [UIImage imageNamed:@"download_icon"];
            [downloadProgress setProgress:downloadItem.currentProgress animated:NO];
            break;
        case KADownloadItemDownloadStateResumed:
        case KADownloadItemDownloadStateInDownloadQueue:
            downloadProgress.hidden = FALSE;
            downloadProgress.progressTintColors = @[APST.brandColor];
            downloadStatusImage.image = [UIImage imageNamed:@"pause_icon"];
            break;
        case KADownloadItemDownloadStateWillStart:
            downloadProgress.hidden = FALSE;
            downloadProgress.progressTintColors = @[CS(@"747474")];
            downloadButton.backgroundColor = (isPad) ? CS(@"575757") : CS(@"4f4f4f");
            downloadStatusImage.image = [UIImage imageNamed:@"pause_icon"];
            [downloadProgress setProgress:1 animated:NO];

            break;
        case KADownloadItemDownloadStateStarted:
            downloadProgress.hidden = FALSE;
            downloadProgress.progressTintColors = @[APST.brandColor];
            [downloadProgress setProgress:downloadItem.currentProgress animated:NO];
            break;
        case KADownloadItemDownloadStateInProgress:
            downloadProgress.hidden = FALSE;
            downloadProgress.progressTintColors = @[APST.brandColor];
            downloadStatusImage.image = [UIImage imageNamed:@"pause_icon"];
            downloadButton.backgroundColor = (isPad) ? CS(@"575757") : CS(@"4f4f4f");
            [downloadProgress setProgress:downloadItem.currentProgress animated:YES];
            break;
        case KADownloadItemDownloadStateDownloaded:
            if ([self.delegateController getCurrentMediaState] != kVODButtonShouldPlayState) {
                if ([target isKindOfClass:[PlayerVODInfoView class]]) {
                    [(PlayerVODInfoView*)target setImageThumbDownloadIndicator:downloadItem.downloadState];
                }
            }
            else {
                downloadProgress.hidden = TRUE;
                downloadStatusImage.image = [[UIImage imageNamed:@"downloaded_icon"] imageTintedWithColor:APST.brandColor];
                downloadButton.backgroundColor = (isPad) ? [CS(@"ffffff") colorWithAlphaComponent:0.15] : CS(@"4f4f4f");
            }
            //disable user interaction of button on download complete
            downloadButton.userInteractionEnabled = FALSE;
            break;
    }

    downloadButton.userInteractionEnabled = downloadsPausedByPlayer?NO:downloadButton.userInteractionEnabled;
    downloadButton.alpha = downloadsPausedByPlayer?0.5:1;


}

- (void) updateParentalAccessCode:(NSString*) parentalCode {
    [[DTGManager sharedInstance] updateParentalCode:parentalCode completion:^(BOOL success) {
        [self.downloadItem updateItemWatchRestriction:TRUE];
    }];
}

- (void) updateDownloadedItemLicense {
    if ([self isItemDownloaded]) {
        [self.downloadItem updateLicense];
    }
}

- (void) updateMediaMarkForLocationChange:(CGFloat) locationInSec {
    self.downloadItem.mediaMark.locationSec = locationInSec;
    if (self.downloadItem.DBID > 0)
        [self.downloadItem updateMediaMark];
}


- (NSDictionary*) syncMediaMark:(NSDictionary*) dict {
    //    {
    //        eStatus = 0;
    //        nGroupID = 0;
    //        nLocationSec = 30;
    //        nMediaID = 256201;
    //        sDeviceID = 855B856B9E594DA9BFBF346BB70FBD50;
    //        sDeviceName = "";
    //        sSiteGUID = 663227;
    //    }
    
    
    
    if (dict) {

        NSDictionary *dictMediaMark = @{@"status": dict[@"eStatus"]?dict[@"eStatus"]:@"",
                                        @"groupId":dict[@"nGroupID"]?dict[@"nGroupID"]:@"",
                                        @"locationSec":dict[@"nLocationSec"]?dict[@"nLocationSec"]:@"",
                                        @"mediaId":[NSString stringWithFormat:@"%d", [dict[@"nMediaID"] intValue]],
                                        @"deviceId":dict[@"sDeviceID"]?dict[@"sDeviceID"]:@"",
                                        @"deviceName":dict[@"sDeviceName"]?dict[@"sDeviceName"]:@"",
                                        @"siteGUID":dict[@"sSiteGUID"]?dict[@"sSiteGUID"]:@"",
                                        @"updateDate":@([[NSDate dateWithTimeIntervalSince1970:0] timeIntervalSince1970])};
        
        self.downloadItem.tempMediaMark =[[KADownloadItem_mediaMark alloc] initWithMediaMarkInfo:dictMediaMark];
        
        if (self.downloadItem.DBID > 0) {
            self.downloadItem.tempMediaMark.locationSec = MAX(self.downloadItem.mediaMark.locationSec, self.downloadItem.tempMediaMark.locationSec);
            self.downloadItem.mediaMark = self.downloadItem.tempMediaMark;
            [self.downloadItem updateMediaMark];
        }
        
        if (self.downloadItem.mediaMark) {
            NSMutableDictionary *updatedDict = [NSMutableDictionary dictionaryWithDictionary:dict];
            updatedDict[@"nLocationSec"] = @(self.downloadItem.mediaMark.locationSec);
            return updatedDict;
        }
        else {
            return dict;
        }
    }
    else {
        return dict;
    }
}

@end

