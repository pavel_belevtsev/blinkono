//
//  PlayerCustomDataAdapter.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 02.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerCustomDataAdapter.h"

@implementation PlayerCustomDataAdapter

+ (PlayerCustomDataAdapter *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (id)init {
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)updateSecuredSiteGUID:(BaseViewController *)delegateController {
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForGetSecuredSiteGUIDWithDelegate:nil];
    
    [request setCompletionBlock:^{
        
        self.securedSiteGUID = [request JSONResponse];
        
    }];
    
    [request setFailedBlock:^{
        
    }];
    
    [delegateController sendRequest:request];
    
}


- (void)requestForGetMediaMark:(TVMediaItem *)mediaItem  delegate:(BaseViewController *)delegateController completion:(void(^)(NSDictionary *result))completion {
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetMediaMarkForMediaID:[mediaItem.mediaID integerValue] npvrID:nil  delegate:nil];
    
    [request setCompletionBlock:^{
        
        completion([[request JSONResponse] dictionaryByRemovingNSNulls]);
        
    }];
    
    [request setFailedBlock:^{
        
        completion([NSDictionary dictionary]);
        
    }];
    
    [delegateController sendRequest:request];
    
}

- (void)requestForGetMediaMarkNPVR:(TVNPVRRecordItem *)recordItem  delegate:(BaseViewController *)delegateController completion:(void(^)(NSDictionary *result))completion {
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetMediaMarkForMediaID:0 npvrID:recordItem.recordingID  delegate:nil];
    
    [request setCompletionBlock:^{
        
        completion([[request JSONResponse] dictionaryByRemovingNSNulls]);
        
    }];
    
    [request setFailedBlock:^{
        
        completion([NSDictionary dictionary]);
        
    }];
    
    [delegateController sendRequest:request];
    
}


- (void)checkLiveParental:(APSTVProgram *)program channel:(TVMediaItem *)channel delegate:(BaseViewController *)delegateController completion:(void(^)(NSArray *result))completion {
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForGetEPGProgramRulesForMediaID:[channel.mediaID integerValue] andProgramID:[program.epgId integerValue] andIP:@"" delegate:nil];
    
    
    [request setCompletionBlock:^{
        
        completion([[request JSONResponse] arrayByRemovingNSNulls]);
        
    }];
    
    [request setFailedBlock:^{
        
        completion([NSArray array]);
        
    }];
    
    [delegateController sendRequest:request];


}


- (void)requestForGetDomainLastPosition:(TVMediaItem *)mediaItem delegate:(BaseViewController *)delegateController completion:(void(^)(NSDictionary *result))completion
{
 
 __weak TVPAPIRequest * request = [TVPDomainAPI requestForGetDomainLastPosition:mediaItem.mediaID  delegate:nil];
 
    [request setCompletionBlock:^{

        completion([[request JSONResponse] dictionaryByRemovingNSNulls]);

    }];

    [request setFailedBlock:^{

        completion([NSDictionary dictionary]);

    }];

    [delegateController sendRequest:request];
 
 }
 
 
@end
