//
//  TrickPlayAdapter.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 3/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseNetworkObject.h"

@interface PlayerPLTVAdapter : BaseNetworkObject

#ifdef PLAYER

+ (void)makeMediaReadyforPLTVFormat:(NSString *)format andMediaToPlay:(TVMediaToPlayInfo *)mediaToPlay  withProgram:(TVEPGProgram *)program withCompletionBlock:(void (^)(BOOL succeeded)) completion;

#endif

@end
