//
//  NPVRPlayerAdapter.m
//  KalturaUnified
//
//  Created by Rivka Schwartz on 2/23/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "NPVRPlayerAdapter.h"

@implementation NPVRPlayerAdapter

-(void) downloadLicencedLinkWithCompletion:(void (^)(NSURL * URL , bool isStartOver)) completion failedBlock:(void (^)()) failed
{
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetNPVRLicensedLinkWithRecordingId:self.recordedAsset.recordingID startTime:nil fileID:-1 basicLink:nil refferer:nil couponCode:nil withdelegate:nil];
    
    [request setFailedBlock:^{
        
        failed();
    }];
    
    [request setCompletionBlock:^{
        
        BOOL isStartOver = [self isStartOver];
        TVNPVRLicenedLink * licenedLink = [[TVNPVRLicenedLink alloc] initWithDictionary:[request JSONResponse]];
        completion(licenedLink.link,isStartOver);
    }];
    
    [self sendRequest:request];
}

-(TVMediaItem *) channelIdForRecordAsset:(TVNPVRRecordItem *) recordedItem
{
    NSUInteger index = [EPGM.channelsList indexOfObjectPassingTest:^BOOL(TVMediaItem * obj, NSUInteger idx, BOOL *stop)
    {
        if ([obj.epgChannelID isEqualToString:[recordedItem epgChannelID]])
        {
            return YES;
        };
        
        return NO;
        
    }];
    
    
    if (index != NSNotFound)
    {
        
        return EPGM.channelsList[index];
    }
    else
    {
        return nil;
    }

}


-(TVMediaItem *)relatedMediaItem
{
    if (_relatedMediaItem == nil)
    {
      _relatedMediaItem = [self channelIdForRecordAsset:self.recordedAsset];
    }
    
    return _relatedMediaItem;
}


-(NSURL *) temporaryFixForURL:(NSURL *) originalURL
{
    NSString *  changedURL = [originalURL absoluteString];
    changedURL = [changedURL stringByReplacingOccurrencesOfString:@"/10.m3u8" withString:@"/6.m3u8"];
    changedURL = [changedURL stringByReplacingOccurrencesOfString:@"device=HLS" withString:@"device=Mobile_Devices_Main_SD"];
    return [NSURL URLWithString:changedURL];
    
    
    
}


-(BOOL) isStartOver
{
    bool isStartOver = YES;
    if ([self.recordedAsset.endDate compare:[NSDate date]] == NSOrderedAscending)
    {
        isStartOver = NO;
    }
    
    return isStartOver;
}
@end
