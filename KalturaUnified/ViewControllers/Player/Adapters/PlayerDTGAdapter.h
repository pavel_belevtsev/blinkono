//
//  PlayerDTGAdapter.h
//  KalturaUnified
//
//  Created by Alex Zchut on 5/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PlayerDTGAdapterDelegate <NSObject>

- (TVMediaItem*) getMediaItem;
- (TVMediaItem*) getInitialMediaItem;
- (VODButtonState) getCurrentMediaState;
- (void) prepareToUpdateDownloadStateForListeners:(void(^)(YLProgressBar* downloadProgress, UIButton* downloadButton, UIImageView* downloadStatusImage, UIView* target))completion;
- (void) showLogin;
- (UIView*) getContainerView;

@optional
-(NSString *) bestFileformat;

@end

@interface PlayerDTGAdapter : NSObject <KADownloadItemDelegate>

@property (nonatomic, weak) KADownloadItem *downloadItem;
@property (nonatomic, weak) id<PlayerDTGAdapterDelegate> delegateController;

- (void) removeDownloadItemAllocation;
- (void) buttonDownloadPressed;
- (void) getItemDetails;
- (BOOL) isInitialDownloadState;
- (BOOL) isItemDownloaded;
- (void) pauseAllDownloads;
- (void) resumeAllDownloads;

- (void) updateParentalAccessCode:(NSString*) parentalCode;
- (NSDictionary*) syncMediaMark:(NSDictionary*) dict;
- (void) updateMediaMarkForLocationChange:(CGFloat) locationInSec;
- (void) updateDownloadedItemLicense;

@end
