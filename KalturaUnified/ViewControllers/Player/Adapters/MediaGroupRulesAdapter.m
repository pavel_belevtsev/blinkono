//
//  MediaGroupRulesAdapter.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 09.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MediaGroupRulesAdapter.h"
#import "PlayerViewController.h"
#import <TvinciSDK/TVRuleItem.h>

@implementation MediaGroupRulesAdapter

+ (MediaGroupRulesAdapter *)instance {
    static dispatch_once_t p = 0;
    __strong static id sharedObject = nil;
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    return sharedObject;
}

- (id)init {
    self = [super init];
    if (self)
    {
        self.dictMediaFilesPrices = [NSMutableDictionary dictionary];
        dictMediaFilesProductCodes = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void) getAssetsStatsById:(NSString*)assetId assetType:(TVAssetType)assetType delegate:(BaseManagement *)delegate completion:(void(^)(NSInteger likes, NSInteger views, NSInteger votes))completion {
    __weak TVPAPIRequest *request = [TVPMediaAPI requestforGetAssetsStatsWithAssetsIDs:@[assetId] assetType:assetType pageSize:1 pageIndex:0 delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSArray *stats = [[request JSONResponse] arrayByRemovingNSNulls];
        NSDictionary *data = stats.firstObject;
        if (data) {
            completion([data[@"m_nLikes"] integerValue] ,[data[@"m_nViews"] integerValue] ,[data[@"m_nVotes"] integerValue]);
        }
        else {
            completion(0,0,0);
        }
    }];
    
    [request setFailedBlock:^{
        completion(0,0,0);
    }];
    
    [delegate sendRequest:request];
}

- (void) getItemsPricesWithCouponsForDownloadedMedias:(NSArray *)mediaItems delegate:(BaseViewController *)delegateController completion:(void(^)(NSArray *updatedMediaItems))completion {
 
    NSMutableArray *filesIds = [NSMutableArray array];
    for (TVMediaItem *mi in mediaItems) {
        TVFile *file = [mi.files filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"format == %@", APST.TVCMediaFormatDownload]].firstObject;
        [filesIds addObject:file.fileID];
    }
    
    [self requestForGetItemsPricesWithCouponsForFiles:filesIds delegate:delegateController completion:^(NSArray *prices) {
        
        for (NSDictionary *dic in prices) {
            TVItemsPrices *tt = [[TVItemsPrices alloc] initWithDictionary:dic];
            
            if ([tt.itemsPrices count] > 0) {
                TVSingleItemPrice* singlePrice = tt.itemsPrices[0];
                TVMediaItem *mediaItem = [mediaItems filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(TVMediaItem *mi, NSDictionary *bindings) {
                    return ([mi.files filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"fileID == %@", [@(tt.fileID) stringValue]]].count);
                }]].firstObject;

                if ([self isPriceReasonPurchased:singlePrice.priceReason]) {
                    mediaItem.priceType = TVPriceTypeFree;
                }
                else {
                    mediaItem.priceType = TVPriceTypeForPurchase;
                }
            }
        }
        completion(mediaItems);
    }];
}

- (void) getItemsPricesWithCoupons:(TVMediaItem *)mediaItem delegate:(BaseViewController *)delegateController completion:(void(^)(NSDictionary *resultAllowedItems, NSDictionary *resultNotAllowedItems))completion {
    
    NSInteger fileIdSD = 0;
    NSInteger fileIdHD = 0;
    
    NSMutableArray *filesIds = [NSMutableArray array];
    
    for (TVFile *file in mediaItem.files) {
        
        if ([file.format isEqualToString:APST.TVCMediaFormatMainSD]) {
            NSLog(@"file %@   %@", file.fileID, file.format);
            fileIdSD = [file.fileID intValue];
            [filesIds addObject:file.fileID];
        } else if ([file.format isEqualToString:APST.TVCMediaFormatMainHD]) {
            NSLog(@"file %@   %@", file.fileID, file.format);
            fileIdHD = [file.fileID intValue];
            [filesIds addObject:file.fileID];
        }
    }
    [self requestForGetItemsPricesWithCouponsForFiles:filesIds delegate:delegateController completion:^(NSArray *prices) {
    
        if (prices.count) {
            NSMutableDictionary *resultAllowedItems = [NSMutableDictionary dictionary];
            NSMutableDictionary *resultNotAllowedItems = [NSMutableDictionary dictionary];

            for (NSDictionary *dic in prices) {
                
                TVItemsPrices *tt = [[TVItemsPrices alloc] initWithDictionary:dic];
                
                if ([tt.itemsPrices count] > 0) {
                    TVSingleItemPrice* singlePrice = tt.itemsPrices[0];
                    
                    //update price type
                    mediaItem.priceType = singlePrice.priceReason;
                    
                    if ([self isPriceReasonPurchased:singlePrice.priceReason]) {
                        if (tt.fileID == fileIdSD) {
                            NSLog(@"SD isPurchased");
                            [resultAllowedItems setObject:@(fileIdSD) forKey:APST.TVCMediaFormatMainSD];
                        }
                        else if (tt.fileID == fileIdHD) {
                            NSLog(@"HD isPurchased");
                            [resultAllowedItems setObject:@(fileIdHD) forKey:APST.TVCMediaFormatMainHD];
                        }
                    }
                    else {
                        if (tt.fileID == fileIdSD) {
                            NSLog(@"SD isPurchased");
                            [resultNotAllowedItems setObject:@(fileIdSD) forKey:APST.TVCMediaFormatMainSD];
                        }
                        else if (tt.fileID == fileIdHD) {
                            NSLog(@"HD isPurchased");
                            [resultNotAllowedItems setObject:@(fileIdHD) forKey:APST.TVCMediaFormatMainHD];
                        }
                    }
                }
            }

            completion(resultAllowedItems, resultNotAllowedItems);
        }
        else {
            completion(@{}, @{});
        }
    }];
}

- (void)requestForGetItemsPricesWithCouponsForFiles:(NSMutableArray *)fileIds delegate:(BaseViewController *)delegateController completion:(void(^)(NSArray *prices))completion {
    
    NSString *userGuid = @"";
    
    if ([TVSessionManager sharedTVSessionManager].currentUser) {
        userGuid = [TVSessionManager sharedTVSessionManager].currentUser.siteGUID;
    }
    NSLog(@"----- > > > GetItemsPricesWithCoupons Start  ----- > > > %@ ----- > >  %f",[NSDate date], ([NSDate timeIntervalSinceReferenceDate] * 1000));

    __weak TVPAPIRequest *request = [TVPConditionalAccessAPI requestForGetItemsPricesWithCoupons:^(RPGetItemsPricesWithCoupons *requestParams) {
        requestParams.fileIDsArray = fileIds;
        requestParams.userGuid = userGuid;
        requestParams.onlyLowest = YES;
        requestParams.couponCode = @"";
        requestParams.languageCode = @"";
        requestParams.deviceName = @"";
    }];
    
    [request setCompletionBlock:^{
        NSLog(@"----- > > > GetItemsPricesWithCoupons End  ----- > > > %@ ----- > >  %f",[NSDate date], ([NSDate timeIntervalSinceReferenceDate] * 1000));

        NSArray *prices = [[request JSONResponse] arrayByRemovingNSNulls];
        completion(prices);
    }];
    
    [request setFailedBlock:^{
        completion(@[]);
    }];
    
    [delegateController sendRequest:request];
}

- (BOOL) isPriceReasonPurchased:(TVPriceType) priceReason {
    BOOL retValue = FALSE;
    switch (priceReason) {
        case TVPriceTypePPVPurchased:
        case TVPriceTypeFree:
        case TVPriceTypePrepaidPurchased:
        case TVPriceTypeSubscriptionPurchased:
        case TVPriceTypeSubscriptionPurchasedWrongCurrency:
            retValue = TRUE;
            break;
        default:
            break;
    }
    return retValue;
}

- (void) getGroupMediaRules:(TVMediaItem *)mediaItem delegate:(BaseViewController *)delegateController completion:(void(^)(NSDictionary *result))completion {

    __weak TVPAPIRequest * request = [TVPSiteAPI requestForGetGroupMediaRules:[mediaItem.mediaID integerValue] delegate:nil];
    
    NSLog(@"----- > > > GetGroupMediaRules Start  ----- > > > %@  ----- > >  %f",[NSDate date], ([NSDate timeIntervalSinceReferenceDate] * 1000));

    [request setCompletionBlock:^{
    
        NSLog(@"----- > > > GetGroupMediaRules End  ----- > > > %@  ----- > >  %f",[NSDate date], ([NSDate timeIntervalSinceReferenceDate] * 1000));

        NSArray *array = [[request JSONResponse] arrayByRemovingNSNulls];
        
        NSMutableDictionary *resultItems = [[NSMutableDictionary alloc] init];
        
        if (array) {
            
            for (NSDictionary *rule in array) {
                if ([rule objectForKey:@"BlockType"]) {}
                TVRuleItem *ruleItem = [[TVRuleItem alloc] initWithDictionary:rule];
                if (ruleItem.blockType == BlockType_AgeBlock) {
                    [resultItems setObject:@"YES" forKey:@"BlockType_AgeBlock"];
                }
                if (ruleItem.blockType == BlockType_Validation) {
                    [resultItems setObject:@(ruleItem.ruleId) forKey:@"BlockType_Validation"];
                }
                if (ruleItem.blockType == BlockType_GeoBlock) {
                    [resultItems setObject:@"YES" forKey:@"BlockType_GeoBlock"];
                }
            }
        }
        
        completion(resultItems);
        
    }];
    
    [request setFailedBlock:^{
        
        completion([NSDictionary dictionary]);
        
    }];
    
    [delegateController sendRequest:request];
}

- (TVSingleItemPrice*) getMediaPrice:(NSArray*)files {
    TVSingleItemPrice *ip = nil;
    for (TVFile *file in files) {
        ip = [self getMediaPriceForFileId:file.fileID];
        if (ip) {
            break;
        }
    }
    return ip;
}

-(NSString * )getFileIdToSingleItemPrice:(TVSingleItemPrice*) singleItemPrice
{
    NSString * fileId=@"0";
    
    NSArray * allKeys = [_dictMediaFilesPrices allKeys];
    for (NSString * key in allKeys)
    {
        if ([_dictMediaFilesPrices objectForKey:key] == singleItemPrice)
        {
            fileId = key;
            break;
        }
    }
    
    return fileId;
}

- (TVSingleItemPrice*) getMediaPriceForFileId:(NSString*)fileId {
    return [_dictMediaFilesPrices objectForKey:fileId];
}

- (void) setMediaPriceForFileId:(NSString*)fileId price:(TVSingleItemPrice*)price {
    if ([_dictMediaFilesPrices objectForKey:fileId])
        [_dictMediaFilesPrices removeObjectForKey:fileId];
    [_dictMediaFilesPrices setObject:price forKey:fileId];
}

- (NSString*) getMediaProductCode:(NSArray*)files {
    NSString *pc = nil;
    for (TVFile *file in files) {
        pc = [dictMediaFilesProductCodes objectForKey:file.fileID];
        if (pc) {
            break;
        }
    }
    return pc;
}

- (void) setProductCodeForFileId:(NSString*)fileId productCode:(NSString*)productCode {
    if ([dictMediaFilesProductCodes objectForKey:fileId])
        [dictMediaFilesProductCodes removeObjectForKey:fileId];
    [dictMediaFilesProductCodes setObject:productCode forKey:fileId];
}

@end
