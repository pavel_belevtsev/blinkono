//
//  PlayerViewControllerVF.h
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerViewController.h"
#import "FavoriteAdapter.h"

@interface PlayerViewControllerVF : PlayerViewController

@property (nonatomic, strong)  UIButton *buttonFavorite;
//@property (weak, nonatomic) IBOutlet UIView *viewSeperator;
@property (nonatomic, strong) FavoriteAdapter *favoriteAdapter;

//-(void)updateFavoritButton;

@end
