//
//  PlayerEpisodesViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 09.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerEpisodesViewController.h"
#import "PlayerViewController.h"
#import "PlayerChannelCollectionViewCell.h"
#import "NSDate+Utilities.h"
#import "MenuViewController.h"
#import "CategorySortTableViewCell.h"
#import "PlayerEpisodeTableViewCell.h"
#import "UIImage+Tint.h"

#import <TvinciSDK/APSBlockOperation.h>

@interface PlayerEpisodesViewController ()

@end

@implementation PlayerEpisodesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.episodesList = [NSArray array];
    
    self.viewContent.layer.cornerRadius = 6;
    self.viewContent.layer.masksToBounds = YES;

    [_buttonSeason setTitle:LS(@"season") forState:UIControlStateNormal];
    
    _viewSort.alpha = 0.0;
    
    [Utils setButtonFont:_buttonSeason bold:YES];
    [_buttonSeason addTarget:self action:@selector(buttonSeasonsPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableViewFilter.layer.cornerRadius = 6;
    self.tableViewFilter.layer.masksToBounds = YES;
    
    _fullList = [[NSMutableArray alloc] init];
    
    UIImage *img = [UIImage imageNamed:@"details_down_arrow"];
    self.imgArrow = [[UIImageView alloc] initWithImage:img];
    
    self.imgArrow.frame = CGRectMake((int)(_buttonSeason.frame.size.width / 2) + 40.0, (int)((_buttonSeason.frame.size.height - img.size.height) / 2.0), img.size.width, img.size.height);
    [_buttonSeason addSubview:_imgArrow];
    
    _viewNoRelatedItems.hidden = YES;
    _labelTitleNoRelatedItems.text = LS(@"player_related_no_items_title");
    _labelSubtitleNoRelatedItems.text = LS(@"player_related_no_items_subtitle");
    [Utils setLabelFont:_labelTitleNoRelatedItems bold:YES];
    [Utils setLabelFont:_labelSubtitleNoRelatedItems bold:NO];
    
    [self skinChanged];
    
}

- (void)skinChanged {
    
    [_buttonSeason setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [_buttonSeason setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    
    [_buttonSeason setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
    [_buttonSeason setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [_buttonSeason setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    
    _viewToolbarBg.backgroundColor = APST.brandColor;

    self.imgArrow.image = [[UIImage imageNamed:@"details_down_arrow"] imageTintedWithColor:APST.brandTextColor];
    
    [_tableViewList reloadData];
    [_tableViewFilter reloadData];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [_tableViewList reloadData];
    
    if (maxFilterTableHeight == 0) {
        maxFilterTableHeight = _tableViewFilter.frame.size.height;
    }
    
}

#pragma mark -

- (void)updateWithMediaItem:(TVMediaItem *)mediaItem {
    
    self.selectedEpisode = mediaItem;
    
    NSInteger season = 0;
    NSString *seriesName = _selectedEpisode.name;
    
    if (_selectedEpisode.isEpisode) {
        
        season = [[_selectedEpisode getMediaItemSeasonNumber] integerValue];
        
        if ([_selectedEpisode.tags objectForKey:@"Series name"] && [[_selectedEpisode.tags objectForKey:@"Series name"] count] > 0) {
            seriesName = [[_selectedEpisode.tags objectForKey:@"Series name"] objectAtIndex:0];
        }
        
        _seriesName = seriesName;
        _season = season;
        
        NSLog(@"Series '%@' %ld", seriesName, (long)_season);
        
        NSDictionary *data = [_delegate.seriesData objectForKey:seriesName];
        
        if (data) {
            
            self.dictionarySeasonsBySeasonNumber = [data objectForKey:@"episodes"];
            
            self.seasons = [data objectForKey:@"seasons"];
            
            self.currentSeason = 0;
            
            int index = 0;
            
            for (NSString *str in _seasons) {
                
                if ([str intValue] == _season) self.currentSeason = index;
                
                index++;
                
            }
            
            [self reloadData];
            
        } else {
            
            [_fullList removeAllObjects];
            [self loadEpisodes:0];
            
        }
    }
}


- (void)loadEpisodes:(int)page {
    
    NSString *episodeTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeEpisode];
    NSDictionary *tagsDictionary = [NSDictionary  dictionaryWithObjectsAndKeys:_seriesName, @"Series Name", nil];
    
     __weak TVPAPIRequest *request = [TVPMediaAPI requestForSearchMediaByAndOrList:@[tagsDictionary] AndList:@[] mediaType:episodeTypeID pageSize:maxPageSize pageIndex:page orderBy:TVOrderByAndOrList_NAME orderDir:TVOrderDirectionByAndOrList_Ascending exact:NO orderMeta:@"" delegate:nil];
    
//    __weak TVPAPIRequest *request= [TVPMediaAPI requestForSearchMediaByMetaDataPairs:nil orTagPairs:tagsDictionary mediaType:episodeTypeID pictureSize:[TVPictureSize iPadItemCellPictureSize] pageSize:maxPageSize pageIndex:page orderBy:TVOrderByName delegate:nil];
   
    [request setCompletionBlock:^{
        
        NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
        
        if ([result count]) {
            
            for (id item in result) {
                [_fullList addObject:item];
            }
            _viewNoRelatedItems.hidden = YES;
            
        }else {
            _viewNoRelatedItems.hidden = NO;
        }
        
        if ([result count] == maxPageSize) {
        
            [self loadEpisodes:page + 1];
        
        } else {
            
            [self sortFullList];
            
        }
        
    }];
    
    [request setFailedBlock:^{
        _viewNoRelatedItems.hidden = NO;
        
    }];
    
    [_delegate sendRequest:request];
    

}

- (void)sortFullList {
    
    self.dictionarySeasonsBySeasonNumber = [Utils mapEpisodesBySeasonsNumber:_fullList];
    
    self.seasons = [Utils sortSeasonsNumbers:self.dictionarySeasonsBySeasonNumber.allKeys];
    
    NSDictionary *item = [[NSDictionary alloc] initWithObjectsAndKeys:_seasons, @"seasons", _dictionarySeasonsBySeasonNumber, @"episodes", nil];
    [_delegate.seriesData setObject:item forKey:_seriesName];
    
    self.currentSeason = 0;
    
    int index = 0;
    
    for (NSString *str in _seasons) {
        
        if ([str intValue] == _season) self.currentSeason = index;
        
        index++;
        
    }
    
    [self reloadData];
    
}

- (void)reloadData {
    
    if ([_seasons count]) {
        
        if (_currentSeason >= [_seasons count]) _currentSeason = 0;
        
        NSString *key = [_seasons objectAtIndex:_currentSeason];
        self.episodesList = [_dictionarySeasonsBySeasonNumber objectForKey:key];
        
        [_buttonSeason setTitle:[NSString stringWithFormat:LS(@"season"), key] forState:UIControlStateNormal];
        
        [_tableViewFilter reloadData];
        [_tableViewList reloadData];
        
        CGFloat height = [_seasons count] * sortRowHeight;
        if (height > maxFilterTableHeight) height = maxFilterTableHeight;
        
        [UIView animateWithDuration:0.3 animations:^{
            [_tableViewFilter superview].frame = CGRectMake([_tableViewFilter superview].frame.origin.x, [_tableViewFilter superview].frame.origin.y, [_tableViewFilter superview].frame.size.width, height + 44);
        }];
        
    }
    
    
}

#pragma mark - Segment Buttons

- (void)buttonSeasonsPressed:(UIButton *)button {

    button.selected = !button.selected;
    
    [UIView animateWithDuration:0.3 animations:^{
    
        _viewSort.alpha = (button.selected ? 1.0 : 0.0);
        [_imgArrow setTransform:CGAffineTransformMakeRotation(button.selected ? M_PI : 0)];

    }];
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    
    if (isPhone) {
        
        [UIView animateWithDuration:duration animations:^{
            
            if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
                
                self.viewContent.frame = CGRectMake(self.viewContent.frame.origin.x, 0, self.viewContent.frame.size.width, self.view.frame.size.height);
                
            } else {
                
                self.viewContent.frame = CGRectMake(self.viewContent.frame.origin.x, 40.0, self.viewContent.frame.size.width, self.view.frame.size.height - 60.0);
                
            }
            
        }];
        
        
    }
    
}


#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag == 0) {
        return [_episodesList count];
    } else {
        return [_seasons count];
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 0) {
        
        static NSString *cellIdentifier = @"PlayerEpisodeTableViewCell";
        PlayerEpisodeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:[Utils nibName:cellIdentifier] owner:self options:nil] objectAtIndex:0];
            
        }
        
        UIView *selectedBGView = [[UIView alloc] init];
        selectedBGView.backgroundColor = APST.brandColor;
        cell.selectedBackgroundView = selectedBGView;

        TVMediaItem *mediaItem = [_episodesList objectAtIndex:indexPath.row];
        [cell updateData:mediaItem];
        
        return cell;

    } else {
        
        static NSString *cellIdentifier = @"CategorySortTableViewCell";
        
        CategorySortTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:tableView options:nil] objectAtIndex:0];
        }
        
        cell.labelSort.text = [NSString stringWithFormat:LS(@"season"), [_seasons objectAtIndex:indexPath.row]];
        
        cell.cellSelected = (indexPath.row == _currentSeason);
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView.tag == 0) {
        
        TVMediaItem *mediaItem = [_episodesList objectAtIndex:indexPath.row];
        [_delegate selectMediaItem:mediaItem];
        
        [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
        
    } else {
        
        [self buttonSeasonsPressed:_buttonSeason];
        
        _currentSeason = indexPath.row;
        
        [self reloadData];
        
    }
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
