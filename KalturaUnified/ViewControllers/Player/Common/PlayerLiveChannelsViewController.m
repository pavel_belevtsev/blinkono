//
//  PlayerLiveChannelsViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 09.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerLiveChannelsViewController.h"
#import "PlayerViewController.h"
#import "PlayerChannelCollectionViewCell.h"
#import "NSDate+Utilities.h"
#import "MenuViewController.h"
#import "CategorySortTableViewCell.h"
#import "UIImage+Tint.h"
#import "GroupedStyleTableView.h"

#import <TvinciSDK/APSBlockOperation.h>

@interface PlayerLiveChannelsViewController ()

@end

@implementation PlayerLiveChannelsViewController

- (void)dealloc {
    _collectionViewList.delegate = nil;
    _collectionViewList.dataSource = nil;
}

- (void) awakeFromNib
{
    self.classNameChannelCell = @"PlayerChannelCollectionViewCell";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.channelsList = [[NSMutableArray alloc] init];
    
    self.viewContent.layer.cornerRadius = 6;
    self.viewContent.layer.masksToBounds = YES;
    
    [_buttonSegmentChannels setTitle:LS(@"category_page_sort_by") forState:UIControlStateNormal];
    _buttonSegmentChannels.titleLabel.numberOfLines = 2;
    _buttonSegmentChannels.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    _buttonSegmentChannels.selected = NO;
    _viewSort.alpha = 0.0;
    
    [Utils setButtonFont:_buttonSegmentChannels bold:YES];
    [_buttonSegmentChannels addTarget:self action:@selector(buttonSegmentSort:) forControlEvents:UIControlEventTouchUpInside];
    
    [_buttonSegmentGuide setTitle:LS(@"playerLiveChannels_FullTVGuide") forState:UIControlStateNormal];
    [Utils setButtonFont:_buttonSegmentGuide bold:YES];
    [_buttonSegmentGuide addTarget:self action:@selector(buttonSegmentTVGuide:) forControlEvents:UIControlEventTouchUpInside];
    
    [_collectionViewList registerNib:[UINib nibWithNibName:[Utils nibName:@"PlayerChannelCollectionViewCell"] bundle:nil] forCellWithReuseIdentifier:self.classNameChannelCell];

    self.dictionaryRequest = [[NSMutableDictionary alloc] init];
    self.dictionaryCurrentProgramByChannelID = [[NSMutableDictionary alloc] init];

    GroupedStyleTableView* table = (GroupedStyleTableView*) self.tableViewFilter;
    table.cornerRadius = 5;
    
    self.filterMenuItemList = [[NSMutableArray alloc] init];

    _displayedSortItems = @[@{@"title":LS(@"category_page_newest"), @"sortBy":@(TVOrderByDateAdded)},
                       @{@"title":LS(@"category_page_most_popular"), @"sortBy":@(TVOrderByNumberOfViews)},
                       @{@"title":LS(@"category_page_most_liked"), @"sortBy":@(TVOrderByRating)}];

    UIImage *img = [UIImage imageNamed:@"details_down_arrow"];
    self.imgArrow = [[UIImageView alloc] initWithImage:[img imageTintedWithColor:CS(@"bebebe")]];
    
    self.imgArrow.frame = CGRectMake(_buttonSegmentChannels.frame.size.width - 20.0, (int)((_buttonSegmentChannels.frame.size.height - img.size.height) / 2.0), img.size.width, img.size.height);
    [_buttonSegmentChannels addSubview:_imgArrow];
    
    [_buttonSegmentChannels addTarget:self action:@selector(buttonChannelsTouchDown:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragInside];
    [_buttonSegmentChannels addTarget:self action:@selector(buttonChannelsTouchCancel:) forControlEvents:UIControlEventTouchCancel | UIControlEventTouchDragOutside];
    
    [self skinChanged];
    _labelNoRelatedTitle.text = LS(@"player_related_no_items_title");
    _labelNoRelatedSubtitle.text = LS(@"player_related_no_items_subtitle");
    [Utils setLabelFont:_labelNoRelatedTitle bold:YES];
    [Utils setLabelFont:_labelNoRelatedSubtitle bold:NO];
    
    self.viewSort.backgroundColor = [UIColor colorWithWhite:68.0/255.0 alpha:1.0];
}

- (void)skinChanged {
    
    [_buttonSegmentChannels setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [_buttonSegmentChannels setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    
    [_buttonSegmentGuide setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [_buttonSegmentGuide setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    
    _viewToolbarBg.backgroundColor = APST.brandColor;
    
    UIImage *img = [UIImage imageNamed:@"details_down_arrow"];
    self.imgArrow.highlightedImage = [img imageTintedWithColor:APST.brandTextColor];
}

- (void)buttonChannelsTouchDown:(UIButton *)button {
    _imgArrow.highlighted = !button.selected;
    
    [self rotateImgArrow];
}

- (void)buttonChannelsTouchCancel:(UIButton *)button {
    _imgArrow.highlighted = NO;
    
    [self rotateImgArrow];
}

- (void)rotateImgArrow {
    
    [UIView animateWithDuration:0.2f animations:^{
        [_imgArrow setTransform:CGAffineTransformMakeRotation(_imgArrow.highlighted ? M_PI : 0)];
    }];
    
}

- (void)resetdisplayedFilterItems {
    
    BOOL isSignIn = [LoginM isSignedIn];
    
    TVMenuItem *epgMenuItem = [MenuM menuItem:TVPageLayoutEPG];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    if (isSignIn) [array addObject:LS(@"my_channels")];
    
    for (id item in epgMenuItem.children) {
        [array addObject:item];
    }
    
    _displayedFilterItems = array;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [_collectionViewList reloadData];
}

#pragma mark -

- (void)updateWithMediaItem:(TVMediaItem *)mediaItem {
    
    self.selectedChannel = mediaItem;
    
    if (!EPGM.channelsList) {
        
        [EPGM loadEPGChannels:self completion:^(NSMutableArray *result) {
            
            [EPGM updateEPGList:result delegate:self completion:^(NSMutableArray *result) {
                
            }];
            
            [self reloadData];
            
        }];
        
    } else {
        
        [self reloadData];
        
    }
    
}

- (void)reloadData {
    
    if ([_filterMenuItemList count]) {
        // user selected a filter
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [array addObject:LS(@"all_filters")];
        [array addObject:[_filterMenuItemList lastObject]];
        
        _displayedFilterItems = array;
    } else {
        // fill
        [self resetdisplayedFilterItems];
    }
    
    [_tableViewFilter reloadData];
    
    BOOL useFilters = [_filterMenuItemList count];
    TVMenuItem *epgMenuItem = [MenuM menuItem:TVPageLayoutEPG];

    NSInteger channelId = [[epgMenuItem.layout objectForKey:ConstKeyMenuItemChannel] integerValue];
    
    BOOL useMyChannels = NO;
    
    if (useFilters) {
        
        TVMenuItem *item = [_filterMenuItemList lastObject];
        if ([item isKindOfClass:[TVMenuItem class]]) {
            channelId = item.channelID;
        } else {
            useMyChannels = YES;
        }
        
    }
    
    if (useFilters) {
        
        if (_selectedFilters) {
            
            [self fillChannelsData:_selectedFilters];
            
        } else {
            
            if (useMyChannels) {
                
                NSMutableArray *array = [[NSMutableArray alloc] init];
                for (TVMediaItem *item in EPGM.channelsList) {
                    if (item.isFavorite) {
                        [array addObject:item];
                    }
                }
                
                [EPGM sortChannelsList:array];
                [self fillChannelsData:array];
                
            } else {
                
                [self showHUDBlockingUI:YES];
                
                [self requestForGetChannelMediaList:channelId orderBy:TVOrderByName completion:^(NSArray *result) {
                    
                    NSMutableArray *array = [[NSMutableArray alloc] init];
                    if ([result count]) {
                        
                        for (NSDictionary *dic in result) {
                            TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dic];
                            if ([item isLive]) {
                                [array addObject:item];
                            }
                        }
                        
                    }
                    
                    [EPGM sortChannelsList:array];
                    // here is where we set the selectedFilters
                    _selectedFilters = array;
                    
                    [self fillChannelsData:array];
                    
                    [self hideHUD];
                    
                }];
                
            }
            
            
        }
        
    } else {
        
        // not filtering channels
        NSArray *array = EPGM.channelsList;
        
//        if (sortOnlyMode && _orderBy > 0) {
        if (_orderBy > 0) {

            if (_orderBy == TVOrderByDateAdded) {
                
                array = [EPGM.channelsList sortedArrayUsingComparator:^NSComparisonResult(TVMediaItem *p1, TVMediaItem *p2) {
                    
                    if ([p1.creationDate timeIntervalSince1970] < [p2.creationDate timeIntervalSince1970]) {
                        return NSOrderedDescending;
                    }  else {
                        return NSOrderedAscending;
                    }
                    
                }];
                
            } else if (_orderBy == TVOrderByNumberOfViews) {
                
                array = [EPGM.channelsList sortedArrayUsingComparator:^NSComparisonResult(TVMediaItem *p1, TVMediaItem *p2) {
                    
                    if (p1.viewCounter < p2.viewCounter) {
                        return NSOrderedDescending;
                    }  else {
                        return NSOrderedAscending;
                    }
                    
                }];
                
            } else if (_orderBy == TVOrderByRating) {
                
                array = [EPGM.channelsList sortedArrayUsingComparator:^NSComparisonResult(TVMediaItem *p1, TVMediaItem *p2) {
                    
                    if (p1.likeCounter < p2.likeCounter) {
                        return NSOrderedDescending;
                    }  else {
                        return NSOrderedAscending;
                    }
                    
                }];
                
                
            }
            
        }
        
        [self fillChannelsData:array];
    }
    
}

- (void)fillChannelsData:(NSArray *)array {
    
    if (array) {
    
        [_channelsList removeAllObjects];
    
        int index = 0;
        int selectedIndex = -1;
        for (TVMediaItem *item in array) {
            [_channelsList addObject:item];
            if (_selectedChannel && [_selectedChannel.mediaID isEqualToString:item.mediaID]) selectedIndex = index;
            index++;
        }
        
        [_collectionViewList reloadData];
        if ([_channelsList count]) {
            _viewNoRelatedItems.hidden = YES;
        }else {
            _viewNoRelatedItems.hidden = NO;
        }
        
        if (selectedIndex >= 0 && !listScrolled) {
            listScrolled = YES;
            [_collectionViewList scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
        }
        
    }
    
}

#pragma mark - Segment Buttons 

- (void)buttonSegmentSort:(UIButton *)button {

    button.selected = !button.selected;
    _imgArrow.highlighted = button.selected;
    
    [self rotateImgArrow];

    [[_viewSort superview] bringSubviewToFront:_viewSort];
    [UIView animateWithDuration:0.3 animations:^{
    
        _viewSort.alpha = (button.selected ? 1.0 : 0.0);
        
    }];
    
}

- (void)buttonSegmentTVGuide:(UIButton *)button {
    
    [[self menuContainerViewController] toggleRightSideMenuCompletion:^{
        
        [((MenuViewController *)[_delegate menuContainerViewController].leftMenuViewController) pressMenuItem:[MenuM menuItem:TVPageLayoutEPG] activityFeedLoadOnMyZoneStart:NO];

    }];
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    
    if (isPhone) {
        
        UIView *viewForSorting = [_tableViewFilter superview];
        if (sortViewHeight == 0) sortViewHeight = viewForSorting.frame.size.height;
        
        [UIView animateWithDuration:duration animations:^{
            
            if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
                
                viewForSorting.frame = CGRectMake(viewForSorting.frame.origin.x, viewForSorting.frame.origin.y, viewForSorting.frame.size.width, 310);
                self.viewContent.frame = CGRectMake(self.viewContent.frame.origin.x, 0, self.viewContent.frame.size.width, self.view.frame.size.height);
                
            } else {
                
                if (sortViewHeight > 0) viewForSorting.frame = CGRectMake(viewForSorting.frame.origin.x, viewForSorting.frame.origin.y, viewForSorting.frame.size.width, sortViewHeight);
                self.viewContent.frame = CGRectMake(self.viewContent.frame.origin.x, 40.0, self.viewContent.frame.size.width, self.view.frame.size.height - 60.0);
                
            }
            
        }];
        
        
    }
    
}

#pragma mark - Collection View Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [_channelsList count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PlayerChannelCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:self.classNameChannelCell forIndexPath:indexPath];
    
    TVMediaItem *channel = [_channelsList objectAtIndex:indexPath.row];
    
    [cellView updateWithChannel:channel];
    
    [cellView selectChannel:[self.selectedChannel.mediaID isEqualToString:channel.mediaID]];
    
    APSTVProgram * program = [self.dictionaryCurrentProgramByChannelID objectForKey:channel.epgChannelID];
    
    if ([program isKindOfClass:[APSTVProgram class]]) {
        if (![program programIsOn]) {
            
            [self.dictionaryCurrentProgramByChannelID removeObjectForKey:channel.epgChannelID];
            APSBlockOperation * request = [self.dictionaryRequest objectForKey:channel.epgChannelID];
            [request cancel];
            if (channel.epgChannelID)
            {
                [self.dictionaryRequest removeObjectForKey:channel.epgChannelID];
            }
            
            program = nil;
        }
    }
    
    if (program == nil && [self.dictionaryRequest objectForKey:channel.epgChannelID] == nil && channel.epgChannelID != nil)
    {
        [self loadProgramForChannel:channel];
        cellView.viewLoading.alpha = 1.0;
        
    } else {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            cellView.viewLoading.alpha = 0.0;
            
        }];
        
        BOOL showTime = YES;
        
        if ([program isKindOfClass:[NSNull class]] || program == nil) {
            
            showTime = NO;
            
        }
        
        cellView.viewInfoProgressBg.hidden = !showTime;
        cellView.labelInfoStart.hidden = !showTime;
        cellView.labelInfoEnd.hidden = !showTime;
        
        BOOL showInfo = YES;
        cellView.labelNoEPG.hidden = showInfo;
        cellView.viewInfo.hidden = !showInfo;
        cellView.viewBuzz.hidden = YES;
        
        [cellView layoutSubviews];
        
        if (showTime)
        {
            NSInteger timeFull = ([program.endDateTime timeIntervalSince1970] - [program.startDateTime timeIntervalSince1970]);
            NSInteger timeCurrent = ([[NSDate date] timeIntervalSince1970] - [program.startDateTime timeIntervalSince1970]);
            
            CGFloat fullWidth = [cellView.viewInfoProgress superview].frame.size.width;
            
            CGRect bounds = cellView.viewInfoProgress.frame;
            bounds.size = CGSizeMake(fullWidth * timeCurrent / timeFull, cellView.viewInfoProgress.frame.size.height);
        
            cellView.viewInfoProgress.frame = bounds;
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"HH:mm"];
            cellView.labelInfoStart.text = [program.startDateTime stringFromDateWithDateFormat:@"HH:mm"];
            cellView.labelInfoEnd.text = [program.endDateTime stringFromDateWithDateFormat:@"HH:mm"];
        
            // NSLog(@"%d  %d    %f   %f", timeFull, timeCurrent, fullWidth, fullWidth * timeCurrent / timeFull);
            
            cellView.labelInfoTitle.text = program.name;
            
        } else {
            
            cellView.labelInfoTitle.text = cellView.labelNoEPG.text;
            
        }
 
        
    }
    //cell.program = program;
    //cell.delegate= self;
    
    return cellView;

}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (![collectionView.indexPathsForVisibleItems containsObject:indexPath]) {
        if (indexPath.item < self.channelsList.count)
        {
            TVMediaItem * channel = self.channelsList[indexPath.item];
            APSBlockOperation * request = [self.dictionaryRequest objectForKey:channel.epgChannelID];
            [request cancel];
            if (channel.epgChannelID)
            {
                [self.dictionaryRequest removeObjectForKey:channel.epgChannelID];
            }
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.item < self.channelsList.count) {
        
        TVMediaItem *item = [self.channelsList objectAtIndex:indexPath.row];
        [self.delegate clearOldAttributes];
        [self.delegate selectMediaItem:item];
        
        [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
        
    }
}

- (void)loadProgramForChannel:(TVMediaItem *) channel {
    
    __weak PlayerLiveChannelsViewController * weakSelf = self;
    __weak TVMediaItem * weakChannel = channel;
    APSBlockOperation * request = [[APSEPGManager sharedEPGManager] programForChannelId:channel.epgChannelID date:[NSDate date] privateContext:self.privateContext starterBlock:^{} failedBlock:^{} completionBlock:^(APSTVProgram *program) {
        if (program != nil) {
            [weakSelf.dictionaryCurrentProgramByChannelID setObject:program forKey:weakChannel.epgChannelID];
        } else {
            [weakSelf.dictionaryCurrentProgramByChannelID setObject:[NSNull null] forKey:weakChannel.epgChannelID];
        }
        
        [weakSelf.collectionViewList reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:[weakSelf.channelsList indexOfObject:weakChannel]inSection:0]]];}];
    if (request != nil && channel.epgChannelID != nil){
        [self.dictionaryRequest setObject:request forKey:channel.epgChannelID];
    }
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // First section - always acontain 3 sort items
    // Second section contains filters recieved from TVM
    BOOL tvmFiltersAvailable = ([_displayedFilterItems count]>0);
    return tvmFiltersAvailable? 2:1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return [_displayedSortItems count];
    }
    return [_displayedFilterItems count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return [LS(@"category_page_sort_by") uppercaseString];
    }
    return [LS(@"browse_by") uppercaseString];
}

// default group titles in UITableView are not aligned with the group cell as product requested,
// the only way to accomplish that is to override the viewForHeaderInSection method:
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *titleLabel;
    
    // frame at (0,0) so that title will align with cells
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 24)];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.textColor = [UIColor colorWithWhite:199.0/255.0 alpha:1.0];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    [Utils setLabelFont:titleLabel size:12 bold:YES];
    return titleLabel;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 33;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:[UIColor colorWithWhite:52.0/255.0 alpha:1.0]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CategorySortTableViewCell";
    
    CategorySortTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:tableView options:nil] objectAtIndex:0];
        cell.defaultTextColor = [UIColor colorWithWhite:168.0/255.0 alpha:1.0];
        cell.viewDivider.backgroundColor = [UIColor colorWithWhite:68.0/255.0 alpha:1.0];
        
        CGRect frame = cell.frame;
        frame.size.height = 36;
        cell.frame = frame;
        
        [Utils setLabelFont:cell.labelSort size:14 bold:YES];
    }
    

    
    cell.selected = NO;
    
    if (indexPath.section == 0) {

        NSDictionary *item = [_displayedSortItems objectAtIndex:indexPath.row];
        cell.labelSort.text = [item objectForKey:@"title"];
        int sortBy = [[item objectForKey:@"sortBy"] intValue];
        cell.cellSelected = (sortBy == _orderBy);
        cell.selected = (sortBy == _orderBy);
        
    } else {
        // handle filter cell item
        TVMenuItem *menuItem = [_displayedFilterItems objectAtIndex:indexPath.row];
        
        if ([menuItem isKindOfClass:[TVMenuItem class]]) {
            
            cell.labelSort.text = menuItem.name;

            // set the selected state of the filters
            if ( [[_filterMenuItemList lastObject] isEqual:menuItem]) {
                cell.cellSelected = YES;
                cell.selected = YES;
            }
            
        } else {
            
            cell.labelSort.text = (NSString *)menuItem;
            if ([[_filterMenuItemList lastObject] isEqual:menuItem]) {
                cell.cellSelected = YES;
                cell.selected = YES;
            }
        }
        
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        NSDictionary *item = [_displayedSortItems objectAtIndex:indexPath.row];
        
        _orderBy = [[item objectForKey:@"sortBy"] intValue];
        
        [tableView reloadData];
        
        [self selectSortBy:_orderBy];
        
        [self buttonSegmentSort:_buttonSegmentChannels];
        
    } else {
        
        _selectedFilters = nil;
        
        TVMenuItem *item = [_displayedFilterItems objectAtIndex:indexPath.row];
        
        if ([item isKindOfClass:[TVMenuItem class]]) {
            
            if (![item.children count]) {
                
                [self buttonSegmentSort:_buttonSegmentChannels];
                
            }
            [self selectFilterOption:item];
            
        } else {
            
            if ([_filterMenuItemList count]) {
                if (indexPath.row == 0) {
                    [self selectFilterBack];
                } else {
                    
                    [self buttonSegmentSort:_buttonSegmentChannels];
                    
                }
            } else {
                [self selectMyChannel:((NSString *)item)];
                
                [self buttonSegmentSort:_buttonSegmentChannels];
                
            }
            
        }
        
    }
}


#pragma mark -

- (void)selectFilterOption:(TVMenuItem *)filterItem {
    
    if (![_filterMenuItemList count] || ![[_filterMenuItemList lastObject] isEqual:filterItem]) {
        [_filterMenuItemList addObject:filterItem];
        
        [self reloadData];
        
    }
    
}

- (void)selectMyChannel:(NSString *)filterItem {
    
    [_filterMenuItemList addObject:filterItem];
    
    [self reloadData];
    
}

- (void)selectFilterBack {
    
    if ([_filterMenuItemList count]) {
        [_filterMenuItemList removeLastObject];
        
        [self reloadData];
        
    }
}

- (void)selectSortBy:(TVOrderBy)orderBy {
    
    _orderBy = orderBy;
    
    [self reloadData];
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
