//
//  PlayerEpisodesViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 09.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlayerViewController;

@interface PlayerEpisodesViewController : BaseViewController {
    
    CGFloat maxFilterTableHeight;
    
    BOOL listScrolled;
    
    BOOL sortOnlyMode;
    
}

- (void)updateWithMediaItem:(TVMediaItem *)mediaItem;
- (void)reloadData;

@property (nonatomic, weak) PlayerViewController *delegate;
@property (nonatomic, strong) TVMediaItem *selectedEpisode;

@property (nonatomic, strong) NSString *seriesName;
@property (nonatomic) NSInteger season;

@property (nonatomic, strong) NSDictionary *dictionarySeasonsBySeasonNumber;
@property (nonatomic) NSInteger currentSeason;

@property (nonatomic, strong) NSMutableArray *fullList;

@property (weak, nonatomic) IBOutlet UITableView *tableViewList;

@property (weak, nonatomic) IBOutlet UITableView *tableViewFilter;

@property (strong, nonatomic) NSArray *seasons;

@property (nonatomic, strong) NSArray *episodesList;

@property (weak, nonatomic) IBOutlet UIView *viewToolbarBg;
@property (weak, nonatomic) IBOutlet UIButton *buttonSeason;

@property (weak, nonatomic) IBOutlet UIView *viewSort;

@property (nonatomic, strong) UIImageView *imgArrow;

@property (weak, nonatomic) IBOutlet UIView *viewNoRelatedItems;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleNoRelatedItems;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitleNoRelatedItems;

@end
