//
//  PlayerLiveChannelsViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 09.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlayerViewController;

@interface PlayerLiveChannelsViewController : BaseViewController<UITableViewDataSource> {
    
    CGFloat maxFilterTableHeight;
    BOOL listScrolled;
    
    float sortViewHeight;

}

- (void)updateWithMediaItem:(TVMediaItem *)mediaItem;
- (void)reloadData;

@property (nonatomic, weak) PlayerViewController *delegate;

@property (nonatomic, strong) TVMediaItem *selectedChannel;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewList;

@property (strong, nonatomic) NSArray *displayedFilterItems;    // list of items displayed in the "Browse By" group
@property (strong, nonatomic) NSArray *displayedSortItems;         // list of items displayed in the "Sort By" group
@property (strong, nonatomic) NSArray *selectedFilters;


@property (nonatomic) TVOrderBy orderBy;
@property (nonatomic, strong) NSMutableArray *filterMenuItemList;   // array of applied/active filters - usually holds one active folter or none

@property (weak, nonatomic) IBOutlet UITableView *tableViewFilter;

@property (nonatomic, strong) NSMutableArray *channelsList;

@property (weak, nonatomic) IBOutlet UIView *viewToolbarBg;
@property (weak, nonatomic) IBOutlet UIButton *buttonSegmentChannels;
@property (weak, nonatomic) IBOutlet UIButton *buttonSegmentGuide;

@property (weak, nonatomic) IBOutlet UIView *viewSort;

@property (weak, nonatomic) IBOutlet UIView *viewNoRelatedItems;
@property (weak, nonatomic) IBOutlet UILabel *labelNoRelatedTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelNoRelatedSubtitle;

@property (strong, nonatomic) NSMutableDictionary *dictionaryRequest;
@property (strong, nonatomic) NSMutableDictionary *dictionaryCurrentProgramByChannelID;


@property (strong, nonatomic) UIImageView *imgArrow;

@property (strong, nonatomic) NSString* classNameChannelCell;

@end
