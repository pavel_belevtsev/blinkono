//
//  PlayerLiveChannelsViewControllerVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/22/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerLiveChannelsViewControllerVF.h"
#import "CategorySortTableViewCellVF.h"

@interface PlayerLiveChannelsViewControllerVF ()

@end


@implementation PlayerLiveChannelsViewControllerVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void) awakeFromNib
{
    [super awakeFromNib];
    
    //TODO: aviv - can we go around the following:
    self.classNameChannelCell = @"PlayerChannelCollectionViewCellVF";
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.collectionViewList.backgroundColor = [UIColor colorWithWhite:35.0/255.0 alpha:1];
    
    CGRect frame = self.imgArrow.frame;
    frame.origin.x -=15;
    self.imgArrow.frame = frame;
    
    self.viewSort.backgroundColor = [UIColor clearColor];
    
    UIView* subViewThatISNotExposed = [[self.viewSort subviews] objectAtIndex:0];
    subViewThatISNotExposed.backgroundColor = [VFTypography instance].grayScale1Color;
    self.displayedSortItems = [NSArray array];
}


#pragma mark - Collection view layout
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    // create a seperator space of one pixel between cells - color of seperator
    // is defined by self.collectionViewList.backgroundColor
    return 1.0;
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */


-(void)updateMenuItemSelectedCell:(CategorySortTableViewCell *)cell toMenuItem:(TVMenuItem *)menuItem
{
    if ([menuItem isKindOfClass:[TVMenuItem class]])
    {
        cell.labelSort.text = menuItem.name;
        // set the selected state of the filters
        if ( [[self.filterMenuItemList lastObject] isEqual:menuItem])
        {
            cell.cellSelected = YES;
            cell.selected = YES;
        }
    }
    else
    {
        cell.labelSort.text = (NSString *)menuItem;
        if ([[self.filterMenuItemList lastObject] isEqual:menuItem])
        {
            cell.cellSelected = YES;
            cell.selected = YES;
        }
    }
}

-(void)updateTitleSelectedCell:(CategorySortTableViewCell *)cell toItem:(NSDictionary*)item
{
    cell.labelSort.text = [item objectForKey:@"title"];
    int sortBy = [[item objectForKey:@"sortBy"] intValue];
    cell.cellSelected = (sortBy == self.orderBy);
    cell.selected = (sortBy == self.orderBy);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CategorySortTableViewCellVF";

    CategorySortTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:tableView options:nil] objectAtIndex:0];
        cell.defaultTextColor = [UIColor colorWithWhite:168.0/255.0 alpha:1.0];
        cell.viewDivider.backgroundColor = [UIColor colorWithWhite:68.0/255.0 alpha:1.0];
        
        CGRect frame = cell.frame;
        frame.size.height = 36;
        cell.frame = frame;
        
        [Utils setLabelFont:cell.labelSort size:14 bold:YES];
    }
    
    cell.selected = NO;
    
    if (indexPath.section == 0)
    {
        NSDictionary *item = [self.displayedSortItems objectAtIndex:indexPath.row];
        [self updateTitleSelectedCell:cell toItem:item];
    }
    else
    {
        // handle filter cell item
        TVMenuItem *menuItem = [self.displayedFilterItems objectAtIndex:indexPath.row];
        [self updateMenuItemSelectedCell:cell toMenuItem:menuItem];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell)
    {
        CategorySortTableViewCellVF* categoryCell = (CategorySortTableViewCellVF*) cell;
        categoryCell.defaultTextColor = [VFTypography instance].grayScale6Color;
    }
    
    return cell;
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]])
    {
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        [Utils setLabelFont:tableViewHeaderFooterView.textLabel bold:YES];
    }
    else if([view isKindOfClass:[UILabel class]])
    {
        UILabel * lab = (UILabel *)view;
        [Utils setLabelFont:lab bold:YES];
    }
}
/* "PSC : Vodafone Ono : SYN-4044 Filter on linear content should be removed */

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return nil;//[LS(@"category_page_sort_by") uppercaseString];
    }
    return [LS(@"browse_by") uppercaseString];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0)
    {
        return 1;
    }
    return 33;
}
@end
