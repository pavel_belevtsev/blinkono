//
//  PlayerRelatedViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerRelatedViewController.h"
#import "PlayerViewController.h"
#import "PlayerVODRelatedTableViewCell.h"

@interface PlayerRelatedViewController ()

@end

@implementation PlayerRelatedViewController

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.relatedMediaList = [[NSMutableArray alloc] init];
    
    self.viewContent.layer.cornerRadius = 6;
    self.viewContent.layer.masksToBounds = YES;

    [Utils setLabelFont:_labelRelated bold:YES];
    _labelRelated.text = LS(@"player_related_related_movies");
    
    _viewNoRelatedItems.hidden = YES;
    _labelTitleNoRelatedItems.text = LS(@"player_related_no_items_title");
    _labelSubtitleNoRelatedItems.text = LS(@"player_related_no_items_subtitle");
    [Utils setLabelFont:_labelTitleNoRelatedItems bold:YES];
    [Utils setLabelFont:_labelSubtitleNoRelatedItems bold:NO];
    
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
    
}

- (void)skinChanged {
    
    [_tableViewList reloadData];
    
}

- (void)updateWithMediaItem:(TVMediaItem *)mediaItem {
    
    [_relatedMediaList removeAllObjects];
    [_tableViewList reloadData];
    _tableViewList.contentOffset = CGPointZero;
    
    [self loadRelated:mediaItem.mediaID pageIndex:0];
    
    
}

- (void)loadRelated:(NSString *)mediaID pageIndex:(int)pageIndex {
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetRelatedMediasWithMediaID:mediaID mediaType:[[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeMovie] pictureSize:[TVPictureSize iPadItemCellPictureSize] pageSize:maxPageSize pageIndex:pageIndex delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
        
        for (NSDictionary *dic in result) {
            TVMediaItem *item = [[TVMediaItem alloc] initWithDictionary:dic];
            [_relatedMediaList addObject:item];
        }

        if ([result count] == maxPageSize) {
            
            [self loadRelated:mediaID pageIndex:(pageIndex + 1)];
            
        } else {
            
            if ([_relatedMediaList count]) {
                
                
                [_tableViewList reloadData];
                _viewNoRelatedItems.hidden = YES;
                
            } else {
                _viewNoRelatedItems.hidden = NO;
            }
        }
        
        
    }];
    
    [request setFailedBlock:^{
        _viewNoRelatedItems.hidden = YES;
        
    }];
    
    [_delegate sendRequest:request];
    
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {
    
    if (isPhone) {
        
        [UIView animateWithDuration:duration animations:^{
            
            if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
                
                _viewContent.frame = CGRectMake(_viewContent.frame.origin.x, 0, _viewContent.frame.size.width, self.view.frame.size.height);
                
            } else {
                
                _viewContent.frame = CGRectMake(_viewContent.frame.origin.x, 40.0, _viewContent.frame.size.width, self.view.frame.size.height - 60.0);
                
            }
            
        }];
        
        
    }
    
}



#pragma mark - UITableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _relatedMediaList.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"PlayerVODRelatedTableViewCell";
    PlayerVODRelatedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:[Utils nibName:cellIdentifier] owner:self options:nil] objectAtIndex:0];
        
        
    }

    UIView *selectedBGView = [[UIView alloc] init];
    selectedBGView.backgroundColor = APST.brandColor;
    cell.selectedBackgroundView = selectedBGView;
    
    TVMediaItem *mediaItem = [_relatedMediaList objectAtIndex:indexPath.row];
    [cell updateData:mediaItem];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [NavM setPhonePortrait];
    
    TVMediaItem *mediaItem = [_relatedMediaList objectAtIndex:indexPath.row];
    [_delegate selectMediaItem:mediaItem];
    
    [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];

}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
