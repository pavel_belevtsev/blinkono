//
//  PlayerRelatedViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVMediaItem+PSTags.h"

@class PlayerViewController;

@interface PlayerRelatedViewController : UIViewController

- (void)updateWithMediaItem:(TVMediaItem *)mediaItem;

@property (nonatomic, weak) PlayerViewController *delegate;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UITableView *tableViewList;
@property (weak, nonatomic) IBOutlet UILabel *labelRelated;

@property (nonatomic, strong) NSMutableArray *relatedMediaList;

@property (weak, nonatomic) IBOutlet UIView *viewNoRelatedItems;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleNoRelatedItems;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitleNoRelatedItems;

@end
