//
//  PlayerProgramCollectionViewCellVF.h
//  Vodafone
//
//  Created by Israel Berezin on 3/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerProgramCollectionViewCell.h"

@interface PlayerProgramCollectionViewCellVF : PlayerProgramCollectionViewCell

@property (nonatomic, strong)  UIButton *phoneLiveLikeButton;

@end
