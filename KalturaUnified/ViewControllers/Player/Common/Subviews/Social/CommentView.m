//
//  Comment.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 15.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CommentView.h"
#import "UIView+Additions.h"
#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"

#import <CoreText/CoreText.h>

NSString * const TVNCommentViewCancelBtnImageName = @"gray_btn";
const NSInteger TVNCommentViewMaximumCharsForTweet = 140;
const NSInteger TVNCommentViewOffsetHeightWhenKeyboardUp = 140;
const CGFloat TVNCommentViewMovingViewAnimateDuration = 0.3;
const CGFloat TVNCommentViewTextViewCornerRadius = 5.0;
const CGFloat TVNCommentViewCornerRadius = 3.0;
const CGFloat TVNCommentViewBorderWidth = 1.0;
const NSInteger TVNCommentViewBackSpaceChar = -8;

@interface CommentView ()
@property (nonatomic) NSInteger charLeft;
@end

@implementation CommentView

- (id)init {
    
    self = [[[NSBundle mainBundle] loadNibNamed:[Utils nibName:NSStringFromClass([CommentView class])]
                                          owner:nil
                                        options:nil] objectAtIndex:0];
    if(self) {
        
        if (isPad) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisappear) name:UIKeyboardWillHideNotification object:nil];
        }
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fbLogin) name:KFbNotification object:nil];

    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark Setup UI

- (void)layoutSubviews {
    self.backgroundColor = CS(@"303030");
    if (isPad) self.layer.borderColor = CS(@"4c4c4c").CGColor;
    self.layer.borderWidth = TVNCommentViewBorderWidth;
    self.layer.cornerRadius = TVNCommentViewCornerRadius;
    self.charLeft = TVNCommentViewMaximumCharsForTweet;
    [self setupCancelButtonUI];
    [self setupCommentsLabelUI];
    [self setupPostButtonUI];
    [self setupPostTextViewUI];
    [self setupSwitchesUI];
    [self setupCharLeftLabel];
    
    if (isPhone) {
        
        UIView *borderView = [_imageViewThumb superview];
        borderView.layer.cornerRadius = 1;
        borderView.layer.masksToBounds = YES;
        
        _imageViewThumb.layer.cornerRadius = 1;
        _imageViewThumb.layer.masksToBounds = YES;
        
        [Utils setLabelFont:_labelName bold:YES];
        
    }
    
    [super layoutSubviews];
}

- (void)updateImgThumb {
    
    if (isPhone) {
        
        [_imageViewThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:_mediaItem andSize:CGSizeMake(_imageViewThumb.frame.size.width * 2, _imageViewThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_2_3.png"]];
    
        _labelName.text = _mediaItem.name;
        
    }

    
}

- (void)prepareForShow {
    self.tvPostDetails.text = @"";
    [self updateImgThumb];
    self.btnPost.enabled = false;
}

- (void)setupCancelButtonUI {

    UIImage *btnCancelImage = [UIImage imageNamed:TVNCommentViewCancelBtnImageName];
    [self.btnCancel setBackgroundImage:[btnCancelImage stretchableImageWithLeftCapWidth:btnCancelImage.size.width / 2.0 topCapHeight:btnCancelImage.size.height / 2.0]forState:UIControlStateNormal];
    [self.btnCancel setTitle:LS(@"cancel") forState:UIControlStateNormal];
    [self.btnCancel setTitleColor:CS(@"bebebe") forState:UIControlStateNormal];
    [Utils setButtonFont:self.btnCancel bold:YES];
    
}

- (void)setupCommentsLabelUI {
    self.lblComments.text = LS(@"comment");
    [Utils setLabelFont:self.lblComments bold:YES];
    self.lblComments.textColor = [UIColor whiteColor];
}

- (void)setupPostButtonUI {
    
    UIImage *btnPostImage = [[UIImage imageNamed:TVNCommentViewCancelBtnImageName] imageTintedWithColor:APST.brandColor];
    
    [self.btnPost setBackgroundImage:[btnPostImage stretchableImageWithLeftCapWidth:btnPostImage.size.width / 2.0 topCapHeight:btnPostImage.size.height / 2.0]forState:UIControlStateNormal];
    
    [self.btnPost setTitle:LS(@"post") forState:UIControlStateNormal];
    [self.btnPost setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
    [Utils setButtonFont:self.btnPost bold:YES];
    self.btnPost.enabled = NO;
}

- (void)setupPostTextViewUI {
    self.tvPostDetails = [[UnderlineTextView alloc] init];
	[self.tvPostDetails setFrame:self.tvPostDeatailsContainer.bounds];
	self.tvPostDetails.delegate  = self;
	self.tvPostDetails.font = Font_Reg(20);
    self.tvPostDetails.layer.cornerRadius = TVNCommentViewTextViewCornerRadius;
    self.tvPostDetails.textColor = CS(@"bebebe");
    self.tvPostDetails.backgroundColor = CS(@"232323");
    [self.tvPostDetails setShowsVerticalScrollIndicator:NO];
	[self.tvPostDeatailsContainer addSubview:self.tvPostDetails];
}

- (void)setupSwitchesUI {
    
    [self.switchFacebook setOnTintColor:APST.brandColor];
    [self.switchFacebook setTintColor:CS(@"bebebe")];
    [self.switchFacebook setThumbTintColor:[UIColor whiteColor]];
    [self.switchFacebook setOn:[LoginM isFacebookUser]];
    
    [self.switchTweet setOnTintColor:APST.brandColor];
    [self.switchTweet setTintColor:CS(@"bebebe")];
    [self.switchTweet setThumbTintColor:[UIColor whiteColor]];
    self.switchTweet.on = NO;
    self.switchTweet.enabled = NO;
}

- (void)setupCharLeftLabel {
    self.lblCharLeft.textColor = CS(@"bebebe");
    [Utils setLabelFont:self.lblCharLeft bold:YES];
    self.lblCharLeft.text = [Utils getStringFromInteger: self.charLeft];
    self.lblCharLeft.hidden = !self.switchTweet.isOn;
}

#pragma mark Actions

- (IBAction)switchTweetValueChange:(id)sender {
    self.lblCharLeft.hidden = !self.switchTweet.isOn;
    [self updateCharLeftUI];
}


#pragma mark Logic UI

- (void)updateCharLeftUI {
    self.charLeft = TVNCommentViewMaximumCharsForTweet - (self.tvPostDetails.text.length);
    
    if (self.charLeft == TVNCommentViewMaximumCharsForTweet) {
        self.switchFacebook.enabled = NO;
        self.switchTweet.enabled = NO;
        [self.switchTweet setOn:NO];
        [self.switchFacebook setOn:NO];
        self.btnPost.enabled = NO;
    } else {
        self.switchFacebook.enabled = YES;
        self.switchTweet.enabled = YES;
        self.btnPost.enabled = YES;
        if (self.charLeft < 0) {
            self.switchTweet.enabled = NO;
            [self.switchTweet setOn:NO];
        }
    }
    self.lblCharLeft.hidden = !self.switchTweet.isOn;
    [self.lblCharLeft setText:[Utils getStringFromInteger: self.charLeft]];
}

#pragma mark TVNUnderlineTextView Delegates

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // To update lines of TVNUnderlineTextView
    [self.tvPostDetails setNeedsDisplay];
}

#pragma mark UITextView Delegates

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
   
    const char * _char = [text cStringUsingEncoding:NSUTF8StringEncoding];
    int character = strcmp(_char, "\b");
    
    if (TVNCommentViewBackSpaceChar != character && self.charLeft <= 0) {
        [self.switchTweet setOn:NO];
        [self.switchTweet setEnabled:NO];
    } else {
        if (TVNCommentViewBackSpaceChar == character && self.charLeft == -1) {
            [self.switchTweet setEnabled:YES];
        } else if (self.charLeft >= 0) {
            [self.switchTweet setEnabled:YES];
        }
    }
    self.lblCharLeft.hidden = !self.switchTweet.isOn;
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    [self updateCharLeftUI];
}

#pragma mark - Keyboard Notifications

- (void)keyboardWillAppear {
    [UIView animateWithDuration:TVNCommentViewMovingViewAnimateDuration animations:^ {
        [self setY:self.y - TVNCommentViewOffsetHeightWhenKeyboardUp];
    }];
}

- (void)keyboardWillDisappear {
    [UIView animateWithDuration:TVNCommentViewMovingViewAnimateDuration animations:^ {
        [self setY:self.y + TVNCommentViewOffsetHeightWhenKeyboardUp];
    }];
}

#pragma mark - FB login

- (void)fbLogin {
    [_switchFacebook setOn:[LoginM isFacebookUser]];
}

@end
