//
//  SocialBuzzCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SocialBuzzCell.h"
#import "SocialBuzzSubCell.h"
#import "UIImage+Tint.h"
#import "PlayerSocialView.h"
#import "UIImageView+WebCache.h"
#import <TvinciSDK/TVSocialFeedComment.h>

@interface SocialBuzzCell ()

@property (nonatomic, strong) IBOutlet UIView *userNameAndshareType;
@property (nonatomic, strong) UITableView *tblSubFeeds;
@property (nonatomic) int commentsCount;

@end

const NSInteger SocialBuzzCellH = 88;

#define SocialBuzzSubCellSizeWidth      (isPad ? 650 : 294)
#define SocialBuzzSubCellSizeHeight     88

#define SocialBuzzCellUserNameFontSize   (isPad ? 16 : 14)
#define SocialBuzzCellFeedTextFontSize   (isPad ? 15 : 12)
#define SocialBuzzCellFeedDateFontSize   (isPad ? 11 : 10)

#define SocialBuzzCellFeedTextWidth      (isPad ? 485 : 212)
#define SocialBuzzCellFeedTextHeightMin  (isPad ? 38 : 48)

const NSInteger SocialBuzzCellNumSubFeedsFontSize = 12;
const NSInteger SocialBuzzCellShareIconLoadingSpace = 8;
NSString * const SocialBuzzCellFacebookIconImageName = @"social_buzz_facebook_icon";
NSString * const SocialBuzzCellTwitterIconImageName = @"social_buzz_twitter_icon";

/*
NSString * const SocialBuzzCellTempUserNameKey = @"userNameKey";
NSString * const SocialBuzzCellTempFeedTextKey = @"feedTextKey";
NSString * const SocialBuzzCellTempFeedDateKey = @"feedDateKey";
*/

const NSInteger SocialBuzzFooterViewH = 30;

@implementation SocialBuzzCell

- (void)awakeFromNib {
    self.feedText.font = Font_Reg(SocialBuzzCellFeedTextFontSize);
    self.feedDate.font = Font_RegItalic(SocialBuzzCellFeedDateFontSize);
    self.subFeedsNum.font = Font_Reg(SocialBuzzCellNumSubFeedsFontSize);
    
    self.cornerImage.image = [[UIImage imageNamed:@"social_buzz_corner"] imageTintedWithColor:APST.brandColor];
    self.viewCornerImage.layer.cornerRadius = self.viewCornerImage.frame.size.width / 2.0;
    self.viewCornerImage.layer.masksToBounds = YES;
    self.viewCornerImage.layer.borderColor = [UIColor grayColor].CGColor;
    self.viewCornerImage.layer.borderWidth = 1.5f;
    
    self.feedText.numberOfLines = 0;
    
    _feedWebView.scrollView.scrollEnabled = NO;
    _feedWebView.scrollView.bounces = NO;
    
}

- (void)dealloc {
    
    _feedWebView.delegate = nil;
    [_feedWebView stopLoading];
    
}

- (void)setUserName:(NSString *)userName andShareType:(SocialBuzzShareType)type {
    
    UIFont *userNameFont = Font_Bold(SocialBuzzCellUserNameFontSize);
    
    CGSize userNameSize = [userName sizeWithAttributes:@{NSFontAttributeName:userNameFont}];
    UILabel *lblUserName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, MIN(userNameSize.width, 200.0), userNameSize.height)];
    lblUserName.backgroundColor = [UIColor clearColor];
    lblUserName.textColor = [UIColor colorWithWhite:0.8 alpha:1.0];
    lblUserName.font = userNameFont;
    lblUserName.text = userName;
    [self.userNameAndshareType addSubview:lblUserName];
    
    NSString *shareImageName = @"";
    if (type == SocialBuzzShareTypeFacebook) {
        shareImageName = SocialBuzzCellFacebookIconImageName;
    } else if (type == SocialBuzzShareTypeTwitter) {
        shareImageName = SocialBuzzCellTwitterIconImageName;
    }
    if (shareImageName.length) {
        UIImageView *shareIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:shareImageName]];
        CGFloat shareIconX = lblUserName.frame.size.width + SocialBuzzCellShareIconLoadingSpace;
        CGSize shareIconImgSize = shareIcon.image.size;
        CGFloat shareIconY = (CGRectGetHeight(_userNameAndshareType.frame) - shareIconImgSize.height) / 2;
        shareIcon.frame = CGRectMake(shareIconX, shareIconY - 2, shareIconImgSize.width, shareIconImgSize.height);
        [self.userNameAndshareType addSubview:shareIcon];
    }
}

- (void)updateUIForFeedText {
    
    _feedText.numberOfLines = 0;
    //CGSize size = [_feedText.text sizeWithFont:Font_Reg(SocialBuzzCellFeedTextFontSize) constrainedToSize:CGSizeMake(SocialBuzzCellFeedTextWidth, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize size = [[NSString stringWithFormat:@"%@\n ", _feedText.text] boundingRectWithSize:CGSizeMake(SocialBuzzCellFeedTextWidth, MAXFLOAT)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:Font_Reg(SocialBuzzCellFeedTextFontSize)}
                                               context:nil].size;
    
    int textHeight = MAX(SocialBuzzCellFeedTextHeightMin, size.height);
    _feedText.frame = CGRectMake(_feedText.frame.origin.x, _feedText.frame.origin.y, SocialBuzzCellFeedTextWidth, textHeight);
    _feedWebView.frame = _feedText.frame;
    
    int cellHeight = _feedText.frame.origin.y + textHeight + (isPad ? 13 : 6);
    
    _viewDivider.frame = CGRectMake(_viewDivider.frame.origin.x, cellHeight - 1, _viewDivider.frame.size.width, _viewDivider.frame.size.height);
    _cornerImage.frame = CGRectMake(_cornerImage.frame.origin.x, cellHeight - (isPad ? 37 : 31), _cornerImage.frame.size.width, _cornerImage.frame.size.height);
    _subFeedsNum.frame = CGRectMake(_subFeedsNum.frame.origin.x, cellHeight - (isPad ? 19 : 17), _subFeedsNum.frame.size.width, _subFeedsNum.frame.size.height);
    
    BOOL hasLinks = (([_feedText.text rangeOfString:@"http://"].location != NSNotFound) || ([_feedText.text rangeOfString:@"https://"].location != NSNotFound));
    if (hasLinks) {
        _feedText.hidden = YES;
        _feedWebView.userInteractionEnabled = hasLinks;
        if (_feedText.text != nil)
        {
            [_feedWebView loadHTMLString:[Utils convertTextToHTML:_feedText.text] baseURL:nil];
            _feedWebView.hidden = NO;
        }
        
        
        
    } else {
        
        _feedText.hidden = NO;
        _feedWebView.hidden = YES;
    }
    
}

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if (inType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

+ (float)getCellHeightForFeedText:(NSString *)text {
    
    //CGSize size = [text sizeWithFont:Font_Reg(SocialBuzzCellFeedTextFontSize) constrainedToSize:CGSizeMake(SocialBuzzCellFeedTextWidth, 10000.0) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize size = [[NSString stringWithFormat:@"%@\n ", text] boundingRectWithSize:CGSizeMake(SocialBuzzCellFeedTextWidth, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName:Font_Reg(SocialBuzzCellFeedTextFontSize)}
                                     context:nil].size;
    
    int textHeight = MAX(SocialBuzzCellFeedTextHeightMin, size.height);
    
    return textHeight + (isPad ? 50 : 40);
    
}

#pragma mark - open state - subFeeds

- (void)addFeedsToCell:(int)count {
    
    self.commentsCount = count;
    
    [self tableViewConfiguration];
    [self addSubview:_tblSubFeeds];
}

- (void)removeFeedsFromCell {
    
    self.commentsCount = 0;
    
    if (_tblSubFeeds) {
        self.tblSubFeeds = nil;
        [self.tblSubFeeds removeFromSuperview];
    }
}

- (void)tableViewConfiguration {

    int subCellHeight = 0;
    
    for (int i = 0; i < self.commentsCount; i++) {
        
        TVSocialFeedComment *subFeed = _subFeeds[i];
    
        subCellHeight = subCellHeight + [SocialBuzzSubCell getCellHeightForFeedText:subFeed.commentBody];
        
    }
    
    
    NSInteger tblHeight = subCellHeight + (self.commentsCount < _subFeeds.count ? SocialBuzzFooterViewH : 0);
    
    NSInteger count = _subFeeds.count - self.commentsCount;
    count = MIN(count, 3);
    
    NSString *title = [NSString stringWithFormat:@"%@ %ld %@", LS(@"socialBuzz_LoadMoreBegin"), (long)count, LS(@"socialBuzz_LoadMoreEnd")];
    
    int cellHeight = [SocialBuzzCell getCellHeightForFeedText:_feedText.text];
    
    if (self.tblSubFeeds) {
        
        self.tblSubFeeds.frame = CGRectMake(self.tblSubFeeds.frame.origin.x, self.tblSubFeeds.frame.origin.y,
                                            self.tblSubFeeds.frame.size.width, tblHeight);
        
        if (count > 0) {
            
            UIView *viewFooter = self.tblSubFeeds.tableFooterView;
            if (viewFooter && [[viewFooter subviews] count] > 0) {
                
                UIButton *button = [[viewFooter subviews] objectAtIndex:0];
                
                if (button && [button isKindOfClass:[UIButton class]]) {
                    [button setTitle:title forState:UIControlStateNormal];
                    
                }
                
            }
        } else {
            
            self.tblSubFeeds.tableFooterView = nil;
            
        }
    } else {
        
        self.tblSubFeeds = [[UITableView alloc] initWithFrame:CGRectMake(0, cellHeight, SocialBuzzSubCellSizeWidth, tblHeight)
                                                        style:UITableViewStylePlain];
        self.tblSubFeeds.backgroundColor = [UIColor clearColor];
        self.tblSubFeeds.dataSource = self;
        self.tblSubFeeds.delegate = self;
        self.tblSubFeeds.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tblSubFeeds.scrollEnabled = NO;
        
        if (self.commentsCount < _subFeeds.count) {
            
            if (count > 0) {
                
                UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SocialBuzzSubCellSizeWidth, SocialBuzzFooterViewH)];
                viewFooter.backgroundColor = [UIColor colorWithWhite:0.325 alpha:1.0];
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = CGRectMake(0, 0, viewFooter.frame.size.width, viewFooter.frame.size.height);
                
                
                [button setTitle:title forState:UIControlStateNormal];
                [button setTitleColor:[UIColor colorWithWhite:0.63 alpha:1.0] forState:UIControlStateNormal];
                [button setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
                [button.titleLabel setFont:Font_RegItalic(SocialBuzzCellFeedDateFontSize + 2)];
                button.tag = self.tag;
                [button addTarget:self.socialBuzzView action:@selector(loadMoreAction:) forControlEvents:UIControlEventTouchUpInside];
                [viewFooter addSubview:button];
                
                self.tblSubFeeds.tableFooterView = viewFooter;
            }
            
        }
        
    }
    
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.commentsCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TVSocialFeedComment *feed = _subFeeds[indexPath.row];
    
    int cellHeight = [SocialBuzzSubCell getCellHeightForFeedText:feed.commentBody];
    
    return cellHeight;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString *SocialBuzzSubCellIdentifier = @"SocialBuzzSubCellIdentifier";
    SocialBuzzSubCell *socialBuzzSubCell = [tableView dequeueReusableCellWithIdentifier:SocialBuzzSubCellIdentifier];
    if (socialBuzzSubCell == nil) {
        NSArray *views = [[NSBundle mainBundle] loadNibNamed:[Utils nibName:NSStringFromClass([SocialBuzzSubCell class])] owner:self options:nil]; 
        socialBuzzSubCell =  [views objectAtIndex:0];
    }
    socialBuzzSubCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    TVSocialFeedComment *feed = _subFeeds[indexPath.row];
    socialBuzzSubCell.userName.text = feed.creatorName;
    socialBuzzSubCell.feedText.text = feed.commentBody;
    [socialBuzzSubCell updateUIForFeedText];
    
    socialBuzzSubCell.feedDate.text = [Utils getActionDurationTimeFromNow:feed.createDate];
    
    NSURL *strURL = feed.creatorUrl;
    if (strURL) {
        
        [socialBuzzSubCell.avatarImage sd_setImageWithURL:strURL placeholderImage:[UIImage imageNamed:@"avatar"]];
        
    } else {
        
        socialBuzzSubCell.avatarImage.image = [UIImage imageNamed:@"avatar"];
        
    }
    
    socialBuzzSubCell.backgroundColor = [UIColor clearColor];
    socialBuzzSubCell.backgroundView = [UIView new];
    socialBuzzSubCell.selectedBackgroundView = [UIView new];
    
    return socialBuzzSubCell;
}

@end
