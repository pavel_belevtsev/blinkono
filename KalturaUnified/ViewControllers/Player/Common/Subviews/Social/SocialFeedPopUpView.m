//
//  SocialFeedPopUpView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SocialFeedPopUpView.h"
#import "UIImage+Tint.h"
#import "PlayerSocialView.h"

const CGFloat TVNSocialFeedPopUpViewHeightCell = 44.0f;
const int TVNSocialFeedPopUpViewNumberOfDisplayItems = 4;
NSString * const TVNSocialFeedPopUpViewBubbleLeft =  @"showpage_share_bubble_left";
NSString * const TVNSocialFeedPopUpViewBubbleRight =  @"showpage_share_bubble_right";
NSString * const TVNSocialFeedPopUpViewBubbleMiddle =  @"showpage_share_bubble_mid";
const CGFloat TVNSocialFeedPopUpViewBottomSpace= 16.0f;

@interface SocialFeedPopUpView ()

@property (nonatomic, strong) NSArray *filtersData;

@end

@implementation SocialFeedPopUpView

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    if (self) {
        self.isDismissAutomatically = NO;
    }
    return self;
}

- (void)awakeFromNib {
    UIImage *image = [UIImage imageNamed:TVNSocialFeedPopUpViewBubbleLeft];
    UIImage *sizeableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(40, 18, 20, 0)];
    self.imgLeft.image =sizeableImage;
    
    image = [UIImage imageNamed:TVNSocialFeedPopUpViewBubbleMiddle];
    sizeableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(40, 0, 20, 0)];
    self.imgMiddle.image =sizeableImage;
    
    image = [UIImage imageNamed:TVNSocialFeedPopUpViewBubbleRight];
    sizeableImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(40, 0, 20, 18)];
    self.imgRight.frame = CGRectMake(CGRectGetMaxX(self.imgMiddle.frame), CGRectGetMinY(self.imgRight.frame), CGRectGetWidth(self.imgRight.frame), CGRectGetHeight(self.imgRight.frame));
    self.imgRight.image =sizeableImage;
    
    self.filtersData = @[@{@"FilterImage": @"btn_all_feed_all_normal",
                           @"FilterTitle": LS(@"socialBuzz_AllFeeds"),
                           @"FilterType": @"0"},
                         
                         @{@"FilterImage": @"btn_all_feed_comments_normal",
                           @"FilterTitle": LS(@"socialBuzz_Comments"),
                           @"FilterType": @"1"},
                         
                         @{@"FilterImage": @"btn_all_feed_facebook_normal",
                           @"FilterTitle": LS(@"socialBuzz_Facebook"),
                           @"FilterType": @"2"},
                         
                         @{@"FilterImage": @"btn_all_feed_twitter_normal",
                           @"FilterTitle": LS(@"socialBuzz_Twitter"),
                           @"FilterType": @"3"}];
    
    self.socialTypeTableView.layer.cornerRadius = 3.0f;
    [self.socialTypeTableView setTableFooterView:[UIView new]];
    
    [self.socialTypeTableView reloadData];
    
}

- (void)setSelectedFilterIndex:(NSInteger)index {
    
    [self.socialTypeTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    
}

- (void)updateButtons:(NSArray *)array {
    
    self.arrayCounts = array;

    [self.socialTypeTableView reloadData];
    
}

#pragma mark - UITableView data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return TVNSocialFeedPopUpViewHeightCell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _filtersData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        UIImage *bgimg = [[UIImage imageNamed:@"settings_sidebar_shadow"] stretchableImageWithLeftCapWidth:6 topCapHeight:4];
        cell.backgroundView = [[UIImageView alloc] initWithImage:bgimg];
        
    }
    
    BOOL buttonEnable = NO;
    
    if (self.arrayCounts && indexPath.row < [self.arrayCounts count]) {
        buttonEnable = ([[self.arrayCounts objectAtIndex:indexPath.row] intValue] > 0);
    }
    
    if (buttonEnable) {
        UIView *selectedBGView = [[UIView alloc] init];
        selectedBGView.backgroundColor = APST.brandColor;
        cell.selectedBackgroundView = selectedBGView;
        
        NSDictionary *item = [_filtersData objectAtIndex:indexPath.row];
        
        cell.textLabel.textColor = CS(@"bebebe");
        cell.textLabel.highlightedTextColor = APST.brandTextColor;
        cell.textLabel.font = Font_Bold(18);
        cell.textLabel.text = [item objectForKey:@"FilterTitle"];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        NSString *imageName = [item objectForKey:@"FilterImage"];
        [cell.imageView setImage:[UIImage imageNamed:imageName]];
        [cell.imageView setHighlightedImage:[[UIImage imageNamed:imageName] imageTintedWithColor:APST.brandTextColor]];
    
        cell.userInteractionEnabled = YES;
        
    } else {
        
        UIView *selectedBGView = [[UIView alloc] init];
        selectedBGView.backgroundColor = [UIColor clearColor];
        cell.selectedBackgroundView = selectedBGView;
        
        NSDictionary *item = [_filtersData objectAtIndex:indexPath.row];
        
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.highlightedTextColor = [UIColor darkGrayColor];
        cell.textLabel.font = Font_Bold(18);
        cell.textLabel.text = [item objectForKey:@"FilterTitle"];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        NSString *imageName = [item objectForKey:@"FilterImage"];
        [cell.imageView setImage:[[UIImage imageNamed:imageName] imageTintedWithColor:[UIColor darkGrayColor]]];
        [cell.imageView setHighlightedImage:[[UIImage imageNamed:imageName] imageTintedWithColor:[UIColor darkGrayColor]]];
        
        cell.userInteractionEnabled = NO;
    }
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    BOOL buttonEnable = NO;
    
    if (self.arrayCounts && indexPath.row < [self.arrayCounts count]) {
        buttonEnable = ([[self.arrayCounts objectAtIndex:indexPath.row] intValue] > 0);
    }
    
    if (buttonEnable) {
        NSDictionary *item = [_filtersData objectAtIndex:indexPath.row];
        [self.socialBuzzView updateMediaFeedWithFilter:item];
    }
    
}

- (void)setViewForNumItems:(NSInteger)numItems{
    CGFloat heightTable;
    if (numItems<TVNSocialFeedPopUpViewNumberOfDisplayItems) {
        heightTable = numItems*TVNSocialFeedPopUpViewHeightCell;
    } else{
        heightTable = TVNSocialFeedPopUpViewHeightCell*TVNSocialFeedPopUpViewNumberOfDisplayItems;
    }

    self.socialTypeTableView.frame = CGRectMake(CGRectGetMinX(self.socialTypeTableView.frame), CGRectGetMinY(self.socialTypeTableView.frame), CGRectGetWidth(self.socialTypeTableView.frame),heightTable-2);
   
    CGFloat totalHeightPopUp = CGRectGetMaxY(self.socialTypeTableView.frame) + TVNSocialFeedPopUpViewBottomSpace;
    self.imgRight.frame = CGRectMake(CGRectGetMinX(self.imgRight.frame), CGRectGetMinY(self.imgRight.frame), CGRectGetWidth(self.imgRight.frame), totalHeightPopUp);
    self.imgLeft.frame = CGRectMake(CGRectGetMinX(self.imgLeft.frame), CGRectGetMinY(self.imgLeft.frame), CGRectGetWidth(self.imgLeft.frame), totalHeightPopUp);
    self.imgMiddle.frame = CGRectMake(CGRectGetMinX(self.imgMiddle.frame), CGRectGetMinY(self.imgMiddle.frame), CGRectGetWidth(self.imgMiddle.frame), totalHeightPopUp);
    self.frame = CGRectMake(CGRectGetMinX(self.frame), CGRectGetMinY(self.frame), CGRectGetWidth(self.frame), totalHeightPopUp);
    self.popUpRectView = self.frame;
}

@end