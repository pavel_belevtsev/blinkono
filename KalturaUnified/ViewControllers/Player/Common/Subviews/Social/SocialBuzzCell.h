//
//  SocialBuzzCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlayerSocialView;

typedef NS_ENUM(NSInteger, SocialBuzzShareType) {
    SocialBuzzShareTypeTvinci,
    SocialBuzzShareTypeFacebook,
    SocialBuzzShareTypeTwitter
};

@protocol SocialBuzzCellDelegate <NSObject>

- (void)reloadSelectFeedAtIndex:(NSInteger)selectedIndex andState:(BOOL)state;

@end

@interface SocialBuzzCell : UITableViewCell <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *avatarImage;
@property (nonatomic, weak) IBOutlet UILabel *feedText;
@property (nonatomic, weak) IBOutlet UIWebView *feedWebView;
@property (nonatomic, weak) IBOutlet UILabel *feedDate;
@property (nonatomic, weak) IBOutlet UILabel *subFeedsNum;
@property (nonatomic, weak) IBOutlet UIImageView *cornerImage;
@property (nonatomic, weak) IBOutlet UIView *viewCornerImage;
@property (nonatomic, weak) IBOutlet UIView *viewDivider;

@property (nonatomic, weak) id <SocialBuzzCellDelegate> delegate;
@property (nonatomic) BOOL isSelected;
@property (nonatomic, strong) NSArray *subFeeds;
@property (nonatomic) BOOL isOpened;

@property (nonatomic, weak) PlayerSocialView *socialBuzzView;

- (void)setUserName:(NSString *)userName andShareType:(SocialBuzzShareType)type;
- (void)addFeedsToCell:(int)count;
- (void)removeFeedsFromCell;
- (void)updateUIForFeedText;

+ (float)getCellHeightForFeedText:(NSString *)text;

@end
