//
//  SocialNoneCommentsView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerSocialView.h"

@interface SocialNoneCommentsView : UIView
{
    
    IBOutlet UILabel *topLabel;
    IBOutlet UILabel *bottomLabel;
    IBOutlet UIButton *button;
}

- (void)setViewWithFirstText:(NSString *)firstString secondText:(NSString *)secondString buttonImageName:(NSString *)buttonImageName buttonText:(NSString *)buttonString;

- (IBAction)footerButtonTapped:(id)sender;
- (IBAction)footerButtonTouchDown:(id)sender;
- (IBAction)footerButtonTouchUpOutside:(id)sender;

@property (nonatomic, weak) PlayerSocialView *delegateView;

@end
