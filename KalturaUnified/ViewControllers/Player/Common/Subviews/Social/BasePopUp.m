//
//  BasePopUp.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BasePopUp.h"

@interface BasePopUp()

@property (nonatomic, strong) NSTimer * timerForDismiss;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic) BOOL autoDismiss;

@end

const CGFloat TVNBasePopUpTimerTimeInterval = 6.0f;
const float TVNBasePopUpDurationAnimation = 0.3;
const CGFloat TVNBasePopUpViewYTranslation = 30.0;

@implementation BasePopUp

#pragma mark - private functions
- (void)dismissPopUpAutomatically {
    self.autoDismiss = YES;
    [self dismissPopUp];
}

#pragma mark - public functions

- (void)dismissPopUp {
    if (!self.superview) {
        return;
    }
    [self removeSelfView];
}

- (void)showPopUp {
    if (!self.tapGesture) {
        self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resetTimer)];
        self.tapGesture.cancelsTouchesInView = NO;
        [self addGestureRecognizer:self.tapGesture];
    }
    [self addSelfView];
}

#pragma mark - animations
- (CGRect)getRectViewAnimation {
    CGRect rect;
    if (self.popUpAnimationType == TVNPopUpAnimationTypeDownToUp) {
        rect = CGRectMake(CGRectGetMinX(self.popUpRectView), CGRectGetMinY(self.popUpRectView) + TVNBasePopUpViewYTranslation, CGRectGetWidth(self.popUpRectView), CGRectGetHeight(self.popUpRectView));
    }
    if (self.popUpAnimationType == TVNPopUpAnimationTypeUpToDown) {
        rect = CGRectMake(CGRectGetMinX(self.popUpRectView), CGRectGetMinY(self.popUpRectView) - TVNBasePopUpViewYTranslation, CGRectGetWidth(self.popUpRectView), CGRectGetHeight(self.popUpRectView));
    }
    return rect;
}

- (void)removeSelfView {
    [self stopTimer];
    CGRect rectViewAnimation = [self getRectViewAnimation];
    [UIView animateWithDuration:TVNBasePopUpDurationAnimation delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = rectViewAnimation;
        self.alpha = 0.0f;
    } completion:^(BOOL finished){
        if ([self.popUpDelegate respondsToSelector:@selector(didDismissPopUp:autoDismiss:)]) {
            [self.popUpDelegate didDismissPopUp:self autoDismiss:self.autoDismiss];
        }
        [self removeFromSuperview];
    }];
}

- (void)addSelfView {
    self.autoDismiss = NO;
    CGRect rectViewAnimation = [self getRectViewAnimation];
    self.frame = rectViewAnimation;
    [self.popUpSuperView addSubview:self];
    self.alpha = 0.0f;
    
    if ([self.popUpDelegate respondsToSelector:@selector(didAddPopUp:)]) {
        [self.popUpDelegate didAddPopUp:self];
    }
    
    [UIView animateWithDuration:TVNBasePopUpDurationAnimation delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = self.popUpRectView;
        self.alpha = 1.0f;
    } completion:^(BOOL finished){
        [self resetTimer];
        if ([self.popUpDelegate respondsToSelector:@selector(didEndAddPopUp:)]) {
            [self.popUpDelegate didEndAddPopUp:self];
        }
    }];
}

#pragma mark - Timer functions
- (void)resetTimer {
    if (self.isDismissAutomatically) {
        [self stopTimer];
        NSLog(@"PopUp reset");

        self.timerForDismiss = [NSTimer scheduledTimerWithTimeInterval:TVNBasePopUpTimerTimeInterval target:self selector:@selector(dismissPopUpAutomatically) userInfo:nil repeats:NO];
    }
}

- (void)stopTimer {
    NSLog(@"PopUp stop");

    if (self.isDismissAutomatically && self.timerForDismiss) {
        [self.timerForDismiss invalidate];
        self.timerForDismiss = nil;
    }
}

- (BOOL)shouldStopTimer{
    return YES;
}

#pragma mark - touches functions
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [self resetTimer];
}

@end
