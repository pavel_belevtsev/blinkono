//
//  SocialFeedPopUpView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePopUp.h"

@class PlayerSocialView;

extern const CGFloat TVNSocialFeedPopUpViewHeightCell;

@interface SocialFeedPopUpView : BasePopUp

@property (assign) BOOL isShowing;
@property (nonatomic, weak) PlayerSocialView *socialBuzzView;

@property (weak, nonatomic) IBOutlet UITableView *socialTypeTableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBg;
@property (weak, nonatomic) IBOutlet UIImageView *imgLeft;
@property (weak, nonatomic) IBOutlet UIImageView *imgRight;
@property (weak, nonatomic) IBOutlet UIImageView *imgMiddle;
@property (nonatomic, strong) NSArray *arrayCounts;

- (void)setViewForNumItems:(NSInteger)numItems;
- (void)setSelectedFilterIndex:(NSInteger)index;
- (void)updateButtons:(NSArray *)array;

@end
