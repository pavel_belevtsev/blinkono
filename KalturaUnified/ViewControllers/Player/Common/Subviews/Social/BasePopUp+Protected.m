//
//  BasePopUp+Protected.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BasePopUp+Protected.h"

@implementation BasePopUp (Protected)
- (void)prepareToDrive {
    //NSLog(@"Doing some internal work to get the %@ ready to drive");
}
@end
