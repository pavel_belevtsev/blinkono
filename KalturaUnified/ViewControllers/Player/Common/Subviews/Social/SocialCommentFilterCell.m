//
//  SocialCommentFilterCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SocialCommentFilterCell.h"
#import "UIImage+Tint.h"

@implementation SocialCommentFilterCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    
    self.filterIcon.highlighted = highlighted;
    
    if (highlighted)
    {
        self.filterName.textColor= APST.brandColor;
    }
    else
    {
        if (self.userInteractionEnabled) {
            self.filterName.textColor = APST.brandTextColor;
        } else {
            self.filterName.textColor = [UIColor colorWithWhite:0.5 alpha:1.0];
        }
        
    }
}

- (void)colorFilterToColor:(UIColor *)color
{
    for ( UIView *view in [self.contentView subviews] )
    {
        if ( [view isKindOfClass:[UIImageView class]] )
        {
            UIImageView *imageView = (UIImageView *)view;
            imageView.image = [imageView.image imageTintedWithColor:color];
        }
    }
}

- (void)changeFilterIcon:(UIImage *)image
{
    UIImageView *filterIcon = [[UIImageView alloc] initWithImage:image];
    filterIcon.frame = CGRectMake(11, (self.frame.size.height - filterIcon.frame.size.height) / 2, filterIcon.frame.size.width, filterIcon.frame.size.height);
    [self addSubview:filterIcon];
}

@end
