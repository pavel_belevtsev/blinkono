
//  UnderlineTextView
//
//  Created by Naveed Ahsan on 10/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UnderlineTextViewDelegate <UITextViewDelegate>

@end

@interface UnderlineTextView : UITextView <UIScrollViewDelegate>{

    
}

@property (nonatomic,assign) id<UnderlineTextViewDelegate> delegate;

@end
