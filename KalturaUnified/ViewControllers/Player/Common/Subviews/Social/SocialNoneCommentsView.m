//
//  NoneCommentsView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SocialNoneCommentsView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Tint.h"

@implementation SocialNoneCommentsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    button.backgroundColor = APST.brandColor;
    button.layer.cornerRadius = 5.0f;
}

- (void)setViewWithFirstText:(NSString *)firstString secondText:(NSString *)secondString buttonImageName:(NSString *)buttonImageName buttonText:(NSString *)buttonString
{
    topLabel.text= firstString;
    [Utils setLabelFont:topLabel bold:YES];
    
    bottomLabel.text= secondString;
    [Utils setLabelFont:bottomLabel bold:YES];
    
    [button setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
    [button setTitle:LS(@"comment") forState:UIControlStateNormal];
    [button setImage:[[UIImage imageNamed:@"details_add_comment_icon"] imageTintedWithColor:APST.brandTextColor] forState:UIControlStateNormal];

}

-(IBAction)footerButtonTapped:(id)sender {
    
    button.backgroundColor = APST.brandColor;
    
    if (self.delegateView) {
        [self.delegateView buttonCommentPressed:nil];
    }
    
}

- (IBAction)footerButtonTouchDown:(id)sender {
    
    button.backgroundColor = APST.brandDarkColor;
    
}

- (IBAction)footerButtonTouchUpOutside:(id)sender {
    
    button.backgroundColor = APST.brandColor;
}

@end
