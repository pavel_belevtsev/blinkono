//
//  BasePopUp+Protected.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BasePopUp.h"

@interface BasePopUp (Protected)
- (void)prepareToDrive ;
@end
