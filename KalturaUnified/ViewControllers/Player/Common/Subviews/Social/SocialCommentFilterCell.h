//
//  SocialCommentFilterCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialCommentFilterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *filterName;
@property (weak, nonatomic) IBOutlet UIImageView *filterIcon;

- (void)changeFilterIcon:(UIImage *)image;

@end
