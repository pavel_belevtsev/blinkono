//
//  BasePopUp.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,TVNPopUpType) {
    TVNPopUpSound,
    TVNPopUpSocialShare,
};

typedef NS_ENUM(NSInteger,TVNPopUpAnimationType) {
    TVNPopUpAnimationTypeUpToDown,
    TVNPopUpAnimationTypeDownToUp,
};

@protocol BasePopUpDelegate;

@interface BasePopUp : UIView

@property (nonatomic, weak) id <BasePopUpDelegate>popUpDelegate;
@property (nonatomic) TVNPopUpAnimationType popUpAnimationType;
@property (nonatomic, strong) UIView *popUpSuperView;
@property (nonatomic) CGRect popUpRectView;
@property (nonatomic) BOOL isDismissAutomatically;

- (void)dismissPopUp;
- (void)showPopUp;

- (void)resetTimer;
- (void)stopTimer;
@end

@protocol BasePopUpDelegate <NSObject>

@optional
- (void)didAddPopUp:(BasePopUp*)popUp;
- (void)didEndAddPopUp:(BasePopUp*)popUp;
- (void)didDismissPopUp:(BasePopUp*)popUp autoDismiss:(BOOL)autoDismiss;

@end