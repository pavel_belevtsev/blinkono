//  TVNUnderlineTextView
//
//  Created by Naveed Ahsan on 10/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UnderlineTextView.h"

const CGFloat TVNUnderlineTextViewLineWidth = 1.0f;
const CGFloat TVNUnderlineTextViewBaseLineOffset = 7.0f;

@implementation UnderlineTextView

- (void)drawRect:(CGRect)rect {
    
    //Get the current drawing context   
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Set the line color and width
    CGContextSetStrokeColorWithColor(context, CS(@"4f4f4f").CGColor);
    CGContextSetLineWidth(context, TVNUnderlineTextViewLineWidth);
    
    
    //Start a new Path
    CGContextBeginPath(context);
	
    //Find the number of lines in our textView + add a bit more height to draw lines in the empty part of the view
    NSUInteger numberOfLines = (self.contentSize.height + self.bounds.size.height) / self.font.leading;
	
    //iterate over numberOfLines and draw each line
    for (NSUInteger x = 1; x < numberOfLines; x++) {
        
        //0.5f offset lines up line with pixel boundary
        CGContextMoveToPoint(context, self.bounds.origin.x, self.font.leading*x + 0.5f + TVNUnderlineTextViewBaseLineOffset);
        CGContextAddLineToPoint(context, self.bounds.size.width, self.font.leading*x + 0.5f + TVNUnderlineTextViewBaseLineOffset);
    }
	
    //Close our Path and Stroke (draw) it
    CGContextClosePath(context);
    CGContextStrokePath(context);
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.delegate performSelector:@selector(scrollViewDidScroll:) withObject:scrollView];
    }
  [self setNeedsDisplay];
}

@end
