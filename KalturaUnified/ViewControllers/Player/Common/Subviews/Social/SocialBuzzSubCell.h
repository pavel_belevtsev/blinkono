//
//  SocialBuzzSubCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialBuzzSubCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *avatarImage;
@property (nonatomic, weak) IBOutlet UILabel *userName;
@property (nonatomic, weak) IBOutlet UILabel *feedText;
@property (nonatomic, weak) IBOutlet UIWebView *feedWebView;
@property (nonatomic, weak) IBOutlet UILabel *feedDate;
@property (nonatomic, weak) IBOutlet UIView *viewCornerImage;
@property (nonatomic, weak) IBOutlet UIView *viewDivider;

- (void)updateUIForFeedText;

+ (float)getCellHeightForFeedText:(NSString *)text;

@end
