//
//  Comment.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 15.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnderlineTextView.h"

@interface CommentView : UIView <UnderlineTextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblComments;
@property (weak, nonatomic) IBOutlet UIButton *btnPost;
@property (strong, nonatomic) UnderlineTextView *tvPostDetails;
@property (weak, nonatomic) IBOutlet UISwitch *switchFacebook;
@property (weak, nonatomic) IBOutlet UITextView *tvPostDeatailsContainer;

@property (weak, nonatomic) IBOutlet UILabel *lblCharLeft;
@property (weak, nonatomic) IBOutlet UISwitch *switchTweet;

@property (nonatomic, strong) TVMediaItem *mediaItem;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewThumb;
@property (weak, nonatomic) IBOutlet UILabel *labelName;

- (void)updateImgThumb;
- (void)prepareForShow;

@end
