//
//  SocialHeaderForFilterView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SocialHeaderForFilterView.h"
#import "UIImage+Tint.h"

@interface SocialHeaderForFilterView ()
{
    BOOL opened;
}

@end

@implementation SocialHeaderForFilterView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)awakeFromNib
{
    opened= NO;
    [Utils setLabelFont:filterName bold:YES];
    
    // details_add_comment_icon.png
    
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
    
}

- (void)skinChanged {
    
    commentButton.backgroundColor= APST.brandColor;
    
    [commentButton setImage:[[UIImage imageNamed:@"details_add_comment_icon"] imageTintedWithColor:APST.brandTextColor] forState:UIControlStateNormal];
    
}


- (void)setIconAndNameFilter:(UIImage *)image name:(NSString *)name
{
    if (opened)
    {
        opened= NO;
    }
    UIColor *color = [UIColor colorWithWhite:0.5 alpha:1.0];
    
    if (filterEnabled) {
        color = APST.brandTextColor;
        
    }
    
    filterImage.image= [image imageTintedWithColor:color];
    filterName.text= name;
}

- (void)setFilterEnabled:(BOOL)isEnabled {

    filterEnabled = isEnabled;
    
    UIColor *color = [UIColor colorWithWhite:0.5 alpha:1.0];
                     
    if (filterEnabled) {
        color = APST.brandTextColor;
        
    }
    
    filterName.textColor = color;
    filterImage.image = [filterImage.image imageTintedWithColor:color];
    filterTriangle.image = [filterTriangle.image imageTintedWithColor:color];
    
}

- (void)setOpened:(BOOL)value
{
    CGAffineTransform transform;
    if (value)
    {
        transform= CGAffineTransformMakeRotation(M_PI);
        opened= YES;
    }
    else
    {
        transform= CGAffineTransformMakeRotation(0);
        opened= NO;
    }
    [UIView animateWithDuration:0.2f animations:^{
        [filterTriangle setTransform:transform];
    }];
    
}

- (IBAction)dropButtonTapped:(id)sender
{
    if (filterEnabled) {
        [self setOpened:!opened];
        [self.delegate dropFilterTable];
    }
    
}

- (IBAction)commentButtonTapped:(id)sender
{
    [self.delegate addComment];
}

@end
