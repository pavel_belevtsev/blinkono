//
//  SocialBuzzSubCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SocialBuzzSubCell.h"

#define SocialBuzzSubCellUserNameFontSize   (isPad ? 16 : 14)
#define SocialBuzzSubCellFeedTextFontSize   (isPad ? 15 : 12)
#define SocialBuzzSubCellFeedDateFontSize   (isPad ? 11 : 10)

#define SocialBuzzSubCellFeedTextWidth      (isPad ? 485 : 218)
#define SocialBuzzSubCellFeedTextHeightMin  (isPad ? 38 : 48)

@implementation SocialBuzzSubCell

- (void)awakeFromNib {
    self.userName.font = Font_Bold(SocialBuzzSubCellUserNameFontSize);
    self.feedText.font = Font_Reg(SocialBuzzSubCellFeedTextFontSize);
    self.feedDate.font = Font_RegItalic(SocialBuzzSubCellFeedDateFontSize);
    
    self.viewCornerImage.layer.cornerRadius = self.viewCornerImage.frame.size.width / 2.0;
    self.viewCornerImage.layer.masksToBounds = YES;
    self.viewCornerImage.layer.borderColor = [UIColor grayColor].CGColor;
    self.viewCornerImage.layer.borderWidth = 1.5f;
    
    self.feedText.numberOfLines = 0;
    
    
    _feedWebView.scrollView.scrollEnabled = NO;
    _feedWebView.scrollView.bounces = NO;
}

- (void)dealloc {
    
    _feedWebView.delegate = nil;
    [_feedWebView stopLoading];
    
}

- (void)updateUIForFeedText {
    
    //CGSize size = [_feedText.text sizeWithFont:Font_Reg(SocialBuzzSubCellFeedTextFontSize) constrainedToSize:CGSizeMake(SocialBuzzSubCellFeedTextWidth, 10000.0) lineBreakMode:NSLineBreakByWordWrapping];
    _feedText.numberOfLines = 0;
    //_feedText.text = [NSString stringWithFormat:@"%@ http://google.com jbkjbkjbk", _feedText.text];
    CGSize size = [[NSString stringWithFormat:@"%@", _feedText.text] boundingRectWithSize:CGSizeMake(SocialBuzzSubCellFeedTextWidth, MAXFLOAT)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:Font_Reg(SocialBuzzSubCellFeedTextFontSize)}
                                               context:nil].size;
    
    int textHeight = MAX(SocialBuzzSubCellFeedTextHeightMin, size.height + 20);
    
    _feedText.frame = CGRectMake(_feedText.frame.origin.x, _feedText.frame.origin.y, SocialBuzzSubCellFeedTextWidth, textHeight);
    _feedWebView.frame = _feedText.frame;
    
    //int cellHeight = _feedText.frame.origin.y + textHeight + (isPad ? 13 : 6);
    
    //_viewDivider.frame = CGRectMake(_viewDivider.frame.origin.x, cellHeight - 1, _viewDivider.frame.size.width, _viewDivider.frame.size.height);
    
    BOOL hasLinks = (([_feedText.text rangeOfString:@"http://"].location != NSNotFound) || ([_feedText.text rangeOfString:@"https://"].location != NSNotFound));
    if (hasLinks) {
        _feedText.hidden = YES;
        _feedWebView.userInteractionEnabled = hasLinks;
        [_feedWebView loadHTMLString:[Utils convertTextToHTML:_feedText.text] baseURL:nil];
        _feedWebView.hidden = NO;
    } else {
        
        _feedText.hidden = NO;
        _feedWebView.hidden = YES;
    }
    
}

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    
    if (inType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

+ (float)getCellHeightForFeedText:(NSString *)text {
    
    CGSize size = [[NSString stringWithFormat:@"%@", text] boundingRectWithSize:CGSizeMake(SocialBuzzSubCellFeedTextWidth, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName:Font_Reg(SocialBuzzSubCellFeedTextFontSize)}
                                     context:nil].size;
    
    int textHeight = MAX(SocialBuzzSubCellFeedTextHeightMin, size.height + 20);
    
    return textHeight + (isPad ? 50 : 40);
    
}


@end
