//
//  SocialHeaderForFilterView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SocialHeaderFilterDelegate <NSObject>

- (void)dropFilterTable;
- (void)addComment;

@end

@interface SocialHeaderForFilterView : UIView {
    
    IBOutlet UILabel *filterName;
    IBOutlet UIImageView *filterImage;
    IBOutlet UIImageView *filterTriangle;
    IBOutlet UIButton *dropButton;
    IBOutlet UIButton *commentButton;
    
    BOOL filterEnabled;
    
}

@property (nonatomic, retain) id <SocialHeaderFilterDelegate> delegate;

- (IBAction)dropButtonTapped:(id)sender;
- (IBAction)commentButtonTapped:(id)sender;

- (void)setIconAndNameFilter:(UIImage *)image name:(NSString *)name;
- (void)setOpened:(BOOL)value;

- (void)setFilterEnabled:(BOOL)isEnabled;

@end
