//
//  PlayerChannelCollectionViewCellVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/22/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerChannelCollectionViewCellVF.h"

@implementation PlayerChannelCollectionViewCellVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.viewLoading.backgroundColor = [VFTypography instance].darkGrayColor;
    self.backgroundColor = [VFTypography instance].darkGrayColor;

    // the following will set a different color for a cell which is selected:
    self.colorView.backgroundColor = [VFTypography instance].grayScale1Color;
    self.labelInfoStart.textAlignment = NSTextAlignmentLeft;

    if (isPhone){
        self.viewSeperatorLine.hidden = YES;
        // align all conttrols to the picture:
        CGRect titleFrame = self.labelInfoTitle.frame;
        
        titleFrame.origin.y +=3;
        self.labelInfoTitle.frame = titleFrame;
        
        [Utils setLabelFont:self.labelInfoTitle size:14 bold:YES];
        
        CGRect viewInfoFrame =  self.viewInfo.frame;
        viewInfoFrame.origin.y -=9;
        self.viewInfo.frame = viewInfoFrame;
    }
    
    
}

- (void)selectChannel:(BOOL)selected {
    [super selectChannel:selected];

    // override changes of colors in super:
    self.viewInfoProgress.backgroundColor = [VFTypography instance].brandColor;
    self.viewInfoProgressBg.backgroundColor = [VFTypography instance].grayScale4Color;

    self.labelInfoStart.textColor = (selected ? [UIColor whiteColor] : [VFTypography instance].grayScale4Color);
    self.labelInfoEnd.textColor = (selected ? [UIColor whiteColor] : [VFTypography instance].grayScale4Color);
}


@end
