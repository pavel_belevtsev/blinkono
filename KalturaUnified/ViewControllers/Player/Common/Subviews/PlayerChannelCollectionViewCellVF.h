//
//  PlayerChannelCollectionViewCellVF.h
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/22/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerChannelCollectionViewCell.h"

@interface PlayerChannelCollectionViewCellVF : PlayerChannelCollectionViewCell

@end
