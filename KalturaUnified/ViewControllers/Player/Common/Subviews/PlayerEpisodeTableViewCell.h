//
//  PlayerEpisodeTableViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerEpisodeTableViewCell : UITableViewCell

- (void)updateData:(TVMediaItem *)item;
- (void)updateData:(TVMediaItem *)item episodesList:(BOOL)episodesList;


@property (nonatomic, weak) IBOutlet UIImageView *imageViewThumb;
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UILabel *labelSubTitle;
@property (nonatomic, weak) IBOutlet UILabel *labelDesc;


@end
