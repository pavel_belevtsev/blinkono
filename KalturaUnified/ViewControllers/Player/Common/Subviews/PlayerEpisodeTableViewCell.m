//
//  PlayerEpisodeTableViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerEpisodeTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation PlayerEpisodeTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {

    UIView *borderView = [_imageViewThumb superview];
    borderView.layer.cornerRadius = 2;
    borderView.layer.masksToBounds = YES;
    
    _imageViewThumb.layer.cornerRadius = 2;
    _imageViewThumb.layer.masksToBounds = YES;
    
    
    [Utils setLabelFont:_labelTitle bold:YES];
    [Utils setLabelFont:_labelSubTitle];
    [Utils setLabelFont:_labelDesc];
}

- (void)updateData:(TVMediaItem *)item {
    
    [self updateData:item episodesList:NO];
}
    
- (void)updateData:(TVMediaItem *)item episodesList:(BOOL)episodesList {

    [_imageViewThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:item andSize:CGSizeMake(_imageViewThumb.frame.size.width * 2, _imageViewThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"]];
    
    
    if (episodesList) {
        
        _labelSubTitle.text = [NSString stringWithFormat:LS(@"episode"), item.metaData[@"Episode number"]];
        
        _labelTitle.text = item.name;

        _labelDesc.text = item.mediaDescription;


    } else {
        
        _labelSubTitle.text = [item.name uppercaseString];
        
        NSString *seriesName = item.name;
        
        NSInteger season = [[item getMediaItemSeasonNumber] integerValue];
        
        if ([item.tags objectForKey:@"Series name"] && [[item.tags objectForKey:@"Series name"] count] > 0) {
            seriesName = [[item.tags objectForKey:@"Series name"] objectAtIndex:0];
        }
        
        
        _labelTitle.text = seriesName;
        
        _labelDesc.text = [NSString stringWithFormat:@"%@ %@, %@ %ld", LS(@"episode_wo_param"), item.metaData[@"Episode number"], LS(@"season_wo_param"), (long)season];
    }
}

@end
