//
//  PlayerVODInfoView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLProgressBar.h"
#import "TVMediaItem+Additions.h"
#import <TvinciSDK/TVPPVModelData.h>

@protocol PlayerVODInfoViewDelegate <NSObject>
- (void)intermediateLogin:(NSString*)segue;
@end

@interface PlayerVODInfoView : UINibView {
    
    IBOutlet UIView *viewLoginContent;
    IBOutlet UIScrollView *scrollViewLogin;
    
    IBOutlet UILabel *labelLogin;
    IBOutlet UIButton *buttonFacebook;
    IBOutlet UIButton *buttonLogin;
    
    IBOutlet UIView *viewButtonCreate;
}

- (void)updateData:(TVMediaItem *)item;

@property (weak, nonatomic) IBOutlet UIButton *playbutton;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewBlur;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewThumb;
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) PlayerViewController *delegateController;
@property (nonatomic, weak) IBOutlet UIView *viewLogin;
@property (nonatomic, retain) UIView *viewItemDetails, *viewMoreSubscriptions;
@property (nonatomic, retain) UILabel *lblItemDetails_watchInstruction, *lblItemDetails_mainActionSubscriptionDaysLeft, *lblItemDetails_secondActionSubscriptionSeasonDaysLeft;
@property (nonatomic, retain) UIButton *btnItemDetails_mainAction, *btnItemDetails_secondAction, *btnItemDetails_downloadAction, *btnMoreSubscriptions;

- (void)updatePadActionButtonsWithState:(VODButtonState)state item:(TVMediaItem *)item;

- (YLProgressBar *) dtgProgress;
- (UIImageView *) dtgStatusImage;
- (UIButton *) dtgButton;
- (void) setImageThumbDownloadIndicator:(KADownloadItemDownloadState) state;

- (void)updatePadActionButtonsWithState:(VODButtonState)state item:(TVMediaItem *)item subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase subscriptionsPurchased:(NSArray *)arrSubscriptionsPurchased;
- (void)updatePadActionButtonsWithState:(VODButtonState)state item:(TVMediaItem *)item subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase ppvRentalInfo:(TVRental*) rentalInfo ppvData:(TVPPVModelData *)ppvData ;

-(void) showLogin;
-(void) hideLogin;
@end
