//
//  PlayerProgramCollectionViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LikeAdapter.h"
#import "RecoredHelper.h"
#import <TvinciSDK/TVNPVRRecordItem.h>

typedef NS_ENUM(NSInteger, ActionButtonsType) {
    ActionButtonsTypeCatchup = 3000,
    ActionButtonsTypeStartOver,
    ActionButtonsTypeRecord,
    ActionButtonsTypeLike,
    ActionButtonsTypeInfo
};

@class PlayerProgramCollectionViewCell;

@protocol PlayerProgramCollectionViewCellDelegate <NSObject>

-(void) playerProgramCell:(PlayerProgramCollectionViewCell *) sender didSelectPlayProgram:(APSTVProgram *) program forChannel:(TVMediaItem *) channel;

-(void) playerProgramCell:(PlayerProgramCollectionViewCell *)sender didSelectRecoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem isSeries:(BOOL)isSeries;

-(void) playerProgramCell:(PlayerProgramCollectionViewCell *)sender didSelectCancelRecored:(RecoredHelper *)recoredHelper;

@end

@interface PlayerProgramCollectionViewCell : UICollectionViewCell

- (void)updateData:(APSTVProgram *)program;


@property (strong, nonatomic) RecoredHelper * recoredHelper;

@property (weak, nonatomic) id<PlayerProgramCollectionViewCellDelegate> delegate;

@property (nonatomic, weak) APSTVProgram *item;
@property (nonatomic, weak) TVMediaItem *mediaItem;
@property (nonatomic, strong) LikeAdapter *likeAdapter;

@property (nonatomic, strong) IBOutlet UIView *viewMain;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *constraintActionButtonsHeight, *constraintLikeButtonHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLikeButtonButtomSpase;
@property (nonatomic, weak) IBOutlet UIButton *liveLikeButton;
@property (nonatomic, weak) IBOutlet UIImageView *imgLikeCounter;
@property (nonatomic, weak) IBOutlet UILabel *labelLikeCounter;
@property (nonatomic, weak) IBOutlet UIView *viewTopLine, *viewTimes, *viewActionButtons;
@property (nonatomic, weak) IBOutlet UILabel *labelTime;
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UILabel *labelDescription;
@property (nonatomic, weak) IBOutlet UILabel *labelSubDescription;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollDescription;
@property (nonatomic, weak) IBOutlet UIImageView *imgThumb;
@property (nonatomic, weak) IBOutlet UIImageView *recoredIconImage;

-(void)showRecoredIcon:(BOOL)isSeries;
-(void)hideRecoredIcon;
- (UIButton*) getRecordButton;
- (CGRect) getRecordButtonLocationInSelf;

- (void) updateButtonUI:(UIButton*) button;
- (void) updateButtonEdgeInsets:(UIButton*) button;
- (void) updateButtonSpaceBetweenElements:(UIButton*) button;
- (void) updateButtonSpaceBetweenElements:(UIButton*) button space:(NSInteger)space;
- (void) addMoreButtonsToArray:(NSMutableArray*) array;
-(UIButton *)likeButtonSelected;
-(void)cancelSeriesRecored;

@end
