//
//  PlayerLangContentView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerLangContentView.h"
#import "PlayerLangCell.h"

const CGFloat TVNLangContentButtonFontSize = 16.0f;
const CGFloat TVNLangBackButtonFontSize = 12.0f;

@interface PlayerLangContentView ()

@property (nonatomic, strong) NSArray *audioList;
@property (nonatomic, strong) NSArray *subList;

@end

@implementation PlayerLangContentView


- (void)awakeFromNib {
    
    [super awakeFromNib];

    
    [Utils setLabelFont:self.btnAudio.titleLabel size:TVNLangContentButtonFontSize bold:YES];
    [Utils setLabelFont:self.btnSubtitles.titleLabel size:TVNLangContentButtonFontSize bold:YES];
    
    self.labelAudio.text = LS(@"audio_language");
    self.labelSubtitles.text = LS(@"subtitles_language");
    
    [self.btnSubtitles setTitle:LS(@"not_selected") forState:UIControlStateNormal];
    self.btnSubtitles.enabled = NO;
    
    self.labelContentBar.text = LS(@"select_language");

    self.labelInnerBar.text = LS(@"select_audio");
    
    [Utils initButtonUI:buttonConfirm title:LS(@"confirm") backGroundColor:APST.brandColor titleColor:APST.brandTextColor];
    [Utils initButtonUI:buttonCancel title:LS(@"cancel") backGroundColor:CS(@"404040") titleColor:CS(@"9e9e9e")];

    if (isPad) {
        
        [self.btnBack.titleLabel setFont:Font_Bold(TVNLangBackButtonFontSize)];
        [self.btnBack setTitle:LS(@"back") forState:UIControlStateNormal];
        
        UIImage *image = [UIImage imageNamed:@"player_lang_frame.png"];
        
        imgFrameBg.image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(80, 0, 56, 0) resizingMode:UIImageResizingModeStretch];
        
        frameContentHeight = imgFrameBg.frame.size.height;
    }
}

- (IBAction)buttonConfirmPressed:(UIButton *)button {
    
    [self.delegate langAudioSelected:selectedAudioIndex];
    [self.delegate langSubSelected:selectedSubIndex];
    
    [self buttonCancelPressed:nil];
}

- (IBAction)buttonCancelPressed:(UIButton *)button {
    
    [self.delegate langSelectionClose:[self superview]];
    
}

- (NSArray*) convertIsoToLangStringArray:(NSArray*) isoArray
{
    // I get the iSO array in the following format:
    //    -audio_482_eng, English (UK)
    //    -audio_483_eng  English (AU)
    
    NSMutableArray* result = [NSMutableArray arrayWithCapacity:isoArray.count];
    
    for (int i = 0; i< isoArray.count; i++) {
        // take last 3 letters of each string
        NSString* isoString = [isoArray objectAtIndex:i];
        NSString* iso639 = [isoString length]>=3 ? [isoString substringFromIndex:[isoString length]-3] : isoString;
        if (iso639.length == 3) {
            /* "PSC : ONO : OPF-1492 VFGTVONES-524 - iOS/iPhone/iPad/Canales/Audio/Audio is in original version for all channels*/

            if([iso639 isEqualToString:@"org"]){
                [result addObject:@"English"];
            }
            NSString* language = [[NSLocale currentLocale] displayNameForKey:NSLocaleLanguageCode value:iso639];
            if(language!= nil){
                
                /* "PSC : ONO : OPF-1329 - App crashes when pressing the language button */
                // Make sure that we will not set index that is not exsist
                [result addObject:language];
            }
        }
    }
    return result;
}


- (void)initContent:(NSArray *)audioList selectedAudio:(NSInteger)selectedAudio subList:(NSArray *)subList selectedSub:(NSInteger)selectedSub {

    imgFrameBg.frame = CGRectMake(0, self.frame.size.height - frameContentHeight, self.frame.size.width, frameContentHeight);

    self.viewContent.alpha = 1.0;
    self.viewContentBar.alpha = 1.0;
    self.viewInner.alpha = 0.0;
    self.viewInnerBar.alpha = 0.0;
    self.audioList = [self convertIsoToLangStringArray:audioList];
    
    selectedAudioIndex = selectedAudio >= self.audioList.count ? 0: selectedAudio;
    
    if (selectedAudioIndex >= [self.audioList count]) {
        selectedAudioIndex = 0;
    }
    
    self.subList = [self convertIsoToLangStringArray:subList];;
    
    selectedSubIndex = selectedSub >= self.subList.count ? 0: selectedSub ;
    
    if (selectedSubIndex > [self.subList count]) {
        selectedSubIndex = 0;
    }
    
    if (selectedAudioIndex < [_audioList count]) [self.btnAudio setTitle:[self.audioList objectAtIndex:selectedAudioIndex] forState:UIControlStateNormal];
    
    if (selectedSubIndex > 0) {
        
        [self.btnSubtitles setTitle:[self.subList objectAtIndex:selectedSubIndex - 1] forState:UIControlStateNormal];
        
    } else {
        
        [self.btnSubtitles setTitle:LS(@"not_selected") forState:UIControlStateNormal];
        
    }
    
    self.btnAudio.enabled = ([self.audioList count] > 1);
    self.btnSubtitles.enabled = ([self.subList count] > 0);
    
}

- (IBAction)buttonContentPressed:(UIButton *)button {
    
    selectedContent = button.tag;
    
    if (selectedContent == 0) {
        
        self.labelInnerBar.text = LS(@"select_audio");
    
    } else {
        
        self.labelInnerBar.text = LS(@"select_subtitles");
        
    }
    
    [self.tableViewInner reloadData];
    
    self.viewContentBar.alpha = 0.0;
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.viewContent.alpha = 0.0;
        self.viewInner.alpha = 1.0;
        
        imgFrameBg.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        
    } completion:^(BOOL finished){
        
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.viewInnerBar.alpha = 1.0;
            
        } completion:^(BOOL finished){
            
        }];

    }];
    
}

- (IBAction)buttonBackPressed:(UIButton *)button {
    
    if (selectedContent == 0) {
        [self.btnAudio setTitle:[self.audioList objectAtIndex:selectedAudioIndex] forState:UIControlStateNormal];
    } else {
        if (selectedSubIndex > 0) {
            
            [self.btnSubtitles setTitle:[self.subList objectAtIndex:selectedSubIndex - 1] forState:UIControlStateNormal];
            
        } else {
            
            [self.btnSubtitles setTitle:LS(@"not_selected") forState:UIControlStateNormal];
            
        }
    }
    
    [UIView animateWithDuration:0.2 animations:^{
    
        self.viewInnerBar.alpha = 0.0;
        
        self.viewInner.alpha = 0.0;
        self.viewContent.alpha = 1.0;
        imgFrameBg.frame = CGRectMake(0, self.frame.size.height - frameContentHeight, self.frame.size.width, frameContentHeight);
        
    } completion:^(BOOL finished){
    
        
        [UIView animateWithDuration:0.2 animations:^{
            
            self.viewContentBar.alpha = 1.0;
            
        } completion:^(BOOL finished){
            
        }];

    }];

    
}

- (void)buttonLangPressed:(UIButton *)button {
    
    if (selectedContent == 0) {
        if (selectedAudioIndex != button.tag) {
            
            selectedAudioIndex = button.tag;
            [self.tableViewInner reloadData];
            
            if (isPad) {
                [self.delegate langAudioSelected:button.tag];
            }
            [self buttonBackPressed:nil];
            
        }

    } else {
        
        if (selectedSubIndex != button.tag) {
            
            selectedSubIndex = button.tag;
            [self.tableViewInner reloadData];
            
            if (isPad) {
                [self.delegate langSubSelected:button.tag];
            }
            [self buttonBackPressed:nil];
            
        }


    }
    
    
}

#pragma mark - TableView Delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return (selectedContent == 0 ? [self.audioList count] : ([self.subList count] + 1));
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellId= @"PlayerLangCell";
    PlayerLangCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellId owner:tableView options:nil] objectAtIndex:0];
    }
    
    cell.viewSelected.backgroundColor = APST.brandColor;
    if (selectedContent == 0) {
        cell.viewSelected.hidden = (indexPath.row != selectedAudioIndex);
    } else {
        cell.viewSelected.hidden = (indexPath.row != selectedSubIndex);
    }
    [cell.buttonLang setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    if (selectedContent == 0) {
        cell.buttonLang.selected = (indexPath.row == selectedAudioIndex);
    } else {
        cell.buttonLang.selected = (indexPath.row == selectedSubIndex);
    }
    [cell.buttonLang addTarget:self action:@selector(buttonLangPressed:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonLang.tag = indexPath.row;
    
    if (selectedContent == 0) {
        [cell.buttonLang setTitle:[self.audioList objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    } else {
        
        if (indexPath.row > 0) {
            
            [cell.buttonLang setTitle:[self.subList objectAtIndex:indexPath.row - 1] forState:UIControlStateNormal];
            
        } else {
            
            [cell.buttonLang setTitle:LS(@"not_selected") forState:UIControlStateNormal];
            
        }
    }
    
    cell.buttonLang.selected = !cell.buttonLang.selected;
    cell.buttonLang.selected = !cell.buttonLang.selected;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [UIView new];
    cell.selectedBackgroundView = [UIView new];
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
