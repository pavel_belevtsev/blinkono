//
//  PlayerLangContentView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LangViewDelegate <NSObject>

- (void)langAudioSelected:(NSInteger)index;
- (void)langSubSelected:(NSInteger)index;
- (void)langSelectionClose:(UIView *)view;

@end

@interface PlayerLangContentView : UINibView {
    
    NSInteger selectedAudioIndex;
    NSInteger selectedSubIndex;
    
    NSInteger selectedContent;
    
    IBOutlet UIButton *buttonConfirm;
    IBOutlet UIButton *buttonCancel;
    
    
    IBOutlet UIImageView *imgFrameBg;

    float frameContentHeight;
    
}

@property(nonatomic, weak) id <LangViewDelegate> delegate;

@property (nonatomic, weak) IBOutlet UIButton *btnAudio;
@property (nonatomic, weak) IBOutlet UIButton *btnSubtitles;

@property (nonatomic, weak) IBOutlet UILabel *labelAudio;
@property (nonatomic, weak) IBOutlet UILabel *labelSubtitles;

@property (nonatomic, weak) IBOutlet UIView *viewContent;

@property (nonatomic, weak) IBOutlet UILabel *labelContentBar;

@property (nonatomic, weak) IBOutlet UIButton *btnBack;

@property (nonatomic, weak) IBOutlet UILabel *labelInnerBar;

@property (nonatomic, weak) IBOutlet UIView *viewInner;
@property (nonatomic, weak) IBOutlet UITableView *tableViewInner;


@property (nonatomic, weak) IBOutlet UIView *viewContentBar;
@property (nonatomic, weak) IBOutlet UIView *viewInnerBar;

- (void)initContent:(NSArray *)audioList selectedAudio:(NSInteger)selectedAudio subList:(NSArray *)subList selectedSub:(NSInteger)selectedSub;

@end
