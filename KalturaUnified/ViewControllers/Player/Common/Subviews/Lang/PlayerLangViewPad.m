//
//  PlayerLangViewPad.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerLangViewPad.h"
#import "PlayerLangContentView.h"

@implementation PlayerLangViewPad

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init {
    
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PlayerLangViewPad class])
                                          owner:nil
                                        options:nil] objectAtIndex:0];
    if (self) {
        
        
        self.popUpAnimationType = TVNPopUpAnimationTypeDownToUp;
        self.isDismissAutomatically = YES;
        
        self.langContentView = [[PlayerLangContentView alloc] initWithNib];
        
        [_viewContent addSubview:_langContentView];
        
    }
    return self;
}

@end
