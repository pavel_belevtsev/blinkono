//
//  PlayerLangView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerLangViewPhone.h"
#import "PlayerLangContentView.h"

@implementation PlayerLangViewPhone

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init {
    
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PlayerLangViewPhone class])
                                          owner:nil
                                        options:nil] objectAtIndex:0];
    if (self) {
        
        self.langContentView = [[PlayerLangContentView alloc] initWithNib];
        
        [_viewContent addSubview:_langContentView];
        
    }
    return self;
}

@end
