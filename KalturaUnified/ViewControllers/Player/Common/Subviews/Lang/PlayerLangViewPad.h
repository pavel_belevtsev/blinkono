//
//  PlayerLangViewPad.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePopUp.h"

@class PlayerLangContentView;

@interface PlayerLangViewPad : BasePopUp {
    
}

@property (nonatomic, strong) IBOutlet UIView *viewContent;
@property (nonatomic, strong) PlayerLangContentView *langContentView;

@end
