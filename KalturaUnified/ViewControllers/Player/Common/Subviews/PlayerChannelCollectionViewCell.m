//
//  PlayerChannelCollectionViewCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerChannelCollectionViewCell.h"
#import "UIImageView+WebCache.h"

@implementation PlayerChannelCollectionViewCell


- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)awakeFromNib {
    
    
    [Utils setLabelFont:_labelNumber bold:YES];
    [Utils setLabelFont:_labelNumberLoading bold:YES];
    [Utils setLabelFont:_labelChannel bold:YES];
    
    _labelBuzz.text = LS(@"series_buzz");
    [Utils setLabelFont:_labelBuzz bold:YES];
    [Utils setLabelFont:_labelBuzzValue bold:YES];
    
    [Utils setLabelFont:_labelInfoTitle bold:YES];
    [Utils setLabelFont:_labelInfoStart bold:YES];
    [Utils setLabelFont:_labelInfoEnd bold:YES];
    
    [Utils setLabelFont:_labelNoEPG];
    self.labelNoEPG.text = LS(@"tv_guide_empty_item_msg");
    self.labelNoEPG.hidden = YES;
    
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
    
}

- (void)skinChanged {
    
    _colorView.backgroundColor = APST.brandColor;

    [self selectChannel:channelSelected];
    
}


- (void)updateWithChannel:(TVMediaItem *)channel {
    
    if (APST.showChannelNumber) {
        _labelNumber.text = [channel.metaData objectForKey:@"Channel number"];
    } else {
        _labelNumber.text = @"";
    }
    
    _labelNumberLoading.text = _labelNumber.text;
    _labelChannel.text = channel.name;
    
    [_imgThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:channel andSize:CGSizeMake(_imgThumb.frame.size.width * 2, _imgThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"epg_channel_placeholder"]];

    
}

- (void)selectChannel:(BOOL)selected {
    
    channelSelected = selected;
    
    _colorView.hidden = !selected;
    
    _labelNumber.textColor = (selected ? [UIColor whiteColor] : CS(@"989898"));
    _labelChannel.textColor = (selected ? [UIColor whiteColor] : CS(@"dbdbdb"));
    
    _labelBuzz.textColor = (selected ? [UIColor whiteColor] : CS(@"aeaeae"));
    _labelBuzzValue.textColor = (selected ? [UIColor whiteColor] : CS(@"ff9c00"));
    
    _labelInfoTitle.textColor = (selected ? [UIColor whiteColor] : CS(@"dbdbdb"));
    _labelInfoStart.textColor = (selected ? [UIColor whiteColor] : APST.brandColor);
    _labelInfoEnd.textColor = (selected ? [UIColor whiteColor] : APST.brandColor);
    
    _labelNoEPG.textColor = (selected ? [UIColor whiteColor] : CS(@"d1d1d1"));
    
    if (selected) {
        
        _viewInfoProgress.backgroundColor = APST.brandDarkColor;
    } else{
        
        _viewInfoProgress.backgroundColor = APST.brandColor;
    }
    
}

- (void)setHighlighted:(BOOL)highlighted {
    
    [super setHighlighted:highlighted];

    _colorView.hidden = (!highlighted && !channelSelected);
    [self setNeedsDisplay];

}

@end
