//
//  PlayerPinView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerPinView.h"

@implementation PlayerPinView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
}

- (void)updatePinSignsView {
    
    int index = 0;
    for (UIImageView *img in [viewPinSigns subviews]) {
        img.highlighted = (index < [self.pinValue length]);
        index++;
    }
    
}

- (void)initParentScreen {
    
    viewPanelLive.hidden = !_liveMode;
    viewPanelVOD.hidden = _liveMode;
    
    self.pinValue = @"";
    [self updatePinSignsView];
    
    labelPinTryAgain.alpha = 0;
    
    [buttonPinCancel setTitle:LS(@"cancel") forState:UIControlStateNormal];
    
    labelPinTitle.text = LS(@"brand_header_parental_pin");
    labelPinText.text = LS(@"brand_parental_pin_dec");
    labelPinTryAgain.text = LS(@"parental_tryagain");
    
    if (isPad) {
        [buttonPinClose setTitle:LS(@"info_btn_close") forState:UIControlStateNormal];
    }
    
    [viewPinPanel performBlockOnSubviewsKindOfClass:[UIButton class] block:^(UIView *subview) {
        [Utils setButtonFont:(UIButton*)subview semibold:YES];
    }];
    
    buttonPinClose.hidden = _liveMode;
}

- (IBAction)buttonPinPressed:(UIButton *)button {
    
    if (labelPinTryAgain.alpha > 0) {
        [UIView animateWithDuration:0.3 animations:^{
            labelPinTryAgain.alpha = 0.0;
        }];
    }
    if ([self.pinValue length] < 4) {
        if (button.tag < 10) {
            self.pinValue = [NSString stringWithFormat:@"%@%ld", self.pinValue, (long)button.tag];
        } else if (button.tag == 10) {
            if (_parentalPinCompletionBlock)
                _parentalPinCompletionBlock(FALSE, @"");
            return;
        } else if (button.tag == 11 && [self.pinValue length] > 0) {
            self.pinValue = [self.pinValue substringToIndex:[self.pinValue length] - 1];
        }
        
        [self updatePinSignsView];
        
        if ([self.pinValue length] == 4) {

            if ([APP_SET isOffline])
            {
               [[DTGManager sharedInstance] verifyParentalCode:self.pinValue completion:^(BOOL succeded)
                {
                    if (_parentalPinCompletionBlock)
                        _parentalPinCompletionBlock(succeded, self.pinValue);
                }];
            }
           else
           { __weak TVPAPIRequest * request = [TVPSiteAPI requestForCheckParentalPINWithRuleID:_pinRuleID andPin:_pinValue delegate:nil];

               [request setFailedBlock:^{

               }];

               [request setCompletionBlock:^{

                   BOOL isUnlocked = [[request JSONResponse] boolValue];

                   if (isUnlocked) {

                       self.pinRuleID = 0;
                       if (_parentalPinCompletionBlock)
                           _parentalPinCompletionBlock(TRUE, self.pinValue);

                   } else {

                       self.pinValue = @"";
                       [self updatePinSignsView];

                       [UIView animateWithDuration:0.3 animations:^{
                           labelPinTryAgain.alpha = 1.0;
                       }];
                   }
               }];

               [_delegateController sendRequest:request];
           }
            

        }
    }
}

@end
