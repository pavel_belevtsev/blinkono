//
//  PlayerPinView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 20.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface PlayerPinView : UINibView {
    
    IBOutlet UIButton *buttonPinClose;
    IBOutlet UIView *viewPinSigns;
    IBOutlet UIView *viewPinPanel;
    IBOutlet UILabel *labelPinTitle;
    IBOutlet UILabel *labelPinText;
    IBOutlet UILabel *labelPinTryAgain;
    IBOutlet UIButton *buttonPinCancel;

    IBOutlet UIView *viewPanelLive;
    IBOutlet UIView *viewPanelVOD;
    
}

- (void)initParentScreen;

@property (nonatomic) NSInteger pinRuleID;
@property (nonatomic, strong) NSString *pinValue;
@property (nonatomic) BOOL liveMode;

@property (nonatomic, weak) BaseViewController *delegateController;
@property (copy) void(^parentalPinCompletionBlock)(BOOL isUnlocked, NSString *enteredCode);

@end
