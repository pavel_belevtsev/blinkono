//
//  PlayerProgramCollectionViewCellVF.m
//  Vodafone
//
//  Created by Israel Berezin on 3/3/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerProgramCollectionViewCellVF.h"
#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"

@interface PlayerProgramCollectionViewCellVF ()

@property (nonatomic, strong) NSLayoutConstraint *bottomConstraint;
@end

@implementation PlayerProgramCollectionViewCellVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)awakeFromNib
{
    [super awakeFromNib];
    if (isPhone)
    {
        [self.liveLikeButton removeConstraints: self.liveLikeButton.constraints];
    }
}

-(void)buildAndAddRecoredButton

{
    
}

- (void)updateData:(APSTVProgram *)program
{
//    if (isPhone)
//    {
//        self.liveLikeButton.hidden=YES;
//       // [self.liveLikeButton removeFromSuperview];
//        self.constraintLikeButtonHeight.constant=0;
//    }

    [super updateData:program];
    
    UIImage * bgImage = isPad ? [UIImage imageNamed:@"btn_base_layer"] : [UIImage imageNamed:@"details_button"];
    
    [self.liveLikeButton setBackgroundImage:[[bgImage imageTintedWithColor:[VFTypography instance].grayScale3Color] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    
    if (isPhone)
    {
        // [self.liveLikeButton removeFromSuperview];
        //self.constraintLikeButtonHeight.constant = 0;
        self.liveLikeButton.hidden=YES;
        self.constraintLikeButtonButtomSpase.constant = 5;
        [self layoutSubviews];
        // [self updateScrollDescriptionFrameIfNeeded];
    }
}

-(void)updateScrollDescriptionFrameIfNeeded
{
    
    NSInteger number =0;
    for (UIView * view in [self.viewActionButtons subviews])
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            number++;
        }
    }
    
    if (number > 2)
    {
        NSInteger newHigh = 80;
        NSInteger numOfTitleLines = [self lineCountForText:self.labelTitle.text];
        if (numOfTitleLines > 1)
        {
            newHigh -= (self.labelTitle.font.lineHeight * (numOfTitleLines-1));
        }
        NSLayoutConstraint *con1 = [NSLayoutConstraint constraintWithItem:self.scrollDescription attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1  constant:newHigh];
        
        [self.scrollDescription addConstraints:@[con1]];
        [self layoutSubviews];
    }
}

- (int)lineCountForText:(NSString *) text
{
    UIFont *font = self.labelTitle.font;
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake(self.labelTitle.width, MAXFLOAT)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName : font}
                                     context:nil];
    
    return ceil(rect.size.height / font.lineHeight);
}

/* "PSC : Vodafone Grup : change to choose reight liveLikeButton : JIRA ticket. */
-(UIButton *)likeButtonSelected
{
    if (isPad)
    {
        return self.liveLikeButton;
    }
    else
    {
        return self.phoneLiveLikeButton;
    }
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)addMoreButtonsToArray:(NSMutableArray *)arr
{
    if (isPhone)
    {
        self.constraintLikeButtonHeight.constant = 0;
        if (self.phoneLiveLikeButton == nil)
        {
            self.phoneLiveLikeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.phoneLiveLikeButton setTag:ActionButtonsTypeLike];
            NSString *imageName = @"details_like_icon";
            UIImage *image = [UIImage imageNamed:imageName];
            [self.phoneLiveLikeButton setImage:image forState:UIControlStateNormal];
            [self.phoneLiveLikeButton setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
            [self.phoneLiveLikeButton setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
            [self.phoneLiveLikeButton setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
            [self.phoneLiveLikeButton setTitleColor:APST.brandColor forState:UIControlStateSelected];
        }
        [self.likeAdapter setupProgram:self.item];
        [self.phoneLiveLikeButton setTitle:[self.item.likeConter stringValue] forState:UIControlStateNormal];
        if (self.phoneLiveLikeButton != nil)
        {
            [arr addObject:self.phoneLiveLikeButton];
        }
    }
}

- (void) updateButtonUI:(UIButton*) button
{  
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 1, 0, 1)];
    UIImage * bgImage = isPad ? [UIImage imageNamed:@"btn_base_layer"] : [UIImage imageNamed:@"details_button"];
    [button setBackgroundImage:[[bgImage imageTintedWithColor:[VFTypography instance].grayScale3Color] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    if (button.tag != ActionButtonsTypeLike)
    {
        [button setBackgroundImage:[[[UIImage imageNamed:@"details_button"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
        [button setBackgroundImage:[[bgImage imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    }

    [button.titleLabel setFont:Font_Bold(15)];
    [Utils setButtonFont:button bold:YES];
}

/* "PSC : Vodafone Grup : change image to best size : JIRA ticket. */
-(void)updateProgramImage
{
    NSURL *pictureURL = [self.item pictureURLForSize:CGSizeMake(568, 320)];
    ASLogInfo(@"pictureURL = %@",pictureURL);
    if (!pictureURL) {
        pictureURL = [self.mediaItem pictureURLForSize:CGSizeMake(self.imgThumb.frame.size.width * 2, self.imgThumb.frame.size.height * 2)];
    }
    
    [self.imgThumb sd_setImageWithURL:pictureURL placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
}

@end
