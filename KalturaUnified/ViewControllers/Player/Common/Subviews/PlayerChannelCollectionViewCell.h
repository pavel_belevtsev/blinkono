//
//  PlayerChannelCollectionViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerChannelCollectionViewCell : UICollectionViewCell {
    
    BOOL channelSelected;
    
}

- (void)updateWithChannel:(TVMediaItem *)channel;
- (void)selectChannel:(BOOL)selected;

@property (nonatomic, weak) IBOutlet UIView *colorView;

@property (nonatomic, weak) IBOutlet UILabel *labelBuzz;

@property (nonatomic, weak) IBOutlet UILabel *labelNumber;
@property (nonatomic, weak) IBOutlet UILabel *labelNumberLoading;
@property (nonatomic, weak) IBOutlet UILabel *labelChannel;
@property (nonatomic, weak) IBOutlet UIImageView *imgThumb;

@property (nonatomic, weak) IBOutlet UIView *viewBuzz;
@property (nonatomic, weak) IBOutlet UILabel *labelBuzzValue;

@property (nonatomic, weak) IBOutlet UILabel *labelInfoTitle;
@property (nonatomic, weak) IBOutlet UILabel *labelInfoStart;
@property (nonatomic, weak) IBOutlet UILabel *labelInfoEnd;
@property (nonatomic, weak) IBOutlet UIView *viewInfoProgressBg;
@property (nonatomic, weak) IBOutlet UIView *viewInfoProgress;
@property (nonatomic, weak) IBOutlet UIView *viewInfo;

@property (nonatomic, weak) IBOutlet UIView *viewLoading;

@property (nonatomic, weak) IBOutlet UILabel *labelNoEPG;
@property (weak, nonatomic) IBOutlet UIView *viewSeperatorLine;

@end
