//
//  PlayerVODRelatedTableViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerVODRelatedTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation PlayerVODRelatedTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {

    UIView *borderView = [_imageViewThumb superview];
    borderView.layer.cornerRadius = 2;
    borderView.layer.masksToBounds = YES;
    
    _imageViewThumb.layer.cornerRadius = 2;
    _imageViewThumb.layer.masksToBounds = YES;
    
    
    [Utils setLabelFont:_labelTitle bold:YES];
    [Utils setLabelFont:_labelSubTitle];
    [Utils setLabelFont:_labelDesc];

}

- (void)updateData:(TVMediaItem *)item {
    
    [_imageViewThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:item andSize:CGSizeMake(_imageViewThumb.frame.size.width * 2, _imageViewThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"]];
    
    _labelTitle.text = item.name;
    
    if (item.isEpisode) {
        _labelSubTitle.text = [NSString stringWithFormat:LS(@"episode"), item.metaData[@"Episode number"]];

    } else if (item.isMovie){
        _labelSubTitle.text = item.metaData[@"Release year"];
    }
    /*”PSC : ONO : OS-91 Ono - CR - Rating needs ot be presented in each media page*/

    if ([item.tags objectForKey:@"Parental Rating"]){// && [[_item.tags objectForKey:@"Parental Rating"] count] > 0) {
        
        if ([_labelSubTitle.text length] > 0) _labelSubTitle.text = [NSString stringWithFormat:@"%@  |  ", _labelSubTitle.text];
        
        _labelSubTitle.text = [NSString stringWithFormat:@"%@%@", _labelSubTitle.text,  LS([Utils ratingToShow:item.tags[@"Parental Rating"]])];
    }
    _labelDesc.text = item.mediaDescription;
    
}

@end
