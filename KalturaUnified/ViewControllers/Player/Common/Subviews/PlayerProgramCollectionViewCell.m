//
//  PlayerProgramCollectionViewCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerProgramCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Tint.h"
#import <CoreText/CoreText.h>
//#import "APSTVProgram+MetaDataTagsHandling.h"

const int TVNButtonGrayColor = 0x575757;

@interface PlayerProgramCollectionViewCell () {
    void(^blockRecordButton)(UIButton *btn);
}

@end

@implementation PlayerProgramCollectionViewCell

@synthesize likeAdapter = _likeAdapter;

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    
    [Utils setLabelFont:_labelTime bold:YES];
    [Utils setLabelFont:_labelTitle bold:YES];
    [Utils setLabelFont:_labelDescription];
    [Utils setLabelFont:_labelSubDescription bold:YES];

    [self createRecordButtonBlock];
    
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
}

- (LikeAdapter*) likeAdapter {
    UIButton* like = [self likeButtonSelected];
    if (like)
    {
        if (!_likeAdapter)
            _likeAdapter = [[LikeAdapter alloc] initWithProgram:nil control:like label:nil imgLike:_imgLikeCounter labelCounter:_labelLikeCounter];
    }
    return _likeAdapter;
}

-(UIButton *)likeButtonSelected
{
    return _liveLikeButton;
}

- (void)skinChanged {
    
    if (isPad) {
        
        [_liveLikeButton setImage:[[UIImage imageNamed:@"details_like_icon"] imageTintedWithColor:CS(@"bebebe")] forState:UIControlStateNormal];
        [_liveLikeButton setImage:[[UIImage imageNamed:@"details_like_icon"] imageTintedWithColor:APST.brandTextColor] forState:UIControlStateNormal];
        
        [_liveLikeButton setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"575757")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
        [_liveLikeButton setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
        
        _imgLikeCounter.image = [[UIImage imageNamed:@"btn_like_counter_base"] imageTintedWithColor:APST.brandColor];
        
        _labelLikeCounter.textColor = APST.brandTextColor;
        
        [Utils setLabelFont:_labelLikeCounter bold:YES];
        [_liveLikeButton.titleLabel setFont:_labelLikeCounter.font];
    }
    else {
        NSString *imageName = @"details_like_icon";
        UIImage *image = [UIImage imageNamed:imageName];
        
        [_liveLikeButton setImage:image forState:UIControlStateNormal];
        [_liveLikeButton setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
        [_liveLikeButton setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
        [_liveLikeButton setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
        [_liveLikeButton setTitleColor:APST.brandColor forState:UIControlStateSelected];
    }

    if (self.item) {
        [self updateData:_item];
    }
}

- (void) createRecordButtonBlock {
    __block PlayerProgramCollectionViewCell *weakSelf = self;
    blockRecordButton = ^(UIButton *btn) {
        [UIView animateWithDuration:0.15 animations:^{
            btn.alpha = 0.5;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.15 animations:^{
                btn.alpha = 1.0;
            } completion:nil];
        }];
        
        BOOL isSeries = NO;
        NSDictionary *dic = weakSelf.item.metaData;
        NSString * seriesValue = [dic objectForKey:@"series ID"];
        NSLog(@"seriesName = %@",seriesValue);
        
        if ([seriesValue length]>0)
        {
            isSeries = YES;
        }
        
        if (btn.selected) // cancel recored
        {
            if ([weakSelf.delegate respondsToSelector:@selector(playerProgramCell:didSelectCancelRecored:)])
            {
                [weakSelf.delegate playerProgramCell:weakSelf didSelectCancelRecored:weakSelf.recoredHelper];
            }
//            if (isSeries == NO)
//            {
                btn.selected =  NO;
                [weakSelf hideRecoredIcon];
//            }
        }
        else // make recored
        {
            if ([weakSelf.delegate respondsToSelector:@selector(playerProgramCell:didSelectRecoredProgram:mediaItem:isSeries:)])
            {
                [weakSelf.delegate playerProgramCell:weakSelf didSelectRecoredProgram:weakSelf.item mediaItem:weakSelf.mediaItem isSeries:isSeries];
            }
        }
    };
}

- (void)updateData:(APSTVProgram *)program {
    self.item = program;
    
    BOOL likeLive = APST.likeLive;
    if ([program.epgChannelID integerValue] < 0)
        likeLive = NO;
    _liveLikeButton.hidden = !likeLive;
    
    if (isPad) {
        [_liveLikeButton superview].hidden = !likeLive;
        _labelLikeCounter.text = [_item.likeConter stringValue];
    }
    else {
        [_liveLikeButton setTitle:[_item.likeConter stringValue] forState:UIControlStateNormal];
    }
    _liveLikeButton.userInteractionEnabled = [LoginM isSignedIn];
    
    if ([LoginM isSignedIn]){
        _imgLikeCounter.highlightedImage = [[UIImage imageNamed:@"btn_like_counter_base"] imageTintedWithColor:CS(@"575757")];
        _labelLikeCounter.highlightedTextColor = CS(@"bebebe");
    }
    [self.likeAdapter setupProgram:program];
    
    UIColor *frameColor = ([program programIsOn]) ? APST.brandColor : CS(@"a2a2a2");
    
    if (isPad) {
        _viewTopLine.backgroundColor = frameColor;
        _labelTime.textColor = ([program programIsOn] ? APST.brandColor : CS(@"999999"));
    }
    else {
        _viewTimes.backgroundColor = _viewTopLine.backgroundColor = frameColor;
        _labelTime.textColor = ([program programIsOn]) ? APST.brandTextColor : [UIColor blackColor];
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];
    _labelTime.text = [NSString stringWithFormat:@"%@ - %@", [df stringFromDate:program.startDateTime], [df stringFromDate:program.endDateTime]];
    _labelTitle.text = program.name;
    
    

    BOOL canStartOver = ([program isStartOverEnabled] && [program programIsOn] && [NU(UIUnit_live_startOver_buttons) isUIUnitExist]);
    BOOL canCatchup = ([program isCathupEnabled] && [program programFinished] && [NU(UIUnit_live_catchup_buttons) isUIUnitExist]);
    BOOL canRecord = ([self.item isRecordingEnabled] && [NU(UIUnit_live_record_buttons) isUIUnitExist]);
    
    [self updateProgramImage];

    if (isPhone) {
        self.constraintLikeButtonHeight.constant = (_liveLikeButton.hidden) ? 0 :28;
    }
    
    NSMutableArray *arrButtons = [NSMutableArray array];
    
    //add StartOver or Catchup button
    UIButton *button = nil;
    if (canStartOver) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.UIUnitID = NU(UIUnit_live_startOver_buttons);
        [button activateAppropriateRule];
        [button setTitle:LS(@"starover_btn_txt") forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"epg_icon_startover"] forState:UIControlStateNormal<<UIControlStateHighlighted];
        [button setTag:ActionButtonsTypeStartOver];
    }
    else if (canCatchup) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.UIUnitID = NU(UIUnit_live_catchup_buttons);
        [button activateAppropriateRule];
        [button setTitle:LS(@"catchup_btn_txt") forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"epg_icon_catchup"] forState:UIControlStateNormal<<UIControlStateHighlighted];
        [button setTag:ActionButtonsTypeCatchup];
    }
    
    if (button) {
        [button addTarget:self action:@selector(playProgram:) forControlEvents:UIControlEventTouchUpInside];
        [arrButtons addObject:button];
    }
    
    //add record button
    if (canRecord) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.UIUnitID = NU(UIUnit_live_record_buttons);
        [button activateAppropriateRule];
        [button setTag:ActionButtonsTypeRecord];
        [button setTitle:LS(@"epg_buttonRecored_rec_text") forState:UIControlStateNormal];
        [button setTitle:LS(@"epg_buttonRecoredc_cancel_text") forState:UIControlStateSelected];
        [button setImage:[UIImage imageNamed:(isPad) ? @"s&s_rec_icon_ipad" : @"s&s_rec_icon_iphone"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:(isPad) ? @"s&s_stop_rec_icon_ipad" : @"s&s_stop_rec_icon_iphone"] forState:UIControlStateSelected];
        [button setTouchUpInsideAction:blockRecordButton];
        
        [arrButtons addObject:button];
    }
    [self addMoreButtonsToArray: arrButtons];
    [self addActionButtonsToButtonsContainer:arrButtons];
    
    
    if ([program.programDescription length]) {
        _labelDescription.text = program.programDescription;
    }
    else {
        _labelDescription.text = LS(@"epg_no_description");
    }
    
    _labelSubDescription.text = [NSString stringWithFormat:@"%d %@", (int)([program.endDateTime timeIntervalSince1970] - [program.startDateTime timeIntervalSince1970]) / 60, LS(@"min")];
    
    if ([program.metaData objectForKey:@"Release year"] && [[program.metaData objectForKey:@"Release year"] length] > 0) {
        _labelSubDescription.text = [NSString stringWithFormat:@"%@  |  %@", _labelSubDescription.text, [program.metaData objectForKey:@"Release year"]];
    }
    /*”PSC : ONO : OS-91 Ono - CR - Rating needs ot be presented in each media page*/

    if ([program.metaData objectForKey:@"rating"] && [[program.metaData objectForKey:@"rating"] length] > 0) {
        _labelSubDescription.text = [NSString stringWithFormat:@"%@  |  %@", _labelSubDescription.text, LS([program.metaData objectForKey:@"rating"])];
    }
    
    [self layoutIfNeeded];
}

- (void) addMoreButtonsToArray:(NSMutableArray*) array {
    //implement in child classes
}

-(void) updateProgramImage
{
    NSURL *pictureURL = [NSURL URLWithString: self.item.pictureURL];
    ASLogInfo(@"pictureURL = %@",pictureURL);
    if (!pictureURL) {
        pictureURL = [self.mediaItem pictureURLForSize:CGSizeMake(_imgThumb.frame.size.width * 2, _imgThumb.frame.size.height * 2)];
    }
    
    [_imgThumb sd_setImageWithURL:pictureURL placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
}


- (IBAction)playProgram:(id)sender
{
    [self.delegate playerProgramCell:self didSelectPlayProgram:self.item forChannel:self.mediaItem];
}

-(void)setRecoredHelper:(RecoredHelper *)recoredHelper {
    UIButton *button = (UIButton*)[self.viewActionButtons viewWithTag:ActionButtonsTypeRecord];
    _recoredHelper = recoredHelper;
    button.selected =  NO;
    [self hideRecoredIcon];
    
    if (recoredHelper.recordingItemStatus == RecoredHelperRecoreded) {
        button.selected = YES;
        [self showRecoredIcon:recoredHelper.isSeries];
    }
    
    if (recoredHelper.recordingItemStatus == RecoredHelperCancelled) {
        button.selected = NO;
    }
}

-(void)showRecoredIcon:(BOOL)isSeries {
    if (isPhone) {
        self.recoredIconImage.image = [UIImage imageNamed:@"record_singleItem_icon"];
    }
    
    if (isSeries) {
        self.recoredIconImage.image = [UIImage imageNamed:@"record_series_icon"];
    }
    else {
        self.recoredIconImage.image = [UIImage imageNamed:@"record_singleItem_icon"];
    }
    self.recoredIconImage.hidden = NO;
}

-(void)hideRecoredIcon {
    self.recoredIconImage.hidden = YES;
}

-(void)cancelSeriesRecored
{
    UIButton *button = (UIButton*)[self.viewActionButtons viewWithTag:ActionButtonsTypeRecord];
    button.selected =  NO;
    [self hideRecoredIcon];
}

- (UIButton*) getRecordButton {
    return (UIButton*)[_viewActionButtons viewWithTag:ActionButtonsTypeRecord];
}

- (CGRect) getRecordButtonLocationInSelf {
    CGRect rect = [[[self getRecordButton] superview] convertRect:[[self getRecordButton] bounds] toView:self];
    rect.origin.x+= [[self getRecordButton] frame].origin.x;
    return rect;
}

- (void) updateButtonUI:(UIButton*) button {
    
    if (isPad) {
        UIColor * bgColor = UIColorFromRGB(TVNButtonGrayColor);
        button.layer.cornerRadius = 4;
        button.layer.shadowColor = [[UIColor blackColor] CGColor];
        button.layer.shadowOffset = CGSizeMake(2, 2);
        
        [button setBackgroundColor:[UIColor clearColor]];
        [button setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:bgColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
        [button setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    }
    else {
        [button setBackgroundImage:[UIImage imageNamed:@"details_button"] forState:UIControlStateNormal];
        [button setBackgroundImage:[[[UIImage imageNamed:@"details_button"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    }
    [button.titleLabel setFont:Font_Bold(15)];
    [Utils setButtonFont:button bold:YES];
}

- (void) updateButtonEdgeInsets:(UIButton*) button {
    [button setContentEdgeInsets:UIEdgeInsetsMake(0, 1, 0, 1)];
    [self updateButtonSpaceBetweenElements:button];
    if (isPad) {
        [self updateButtonEdgeInsets:button appendContentInsets:UIEdgeInsetsMake(0, 0, 2, 0)];
    }
}

- (void) updateButtonSpaceBetweenElements:(UIButton*) button {
    NSInteger space = 0;
    switch (button.tag) {
        case ActionButtonsTypeRecord:
            space = 6;
            break;
        case ActionButtonsTypeStartOver:
            space = 1;
            break;
        default:
            space = 3;
            break;
    }
    [self updateButtonSpaceBetweenElements:button space:space];
}

- (void) updateButtonSpaceBetweenElements:(UIButton*) button space:(NSInteger)space {
    [button setTitleEdgeInsets:UIEdgeInsetsMake(2, space, 0, 0)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, space)];
}

- (void) updateButtonEdgeInsets:(UIButton*) button appendTitleInsets:(UIEdgeInsets)insets {
    UIEdgeInsets titleInsets = button.titleEdgeInsets;
    titleInsets.bottom += insets.bottom;
    titleInsets.top += insets.top;
    titleInsets.left += insets.left;
    titleInsets.right += insets.right;
    button.titleEdgeInsets = titleInsets;
}

- (void) updateButtonEdgeInsets:(UIButton*) button appendImageInsets:(UIEdgeInsets)insets {
    UIEdgeInsets imageInsets = button.imageEdgeInsets;
    imageInsets.bottom += insets.bottom;
    imageInsets.top += insets.top;
    imageInsets.left += insets.left;
    imageInsets.right += insets.right;
    button.imageEdgeInsets = imageInsets;
}

- (void) updateButtonEdgeInsets:(UIButton*) button appendContentInsets:(UIEdgeInsets)insets {
    UIEdgeInsets contentInsets = button.contentEdgeInsets;
    contentInsets.bottom += insets.bottom;
    contentInsets.top += insets.top;
    contentInsets.left += insets.left;
    contentInsets.right += insets.right;
    button.contentEdgeInsets = contentInsets;
}

- (void) addActionButtonsToButtonsContainer:(NSMutableArray*) arrButtons {

    [_viewActionButtons removeAllSubviews];
    
    if (!arrButtons.count) {
        self.constraintActionButtonsHeight.constant = 0;
        return;
    }
    
    NSInteger indexOfLeftmostButtonOnCurrentLine = 0, linesCount = 1;
    CGFloat runningWidth = 0.0f;
    CGFloat maxWidth = _viewActionButtons.width;
    CGFloat horizontalSpaceBetweenButtons = 7.0f;
    CGFloat verticalSpaceBetweenButtons = 7.0f;
    CGFloat buttonHeight = 32.0f;
    CGFloat buttonWidth = 110.0f;
    CGFloat buttonsArrayHeightPerLine = 32.0f;
    
    for (int i=0; i<arrButtons.count; i++) {
        UIButton *button = arrButtons[i];
        button.translatesAutoresizingMaskIntoConstraints = NO;
        [self updateButtonUI:button];
        [self updateButtonEdgeInsets:button];

        [_viewActionButtons addSubview:button];
        [_viewActionButtons addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                                       attribute:NSLayoutAttributeHeight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0f constant:buttonHeight]];
        [_viewActionButtons addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0f constant:buttonWidth]];
        [button sizeToFit];
        
        // check if first button or button would exceed maxWidth
        if ((i == 0) || (runningWidth + button.frame.size.width > maxWidth)) {
            // wrap around into next line
            runningWidth = button.frame.size.width;
            
            if (i== 0) {
                // first button (top left)
                // horizontal position: same as previous leftmost button (on line above)
                NSLayoutConstraint *horizontalConstraint = [NSLayoutConstraint constraintWithItem:button
                                                                                        attribute:NSLayoutAttributeLeft
                                                                                        relatedBy:NSLayoutRelationEqual
                                                                                           toItem:_viewActionButtons
                                                                                        attribute:NSLayoutAttributeLeft
                                                                                       multiplier:1.0f
                                                                                         constant:0];
                [_viewActionButtons addConstraint:horizontalConstraint];
                
                // vertical position:
                NSLayoutConstraint *verticalConstraint = [NSLayoutConstraint constraintWithItem:button
                                                                                      attribute:NSLayoutAttributeTop
                                                                                      relatedBy:NSLayoutRelationEqual
                                                                                         toItem:_viewActionButtons
                                                                                      attribute:NSLayoutAttributeTop
                                                                                     multiplier:1.0f
                                                                                       constant:0];
                [_viewActionButtons addConstraint:verticalConstraint];
                
                
            } else {
                linesCount++;
                
                // put it in new line
                UIButton *previousLeftmostButton = [arrButtons objectAtIndex:indexOfLeftmostButtonOnCurrentLine];
                
                // horizontal position: same as previous leftmost button (on line above)
                NSLayoutConstraint *horizontalConstraint = [NSLayoutConstraint constraintWithItem:button
                                                                                        attribute:NSLayoutAttributeLeft
                                                                                        relatedBy:NSLayoutRelationEqual
                                                                                           toItem:previousLeftmostButton
                                                                                        attribute:NSLayoutAttributeLeft
                                                                                       multiplier:1.0f
                                                                                         constant:0.0f];
                [_viewActionButtons addConstraint:horizontalConstraint];
                
                // vertical position:
                NSLayoutConstraint *verticalConstraint = [NSLayoutConstraint constraintWithItem:button
                                                                                      attribute:NSLayoutAttributeTop
                                                                                      relatedBy:NSLayoutRelationEqual
                                                                                         toItem:previousLeftmostButton
                                                                                      attribute:NSLayoutAttributeBottom
                                                                                     multiplier:1.0f
                                                                                       constant:verticalSpaceBetweenButtons];
                [_viewActionButtons addConstraint:verticalConstraint];
                
                indexOfLeftmostButtonOnCurrentLine = i;
            }
        } else {
            // put it right from previous buttom
            runningWidth += button.frame.size.width + horizontalSpaceBetweenButtons;
            
            UIButton *previousButton = [arrButtons objectAtIndex:(i-1)];
            
            // horizontal position: right from previous button
            NSLayoutConstraint *horizontalConstraint = [NSLayoutConstraint constraintWithItem:button
                                                                                    attribute:NSLayoutAttributeLeft
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:previousButton
                                                                                    attribute:NSLayoutAttributeRight
                                                                                   multiplier:1.0f
                                                                                     constant:horizontalSpaceBetweenButtons];
            [_viewActionButtons addConstraint:horizontalConstraint];
            
            // vertical position same as previous button
            NSLayoutConstraint *verticalConstraint = [NSLayoutConstraint constraintWithItem:button
                                                                                  attribute:NSLayoutAttributeTop
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:previousButton
                                                                                  attribute:NSLayoutAttributeTop
                                                                                 multiplier:1.0f
                                                                                   constant:0.0f];
            [_viewActionButtons addConstraint:verticalConstraint];
        }
    }
    self.constraintActionButtonsHeight.constant = linesCount*buttonsArrayHeightPerLine + verticalSpaceBetweenButtons*((int)(linesCount/2));
}

@end
