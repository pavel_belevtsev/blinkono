//
//  MiniEpgRecoredPopUpVF.m
//  Vodafone
//
//  Created by Israel Berezin on 3/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MiniEpgRecoredPopUpVF.h"

@implementation MiniEpgRecoredPopUpVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */

-(void)setupBGColor
{
    [self.internalView setBackgroundColor:[VFTypography instance].darkGrayColor]; // :CS(@"232323")
}

-(void)insertMarginsBgColor
{
    [super insertMarginsBgColor];
    self.toolShowView.layer.borderColor = [VFTypography instance].grayScale1Color.CGColor;
    [self.toolShowView setBackgroundColor:[VFTypography instance].grayScale1Color];
}

-(void)updateSelectOptionFontSizeAndColor:(UILabel *) lab
{
    [Utils setLabelFont:lab size:14 bold:NO];
    [lab setTextColor:[VFTypography instance].grayScale4Color];
}
@end
