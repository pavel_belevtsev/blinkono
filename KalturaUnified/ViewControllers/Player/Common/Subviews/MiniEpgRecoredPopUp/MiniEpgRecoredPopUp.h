//
//  MiniEpgRecoredPopUp.h
//  KalturaUnified
//
//  Created by Israel Berezin on 2/10/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "UINibView.h"
#import "ToolTipView.h"
#import "APSComplexButton.h"

typedef enum
{
    MiniEpgRecoredTypeEpisode =0,
    MiniEpgRecoredTypeSeires
}MiniEpgRecoredType;

@class MiniEpgRecoredPopUp;

@protocol MiniEpgRecoredPopUpDelegate <NSObject>

-(void) miniEpgRecoredPopUp:(MiniEpgRecoredPopUp *)miniEpgRecoredPopUp recredType:(RecoredType)recoredType recoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem andLinkageView:(UIView*)linkageView;
@end


@interface MiniEpgRecoredPopUp : ToolTipView

@property (strong, nonatomic) UIView * mainSuperView;
@property (assign,nonatomic) id<MiniEpgRecoredPopUpDelegate> delegate;

@property (strong, nonatomic) APSTVProgram * program;
@property (strong, nonatomic) TVMediaItem * mediaItem;

@property (strong, nonatomic) RecoredHelper * recoredHelper;
@property (strong,nonatomic ) UIView * internalView;

-(void)buildWithSuperView:(UIView*)mainSuperView OnLinkageView:(UIView *)linkageView withRecoredProgram:(APSTVProgram *) program andMediaItem:(TVMediaItem *) mediaItem toolTipArrowType:(ToolTipArrowType)toolTipArrowType
            toolCloseType:(ToolCloseType)toolCloseType;

-(void)buildWithSuperView:(UIView*)mainSuperView OnLinkageView:(UIView *)linkageView withRecoredHelper:(RecoredHelper *) recoredHelper toolTipArrowType:(ToolTipArrowType)toolTipArrowType
            toolCloseType:(ToolCloseType)toolCloseType;


-(void)setupButtonUI:(APSComplexButton *)button withText:(NSString*)title andImage:(UIImage*)image;
-(void)setupLineColor:(UIView*)line;
-(void)setupBGColor;
-(void)insertCloseButtonIfNeed;

@end
