//
//  PlayerVODInfoView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerVODInfoView.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ImageEffects.h"
#import "UIImage+Blur.h"
#import "AnalyticsManager.h"
#import "SubscriptionsListViewController.h"
#import "UIImage+Tint.h"
#import "TVRental+Additions.h"
#import "PPVFullData.h"

@interface PlayerVODInfoView () {
    YLProgressBar *downloadProgress;
    UIImageView *downloadStatusImage;
}

@property (nonatomic, retain) NSLayoutConstraint *constraintTrailerBtnHeight, *constraintDelimiter1Height, *constraintMoreSubscriptionsViewHeight;
@end
@implementation PlayerVODInfoView

#define kSeasonPassActivityStatus FALSE

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    if (_labelTitle) {
        [Utils setLabelFont:_labelTitle bold:YES];
    }
}

- (void)updateData:(TVMediaItem *)item {
    
    if (_labelTitle) _labelTitle.text = item.name;

    __weak UIImageView *imgBlur = self.imageViewBlur;
    imgBlur.image = nil;
    self.playbutton.hidden = YES;
    
    if (isPad) {
        [_imageViewBlur sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:item andSize:CGSizeMake(_imageViewBlur.frame.size.width, _imageViewBlur.frame.size.height)] placeholderImage:[UIImage imageNamed:@"placeholder_16_9"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (!error && image) {
                imgBlur.image = [image blurredImageWithRadius:BlurRadius];
            }
        }];
        
        
        
        if (![[TVSessionManager sharedTVSessionManager] isSignedIn]) {
            [self initLoginView];
        } else {
            _viewLogin.alpha = 0.0;
            _labelTitle.alpha = 0.0;
        }

    }
    
    [_imageViewThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:item andSize:CGSizeMake(_imageViewThumb.frame.size.width * 2, _imageViewThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_2_3"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!error && image) {
            if (isPhone) {
                imgBlur.image = [image blurredImageWithRadius:3.0];
            }
        }
    }];
 
    if (_viewItemDetails) {
        [_viewItemDetails removeFromSuperview];
        _viewItemDetails = nil;
    }
    
    //set initial button state for ipad
    if (isPad)
        [self updatePadActionButtonsWithState:kVODButtonInitial item:item];
}

- (IBAction)itemSelected:(UIButton *)button {
    
    if (button.tag == 1) {
        [AnalyticsM.currentEvent clearFields];
        AnalyticsM.currentEvent.category = @"Login and registration";
        AnalyticsM.currentEvent.action = @"Facebook User Sign in";
        AnalyticsM.currentEvent.label = @"Item page";
        [self.delegateController intermediateLogin:@"fbLogin"];
        
    } else if (button.tag == 2) {
        [AnalyticsM.currentEvent clearFields];
        AnalyticsM.currentEvent.category = @"Login and registration";
        AnalyticsM.currentEvent.action = @"Sign in";
        AnalyticsM.currentEvent.label = @"Item page";
        [self.delegateController intermediateLogin:@"regularLogin"];
    }
}

-(void) initLoginView {
    
    if (_viewLogin.alpha == 0)
    {
        _viewLogin.alpha = 1.0;
        _labelTitle.alpha = 1.0;

        CGFloat width = _labelTitle.frame.size.width;
        [_labelTitle sizeToFit];
        [_labelTitle setNeedsLayout];
        _labelTitle.frame = CGRectMake(_labelTitle.frame.origin.x, _labelTitle.frame.origin.y, width, _labelTitle.frame.size.height);
        
        _viewLogin.frame = CGRectMake(_labelTitle.frame.origin.x, _labelTitle.frame.origin.y + _labelTitle.frame.size.height, _viewLogin.frame.size.width, _viewLogin.frame.size.height);
        
        [_delegateController initBrandButtonUI:buttonLogin];
        [buttonLogin setTitle:[NSString stringWithFormat:LS(@"login_brand"), APST.brandName] forState:UIControlStateNormal];

        [_delegateController initFacebookButtonUI:buttonFacebook];
        [buttonFacebook setTitle:LS(@"login_facebook") forState:UIControlStateNormal];

        /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
        if (!APST.fbSupport) {
            [buttonLogin setFrame:buttonFacebook.frame];
            [buttonFacebook setHidden:YES];
        }

        [Utils setLabelFont:labelLogin bold:NO];
        labelLogin.text = LS(@"login_to_watch2");
        
        TVUnderlineButton *buttonCreate = [_delegateController createUnderlineButtonUI:viewButtonCreate font:buttonFacebook.titleLabel.font];
        
        if (APST.multiUser) {
            [buttonCreate setTitle:LS(@"guest") forState:UIControlStateNormal];
            buttonCreate.hidden = !APST.showGuestUser;
            [buttonCreate setTouchUpInsideAction:^(UIButton *sender) {
                [self.delegateController intermediateLogin:@"buttonJoinVCPressed"];
            }];
        } else {
            [buttonCreate setTitle:LS(@"create_a_free_account") forState:UIControlStateNormal];
            [buttonCreate setTouchUpInsideAction:^(UIButton *sender) {
                [self.delegateController intermediateLogin:@"buttonCreateAccountPressed"];
            }];
        }
        
        [buttonCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        buttonCreate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        scrollViewLogin.contentSize = viewLoginContent.frame.size;
        
        //remove action buttons view if active
        if (_viewItemDetails) {
            [_viewItemDetails removeFromSuperview];
            _viewItemDetails = nil;
        }
//        if (_viewShouldPlay) {
//            [_viewShouldPlay removeFromSuperview];
//            _viewShouldPlay = nil;
//        }
    }
}

- (NSAttributedString*)attributedInstuctionTextForState:(VODButtonState)state item:(TVMediaItem*)item {
    
    NSMutableAttributedString* attributedDescription = nil;
    const CGFloat fontSize = 14.f;

    switch (state) {
        case kVODButtonShouldPlayState:
        {
            TVSingleItemPrice *sip = [MediaGRA getMediaPrice:item.files];
            NSString *descriptionFormat = @"";
            if (APST.hasInAppPurchases && sip && [MediaGRA isPriceReasonPurchased:sip.priceReason])
                descriptionFormat = LS(@"item_page_free_with_subscription_description");
            else if (sip && sip.priceReason == TVPriceTypeFree)
                descriptionFormat = LS(@"item_page_free_description");
            NSString *descriptionText = [NSString stringWithFormat:descriptionFormat, LS(@"item_page_free")];
            
            NSDictionary* attributes = @{NSForegroundColorAttributeName : APST.brandTextColor, NSFontAttributeName : [UIFont systemFontOfSize:fontSize]};
            attributedDescription = [[NSMutableAttributedString alloc] initWithString:descriptionText attributes:attributes];
            
            NSRange freeStrRange = [descriptionText rangeOfString:LS(@"item_page_free")];
            if (freeStrRange.location != NSNotFound) {
                [attributedDescription setAttributes:@{NSForegroundColorAttributeName : APST.brandColor, NSFontAttributeName : [UIFont boldSystemFontOfSize:fontSize]} range:freeStrRange];
            }
        }
            break;
            
        default:
            break;
    }

    return attributedDescription;
}

- (void)updatePadActionButtonsWithState:(VODButtonState)state item:(TVMediaItem *)item {
    [self updatePadActionButtonsWithState:state item:item subscriptionsPurchased:nil subscriptionsToPurchase:nil ppvRentalInfo:nil ppvData:nil];
}

- (void)updatePadActionButtonsWithState:(VODButtonState)state item:(TVMediaItem *)item subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase ppvRentalInfo:(TVRental*) rentalInfo ppvData:(TVPPVModelData *)ppvData{
    [self updatePadActionButtonsWithState:state item:item subscriptionsPurchased:nil subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:rentalInfo ppvData:ppvData];
    
}
- (void)updatePadActionButtonsWithState:(VODButtonState)state item:(TVMediaItem *)item subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase subscriptionsPurchased:(NSArray *)arrSubscriptionsPurchased {
    [self updatePadActionButtonsWithState:state item:item subscriptionsPurchased:arrSubscriptionsPurchased subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:nil ppvData:nil];
}

- (void)updatePadActionButtonsWithState:(VODButtonState)state item:(TVMediaItem *)item subscriptionsPurchased:(NSArray *)arrSubscriptionsPurchased subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase ppvRentalInfo: (TVRental*) rentalInfo ppvData:(TVPPVModelData *)ppvData {

    [self.delegateController updateCurrentMediaState: state];
    
    if (_viewLogin.alpha == 0) {
        [self createItemDetailsView: item];
        
        //dont show more subscriptions button
        _constraintMoreSubscriptionsViewHeight.constant = 0;
        
        
        NSString *buttonTitle = nil;
        NSAttributedString *buttonAttributedTitle = nil;
        
        __weak PlayerVODInfoView *sself = self;
        void(^block)(UIButton *btn) = ^(UIButton *btn) {
            [self.delegateController buttonPlayPressed];
        };
        
        [Utils setLabelFont:_lblItemDetails_watchInstruction size:14 bold:NO];
        [Utils setLabelFont:_btnItemDetails_mainAction.titleLabel size:16 bold:YES];
        _btnItemDetails_mainAction.hidden = NO;
        
        switch (state)
        {
            case kVODButtonInitial:
            {
                //set temp loading state to button
                buttonAttributedTitle = [self.delegateController attributedActionButtonTitleForState:kVODSubscriptionButtonInitial subscriptionPrice:nil ppvPrice:nil baseFontSize:16];
                [self.delegateController removeActivityIndicator];
                block = nil;
            }
                break;
            case kVODButtonAddToWatchListState:
            {
                if ([[NSDate date] timeIntervalSinceDate:item.StartDate] >= 0){
                    _lblItemDetails_watchInstruction.text = LS(@"item_page_add_to_watch_list_message");
                    
                    buttonTitle = LS(@"item_page_add_to_watch_list");
                    block = ^(UIButton *btn) {
                        [sself.delegateController addToWatchList];
                    };
                    
                } else {
                    _lblItemDetails_watchInstruction.text = LS(@"coming_soon");
                    
                    [Utils setLabelFont:_lblItemDetails_watchInstruction size:18 bold:NO];
                    _btnItemDetails_mainAction.hidden = YES;
                }
                _constraintDelimiter1Height.constant = 26;

            }
                break;
            case kVODButtonAddedToWatchListState:
            {
                _lblItemDetails_watchInstruction.text = LS(@"item_page_add_to_watch_list_message");
                _constraintDelimiter1Height.constant = 26;
                buttonTitle = LS(@"item_page_added_to_watch_list");
                block = ^(UIButton *btn) {
                    [sself.delegateController removeFromWatchList];
                };
            }
                break;
            case kVODButtonPurchaseState:
            {
                NSString *instructionText = @"";
                
                if (APST.hasInAppPurchases) {
                    switch (arrSubscriptionsToPurchase.count) {
                        case 0:
                        {
                            
                            instructionText = @"";
                            buttonAttributedTitle = [self.delegateController attributedActionButtonTitleForState:kVODSubscriptionButtonEmpty subscriptionPrice:nil ppvPrice:nil baseFontSize:16];
                            
                            if (rentalInfo != nil && rentalInfo.purchaseDate==nil)
                            {
                                VODSubscriptionButtonState subscriptionTypeState = kVODSubscriptionButtonTVODsingle;
                                [Utils getDurationTimeFromDate:[NSDate date] toDate:rentalInfo.endDate completion:^(NSInteger number, NSString *unit, NSString *altValue) {
                                   _lblItemDetails_mainActionSubscriptionDaysLeft.text = [NSString stringWithFormat:LS(@"item_page_buy_subscription_for_period"), @(number), [unit lowercaseString]];
                                }];

                                __strong TVSubscriptionData *subscriptionData = [[TVSubscriptionData alloc]init];
                                subscriptionData.isRecurring = NO;
                                subscriptionData.productCode = ppvData.productCode;
                                subscriptionData.priceCode = [TVSubscriptionData_subscriptionPriceCode new];
                                subscriptionData.priceCode.prise = [TVSubscriptionData_prise new];
                                subscriptionData.priceCode.prise.price = [rentalInfo.price floatValue];
                                subscriptionData.priceCode.prise.name = ppvData.PPVPriceCode.prise.name;
                                subscriptionData.priceCode.prise.currency = [TVSubscriptionData_prise_currency new];
                                subscriptionData.priceCode.prise.currency.currencyCD3 = ppvData.PPVPriceCode.prise.currency.currencyCD3;
                                buttonAttributedTitle = [self.delegateController attributedActionButtonTitleForState:subscriptionTypeState subscriptionPrice:subscriptionData.priceCode.prise ppvPrice:ppvData.PPVPriceCode.prise baseFontSize:16];

                                __strong PPVFullData * fullData;
                                fullData =  [[PPVFullData alloc]init];
                                fullData.ppvData = ppvData;
                                fullData.rentalInfo = rentalInfo;

                                block = ^(UIButton *btn) {
                                    [self.delegateController initializePurchaseFlow:fullData];
                                };
                            }
                    }
                            break;
                        case 1:
                        {
                            TVSubscriptionData *subscriptionData = [arrSubscriptionsToPurchase firstObject];
                            VODSubscriptionButtonState subscriptionTypeState = kVODSubscriptionButtonSVODsingle;
                            if (subscriptionData.isRecurring) {
                                subscriptionTypeState = kVODSubscriptionButtonSVODsingle;
                                instructionText = LS(@"item_page_buy_description_movies_svod_single");
                            }
                            else {
                                [Utils getDurationTimeFromNow:[NSDate dateWithMinutesBeforeNow:subscriptionData.subscriptionUsageModule.maxUsageModuleLifeCycle] completion:^(NSInteger number, NSString *unit, NSString *altValue) {
                                    _lblItemDetails_mainActionSubscriptionDaysLeft.text = [NSString stringWithFormat:LS(@"item_page_buy_subscription_for_period"), @(number), [unit lowercaseString]];
                                }];
                            }
                            
                           
                            buttonAttributedTitle = [self.delegateController attributedActionButtonTitleForState:subscriptionTypeState subscriptionPrice:subscriptionData.priceCode.prise ppvPrice:nil baseFontSize:16];

                            block = ^(UIButton *btn) {
                                [self.delegateController initializePurchaseFlow:subscriptionData];
                            };
                        }
                            break;
                        default:
                        {
                            if (item.isEpisode && kSeasonPassActivityStatus) {
                                //not yet implemented
                            }
                            else {
                                NSNumber *minSubscriptionPrice = [arrSubscriptionsToPurchase valueForKeyPath:@"@min.priceCode.prise.price"];
                                TVSubscriptionData_subscriptionPriceCode *subscriptionPriceMinimum = [[[arrSubscriptionsToPurchase filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"priceCode.prise.price == %f", [minSubscriptionPrice floatValue]]] firstObject] priceCode];
                                
                                if (item.isEpisode)
                                    instructionText = LS(@"item_page_buy_description_episode_svod_multi");
                                else
                                    instructionText = LS(@"item_page_buy_description_movies_svod_multi");
                                buttonAttributedTitle = [self.delegateController attributedActionButtonTitleForState:kVODSubscriptionButtonSVODmulti subscriptionPrice:subscriptionPriceMinimum.prise ppvPrice:nil baseFontSize:16];
                                block = ^(UIButton *btn) {
                                    SubscriptionsListViewController *controller = [[ViewControllersFactory defaultFactory] subscriptionsListViewController];
                                    controller.arrSubscriptionsIDsContainingSelectedMedia = [arrSubscriptionsToPurchase valueForKeyPath:@"subscriptionCode"];
                                    [self.delegateController.navigationController pushViewController:controller animated:YES];
                                };
                            }
                        }
                            break;
                    }
                    _lblItemDetails_watchInstruction.text = instructionText;
                    _constraintDelimiter1Height.constant = 15;
                }
                else {
                    instructionText = @"";
                    block = ^(UIButton *btn) {
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                                            message:LS(@"player_watch_with_subscription")
                                                                           delegate:nil
                                                                  cancelButtonTitle:nil
                                                                  otherButtonTitles:LS(@"ok"), nil];
                        
                        [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                            [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
                        }];
                    };
                    _lblItemDetails_watchInstruction.text = instructionText;
                    _constraintDelimiter1Height.constant = 15;
                    buttonTitle = LS(@"purchase");
                }
            }
                break;
            case kVODButtonShouldPlayState:
            {
                if (item.isLive)
                { //start playing live
                   // [self.delegateController buttonPlayPressed];
                }
                else
                {
                    id data = [arrSubscriptionsPurchased firstObject];
                    if( [data isKindOfClass:[TVSubscriptionData class]])
                    {
                        TVSubscriptionData *subscriptionData = (TVSubscriptionData*)[arrSubscriptionsPurchased firstObject];
                        if (subscriptionData && !subscriptionData.isRecurring)
                        {
                            _lblItemDetails_mainActionSubscriptionDaysLeft.text = [SubscriptionM getNRSubscriptionLeftPeriod:subscriptionData];
                        }
                        
                        _lblItemDetails_watchInstruction.attributedText = [self attributedInstuctionTextForState:state item:item];
                        _constraintDelimiter1Height.constant = 15;
                        buttonTitle = LS(@"vod_info_watch_now");
                        
                        if (arrSubscriptionsToPurchase.count) {
                            _constraintMoreSubscriptionsViewHeight.constant = 25;
                            [_btnMoreSubscriptions setTouchUpInsideAction:^(UIButton *btn){
                                SubscriptionsListViewController *controller = [[ViewControllersFactory defaultFactory] subscriptionsListViewController];
                                controller.arrSubscriptionsIDsContainingSelectedMedia = [arrSubscriptionsToPurchase valueForKeyPath:@"subscriptionCode"];
                                [sself.delegateController.navigationController pushViewController:controller animated:YES];
                            }];
                        }
                    }
                    else if([data isKindOfClass:[PPVFullData class]] || rentalInfo.purchaseDate != nil)
                    {
                        TVRental * rental = rentalInfo;

                        if( [data isKindOfClass:[PPVFullData class]])
                        {
                            PPVFullData * fullppvData = (PPVFullData*)[arrSubscriptionsPurchased firstObject];
                            rental = fullppvData.rentalInfo;
                        }
                          
                        if (rental)
                        {
                            _lblItemDetails_mainActionSubscriptionDaysLeft.text = [PPVM getNR_PPVLeftPeriod:rental];
                        }
                        _lblItemDetails_watchInstruction.attributedText = [self attributedInstuctionTextForState:state item:item];
                        _constraintDelimiter1Height.constant = 15;
                        buttonTitle = LS(@"vod_info_watch_now");

                    }
                    else
                    {
                        _lblItemDetails_watchInstruction.attributedText = [self attributedInstuctionTextForState:state item:item];
                        _constraintDelimiter1Height.constant = 15;
                        buttonTitle = LS(@"vod_info_watch_now");
                    }
                    
                    //create download action button
                    if ([self.delegateController isMediaAllowedForDownloading:item]) {
                        [self createDownloadActionButton:item];
                    }
                }
            }
                break;
           
            default:
                break;
        }
        
        [_btnItemDetails_mainAction setTitle:buttonTitle forState:UIControlStateNormal];
        [_btnItemDetails_mainAction setAttributedTitle:buttonAttributedTitle forState:UIControlStateNormal];
        [_btnItemDetails_mainAction setTouchUpInsideAction:block];
    }
    
    //update visibility of more subscription view
    _viewMoreSubscriptions.hidden = (!_constraintMoreSubscriptionsViewHeight.constant);
    
    [self.delegateController finishedUpdatingActionButtonForState:state];

}

- (void) createItemDetailsView:(TVMediaItem *)item {
    if (!_viewItemDetails) {
        UIView *superView = _viewLogin.superview;
        _viewItemDetails = [UIView new];
        _viewItemDetails.translatesAutoresizingMaskIntoConstraints = NO;
        [superView addSubview:_viewItemDetails];

        NSDictionary *metrics = @{@"top":@(self.labelTitle.position.y),
                                  @"left":@(self.labelTitle.position.x)};
        [superView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[_viewItemDetails]|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_viewItemDetails)]];
        [superView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(left)-[_viewItemDetails(314)]->=0-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_viewItemDetails)]];
        
        
        //create item title label
        UILabel *lblTitle = [self createMediaTitleLabel: item];
        
        //create item type and year text label
        UILabel *lblTypeAndYear = [self createMediaTypeAndYearLabel: item];

        //create trailer button
        UIButton *btnPlayTrailer = [self createPlayTrailerButton: item];

        //create item description
        UILabel *lblDescription = [self createMediaDescriptionLabel: item];
        
        //delimeter between item data and actions
        UIView *viewDelimeter1 = [self createDelimeterView1];
        
        //create instruction text label
        [self createWatchInstructionLabel];

        //create main action button
        [self createMainActionButton];
        
        //create main action subscription days left text label
        [self createMainActionSubscriptionDaysLeftLabel];
    
        
        //create trailer button
        [self createMoreSubscriptionsButtonView];
        
        NSMutableDictionary *viewsDictionary = [NSMutableDictionary dictionaryWithDictionary:
                                         @{@"title":lblTitle,
                                          @"typeAndYear":lblTypeAndYear,
                                          @"playTrailer":btnPlayTrailer,
                                          @"description":lblDescription,
                                          @"delimeter1":viewDelimeter1,
                                          @"watchInstruction":_lblItemDetails_watchInstruction,
                                          @"mainAction":_btnItemDetails_mainAction,
                                          @"maDaysLeft":_lblItemDetails_mainActionSubscriptionDaysLeft,
                                          @"moreSubs":_viewMoreSubscriptions}];
        
        NSString *additionalConstraints = @"";
        if (item.isEpisode && kSeasonPassActivityStatus)
        {
            //create second action button
            [self createSecondActionButton];
            
            //create second action subscription days left text label
            [self createSecondActionSubscriptionDaysLeftLabel];
            
            [viewsDictionary addEntriesFromDictionary:@{
                                        @"secondAction":_btnItemDetails_secondAction,
                                        @"saDaysLeft":_lblItemDetails_secondActionSubscriptionSeasonDaysLeft}];
            
            
            additionalConstraints = @"15-[secondAction(44)]-3-[saDaysLeft]-";
        }
        
        metrics = @{@"spaceAfterTrailer": ([self shouldShowTrailerButtonForItem:item]) ? @15 : @0};
        [_viewItemDetails addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-15-[title]-6-[typeAndYear]-12-[playTrailer]-(spaceAfterTrailer)-[description]-10-[delimeter1]-0-[watchInstruction(>=16)]-8-[mainAction(44)]-3-[maDaysLeft]-%@8-[moreSubs]", additionalConstraints] options:0 metrics:metrics views:viewsDictionary]];

    }
    else if (![_viewItemDetails superview])
        [_viewLogin.superview addSubview:_viewItemDetails];
}

- (UILabel*) createMediaTitleLabel:(TVMediaItem *)item {
    UILabel *label = [UILabel new];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentLeft;
    label.text = item.name;
    label.textColor = APST.brandTextColor;
    [_viewItemDetails addSubview:label];
    [_viewItemDetails addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[label]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
    [Utils setLabelFont:label size:24 bold:YES];

    return label;
}

- (UIButton*) createPlayTrailerButton:(TVMediaItem *)item {
    __block PlayerVODInfoView *sself = self;

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    
    if ([self shouldShowTrailerButtonForItem:item]) {
        UIImage *imgPlayTrailer = [UIImage imageNamed:@"arrow_play-trailer"];
        NSString *txtPlayTrailer = [NSString stringWithFormat:@"  %@", LS(@"item_page_play_trailer")];
        button.titleLabel.font = Font_Bold(14);
        [button setBackgroundColor:[UIColor clearColor]];
        [button setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
        [button setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
        [button setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
        [button setImage:imgPlayTrailer forState:UIControlStateNormal];
        [button setImage:imgPlayTrailer forState:UIControlStateHighlighted];
        [button setImage:imgPlayTrailer forState:UIControlStateSelected];
        [button setTitle:txtPlayTrailer forState:UIControlStateNormal];
        [button setTitle:txtPlayTrailer forState:UIControlStateHighlighted];
        [button setTitle:txtPlayTrailer forState:UIControlStateSelected];
        button.layer.cornerRadius = 5;
        button.layer.masksToBounds = YES;
        button.layer.borderWidth = 1.0f;
        button.layer.borderColor = [UIColor whiteColor].CGColor;
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 10)];

        [button setTouchUpInsideAction:^(UIButton *btn) {
            [sself.delegateController buttonPlayTrailerPressed];
        }];
    }
    [_viewItemDetails addSubview:button];
    [_viewItemDetails addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button(114)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)]];
    [_viewItemDetails addConstraint:_constraintTrailerBtnHeight = [NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:0]];
    _constraintTrailerBtnHeight.constant = ([self shouldShowTrailerButtonForItem:item]) ? 30 : 0;
    
    return button;
}

- (void) createMoreSubscriptionsButtonView {
    
    _viewMoreSubscriptions = [UIView new];
    _viewMoreSubscriptions.translatesAutoresizingMaskIntoConstraints = NO;
    [_viewItemDetails addSubview:_viewMoreSubscriptions];
    
    _btnMoreSubscriptions = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnMoreSubscriptions.translatesAutoresizingMaskIntoConstraints = NO;
    _btnMoreSubscriptions.titleLabel.font = Font_Bold(14);
    [_btnMoreSubscriptions setBackgroundColor:[UIColor clearColor]];
    [_btnMoreSubscriptions setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
    [_btnMoreSubscriptions setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [_btnMoreSubscriptions setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    [_btnMoreSubscriptions setTitle:LS(@"more_subscriptions") forState:UIControlStateNormal];
    [_btnMoreSubscriptions setTitle:LS(@"more_subscriptions") forState:UIControlStateHighlighted];
    [_btnMoreSubscriptions setTitle:LS(@"more_subscriptions") forState:UIControlStateSelected];
    [_viewMoreSubscriptions addSubview:_btnMoreSubscriptions];

    UIImageView *imgArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_more-subs"]];
    imgArrow.translatesAutoresizingMaskIntoConstraints = NO;
    imgArrow.contentMode = UIViewContentModeCenter;
    
    [_viewMoreSubscriptions addSubview:imgArrow];
    [_viewMoreSubscriptions addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_btnMoreSubscriptions]-5-[imgArrow(8)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgArrow, _btnMoreSubscriptions)]];
    [_viewMoreSubscriptions addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imgArrow]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgArrow)]];
    [_viewMoreSubscriptions addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_btnMoreSubscriptions]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_btnMoreSubscriptions)]];

    [_viewItemDetails addConstraint:_constraintMoreSubscriptionsViewHeight = [NSLayoutConstraint constraintWithItem:_viewMoreSubscriptions attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:0]];
    
    [_viewItemDetails addConstraint:[NSLayoutConstraint constraintWithItem:_viewMoreSubscriptions
                                                                 attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:_btnItemDetails_mainAction
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.0
                                                                  constant:0.0]];
    _constraintMoreSubscriptionsViewHeight.constant = 0;
}

- (UILabel*) createMediaTypeAndYearLabel:(TVMediaItem *)item {
    
    UILabel *label = [UILabel new];
    /*”PSC : ONO : OS-91 Ono - CR - Rating needs ot be presented in each media page*/

    label.text=@"";
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentLeft;
    label.font = Font_Reg(14);
    NSString *genre = [item.tags[TVTagGenre] firstObject];
    NSString *year =  item.metaData[TVMetaDataReleaseYear];
    
    NSString *rating = LS([Utils ratingToShow:item.tags[@"Parental Rating"]]);

    
    NSString *duration = item.durationDescription;
    if (duration && duration.length) {
        label.text = duration;
    }
        
    if (year && year.length) {
        if (label.text.length) {
            label.text = [label.text stringByAppendingString:@"  |  "];
        }
        label.text = [label.text stringByAppendingString:year];
    }
    
    if (genre && genre.length) {
        if (label.text.length) {
            label.text = [label.text stringByAppendingString:@"  |  "];
        }
        label.text = [label.text stringByAppendingString:genre];
    }
    if (rating && rating.length) {
        if (label.text.length) {
            label.text = [label.text stringByAppendingString:@"  |  "];
        }
        label.text = [label.text stringByAppendingString:rating];
    }
    
    label.textColor = APST.brandTextColor;
    [_viewItemDetails addSubview:label];
    [_viewItemDetails addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[label]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];

    return label;
}

- (UILabel*) createMediaDescriptionLabel:(TVMediaItem *)item {
    UILabel *label = [UILabel new];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.lineBreakMode = NSLineBreakByTruncatingTail;
    label.numberOfLines = 4;
    label.textAlignment = NSTextAlignmentLeft;
    label.text = [item.mediaDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    label.textColor = APST.brandTextColor;
    [_viewItemDetails addSubview:label];
    [_viewItemDetails addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[label]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
    [Utils setLabelFont:label size:14 bold:NO];

    return label;
}

- (UIView*) createDelimeterView1 {
    UIView *view = [UIView new];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [_viewItemDetails addSubview:view];
    [_viewItemDetails addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view(264)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
    [_viewItemDetails addConstraint:_constraintDelimiter1Height = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:16]];
    
    UIView *line = [UIView new];
    line.translatesAutoresizingMaskIntoConstraints = NO;
    line.backgroundColor = [APST.brandTextColor colorWithAlphaComponent:0.15];
    [view addSubview:line];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[line]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(line)]];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:line
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:view
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0.0]];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:line
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1.0
                                                      constant:1.0]];
    return view;
}

- (void) createWatchInstructionLabel {
    _lblItemDetails_watchInstruction = [UILabel new];
    _lblItemDetails_watchInstruction.translatesAutoresizingMaskIntoConstraints = NO;
    _lblItemDetails_watchInstruction.numberOfLines = 0;
    _lblItemDetails_watchInstruction.textAlignment = NSTextAlignmentLeft;
    _lblItemDetails_watchInstruction.text = @"";
    _lblItemDetails_watchInstruction.textColor = APST.brandTextColor;
    [_viewItemDetails addSubview:_lblItemDetails_watchInstruction];
    [_viewItemDetails addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_lblItemDetails_watchInstruction]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_lblItemDetails_watchInstruction)]];
    [Utils setLabelFont:_lblItemDetails_watchInstruction size:14 bold:YES];
}

- (void) createMainActionButton {
    _btnItemDetails_mainAction = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnItemDetails_mainAction setTitle:@"" forState:UIControlStateNormal];
    [_viewItemDetails addSubview:_btnItemDetails_mainAction];
    [self updateActionButton: _btnItemDetails_mainAction];
}

- (void) createSecondActionButton {
    _btnItemDetails_secondAction = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnItemDetails_secondAction setTitle:@"" forState:UIControlStateNormal];
    [_viewItemDetails addSubview:_btnItemDetails_secondAction];
    [self updateActionButton: _btnItemDetails_secondAction];
}

- (void) updateActionButton:(UIButton*)button {
    button.translatesAutoresizingMaskIntoConstraints = NO;
    button.layer.cornerRadius = 5;
    button.layer.masksToBounds = YES;
    [button setBackgroundColor:APST.brandColor];
    [button setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
    [button setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [button setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    [[button superview] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button(210)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(button)]];
}

- (void) createDownloadActionButton:(TVMediaItem *)item {
    __weak PlayerVODInfoView *sself = self;
    if (!_btnItemDetails_downloadAction || !_btnItemDetails_downloadAction.superview) {
        _btnItemDetails_downloadAction = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnItemDetails_downloadAction setTitle:@"" forState:UIControlStateNormal];
        [_btnItemDetails_downloadAction.titleLabel setFont:Font_Bold(14)];
        [_btnItemDetails_downloadAction setTouchUpInsideAction:^(UIButton *btn) {
            [sself.delegateController buttonDownloadPressed];
        }];
        
        [_viewItemDetails addSubview:_btnItemDetails_downloadAction];
        
        _btnItemDetails_downloadAction.translatesAutoresizingMaskIntoConstraints = NO;
        _btnItemDetails_downloadAction.layer.cornerRadius = 5;
        _btnItemDetails_downloadAction.layer.masksToBounds = YES;
        [_btnItemDetails_downloadAction setBackgroundColor:CS(@"575757")];
        
        [_viewItemDetails addConstraint:[NSLayoutConstraint constraintWithItem:_btnItemDetails_downloadAction
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:_btnItemDetails_mainAction
                                                                     attribute:NSLayoutAttributeHeight
                                                                    multiplier:1.0
                                                                      constant:1.0]];
        [_viewItemDetails addConstraint:[NSLayoutConstraint constraintWithItem:_btnItemDetails_downloadAction
                                                                     attribute:NSLayoutAttributeWidth
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:nil
                                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                                    multiplier:1.0
                                                                      constant:68.0]];
        
        [_viewItemDetails addConstraint:[NSLayoutConstraint constraintWithItem:_btnItemDetails_downloadAction
                                                                     attribute:NSLayoutAttributeLeading
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:_btnItemDetails_mainAction
                                                                     attribute:NSLayoutAttributeTrailing
                                                                    multiplier:1.0
                                                                      constant:14.0]];
        
        [_viewItemDetails addConstraint:[NSLayoutConstraint constraintWithItem:_btnItemDetails_downloadAction
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:_btnItemDetails_mainAction
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0
                                                                      constant:0.0]];
        
        
        [self createDownloadActionButtonSubviews];
        
    }
}

- (void) createDownloadActionButtonSubviews {
    downloadProgress = [YLProgressBar new];
    downloadProgress.userInteractionEnabled = FALSE;
    downloadProgress.translatesAutoresizingMaskIntoConstraints = NO;
    [downloadProgress setProgress:1 animated:NO];
    downloadProgress.type = YLProgressBarTypeFlat;
    downloadProgress.stripesOrientation = YLProgressBarStripesOrientationRight;
    downloadProgress.stripesDirection = YLProgressBarStripesDirectionRight;
    downloadProgress.indicatorTextDisplayMode = YLProgressBarIndicatorTextDisplayModeNone;
    downloadProgress.behavior = YLProgressBarBehaviorWaiting;

    [_btnItemDetails_downloadAction addSubview:downloadProgress];
    [_btnItemDetails_downloadAction addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[downloadProgress]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(downloadProgress)]];
    [_btnItemDetails_downloadAction addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[downloadProgress(6)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(downloadProgress)]];
    downloadProgress.backgroundColor = [UIColor clearColor];
    downloadProgress.trackTintColor = [UIColor clearColor];
    
    downloadStatusImage = [UIImageView new];
    downloadStatusImage.userInteractionEnabled = FALSE;
    downloadStatusImage.image = [[UIImage imageNamed:@"download_icon"] imageTintedWithColor:CS(@"737373")];

    downloadStatusImage.translatesAutoresizingMaskIntoConstraints = NO;
    [_btnItemDetails_downloadAction addSubview:downloadStatusImage];
    [_btnItemDetails_downloadAction addConstraint:[NSLayoutConstraint constraintWithItem:downloadStatusImage
                                                                               attribute:NSLayoutAttributeCenterY
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:_btnItemDetails_downloadAction
                                                                               attribute:NSLayoutAttributeCenterY
                                                                              multiplier:1.0
                                                                                constant:0.0]];
    [_btnItemDetails_downloadAction addConstraint:[NSLayoutConstraint constraintWithItem:downloadStatusImage
                                                                               attribute:NSLayoutAttributeCenterX
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:_btnItemDetails_downloadAction
                                                                               attribute:NSLayoutAttributeCenterX
                                                                              multiplier:1.0
                                                                                constant:0.0]];
    [_btnItemDetails_downloadAction addConstraint:[NSLayoutConstraint constraintWithItem:downloadStatusImage
                                                                               attribute:NSLayoutAttributeWidth
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:nil
                                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                                              multiplier:1.0
                                                                                constant:16.0]];
    [_btnItemDetails_downloadAction addConstraint:[NSLayoutConstraint constraintWithItem:downloadStatusImage
                                                                               attribute:NSLayoutAttributeHeight
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:nil
                                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                                              multiplier:1.0
                                                                                constant:16.0]];
}

- (void) setImageThumbDownloadIndicator:(KADownloadItemDownloadState) state {
    [self removeImageThumbDownloadIndicator];
    UIView *viewImageContainer = [self.imageViewThumb superview];
    
    UIImageView *image = [UIImageView new];
    image.translatesAutoresizingMaskIntoConstraints = NO;
    image.tag = 101;
    
    switch (state) {
        case KADownloadItemDownloadStateDownloaded:
            [image setImage:[UIImage imageNamed:@"mediapage_downloaded_indicator"]];
            break;
        default:
            [image setImage:[UIImage imageNamed:@"mediapage_downloadable_indicator"]];
            break;
    }

    [viewImageContainer addSubview:image];
    [viewImageContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[image]-1-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(image)]];
    [viewImageContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[image]-1-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(image)]];
}

- (void) removeImageThumbDownloadIndicator {
    UIView *viewImageContainer = [self.imageViewThumb superview];
    [[viewImageContainer viewWithTag:101] removeFromSuperview];
}

#pragma mark - other

- (void) createMainActionSubscriptionDaysLeftLabel {
    _lblItemDetails_mainActionSubscriptionDaysLeft = [UILabel new];
    [_viewItemDetails addSubview:_lblItemDetails_mainActionSubscriptionDaysLeft];
    [self updateSubscriptionDaysLeftLabel: _lblItemDetails_mainActionSubscriptionDaysLeft relatedToView:_btnItemDetails_mainAction];
}

- (void) createSecondActionSubscriptionDaysLeftLabel {
    _lblItemDetails_secondActionSubscriptionSeasonDaysLeft = [UILabel new];
    [_viewItemDetails addSubview:_lblItemDetails_secondActionSubscriptionSeasonDaysLeft];
    [self updateSubscriptionDaysLeftLabel: _lblItemDetails_secondActionSubscriptionSeasonDaysLeft relatedToView:_btnItemDetails_secondAction];
}

- (void) updateSubscriptionDaysLeftLabel:(UILabel*)label relatedToView:(UIView*)relatedView {
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.font = [UIFont systemFontOfSize:16.0f];
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"";
    label.textColor = APST.brandTextColor;
    [[label superview] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[label]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
    [[label superview] addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:relatedView attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
    [Utils setLabelFont:label size:13 bold:NO];

}

-(void) showLogin
{
    [self initLoginView];
}

-(void) hideLogin
{
    _viewLogin.alpha = 0.0;
    _labelTitle.alpha = 0.0;
    
}

- (IBAction)playSelected:(id)sender
{
    [self.delegateController buttonPlayPressed];
}

#pragma mark - DTG
- (YLProgressBar *) dtgProgress {
    return downloadProgress;
}

- (UIImageView *) dtgStatusImage {
    return downloadStatusImage;
}

- (UIButton *) dtgButton {
    return self.btnItemDetails_downloadAction;
}

- (BOOL)shouldShowTrailerButtonForItem:(TVMediaItem *) mediaItem {

    return mediaItem.hasTrailerFile && ![APP_SET isOffline];
}


@end
