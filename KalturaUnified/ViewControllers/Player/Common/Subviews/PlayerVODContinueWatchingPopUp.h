//
//  PlayerVODContinueWatchingPopUp.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 19.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PlayerVODContinueWatchingPopUpDelegate <NSObject>

- (void)playerVODContinueWatchingStartOverPressed;
- (void)playerVODContinueWatchingContinueFrom:(CGFloat)startTime;
- (void)playerVODContinueWatchingStartOverClosed;

@end

@interface PlayerVODContinueWatchingPopUp : UIView

@property (nonatomic, weak) id <PlayerVODContinueWatchingPopUpDelegate> delegate;

- (id)initWithMedia:(TVMediaItem *)mediaItem continueFrom:(NSInteger)startTime;
- (id)initWithMedia:(TVMediaItem *)mediaItem continueFrom:(NSInteger)startTime  STBcontinueFrom:(NSInteger)STBstartTime;

@end
