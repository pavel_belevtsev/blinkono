//
//  PlayerNPVRInfoView.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 3/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlayerNPVRInfoView;

@protocol PlayerNPVRInfoViewDelegate <NSObject>
-(void) playerNPVRInfoView:(PlayerNPVRInfoView *) sender didSelectStartOver:(UIButton *) startOver;
@end

@interface PlayerNPVRInfoView : UINibView


-(void) updateNPVRItem:(TVNPVRRecordItem *) recordItem;

@property (nonatomic, strong) TVNPVRRecordItem * recordedItem;
@property (nonatomic, strong) TVMediaItem * channel;
@property (nonatomic, weak) IBOutlet UILabel *labelName;
@property (nonatomic, weak) IBOutlet UILabel *labelSubName;


@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewContent;
@property (weak, nonatomic) IBOutlet UIView *viewDetailsContent;
@property (nonatomic, weak) IBOutlet UITextView *textViewDescription;

@property (nonatomic, weak) IBOutlet UIImageView *imgThumbBlur;
@property (nonatomic, weak) IBOutlet UIImageView *imgThumb;
@property (nonatomic, weak) IBOutlet UIView *viewBar;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *constraintMediaNameTop;


@property (nonatomic, weak) IBOutlet UIButton * startOverButton;

@property (nonatomic, strong) IBOutlet TVLinearLayout * linearLayout;
@property (nonatomic, strong) IBOutlet UILabel * labelRecordedon;
@property (weak, nonatomic) IBOutlet UIView *recordedonPlaceHolder;




@property (nonatomic, weak) id <PlayerNPVRInfoViewDelegate> delegate;

-(void) updateUI;

@end
