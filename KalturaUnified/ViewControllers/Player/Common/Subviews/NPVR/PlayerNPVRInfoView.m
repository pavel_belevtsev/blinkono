//
//  PlayerNPVRInfoView.m
//  KalturaUnified
//
//  Created by Rivka Schwartz on 3/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerNPVRInfoView.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Blur.h"
#import "UIImage+Tint.h"
#import <TvinciSDK/TVLinearLayout.h> 

#define  TVNButtonGrayColor  0x575757
#define GRAY_COLOR [UIColor lightGrayColor]

@implementation PlayerNPVRInfoView


- (void)updateData:(TVMediaItem *)item
{}

-(void) updateNPVRItem:(TVNPVRRecordItem *) recordItem
{
    self.recordedItem = recordItem;
    [self updateUI];
}

-(void) updateUI
{
     
    
    self.viewBar.backgroundColor = APST.brandColor;
    self.labelRecordedon.textColor = [UIColor whiteColor];
    
    self.linearLayout.margins = 5;
    self.linearLayout.orientation =  TVLayoutOrientationHorizontal;
    
    _scrollViewContent.contentOffset = CGPointZero;
    
    
    _labelName.text = self.recordedItem.name;
        _labelSubName.text = @"";
    
    if ([[self.recordedItem.epgTags objectForKey:@"episode"] lastObject])
    {
        self.labelSubName.text = [[NSString stringWithFormat:LS(@"episode"),[[self.recordedItem.epgTags objectForKey:@"episode"] lastObject]] uppercaseString];
    }
    
    NSString * duration = [NSString stringWithFormat:@"%d %@", (int)([self.recordedItem.endDate timeIntervalSince1970] - [self.recordedItem.startDate timeIntervalSince1970]) / 60, LS(@"min")];
    
    NSMutableArray * metas = [NSMutableArray array];
    duration?[metas addObject:duration]:nil;
    
    NSString * metasString = [metas firstObject];
    if ([self.channel.name length]) metasString =  [NSString stringWithFormat:@"%@ | %@",metasString,self.channel.name];

    
    UILabel * metaLabel = [[UILabel alloc] init];
    metaLabel.text = metasString;
    [metaLabel sizeToFit];
    metaLabel.textColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0  blue:204.0/255.0  alpha:1];
    [Utils setLabelFont:metaLabel size:isPad?13:12 bold:isPad?NO:YES];
    


    [self.linearLayout removeAllSubviews];
    [self.linearLayout addSubview:metaLabel];
    [self.linearLayout layoutSubviews];
    
    NSString * recordAtDate = [self.recordedItem.startDate stringFromDateWithDateFormat:@"dd/MM/yy"];
    self.labelRecordedon.text = [NSString stringWithFormat:LS(@"whats_on_npvr_recorded_on"),recordAtDate];
    [Utils setLabelFont:self.labelRecordedon size:12 bold:YES];
    
    self.recordedonPlaceHolder.backgroundColor = APST.brandColor;
    self.recordedonPlaceHolder.layer.cornerRadius = 1;
    


    self.textViewDescription.text = self.recordedItem.recordDescription;
    [Utils setTextViewFont:self.textViewDescription size:isPad?15:14 bold:NO];
    
    self.textViewDescription.textContainer.lineFragmentPadding = 0;
    self.textViewDescription.textContainerInset = UIEdgeInsetsZero;
    
    
    if (self.textViewDescription.text.length == 0)
    {
        self.textViewDescription.text = @"";
    }


         [self.startOverButton setTitle:[NSString stringWithFormat:@" %@",LS(@"player_dialog_continue_start_over")] forState:UIControlStateNormal];
        if (isPad) {
            
            [_imgThumbBlur sd_setImageWithURL:self.recordedItem.picURL placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (!error && image) {
                    self.imgThumbBlur.image = [image blurredImageWithRadius:BlurRadius];
                }
            }];
            
            [_imgThumb superview].layer.cornerRadius = 2;
            [_imgThumb sd_setImageWithURL:self.recordedItem.picURL placeholderImage:[UIImage imageNamed:@"placeholder_2_3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
            
            [self.startOverButton setBackgroundColor:UIColorFromRGB(TVNButtonGrayColor)];
            self.startOverButton.layer.cornerRadius = 4;
            self.startOverButton.layer.shadowColor = [[UIColor blackColor] CGColor];
            self.startOverButton.layer.shadowOffset = CGSizeMake(2, 2);
       

        }
    
    
    self.textViewDescription.textColor = GRAY_COLOR;


    
}


-(IBAction)startOver:(id)sender
{
    [self.delegate playerNPVRInfoView:self didSelectStartOver:sender];
}

@end
