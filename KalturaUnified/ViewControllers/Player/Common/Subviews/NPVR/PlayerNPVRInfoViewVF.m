//
//  PlayerNPVRInfoViewVF.m
//  Vodafone
//
//  Created by Israel Berezin on 3/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerNPVRInfoViewVF.h"

@implementation PlayerNPVRInfoViewVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void) updateUI
{
    [super updateUI];
    if (isPad)
    {
        [self.startOverButton setBackgroundColor:[VFTypography instance].grayScale3Color];
    }
}


@end
