//
//  ShareContentView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "ShareContentView.h"
#import "UIImage+Tint.h"
#import "SocialManagement.h"
#import "PlayerViewController.h"

@implementation ShareContentView

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)awakeFromNib {
    
    [super awakeFromNib];

    NSArray *array = [NSArray arrayWithObjects:LS(@"facebook"), LS(@"twitter"), LS(@"mail_link"), LS(@"like"), LS(@"comment"), LS(@"favorite"), nil];
    
    for (int i = 0; i < [array count]; i++) {
        
        int tag = i + 1;
        
        NSString *title = [array objectAtIndex:i];
        UIButton *button = (UIButton *)[self viewWithTag:tag];
        UILabel *label = (UILabel *)[self viewWithTag:(tag + 10)];
    
        [self initButton:title button:button label:label];
        
        if (!APST.socialFeed && (i == 4)) {
            button.hidden = YES;
            label.hidden = YES;
        }
    }
    
    [Utils setButtonFont:_btnPrivately bold:YES];
    [self.btnPrivately setTitleColor:CS(@"bebebe") forState:UIControlStateDisabled];
    [self.btnPrivately setTitle:LS(@"watch_privately") forState:UIControlStateNormal];
    
    _viewPrivately.hidden = !APST.sharePrivacy;
    
    if (_viewPrivately.hidden) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, _viewPrivately.frame.origin.y);
    } else {
        
        _btnPrivately.userInteractionEnabled = NO;
        
    }
    
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
}

- (LikeAdapter*) likeAdapter {
    if (!_likeAdapter)
    {
        _likeAdapter = [[LikeAdapter alloc] initWithMediaItem:nil control:_buttonLike label:_labelLike];
        _likeAdapter.hideButtonTitleForLikes = YES;
    }
    
    return _likeAdapter;
}

- (FavoriteAdapter*) favoriteAdapter {
    if (!_favoriteAdapter)
        _favoriteAdapter = [[FavoriteAdapter alloc] initWithMediaItem:nil control:_buttonFavorite label:_labelFavorite];
    return _favoriteAdapter;

}

- (void)skinChanged {
    
    for (int i = 0; i < 6; i++) {
        
        int tag = i + 1;
        
        UIButton *button = (UIButton *)[self viewWithTag:tag];
        UILabel *label = (UILabel *)[self viewWithTag:(tag + 10)];
        
        label.highlightedTextColor = APST.brandColor;
        
        UIImage *image = button.imageView.image;
        [button setImage:[image imageTintedWithColor:CS(@"bebebe")] forState:UIControlStateNormal];
        UIImage *coloredBrandImage = [image imageTintedWithColor:APST.brandColor];
        [button setImage:coloredBrandImage forState:UIControlStateHighlighted];
        [button setImage:coloredBrandImage forState:UIControlStateSelected];        
    }
    
    UIImage *coloredBrandImage = [[UIImage imageNamed:@"share_check_on"] imageTintedWithColor:APST.brandColor];
    [self.btnPrivately setImage:coloredBrandImage forState:UIControlStateHighlighted];
    [self.btnPrivately setImage:coloredBrandImage forState:UIControlStateSelected];
}

- (void)setupWithMedia:(TVMediaItem *)mediaItem {

    [self.likeAdapter setupMediaItem:mediaItem];
    self.likeAdapter.hideButtonTitleForLikes = YES;
    [self.favoriteAdapter setupMediaItem:mediaItem];

    self.shareMediaItem = mediaItem;
    self.isLive = mediaItem.isLive;
    
    NSString *iconFileName = (_isLive ? @"share_favorite_live_icon" : @"share_favorite_icon");
    
    [_buttonFavorite setImage:[UIImage imageNamed:iconFileName] forState:UIControlStateNormal];
    UIImage *image = _buttonFavorite.imageView.image;
    [_buttonFavorite setImage:[image imageTintedWithColor:CS(@"bebebe")] forState:UIControlStateNormal];
    
    UIImage *coloredBrandImage = [[UIImage imageNamed:iconFileName] imageTintedWithColor:APST.brandColor];
    [_buttonFavorite setImage:coloredBrandImage forState:UIControlStateHighlighted];
    [_buttonFavorite setImage:coloredBrandImage forState:UIControlStateSelected];

 
    NSString *title = _isLive ? (mediaItem.isFavorite ? LS(@"bookmarked") : LS(@"bookmark")) : LS(@"favorite");
    UILabel *label = (UILabel *)[self viewWithTag:(_buttonFavorite.tag + 10)];
    
    [self initButton:title button:_buttonFavorite label:label];
    
    if (LoginM.externalShareWatches) {
        _btnPrivately.selected = [LoginM.externalShareWatches isEqualToString:@"DONT_ALLOW"];
    }
    
    if (mediaItem.isSeries) {
        _viewPrivately.hidden = YES;
    }
}

- (void)initButton:(NSString *)title button:(UIButton *)button label:(UILabel *)label {
    
    [button addTarget:self action:@selector(socialShareButtonTouchDown:) forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragInside];
    [button addTarget:self action:@selector(socialShareButtonTouchCancel:) forControlEvents:UIControlEventTouchCancel | UIControlEventTouchDragOutside];
    
    label.text = title;
    [Utils setLabelFont:label bold:YES];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = CS(@"bebebe");
    label.adjustsFontSizeToFitWidth = YES;
    
    UIImage *image = button.imageView.image;
    [button setImage:[image imageTintedWithColor:CS(@"bebebe")] forState:UIControlStateNormal];
    
}

#pragma mark - Buttons

- (IBAction)buttonSharePressed:(UIButton *)button {
    
    [self button:button shouldHighlighted:NO];
    
    if (button.tag == PlayerActionTypeLike) {
        
    } else if (button.tag == PlayerActionTypeFavorite) {
        
    } else {
        [_delegate shareAction:button.tag];
    }
    
}

- (IBAction)buttonPrivacyPressed:(UIButton *)button {
    
    _btnPrivately.selected = !_btnPrivately.selected;
}


- (void)socialShareButtonTouchDown:(UIButton *)button {
    [self button:button shouldHighlighted:YES];
}

- (void)socialShareButtonTouchCancel:(UIButton *)button {
    [self button:button shouldHighlighted:NO];
}

#pragma mark - Logic UI

- (void)button:(UIButton *)button shouldHighlighted:(BOOL)highlighted {
    UILabel *lbl = (UILabel *)[self viewWithTag:(button.tag + 10)];
    lbl.highlighted = highlighted | button.selected;
    if (button.tag == PlayerActionTypeLike) {
        lbl.text = (highlighted | button.selected) ? LS(@"liked") : LS(@"like");
    } else if(button.tag == PlayerActionTypeFavorite) {
        if (self.isLive) {
            lbl.text = (highlighted | button.selected) ? LS(@"bookmarked") : LS(@"bookmark");
        } else {
            lbl.text = LS(@"favorite");
            
        }
    }
}

@end
