//
//  ShareView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"

@protocol ShareViewDelegate

- (void)closeView;

@end

@interface ShareView : UINibView

@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UIButton *buttonClose;

@property (nonatomic, weak) IBOutlet UIView *viewContent;

@property (nonatomic, weak) id<ShareViewDelegate> delegate;

@end
