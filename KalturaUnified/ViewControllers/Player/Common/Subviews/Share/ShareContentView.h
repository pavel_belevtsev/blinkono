//
//  ShareContentView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"
#import "LikeAdapter.h"
#import "FavoriteAdapter.h"

@protocol ShareContentViewDelegate <NSObject>

- (void)shareAction:(NSInteger)socialAction;

@end

@interface ShareContentView : UINibView

- (void)setupWithMedia:(TVMediaItem *)mediaItem;

@property (nonatomic, weak) IBOutlet UIButton *buttonFacebook;
@property (nonatomic, weak) IBOutlet UIButton *buttonTwitter;
@property (nonatomic, weak) IBOutlet UIButton *buttonMail;
@property (nonatomic, weak) IBOutlet UIButton *buttonLike;
@property (nonatomic, weak) IBOutlet UIButton *buttonComment;
@property (nonatomic, weak) IBOutlet UIButton *buttonFavorite;
@property (nonatomic, weak) IBOutlet UILabel *labelFacebook;
@property (nonatomic, weak) IBOutlet UILabel *labelTwitter;
@property (nonatomic, weak) IBOutlet UILabel *labelMail;
@property (nonatomic, weak) IBOutlet UILabel *labelLike;
@property (nonatomic, weak) IBOutlet UILabel *labelComment;
@property (nonatomic, weak) IBOutlet UILabel *labelFavorite;

@property (nonatomic, weak) IBOutlet UIView *viewPrivately;
@property (nonatomic, weak) IBOutlet UIButton *btnPrivately;

@property BOOL isLive;

@property (nonatomic, weak) id <ShareContentViewDelegate> delegate;
@property (nonatomic, strong) LikeAdapter *likeAdapter;
@property (nonatomic, strong) FavoriteAdapter *favoriteAdapter;

@property (nonatomic, strong) TVMediaItem *shareMediaItem;

@end
