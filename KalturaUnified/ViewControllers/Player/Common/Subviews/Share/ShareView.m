//
//  ShareView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "ShareView.h"

@implementation ShareView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [Utils setLabelFont:_labelTitle bold:YES];
    _labelTitle.text = LS(@"share");
    
    ((UIView *)[[self subviews] objectAtIndex:0]).layer.cornerRadius = 5.f;
    
}

- (IBAction)buttonClosePressed:(UIButton *)button {
   
    [self.delegate closeView];
    //[self removeFromSuperview];
    
}

@end
