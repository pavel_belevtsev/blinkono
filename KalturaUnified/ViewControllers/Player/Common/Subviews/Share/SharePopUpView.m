//
//  SharePopUpView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "SharePopUpView.h"

@implementation SharePopUpView

- (instancetype)initWithArrowDirection:(SharePopUpViewArrowDirection) direction contentView:(UIView*) contentView {
    self = [[[NSBundle mainBundle] loadNibNamed:@"SharePopUpView" owner:self options:nil] firstObject];
    if (self) {
        [self createViews:direction contentView:contentView];
    }
    return self;
}

- (void) createViews:(SharePopUpViewArrowDirection) direction contentView:(UIView*) contentView {

    UIImageView *imgLeft = [UIImageView new];
    imgLeft.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:imgLeft];
    
    UIImageView *imgMiddle = [UIImageView new];
    imgMiddle.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:imgMiddle];
    
    UIImageView *imgRight = [UIImageView new];
    imgRight.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:imgRight];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imgLeft]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgLeft)]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imgMiddle]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgMiddle)]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imgRight]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgRight)]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imgRight attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:imgMiddle attribute:NSLayoutAttributeTopMargin multiplier:1 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:imgLeft attribute:NSLayoutAttributeTopMargin relatedBy:NSLayoutRelationEqual toItem:imgMiddle attribute:NSLayoutAttributeTopMargin multiplier:1 constant:0]];
    
    contentView.translatesAutoresizingMaskIntoConstraints = NO;
    contentView.userInteractionEnabled = YES;
    [self addSubview:contentView];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[contentView]-15-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(contentView)]];

    NSInteger contentTopOffset = 0;
    switch (direction) {
        case kSharePopUpViewArrowDirectionDown:
            [imgLeft setImage:[UIImage imageNamed:@"share_bubble_right_down-1"]];
            [imgMiddle setImage:[UIImage imageNamed:@"share_bubble_middle_down-1"]];
            [imgRight setImage:[UIImage imageNamed:@"share_bubble_left_down-1"]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imgLeft(==imgRight)][imgMiddle][imgRight]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgLeft, imgMiddle, imgRight)]];

            contentTopOffset = 15;
            break;
        case kSharePopUpViewArrowDirectionUp:
            [imgLeft setImage:[UIImage imageNamed:@"share_bubble_left_up-1"]];
            [imgMiddle setImage:[UIImage imageNamed:@"share_bubble_middle_up-1"]];
            [imgRight setImage:[UIImage imageNamed:@"share_bubble_right_up-1"]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imgLeft(>=160)][imgMiddle][imgRight]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imgLeft, imgMiddle, imgRight)]];

            contentTopOffset = 40;
            break;
        default:
            break;
    }
    NSDictionary *metrics = @{@"top":@(contentTopOffset)};
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[contentView]->=0-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(contentView)]];
    [self addConstraint: _constraintContentHeight = [NSLayoutConstraint constraintWithItem:contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:contentView.height]];
}
@end
