//
//  SharePopUpView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 14.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BasePopUp.h"

typedef enum SharePopUpViewArrowDirection
{
    kSharePopUpViewArrowDirectionDown,
    kSharePopUpViewArrowDirectionUp,

} SharePopUpViewArrowDirection;

@interface SharePopUpView : BasePopUp

@property (nonatomic, retain) NSLayoutConstraint *constraintContentHeight;

- (instancetype)initWithArrowDirection:(SharePopUpViewArrowDirection) direction contentView:(UIView*) contentView;
@end
