//
//  PlayerLiveEPGFlowLayout.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerLiveEPGFlowLayout : UICollectionViewFlowLayout

@end
