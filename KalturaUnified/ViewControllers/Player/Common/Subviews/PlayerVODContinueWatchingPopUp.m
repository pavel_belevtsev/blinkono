//
//  PlayerVODContinueWatchingPopUp.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 19.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerVODContinueWatchingPopUp.h"
#import "UIImage+Tint.h"
#import "UIView+Additions.h"
#import "APSComplexButton.h"

@interface PlayerVODContinueWatchingPopUp ()

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) IBOutlet UIImageView *backgroundImage;
@property (nonatomic, strong) IBOutlet APSComplexButton *btnContinueFrom;
@property (nonatomic, strong) IBOutlet APSComplexButton *btnSTBContinueFrom;
@property (nonatomic, strong) IBOutlet APSComplexButton *btnStartOver;
@property (nonatomic, strong) TVMediaItem *mediaItem;
@property (nonatomic, assign) NSInteger startTime;
@property (nonatomic, assign) NSInteger STBstartTime;



@end

NSString * const PlayerVODContinueWatchingPopUpButtonsBgImageName = @"brand_button";
const NSInteger PlayerVODContinueWatchingPopUpButtonsFontSize = 16;
const CGFloat PlayerVODContinueWatchingPopUpBluredImageTransparancy = 0.7;
const CGFloat PlayerVODContinueWatchingPopUpBlackTransparancy = 0.5;

@implementation PlayerVODContinueWatchingPopUp

- (id)initWithMedia:(TVMediaItem *)mediaItem continueFrom:(NSInteger)startTime {
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    if (self) {
        self.mediaItem = mediaItem;
        self.startTime = startTime;
        [self configUI];
    }
    return self;
}

- (id)initWithMedia:(TVMediaItem *)mediaItem continueFrom:(NSInteger)startTime  STBcontinueFrom:(NSInteger)STBstartTime
{
    self = [self PlayerVODContinueWatchingSTBPopUp];
    if (self) {
        self.mediaItem = mediaItem;
        self.startTime = startTime;
        self.STBstartTime = STBstartTime;
        [self configUI];
    }
    return self;
}

-(PlayerVODContinueWatchingPopUp *) PlayerVODContinueWatchingSTBPopUp
{
    PlayerVODContinueWatchingPopUp *view = nil;
    NSString *nibName = @"PlayerVODContinueWatchingSTBPopUp";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:[self class]])
        {
            view = object;
            break;
        }
    }
    return view;
}

- (void)configUI {
    
    UIImage *image = [UIImage imageNamed:@"followme_bg"];
    
    _backgroundImage.image = [image stretchableImageWithLeftCapWidth:image.size.width / 2.0 topCapHeight:image.size.height / 2.0];
    
    [self displayContinueFromBtnUI];
    [self displayStartOverBtnUI];
    [self displayButtonSTBContinueBtnUI];
    
    [self addTapToCloseGestureRecognizer];
}

- (void)displayContinueFromBtnUI {
    
    NSString *title = [NSString stringWithFormat:@"%@ %@",
                       LS(@"player_vod_continue_from"),
                       [self timeFormatted:self.startTime]];
    [_btnContinueFrom setTitle:title forState:UIControlStateNormal];
    [_btnContinueFrom setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
    _btnContinueFrom.titleLabel.font = Font_Reg(PlayerVODContinueWatchingPopUpButtonsFontSize);
    
    [_btnContinueFrom setBackgroundImage:[self bgImageForButtonWithColor:APST.brandColor] forState:UIControlStateNormal];
    
    UIImage *btnImage = [UIImage imageNamed:@"stb_play"];
    [self addImage:btnImage toButton:_btnContinueFrom withTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
}

- (void)displayStartOverBtnUI {
    
    [_btnStartOver setTitle:LS(@"player_vod_start_over") forState:UIControlStateNormal];
    [_btnStartOver setTitleColor:CS(@"bebebe") forState:UIControlStateNormal];
    _btnStartOver.titleLabel.font = Font_Reg(PlayerVODContinueWatchingPopUpButtonsFontSize);
    [_btnStartOver setTitleColor:CS(@"bebebe") forState:UIControlStateNormal];
    
    [_btnStartOver setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"4e4e4e")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)] forState:UIControlStateNormal];
    
    UIImage *btnImage = [[[UIImage imageNamed:@"stb_startover"] imageTintedWithColor:CS(@"bebebe")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [self addImage:btnImage toButton:_btnStartOver withTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
}

- (void)displayButtonSTBContinueBtnUI
{
    NSString *title = [NSString stringWithFormat:@"%@ %@",
                       LS(@"player_vod_continue_from"),
                       [self timeFormatted:self.STBstartTime]];
    [_btnSTBContinueFrom setTitle:title forState:UIControlStateNormal];
    [_btnSTBContinueFrom setTitleColor:CS(@"bebebe") forState:UIControlStateNormal];
    _btnSTBContinueFrom.titleLabel.font = Font_Reg(PlayerVODContinueWatchingPopUpButtonsFontSize);
    
    [_btnSTBContinueFrom setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"4e4e4e")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)] forState:UIControlStateNormal];
    
    UIImage *btnImage =[[[UIImage imageNamed:@"stb_tv"] imageTintedWithColor:CS(@"bebebe")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [self addImage:btnImage toButton:_btnSTBContinueFrom withTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
}

- (void)addTapToCloseGestureRecognizer {
    UITapGestureRecognizer* tapToCloseRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundViewPressed:)];
    [self.backgroundView addGestureRecognizer:tapToCloseRecognizer];
}

-(void)addImage:(UIImage*)image toButton:(APSComplexButton *)button withTitleEdgeInsets:(UIEdgeInsets)titleEdgeInsets
{
    UIImageView * imgFilter = [[UIImageView alloc] initWithImage:image];
    imgFilter.center = CGPointMake(button.frame.origin.x + 5.0, button.frame.size.height / 2.0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    [button addSubview:imgFilter];
}


- (UIImage *)bgImageForButtonWithColor:(UIColor *)color {
    
    UIImage *brandImage = [UIImage imageNamed:PlayerVODContinueWatchingPopUpButtonsBgImageName];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:color];
    return [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
}

-(UIImage *)imageName:(NSString *)imageName color:(UIColor *)color
{
    UIImage *brandImage = [UIImage imageNamed:imageName];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:color];
    return [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
}


#pragma mark - IBActions

- (IBAction)btnContinueFromPressed {
    [self.delegate playerVODContinueWatchingContinueFrom:self.startTime];
    [self removeFromSuperview];
}

- (IBAction)btnStartOverPressed {
    [self.delegate playerVODContinueWatchingStartOverPressed];
    [self removeFromSuperview];
}

- (IBAction)btnSTBContinueFromPressed:(id)sender {
    [self closePopUp:nil];
    [self.delegate playerVODContinueWatchingContinueFrom:self.STBstartTime];
}

- (void)backgroundViewPressed:(id)sender {
    [self closePopUp:nil];
    [self.delegate playerVODContinueWatchingStartOverClosed];
}

#pragma mark - Help functions


- (NSString *)timeFormatted :(NSInteger)time {
    
    NSInteger seconds = time % 60;
    NSInteger minutes = (time / 60) % 60;
    NSInteger hours = time / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",(int)hours, (int)minutes, (int)seconds];
}

- (IBAction)closePopUp:(id)sender
{
    self.alpha = 1.0;
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.alpha = 1.0;
    }];
}
@end
