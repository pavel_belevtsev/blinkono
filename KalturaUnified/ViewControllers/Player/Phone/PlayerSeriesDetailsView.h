//
//  PlayerSeriesDetailsView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LikeAdapter.h"
#import "FavoriteAdapter.h"

@protocol PlayerSeriesDetailsViewDelegate <NSObject>

- (void)buttonShareSeriesPressed;

@end

@interface PlayerSeriesDetailsView : UINibView

- (void)updateData:(TVMediaItem *)item;

@property (nonatomic, strong) TVMediaItem *item;
@property (nonatomic, strong) LikeAdapter *likeAdapter;
@property (nonatomic, strong) FavoriteAdapter *favoriteAdapter;

@property (nonatomic, weak) IBOutlet UILabel *labelName;
@property (nonatomic, weak) IBOutlet UIButton *buttonFavorite;
@property (nonatomic, weak) IBOutlet UIButton *buttonShare;
@property (nonatomic, weak) IBOutlet UIButton *buttonLike;

@property (nonatomic, weak) IBOutlet UILabel *labelDescription;
@property (nonatomic, weak) IBOutlet UILabel *labelTags;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewContent;

@property (nonatomic, weak) id <PlayerSeriesDetailsViewDelegate> delegate;

@end
