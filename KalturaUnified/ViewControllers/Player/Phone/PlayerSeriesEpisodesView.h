//
//  PlayerSeriesEpisodesView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlayerViewController;

@interface PlayerSeriesEpisodesView : UINibView

- (void)updateData:(NSArray *)array;

@property (nonatomic, strong) NSArray *episodesList;
@property (nonatomic, strong) IBOutlet UITableView *tableViewEpisodes;
@property (nonatomic, weak) PlayerViewController *delegate;




@end
