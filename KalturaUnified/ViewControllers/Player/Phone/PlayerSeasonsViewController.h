//
//  PlayerSeasonsViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 09.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlayerViewController;

@interface PlayerSeasonsViewController : BaseViewController {
    
    CGFloat maxFilterTableHeight;
        
}

- (void)reloadData;

@property (nonatomic, weak) PlayerViewController *delegate;

@property (nonatomic, strong) NSString *seriesName;
@property (nonatomic) NSInteger currentSeason;

@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *labelSeasons;

@property (weak, nonatomic) IBOutlet UITableView *tableViewFilter;

@property (strong, nonatomic) NSArray *seasons;

@property (weak, nonatomic) IBOutlet UIView *viewSort;

@end
