//
//  PlayerVODDetailsView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerSeriesEpisodesView.h"
#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ImageEffects.h"
#import "UIImage+Blur.h"
#import "PlayerEpisodeTableViewCell.h"
#import "PlayerViewController.h"

@implementation PlayerSeriesEpisodesView

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
    
}

- (void)skinChanged {
    
    [_tableViewEpisodes reloadData];
    
}


- (void)updateData:(NSArray *)array {

    self.episodesList = array;
    
    [_tableViewEpisodes reloadData];
    [_tableViewEpisodes scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
}

#pragma mark - UITableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _episodesList.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"PlayerEpisodeTableViewCell";
    PlayerEpisodeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:[Utils nibName:cellIdentifier] owner:self options:nil] objectAtIndex:1];
        
    }
    
    UIView *selectedBGView = [[UIView alloc] init];
    selectedBGView.backgroundColor = APST.brandColor;
    cell.selectedBackgroundView = selectedBGView;

    TVMediaItem *mediaItem = [_episodesList objectAtIndex:indexPath.row];
    [cell updateData:mediaItem episodesList:YES];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TVMediaItem *mediaItem = [_episodesList objectAtIndex:indexPath.row];
    [_delegate selectMediaItem:mediaItem];
    
}

@end
