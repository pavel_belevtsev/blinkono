//
//  PlayerVODDetailsView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerVODDetailsView.h"
#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ImageEffects.h"
#import "UIImage+Blur.h"
#import <TvinciSDK/TVRental.h>
#import "TVRental+Additions.h"
#import "PPVFullData.h"

@interface PlayerVODDetailsView () {
    YLProgressBar *downloadProgress;
    UIImageView *downloadStatusImage;
}

@property (nonatomic, strong) UIImageView *buttonPlayIcon;
@property (nonatomic, strong) UILabel *buttonPlayLabel;
@property (nonatomic, strong) NSLayoutConstraint *constraintButtonPlayIconWidth;
@property (weak, nonatomic) IBOutlet UIView *likePlaceHolder;

@end

@implementation PlayerVODDetailsView

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
        
    buttonState = kVODButtonInitial;
    
    [Utils setLabelFont:_labelName bold:YES];
    [Utils setLabelFont:_labelSubName];
    [Utils setLabelFont:_labelDescription];
    
    if (isPad) {

        [Utils setLabelFont:_labelRating bold:YES];
        
        _labelRating.layer.borderColor = CS(@"999999").CGColor;
        _labelRating.layer.borderWidth = 1.0f;
        _labelRating.layer.cornerRadius = 3.0f;
    }
    
    [Utils setLabelFont:_labelTags bold:YES];
        
    [_buttonLike setImage:[UIImage imageNamed:@"details_like_icon"] forState:UIControlStateNormal];
        
    [self skinChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
}

- (LikeAdapter*) likeAdapter {
    if (!_likeAdapter)
        _likeAdapter = [[LikeAdapter alloc] initWithMediaItem:nil control:_buttonLike label:nil imgLike:_imgLikeCounter labelCounter:_labelLikeCounter];
    return _likeAdapter;
}

- (void)skinChanged {
    
    [_buttonTrailer setImage:[[UIImage imageNamed:@"details_play_gray_icon"] imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
    if (isPad ) {
        [_buttonTrailer setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"575757")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)] forState:UIControlStateNormal];
        _buttonTrailer.layer.cornerRadius = 5;
    }
    else {
        [_buttonTrailer setBackgroundColor:CS(@"575757")];
    }

    [_buttonTrailer setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
    [Utils setButtonFont:_buttonTrailer bold:YES];
    
    _viewBar.backgroundColor = APST.brandColor;
    
    UILabel *option = (UILabel *)[_buttonPlay viewWithTag:optionButtonLabelTag];
    option.textColor = APST.brandTextColor;

    _buttonPlayLabel.textColor = APST.brandTextColor;
    _buttonPlay.backgroundColor = APST.brandColor;
 
    [self updatePlayButtonWithState:buttonState];
    
    NSString *imageName = @"details_like_icon";
    UIImage *image = [UIImage imageNamed:imageName];
    
    [_buttonLike setImage:[image imageTintedWithColor:CS(@"bebebe")] forState:UIControlStateNormal];
    
    if (isPad) {
        
        _labelLikeCounter.textColor = APST.brandTextColor;

        [_buttonLike setImage:[image imageTintedWithColor:APST.brandTextColor] forState:UIControlStateSelected];
        [_buttonLike setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
        [_buttonLike setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
        
        _imgLikeCounter.image = [[UIImage imageNamed:@"btn_like_counter_base"] imageTintedWithColor:APST.brandColor];

    } else {

        [_buttonLike setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
        [_buttonLike setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
        [_buttonLike setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
        [_buttonLike setTitleColor:APST.brandColor forState:UIControlStateSelected];
    }

    _buttonLike.hidden = [APP_SET isOffline]? YES:_buttonLike.hidden;
    _buttonDownload.hidden = [APP_SET isOffline]? YES:_buttonDownload.hidden;
    self.likePlaceHolder.hidden=  [APP_SET isOffline]? YES:self.likePlaceHolder.hidden;

}

- (void)updatePlayButtonWithState:(VODButtonState)state {
    [self updatePlayButtonWithState:state subscriptionsPurchased:nil subscriptionsToPurchase:nil ppvRentalInfo:nil ppvData:nil];
}

- (void)updatePlayButtonWithState:(VODButtonState)state subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase ppvRentalInfo:(TVRental*) rentalInfo ppvData:(TVPPVModelData *)ppvData{
    [self updatePlayButtonWithState:state subscriptionsPurchased:nil subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:rentalInfo ppvData:ppvData];

}
- (void)updatePlayButtonWithState:(VODButtonState)state subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase subscriptionsPurchased:(NSArray *)arrSubscriptionsPurchased {
    [self updatePlayButtonWithState:state subscriptionsPurchased:arrSubscriptionsPurchased subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:nil ppvData:nil];
}

- (void)updatePlayButtonWithState:(VODButtonState)state subscriptionsPurchased:(NSArray *)arrSubscriptionsPurchased subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase ppvRentalInfo: (TVRental*) rentalInfo ppvData:(TVPPVModelData *)ppvData{
    
    if (!_buttonPlay) {
        if (isPad ) {
            if ([self.delegate isMediaAllowedForDownloading:_item] && ![APP_SET isOffline]) {
                [self createDownloadButtonSubviews:state];
            }
        }
        return;
    }
    
    if ([self.delegate isMediaAllowedForDownloading:_item])
        [self createDownloadButtonSubviews: state];
    else {
        _constraintDownloadButtonWidth.constant = 0;
        _constraintDownloadButtonRightSpace.constant = 0;
    }
    
    [self.delegate updateCurrentMediaState: state];

    [_buttonPlay removeAllSubviews];
    _buttonPlay.enabled = YES;

    buttonState = state;

    _buttonPlayLabel = [UILabel new];
    _buttonPlayLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _buttonPlayLabel.backgroundColor = [UIColor clearColor];
    _buttonPlayLabel.textColor = APST.brandTextColor;
    
    CGFloat alpha = 1.f;
    UIImage *icon = nil;
    NSString *buttonTitle = nil;
    NSAttributedString *buttonAttributedTitle = nil;
    
    __weak PlayerVODDetailsView *sself = self;
    void(^block)(UIButton *btn) = ^(UIButton *btn) {
        [self.delegate buttonPlayPressed];
    };
    
    //remove subscription days left view until needed
    _constraintSubscriptionLeftDaysHeight.constant = 0;
    [_viewSubscriptionDaysLeft removeAllSubviews];
    
    switch (state) {
        case kVODButtonInitial:
        {
            //set temp loading state to button
            buttonTitle = LS(@"loading");
            block = nil;
            [self.delegate removeActivityIndicator];
        }
            break;
        case kVODButtonShouldPlayState:
        {
            icon = [UIImage imageNamed:@"details_play_icon"];
            buttonTitle = LS(@"vod_info_watch_now");
            id data = [arrSubscriptionsPurchased firstObject];
            if( [data isKindOfClass:[TVSubscriptionData class]])
            {
                TVSubscriptionData *subscriptionData = [arrSubscriptionsPurchased firstObject];
                if (subscriptionData && !subscriptionData.isRecurring)
                {
                    UIImageView *imageClock = [UIImageView new];
                    imageClock.translatesAutoresizingMaskIntoConstraints = NO;
                    [imageClock setImage:[UIImage imageNamed:@"clock_daysleft_icon_smartphone"]];
                    [_viewSubscriptionDaysLeft addSubview:imageClock];
                    [_viewSubscriptionDaysLeft addConstraint:[NSLayoutConstraint constraintWithItem:imageClock
                                                                                 attribute:NSLayoutAttributeCenterY
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:_viewSubscriptionDaysLeft
                                                                                 attribute:NSLayoutAttributeCenterY
                                                                                multiplier:1.0
                                                                                  constant:0.0]];
                    
                    UILabel *lblSubscriptionDaysLeft = [UILabel new];
                    lblSubscriptionDaysLeft.translatesAutoresizingMaskIntoConstraints = NO;
                    lblSubscriptionDaysLeft.text = [SubscriptionM getNRSubscriptionLeftPeriod:subscriptionData];
                    lblSubscriptionDaysLeft.textAlignment = NSTextAlignmentLeft;
                    lblSubscriptionDaysLeft.textColor = CS(@"959595");
                    [Utils setLabelFont:lblSubscriptionDaysLeft size:12 bold:NO];

                    [_viewSubscriptionDaysLeft addSubview:lblSubscriptionDaysLeft];
                    [_viewSubscriptionDaysLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imageClock]-5-[lblSubscriptionDaysLeft]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblSubscriptionDaysLeft, imageClock)]];
                    [_viewSubscriptionDaysLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[lblSubscriptionDaysLeft]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblSubscriptionDaysLeft)]];

                    _constraintSubscriptionLeftDaysHeight.constant = 20;
                }
            }
            else if([data isKindOfClass:[PPVFullData class]] || (rentalInfo != nil && rentalInfo.purchaseDate != nil))
            {
                TVRental * rental = rentalInfo;
                
                if( [data isKindOfClass:[PPVFullData class]])
                {
                    PPVFullData * fullppvData = (PPVFullData*)[arrSubscriptionsPurchased firstObject];
                    rental = fullppvData.rentalInfo;
                }
                UIImageView *imageClock = [UIImageView new];
                imageClock.translatesAutoresizingMaskIntoConstraints = NO;
                [imageClock setImage:[UIImage imageNamed:@"clock_daysleft_icon_smartphone"]];
                [_viewSubscriptionDaysLeft addSubview:imageClock];
                [_viewSubscriptionDaysLeft addConstraint:[NSLayoutConstraint constraintWithItem:imageClock
                                                                                      attribute:NSLayoutAttributeCenterY
                                                                                      relatedBy:NSLayoutRelationEqual
                                                                                         toItem:_viewSubscriptionDaysLeft
                                                                                      attribute:NSLayoutAttributeCenterY
                                                                                     multiplier:1.0
                                                                                       constant:0.0]];
                
                UILabel *lblSubscriptionDaysLeft = [UILabel new];
                lblSubscriptionDaysLeft.translatesAutoresizingMaskIntoConstraints = NO;
                lblSubscriptionDaysLeft.text = [PPVM getNR_PPVLeftPeriod:rental];
                lblSubscriptionDaysLeft.textAlignment = NSTextAlignmentLeft;
                lblSubscriptionDaysLeft.textColor = CS(@"959595");
                [Utils setLabelFont:lblSubscriptionDaysLeft size:12 bold:NO];
                
                [_viewSubscriptionDaysLeft addSubview:lblSubscriptionDaysLeft];
                [_viewSubscriptionDaysLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imageClock]-5-[lblSubscriptionDaysLeft]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblSubscriptionDaysLeft, imageClock)]];
                [_viewSubscriptionDaysLeft addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[lblSubscriptionDaysLeft]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblSubscriptionDaysLeft)]];
                
                _constraintSubscriptionLeftDaysHeight.constant = 20;
              }
            else {
                //nothing
            }
        }
            break;
        case kVODButtonPurchaseState:
        {
            if (APST.hasInAppPurchases) {
                switch (arrSubscriptionsToPurchase.count) {
                    case 0:
                    {
                        buttonAttributedTitle = [self.delegate attributedActionButtonTitleForState:kVODSubscriptionButtonEmpty subscriptionPrice:nil ppvPrice:nil baseFontSize:14];
                        if (rentalInfo != nil && rentalInfo.purchaseDate==nil)
                        {
                            VODSubscriptionButtonState subscriptionTypeState = kVODSubscriptionButtonTVODsingle;
                            TVSubscriptionData *subscriptionData = [TVSubscriptionData new];
                            subscriptionData.isRecurring = NO;
                            subscriptionData.productCode = rentalInfo.productCode;
                            subscriptionData.priceCode = [TVSubscriptionData_subscriptionPriceCode new];
                            NSString * price = rentalInfo.price;
                            subscriptionData.priceCode.prise = [TVSubscriptionData_prise new];
                            subscriptionData.priceCode.prise.price = [price floatValue];
                            subscriptionData.priceCode.prise.name =rentalInfo.currencySign;
                            
                            buttonAttributedTitle = [self.delegate attributedActionButtonTitleForState:subscriptionTypeState subscriptionPrice:nil ppvPrice:ppvData.PPVPriceCode.prise baseFontSize:14];
                            
                            block = ^(UIButton *btn) {
                                PPVFullData * fullData =  [PPVFullData new];
                                fullData.ppvData = ppvData;
                                fullData.rentalInfo = rentalInfo;
                                [self.delegate initializePurchaseFlow:fullData];
                            };
                        }

                    }
                        break;
                    case 1:
                    {
                        TVSubscriptionData *subscriptionData = [arrSubscriptionsToPurchase firstObject];
                        VODSubscriptionButtonState subscriptionTypeState = (subscriptionData.isRecurring) ? kVODSubscriptionButtonSVODsingle : kVODSubscriptionButtonTVODsingle;

                        buttonAttributedTitle = [self.delegate attributedActionButtonTitleForState:subscriptionTypeState subscriptionPrice:subscriptionData.priceCode.prise ppvPrice:nil baseFontSize:14];

                        block = ^(UIButton *btn) {
                            [sself.delegate initializePurchaseFlow:subscriptionData];
                        };
                    }
                        break;
                    default:
                    {
                        NSNumber *minSubscriptionPrice = [arrSubscriptionsToPurchase valueForKeyPath:@"@min.priceCode.prise.price"];
                        TVSubscriptionData_subscriptionPriceCode *subscriptionPriceMinimum = [[[arrSubscriptionsToPurchase filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"priceCode.prise.price == %f", [minSubscriptionPrice floatValue]]] firstObject] priceCode];
                        buttonAttributedTitle = [self.delegate attributedActionButtonTitleForState:kVODSubscriptionButtonSVODmulti subscriptionPrice:subscriptionPriceMinimum.prise ppvPrice:nil baseFontSize:14];
                        block = ^(UIButton *btn) {
                            SubscriptionsListViewController *controller = [[ViewControllersFactory defaultFactory] subscriptionsListViewController];
                            controller.arrSubscriptionsIDsContainingSelectedMedia = [arrSubscriptionsToPurchase valueForKeyPath:@"subscriptionCode"];
                            [[sself.delegate getNavigationController] pushViewController:controller animated:YES];
                        };
                    }
                        break;
                }
            }
            else {
                buttonTitle = LS(@"purchase");
            }
        }
            break;
        case kVODButtonPausedState:
        {
            icon = [UIImage imageNamed:@"details_play_icon"];
            if (isPad)
            {
                buttonTitle = LS(@"vod_info_resume_watching");
            }
            else
            {
                buttonTitle = LS(@"vod_info_resume_watching_phone");
            }
            block = ^(UIButton *btn) {
                [self.delegate   buttonResumWatchingPressed];
            };
        }
            break;
        case kVODButtonPlayngState:
        {
            buttonTitle = LS(@"vod_info_playing_now");
            alpha = 0.5f;
            _buttonPlay.enabled = NO;
        }
            break;
        case kVODButtonAddedToWatchListState:
        {
            buttonTitle = LS(@"vod_info_added_to_watch_list");
            _buttonPlayLabel.backgroundColor = APST.brandColor;
            _buttonPlayLabel.textColor = APST.brandTextColor;
            block = ^(UIButton *btn) {
                [self.delegate removeFromWatchList];
            };
        }
            break;
        case kVODButtonAddToWatchListState:
        {
            buttonTitle = LS(@"vod_info_add_to_watch_list");
            block = ^(UIButton *btn) {
                [self.delegate addToWatchList];
            };
        }
            break;
        default:
        {
        }
            break;
    }
    
    if (buttonTitle || buttonAttributedTitle) {
        if (buttonTitle) {
            _buttonPlayLabel.attributedText = nil;
            _buttonPlayLabel.text = buttonTitle;
        }
        else if (buttonAttributedTitle) {
            _buttonPlayLabel.text = nil;
            _buttonPlayLabel.attributedText = buttonAttributedTitle;
        }
        
        _buttonPlayLabel.textAlignment = NSTextAlignmentCenter;
        [Utils setLabelFont:_buttonPlayLabel size:14 bold:YES];
        [_buttonPlay addSubview:_buttonPlayLabel];
        [_buttonPlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_buttonPlayLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_buttonPlayLabel)]];
        
        _buttonPlayIcon = nil;
        if (icon) {
            _buttonPlayIcon = [UIImageView new];
            _buttonPlayIcon.translatesAutoresizingMaskIntoConstraints = NO;
            _buttonPlayIcon.image = icon;
            _buttonPlayIcon.tag = optionButtonIconTag;
            _buttonPlayIcon.contentMode = UIViewContentModeCenter;
            _buttonPlayIcon.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
            [_buttonPlay addSubview:_buttonPlayIcon];
            NSDictionary *metrics = @{@"iconH": @(32), @"iconW": @(32)};
            [_buttonPlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_buttonPlayIcon(iconH)]|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_buttonPlayIcon)]];
            [_buttonPlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_buttonPlayIcon(iconW)][_buttonPlayLabel]|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_buttonPlayIcon, _buttonPlayLabel)]];
        }
        else
            [_buttonPlay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_buttonPlayLabel]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_buttonPlayLabel)]];
    }
    
    [_buttonPlay setTouchUpInsideAction:block];
    _buttonPlay.alpha = alpha;
    
    [self.delegate finishedUpdatingActionButtonForState:state];
}

- (void)updateData:(TVMediaItem *)item {

    self.item = item;
    [self.likeAdapter setupMediaItem:item];
    [self createLabels];
    
    _buttonLike.userInteractionEnabled  = [LoginM isSignedIn];
}

- (void)createLabels {
    //reset button state
    buttonState = kVODButtonInitial;

    self.downloadedOnLabel.text = ((KADownloadItem *)self.item.downloadItemRef).updateDateString && [APP_SET isOffline]?[NSString stringWithFormat:@"%@ %@",LS(@"Downloaded on"),((KADownloadItem *)self.item.downloadItemRef).updateDateString]:nil;
    self.downloadedOnLabel.backgroundColor = isPad?APST.brandColor:self.downloadedOnLabel.backgroundColor;

    _scrollViewContent.contentOffset = CGPointZero;
    
    _labelName.text = _item.name;    
    _labelSubName.text = @"";
    if ([_item.mediaTypeName isEqualToString:@"episode"]) {
        
        int season = [[_item getMediaItemSeasonNumber] intValue];
        int episode = [[_item getMediaItemEpisodeNumber] intValue];
        
        if (season > 0 && episode > 0) {
            _labelSubName.text = [NSString stringWithFormat:@"%@ %d, %@ %d", LS(@"season_wo_param"), season, LS(@"episode_wo_param"), episode];
        }
    }
    
    _labelDescription.text = _item.mediaDescription;
    
    _labelTags.text = @"";
    
    NSString *strRating = @"";
    
    if (_item.tags[@"Rating"] && [_item.tags[@"Rating"] lastObject]) {
        strRating = [NSString stringWithFormat:@"%@", [_item.tags[@"Rating"] lastObject]];
    }
    
    if ([[strRating uppercaseString] isEqualToString:@"FSK0"]) strRating = @"";
    
    if (isPad) {
        if ([strRating length]) {
            _labelRating.hidden = NO;
            _labelRating.text =  [NSString stringWithFormat:@" %@ ", strRating];
            _constraintRatingRightSpace.constant = 12;

        } else {
            _labelRating.hidden = YES;
            _labelRating.text = @"";
            _constraintRatingRightSpace.constant = 0;
        }
    }
    
    NSString *duration = _item.durationDescription;
    if (duration.length) {
        _labelTags.text = duration;
    }
    
    NSString *releaseYear = _item.releaseYearDescription;
    if (releaseYear.length) {
        if (_labelTags.text.length) {
            _labelTags.text = [_labelTags.text stringByAppendingString:@"  |  "];
        }
        _labelTags.text = [_labelTags.text stringByAppendingString:releaseYear];
    }
    
    NSString *countryDescription = _item.countryDescription;
    if (countryDescription.length) {
        if (_labelTags.text.length) {
            _labelTags.text = [_labelTags.text stringByAppendingString:@"  |  "];
        }
        _labelTags.text = [_labelTags.text stringByAppendingString:countryDescription];
    }
    
    if (isPhone) {
        
        if ([strRating length]) {
            
            if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
            
            _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text, strRating];
        }
    }
    
    _labelTags.hidden = ([_labelTags.text length] == 0);
    
    BOOL hasTrailerFile = [self shouldShowTrailerButton];
    _buttonTrailer.hidden = !hasTrailerFile;
    _buttonLike.hidden = NO;
    
    if (isPad) {
        
        _labelDescription.numberOfLines = 0;
        
        [_buttonTrailer setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"575757")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    
        _imgThumbBlur.image = [UIImage imageNamed:@"placeholder_16_9.png"];
        
        __weak UIImageView *imgBlur = _imgThumbBlur;
        imgBlur.image = nil;
        
        [_imgThumbBlur sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:_item andSize:CGSizeMake(_imgThumbBlur.frame.size.width, _imgThumbBlur.frame.size.height)] placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (!error && image) {
                imgBlur.image = [image blurredImageWithRadius:BlurRadius];
            }
        }];
        
        [_imgThumb superview].layer.cornerRadius = 2;
        
        [_imgThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:_item andSize:CGSizeMake(_imgThumb.frame.size.width * 2, _imgThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_2_3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        
        [Utils setLabelFont:_labelLikeCounter bold:YES];
        _labelLikeCounter.text = [Utils getStringFromInteger: _item.likeCounter];
        _labelLikeCounter.highlightedTextColor = CS(@"bebebe");
        
        [_buttonLike.titleLabel setFont:_labelLikeCounter.font];
        [_buttonLike setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"575757")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];

        _imgLikeCounter.highlightedImage = [[UIImage imageNamed:@"btn_like_counter_base"] imageTintedWithColor:CS(@"575757")];
        
        [self skinChanged];

        return;
    }
    
    _buttonPlay.enabled = YES;
 
    [_buttonLike setTitle:[Utils getStringFromInteger:_item.likeCounter] forState:UIControlStateNormal<<UIControlStateHighlighted<<UIControlStateSelected];
    
    [self skinChanged];


}

- (BOOL)shouldShowTrailerButton {

    BOOL hasTrailerFile = NO;
    for (TVFile *file in _item.files) {
        if ([file.format isEqualToString:APST.TVCMediaFormatTrailer]) {
            hasTrailerFile = YES;
        }
    }

    return hasTrailerFile && ![APP_SET isOffline];

}

#pragma mark - Buttons actions

- (IBAction)buttonPlayTrailerPressed:(UIButton *)button {
    [self.delegate buttonPlayTrailerPressed];
}

-(void)updateMediaItemAndFavoriteButton:(TVMediaItem*) mediaItem
{
    // implament in subclass.
}

#pragma mark - DTG
- (YLProgressBar *) dtgProgress {
    return downloadProgress;
}

- (UIImageView *) dtgStatusImage {
    return downloadStatusImage;
}

- (UIButton *) dtgButton {
    return self.buttonDownload;
}

- (void) createDownloadButtonSubviews:(VODButtonState) state {

    _constraintDownloadButtonHeight.constant = 32;
    _constraintDownloadButtonWidth.constant = 67;
    _constraintDownloadButtonBottomSpace.constant = 10;
    _constraintDownloadButtonRightSpace.constant = 10;

    if (![APP_SET isOffline] && self.buttonDownload.hidden)
        self.buttonDownload.hidden = FALSE;
        
    if (self.buttonDownload.subviews.count > 1) {
        [downloadProgress setProgress:0 animated:NO];
        downloadProgress.hidden = TRUE;
        return;

    }
    self.buttonDownload.layer.cornerRadius = (isPad) ? 5 : 0;
    self.buttonDownload.layer.masksToBounds = YES;
    [self.buttonDownload setBackgroundColor:CS(@"4f4f4f")];
    [self.buttonDownload setTouchUpInsideAction:^(UIButton *btn) {
        [self.delegate buttonDownloadPressed];
    }];
    downloadProgress = [YLProgressBar new];
    downloadProgress.userInteractionEnabled = FALSE;
    downloadProgress.translatesAutoresizingMaskIntoConstraints = NO;
    [downloadProgress setProgress:1 animated:NO];
    downloadProgress.hidden = TRUE;
    downloadProgress.type = YLProgressBarTypeFlat;
    downloadProgress.stripesOrientation = YLProgressBarStripesOrientationRight;
    downloadProgress.stripesDirection = YLProgressBarStripesDirectionRight;
    downloadProgress.indicatorTextDisplayMode = YLProgressBarIndicatorTextDisplayModeNone;
    downloadProgress.behavior = YLProgressBarBehaviorWaiting;
    
    [self.buttonDownload addSubview:downloadProgress];
    [self.buttonDownload addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[downloadProgress]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(downloadProgress)]];
    [self.buttonDownload addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[downloadProgress(4)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(downloadProgress)]];
    downloadProgress.backgroundColor = [UIColor clearColor];
    downloadProgress.trackTintColor = [UIColor clearColor];
    
    downloadStatusImage = [UIImageView new];
    downloadStatusImage.userInteractionEnabled = FALSE;
    downloadStatusImage.translatesAutoresizingMaskIntoConstraints = NO;
    [self.buttonDownload addSubview:downloadStatusImage];
    [self.buttonDownload addConstraint:[NSLayoutConstraint constraintWithItem:downloadStatusImage
                                                                               attribute:NSLayoutAttributeCenterY
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:self.buttonDownload
                                                                               attribute:NSLayoutAttributeCenterY
                                                                              multiplier:1.0
                                                                                constant:0.0]];
    [self.buttonDownload addConstraint:[NSLayoutConstraint constraintWithItem:downloadStatusImage
                                                                               attribute:NSLayoutAttributeCenterX
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:self.buttonDownload
                                                                               attribute:NSLayoutAttributeCenterX
                                                                              multiplier:1.0
                                                                                constant:0.0]];
    [self.buttonDownload addConstraint:[NSLayoutConstraint constraintWithItem:downloadStatusImage
                                                                               attribute:NSLayoutAttributeWidth
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:nil
                                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                                              multiplier:1.0
                                                                                constant:16.0]];
    [self.buttonDownload addConstraint:[NSLayoutConstraint constraintWithItem:downloadStatusImage
                                                                               attribute:NSLayoutAttributeHeight
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:nil
                                                                               attribute:NSLayoutAttributeNotAnAttribute
                                                                              multiplier:1.0
                                                                                constant:16.0]];
}


@end
