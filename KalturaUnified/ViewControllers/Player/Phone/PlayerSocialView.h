//
//  PlayerSocialView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialBuzzCell.h"
#import "SocialFeedPopUpView.h"
#import "BasePopUp.h"
#import "LikeAdapter.h"
#import "LoginInterMediateView.h"


@class PlayerViewController;


@protocol PlayerSocialViewDelegate <NSObject>

- (void)intermediateLogin:(NSString *)segue;

@end

@interface PlayerSocialView : UINibView <SocialBuzzCellDelegate, BasePopUpDelegate> {
    
    IBOutlet UITableView *commentsFilterTable;
    IBOutlet UITableView *commentTable;
    
    IBOutlet UILabel *labelTitle;

    IBOutlet UIView *viewNotAvailable;
    IBOutlet UILabel *labelNotAvailable;
    
    IBOutlet UIImageView *imgViewActivity;
    LoginInterMediateView *viewLogin;
}

- (void)enableContent;
- (void)initInterface;

- (void)updateMedia:(TVMediaItem *)mediaItem;
- (void)loadMoreAction:(UIButton *)button;
- (void)updateMediaFeed;
- (void)updateMediaFeedWithFilter:(NSDictionary *)filter;

- (IBAction)buttonCommentPressed:(UIButton *)button;

@property (nonatomic, strong) TVMediaItem *item;
@property (nonatomic, strong) LikeAdapter *likeAdapter;

@property (nonatomic, strong) NSArray *feedsData;
@property (nonatomic, strong) NSArray *feedsDataInApp;
@property (nonatomic, strong) NSArray *feedsDataFacebook;
@property (nonatomic, strong) NSArray *feedsDataTwitter;
@property (nonatomic, strong) NSMutableArray *rowsState;

@property (nonatomic) NSInteger filterType;

@property (nonatomic, weak) PlayerViewController *delegateController;
@property (nonatomic) BOOL commentFilterOpened;

@property (nonatomic, strong) IBOutlet UIButton *btnFilter;
@property (nonatomic, strong) UIImageView *imgFilter;

@property (nonatomic, weak) IBOutlet UIButton *buttonLike;

@property (nonatomic, weak) IBOutlet UIImageView *imgLikeCounter;
@property (nonatomic, weak) IBOutlet UILabel *labelLikeCounter;

@property (nonatomic, weak) IBOutlet UIButton *buttonComment;

@end
