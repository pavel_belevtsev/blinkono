//
//  PlayerVODDetailsViewVF.m
//  KalturaUnified
//
//  Created by Synergetica LLC on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerVODDetailsViewVF.h"
#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ImageEffects.h"
#import "UIImage+Blur.h"

@implementation PlayerVODDetailsViewVF
/* "PSC : Vodafone Grup : change View : JIRA ticket. */
- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (isPhone)
    {
        [self.buttonTrailer setImage:[[UIImage imageNamed:@"details_play_gray_icon"] imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
        [self.buttonTrailer setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
       // [self.buttonTrailer setTitle:LS(@"Tralier") forState:UIControlStateNormal];
    }

    [self.buttonTrailer setTitle:LS(@"item_page_play_trailer") forState:(UIControlStateNormal)];
    
}

- (void)updatePlayButtonWithState:(VODButtonState)state subscriptionsPurchased:(NSArray *)arrSubscriptionsPurchased subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase ppvRentalInfo: (TVRental*) rentalInfo ppvData:(TVPPVModelData *)ppvData
{
    if (isPad)
    {
        [super updatePlayButtonWithState:state subscriptionsPurchased:arrSubscriptionsPurchased subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:rentalInfo ppvData:ppvData];
    }
    else
    {
        self.translatesAutoresizingMaskIntoConstraints = NO;

        [super updatePlayButtonWithState:state subscriptionsPurchased:arrSubscriptionsPurchased subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:rentalInfo ppvData:ppvData];
//        [self.buttonPlay setTitle:@"Watch Now" forState:UIControlStateNormal];

        [self.buttonPlay setTitleColor:APST.brandTextColor forState:UIControlStateNormal];
        [self.buttonPlay setBackgroundImage:[[[UIImage imageNamed:@"details_button"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];

        [Utils setButtonFont:self.buttonPlay  bold:YES];
    }
}

- (void)updateData:(TVMediaItem *)item {

    [self.buttonPlay removeAllSubviews];
    self.item = item;
    
    if (isPhone)
    {
          self.favoriteAdapter = [[FavoriteAdapter alloc] initWithMediaItem:self.item control:self.buttonFavorite];
    }
    [self.likeAdapter setupMediaItem:item];
    [self createLabels];
        
}

- (void)createLabels
{
//    if (isPad)
//    {
//        [super createLabels];
//        return;
//    }
    
    [self.downloadedOnLabel setHidden:YES];
    
    self.scrollViewContent.contentOffset = CGPointZero;
    
    self.labelName.text = self.item.name;
    self.labelSubName.text = @"";
    
    if ([self.item.mediaTypeName isEqualToString:@"Episode"])
    {
        int season = [[self.item getMediaItemSeasonNumber] intValue];
        int episode = [[self.item getMediaItemEpisodeNumber] intValue];
        
        if (season > 0 && episode > 0)
        {
            self.labelSubName.text = [NSString stringWithFormat:@"%@ %d, %@ %d", LS(@"Season"), season, LS(@"Episode"), episode];
        }
    }
    
    self.labelDescription.text = self.item.mediaDescription;
    
    [self buildLabelTagsText];
    
    self.labelTags.hidden = ([self.labelTags.text length] == 0);
    
    BOOL hasTrailerFile = [self shouldShowTrailerButton];
    self.buttonTrailer.hidden = !hasTrailerFile;
//    self.imgTrailerFX.hidden = self.buttonTrailer.hidden;
    
    self.buttonLike.hidden = NO;
    
    if (isPad)
    {
        [self createLabelsPad];
    }
    else
    {
        [self createLabelsPhone];
    }
}

- (void) updateButtonEdgeInset:(UIButton*) button space:(NSInteger)space
{
    [button setTitleEdgeInsets:UIEdgeInsetsMake(2, space, 0, 0)];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, space)];
}

-(void)buildLabelTagsText
{
    self.labelTags.text = @"";
    
    NSString *runtimeText  = [self.item.metaData objectForKey:@"Runtime"];
    if (runtimeText && [runtimeText length] > 0)
    {
        self.labelTags.text = [NSString stringWithFormat:@"%d %@", (int)([runtimeText intValue]) / 60, LS(@"min")];
    }
    
    NSString *releaseText  = [self.item.metaData objectForKey:@"Release year"];
    if (releaseText && [releaseText length] > 0)
    {
        [self appPipeIfNeddToLabelTags];
        self.labelTags.text = [NSString stringWithFormat:@"%@%@", self.labelTags.text, releaseText];
    }


    NSArray *countryArray  = [self.item.tags objectForKey:@"Country"];

    if (countryArray && [countryArray count] > 0 && [[countryArray firstObject] length] > 0)
    {
        [self appPipeIfNeddToLabelTags];
        self.labelTags.text = [NSString stringWithFormat:@"%@%@", self.labelTags.text, [countryArray firstObject]];
    }
    
    NSString *ratingText  =  [Utils ratingToShow:self.item.tags[@"Parental Rating"]];
    if ([[ratingText uppercaseString] isEqualToString:@"FSK0"]) ratingText = @"";
    
    if (ratingText && [ratingText length] > 0) {
        
        [self appPipeIfNeddToLabelTags];
        self.labelTags.text = [NSString stringWithFormat:@"%@%@", self.labelTags.text, LS(ratingText)];
    }
    
    if (isPad)
    {
        self.labelRating.hidden = YES;
//        if ([ratingText length]) {
//            self.labelRating.hidden = NO;
//            self.constraintRatingRightSpace.constant = 12;
//            
//        } else {
//            self.labelRating.hidden = YES;
//            self.labelRating.text = @"";
//            self.constraintRatingRightSpace.constant = 0;
//        }
    }
}

-(void) appPipeIfNeddToLabelTags
{
    if ([self.labelTags.text length] > 0)
    {
        self.labelTags.text = [NSString stringWithFormat:@"%@  |  ", self.labelTags.text];
    }
}

-(void)createLabelsPad
{
    self.labelDescription.numberOfLines = 0;
    
    CGRect descFrame = [self.labelDescription textRectForBounds:CGRectMake(0, 0, self.scrollViewContent.frame.size.width, CGFLOAT_MAX) limitedToNumberOfLines:0];
    self.labelDescription.frame = CGRectMake(0.0, 0.0, self.labelDescription.frame.size.width, descFrame.size.height + 5);
    
    self.scrollViewContent.contentSize = self.labelDescription.frame.size;
    
    [self.buttonTrailer setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"4e4e4e")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    
    self.imgThumbBlur.image = [UIImage imageNamed:@"placeholder_16_9.png"];
    
    __weak UIImageView *imgBlur = self.imgThumbBlur;
    imgBlur.image = nil;
    
    [self.imgThumbBlur sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:self.item andSize:CGSizeMake(self.imgThumbBlur.frame.size.width, self.imgThumbBlur.frame.size.height)] placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!error && image) {
            imgBlur.image = [image blurredImageWithRadius:BlurRadius];
        }
    }];
    
    [self.imgThumb superview].layer.cornerRadius = 2;
    
    [self.imgThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:self.item andSize:CGSizeMake(self.imgThumb.frame.size.width * 2, self.imgThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_2_3.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    [self.buttonLike setImage:[[UIImage imageNamed:@"details_like_icon"] imageTintedWithColor:CS(@"bebebe")] forState:UIControlStateNormal];
    [self.buttonLike setImage:[[UIImage imageNamed:@"details_like_icon"] imageTintedWithColor:APST.brandTextColor] forState:UIControlStateNormal];
    
    [self.buttonLike setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"4e4e4e")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    [self.buttonLike setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    
    self.imgLikeCounter.image = [[UIImage imageNamed:@"btn_like_counter_base"] imageTintedWithColor:APST.brandColor];
    self.imgLikeCounter.highlightedImage = [[UIImage imageNamed:@"btn_like_counter_base"] imageTintedWithColor:CS(@"4e4e4e")];
    
    self.labelLikeCounter.textColor = APST.brandTextColor;
    self.labelLikeCounter.highlightedTextColor = CS(@"bebebe");
    
    [Utils setLabelFont:self.labelLikeCounter bold:YES];
    self.labelLikeCounter.text = [NSString stringWithFormat:@"%d", self.item.likeCounter];
}

-(void)createLabelsPhone
{
    [self updateButtonsUIiPhone];
    
    self.labelName.frame = [Utils getLabelFrame:self.labelName maxHeight:48 minHeight:0];
    
    float origenY = self.labelName.frame.origin.y + self.labelName.frame.size.height + 8;
    
    if ([self.labelName.text length])
    {
        self.labelSubName.frame = CGRectMake(self.labelSubName.frame.origin.x, origenY - 8, self.labelSubName.frame.size.width, self.labelSubName.frame.size.height);
        origenY += self.labelSubName.frame.size.height;
    }
    
    origenY += 25;
    
    CGRect fremeD = self.descriptionScrollView.frame;
    fremeD.origin.y = origenY;
    self.descriptionScrollView.frame = fremeD;
    
    CGRect descFrame = [self.labelDescription textRectForBounds:CGRectMake(0, 0, 264, CGFLOAT_MAX) limitedToNumberOfLines:0];
    
    self.labelDescription.frame = CGRectMake(self.labelDescription.frame.origin.x, 0, descFrame.size.width, descFrame.size.height + 5);
    
    self.descriptionScrollView.contentSize = self.labelDescription.frame.size;
    
    origenY += self.descriptionScrollView.frame.size.height + 10;
    
    if (!self.labelTags.hidden)
    {
        CGRect descFrame = [self.labelTags textRectForBounds:CGRectMake(0, 0, 264, CGFLOAT_MAX) limitedToNumberOfLines:0];
        self.labelTags.frame = CGRectMake(self.labelTags.frame.origin.x, origenY, descFrame.size.width, descFrame.size.height + 5);
        origenY += self.labelTags.frame.size.height + 8;
    }
    
    int height = origenY + 10 ;
    
    self.scrollViewContent.contentSize = CGSizeMake(self.scrollViewContent.frame.size.width, height);
    self.buttonPlay.enabled = YES;
    
    [self skinChanged];
    
    CGRect frame = self.mainItemsView.frame;
    if ([self.labelSubName.text isEqualToString:@""])
    {
        frame.origin.y = self.labelSubName.frame.origin.y + 10;
    }
    else
    {
        frame.origin.y = self.labelSubName.frame.origin.y + self.labelSubName.frame.size.height +10;
        self.labelTags.frame = CGRectMake(self.labelTags.frame.origin.x, self.labelTags.frame.origin.y- self.labelSubName.frame.size.height, self.labelTags.frame.size.width, self.labelTags.frame.size.height + 5);
    }
    self.mainItemsView.frame = frame;
}

-(void)updateButtonsUIiPhone
{
    [self.buttonFavorite setBackgroundImage:[[[UIImage imageNamed:@"details_button"] imageTintedWithColor:CS(@"4e4e4e")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    [self.buttonFavorite setBackgroundImage:[[[UIImage imageNamed:@"details_button"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    
    [self.buttonFavorite setImage:[UIImage imageNamed:@"button_white_add_phone"]  forState:UIControlStateNormal];
    [self.buttonFavorite setImage:[UIImage imageNamed:@"button_white_added_phone_new"] forState:UIControlStateHighlighted];
    [self.buttonFavorite setImage:[UIImage imageNamed:@"button_white_added_phone_new"] forState:UIControlStateSelected];
    
    [self.buttonFavorite setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
    [self.buttonFavorite setTitle:LS(@"my_zone_tab_favorites") forState:UIControlStateNormal];
    
    [Utils setButtonFont:self.buttonFavorite bold:YES];
    [Utils setButtonFont:self.buttonTrailer bold:YES];
    [Utils setButtonFont:self.buttonPlay bold:YES];
    [Utils setButtonFont:self.buttonLike bold:YES];
    
    [self updateButtonEdgeInset:self.buttonFavorite space:6];
    [self.buttonTrailer setImage:[[UIImage imageNamed:@"details_play_gray_icon"] imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
    [self.buttonTrailer setBackgroundImage:[[[UIImage imageNamed:@"details_button"] imageTintedWithColor:CS(@"4e4e4e")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    
    [self updateFavoriteButtonsUIiPhone];
    
    self.buttonPlay.enabled = YES;
}

-(void)updateFavoriteButtonsUIiPhone
{
    NSString *imageName = @"details_like_icon";
    UIImage *image = [UIImage imageNamed:imageName];
    
    NSString * likesStrong = LS(@"LIKES");
    
    [self.buttonLike setImage:image forState:UIControlStateNormal];
    [self.buttonLike setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
    [self.buttonLike setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
    [self.buttonLike setTitle:[NSString stringWithFormat:@"%d %@", self.item.likeCounter,likesStrong] forState:UIControlStateNormal];
    [self.buttonLike setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
    [self.buttonLike setTitleColor:APST.brandColor forState:UIControlStateSelected];
    
    NSString * text = [Utils getStringFromInteger:self.item.likeCounter];
    [self.buttonLike setTitle:text forState:UIControlStateNormal<<UIControlStateHighlighted<<UIControlStateSelected];
}
@end
