//
//  PlayerVODDetailsView.h
//  KalturaUnified
//
//  Created by Synergetica LLC on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LikeAdapter.h"
#import "PlayerVODDetailsView.h"
#import "FavoriteAdapter.h"
#import "APSComplexButton.h"

@interface PlayerVODDetailsViewVF : PlayerVODDetailsView



@property (weak, nonatomic) IBOutlet APSComplexButton *buttonFavorite;
@property (nonatomic, strong) FavoriteAdapter *favoriteAdapter;

-(IBAction)toggleAddRemoveFavorite:(id)sender;
-(void)updateMediaItemAndFavoriteButton:(TVMediaItem*) mediaItem;
@property (weak, nonatomic) IBOutlet UIView *mainItemsView;
@property (weak, nonatomic) IBOutlet UIScrollView *descriptionScrollView;

@end
