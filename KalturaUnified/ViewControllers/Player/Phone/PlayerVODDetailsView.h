//
//  PlayerVODDetailsView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LikeAdapter.h"
#import "Unifide-Swift.h"
#import "YLProgressBar.h"
#import "TVMediaItem+Additions.h"
#import "UINibView.h"
#import <TvinciSDK/TVPPVModelData.h>

@class TVPPVData_prise;

typedef enum VODButtonState
{
    kVODButtonShouldPlayState,
    kVODButtonPurchaseState,
    kVODButtonPlayngState,
    kVODButtonPausedState,
    kVODButtonAddedToWatchListState,
    kVODButtonAddToWatchListState,
    kVODButtonInitial

} VODButtonState;

typedef enum VODSubscriptionButtonState
{
    kVODSubscriptionButtonEmpty,
    kVODSubscriptionButtonSVODsingle,
    kVODSubscriptionButtonSVODmulti,
    kVODSubscriptionButtonTVODsingle,
    kVODSubscriptionButtonInitial

} VODSubscriptionButtonState;

@protocol PlayerVODDetailsViewDelegate <NSObject>

- (void)buttonResumWatchingPressed;
- (void)buttonPlayPressed;
- (void)buttonPlayTrailerPressed;
- (void)buttonDownloadPressed;
- (void)updateMediaItemAndFavoriteButton:(TVMediaItem*) mediaItem;
- (void)addToWatchList;
- (void)removeFromWatchList;
- (void)initializePurchaseFlow:(id) purchasingElement;
- (UINavigationController*)getNavigationController;
- (NSAttributedString*) attributedActionButtonTitleForState:(VODSubscriptionButtonState)state
                                          subscriptionPrice:(TVSubscriptionData_prise*)subscriptionPrice
                                                   ppvPrice:(TVPPVData_prise*)ppvPrice
                                               baseFontSize:(CGFloat)fontSize;
-(void) removeActivityIndicator;

@optional
-(void) prepareDownloadedFile:(KADownloadItem *)downloadItem;
-(void) updateCurrentMediaState:(VODButtonState) state;
-(BOOL) isMediaAllowedForDownloading:(TVMediaItem*) item;
-(BOOL) isPlayingLocalFile;
-(void) finishedUpdatingActionButtonForState:(VODButtonState)state;
@end

@class TVRental;

@interface PlayerVODDetailsView : UINibView {
    
    int buttonState;
    
}

- (void)updateData:(TVMediaItem *)item;

@property (nonatomic, strong) TVMediaItem *item;
@property (nonatomic, strong) LikeAdapter *likeAdapter;

@property (nonatomic, weak) IBOutlet UILabel *labelName;
@property (nonatomic, weak) IBOutlet UILabel *labelSubName;
@property (nonatomic, weak) IBOutlet UIButton *buttonPlay;
@property (nonatomic, weak) IBOutlet UIButton *buttonLike;
@property (nonatomic, weak) IBOutlet UIButton *buttonDownload;
@property (nonatomic, weak) IBOutlet UIImageView *imgLikeCounter;
@property (nonatomic, weak) IBOutlet UILabel *labelLikeCounter;

@property (nonatomic, weak) IBOutlet UILabel *labelDescription;
@property (nonatomic, weak) IBOutlet UILabel *labelRating;
@property (nonatomic, weak) IBOutlet UILabel *labelTags;
@property (nonatomic, weak) IBOutlet UIButton *buttonTrailer;
@property (weak, nonatomic) IBOutlet UILabel *downloadedOnLabel;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollViewContent;
@property (weak, nonatomic) IBOutlet UIView *viewDetailsContent;
@property (nonatomic, retain) IBOutlet NSLayoutConstraint *constraintRatingRightSpace, *constraintDownloadButtonWidth, *constraintDownloadButtonHeight, *constraintDownloadButtonBottomSpace, *constraintDownloadButtonRightSpace;
@property (nonatomic, weak) id <PlayerVODDetailsViewDelegate> delegate;

@property (nonatomic, weak) IBOutlet UIImageView *imgThumbBlur;
@property (nonatomic, weak) IBOutlet UIImageView *imgThumb;

@property (nonatomic, weak) IBOutlet UIView *viewBar, *viewSubscriptionDaysLeft;

@property (nonatomic, retain) IBOutlet NSLayoutConstraint *constraintButtonPlayTrailerHeight, *constraintSubscriptionLeftDaysHeight;

- (void)updateMediaItemAndFavoriteButton:(TVMediaItem*) mediaItem;
- (BOOL)shouldShowTrailerButton;
- (void)updatePlayButtonWithState:(VODButtonState)state;
- (void)updatePlayButtonWithState:(VODButtonState)state subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase ppvRentalInfo:(TVRental*) rentalInfo ppvData:(TVPPVModelData *)ppvData;
- (void)updatePlayButtonWithState:(VODButtonState)state subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase subscriptionsPurchased:(NSArray *)arrSubscriptionsPurchased;
- (void)skinChanged;
- (void)createLabels;
- (YLProgressBar *) dtgProgress;
- (UIImageView *) dtgStatusImage;
- (UIButton *) dtgButton;
- (void)updatePlayButtonWithState:(VODButtonState)state subscriptionsPurchased:(NSArray *)arrSubscriptionsPurchased subscriptionsToPurchase:(NSArray*)arrSubscriptionsToPurchase ppvRentalInfo: (TVRental*) rentalInfo ppvData:(TVPPVModelData *)ppvData;

@end
