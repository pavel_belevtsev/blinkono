//
//  PlayerLiveDetailsView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerLiveDetailsView.h"
#import "UIImageView+WebCache.h"

#import "PlayerLiveEPGFlowLayout.h"
#import "NSDate+Utilities.h"
#import "PlayerViewController.h"
#import <TvinciSDK/APSBlockOperation.h>

@implementation PlayerLiveDetailsView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [Utils setLabelFont:_labelTitle bold:YES];
    [Utils setLabelFont:_labelNoDataForChannel bold:YES];
    _labelNoDataForChannel.text = LS(@"live_program_empty_item_msg");
    
    [_collectionViewEPG registerNib:[UINib nibWithNibName:[Utils nibName:@"PlayerProgramCollectionViewCell"] bundle:nil] forCellWithReuseIdentifier:@"PlayerProgramCollectionViewCell"];

    _collectionViewEPG.panGestureRecognizer.enabled = NO;
    [_collectionViewEPG addGestureRecognizer:_hiddenScrollView.panGestureRecognizer];
    
    
    self.epgList = [[NSMutableArray alloc] init];
    self.dictionaryRequest = [[NSMutableDictionary alloc] init];
    self.dictionaryNpvrRequest = [[NSMutableDictionary alloc]init];

    self.viewNoDataForChannel.hidden = YES;
    
    self.recoredAdapter = [[RecoredAdapter alloc] init];
    
    self.recoredAdapter.delegate = self;
}

- (void)dealloc {
    
    [self clearRequest];
    
}

- (void)clearRequest {
    
    APSBlockOperation * request = [self.dictionaryRequest objectForKey:@"request"];
    if (request) {
        [request cancel];
        [self.dictionaryRequest removeObjectForKey:@"request"];
    }
    TVPAPIRequest * req = [self.dictionaryNpvrRequest objectForKey:@"NpvrRequest"];
    if (req)
    {
        [req cancel];
        [self.dictionaryNpvrRequest removeObjectForKey:@"NpvrRequest"];
    }
}

- (NSDate *)dateWithOutTime:(NSDate *)datDate
{
    if( datDate == nil )
    {
        datDate = [NSDate date];
    }
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:datDate];
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}


- (void)updateData:(TVMediaItem *)item withFirstShownProgram:(APSTVProgram *) program
{
    self.item = item;
    
    _labelTitle.text = _item.name;
    
    [_imgThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:_item andSize:CGSizeMake(_imgThumb.frame.size.width * 2, _imgThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"epg_channel_placeholder"]];
    
    [self.epgList removeAllObjects];
    [_collectionViewEPG reloadData];
    _hiddenScrollView.contentSize = _hiddenScrollView.bounds.size;
    
    __weak PlayerLiveDetailsView * weakSelf = self;
    
    
    NSDate *date = [NSDate date];
    
    date = [self dateWithOutTime:date];
    NSDate *dateLast = [date dateByAddingDays:1];
    
    [self clearRequest];
    
    //NSLog(@"%@  %@", date, dateLast);
    APSBlockOperation * request = [[APSEPGManager sharedEPGManager] programsByRangeForChannel:_item.epgChannelID fromDate:date toDate:dateLast actualFromDate:date actualToDate:dateLast includeProgramsDuringfromDate:YES includeProgramsDuringToDate:YES privateContext:self.delegateController.privateContext starterBlock:^{
    } failedBlock:^{
        NSLog(@"failed");
    } completionBlock:^(NSArray *programs) {
        
        if ((programs != nil) && ([programs count]))
        {
            NSArray *array = [programs  sortedArrayUsingComparator:^NSComparisonResult(APSTVProgram *obj1, APSTVProgram *obj2) {
                
                NSDate *date1 = obj1.startDateTime;
                NSDate *date2 = obj2.startDateTime;
                
                return [date1 compare:date2];
                
            }];
            
            NSMutableArray *fillArray = [EPGM addEmptyProgramsForEmptySpacesInProgramsArray:[array mutableCopy] forDate:date privateContext:self.delegateController.privateContext];
            
            for (APSTVProgram *program in fillArray) {
                [weakSelf.epgList addObject:program];
            };
            self.viewNoDataForChannel.hidden = YES;
            
        }
        else
        {
            self.viewNoDataForChannel.hidden = NO;
        }
        
        [weakSelf.collectionViewEPG reloadData];
        weakSelf.hiddenScrollView.contentSize = CGSizeMake(weakSelf.hiddenScrollView.bounds.size.width * [weakSelf.epgList count], weakSelf.hiddenScrollView.bounds.size.height);

        if (program == nil)
        {
            [weakSelf checkLiveProgram:NO];
        }
        else
        {
            [self scrollToProgram:program animated:NO];
        }
        if ([weakSelf.epgList count]>0)
        {
            [self.recoredAdapter cleanAllReoredsDataFromMemory];
            TVPAPIRequest * req =  [self.recoredAdapter loadRecoredByChannel:self.item andPrograms:weakSelf.epgList scrollToProgram:nil];
            if (req)
            {
                [self.dictionaryNpvrRequest setObject:req forKey:@"NpvrRequest"];
            }

        }
        
    }];
    
    if (request != nil) {
        
        [self.dictionaryRequest setObject:request forKey:@"request"];

    }

}

- (void)checkLiveProgram:(BOOL)animated {
    
    int index = -1;
    for (int i = 0; i < [_epgList count]; i++) {
        APSTVProgram *program = [_epgList objectAtIndex:i];
        if ([program programIsOn]) {
            index = i;
        }
    }
    
    if (index >= 0) {
        
        [_collectionViewEPG reloadData];
        [_hiddenScrollView setContentOffset:CGPointMake(_hiddenScrollView.bounds.size.width * index, 0) animated:animated];
    }
    
}

-(void) scrollToProgram:(APSTVProgram *) scrolledProgram animated:(BOOL) animated
{
    int index = -1;
    for (int i = 0; i < [_epgList count]; i++) {
        APSTVProgram *program = [_epgList objectAtIndex:i];
        if ([program.epgId isEqualToString:scrolledProgram.epgId])
        {
            index = i;
        }
    }
    
    if (index >= 0) {
        
        [_collectionViewEPG reloadData];
        [_hiddenScrollView setContentOffset:CGPointMake(_hiddenScrollView.bounds.size.width * index, 0) animated:animated];
        
    }

}

#pragma mark hiddenScrollView delegate

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.hiddenScrollView) {
        
        CGPoint contentOffset = scrollView.contentOffset;
        _collectionViewEPG.contentOffset = contentOffset;
        
    }
}


#pragma mark - UICollectionView

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (self.recordItem != nil)
    {
        return 1;
    }
    else
    {
        return [self.epgList count];
    }

    
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    return CGSizeMake(_hiddenScrollView.frame.size.width, collectionView.frame.size.height);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PlayerProgramCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:@"PlayerProgramCollectionViewCell" forIndexPath:indexPath];

    cellView.mediaItem = _item;
    APSTVProgram * epgProgram = [_epgList objectAtIndex:indexPath.row];
    [cellView updateData:epgProgram];
    cellView.delegate = self;
    RecoredHelper * recHelp = [self.recoredAdapter recoredHelperToepgProgram:epgProgram mediaItem:self.item];
    
    cellView.recoredHelper = recHelp;
    return cellView;
    
}


#pragma mark -  PlayerProgramCollectionViewCellDelegate Methods

-(void) playerProgramCell:(PlayerProgramCollectionViewCell *) sender didSelectPlayProgram:(APSTVProgram *) program forChannel:(TVMediaItem *) channel
{
    [self.delegate playerLiveDetails:self didSelectPlayProgram:program forChannel:channel];
}

-(void)playerProgramCell:(PlayerProgramCollectionViewCell *)sender didSelectCancelRecored:(RecoredHelper *)recoredHelper
{
    if (recoredHelper.isSeries)
    {
        self.cancelSeriesRecordingPopUp = [[CustomViewFactory defaultFactory] cancelSeriesRecordingPopUpWithRecoredHelper:recoredHelper andLinkageView:sender];
      //  [[CancelSeriesRecordingPopUp alloc] initWithNibWithRecoredHelper:recoredHelper andLinkageView:sender];
        self.cancelSeriesRecordingPopUp.delegate = self;
        if (isPhone)
        {
            [self.cancelSeriesRecordingPopUp updateViewFrameAndCentetMainView:self.delegateController.view.frame];

            [self.delegateController.view addSubview:self.cancelSeriesRecordingPopUp];
            [self.delegateController.view bringSubviewToFront:self.cancelSeriesRecordingPopUp];
        }
        else
        {
            [self.cancelSeriesRecordingPopUp updateViewFrameAndCentetMainView:self.frame];

            [self addSubview:self.cancelSeriesRecordingPopUp];
            [self bringSubviewToFront:self.cancelSeriesRecordingPopUp];
        }
    }
    else
    {
        [self.recoredAdapter deleteOneRecored:recoredHelper.recordItem Program:recoredHelper.program mediaItem:recoredHelper.mediaItem];
    }

}
-(void)playerProgramCell:(PlayerProgramCollectionViewCell *)sender didSelectRecoredProgram:(APSTVProgram *)program mediaItem:(TVMediaItem *)mediaItem isSeries:(BOOL)isSeries
{
    if (isSeries)
    {
        self.miniEpgRecoredPopUp = [[CustomViewFactory defaultFactory] miniEpgRecoredPopUp];
        self.miniEpgRecoredPopUp.delegate = self;

        if (isPad)
        {
            UIView *linkageView = [[UIView alloc] initWithFrame: [sender getRecordButtonLocationInSelf]];
            [self.miniEpgRecoredPopUp buildWithSuperView:self OnLinkageView:linkageView withRecoredProgram:program andMediaItem:mediaItem toolTipArrowType:ToolTipTypeArrowDown toolCloseType:ToolTipCloseFullScreen];
             [self addSubview:self.miniEpgRecoredPopUp];
        }
        else
        {
            [self.miniEpgRecoredPopUp buildWithSuperView:self.delegateController.view OnLinkageView:self.delegateController.view withRecoredProgram:program andMediaItem:mediaItem toolTipArrowType:ToolTipTypeArrowNone toolCloseType:ToolTipCloseButtonOnly];
            [self.delegateController.view addSubview:self.miniEpgRecoredPopUp];
            [self.delegateController.view bringSubviewToFront:self.miniEpgRecoredPopUp];
        }
    }
    else
    {
        [self.recoredAdapter recordAsset:program mediaItem:mediaItem];
    }
}

#pragma mark - cancelSeriesRecordingPopUp Delegate -

-(void) cancelSeriesRecordingPopUp:(CancelSeriesRecordingPopUp *)epgRecoredPopUp cnacelRecoredType:(CnacelRecoredType)cnacelRecoredType WithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView*)linkageView
{
    [self.recoredAdapter cancelOrDeleteSeriesRecordingWithCnacelRecoredType:cnacelRecoredType WithRecoredHelper:recoredHelper];
}

-(void) cancelSeriesRecordingPopUp:(CancelSeriesRecordingPopUp *)epgRecoredPopUp didCancelViewWithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView*)linkageView
{
    APSTVProgram * prog = self.delegateController.programToPlay;
    if (prog == nil)
    {
        prog = self.delegateController.playedProgram;
    }
    RecoredHelper * recHelp = [self.recoredAdapter recoredHelperToepgProgram:recoredHelper.program mediaItem:recoredHelper.mediaItem];
    if (recoredHelper.program)
    {
       [self scrollToProgram:recoredHelper.program animated:NO];
    }
    if (recoredHelper.program && [prog.epgId isEqualToString:recoredHelper.program.epgId])
    {
            [self.delegateController showRecoredIcon:recHelp.isSeries];
    }
}

-(void) miniEpgRecoredPopUp:(MiniEpgRecoredPopUp *)miniEpgRecoredPopUp recredType:(RecoredType)recoredType recoredProgram:(APSTVProgram *) program mediaItem:(TVMediaItem *) mediaItem andLinkageView:(UIView*)linkageView
{
    if (recoredType == RecoredTypeEpisode)
    {
        [self.recoredAdapter recordAsset:program mediaItem:mediaItem];
    }
    else
    {
        [self.recoredAdapter recordSeriesByProgram:program mediaItem:mediaItem];
    }
}

#pragma mark

-(void)recoredAdapter:(RecoredAdapter *)epgRecoredPopUp complateRecordAssetOrSeriesToToChannel:(TVMediaItem*)mediaItem Program:(APSTVProgram *) epgProgram recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus
{
    APSTVProgram * prog = self.delegateController.programToPlay;
    if (prog == nil)
    {
        prog = self.delegateController.playedProgram;
    }
    RecoredHelper * recHelp = [self.recoredAdapter recoredHelperToepgProgram:epgProgram mediaItem:mediaItem];
    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
        [self scrollToProgram:epgProgram animated:NO];
        if ([prog.epgId isEqualToString:epgProgram.epgId])
        {
            [self.delegateController showRecoredIcon:recHelp.isSeries];
        }
        
    }
    else
    {
        [self.delegateController hideRecoredIcon];
    }
    [self.delegateController showRecoredTostStatusViewToProgram:epgProgram recoredAdapterStatus:recoredAdapterStatus mediaItem:mediaItem recoredHelper:recHelp];
}

-(void)recoredAdapter:(RecoredAdapter *)recoredAdapter complateLoadRecoredItemsToChannel:(TVMediaItem*)mediaItem isChannelHeveRecoredItems:(BOOL)isChannelHeveRecoredItems scrollToProgram:(APSTVProgram *)scrolledProgram

{
    [UIView performWithoutAnimation:^{

        [self.collectionViewEPG reloadData];
    }];
    APSTVProgram * prog = self.delegateController.programToPlay;
    
    if (prog == nil)
    {
        prog = self.delegateController.playedProgram;
        if (prog == nil)
        {
            [self performSelector:@selector(tryAgaincomplateLoadRecoredItemsToChannel:) withObject:mediaItem afterDelay:0.2];
        }
    }
    RecoredHelper * recHelp = [self.recoredAdapter recoredHelperToepgProgram:prog mediaItem:mediaItem];

    if (recHelp.recordingItemStatus == RecoredHelperRecoreded)
    {
        [self.delegateController showRecoredIcon:recHelp.isSeries];
    }
    else
    {
        [self.delegateController hideRecoredIcon];
    }
    if(scrolledProgram) {
        [self scrollToProgram:scrolledProgram animated:NO];
    }
}

-(void)tryAgaincomplateLoadRecoredItemsToChannel:(TVMediaItem*)mediaItem
{
    APSTVProgram * prog = self.delegateController.programToPlay;
    
    if (prog == nil)
    {
        prog = self.delegateController.playedProgram;
        if (prog == nil)
        {
            [self performSelector:@selector(tryAgaincomplateLoadRecoredItemsToChannel:) withObject:mediaItem afterDelay:0.2];
        }
    }
    RecoredHelper * recHelp = [self.recoredAdapter recoredHelperToepgProgram:prog mediaItem:mediaItem];
    
    if (recHelp.recordingItemStatus == RecoredHelperRecoreded)
    {
        [self.delegateController showRecoredIcon:recHelp.isSeries];
    }
    else
    {
        [self.delegateController hideRecoredIcon];
    }
}

//-(void)tryAgaincomplateLoadRecoredItemsToChannel:(TVMediaItem*)mediaItem
//{
//    [self recoredAdapter:nil complateLoadRecoredItemsToChannel:mediaItem isChannelHeveRecoredItems:YES scrollToProgram:nil];
//}

-(void) recoredAdapter:(RecoredAdapter *)recoredAdapter  complateCancelOrDeleateAssetRecording:(TVNPVRRecordItem *) recordItem  Program:(APSTVProgram *) epgProgram mediaItem:(TVMediaItem *) mediaItem recoredAdapterCallName:(RecoredAdapterCallName)recoredAdapterCallName recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus;
{
    APSTVProgram * prog = self.delegateController.programToPlay;
    if (prog == nil)
    {
        prog = self.delegateController.playedProgram;
    }

    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
        if ([prog.epgId isEqualToString:epgProgram.epgId])
        {
            [self.delegateController hideRecoredIcon];
        }
        [self scrollToProgram:epgProgram animated:NO];
    }
    [self.delegateController showDeleteRecoredTostStatusViewToProgram:epgProgram recoredAdapterStatus:recoredAdapterStatus mediaItem:mediaItem isSeries:NO];
}

-(void) recoredAdapter:(RecoredAdapter *)recoredAdapter  complateCancelOrDeleateSeriesRecording:(RecoredHelper*)recoredHelper recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus
{
     [self.delegateController showDeleteRecoredTostStatusViewToProgram:recoredHelper.program recoredAdapterStatus:recoredAdapterStatus mediaItem:recoredHelper.mediaItem isSeries:YES];
}

-(NSArray*) currentProgramsToChannel:(TVMediaItem*)channel {
    return _epgList;
}
@end
