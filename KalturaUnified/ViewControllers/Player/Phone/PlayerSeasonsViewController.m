//
//  PlayerEpisodesViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 09.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerSeasonsViewController.h"
#import "PlayerViewController.h"
#import "PlayerChannelCollectionViewCell.h"
#import "NSDate+Utilities.h"
#import "MenuViewController.h"
#import "CategorySortTableViewCell.h"

#import <TvinciSDK/APSBlockOperation.h>

@interface PlayerSeasonsViewController ()

@end

@implementation PlayerSeasonsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.viewContent.layer.cornerRadius = 6;
    self.viewContent.layer.masksToBounds = YES;

    _viewSort.alpha = 1.0;
    
    self.tableViewFilter.layer.cornerRadius = 6;
    self.tableViewFilter.layer.masksToBounds = YES;
 
    _labelSeasons.text = [LS(@"seasons") uppercaseString];
    
    [Utils setLabelFont:_labelSeasons bold:YES];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [_tableViewFilter reloadData];
    
    if (maxFilterTableHeight == 0) {
        maxFilterTableHeight = _tableViewFilter.frame.size.height;
    }
    
}

- (void)reloadData {
    
    [_tableViewFilter reloadData];
    
    CGFloat height = [_seasons count] * sortRowHeight;
    if (height > maxFilterTableHeight) height = maxFilterTableHeight;
    
    _tableViewFilter.frame = CGRectMake(_tableViewFilter.frame.origin.x, _tableViewFilter.frame.origin.y, _tableViewFilter.frame.size.width, height);
    
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_seasons count];
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CategorySortTableViewCell";
    
    CategorySortTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:tableView options:nil] objectAtIndex:0];
    }
    
    cell.labelSort.text = [NSString stringWithFormat:LS(@"season"), [_seasons objectAtIndex:indexPath.row]];
    
    cell.cellSelected = (indexPath.row == _currentSeason);
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;


    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

        _currentSeason = indexPath.row;
        
        [self reloadData];
    
        [_delegate selectSeriesSeason:_currentSeason];
    

        [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
        
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
