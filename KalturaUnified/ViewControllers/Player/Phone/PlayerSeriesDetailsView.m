//
//  PlayerSeriesDetailsView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerSeriesDetailsView.h"
#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ImageEffects.h"
#import "UIImage+Blur.h"

@implementation PlayerSeriesDetailsView

- (void)dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [Utils setLabelFont:_labelName bold:YES];
    
    [Utils setLabelFont:_labelDescription];
    
    [Utils setLabelFont:_labelTags bold:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
}

- (LikeAdapter*) likeAdapter {
    if (!_likeAdapter)
        _likeAdapter = [[LikeAdapter alloc] initWithMediaItem:_item control:_buttonLike];
    return _likeAdapter;
}

- (FavoriteAdapter*) favoriteAdapter {
    if (!_favoriteAdapter)
        _favoriteAdapter = [[FavoriteAdapter alloc] initWithMediaItem:nil control:_buttonFavorite];
    return _favoriteAdapter;
}

- (void)skinChanged {
    
    [self createButton:_buttonLike image:@"details_like_icon" title:[Utils getStringFromInteger: _item.likeCounter]];
    
    [self createButton:_buttonFavorite image:@"details_favorite_small_icon" title:LS(@"favorite")];
    
    [self createButton:_buttonShare image:@"player_share_icon" title:LS(@"share")];
    _buttonShare.UIUnitID = NU(UIUnit_Social_buttons);
    [_buttonShare activateAppropriateRule];

    
    _buttonFavorite.UIUnitID = NU(UIUnit_favorites_buttons);
    [_buttonFavorite activateAppropriateRule];

    
}

- (void)createButton:(UIButton *)button image:(NSString *)imageName title:(NSString *)title {

    UIImage *image = [UIImage imageNamed:imageName];
    
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
    [button setImage:[image imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:APST.brandColor forState:UIControlStateHighlighted];
    [button setTitleColor:APST.brandColor forState:UIControlStateSelected];


}

- (void)updateData:(TVMediaItem *)item {

    self.item = item;
    
    [self.likeAdapter setupMediaItem:item];
    [self.favoriteAdapter setupMediaItem:item];
    [self createLabels];
    
    BOOL isSignIn = [LoginM isSignedIn];
    _buttonFavorite.userInteractionEnabled = isSignIn;
    _buttonShare.userInteractionEnabled = isSignIn;
    _buttonLike.userInteractionEnabled = isSignIn;
}

- (void)createLabels {
    
    _scrollViewContent.contentOffset = CGPointZero;
    
    _labelName.text = _item.name;
    
    _labelDescription.text = _item.mediaDescription;
    
    _buttonLike.hidden = NO;
    
    _labelName.frame = [Utils getLabelFrame:_labelName maxHeight:48 minHeight:0];
    
    float y = _labelName.frame.origin.y + _labelName.frame.size.height + 8;
    
    _buttonFavorite.frame = CGRectMake(_buttonFavorite.frame.origin.x, y, _buttonFavorite.frame.size.width, _buttonFavorite.frame.size.height);
    _buttonShare.frame = CGRectMake(_buttonShare.frame.origin.x, y, _buttonShare.frame.size.width, _buttonShare.frame.size.height);
    
    y += 35;
    
    
    CGRect descFrame = [_labelDescription textRectForBounds:CGRectMake(0, 0, 264, CGFLOAT_MAX) limitedToNumberOfLines:0];
    _labelDescription.frame = CGRectMake(_labelDescription.frame.origin.x, y, descFrame.size.width, descFrame.size.height + 5);
    
    y += _labelDescription.frame.size.height;
    
    _labelTags.text = @"";
    
    if ([_item.tags objectForKey:@"Genre"] && [[_item.tags objectForKey:@"Genre"] count] > 0) {
        
        if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
        
        _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text, [[_item.tags objectForKey:@"Genre"] firstObject]];
    }
    
    if ([_item.metaData objectForKey:@"Release year"] && [[_item.metaData objectForKey:@"Release year"] length] > 0) {
        
        if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
        
        _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text, [_item.metaData objectForKey:@"Release year"]];
    }
    
    if ([_item.metaData objectForKey:@"Rating"] && [[_item.metaData objectForKey:@"Rating"] length] > 0) {
        
        if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
        
        _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text, [_item.metaData objectForKey:@"Rating"]];
    }
    
    if ([_item.tags objectForKey:@"Country"] && [[_item.tags objectForKey:@"Country"] count] > 0) {
        
        if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
        
        _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text, [[_item.tags objectForKey:@"Country"] firstObject]];
    }
    /*”PSC : ONO : OS-91 Ono - CR - Rating needs ot be presented in each media page*/

    if ([_item.tags objectForKey:@"Parental Rating"]){// && [[_item.tags objectForKey:@"Parental Rating"] count] > 0) {
        
        if ([_labelTags.text length] > 0) _labelTags.text = [NSString stringWithFormat:@"%@  |  ", _labelTags.text];
        
        _labelTags.text = [NSString stringWithFormat:@"%@%@", _labelTags.text, LS([[_item.tags objectForKey:@"Parental Rating"]firstObject])];
    }
    
    
    _labelTags.hidden = ([_labelTags.text length] == 0);
    
    y += 10;
    
    if (!_labelTags.hidden) {
        
        _labelTags.frame = CGRectMake(_labelTags.frame.origin.x, y - 5, _labelTags.frame.size.width, _labelTags.frame.size.height);
        y += _labelTags.frame.size.height + 5;
        
    }
    
    
    _scrollViewContent.contentSize = CGSizeMake(_scrollViewContent.frame.size.width, y);
    
    [self skinChanged];
    
}

#pragma mark - Buttons actions

- (IBAction)buttonSharePressed:(UIButton *)button {

    if (_delegate && [_delegate respondsToSelector:@selector(buttonShareSeriesPressed)]) {
        
        [_delegate buttonShareSeriesPressed];
        
    }
    
}

@end
