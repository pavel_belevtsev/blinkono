//
//  SocialView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 23.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerSocialView.h"
#import "SocialNoneCommentsView.h"
#import "SocialHeaderForFilterView.h"
#import "SocialCommentFilterCell.h"
#import "TVUnderlineButton.h"
#import "SocialBuzzSubCell.h"
#import "UIImageView+Animation.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Tint.h"
#import "PlayerViewController.h"
#import "SocialManagement.h"
#import <TvinciSDK/TVSocialFeed.h>
#import <TvinciSDK/TVSocialFeedComment.h>

@interface PlayerSocialView () <SocialHeaderFilterDelegate>
{
    NSArray *commentsFiltersArray;
    NSInteger currentFilter;
    NSInteger commentsCount;
    
    SocialHeaderForFilterView *filterHeader;
    
}

@property (nonatomic, strong) SocialFeedPopUpView *socialFeedPopUpView;
@property (nonatomic, strong) BasePopUp *popUpToShow;
@property (nonatomic, strong) BasePopUp *currentPopUp;
@property (nonatomic, strong) UIButton *btnOfPopUpToShow;
@property (nonatomic, strong) UIButton *currentbtnOfPopUp;

@property (nonatomic, strong) TVSocialFeed *socialFeed;

@end

@implementation PlayerSocialView
/*
NSString * const SocialBuzzViewTempUserNameKey = @"userNameKey";
NSString * const SocialBuzzViewTempUserImageKey = @"userImageKey";
NSString * const SocialBuzzViewTempShareTypeKey = @"shareTypeKey";
NSString * const SocialBuzzViewTempFeedTextKey = @"feedTextKey";
NSString * const SocialBuzzViewTempFeedDateKey = @"feedDateKey";
NSString * const SocialBuzzViewTempSubFeedsKey = @"numSubFeedsKey";
*/
const NSInteger SocialBuzzViewH = 88;
const NSInteger SocialBuzzCommentsFooterViewH = 30;

const CGFloat popUpSocialViewXOrigin = 558.0;
const CGFloat popUpSocialViewYOrigin = 46.0;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)awakeFromNib
{
    commentsCount= 0;
    
    [self createFilterArray];
    [commentsFilterTable reloadData];
    [commentTable reloadData];
    
    filterHeader = [[[NSBundle mainBundle] loadNibNamed:@"SocialHeaderForFilterView" owner:nil options:nil] objectAtIndex:0];
    filterHeader.delegate = self;
    
    self.rowsState = [[NSMutableArray alloc] init];
    
    if (isPad) {
        UIView *tableView = [commentTable superview];
        
        tableView.layer.cornerRadius = 6;
        tableView.layer.masksToBounds = YES;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(socialCommentAdded:) name:KSocialCommentAddedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skinChanged) name:TVNUserSkinChangedNotification object:nil];
}

- (LikeAdapter*) likeAdapter {
    if (!_likeAdapter)
        _likeAdapter = [[LikeAdapter alloc] initWithMediaItem:nil control:_buttonLike label:nil imgLike:_imgLikeCounter labelCounter:_labelLikeCounter];
    return _likeAdapter;
}

- (void)skinChanged {

    if (isPad) {
        
        _labelLikeCounter.textColor = APST.brandTextColor;
        
        [_buttonLike setImage:[[UIImage imageNamed:@"details_like_icon"] imageTintedWithColor:APST.brandTextColor] forState:UIControlStateSelected];
        
        [_buttonLike setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
        
        [_buttonLike setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
        
        _imgLikeCounter.image = [[UIImage imageNamed:@"btn_like_counter_base"] imageTintedWithColor:APST.brandColor];
        
        [_buttonComment setImage:[[UIImage imageNamed:@"btn_comment_icon_normal"] imageTintedWithColor:APST.brandTextColor] forState:UIControlStateHighlighted];
        
        [_buttonComment setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
        
        [_buttonComment setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateHighlighted];
        
    }

    if (commentsFilterTable) {
        [commentsFilterTable reloadData];
    }
}

- (void)socialCommentAdded:(NSNotification *)notification {
    
    if (notification.userInfo && [notification.userInfo objectOrNilForKey:@"mediaID"]) {
        if (_item && [_item.mediaID isEqualToString:[notification.userInfo objectOrNilForKey:@"mediaID"]]) {
            [self updateMediaFeed];
        }
    }
    
}

- (void)initInterface {
    
    [Utils setLabelFont:labelNotAvailable bold:YES];
    labelNotAvailable.text = LS(@"socialBuzz_NoSocialFeedForChannelMessage");
    
    if (!viewLogin){
        viewLogin = [[LoginInterMediateView alloc] initWithNib];
        [self addSubview:viewLogin];
        viewLogin.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        viewLogin.delegateController = _delegateController;
        [self enableContent];
        

    }

    [_delegateController initBrandButtonUI:viewLogin.buttonLogin];
    [viewLogin.buttonLogin setTitle:[NSString stringWithFormat:LS(@"login_brand"), APST.brandName] forState:UIControlStateNormal];
    
    [_delegateController initFacebookButtonUI:viewLogin.buttonFacebook];
    [viewLogin.buttonFacebook setTitle:LS(@"login_facebook") forState:UIControlStateNormal];
    
    [Utils setLabelFont:viewLogin.labelLogin bold:YES];
    viewLogin.labelLogin.text = LS(@"my_zone_login_needed");
    
    
    viewLogin.scrollViewLogin.contentSize = viewLogin.viewLoginContent.frame.size;
    viewLogin.viewLoginContent.center = CGPointMake(viewLogin.viewLoginContent.center.x, viewLogin.frame.size.height/2);

    if (isPad) {
        [Utils setLabelFont:labelTitle bold:YES];
    
        [Utils setButtonFont:_btnFilter bold:YES];
        [_btnFilter setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"575757")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)] forState:UIControlStateNormal];
        [_btnFilter setTitle:LS(@"socialBuzz_AllFeeds") forState:UIControlStateNormal];
        
        [_btnFilter addTarget:self action:@selector(showSocialFeedPopUp:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImage *image = [UIImage imageNamed:@"season_gray_down_arrow_icon"];
        self.imgFilter = [[UIImageView alloc] initWithImage:image];
        self.imgFilter.highlightedImage = [UIImage imageNamed:@"season_gray_up_arrow_icon"];
        self.imgFilter.center = CGPointMake(_btnFilter.frame.size.width - 14.0, _btnFilter.frame.size.height / 2.0);
        
        [self.btnFilter addSubview:self.imgFilter];
        
        [self initializeSocialFeedPopUpView];
        
        _labelLikeCounter.highlightedTextColor = CS(@"bebebe");
        
        [Utils setLabelFont:_labelLikeCounter bold:YES];
        
        [_buttonLike setImage:[[UIImage imageNamed:@"details_like_icon"] imageTintedWithColor:CS(@"bebebe")] forState:UIControlStateNormal];
        
        [_buttonLike.titleLabel setFont:_labelLikeCounter.font];
        
        [_buttonLike setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"575757")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
        
        _imgLikeCounter.highlightedImage = [[UIImage imageNamed:@"btn_like_counter_base"] imageTintedWithColor:CS(@"575757")];
        
        
        [_buttonComment setTitle:LS(@"comment") forState:UIControlStateNormal];
        
        [_buttonComment setImage:[[UIImage imageNamed:@"btn_comment_icon_normal"] imageTintedWithColor:CS(@"bebebe")] forState:UIControlStateNormal];
        
        [_buttonComment setTitleColor:CS(@"bebebe") forState:UIControlStateNormal];
        
        [Utils setButtonFont:_buttonComment bold:YES];
        
        [_buttonComment setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"575757")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
        
        [self skinChanged];
        
        /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
        if (!APST.fbSupport) {
            CGRect loginButtonFrame = viewLogin.buttonLogin.frame;
            loginButtonFrame.origin.x = (viewLogin.frame.size.width/2) - (viewLogin.buttonLogin.frame.size.width/2);
            [viewLogin.buttonLogin setFrame:loginButtonFrame];
            [viewLogin.buttonFacebook setHidden:YES];
        }
    }else{

            [viewLogin.buttonLogin setFrame:viewLogin.buttonFacebook.frame];
            [viewLogin.buttonFacebook setHidden:YES];
   
    }
}

- (IBAction)buttonCommentPressed:(UIButton *)button {

    [self.delegateController shareAction:PlayerActionTypeComment];
    
}

#pragma mark Button Filter iPad

#pragma mark - Touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (isPad) {
    
        UITouch *touch = [touches anyObject];
        CGPoint touchPoint = [touch locationInView:self];
        if ([self.layer containsPoint:touchPoint]) {
            [self.socialFeedPopUpView dismissPopUp];
        }
        
    }
    
}

- (void)initializeSocialFeedPopUpView {
    self.socialFeedPopUpView =  [[SocialFeedPopUpView alloc]init];
    self.socialFeedPopUpView.frame = CGRectMake(popUpSocialViewXOrigin, popUpSocialViewYOrigin, CGRectGetWidth(self.socialFeedPopUpView.frame), CGRectGetHeight(self.socialFeedPopUpView.frame));
    self.socialFeedPopUpView.popUpRectView = CGRectMake(popUpSocialViewXOrigin, popUpSocialViewYOrigin, CGRectGetWidth(self.socialFeedPopUpView.frame), CGRectGetHeight(self.socialFeedPopUpView.frame));
    self.socialFeedPopUpView.popUpSuperView = self;
    self.socialFeedPopUpView.isDismissAutomatically = YES;
    
    self.socialFeedPopUpView.socialBuzzView = self;
    
    //self.socialFeedPopUpView.socialTypeTableView.delegate = self;
    //self.socialFeedPopUpView.socialTypeTableView.dataSource = self;
    
    [self.socialFeedPopUpView setViewForNumItems:4];
    
}

- (void)showSocialFeedPopUp:(UIButton *)button {
    
    [self.socialFeedPopUpView setSelectedFilterIndex:self.filterType];
    
    if (self.socialFeedPopUpView.superview) {
        ((UIButton*)button).selected = NO;
        self.imgFilter.highlighted = NO;
        [self.socialFeedPopUpView dismissPopUp];
    }
    else {
        if (![self.subviews containsObject:self.currentPopUp]) {//there is no popup in the screen
            ((UIButton*)button).selected = YES;
            self.imgFilter.highlighted = YES;
            self.currentPopUp = self.socialFeedPopUpView;
            self.currentPopUp.popUpDelegate = self;
            self.currentbtnOfPopUp = ((UIButton*)button);
            
            [self.socialFeedPopUpView showPopUp];
            
        } else {//there is a popup in the screen, first remove than add other
            [self.currentPopUp dismissPopUp];
            self.popUpToShow = self.socialFeedPopUpView;
            self.btnOfPopUpToShow = ((UIButton*)button);
        }
    }
    
}

- (void)updateMediaFeedWithFilter:(NSDictionary *)filter {
    
    [_btnFilter setTitle:[filter objectForKey:@"FilterTitle"] forState:UIControlStateNormal];
    self.filterType = [[filter objectForKey:@"FilterType"] integerValue];
    
    [self.socialFeedPopUpView dismissPopUp];
    
    
    [self.rowsState removeAllObjects];
    
    NSArray *feedsData = [self getFeedArray];
    for(int i = 0; i < feedsData.count; i++) {
        [self.rowsState addObject:@(0)];
    }
    
    [commentTable reloadData];
    
    [commentTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

#pragma mark - TVNBasePopUpDelegate

- (void)didAddPopUp:(BasePopUp *)popUp {
    //[self bringSubviewToFront:self.middleNavigationBar];
}

- (void)didEndAddPopUp:(BasePopUp*)popUp {
}

- (void)didDismissPopUp:(BasePopUp*)popUp autoDismiss:(BOOL)autoDismiss{
    self.currentbtnOfPopUp.selected = NO;
    self.imgFilter.highlighted = NO;
    self.currentPopUp.popUpDelegate = nil;
    self.currentPopUp = nil;
    if (self.popUpToShow) { //threre is a popup to show
        [self.popUpToShow showPopUp];
        self.btnOfPopUpToShow.selected = YES;
        self.imgFilter.highlighted = YES;
        
        self.currentPopUp = self.popUpToShow;
        self.currentbtnOfPopUp = self.btnOfPopUpToShow;
        self.currentPopUp.popUpDelegate = self;
        self.popUpToShow = nil;
        self.btnOfPopUpToShow = nil;
    }
}

#pragma mark -

- (void)enableContent {
    
    viewLogin.buttonFacebook.enabled = YES;
    
    viewLogin.buttonLogin.enabled = YES;
    
    if ([[viewLogin.viewButtonCreate subviews] count]) {
        TVUnderlineButton *buttonCreate = [[viewLogin.viewButtonCreate subviews] objectAtIndex:0];
        buttonCreate.enabled = YES;
    }
    
    viewLogin.hidden = YES;
}

- (void)showStartScreenWithType:(int)type {
    
    viewLogin.buttonFacebook.enabled = NO;
    viewLogin.buttonLogin.enabled = NO;
    
    if ([[viewLogin.viewButtonCreate subviews] count]) {
        TVUnderlineButton *buttonCreate = [[viewLogin.viewButtonCreate subviews] objectAtIndex:0];
        buttonCreate.enabled = NO;
    }
}

- (void)updateMedia:(TVMediaItem *)mediaItem {
    
    self.item = mediaItem;
    
    if (isPad) {
        [self.likeAdapter setupMediaItem:mediaItem];
        _labelLikeCounter.text = [Utils getStringFromInteger: _item.likeCounter];
    }
    
    //[self updateMediaFeed];
    labelTitle.text = [NSString stringWithFormat:@"%@ @ %@", LS(@"social_feed"), self.item.name];
    
    self.socialFeed = nil;
    
    [self initElements];
    
    [commentTable removeAllSubviewsKindOfClass:[SocialNoneCommentsView class]];
    
    viewLogin.labelLogin.text = [NSString stringWithFormat:@"%@ %@", LS(@"socialBuzz_LoginMessage"), self.item.name];
}

- (void)updateMediaFeed {
    
    if ([LoginM isSignedIn]) {
        
        viewLogin.alpha = 0.0;
        viewLogin.hidden = YES;
    
        NSLog(@"updateMediaFeed");
        
        BOOL isEnable = YES;
        
        if (self.item.isLive) {
            
            if ([[self.item.metaData objectForKey:@"Limited UI channel"] intValue] > 0) {
                isEnable = NO;
            }
        }
        
        viewNotAvailable.hidden = isEnable;
        
        if (!isEnable) return;
        //if (!ContM.isConnectedUser) return;
        
        [filterHeader setFilterEnabled:([_feedsData count])];
        if (self.commentFilterOpened) {
            [self dropFilterTable];
        }
        
        if ([_feedsData count] == 0) {
            [imgViewActivity startBrandAnimation];
        }
        
        PlayerSocialView *weakSelf = self;
        UIImageView *weakImgActivity = imgViewActivity;
        
        [_delegateController.socialManager requestforGetSocialFeedWithMediaID:[self.item.mediaID integerValue] completionBlock:^(id jsonResponse) {
            
            if (jsonResponse) {
                
                self.socialFeed = [[TVSocialFeed alloc] initWithDictionary:jsonResponse];
                
                [weakImgActivity stopBrandAnimation];
                
                [weakSelf initElements];
            }
            else
            {
                [weakImgActivity stopBrandAnimation];
                [self checkAndCreateNonCommentsView:0];
            }
        }];
        
    } else {
        
        viewLogin.alpha = 1.0;
        viewLogin.hidden = NO;
    }
}

- (void)initElements {
    
    NSMutableArray *arrayFeed = [[NSMutableArray alloc] init];
    
    if (_socialFeed) {
        
        for (TVSocialFeedComment *comment in _socialFeed.inAppCommentsArr) {
            [arrayFeed addObject:comment];
        }
        for (TVSocialFeedComment *comment in _socialFeed.facebookCommentsArr) {
            [arrayFeed addObject:comment];
        }
        for (TVSocialFeedComment *comment in _socialFeed.twitterCommentsArr) {
            [arrayFeed addObject:comment];
        }
        
        [arrayFeed sortUsingComparator:^NSComparisonResult(TVSocialFeedComment *a, TVSocialFeedComment *b) {
            NSDate *first = a.createDate;
            NSDate *second = b.createDate;
            
            return [second compare:first];
            
        }];
        
        self.feedsDataInApp = [_socialFeed.inAppCommentsArr sortedArrayUsingComparator:^NSComparisonResult(TVSocialFeedComment *obj1, TVSocialFeedComment *obj2) {
            return [obj2.createDate compare:obj1.createDate];
        }];
        self.feedsDataFacebook = [_socialFeed.facebookCommentsArr sortedArrayUsingComparator:^NSComparisonResult(TVSocialFeedComment *obj1, TVSocialFeedComment *obj2) {
            return [obj2.createDate compare:obj1.createDate];
        }];
        self.feedsDataTwitter = [_socialFeed.twitterCommentsArr sortedArrayUsingComparator:^NSComparisonResult(TVSocialFeedComment *obj1, TVSocialFeedComment *obj2) {
            return [obj2.createDate compare:obj1.createDate];
        }];
        
    } else {
        
        self.feedsDataInApp = [NSArray array];
        self.feedsDataFacebook = [NSArray array];
        self.feedsDataTwitter = [NSArray array];
        
    }
    
    self.feedsData = arrayFeed;
    
    
    [self.rowsState removeAllObjects];
    
    for(int i = 0; i < _feedsData.count; i++) {
        [self.rowsState addObject:@(0)];
    }
    
    [commentTable reloadData];
    
    [filterHeader setFilterEnabled:([_feedsData count])];
    
    commentsCount = [_feedsData count];
    [self checkAndCreateNonCommentsView:0];
    
    [commentsFilterTable reloadData];
    
    [self.socialFeedPopUpView updateButtons:@[@([_feedsData count]), @([_feedsDataInApp count]), @([_feedsDataFacebook count]), @([_feedsDataTwitter count])]];
}

- (void)checkAndCreateNonCommentsView:(NSInteger)filterNumber
{
    [commentTable removeAllSubviewsKindOfClass:[SocialNoneCommentsView class]];
    
    if (commentsCount == 0)
    {
        SocialNoneCommentsView *view = [[[NSBundle mainBundle] loadNibNamed:@"SocialNoneCommentsView" owner:nil options:nil] objectAtIndex:0];
        view.delegateView = self;
        
        switch (filterNumber) {
            case 0:
                [view setViewWithFirstText:LS(@"socialBuzz_NoSocialFeedComments") secondText:self.item.name buttonImageName:@"details_all_feed_icon" buttonText:@"Comment it"];
                break;
            case 1:
                [view setViewWithFirstText:@"Be the first to comment about" secondText:self.item.name buttonImageName:@"details_comments_icon" buttonText:@"Comment it"];
                break;
            case 2:
                [view setViewWithFirstText:@"Be the first to comment about" secondText:self.item.name buttonImageName:@"details_facebook_icon" buttonText:@"Comment on Facebook"];
                break;
            case 3:
                [view setViewWithFirstText:@"Be the first to tweet about" secondText:self.item.name buttonImageName:@"details_twitter_icon" buttonText:@"Comment on Twitter"];
                break;
            default:
                break;
        }
        
        view.center = CGPointMake((int)(commentTable.frame.size.width / 2.0), (int)(commentTable.frame.size.height / 2.0));
        [commentTable addSubview:view];
    }
    
    [commentTable reloadData];
    
}

- (void)createFilterArray
{
    NSString *filter_0 = LS(@"all_feeds");
    UIImage *image_0 = [UIImage imageNamed:@"details_all_feed_icon"];
    NSDictionary *filt_0 = [NSDictionary dictionaryWithObjects:@[filter_0, image_0] forKeys:@[@"filterName", @"filterIcon"]];
    
    NSString *filter_1 = LS(@"comment");
    UIImage *image_1 = [UIImage imageNamed:@"details_comments_icon"];
    NSDictionary *filt_1 = [NSDictionary dictionaryWithObjects:@[filter_1, image_1] forKeys:@[@"filterName", @"filterIcon"]];
    
    NSString *filter_2 = @"Facebook";
    UIImage *image_2 = [UIImage imageNamed:@"details_facebook_icon"];
    NSDictionary *filt_2 = [NSDictionary dictionaryWithObjects:@[filter_2, image_2] forKeys:@[@"filterName", @"filterIcon"]];
    
    NSString *filter_3 = @"Twitter";
    UIImage *image_3 = [UIImage imageNamed:@"details_twitter_icon"];
    NSDictionary *filt_3 = [NSDictionary dictionaryWithObjects:@[filter_3, image_3] forKeys:@[@"filterName", @"filterIcon"]];
    
    commentsFiltersArray = [NSArray arrayWithObjects:filt_0, filt_1, filt_2, filt_3, nil];
}

#pragma mark - Table View

- (NSArray *)getFeedArray {
    
    if (self.filterType == 1) {
        return _feedsDataInApp;
    } else if (self.filterType == 2) {
        return _feedsDataFacebook;
    } else if (self.filterType == 3) {
        return _feedsDataTwitter;
    } else {
        return _feedsData;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == commentTable)
    {
        NSArray *feedsData = [self getFeedArray];
        
        return feedsData.count;
    }
    else if (tableView == commentsFilterTable)
    {
        return [commentsFiltersArray count]-1;
    }
    
    else return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == commentTable) {
        
        NSArray *feedsData = [self getFeedArray];
        
        TVSocialFeedComment *feed = feedsData[indexPath.row];
        
        int cellHeight = [SocialBuzzCell getCellHeightForFeedText:feed.commentBody];
        
        if([_rowsState[indexPath.row] intValue] > 0) {
            NSArray *subFeeds = feed.subComments;
            
            NSInteger count = MIN([_rowsState[indexPath.row] intValue], subFeeds.count);
            
            NSInteger subCellHeight = 0;
            
            for (int i = 0; i < count; i++) {
                
                TVSocialFeedComment *subFeed = subFeeds[i];
                
                subCellHeight = subCellHeight + [SocialBuzzSubCell getCellHeightForFeedText:subFeed.commentBody];
                
            }
            
            cellHeight += subCellHeight + (count < subFeeds.count ? SocialBuzzCommentsFooterViewH : 0);
            
            return cellHeight;
        }
        return cellHeight;
        
    } else {
        
        return tableView.rowHeight;
        
    }

}

- (SocialBuzzShareType)commentFeedType:(TVSocialFeedComment *)feed {
    
    if (_socialFeed) {
        if ([_socialFeed.facebookCommentsArr containsObject:feed]) {
            return SocialBuzzShareTypeFacebook;
        } else if ([_socialFeed.twitterCommentsArr containsObject:feed]) {
            return SocialBuzzShareTypeTwitter;
        }

    }
    
    return SocialBuzzShareTypeTvinci;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == commentTable)
    {
        static NSString *SocialBuzzCellIdentifier = @"SocialBuzzCellIdentifier";
        SocialBuzzCell *socialBuzzCell = [tableView dequeueReusableCellWithIdentifier:SocialBuzzCellIdentifier];
        if (socialBuzzCell == nil) {
            NSArray *views = [[NSBundle mainBundle] loadNibNamed:[Utils nibName:NSStringFromClass([SocialBuzzCell class])] owner:self options:nil];
            socialBuzzCell =  [views objectAtIndex:0];
        }
        socialBuzzCell.selectionStyle = UITableViewCellSelectionStyleNone;
        socialBuzzCell.delegate = self;
        socialBuzzCell.socialBuzzView = self;
        
        socialBuzzCell.tag = indexPath.row;
        
        NSArray *feedsData = [self getFeedArray];
        
        TVSocialFeedComment *feed = feedsData[indexPath.row];
        [socialBuzzCell setUserName:feed.creatorName andShareType:[self commentFeedType:feed]];
        socialBuzzCell.feedText.text = feed.commentBody;
        [socialBuzzCell updateUIForFeedText];
        socialBuzzCell.feedDate.text = [Utils getActionDurationTimeFromNow:feed.createDate];
        socialBuzzCell.subFeeds = feed.subComments;
        
        // nav_bar_user_avavtar_default.png
        
        NSURL *strURL = feed.creatorUrl;
        if (strURL) {

            [socialBuzzCell.avatarImage sd_setImageWithURL:strURL placeholderImage:[UIImage imageNamed:@"avatar"]];
            
        } else {
            
            socialBuzzCell.avatarImage.image = [UIImage imageNamed:@"avatar"];
            
        }
        
        if (socialBuzzCell.subFeeds.count) {
            socialBuzzCell.subFeedsNum.text = [Utils getStringFromInteger: socialBuzzCell.subFeeds.count];
            socialBuzzCell.cornerImage.hidden = NO;
        } else {
            socialBuzzCell.subFeedsNum.text = @"";
            socialBuzzCell.cornerImage.hidden = YES;
        }
        
        socialBuzzCell.isOpened = ([_rowsState[indexPath.row] intValue] > 0);
        if ([_rowsState[indexPath.row] intValue] > 0) {
            [socialBuzzCell addFeedsToCell:[_rowsState[indexPath.row] intValue]];
        } else {
            [socialBuzzCell removeFeedsFromCell];
        }
        
        
        socialBuzzCell.backgroundColor = [UIColor clearColor];
        socialBuzzCell.backgroundView = [UIView new];
        socialBuzzCell.selectedBackgroundView = [UIView new];
        
        return socialBuzzCell;

    }
    else if (tableView == commentsFilterTable)
    {
        static NSString * const st_cellId = @"SocialCommentFilterCell";
        SocialCommentFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:st_cellId];
        
        if (!cell)
        {
            NSArray *views = [[NSBundle mainBundle] loadNibNamed:st_cellId owner:tableView options:nil];
            for( UIView *view in views )
            {
                if( [view isKindOfClass:[UITableViewCell class]] )
                {
                    cell= (SocialCommentFilterCell *)view;
                    break;
                }
            }
        }
        
        cell.filterName.font= [UIFont fontWithName:FONT_BOLD size:15];
        NSInteger index= indexPath.row;
        
        if (currentFilter<=index)
        {
            index++;
        }
        
        NSInteger itemsCount = 0;
        
        if (index == 0) itemsCount = [_feedsData count];
        if (index == 1) itemsCount = [_feedsDataInApp count];
        if (index == 2) itemsCount = [_feedsDataFacebook count];
        if (index == 3) itemsCount = [_feedsDataTwitter count];
        
        cell.userInteractionEnabled = (itemsCount > 0);
        
        
        cell.filterName.text= [[commentsFiltersArray objectAtIndex:index] objectForKey:@"filterName"];
        //[cell changeFilterIcon:[[commentsFiltersArray objectAtIndex:index] objectForKey:@"filterIcon"]];
        
        UIImage *iconName = [[commentsFiltersArray objectAtIndex:index] objectForKey:@"filterIcon"];
        
        if (cell.userInteractionEnabled) {
            
            cell.filterIcon.image = [iconName imageTintedWithColor:APST.brandTextColor];
            cell.filterIcon.highlightedImage = [iconName imageTintedWithColor:APST.brandColor];
        
        } else {
            
            cell.filterIcon.image = [iconName imageTintedWithColor:[UIColor colorWithWhite:0.5 alpha:1.0]];
            
        }
        return cell;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.1f)];
    
    if (tableView == commentsFilterTable) {
        [filterHeader setIconAndNameFilter:[[commentsFiltersArray objectAtIndex:currentFilter] objectForKey:@"filterIcon"] name:[[commentsFiltersArray objectAtIndex:currentFilter] objectForKey:@"filterName"]];
        
        return filterHeader;
    }
    
    return  header;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == commentTable) {
        
        SocialBuzzCell *socialCell = (SocialBuzzCell *)[tableView cellForRowAtIndexPath:indexPath];
        BOOL state = !socialCell.isOpened;
        [self reloadSelectFeedAtIndex:indexPath.row andState:state];
        
    } else if (tableView == commentsFilterTable)
    {
        if (indexPath.row >= currentFilter)
        {
            currentFilter= indexPath.row+1;
        }
        else
        {
            currentFilter= indexPath.row;
        }
        
        self.filterType = currentFilter;
        
        NSArray *feedsData = [self getFeedArray];
        
        [self.rowsState removeAllObjects];
        
        for(int i = 0; i < feedsData.count; i++) {
            [self.rowsState addObject:@(0)];
        }
        
        [self dropFilterTable];
        [commentsFilterTable beginUpdates];
        [commentsFilterTable reloadData];
        [commentsFilterTable endUpdates];
        
        [commentTable reloadData];
        
    } 
}

- (NSString *)stringFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    return [formatter stringFromDate:date];
}

#pragma mark - SocialBuzzCellDelegate

- (void)reloadSelectFeedAtIndex:(NSInteger)selectedIndex andState:(BOOL)state {
    
    NSArray *feedsData = [self getFeedArray];
    
    BOOL needScroll = NO;
    
    for(int i = 0; i < feedsData.count; i++) {
        if(i == selectedIndex) {
            
            NSInteger count = 0;
            
            if (state) {
                count = 3;
                NSArray *subFeeds = ((TVSocialFeedComment *)feedsData[i]).subComments;
                
                if ([subFeeds count]) needScroll = YES;
                
                count = MIN(count, subFeeds.count);
                
            }
            
            self.rowsState[selectedIndex] = @(count);
        } else {
            self.rowsState[i] = @(0);
        }
    }
    NSIndexPath *selectedFeedIndex = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
    [commentTable reloadRowsAtIndexPaths:@[selectedFeedIndex] withRowAnimation:UITableViewRowAnimationFade];
    if (!needScroll) {
        if (![[commentTable visibleCells] containsObject:[commentTable cellForRowAtIndexPath:[NSIndexPath indexPathForItem:selectedIndex inSection:0]]]) {
            needScroll = YES;
        }
    }
    if (needScroll) {
        
        [commentTable scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:selectedIndex inSection:0]
                            atScrollPosition:UITableViewScrollPositionTop
                                    animated:NO];
    }

}

- (void)loadMoreAction:(UIButton *)button {
    
    NSInteger selectedIndex = button.tag;
    
    if (selectedIndex < _rowsState.count) {
        
        NSInteger count = [_rowsState[selectedIndex] intValue];
        
        count += 3;
        
        NSArray *feedsData = [self getFeedArray];
        
        NSArray *subFeeds = ((TVSocialFeedComment *)feedsData[selectedIndex]).subComments;
        
        count = MIN(count, subFeeds.count);
        
        self.rowsState[selectedIndex] = @(count);
        
        NSIndexPath *selectedFeedIndex = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        [commentTable reloadRowsAtIndexPaths:@[selectedFeedIndex] withRowAnimation:UITableViewRowAnimationFade];
        [commentTable scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:selectedIndex inSection:0]
                         atScrollPosition:UITableViewScrollPositionBottom
                                 animated:NO];
        
    }
}

#pragma mark filterHeaderDelegate methods

- (void)dropFilterTable
{
    CGRect frame = commentTable.frame;
    [commentsFilterTable setUserInteractionEnabled:NO];
    
    if (self.commentFilterOpened)
    {
        frame.origin.y = 36.f;
        self.commentFilterOpened= NO;
    }
    else
    {
        frame.origin.y = 144.f;
        self.commentFilterOpened= YES;
    }
    
    [UIView animateWithDuration:0.2f animations:^{
        commentTable.frame = frame;
        
    } completion:^(BOOL finished) {
        [commentsFilterTable setUserInteractionEnabled:YES];
    }];
    [filterHeader setOpened:self.commentFilterOpened];
}

- (void)addComment
{
    [self.delegateController.socialManager shareAction:PlayerActionTypeComment];
}

@end
