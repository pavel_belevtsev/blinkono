//
//  PlayerLiveDetailsView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"
#import "PlayerProgramCollectionViewCell.h"
#import "RecoredAdapter.h"
#import "MiniEpgRecoredPopUp.h"
#import "RecoredHelper.h"
#import "CancelSeriesRecordingPopUp.h"


@class PlayerLiveDetailsView;
@class TVNPVRRecordItem;

@protocol PlayerLiveDetailsViewDelegate <NSObject>

-(void) playerLiveDetails:(PlayerLiveDetailsView *) sender didSelectPlayProgram:(APSTVProgram *) program forChannel:(TVMediaItem *) channel;


@end

@class PlayerViewController;

@interface PlayerLiveDetailsView : UINibView<PlayerProgramCollectionViewCellDelegate, RecoredAdapterDelegate,MiniEpgRecoredPopUpDelegate,CancelSeriesRecordingPopUpDelegate>

- (void)updateData:(TVMediaItem *)item withFirstShownProgram:(APSTVProgram *) program;
- (void)checkLiveProgram:(BOOL)animated;
-(void) scrollToProgram:(APSTVProgram *) program animated:(BOOL) animated;


@property (nonatomic, strong) TVMediaItem *item;

@property (nonatomic, weak) IBOutlet UIImageView *imgThumb;
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionViewEPG;
@property (nonatomic, weak) IBOutlet UIScrollView *hiddenScrollView;
@property (weak, nonatomic) IBOutlet UIView *viewNoDataForChannel;
@property (weak, nonatomic) IBOutlet UILabel *labelNoDataForChannel;
@property (nonatomic, strong) NSMutableArray *epgList;



// This is not delegate, this is only weak pointer to a known object.
@property (nonatomic, weak) PlayerViewController<PlayerVODDetailsViewDelegate> *delegateController;

@property (nonatomic, weak) id<PlayerLiveDetailsViewDelegate> delegate;

@property (nonatomic, strong) NSMutableDictionary *dictionaryRequest;
@property (nonatomic, strong) NSMutableDictionary *dictionaryNpvrRequest;

@property (nonatomic, strong) RecoredAdapter * recoredAdapter;

@property (nonatomic, strong) MiniEpgRecoredPopUp * miniEpgRecoredPopUp;
@property (strong, nonatomic) CancelSeriesRecordingPopUp* cancelSeriesRecordingPopUp;

@property (strong, nonatomic) TVNPVRRecordItem * recordItem;



@end
