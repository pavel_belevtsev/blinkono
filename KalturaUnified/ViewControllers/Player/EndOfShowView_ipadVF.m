//
//  EndOfShowView_ipadVF.m
//  Vodafone
//
//  Created by Admin on 05.10.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EndOfShowView_ipadVF.h"

@implementation EndOfShowView_ipadVF

#define kButtonHeight @49
#define kButtonWidth @238

/* "PSC : Vodafone Group : SYN-4029 : StartOver option appear when returning from background */
- (void) createButtonsForLiveMedia:(UIView*)target {
    
    UIButton *btnBackToLive = [self createButtonOnTarget:target text:LS(@"player_dialog_end_of_show_back_to_live") image:[UIImage imageNamed:@"endofshow_backtolive"] sameUIForAllStates:YES clickBlock:^(UIButton *btn) {
        [self.delegate buttonBackToLivePressed];
    }];
    
    UIView *middleEmptyView = [self createMiddleEmptyViewOnTatget:target];
    
    NSDictionary *metricsButtons = @{@"b1width": @15,
                                     @"b2width": kButtonWidth,
                                     @"b3width": @15,
                                     @"b1height": kButtonHeight,
                                     @"b2height": kButtonHeight,
                                     @"b3height": kButtonHeight,
                                     @"space": @0};
    [self updateUIWithButtons:@[middleEmptyView, btnBackToLive, middleEmptyView] metrics:metricsButtons onTargetView:target];
}

@end
