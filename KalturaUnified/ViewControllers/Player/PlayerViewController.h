//
//  PlayerViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 22.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//
#ifdef PLAYER_WV

#import <TVCPlayerWV/TVCPlayer.h>
#import <TVCPlayerWV/TVCPlayerFactory.h>
#import <TVCPlayerWV/TVCPltvUtils.h>


#endif

#ifdef PLAYER_DX

#import <SecurePlayer/SecurePlayer.h>
#import <TVCPlayerStaticLibrary/TVCPlayer.h>
#import <TVCPlayerStaticLibrary/TVCPlayerFactory.h>
#import <TVCPlayerStaticLibrary/TVCPltvUtils.h>

#endif

#import "PlayerVODDetailsView.h"
#import "PlayerLiveDetailsView.h"
#import "PlayerSocialView.h"
#import "PlayerSeriesDetailsView.h"
#import "PlayerSeriesEpisodesView.h"
#import "ShareContentView.h"
#import "ShareView.h"
#import "SharePopUpView.h"
#import "CoachMarksView.h"
#import "PlayerVODContinueWatchingPopUp.h"
#import "PlayerLangContentView.h"
#import "PlayerLangViewPad.h"
#import "PlayerPinView.h"
#import "HomeNavProfileView.h"
#import "CompanionViewPhone.h"
#import "PlayerSoundSlider.h"
#import "LikeAdapter.h"
#import "FavoriteAdapter.h"
#import "SocialManagement.h"
#import "EndOfShowView.h"
#import "EPGRcoredStatusView.h"
#import "WatchAdapter.h"
#import "PlayerDTGAdapter.h"


@class PlayerVODInfoView;
@class SocialManagement;
@class EUOnlineWatchList;
@class CompanionModeOverlay;
@class NPVRPlayerAdapter;
@class PlayerNPVRInfoView;
@class PlayerPLTVAdapter;



@interface PlayerViewController : BaseViewController <PlayerVODDetailsViewDelegate, BasePopUpDelegate, CoachMarksViewDelegate, PlayerVODContinueWatchingPopUpDelegate, LangViewDelegate, HomeNavProfileViewDelegate, PlayerSeriesDetailsViewDelegate, PlayerSoundSliderDelegate, PlayerSocialViewDelegate, ShareContentViewDelegate, SocialManagementDelegate, PlayerEndOfShowDelegate,UINavigationControllerDelegate, PlayerDTGAdapterDelegate> {
    
    BOOL isInitialized;
    BOOL returnToViewAfterLogin;
    BOOL returnToViewAfterShare;
    
    CGFloat playerWidth, playerHeight, playerX, playerY;
    CGFloat playerFullscreenWidth, playerFullscreenHeight, playerFullscreenX, playerFullscreenY;
    
    CGFloat viewContentY, viewContentYOpen, viewDetailsContentY;
}

- (void)selectMediaItem:(TVMediaItem *)mediaItem;
- (void)setLivePlayer;

- (void)dismissSocialShareView;

- (void)showCommentView:(UIView *)viewComment;
- (void)removeCommentView;
- (void)loginWithFB;

- (void)selectSeriesSeason:(NSInteger)seasonNum;

- (void)hideCompanionScreen;
- (void)startTimer;
- (void)updateItemMainActionState;
- (BOOL)playerIsPlaying;
- ( NSString *) bestFileformat;

@property (weak, nonatomic)   TVMediaItem *initialMediaItem;
@property (strong, nonatomic) TVMediaItem *playerMediaItem;
@property (strong, nonatomic) NSMutableArray *arrPlayerMediasStack;

@property (weak, nonatomic) IBOutlet UIButton *buttonStartover;

@property (nonatomic, weak) IBOutlet UIView *viewRightOpen;
@property (nonatomic, weak) IBOutlet UILabel *labelRightOpen;

@property (nonatomic, weak) IBOutlet UIButton *buttonRightOpen;

@property (nonatomic, weak) IBOutlet UIButton *buttonBack;
@property (nonatomic, weak) IBOutlet UIButton *buttonMenu;

@property (nonatomic, weak) IBOutlet UIView *viewCenter;
@property (nonatomic, weak) IBOutlet UIView *viewPanel;
@property (weak, nonatomic) IBOutlet UIImageView *recoredIconImage;
@property (weak, nonatomic) IBOutlet UIButton *recoredIconButton;

@property (weak, nonatomic) IBOutlet UIView *sepratorView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleFullscreen;
@property (weak, nonatomic) IBOutlet UILabel *labelLiveProgramTime;
@property (weak, nonatomic) IBOutlet UILabel *labelCurrentTime;
@property (weak, nonatomic) IBOutlet UILabel *labelFullTime;
@property (weak, nonatomic) IBOutlet UISlider *timeSlider;
@property (weak, nonatomic) IBOutlet UISlider *sliderReferenceLiveProgress;
@property (strong, nonatomic) IBOutlet UIButton *buttonShare;
@property (weak, nonatomic) IBOutlet UIButton *buttonInfo;
@property (weak, nonatomic) IBOutlet UIButton *buttonSound;

@property (weak, nonatomic) IBOutlet UIButton *buttonPlayPause;
@property (weak, nonatomic) IBOutlet UIButton *buttonFullScreen;

@property (weak, nonatomic) IBOutlet UIButton *buttonLang;
@property (weak, nonatomic) IBOutlet UIButton *buttonCompanion;

@property BOOL sliderChanges;
@property BOOL liveTV;
@property BOOL mediaStarted;

@property (weak, nonatomic) IBOutlet UIView *viewPlayerContainer;
@property (weak, nonatomic) IBOutlet UIView *viewTapClose;

@property (weak, nonatomic) IBOutlet UIView *viewTopControls;
@property (weak, nonatomic) IBOutlet UIView *viewBottomControls;
@property (weak, nonatomic) IBOutlet UIView *innerTopControls;

@property (nonatomic, strong) PlayerVODInfoView *playerVODInfoView;
@property (nonatomic, strong) PlayerVODDetailsView *playerVODDetailsView;
@property (nonatomic, strong) PlayerLiveDetailsView *playerLiveDetailsView; 
@property (nonatomic, strong) PlayerSocialView *playerSocialView;
@property (nonatomic, strong) PlayerSeriesDetailsView *playerSeriesDetailsView;
@property (nonatomic, strong) PlayerSeriesEpisodesView *playerSeriesEpisodesView;
@property (nonatomic, strong) PlayerNPVRInfoView * playerNPVRInfoView;
@property (nonatomic, strong) PlayerDTGAdapter *playerDTGAdapter;

@property (nonatomic, weak) IBOutlet UIView *detailsContainerView;
@property BOOL detailsContainerOpen;
@property (weak, nonatomic) IBOutlet UIView *seekBarPlaceHolder;

#ifdef PLAYER

@property (nonatomic, strong) TVMediaToPlayInfo *mediaToPlayInfo;
@property (nonatomic, strong) TVCplayer* videoPlayerControl;

#endif

@property (nonatomic, strong) NSDictionary *itemsPricesWithCoupons;
@property (nonatomic, strong) NSDictionary *itemsPricesWithCouponsNotPurchase;

@property (nonatomic, strong) NSDictionary *groupMediaRules;
@property (nonatomic) NSInteger groupModeRule;

@property (nonatomic, strong) NSDictionary *mediaMark;
@property (weak, nonatomic) IBOutlet UIView *viewTabBarBg;
@property (weak, nonatomic) IBOutlet UIView *viewTabBarMain;

@property (nonatomic, weak) IBOutlet UIView *viewTabBar;
@property (nonatomic, strong) UIButton *buttonTabLeft;
@property (nonatomic, strong) UIButton *buttonTabRight;

@property (nonatomic, strong) SocialManagement *socialManager;

@property (nonatomic, strong) ShareContentView *shareContentView;
@property (nonatomic, strong) ShareView *shareView;
@property (nonatomic, strong) PlayerLangViewPad *langPopUpView;
@property (nonatomic, strong) SharePopUpView *sharePopUpView;
@property BOOL popUpOpen;

@property (nonatomic, strong) CoachMarksView *coachMarkView;

@property (nonatomic, strong) PlayerPinView *playerPinView;

@property (nonatomic) NSInteger selectedAudioIndex;
@property (nonatomic) NSInteger selectedSubIndex;

@property (strong, nonatomic) NSMutableDictionary *seriesData;

@property (strong, nonatomic) NSMutableArray *fullList;
@property NSInteger currentSeason;
@property (strong, nonatomic) NSString *seriesName;

@property (nonatomic, weak) IBOutlet EndOfShowView *viewEndOfShow;

@property (nonatomic, strong) NSTimer *liveTVTimer;
@property (nonatomic, strong) APSTVProgram *playedProgram;
@property (nonatomic, strong) APSTVProgram * programToPlay;
@property (nonatomic, strong)  UISwipeGestureRecognizer * swipeGesture;
@property (nonatomic, strong) CompanionModeOverlay * companionModeOverlay;
@property (nonatomic, retain) IBOutlet UIView * bannerContainer;
@property (nonatomic, strong) EUOnlineWatchList *watchListAPI;

@property (strong, nonatomic) NPVRPlayerAdapter * npvrPlayerAdapter;
@property (strong, nonatomic) PlayerPLTVAdapter * pltvPlayerAdapter;

@property (strong, nonatomic) EPGRcoredStatusView * recoredStatusPopupView;
@property (strong, nonatomic) WatchAdapter * watchAdapter;
@property (weak, nonatomic) IBOutlet UIView *recoredStatusView;

@property (weak, nonatomic) IBOutlet UIView *viewStatusBar;

@property (nonatomic, readwrite) VODButtonState currentMediaState;

-(void)showRecoredIcon:(BOOL)isSeries;
-(void)hideRecoredIcon;
-(void) clearOldAttributes;
-(void) finishedUpdatingActionButtonForState:(VODButtonState)state;
-(void) buttonDownloadPressed;
-(BOOL) isMediaAllowedForDownloading:(TVMediaItem*) item;

-(void)showRecoredTostStatusViewToProgram:(APSTVProgram *) epgProgram recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus mediaItem:(TVMediaItem *) mediaItem recoredHelper:(RecoredHelper *)rec;

-(void)showDeleteRecoredTostStatusViewToProgram:(APSTVProgram *) epgProgram recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus mediaItem:(TVMediaItem *) mediaItem isSeries:(BOOL)isSeries;

- (IBAction)buttonCompanionPressed:(UIButton *)button ;
-(void)movieFinishedPresentationWithMediaItem:(TVMediaItem *)mediaItem atPlayer:(TVCplayer*)player ;
-(void)movieHasFinishedBufferingForMediaItem:(TVMediaItem *)mediaItem;
- (IBAction)buttonPlayPausePressed:(UIButton *)button;

@property (nonatomic) float playerStreamLocationBeforeCasting;
@end
