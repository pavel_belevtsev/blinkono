//
//  PlayerViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 22.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerViewController.h"
#import "PlayerRelatedViewController.h"
#import "PlayerLiveChannelsViewController.h"
#import "PlayerEpisodesViewController.h"
#import "PlayerSeasonsViewController.h"
#import "UIView+Additions.h"
#import "PlayerVODInfoView.h"
#import "PlayerLangViewPhone.h"
#import "UIImage+Tint.h"
#import <TvinciSDK/TVRuleItem.h>
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "LoginAnimatorPush.h"
#import "LoginAnimatorPop.h"
#import "CommentView.h"
#import "AnalyticsManager.h"
#import <TvinciSDK/APSTVProgram+Additions.h>
#import "TVCompanionManager.h"
#import "CompanionModeOverlay.h"
#import <TvinciSDK/TVMixedCompanionManager.h>
#import "ProximityChecker.h"
#import "NPVRPlayerAdapter.h"
#import "CustomViewFactory.h"
#import "PlayerNPVRInfoView.h"
#import "PlayerPLTVAdapter.h"
#import "UIAlertView+Blocks.h"
#import "UIView+RemoveConstraints.h"
#import "TVRental+Additions.h"
#import <TvinciSDK/TVPPVModelData.h>
#import "PPVFullData.h"


NSString * const TVNMediaPlayerSliderMinTrackImage = @"player_slider_min";
NSString * const TVNMediaPlayerSlidertMaximumTrackImage = @"player_slider";
NSString * const TVNMediaPlayerSlidertThumbImage = @"player_slider_thumb";

#define kTagEndOfShowOverlayView 5111441

typedef enum playerState
{
    playerModeNone,
    playerModeTrailer,
    playerModeVOD,
    playerModeLIVE,
    playerModeCatchUp,
    playerModeStartOver,

}playerMode;



@interface PlayerViewController ()
#ifdef PLAYER
<TVCplayerStatusProtocol, ShareViewDelegate,PlayerLiveDetailsViewDelegate,CompanionModeOverlayDelegate,CompanionViewDelegate,PlayerNPVRInfoViewDelegate>
#else
<ShareViewDelegate,PlayerLiveDetailsViewDelegate>
#endif
{
    NSTimer *timerForHidingControls;
    BOOL hasPurchasedSubscriptions;
    // "PSC : ONO : OPF-626 - Concurrent Streams - user can continue play after message confirmation
    BOOL isCurentMediaConcurrent;
}

@property (nonatomic, strong) BasePopUp *popUpToShow;
@property (nonatomic, strong) BasePopUp *currentPopUp;
@property (nonatomic, strong) UIButton *btnOfPopUpToShow;
@property (nonatomic, strong) UIButton *currentbtnOfPopUp;
@property (nonatomic, strong) PlayerSoundSlider *soundSiderView;
@property (nonatomic, assign) playerMode playerMode;
@property (nonatomic) CGRect titleLabelRect;
@property (assign, nonatomic) NSTimeInterval lastPausePosition;
@property (strong, nonatomic) APSTVProgram * lastProgramPlayed;

@property BOOL playerIsVisible;
@property BOOL fullscreenModeForPad;
@property (assign, nonatomic) BOOL blockedUI;
@property (assign, nonatomic) BOOL langButtonEnabledBeforeSwoosh;

@property (nonatomic, weak) IBOutlet UILabel *labelAutomation;

@end

#define playerRightMenuWidth    (isPad ? 315 : 270)
#define kTagTempBlackScreenView   888441

@implementation PlayerViewController

const CGFloat popUpShareViewXOrigin = 674.0;
const CGFloat popUpShareViewYOrigin = 420.0;

const NSInteger popUpLangViewPositionX = 736;
const NSInteger popUpLangViewPositionY = 486;

const CGRect playerSoundSliderRect = {928, 506, 33, 203};

-(void) registerForNotification
{
    [self unRegisterForNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];

}

-(void) unRegisterForNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)viewDidLoad {



    [self registerForNotification];
    [self registerForCompanionNotification];

    self.loadNoInternetView = YES;

    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(menuStateChange:)
                                                 name:MFSideMenuStateNotificationEvent
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authenticationCompleted) name:KAuthenticationCompletedNotification object:nil];

    if (isPad) {
        [Utils setButtonFont:_buttonBack bold:YES];
        [_buttonBack setTitle:LS(@"back") forState:UIControlStateNormal];

        [Utils setLabelFont:_labelLiveProgramTime];
        _labelLiveProgramTime.layer.cornerRadius = 2.0f;
        _labelLiveProgramTime.layer.masksToBounds = YES;
        _labelLiveProgramTime.hidden = YES;
    } else {

        [Utils setLabelFont:_labelTitleFullscreen bold:YES];
        _labelTitleFullscreen.alpha = 0.0;
        self.viewRightOpen.hidden = YES;
    }

    _arrPlayerMediasStack = [NSMutableArray array];

    if (_labelTitle) {
        [Utils setLabelFont:_labelTitle bold:YES];
    }

    if (isPhone) {
        [_buttonShare setTitle:LS(@"share") forState:UIControlStateNormal];
        [_buttonShare setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        [_recoredIconButton setTitle:LS(@"Rec") forState:UIControlStateNormal];
        [Utils setButtonFont:_buttonShare bold:YES];
        [_recoredIconButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        [Utils setButtonFont:_recoredIconButton bold:YES];
        CGFloat spacing = 5; // the amount of spacing to appear between image and title
        _recoredIconButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
        _recoredIconButton.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
    }

    self.socialManager = [[SocialManagement alloc] init];
    _socialManager.delegate = self;

    self.seriesData = [[NSMutableDictionary alloc] init];

    if (_buttonCompanion) {
        _buttonCompanion.UIUnitID = NU(UIUnit_Companion_buttons);
        [_buttonCompanion activateAppropriateRule];


        if (isPhone && _buttonCompanion.hidden) {

            _timeSlider.frame = CGRectMake(_timeSlider.frame.origin.x, _timeSlider.frame.origin.y, _timeSlider.frame.size.width + 40, _timeSlider.frame.size.height);
            _sliderReferenceLiveProgress.frame = CGRectMake(_sliderReferenceLiveProgress.frame.origin.x, _sliderReferenceLiveProgress.frame.origin.y, _sliderReferenceLiveProgress.frame.size.width + 40, _sliderReferenceLiveProgress.frame.size.height);
            _labelFullTime.frame = CGRectMake(_labelFullTime.frame.origin.x + 40, _labelFullTime.frame.origin.y, _labelFullTime.frame.size.width, _labelFullTime.frame.size.height);
        }
    }

    [self initEndOfShowScreen];

    //[self startTimer];
    self.fullscreenModeForPad = NO;
    if (_labelTitle) {
        self.titleLabelRect = _labelTitle.frame;
    }

    returnToViewAfterLogin = NO;
    _buttonFullScreen.enabled = NO;

    [self skinChanged];

    self.swipeGesture = [[UISwipeGestureRecognizer alloc] init];
    [self.swipeGesture addTarget:self action:@selector(swiped:)];
    self.swipeGesture.numberOfTouchesRequired = 2;
    self.swipeGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:self.swipeGesture];

    self.pltvPlayerAdapter = [[PlayerPLTVAdapter alloc] init];
    self.watchAdapter = [[WatchAdapter alloc] init];

    self.labelAutomation.isAccessibilityElement = YES;
    self.labelAutomation.accessibilityLabel = @"Label Automation";
    self.labelAutomation.accessibilityValue = @"";
    
}

-(void) registerForCompanionNotification
{
    [self unregisterForCompanionNotification];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(companionDeviceUnPaired:) name:TVCompanionMixedNotificationDeviceDisconnected object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(companionDevicePaired:) name:TVCompanionMixedNotificationDeviceConnected object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(companionDeviceStop:) name:@"userMakeStopNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chromeCastForceDisconnect:) name:@"chromeCastForceDisconnectNotifcation" object:nil];

    
}

-(void) unregisterForCompanionNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVCompanionMixedNotificationDeviceDisconnected object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVCompanionMixedNotificationDeviceConnected object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"userMakeStopNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"chromeCastForceDisconnectNotifcation" object:nil];


}

- (void)skinChanged {

    [self configurationTaBar];

    if (isPad) {

        _labelLiveProgramTime.textColor = APST.brandTextColor;
        _labelLiveProgramTime.backgroundColor = APST.brandColor;

        [_buttonInfo setImage:[_buttonInfo.imageView.image imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
        [_buttonShare setImage:[_buttonShare.imageView.image imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
        [_buttonLang setImage:[_buttonLang.imageView.image imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
        [_buttonSound setImage:[_buttonSound.imageView.image imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
        [_buttonInfo setImage:[_buttonInfo.imageView.image imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
        [_buttonShare setImage:[_buttonShare.imageView.image imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
        [_buttonLang setImage:[_buttonLang.imageView.image imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
        [_buttonSound setImage:[_buttonSound.imageView.image imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
    }

    if (_buttonCompanion) {

        UIImage *img = [UIImage imageNamed:@"chromecast_icon_not-connected"];
        [_buttonCompanion setImage:[img imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
        [_buttonCompanion setImage:[img imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
    }

    UIImage* tabBarImage = [[UIImage imageNamed:@"tab_highlight_background"] imageTintedWithColor:APST.brandColor];

    [self.buttonTabLeft setBackgroundImage:tabBarImage forState:UIControlStateDisabled];

    [self.buttonTabLeft setTitleColor:[UIColor colorWithWhite:168.0/255.0 alpha:1.0]  forState:UIControlStateDisabled];
    [self.buttonTabLeft setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];

    [self.buttonTabRight setBackgroundImage:tabBarImage forState:UIControlStateDisabled];
    [self.buttonTabRight setTitleColor:[UIColor colorWithWhite:168.0/255.0 alpha:1.0] forState:UIControlStateDisabled];
    [self.buttonTabRight setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];

    self.viewTabBar.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.2].CGColor;
    self.viewTabBar.layer.shadowOffset = CGSizeMake(0, -4);
    self.viewTabBar.layer.shadowOpacity = 0.5;

    if (_labelRightOpen) {
        _labelRightOpen.backgroundColor = APST.brandColor;
        _labelRightOpen.textColor = APST.brandTextColor;
    }

    if (_buttonRightOpen) {
        [self initSelectedBrandButtonUI:_buttonRightOpen];
    }
}

-(void) startCheckLiveProgram {
    [self stopCheckLiveProgram];
    self.liveTVTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkLiveProgram) userInfo:nil repeats:YES];
}

-(void) stopCheckLiveProgram {
    [self.liveTVTimer invalidate];
    self.liveTVTimer = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self setupConstraints];

    if (isPhone)
    {
        if (_buttonCompanion)
        {
            UIImage *img = [UIImage imageNamed:@"chromecast_icon_not-connected"];
            [_buttonCompanion setImage:[img imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
            UIImage *imgConnected = [UIImage imageNamed:@"chromecast_icon_connected"];
            [_buttonCompanion setImage:[imgConnected imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
        }
    }
    if ([[TVMixedCompanionManager sharedInstance] isConnected])
    {
         _buttonCompanion.selected = YES;
        if (self.topBanner.superview == nil)
        {
            [self showTopBannerWithText:[self getCompanionBannerText] duration:DBL_MAX];
        }
    }
    else
    {
        _buttonCompanion.selected = NO;
    }

    if (returnToViewAfterShare)
    {
        returnToViewAfterShare = NO;

        if (isPhone){
            if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {

                _viewPlayerContainer.frame = CGRectMake(playerFullscreenX, playerFullscreenY, playerFullscreenWidth, playerFullscreenHeight);

            } else {
                _viewPlayerContainer.frame = CGRectMake(playerX, playerY, playerWidth, playerHeight);
            }
        }
        return;
    }

    _buttonShare.enabled = [LoginM isSignedIn];

    super.panModeDefault = MFSideMenuPanModeDefault;
    [self menuContainerViewController].panMode = (MFSideMenuPanMode)super.panModeDefault;

    if (_liveTV) [self setLivePlayer];

    _viewPlayerContainer.hidden = YES;

    if (APST.sharePrivacy) {

        if (!LoginM.externalShareWatches) {

            [self requestForGetUserExternalActionsShareWatches:TVUserSocialActionWATCHES completion:^(BOOL result) {

            }];
        }
    }

    if (!isInitialized) {
        isInitialized = YES;

        self.playerVODInfoView = [[PlayerVODInfoView alloc] initWithNib];
        [_viewCenter addSubview:_playerVODInfoView];
        _playerVODInfoView.delegateController = self;

        _playerVODInfoView.hidden = YES;

        [self configurationTaBar];

        [self configurationShare];

        self.playerNPVRInfoView = [[PlayerNPVRInfoView alloc] initWithNib];
        self.playerNPVRInfoView.delegate = self;
        self.playerNPVRInfoView.hidden = YES;

        if (isPhone) {

            self.playerVODDetailsView = [[CustomViewFactory defaultFactory] playerVODDetailsView];
            self.playerVODDetailsView.translatesAutoresizingMaskIntoConstraints = NO;

            [_viewCenter addSubview:_playerVODDetailsView];
            NSDictionary *metrics = @{@"top": @(_playerVODInfoView.frame.size.height),
                                      @"bottom": @(_viewTabBar.frame.size.height - 13)};
            NSDictionary *views = @{@"view": self.playerVODDetailsView};
            [_viewCenter addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views]];
            [_viewCenter addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[view(293)]-(>=bottom)-|" options:0 metrics:metrics views:views]];

            _playerVODDetailsView.delegate = self;
            _playerVODDetailsView.hidden = YES;

            float subviewHeight = _viewCenter.frame.size.height - _playerVODInfoView.frame.size.height;
            if (!_viewTabBar.hidden) subviewHeight -= (_viewTabBar.frame.size.height - 13);

            self.playerLiveDetailsView = [[PlayerLiveDetailsView alloc] initWithNib];
            _playerLiveDetailsView.delegateController = self;
            _playerLiveDetailsView.delegate = self;
            _playerLiveDetailsView.frame = CGRectMake(0, _playerVODInfoView.frame.size.height, _viewCenter.frame.size.width, subviewHeight);
            [_viewCenter addSubview:_playerLiveDetailsView];
            _playerLiveDetailsView.hidden = YES;

            self.playerSocialView =  [[PlayerSocialView alloc] initWithNib];
            _playerSocialView.delegateController = self;
            _playerSocialView.frame = CGRectMake(0, _playerVODInfoView.frame.size.height, _viewCenter.frame.size.width, subviewHeight-13);
            [_playerSocialView initInterface];
            [_viewCenter addSubview:_playerSocialView];
            _playerSocialView.hidden = YES;

            self.playerSeriesDetailsView = [[PlayerSeriesDetailsView alloc] initWithNib];
            _playerSeriesDetailsView.frame = CGRectMake(0, _playerVODInfoView.frame.size.height - 45, _viewCenter.frame.size.width, subviewHeight + 45);
            _playerSeriesDetailsView.delegate = self;
            [_viewCenter addSubview:_playerSeriesDetailsView];
            _playerSeriesDetailsView.hidden = YES;

            self.playerSeriesEpisodesView = [[PlayerSeriesEpisodesView alloc] initWithNib];
            _playerSeriesEpisodesView.frame = CGRectMake(0, _playerVODInfoView.frame.size.height, _viewCenter.frame.size.width, subviewHeight);
            _playerSeriesEpisodesView.delegate = self;
            [_viewCenter addSubview:_playerSeriesEpisodesView];
            _playerSeriesEpisodesView.hidden = YES;


            self.playerNPVRInfoView.frame = CGRectMake(0, _playerVODInfoView.frame.size.height, _viewCenter.frame.size.width, subviewHeight);
            [_viewCenter addSubview:self.playerNPVRInfoView];

        } else {

            viewContentY = [super.viewContent superview].frame.size.height - super.viewContent.frame.size.height;
            viewContentYOpen = viewContentY - _detailsContainerView.frame.size.height;

            viewDetailsContentY = _detailsContainerView.frame.origin.y;

            self.playerVODDetailsView = [[CustomViewFactory defaultFactory] playerVODDetailsView];//[[PlayerVODDetailsView alloc] initWithNib];
            _playerVODDetailsView.frame = CGRectMake(0, 0, _playerVODDetailsView.frame.size.width, _playerVODDetailsView.frame.size.height);
            _playerVODDetailsView.delegate = self;
            [_detailsContainerView addSubview:_playerVODDetailsView];
            _playerVODDetailsView.hidden = YES;


            self.playerLiveDetailsView = [[PlayerLiveDetailsView alloc] initWithNib];
            _playerLiveDetailsView.delegateController = self;
            _playerLiveDetailsView.delegate = self;
            _playerLiveDetailsView.frame = CGRectMake(0, 0, _playerLiveDetailsView.frame.size.width, _playerLiveDetailsView.frame.size.height);
            [_detailsContainerView addSubview:_playerLiveDetailsView];
            _playerLiveDetailsView.hidden = YES;

            self.playerSocialView =  [[PlayerSocialView alloc] initWithNib];
            _playerSocialView.delegateController = self;
            _playerSocialView.frame = CGRectMake(0, 0, _playerSocialView.frame.size.width, _playerSocialView.frame.size.height);
            [_playerSocialView initInterface];
            [_detailsContainerView addSubview:_playerSocialView];
            _playerSocialView.hidden = YES;


            self.playerNPVRInfoView.frame = CGRectMake(0, 0, self.playerNPVRInfoView.frame.size.width, self.playerNPVRInfoView.frame.size.height);
            [_detailsContainerView addSubview:self.playerNPVRInfoView];
        }

        [self updateRightButton];

        [self createChannelsController];

        playerFullscreenWidth = self.view.frame.size.height;
        playerFullscreenHeight = super.viewContent.frame.size.width;
        playerFullscreenX = super.viewContent.frame.origin.x;
        playerFullscreenY = super.viewContent.frame.origin.y - 20;

        playerWidth = _viewPlayerContainer.frame.size.width;
        playerHeight = _viewPlayerContainer.frame.size.height;
        playerX = _viewPlayerContainer.frame.origin.x;
        playerY = _viewPlayerContainer.frame.origin.y;

        [self configurationPlayer];

        self.playerPinView = [[PlayerPinView alloc] initWithNib];
        _playerPinView.delegateController = self;

#ifdef PLAYER_DX
        [PlayerCDA updateSecuredSiteGUID:self];
#endif
    }

    if (returnToViewAfterLogin) {

        returnToViewAfterLogin = NO;

        if (_buttonTabRight && _buttonTabRight.selected){
            [self buttonTabBarPressed:_buttonTabRight];
        }

        if ([LoginM isSignedIn]){
            _playerVODInfoView.viewLogin.alpha = 0.0;
        }

        if (_playerMediaItem) {

            if ([LoginM isSignedIn] || ![_playerMediaItem isLive]) {
                [self selectMediaItem:_playerMediaItem];
            }
        }
        return;
    }
    if (_initialMediaItem) [self selectMediaItem:_initialMediaItem];
}

- (void)viewDidAppear:(BOOL)animated {

    self.playerIsVisible = YES;
    self.view.userInteractionEnabled = YES;
    _buttonFullScreen.enabled = NO;
}

- (void)viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:animated];

    [self.liveTVTimer invalidate];
    self.liveTVTimer = nil;

    if (!(returnToViewAfterShare || returnToViewAfterLogin)) {

        [self menuContainerViewController].rightMenuViewController = nil;

        [AppDelegate sharedAppDelegate].blockOrientationRotation = YES;

        self.playerIsVisible = NO;

        [self stopPlayer];
    }
    [self.companionModeOverlay animateOutWithCompletion:nil];
    [self.playerDTGAdapter removeDownloadItemAllocation];
}

#pragma mark - End of Show Screen -

- (void)initEndOfShowScreen {
    [self hideEndOfShowScreen];
}

- (void)hideEndOfShowScreen {

    [self adjustSliderAccordingToCurrentState];
    [_viewEndOfShow hide];
    [[_viewPlayerContainer viewWithTag:kTagEndOfShowOverlayView] removeFromSuperview];

}

- (void)showEndOfShowScreen
{
    self.timeSlider.userInteractionEnabled = NO;

    if (self.companionModeOverlay.isPresnted) {
        return;
    }

    BOOL mediaTrailer = NO;
#ifdef PLAYER
    if (_mediaToPlayInfo && [_mediaToPlayInfo.fileTypeFormatKey isEqualToString:APST.TVCMediaFormatTrailer]) {
        mediaTrailer = YES;
    }
#endif
    if (!_viewEndOfShow.delegate)
        _viewEndOfShow.delegate = self;

    //create half transparent black overlay for endOfShow view
    UIView *viewOverlay = [UIView new];
    viewOverlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.65f];
    viewOverlay.tag = kTagEndOfShowOverlayView;
    viewOverlay.translatesAutoresizingMaskIntoConstraints = NO;
    [_viewPlayerContainer insertSubview:viewOverlay belowSubview:_viewTopControls];
    [_viewPlayerContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[viewOverlay]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewOverlay)]];
    [_viewPlayerContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[viewOverlay]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewOverlay)]];

    [_viewEndOfShow showForMediaItem:_playerMediaItem hasMediaTrailer:mediaTrailer hasNextEpisode:[self hasNextEpisode]];
    
    // "PSC : Vodafone OnO :SYN-4175 chromecast - user is not able to play the assets when return from backgroung /
    [_viewEndOfShow.superview bringSubviewToFront:_viewEndOfShow];
    [_viewBottomControls.superview bringSubviewToFront:_viewBottomControls];
    //resume all downloads if not playing local file
    [self.playerDTGAdapter resumeAllDownloads];
}

- (BOOL)hasNextEpisode {
    BOOL retValue = NO;
    if (_playerMediaItem.isEpisode || _playerMediaItem.isSeries) {
        NSArray *episodes = [self episodesArray];
        NSUInteger currentEpisodeIndex = [episodes indexOfObject:_playerMediaItem];

        if (currentEpisodeIndex != NSNotFound) {
            if (currentEpisodeIndex > 0) {
                retValue = TRUE;
            }
        }
    }
    return retValue;
}

- (TVMediaItem*)getNextEpisode {
    TVMediaItem* nextEpisode = nil;

    if (_playerMediaItem.isEpisode || _playerMediaItem.isSeries) {
        NSArray *episodes = [self episodesArray];
        NSUInteger currentEpisodeIndex = [episodes indexOfObject:_playerMediaItem];

        if (currentEpisodeIndex != NSNotFound) {
            NSInteger newEpisodeIndex = currentEpisodeIndex - 1;

            if (newEpisodeIndex >= 0) {
                nextEpisode = [episodes objectAtIndex:newEpisodeIndex];
            } else {
                _currentSeason++;
                nextEpisode = [self getNextEpisode];
            }
        } else {
            nextEpisode = _currentSeason ? [episodes firstObject] : nil;
        }
    }
    return nextEpisode;
}

#pragma mark - End of Show delegates -

- (void)buttonBackToLivePressed {
    [self hideEndOfShowScreen];

#ifdef PLAYER


    if (self.npvrPlayerAdapter)
    {
        [self clearOldAttributes];
        [self selectMediaItem:self.videoPlayerControl.mediaToPlayInfo.mediaItem];
    }
    else
    {
        [self clearOldAttributes];
        [self playMediaType:[self bestFileformat] startTime:.0f];
    }


#endif
}

- (void)buttonStartOverPressed {
    [self hideEndOfShowScreen];

    // start over and seek to selected time
    if (!self.npvrPlayerAdapter)
    {
        [self clearOldAttributes];
        self.programToPlay = self.playedProgram;

    }
#ifdef PLAYER
    [self playMediaType:[self bestFileformat] startTime:.0f];
#endif
}

- (void) buttonNextEpisodePressed {
    [self hideEndOfShowScreen];
    [NavM setPhonePortrait];
    [self selectMediaItem:[self getNextEpisode]];
}

- (void)updateButtonBrandUI:(UIButton*)btn sameForAllStates:(BOOL) sameForAllStates {
    if (sameForAllStates)
        [self initOneColorBrandButtonUI:btn];
    else
        [self initSelectedBrandButtonUI:btn];
}

- (void)buttonEndOfShowReplayPressed {
    [self hideEndOfShowScreen];

    _timeSlider.value = 0;
    _labelCurrentTime.text = [NSString stringWithFormat:@"%02i:%02i", 0, 0];

#ifdef PLAYER
    [self playMediaType:_mediaToPlayInfo.fileTypeFormatKey startTime:0];
#endif
}

- (void)buttonEndOfShowWatchFullMoviePressed {

    [self hideEndOfShowScreen];
    _mediaStarted = NO;
    [self buttonPlayPausePressed:_buttonPlayPause];
}

-(void)buttonEndOfShowRemoveDownloadedItem
{
    [self showHUDBlockingUI:YES];
    [[DTGManager sharedInstance] deleteItem:self.playerDTGAdapter.downloadItem completion:^(BOOL success) {
        [self hideHUD];
        [self buttonBackPressed:self.buttonBack];
    }];
}

#pragma mark - LiveTV -
-(void) updateLiveProgramWithCompletion:(void(^)(bool changed)) completion
{
    [[APSEPGManager sharedEPGManager] programForChannelId:_playerMediaItem.epgChannelID date:[NSDate date] privateContext:self.privateContext starterBlock:nil failedBlock:nil completionBlock:^(APSTVProgram *program) {

        if ((!_playedProgram || ![_playedProgram.epgId isEqualToString:program.epgId]) && program)
        {
            [self setPlayedProgram:program];

            if (completion)
            {
                completion(YES);
            }
        }
        else
        {
            if (completion)
            {
                completion(NO);
            }
        }
    }];

}

- (void)checkLiveProgram
{

    if (self.playerMode == playerModeLIVE)
    {
        [self updateLiveProgramWithCompletion:^(bool changed) {
            if (changed)
            {
                [self checkLiveParentalForProgram:self.playedProgram completion:^(bool hasParentalRule) {
                    if (hasParentalRule)
                    {
#ifdef PLAYER
                        [self.videoPlayerControl Pause];
                        [self playerPinViewShowWithcompletion:^(BOOL isUnlocked, NSString *enteredCode) {
                            if (isUnlocked) {
                                self.blockedUI = NO;
                                self.groupMediaRules = [NSDictionary dictionary];
                                _playerPinView.pinRuleID = 0;
                                [self.videoPlayerControl Play];
                            }

                            [self playerPinViewClose];
                            [self changePinViewHidden:YES];
                        }];
#endif
                    }
                }];
            }
        }];

    }

}

-(void) updatePlayedProgramUI:(id) playedProgram
{
    if (self.playerMode == playerModeLIVE)
    {
        [self startCheckLiveProgram];
    }

    TVNPVRRecordItem * npvrAsset = nil;
    APSTVProgram * programAsset = nil;
    if ([playedProgram isKindOfClass:[TVNPVRRecordItem class]])
    {
        npvrAsset = playedProgram;
    }
    else
    {
        programAsset = playedProgram;
        [self.playerLiveDetailsView scrollToProgram:programAsset animated:NO];
    }

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];

    _labelCurrentTime.text = [df stringFromDate:programAsset?programAsset.startDateTime:npvrAsset.startDate];
    _labelFullTime.text = [df stringFromDate:programAsset?programAsset.endDateTime:npvrAsset.endDate];

    _timeSlider.alpha = 1.0;

    if (isPad) {

        NSString *title = npvrAsset?nil:_playerMediaItem.name;
        NSString *subTitle = [NSString stringWithFormat:@"     %@", programAsset?(programAsset.name?programAsset.name:@""):(npvrAsset.name?npvrAsset.name:@"")];


        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] init];
        title?[attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:title]]:nil;
        subTitle?[attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:subTitle]]:nil;
        //WithString:[NSString stringWithFormat:@"%@%@", title, subTitle]];

        [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, title.length)];
        [attrStr addAttribute:NSFontAttributeName value:Font_Bold(_labelTitle.font.pointSize) range:NSMakeRange(0, title.length)];

        [attrStr addAttribute:NSForegroundColorAttributeName value:CS(@"999999") range:NSMakeRange(title.length, subTitle.length)];
        [attrStr addAttribute:NSFontAttributeName value:Font_Reg(_labelTitle.font.pointSize - 1) range:NSMakeRange(title.length, subTitle.length)];

        _labelTitle.attributedText = attrStr;

        _labelTitle.frame = CGRectMake(_labelTitle.frame.origin.x, _labelTitle.frame.origin.y, _buttonInfo.frame.origin.x - _labelTitle.frame.origin.x, _labelTitle.frame.size.height);

        CGFloat titleWidth = [_labelTitle.attributedText size].width;

        UILabel *lab  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 22)];
        lab.font = _labelTitle.font;
        lab.text = title;
        CGSize textSize = [[lab text] sizeWithAttributes:@{NSFontAttributeName:[lab font]}];
        CGFloat strikeWidth = textSize.width;

        self.recoredIconImage.image = [UIImage imageNamed:@"record_singleItem_icon"];
        CGRect frame = self.recoredIconImage.frame;
        frame.origin.x = self.labelTitle.frame.origin.x + strikeWidth+3;
        self.recoredIconImage.frame = frame;

        CGFloat spaceSize = 8.0;

        _labelLiveProgramTime.hidden = NO;

        if (npvrAsset!=nil)
        {
            _labelLiveProgramTime.hidden = YES;
        }
        if (_labelCurrentTime.text && _labelFullTime.text )
        {
            _labelLiveProgramTime.text = [NSString stringWithFormat:@"%@-%@", _labelCurrentTime.text, _labelFullTime.text];
        } else {
            _labelLiveProgramTime.text = @"";
        }
        _labelLiveProgramTime.frame = CGRectMake(_labelLiveProgramTime.frame.origin.x, _labelLiveProgramTime.frame.origin.y, [_labelLiveProgramTime.text sizeWithAttributes:@{NSFontAttributeName:_labelLiveProgramTime.font}].width + (spaceSize * 2.0), _labelLiveProgramTime.frame.size.height);


        if (_labelTitle.frame.origin.x + titleWidth + _labelLiveProgramTime.frame.size.width + spaceSize > _buttonInfo.frame.origin.x) {

            titleWidth = _buttonInfo.frame.origin.x - (_labelTitle.frame.origin.x + _labelLiveProgramTime.frame.size.width + spaceSize);

        }

        _labelTitle.frame = CGRectMake(_labelTitle.frame.origin.x, _labelTitle.frame.origin.y, titleWidth, _labelTitle.frame.size.height);

        _labelLiveProgramTime.frame = CGRectMake((int)(_labelTitle.frame.origin.x + _labelTitle.frame.size.width + spaceSize), _labelLiveProgramTime.frame.origin.y, (int)(_labelLiveProgramTime.frame.size.width), _labelLiveProgramTime.frame.size.height);

    } else {

        if(self.playerMode == playerModeLIVE)
        {
            if (programAsset.name) {
                _labelTitleFullscreen.text = [NSString stringWithFormat:@"%@ - %@", _playerMediaItem.name, programAsset.name];
            } else {
                _labelTitleFullscreen.text = _playerMediaItem.name;
            }

            if (npvrAsset.name)
            {
                _labelTitleFullscreen.text = [NSString stringWithFormat:@"%@ - %@", _playerMediaItem.name, npvrAsset.name];
            }
            else
            {
                _labelTitleFullscreen.text = _playerMediaItem.name;
            }
        }
    }


    if (self.playerMode == playerModeLIVE)
    {
        self.buttonPlayPause.UIUnitID = NU(UIUnit_live_playPause_button);
        [self.buttonPlayPause activateAppropriateRule];
    }
}

- (void)setPlayedProgram:(APSTVProgram *)liveProgram  {

    if (_playedProgram != liveProgram)
    {
        _playedProgram = liveProgram;

        if (self.playerMode == playerModeLIVE)
        {
            [self updatePlayedProgramUI:liveProgram];


        }

    }

    //
}

#pragma mark - Buttons -

-(void) authenticationCompleted {
    returnToViewAfterLogin = YES;
}

#pragma mark -

- (void)stopPlayer {

#ifdef PLAYER

    //[_videoPlayerControl Pause];
    [_videoPlayerControl Stop];
    [_videoPlayerControl cleanPlayer];
    [self stopTimer];
#endif

}

- (void)dealloc {


    [self unregisterForCompanionNotification];
    [self unRegisterForNotification];

    if ([timerForHidingControls isValid]) {
        [timerForHidingControls invalidate];
    }
    timerForHidingControls = nil;

    _socialManager = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    /*
     #ifdef PLAYER

     [_videoPlayerControl Stop];
     [_videoPlayerControl cleanPlayer];

     _videoPlayerControl = nil;
     [self stopTimer];

     #endif
     */

    [_arrPlayerMediasStack removeAllObjects];
}

#pragma mark - Timer -

- (BOOL)tapOnPanels:(UITapGestureRecognizer *)tapRecognizer {
    CGPoint pointInTopPanel = [tapRecognizer locationInView: self.viewTopControls];
    CGPoint pointInBottomPanel = [tapRecognizer locationInView: self.viewBottomControls];
    BOOL pointInPanels = (CGRectContainsPoint(self.viewTopControls.frame,
                                              pointInTopPanel) ||
                          CGRectContainsPoint(self.viewTopControls.frame,
                                              pointInBottomPanel));
    return pointInPanels;
}

- (IBAction)screenTaped:(id)sender {

    BOOL panelsHidden = (self.viewTopControls.hidden && self.viewBottomControls.hidden);

    if (panelsHidden) {
        [self restartTimer];
        if (self.fullscreenModeForPad) {
            self.fullscreenModeForPad = NO;
            [self.buttonFullScreen setImage:[UIImage imageNamed:@"player_maximize_icon"]
                                   forState:UIControlStateNormal];
            self.viewTopControls.hidden = NO;
            self.viewBottomControls.hidden = NO;
            self.viewTopControls.alpha = 1.f;
            self.viewBottomControls.alpha = 1.f;
            self.viewRightOpen.hidden = (NO || (isPhone &&(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? NO : YES)));
            self.viewRightOpen.alpha = ((isPad ||(isPhone &&(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)))) ? 1.0 : 0.0);
            self.viewRightOpen.hidden = [APP_SET isOffline]?YES:self.viewRightOpen.hidden;

        }
    }else {
#ifdef PLAYER
        BOOL pointInPanels = [self tapOnPanels:sender];
        if (self.videoPlayerControl.playerStatus == TVPPlaybackStateIsPlaying &&
            !_detailsContainerOpen &&
            !_popUpOpen &&
            !pointInPanels && !self.isCompanionDiscoveryOpned) {
            _buttonFullScreen.enabled = YES;

            [UIView animateWithDuration:.5f animations:^(void){
                self.viewTopControls.alpha = 0.f;
                self.viewBottomControls.alpha = 0.f;
                self.viewRightOpen.alpha = 0.f;

            } completion:^(BOOL finished) {
                self.viewTopControls.hidden = YES;
                self.viewBottomControls.hidden = YES;
                self.viewRightOpen.hidden = YES;

            }];
        }
#endif
    }
}

- (BOOL)playerIsPlaying {

#ifdef PLAYER
    return (self.videoPlayerControl.playerStatus == TVPPlaybackStateIsPlaying);
#endif

    return NO;

}

- (void)startTimer {
    [self stopTimer];
#ifdef PLAYER
    if (self.videoPlayerControl.playerStatus == TVPPlaybackStateIsPlaying &&
        !_detailsContainerOpen &&
        !_popUpOpen) {
        _buttonFullScreen.enabled = YES;
        timerForHidingControls = [NSTimer scheduledTimerWithTimeInterval:6.0f
                                                                  target:self
                                                                selector:@selector(hideControlPanels)
                                                                userInfo:nil
                                                                 repeats:NO];
    }
#else
    if (!_detailsContainerOpen &&
        !_popUpOpen) {
        _buttonFullScreen.enabled = YES;
        timerForHidingControls = [NSTimer scheduledTimerWithTimeInterval:6.0f
                                                                  target:self
                                                                selector:@selector(hideControlPanels)
                                                                userInfo:nil
                                                                 repeats:NO];
    }
#endif
}

- (void)restartTimer {
    self.viewTopControls.hidden = NO;
    self.viewBottomControls.hidden = NO;
    self.viewRightOpen.hidden = (NO || (isPhone&&(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? NO : YES)));
    self.viewRightOpen.hidden = [APP_SET isOffline]?YES:self.viewRightOpen.hidden;

    self.viewTopControls.alpha = 1.f;
    self.viewBottomControls.alpha = 1.f;
    self.viewRightOpen.alpha = ((isPad ||(isPhone&&(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)))) ? 1.0 : 0.0);

    [self startTimer];
}

- (void)stopTimer {
    self.viewTopControls.hidden = NO;
    self.viewBottomControls.hidden = NO;
    self.viewRightOpen.hidden = (NO || (isPhone&&(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? NO : YES)));
    self.viewRightOpen.hidden = [APP_SET isOffline]?YES:self.viewRightOpen.hidden;

    self.viewTopControls.alpha = 1.f;
    self.viewBottomControls.alpha = 1.f;
    self.viewRightOpen.alpha = ((isPad ||(isPhone&&(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)))) ? 1.0 : 0.0);

    if (timerForHidingControls && [timerForHidingControls isValid]) {
        [timerForHidingControls invalidate];
        timerForHidingControls = nil;
    }
}

- (void)hideControlPanels {
    [UIView animateWithDuration:.5f animations:^(void){
        self.viewTopControls.alpha = 0.f;
        self.viewBottomControls.alpha = 0.f;
        self.viewBottomControls.alpha = 0.0;
        self.viewRightOpen.alpha = 0.0;
    } completion:^(BOOL finished) {
        self.viewTopControls.hidden = YES;
        self.viewBottomControls.hidden = YES;
        self.viewRightOpen.hidden = YES;
    }];
}

#pragma mark - Share -

- (void)configurationShare {

    self.shareContentView = [[ShareContentView alloc] initWithNib];
    _shareContentView.delegate = self;

    if (isPhone) {

        self.shareView = [[ShareView alloc] initWithNib];
        [_shareView.viewContent addSubview:_shareContentView];
        _shareView.delegate = self;

    } else {

        [self initializeSocialFeedPopUpView];
    }
}

- (IBAction)buttonSharePressed:(UIButton *)button {
    [self startTimer];
    [_shareContentView setupWithMedia:_playerMediaItem];

    if (isPhone) {
        _shareView.frame = self.view.bounds;
        [self.view addSubview:_shareView];
        _popUpOpen = YES;

        [self menuContainerViewController].panMode = MFSideMenuPanModeNone;

    } else {

        [self showSocialFeedPopUp:button];
    }
}

- (void)dismissSocialShareView {

    if (isPhone) {
        [_shareView removeFromSuperview];

        [self menuContainerViewController].panMode = (MFSideMenuPanMode)super.panModeDefault;

    } else {
        [self.sharePopUpView dismissPopUp];
        self.btnOfPopUpToShow.selected = !self.btnOfPopUpToShow.selected;
    }
    _popUpOpen = NO;
    [self startTimer];
}

- (void)showCommentView:(UIView *)viewComment {
    //[self stopFullScreenModeTimer];
    [self stopTimer];

    [UIView animateWithDuration:kViewAnimationDuration animations:^{
        [ViewHelper addBlockViewWithDurationAnimation:kViewAnimationDuration delegate:viewComment controller:self];
    } completion:^(BOOL finished) {
        //UIApplication *appDelegate = [UIApplication sharedApplication];
        //UIView *rootView = [[[[appDelegate windows] objectAtIndex:0] subviews] lastObject];
        //[viewComment setFrame:CGRectMake((rootView.width - viewComment.width) / 2, (rootView.height - viewComment.height) / 2,viewComment.width,viewComment.height)];

        if (isPhone) {
            [NavM setPhonePortrait];

            viewComment.frame = super.viewContent.frame;
            [self.view addSubview:viewComment];
        } else {
            viewComment.center = CGPointMake(self.view.width / 2, self.view.height / 2);
            [self.view addSubview:viewComment];
        }
    }];
}

- (void)removeCommentView {
    //[self shareAction:nil];
    //[self resetFullScreenModeTimer];
    [ViewHelper removeBlockViewWithDurationAnimation:kViewAnimationDuration];
    [self startTimer];
}

- (void)presentViewController:(UIViewController *)controller {

    if (isPhone) {
        //    [NavM setPhonePortrait];
    }

    if ([controller isKindOfClass:[MFMailComposeViewController class]]) {
        returnToViewAfterShare = YES;
    }

    [self presentViewController:controller animated:YES completion:nil];
}

- (void)dismissViewController {

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) loginWithFB{
    [self intermediateLogin:@"fbLogin"];
}

#pragma mark - ShareViewDelegate -

- (void)closeView {
    [self dismissSocialShareView];
}

#pragma mark - ShareContentViewDelegate -

- (void)shareAction:(NSInteger)socialAction {
    [self startTimer];

    [_socialManager shareAction:socialAction];
}

#pragma mark - Touches -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    if (isPad) {

        UITouch *touch = [touches anyObject];
        CGPoint touchPoint = [touch locationInView:self.view];
        if ([self.view.layer containsPoint:touchPoint]) {

            if(!CGRectContainsPoint(self.sharePopUpView.frame, touchPoint))
                [self.sharePopUpView dismissPopUp];

            if ([self.view.subviews.lastObject isKindOfClass:[CommentView class]]){
                [self startTimer];
            }
        }
    }
}

- (void)initializeSocialFeedPopUpView {
    self.sharePopUpView =  [[SharePopUpView alloc] initWithArrowDirection:kSharePopUpViewArrowDirectionDown contentView:_shareContentView];
    self.sharePopUpView.frame = CGRectMake(popUpShareViewXOrigin, popUpShareViewYOrigin, CGRectGetWidth(self.sharePopUpView.frame), CGRectGetHeight(self.sharePopUpView.frame));
    if (!APST.sharePrivacy) {
        self.sharePopUpView.frame = CGRectMake(self.sharePopUpView.frame.origin.x, self.sharePopUpView.frame.origin.y + _shareContentView.viewPrivately.frame.size.height, self.sharePopUpView.frame.size.width, self.sharePopUpView.frame.size.height - _shareContentView.viewPrivately.frame.size.height);
        self.sharePopUpView.constraintContentHeight.constant -= _shareContentView.viewPrivately.height;
    }
    self.sharePopUpView.popUpRectView = CGRectMake(self.sharePopUpView.frame.origin.x, self.sharePopUpView.frame.origin.y, CGRectGetWidth(self.sharePopUpView.frame), CGRectGetHeight(self.sharePopUpView.frame));
    self.sharePopUpView.popUpSuperView = self.view;
    self.sharePopUpView.isDismissAutomatically = YES;
    self.sharePopUpView.popUpAnimationType = TVNPopUpAnimationTypeDownToUp;

    //self.sharePopUpView.socialBuzzView = self;
}

- (void)showSocialFeedPopUp:(UIButton *)button {

    if (self.sharePopUpView.superview) {
        ((UIButton*)button).selected = NO;
        [self.sharePopUpView dismissPopUp];
        _popUpOpen = NO;
    }
    else {
        if (![self.view.subviews containsObject:self.currentPopUp]) {//there is no popup in the screen
            ((UIButton*)button).selected = YES;
            self.currentPopUp = self.sharePopUpView;
            self.currentPopUp.popUpDelegate = self;
            self.currentbtnOfPopUp = ((UIButton*)button);

            [self.sharePopUpView showPopUp];

        } else {//there is a popup in the screen, first remove than add other
            [self.currentPopUp dismissPopUp];
            self.popUpToShow = self.sharePopUpView;
            self.btnOfPopUpToShow = ((UIButton*)button);
        }
        _popUpOpen = YES;
    }
}

#pragma mark - TVNBasePopUpDelegate -

- (void)didAddPopUp:(BasePopUp *)popUp {
    //[self bringSubviewToFront:self.middleNavigationBar];
}

- (void)didEndAddPopUp:(BasePopUp*)popUp {
}

- (void)didDismissPopUp:(BasePopUp*)popUp autoDismiss:(BOOL)autoDismiss{
    self.currentbtnOfPopUp.selected = NO;
    self.currentPopUp.popUpDelegate = nil;
    self.currentPopUp = nil;
    if (self.popUpToShow) { //threre is a popup to show
        [self.popUpToShow showPopUp];
        self.btnOfPopUpToShow.selected = YES;

        self.currentPopUp = self.popUpToShow;
        self.currentbtnOfPopUp = self.btnOfPopUpToShow;
        self.currentPopUp.popUpDelegate = self;
        self.popUpToShow = nil;
        self.btnOfPopUpToShow = nil;
    }
    //    if (autoDismiss) {
    _popUpOpen = NO;
    //    }

    if (self.fullscreenModeForPad){
        return;
    }

    if (!autoDismiss && ([self.view.subviews containsObject:_sharePopUpView] || [self.view.subviews containsObject:_langPopUpView] || [self.view.subviews containsObject:_soundSiderView])) {
        return;
    }

#ifdef PLAYER
    if ([popUp isKindOfClass:[PlayerSoundSlider class]] && (self.videoPlayerControl.playerStatus == TVPPlaybackStateIsPlaying) && !_detailsContainerOpen && !super.myZoneViewController) {

        [self hideControlPanels];
    }

    if ([popUp isKindOfClass:[SharePopUpView class]] && (self.videoPlayerControl.playerStatus == TVPPlaybackStateIsPlaying)) {
        if (autoDismiss){
            [self hideControlPanels];
        } else {
            [self startTimer];
        }
    }

    if (([popUp isKindOfClass:[PlayerLangViewPad class]]) && (self.videoPlayerControl.playerStatus == TVPPlaybackStateIsPlaying) && !super.myZoneViewController){
        if (autoDismiss){
            [self hideControlPanels];
        } else {
            [self startTimer];
        }
    }
#else
    if ([popUp isKindOfClass:[PlayerSoundSlider class]] && !_detailsContainerOpen && !super.myZoneViewController)  {

        [self hideControlPanels];
    }

    if ([popUp isKindOfClass:[SharePopUpView class]]){
        if (autoDismiss){
            [self hideControlPanels];
        } else {
            [self startTimer];
        }
    }

    if ([popUp isKindOfClass:[PlayerLangViewPad class]] && !super.myZoneViewController){
        if (autoDismiss){
            [self hideControlPanels];
        } else {
            [self startTimer];
        }
    }
#endif
}

#pragma mark -

- (void)configurationTaBar {

    if (_buttonTabLeft && _buttonTabRight)
        return;

    _buttonTabLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    _buttonTabLeft.translatesAutoresizingMaskIntoConstraints = NO;
    _buttonTabLeft.titleLabel.font = Font_Bold(14);
    _buttonTabRight.tag = 0;
    [_buttonTabLeft addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
    [Utils setButtonFont:_buttonTabLeft bold:YES];
    [_viewTabBar addSubview:_buttonTabLeft];


    _buttonTabRight = [UIButton buttonWithType:UIButtonTypeCustom];
    _buttonTabRight.translatesAutoresizingMaskIntoConstraints = NO;
    _buttonTabRight.titleLabel.font = Font_Bold(14);
    _buttonTabRight.tag = 1;
    [_buttonTabRight addTarget:self action:@selector(buttonTabBarPressed:) forControlEvents:UIControlEventTouchUpInside];
    [Utils setButtonFont:_buttonTabRight bold:YES];
    [_viewTabBar addSubview:_buttonTabRight];

    _buttonTabLeft.translatesAutoresizingMaskIntoConstraints = NO;
    _buttonTabRight.translatesAutoresizingMaskIntoConstraints = NO;

    [_viewTabBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_buttonTabLeft]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_buttonTabLeft)]];
    [_viewTabBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_buttonTabRight]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_buttonTabRight)]];

    if (isPad) {
        [_viewTabBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_buttonTabLeft]-0-[_buttonTabRight]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_buttonTabLeft, _buttonTabRight)]];
    }
    else {
        [_viewTabBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_buttonTabLeft]-0-[_buttonTabRight]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_buttonTabLeft, _buttonTabRight)]];
    }
    [_viewTabBar addConstraint: [NSLayoutConstraint constraintWithItem:_buttonTabLeft attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_viewTabBar attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];

    [_buttonTabLeft setContentEdgeInsets:UIEdgeInsetsMake(0, 16, 0, 16)];
    [_buttonTabRight setContentEdgeInsets:UIEdgeInsetsMake(0, 16, 0, 16)];


    _viewTabBar.hidden = !APST.socialFeed || [APP_SET isOffline];
    _viewTabBar.alpha = 0.0;

    if (_viewTabBar.hidden) {
        if (isPad) {
            _detailsContainerView.frame = CGRectMake(_detailsContainerView.frame.origin.x, _detailsContainerView.frame.origin.y + _viewTabBar.frame.size.height,  _detailsContainerView.frame.size.width,  _detailsContainerView.frame.size.height - _viewTabBar.frame.size.height);
        }
    }

    if (isPhone) {
        _viewTabBar.backgroundColor = CS(@"4D4D4D");
    }
}

- (void)configurationPlayer {

    [Utils setLabelFont:_labelCurrentTime bold:YES];
    [Utils setLabelFont:_labelFullTime bold:YES];

    [self setupSlider];

#ifdef PLAYER
    self.videoPlayerControl = [[TVCPlayerFactory playerFactory] GetPlayerForMediaItem:nil];

    [self.videoPlayerControl setDelegate:self];

    self.videoPlayerControl.view.backgroundColor = [UIColor blackColor];
    self.videoPlayerControl.view.userInteractionEnabled = NO;

    self.videoPlayerControl.view.frame = _viewPlayerContainer.bounds;
    self.videoPlayerControl.view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    [_viewPlayerContainer insertSubview:self.videoPlayerControl.view atIndex:0];

#endif
}

- (void)updateRightButton {

    NSString *rightLabelText = LS(@"player_related_related_movies");

    if (_playerMediaItem && _playerMediaItem.isLive) {

        rightLabelText = LS(@"channels");

    }

    if (_labelRightOpen) {

        [Utils setLabelFont:_labelRightOpen bold:YES];
        _labelRightOpen.text = rightLabelText;
        _viewRightOpen.frame = CGRectMake(_viewRightOpen.frame.origin.x, _viewRightOpen.frame.origin.y, [_labelRightOpen.text sizeWithAttributes:@{NSFontAttributeName:_labelRightOpen.font}].width + _viewRightOpen.frame.size.height, _viewRightOpen.frame.size.height);
        _viewRightOpen.transform = CGAffineTransformMakeRotation(-M_PI / 2);
        _viewRightOpen.hidden =  YES;

        if (isPhone) {
            [_labelRightOpen superview].layer.cornerRadius = 4.0;
            [_labelRightOpen superview].clipsToBounds = YES;

            _viewRightOpen.center = CGPointMake([_viewRightOpen superview].frame.size.width - (_viewRightOpen.frame.size.width / 2.0), ([_viewRightOpen superview].frame.size.height / 2.0));

            _viewRightOpen.alpha = (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? 1.0 : 0.0);

        } else {

            _viewRightOpen.center = CGPointMake(self.view.frame.size.width - (_viewRightOpen.frame.size.width / 2.0), (self.view.frame.size.height / 2.0) - 20.0);
        }
    }

    if (_buttonRightOpen) {

        [_buttonRightOpen setTitle:rightLabelText forState:UIControlStateNormal];
        [self initSelectedBrandButtonUI:_buttonRightOpen];
        _buttonRightOpen.hidden = YES;
    }
}

- (IBAction)tapRightByButton:(UITapGestureRecognizer *)recognizer {

    [self showRightMenu];
}

- (IBAction)buttonRightOpenPressed:(UIButton *)button {

    [AppDelegate sharedAppDelegate].blockOrientationRotation = YES;
    [self showRightMenu];
}

- (void)showRightMenu {

    UIViewController *controller = [self menuContainerViewController].rightMenuViewController;
    if (controller && [controller isKindOfClass:[PlayerLiveChannelsViewController class]]) {
        [((PlayerLiveChannelsViewController *)controller) reloadData];
    }

    [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
}

- (void)createRelatedController {

    PlayerRelatedViewController *relatedController = [[ViewControllersFactory defaultFactory] playerRelatedViewController];
    relatedController.delegate = self;
    [self menuContainerViewController].rightMenuViewController = relatedController;
    [[self menuContainerViewController] setRightMenuWidth:playerRightMenuWidth];
    [self stopTimer];
}

- (void)createChannelsController {

    PlayerLiveChannelsViewController *relatedController = [[ViewControllersFactory defaultFactory] playerLiveChannelsViewController];
    relatedController.delegate = self;
    [self menuContainerViewController].rightMenuViewController = relatedController;
    [[self menuContainerViewController] setRightMenuWidth:playerRightMenuWidth];
}

- (void)createEpisodesController {

    PlayerEpisodesViewController *episodesController = [[ViewControllersFactory defaultFactory] playerEpisodesViewController];
    episodesController.delegate = self;
    [self menuContainerViewController].rightMenuViewController = episodesController;
    [[self menuContainerViewController] setRightMenuWidth:playerRightMenuWidth];

}

- (void)createSeasonsController {

    PlayerSeasonsViewController *seasonsController = [[ViewControllersFactory defaultFactory] playerSeasonsViewController];
    seasonsController.delegate = self;
    [self menuContainerViewController].rightMenuViewController = seasonsController;
    [[self menuContainerViewController] setRightMenuWidth:playerRightMenuWidth];
}


#pragma mark - Container Details -

- (void)setContainerDetailsOpenState:(BOOL)detailsOpen {

    [AppDelegate sharedAppDelegate].blockOrientationRotation = YES;

    if (isPad) {

        _buttonInfo.selected = detailsOpen;

        if ([self.view.subviews containsObject:self.sharePopUpView] || [self.view.subviews containsObject:self.soundSiderView] || [self.view.subviews containsObject:self.langPopUpView]) {

            [self.currentPopUp dismissPopUp];

        }
        _detailsContainerOpen = detailsOpen;

        _viewTapClose.hidden = !_detailsContainerOpen;
        float contentY = (_detailsContainerOpen ? viewContentYOpen : viewContentY);

        float contentDistance = fabsf(contentY - super.viewContent.frame.origin.y);
        float contentMaxDistance = viewContentYOpen - viewContentY;

        [UIView animateWithDuration:(0.3 * contentDistance / contentMaxDistance) animations:^{
            super.viewContent.frame = CGRectMake(0, contentY, super.viewContent.frame.size.width, super.viewContent.frame.size.height);

            _detailsContainerView.alpha = (detailsOpen ? 1.0 : 0.0);
            _detailsContainerView.frame = CGRectMake(_detailsContainerView.frame.origin.x, viewDetailsContentY - (detailsOpen ? 0.0 : _viewTabBar.frame.size.height * 1.5), _detailsContainerView.frame.size.width, _detailsContainerView.frame.size.height);
            CGRect frame = self.recoredStatusView.frame;
            frame.origin.y =+ self.viewNavigationBar.frame.origin.y + self.viewNavigationBar.frame.size.height+20;
            self.recoredStatusView.frame = frame;

        } completion:^(BOOL finished) {
            if (super.myZoneViewController) {
                [super clearMyZone];
            }
        }];
        if (detailsOpen) {
            [self stopTimer];
        }else {
            [self startTimer];
            self.fullscreenModeForPad = NO;
            [self.buttonFullScreen setImage:[UIImage imageNamed:@"player_maximize_icon"]
                                   forState:UIControlStateNormal];
        }
    }
}

- (IBAction) buttonInfoPressed:(id) sender{
    [self setContainerDetailsOpenState:!_detailsContainerOpen];
}

- (IBAction)tapOpenDetailsRecognizer:(UITapGestureRecognizer *)recognizer {
    if (_detailsContainerOpen) [self setContainerDetailsOpenState:!_detailsContainerOpen];
}

- (IBAction)tapCloseDetailsRecognizer:(UITapGestureRecognizer *)recognizer {

    if (super.viewContent.frame.origin.y > (kViewMyZoneHeight / 2)) {
        [self closeMyZone];
    } else {
        [self setContainerDetailsOpenState:NO];
    }
}

- (IBAction)swipeScreenUp:(UISwipeGestureRecognizer *)recognizer {

    if (super.viewContent.frame.origin.y > viewContentY) {
        [self closeMyZone];
    } else if (!_detailsContainerOpen) {
        [self setContainerDetailsOpenState:YES];
    } else {

    }
}

- (IBAction)swipeScreenDown:(UISwipeGestureRecognizer *)recognizer {

    if (super.viewContent.frame.origin.y > viewContentY) {

    } else if (_detailsContainerOpen) {
        [self setContainerDetailsOpenState:NO];
    } else {
        [self openMyZone];
    }
}

- (IBAction)detailsPanRecognizer:(UIPanGestureRecognizer *)recognizer {

    if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateFailed || recognizer.state == UIGestureRecognizerStateCancelled) {

        if (super.viewContent.frame.origin.y > (kViewMyZoneHeight / 2)) {

            float contentDistance = super.viewContent.frame.origin.y;
            float contentMaxDistance = kViewMyZoneHeight;

            [UIView animateWithDuration:(0.3 * contentDistance / contentMaxDistance) animations:^{

                super.viewContent.frame = CGRectMake(0, kViewMyZoneHeight, super.viewContent.frame.size.width, super.viewContent.frame.size.height);

                super.myZoneViewController.view.alpha = 1.0;
                super.myZoneViewController.view.frame = CGRectMake(super.myZoneViewController.view.frame.origin.x, 0.0, super.myZoneViewController.view.frame.size.width, super.myZoneViewController.view.frame.size.height);

            }];

        } else {

            float contentY = super.viewContent.frame.origin.y - viewContentYOpen;

            float contentHeight = viewContentYOpen - viewContentY;

            if (fabsf(contentY) < fabsf(contentHeight / 2.0)) {
                [self setContainerDetailsOpenState:YES];
            } else {
                [self setContainerDetailsOpenState:NO];
            }
        }
    }
    else {

        float contentY = super.viewContent.frame.origin.y + [recognizer translationInView:recognizer.view].y;

        //NSLog(@"pan %f  %f  %f", contentY, viewContentY, viewContentYOpen);

        if (contentY > viewContentY) {

            if (contentY > kViewMyZoneHeight) {
                contentY = kViewMyZoneHeight;
            }

            if (!super.myZoneViewController) {
                [super initMyZone];
            } else {

                float dStep = fabsf((contentY - viewContentY) / (kViewMyZoneHeight - viewContentY));
                super.myZoneViewController.view.alpha = dStep;
                super.myZoneViewController.view.frame = CGRectMake(super.myZoneViewController.view.frame.origin.x, (super.myZoneViewController.view.frame.size.height * (1.0 - dStep) * 0.2), super.myZoneViewController.view.frame.size.width, super.myZoneViewController.view.frame.size.height);
            }
        }
        else {

            if (contentY <= viewContentY) {

                if (super.myZoneViewController) {
                    [super clearMyZone];
                }
            }
        }

        if (contentY < viewContentYOpen) contentY = viewContentYOpen;

        float dStep = fabsf((contentY - viewContentYOpen) / (viewContentY - viewContentYOpen));
        _detailsContainerView.alpha = 1.0 - dStep;
        _detailsContainerView.frame = CGRectMake(_detailsContainerView.frame.origin.x, viewDetailsContentY - (_detailsContainerView.frame.size.height * dStep * 0.2), _detailsContainerView.frame.size.width, _detailsContainerView.frame.size.height);

        [recognizer setTranslation:CGPointZero inView:recognizer.view];

        super.viewContent.frame = CGRectMake(0, contentY, super.viewContent.frame.size.width, super.viewContent.frame.size.height);
    }
}

#pragma mark - Select media -

- (void)selectMediaItem:(TVMediaItem *)mediaItem {
    
    self.labelAutomation.accessibilityValue = @"";

    NSLog(@"----- > > > selectMediaItem  ----- > > > ----- > > > %@ ----- > >  %f",[NSDate date], ([NSDate timeIntervalSinceReferenceDate] * 1000));
    [self grayOutSlider:NO];

    if ([TVMixedCompanionManager sharedInstance].isConnected == YES && self.topBanner != nil)
    {
        [self changeTopBannerText:[self getCompanionBannerText]];
    }
    [self.playerDTGAdapter removeDownloadItemAllocation];

    [self hideRecoredIcon];
    self.timeSlider.hidden = YES;
    self.sliderReferenceLiveProgress.hidden = YES;
    self.labelCurrentTime.hidden = YES;
    self.labelFullTime.hidden = YES;

    self.playerMode = playerModeNone;

    [self hideEndOfShowScreen];

    _mediaStarted = NO;
    _mediaMark = nil;
    _itemsPricesWithCoupons = nil;
    _groupMediaRules = nil;

    _playedProgram = nil;

    [self setContainerDetailsOpenState:NO];

    _viewPlayerContainer.hidden = YES;
    _playerVODInfoView.hidden = NO;

    _buttonPlayPause.selected = NO;
    [_buttonPlayPause setImage:[UIImage imageNamed:@"player_play"] forState:UIControlStateSelected];
    [_buttonPlayPause setImage:[UIImage imageNamed:@"player_play"] forState:UIControlStateDisabled];

    [self stopPlayer];

    self.playerMediaItem = mediaItem;
    if ([self.initialMediaItem isEqual:mediaItem])
        [_arrPlayerMediasStack removeAllObjects];
    else {
        if (!_arrPlayerMediasStack.count || (_arrPlayerMediasStack.count && [_arrPlayerMediasStack lastObject] != mediaItem))
            [_arrPlayerMediasStack addObject:mediaItem];
    }
    _buttonBack.hidden = [mediaItem isLive];
    _buttonMenu.hidden = ![mediaItem isLive];

    _buttonBack.hidden = _liveTV;
    _buttonMenu.hidden = !_liveTV;

    _playerPinView.pinRuleID = 0;
    _playerPinView.liveMode = NO;
    [self playerPinViewClose];
    [self changePinViewHidden:YES];

    for (UIView* popup in self.view.subviews){
        if ([popup isKindOfClass:[PlayerVODContinueWatchingPopUp class]] || [popup isKindOfClass:[PlayerLangViewPhone class]]){
            [popup removeFromSuperview];
        }
    }

    [_shareContentView setupWithMedia:mediaItem];
    NSString *rightLabelText = LS(@"player_related_related_movies");
    _buttonShare.enabled = [LoginM isSignedIn];

    if (_playerMediaItem.isLive)
    {

        [[NSUserDefaults standardUserDefaults] setObject:mediaItem.mediaID forKey:@"LiveMediaSelected"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        UIViewController *controller = [self menuContainerViewController].rightMenuViewController;
        if (!controller || ![controller isKindOfClass:[PlayerLiveChannelsViewController class]]) {
            [self menuContainerViewController].rightMenuViewController = nil;
            [self createChannelsController];
        }

        PlayerLiveChannelsViewController *channelsController = (PlayerLiveChannelsViewController *)([self menuContainerViewController].rightMenuViewController);
        [channelsController updateWithMediaItem:_playerMediaItem];

        rightLabelText = LS(@"channels");

        if (_buttonShare.enabled && [[_playerMediaItem.metaData objectForKey:@"Limited UI channel"] boolValue]) {
            _buttonShare.enabled = NO;
        }

    } else if (_playerMediaItem.isEpisode) {

        UIViewController *controller = [self menuContainerViewController].rightMenuViewController;
        if (!controller || ![controller isKindOfClass:[PlayerEpisodesViewController class]]) {
            [self menuContainerViewController].rightMenuViewController = nil;
            [self createEpisodesController];
        }

        PlayerEpisodesViewController *episodesController = (PlayerEpisodesViewController *)([self menuContainerViewController].rightMenuViewController);
        [episodesController updateWithMediaItem:_playerMediaItem];

        rightLabelText = [LS(@"episodes") uppercaseString];


    } else if (_playerMediaItem.isSeries) {

        [self menuContainerViewController].rightMenuViewController = nil;

        rightLabelText = [LS(@"season_wo_param") uppercaseString];
        _buttonRightOpen.hidden = YES;

    } else {

        UIViewController *controller = [self menuContainerViewController].rightMenuViewController;
        if (!controller || ![controller isKindOfClass:[PlayerRelatedViewController class]]) {
            [self menuContainerViewController].rightMenuViewController = nil;
            [self createRelatedController];
        }

        PlayerRelatedViewController *relatedController = (PlayerRelatedViewController *)([self menuContainerViewController].rightMenuViewController);
        [relatedController updateWithMediaItem:_playerMediaItem];

    }

    if (_labelRightOpen) {
        _labelRightOpen.text = rightLabelText;
        _viewRightOpen.hidden = [APP_SET isOffline]?YES:NO;
    }

    if (_buttonRightOpen) {
        [_buttonRightOpen setTitle:rightLabelText forState:UIControlStateNormal];
        if (!_playerMediaItem.isSeries) {
            _buttonRightOpen.hidden = [APP_SET isOffline]?YES:NO;
        }
    }

    if (isPad) {

        _labelTitle.frame = CGRectMake(_labelTitle.frame.origin.x, _labelTitle.frame.origin.y, _buttonInfo.frame.origin.x - _labelTitle.frame.origin.x, _labelTitle.frame.size.height);
        _labelLiveProgramTime.hidden = YES;

    }

    _labelTitleFullscreen.text = mediaItem.name;

    if (_labelTitle) {
        _labelTitle.text = mediaItem.name;

        if (mediaItem.isEpisode) {

            _labelTitle.text = [Utils getSeriesName:mediaItem];

            if (isPad) {

                NSString *title = _labelTitle.text;
                NSString *subTitle = [NSString stringWithFormat:@"    %@ %ld, %@ %ld", LS(@"season_wo_param"), (long)[[mediaItem getMediaItemSeasonNumber] integerValue], LS(@"episode_wo_param"), (long)[[mediaItem getMediaItemEpisodeNumber] integerValue]];

                NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", title, subTitle]];

                [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, title.length)];
                [attrStr addAttribute:NSFontAttributeName value:Font_Bold(_labelTitle.font.pointSize) range:NSMakeRange(0, title.length)];

                [attrStr addAttribute:NSForegroundColorAttributeName value:CS(@"999999") range:NSMakeRange(title.length, subTitle.length)];
                [attrStr addAttribute:NSFontAttributeName value:Font_Reg(_labelTitle.font.pointSize - 1) range:NSMakeRange(title.length, subTitle.length)];

                _labelTitle.attributedText = attrStr;

            }

        }
    }

    [_playerVODInfoView updateData:mediaItem];
    _playerVODInfoView.hidden = NO;

    _playerLiveDetailsView.hidden = YES;
    _playerVODDetailsView.hidden = YES;
    _playerSeriesDetailsView.hidden = YES;
    _playerSeriesEpisodesView.hidden = YES;
    _playerNPVRInfoView.hidden = YES;

    _playerLiveDetailsView.alpha = 1.0;
    _playerVODDetailsView.alpha = 1.0;
    _playerSeriesDetailsView.alpha = 1.0;
    _playerSeriesEpisodesView.alpha = 1.0;
    _playerNPVRInfoView.alpha = 1.0;

    self.selectedAudioIndex = 0;
    self.selectedSubIndex = 0;


    _buttonTabLeft.enabled = YES;
    _buttonTabRight.enabled = YES;

    _buttonTabLeft.enabled = NO;
    _viewTabBar.alpha = 1.0;

    NSString *titleLeft = LS(@"info");
    NSString *titleRight = LS(@"social_feed");

    if (_playerMediaItem.isLive)
    {
        if (self.npvrPlayerAdapter)
        {
            self.playerNPVRInfoView.channel = mediaItem;
            [self.playerNPVRInfoView updateNPVRItem:self.npvrPlayerAdapter.recordedAsset];
            _playerNPVRInfoView.hidden = NO;
            titleLeft = LS(@"info");
        }
        else
        {
            [_playerLiveDetailsView updateData:mediaItem withFirstShownProgram:self.programToPlay?self.programToPlay:nil];
            _playerLiveDetailsView.hidden = NO;
            titleLeft = LS(@"whats_on");
        }


    } else if (_playerMediaItem.isSeries) {

        titleRight = LS(@"episodes");

        [_playerSeriesDetailsView updateData:mediaItem];
        _playerSeriesDetailsView.hidden = NO;

        self.seriesName = _playerMediaItem.name;

        NSDictionary *data = [_seriesData objectForKey:_seriesName];

        if (data) {

            [self updateEpisodes];
        } else {

            self.fullList = [[NSMutableArray alloc] init];

            [_playerSeriesEpisodesView updateData:[NSArray array]];
            [_playerSeriesEpisodesView showHUDNoBack];

            [self loadEpisodes:0];

        }

        [self buttonTabBarPressed:_buttonTabRight];

    } else {
        [_playerVODDetailsView updateData:mediaItem];
        _playerVODDetailsView.hidden = NO;
    }

    [_buttonTabLeft setTitle:titleLeft forState:UIControlStateNormal];
    [_buttonTabRight setTitle:titleRight forState:UIControlStateNormal];

    _labelCurrentTime.text = @"00:00";
    _labelFullTime.text = @"00:00";

    _labelCurrentTime.text = @"";
    _labelFullTime.text = @"";

    if (![_playerMediaItem isLive]) {

        if ([_playerMediaItem.metaData objectForKey:@"Runtime"] && [[_playerMediaItem.metaData objectForKey:@"Runtime"] length] > 0) {

            int min = (int)([[_playerMediaItem.metaData objectForKey:@"Runtime"] intValue]) / 60;
            if (min < 5) {
                min = [[_playerMediaItem.metaData objectForKey:@"Runtime"] intValue];
            }

            int hours = min / 60;
            int minutes = min % 60;

            _labelCurrentTime.text = @"00:00";

            if (hours > 0) {
                _labelFullTime.text = [NSString stringWithFormat:@"%i:%02i:%02i", hours, minutes, 0];
            } else {
                _labelFullTime.text = [NSString stringWithFormat:@"%02i:%02i", minutes, 0];
            }

        }


    }

    _timeSlider.value = 0;

    _buttonLang.enabled = NO;
    if (isPhone){
        _buttonLang.alpha = (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? 1.0 : 0.0);

        self.buttonStartover.hidden = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ?!(self.playerMode==playerModeStartOver || self.playerMode == playerModeLIVE || self.playerMode == playerModeCatchUp) : YES;

        if (self.buttonStartover.hidden != YES)
        {
            self.buttonStartover.UIUnitID = NU(UIUnit_live_startOver_buttons);
            [self.buttonStartover activateAppropriateRule];

        }


    }

    _viewTabBar.hidden = !APST.socialFeed || [APP_SET isOffline];
    if (_playerMediaItem.isSeries && isPhone) {
        _viewTabBar.hidden = NO;
    }

    [_socialManager updateWithMedia:_playerMediaItem];

    _playerSocialView.hidden = YES;

    [_playerSocialView updateMedia:_playerMediaItem];

    if (!_playerMediaItem.isSeries) {

        if (self.npvrPlayerAdapter)
        {
            if ([self.npvrPlayerAdapter isStartOver]) {
                [self hideHUD];
                [self buttonPlayPressed];
            }
            else {
                [self getMediaMark];
            }
        }
        else
        {
            //hide item details on ipad if live and show only if not subscribed
            if (self.playerMediaItem.isLive) {
                if (isPad)
                    _playerVODInfoView.hidden = YES;
            }

            if (![APP_SET isOffline])
            {
                [MediaGRA getItemsPricesWithCoupons:self.playerMediaItem delegate:self completion:^(NSDictionary *resultAllowedItems, NSDictionary *resultNotAllowedItems) {
                    //get item prices
                    self.itemsPricesWithCoupons = resultAllowedItems;
                    self.itemsPricesWithCouponsNotPurchase = resultNotAllowedItems;
                    [self updateItemMainActionState];
                }];
            }
            else
            {
                [self updateItemMainActionState];
            }
        }
    }

    if ([self showCoachMark]) {
        self.coachMarkView = [[CoachMarksView alloc]initWithMediaItem:self.playerMediaItem];
        self.coachMarkView.delegate = self;
        [self.coachMarkView show];
        [self didShowCoachMark];
    }

    if ([APP_SET isOffline])
    {
        [self menuContainerViewController].rightMenuViewController = Nil;
        [[self menuContainerViewController] setRightMenuWidth:playerRightMenuWidth];

    }
}



- (void) updateItemMainActionState {

    if (!LoginM.isSignedIn && ![self canPlay])
    {
        if (self.playerMediaItem.isLive)
        {
            self.playerVODInfoView.playbutton.hidden = NO;
        }
        self.playerVODInfoView.hidden = NO;
        [self.playerVODInfoView showLogin];

        if (isPad)
        {
            return;
        }
    }
    else
    {
        self.playerVODInfoView.viewLogin.alpha = 0.0;
        self.playerVODInfoView.labelTitle.alpha = 0.0;
    }

    if (self.itemsPricesWithCoupons.count || [APP_SET isOffline]) {
        //has prices
        [self updateActionButtonForState:kVODButtonShouldPlayState];
    }
    else {

        //check if subscriptions are active
        if (APST.hasInAppPurchases) {
            if ([LoginM isSignedIn]) {
                hasPurchasedSubscriptions = FALSE;
                [SubscriptionM getSubscriptionsContainingMediaItem:self.playerMediaItem delegate:self completion:^(NSArray *arrSubscriptionsPurchased, NSArray *arrSubscriptionsToPurchase) {
                    
                    //check if hidden item page data and show it (live)
                    if (isPad && _playerVODInfoView.hidden && !arrSubscriptionsPurchased.count)
                        _playerVODInfoView.hidden = NO;
                    
                    //found subscriptions
                    if (arrSubscriptionsPurchased.count) {
                        [self updateActionButtonForState:kVODButtonShouldPlayState
                                  subscriptionsPurchased:arrSubscriptionsPurchased
                                 subscriptionsToPurchase:arrSubscriptionsToPurchase];
                    }
                    else {
                        TVSingleItemPrice *sip = [MediaGRA getMediaPrice:self.playerMediaItem.files];
                        if (sip) {
                            NSString *format = [self bestFileformat];
                            NSInteger fileId = [[self.itemsPricesWithCoupons objectForKey:format] integerValue];
                            if (fileId==0)
                            {
                                fileId = [[self.itemsPricesWithCouponsNotPurchase objectForKey:format] integerValue];
                            }
                            if ([MediaGRA isPriceReasonPurchased:sip.priceReason]) {
                                [PurchasesM getMediaFileRentInfo:fileId delegate:self completion:^(TVRental *mediaRentalDetails) {
                                    [self updateActionButtonForState:kVODButtonShouldPlayState subscriptionsPurchased:nil subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:mediaRentalDetails ppvData:nil];
                                }];
                            }
                            else {//need to purchase
                                
                                switch (sip.priceReason) {
                                    case TVPriceTypeForPurchaseSubscriptionOnly:
                                        [self updateActionButtonForState:kVODButtonPurchaseState subscriptionsPurchased:arrSubscriptionsPurchased subscriptionsToPurchase:arrSubscriptionsToPurchase];
                                        break;
                                    case TVPriceTypeForPurchase: {
                                        TVSingleItemPrice *sip1 = [MediaGRA getMediaPrice:self.playerMediaItem.files];
                                        NSString *productCode = [MediaGRA getMediaProductCode:self.playerMediaItem.files];
                                        if (productCode && sip1.PPVmoduleCode && sip1.PPVmoduleCode.length) {
                                            NSString *ppvModuleCode = [[NSString alloc] initWithString: sip1.PPVmoduleCode];
                                            [PPVM getPPVModuleDataWithPPVmodel:ppvModuleCode productCode:productCode completion:^(TVPPVModelData *ppvData) {
                                                __strong TVRental * rent = [TVRental new];
                                                rent.currentDate = [NSDate date];
                                                rent.endDate = [NSDate dateWithTimeIntervalSinceNow:(60*(ppvData.PPVUsageModule.viewLifeCycle+1))];
                                                ppvData.PPVPriceCode.prise.price = sip.price.price;
                                                rent.price = [NSString stringWithFormat:@"%f",sip.price.price];
                                                rent.currencySign = ppvData.PPVPriceCode.prise.currency.currencySign;
                                                rent.productCode = ppvData.productCode;
                                                rent.mediaFileID = [[MediaGRA getFileIdToSingleItemPrice:sip1] integerValue];
                                                rent.deviceUDID = [TVConfigurationManager getTVUDID];
                                                [self updateActionButtonForState:kVODButtonPurchaseState subscriptionsPurchased:nil subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:rent ppvData:ppvData];
                                            }];
                                        }
                                        else
                                        {
                                            [self updateActionButtonForState:kVODButtonPurchaseState subscriptionsPurchased:nil subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:nil ppvData:nil];
                                        }
                                        
                                    }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        else {
                            [self updateActionButtonForState:kVODButtonPurchaseState];
                        }
                    }
                }];
            }
            else //if not signed in show play and init login on press
                [self updateActionButtonForState:kVODButtonShouldPlayState];
            
        }
        else {
            //check if hidden item page data and show it (live)
            if (isPad && _playerVODInfoView.hidden)
                _playerVODInfoView.hidden = NO;
            
            if (self.itemsPricesWithCoupons.count) {
                //has prices
                [self updateActionButtonForState:kVODButtonShouldPlayState];
            }
            else if (APST.hasWatchList) { //go to watchlist flow
                [self updateWatchListButtonState];
            }
            else {
                [self updateActionButtonForState:kVODButtonPurchaseState];
            }
        }
    }
}

- (void)updateWatchListButtonState {
    [PurchasesM isMediaInWatchList:[self.playerMediaItem.mediaID integerValue] delegate:self completion:^(BOOL success, NSError *error) {
        VODButtonState vodButtonsState = success ? kVODButtonAddedToWatchListState : kVODButtonAddToWatchListState;
        [self updateActionButtonForState:vodButtonsState];
    }];
}

- (void)updateActionButtonForState:(VODButtonState)state {
    [self updateActionButtonForState:state subscriptionsPurchased:nil subscriptionsToPurchase:nil];
}

- (void)updateActionButtonForState:(VODButtonState)state subscriptionsPurchased:(NSArray *) arrSubscriptionsPurchased subscriptionsToPurchase:(NSArray*) arrSubscriptionsToPurchase
{
    [self updateActionButtonForState:state subscriptionsPurchased:arrSubscriptionsPurchased subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:nil ppvData:nil];
}

- (void)updateActionButtonForState:(VODButtonState)state subscriptionsPurchased:(NSArray *) arrSubscriptionsPurchased subscriptionsToPurchase:(NSArray*) arrSubscriptionsToPurchase ppvRentalInfo:(TVRental*) ppvRentalInfo ppvData:(TVPPVModelData *)ppvData
{
    hasPurchasedSubscriptions = (arrSubscriptionsPurchased.count) ? TRUE : FALSE;
    if (isPad) {
        if (ppvRentalInfo) {
            [self.playerVODInfoView updatePadActionButtonsWithState:state item:self.playerMediaItem subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:ppvRentalInfo ppvData:ppvData];
        }
        else {
            [self.playerVODInfoView updatePadActionButtonsWithState:state item:self.playerMediaItem subscriptionsToPurchase:arrSubscriptionsToPurchase subscriptionsPurchased:arrSubscriptionsPurchased];
        }
    }
    else {
        if (ppvRentalInfo) {
            [self.playerVODDetailsView updatePlayButtonWithState:state subscriptionsToPurchase:arrSubscriptionsToPurchase ppvRentalInfo:ppvRentalInfo ppvData:ppvData];
        }
        else {
            [self.playerVODDetailsView updatePlayButtonWithState:state subscriptionsToPurchase:arrSubscriptionsToPurchase subscriptionsPurchased:arrSubscriptionsPurchased ];
        }
    }
    
    [self checkMediaRules];
}

- (void)finishedUpdatingActionButtonForState:(VODButtonState)state {

    if ([self isMediaAllowedForDownloading:self.playerMediaItem]) {
        if (!self.playerDTGAdapter) {
            self.playerDTGAdapter = [PlayerDTGAdapter new];
        }
        self.playerDTGAdapter.delegateController = self;
        [self.playerDTGAdapter getItemDetails];
    }
    else {
        if (!self.playerDTGAdapter) {
            self.playerDTGAdapter = [PlayerDTGAdapter new];
        }
    }
}

- (void)setLivePlayer {

    _liveTV = YES;

#ifdef PLAYER

    [self.videoPlayerControl Stop];
    [self.videoPlayerControl cleanPlayer];
    [self clearOldAttributes];
    [self stopTimer];
#endif

    _labelTitle.text = @"";

    if (isPad){
        _playerVODInfoView.hidden = YES;
        _playerVODInfoView.alpha = 1.0;
        _labelLiveProgramTime.hidden = YES;
        _viewRightOpen.hidden = YES;
    }

    if (isPhone){
        _playerSeriesDetailsView.hidden = YES;
        _playerSeriesEpisodesView.hidden = YES;
        _playerVODInfoView.hidden = YES;

        _playerSeriesDetailsView.alpha = 1.0;
        _playerSeriesEpisodesView.alpha = 1.0;
        _playerVODInfoView.alpha = 1.0;

        _buttonRightOpen.hidden = YES;
        _viewTabBar.hidden = YES;
    }

    if (!EPGM.channelsList) {

        [self showHUDBlockingUI:YES];

        [EPGM loadEPGChannels:self completion:^(NSMutableArray *result) {

            [EPGM updateEPGList:result delegate:self completion:^(NSMutableArray *result) {

            }];

            [self hideHUD];
            [self startLiveChannel];

        }];

    } else {

        [self startLiveChannel];

    }

}

- (void)startLiveChannel {

    if ([EPGM.channelsList count]) {

        int index = 0;

        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"LiveMediaSelected"]) {

            NSString *mediaId = [[NSUserDefaults standardUserDefaults] objectForKey:@"LiveMediaSelected"];

            for (int i = 0; i < [EPGM.channelsList count]; i++) {
                TVMediaItem *media = [EPGM.channelsList objectAtIndex:i];
                if ([media.mediaID isEqualToString:mediaId]) {
                    index = i;
                }
            }
        }


        if (isInitialized) {
            [self selectMediaItem:[EPGM.channelsList objectAtIndex:index]];
        } else {
            self.initialMediaItem = [EPGM.channelsList objectAtIndex:index];
        }
        TVMediaItem *mediaItem = [EPGM.channelsList objectAtIndex:index];
        NSString *screen = [NSString stringWithFormat:@"Linear:%@", mediaItem.name];
        [AnalyticsM trackScreen:screen];
    }
}


-(NSString *) fileFormatToPlayForMedia:(TVMediaItem *) mediaItem itemPrices:(NSDictionary *) itemsPrices
{
    NSString *fileTypeFormatKey = APST.TVCMediaFormatMainSD;
    if ([itemsPrices count]) {

        if ([itemsPrices objectForKey:APST.TVCMediaFormatMainHD]) {
            fileTypeFormatKey = APST.TVCMediaFormatMainHD;
        } else {
            fileTypeFormatKey = APST.TVCMediaFormatMainSD;
        }
    }
    return fileTypeFormatKey;
}


-(void)buildCustomData
{
    //    [self.mediaToPlayInfo setCustomData:[NSString stringWithFormat:@"%@|185|%@", PlayerCDA.securedSiteGUID,self.mediaToPlayInfo.currentFile.fileID]];
}

-(void)updateIsClearContent
{
#ifdef PLAYER
    self.mediaToPlayInfo.isClearContent = NO;
#endif
}

- (void)playMediaType:(NSString *)fileTypeFormatKey startTime:(float)startTime
{
#ifdef PLAYER
    self.mediaToPlayInfo = [[TVMediaToPlayInfo alloc] initWithMediaItem:self.playerMediaItem];
    self.mediaToPlayInfo.startTime = startTime;
    self.mediaToPlayInfo.fileTypeFormatKey = fileTypeFormatKey;
#ifdef PLAYER_DX
    [self.mediaToPlayInfo setUseSignedUrl:YES];
    [self.mediaToPlayInfo setIsHarmonicsHLS:NO];
    [self buildCustomData];
    [self updateIsClearContent];
#endif

#ifdef PLAYER_WV
    [self.mediaToPlayInfo setUseSignedUrl:YES];
#endif

    //play downloaded file
    if (!self.playerMediaItem.isLive && [self.playerDTGAdapter isItemDownloaded] && ![fileTypeFormatKey isEqualToString:self.playerMediaItem.trailerFile.format]) {
        self.playerMode = playerModeVOD;
        if (!self.playerDTGAdapter.downloadItem.wasWatched)
            self.playerDTGAdapter.downloadItem.wasWatched = TRUE;
        NSString *origFormat = self.playerDTGAdapter.downloadItem.format;
        NSString *format = [origFormat stringByAppendingString:APST.dtgParams[@"downloadedFileFormatNameSuffix"]];
        TVFile *fileOriginalforDownloadedFormat = [self.playerMediaItem.files filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"format == %@", origFormat]].firstObject;

        KADownloadItem *downloadItem = self.playerDTGAdapter.downloadItem;
        NSURL* itemPlaybackURL = [[DTGManager sharedInstance] getDownloadedFileUrl:downloadItem];
        
        [self.mediaToPlayInfo addPLTVFileWithFormat:format andUrlString:itemPlaybackURL.absoluteString andBaseFile:fileOriginalforDownloadedFormat];
        self.mediaToPlayInfo.fileTypeFormatKey = format;
        //        self.mediaToPlayInfo.isClearContent = YES;
        [self.mediaToPlayInfo setUseSignedUrl:NO];
        [self checkParentalBeforePlayingIfNeededAndPlayMedia:self.mediaToPlayInfo];
    }
    // npvr play
    else if (self.npvrPlayerAdapter)
    {
        [self.npvrPlayerAdapter downloadLicencedLinkWithCompletion:^(NSURL * url,bool isStartOver)
         {

             NSString * fileType = isStartOver?TVCMediaFormat_StartOver:TVCMediaFormat_CatchUp;
             [self.mediaToPlayInfo addPLTVFileWithFormat:fileType andUrlString:[url absoluteString] andBaseFile:self.mediaToPlayInfo.mediaItem.mainFile];
             self.mediaToPlayInfo.fileTypeFormatKey = fileType;
             self.mediaToPlayInfo.npvrId = self.npvrPlayerAdapter.recordedAsset.recordingID;
             [self.mediaToPlayInfo setUseSignedUrl:NO];
             self.playerMode = isStartOver?playerModeStartOver:playerModeCatchUp;
             [self checkParentalBeforePlayingIfNeededAndPlayMedia:self.mediaToPlayInfo];
         } failedBlock:^{

         }];

    }
    // pltv play
    else if (self.programToPlay != nil && [NU(UIUnit_Play_Catchup) isUIUnitExist] && [NU(UIUnit_Play_StartOver) isUIUnitExist])
    {   // PSC : Vodafone Grup : SYN-3577 -> playout errors appears from time to time [IOS]
        if ([Utils canTrickPlayCachupToProgram:self.programToPlay] == NO)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Action_is_not_applicable" message:LS(@"This_action_cannot_be_used_for_this_program_due_to_the_program's_length.") delegate:nil cancelButtonTitle:LS(@"Ok") otherButtonTitles:nil];
            [alertView show];
            return;
        }
        self.watchAdapter.program = self.programToPlay;
        NSString * trickPlayFormat = nil;

        if ([self.programToPlay programIsOn])
        {
            // start over / Trick play
            self.playerMode = playerModeStartOver;
            self.mediaToPlayInfo.startTime = startTime>1?startTime:1;
            trickPlayFormat =  startTime>1?TVCMediaFormat_TrickPlay:TVCMediaFormat_StartOver;

        }
        else if ([self.programToPlay programFinished])
        {
            //Catch up
            self.playerMode = playerModeCatchUp;
            self.mediaToPlayInfo.startTime = startTime > 0?startTime:0;
            trickPlayFormat =  TVCMediaFormat_CatchUp;
        }

        [PlayerPLTVAdapter makeMediaReadyforPLTVFormat:trickPlayFormat andMediaToPlay:self.mediaToPlayInfo withProgram:[self.programToPlay EPGProgram] withCompletionBlock:^(BOOL succeeded) {

            if (succeeded)
            {
                [self.mediaToPlayInfo setUseSignedUrl:NO];
                [self setPlayedProgram:self.programToPlay];
                [self checkParentalBeforePlayingIfNeededAndPlayMedia:self.mediaToPlayInfo];

            }
        }];

    }
    // regular play
    else
    {
        self.watchAdapter.mediaItem = self.mediaToPlayInfo.mediaItem;
        self.playerMode = [self.mediaToPlayInfo.currentFile.format isEqualToString:APST.TVCMediaFormatTrailer]? playerModeTrailer:playerModeVOD;
        self.playerMode  = [self.mediaToPlayInfo.mediaItem isLive]?playerModeLIVE:self.playerMode;


        if (self.playerMode == playerModeLIVE)
        {
            [self updateLiveProgramWithCompletion:^(bool changed) {
                [self checkParentalBeforePlayingIfNeededAndPlayMedia:self.mediaToPlayInfo];
            }];
        }
        else
        {
            [self checkParentalBeforePlayingIfNeededAndPlayMedia:self.mediaToPlayInfo];
        }
    }

    [self checkIsUserInDomain];

#endif
    _viewPlayerContainer.hidden = NO;
    _playerVODInfoView.hidden = YES;
}

#ifdef PLAYER
-(void) checkParentalBeforePlayingIfNeededAndPlayMedia:(TVMediaToPlayInfo *) mediaToPlay
{
    if ((self.playerMode == playerModeLIVE || self.playerMode == playerModeCatchUp || self.playerMode == playerModeStartOver) && self.npvrPlayerAdapter == nil)
    {
        [self checkLiveParentalForProgram:self.playedProgram completion:^(bool hasParentalRule) {
            if (!hasParentalRule) {
                [self finalPlayWithPrepareMediaToPlay:mediaToPlay];
            }
            else
            {
                [self playerPinViewShowWithcompletion:^(BOOL isUnlocked, NSString *enteredCode) {
                    if (isUnlocked) {
                        self.blockedUI = NO;
                        self.groupMediaRules = [NSDictionary dictionary];
                        _playerPinView.pinRuleID = 0;

                        if (![APP_SET isOffline]) {
                            [self.playerDTGAdapter updateParentalAccessCode:enteredCode];
                        }
                        [self finalPlayWithPrepareMediaToPlay:mediaToPlay];
                    }

                    [self playerPinViewClose];
                    [self changePinViewHidden:YES];
                }];
            }
        }];
    }
    else {
        [self finalPlayWithPrepareMediaToPlay:mediaToPlay];
    }
}

-(void) finalPlayWithPrepareMediaToPlay:(TVMediaToPlayInfo *) mediaToPlay {
    [self.videoPlayerControl playMedia:mediaToPlay];
    [self updateUIAcordingToPlayerState];
}

#endif

- (void)checkIsUserInDomain
{
    NSDictionary* metaData = self.playedProgram.metaData;
    BOOL checkProximity = [[ProximityChecker instance] needCheckingProgramWithMetaData:metaData catchingUp:self.playerMode == playerModeCatchUp];

    if (checkProximity) {
        __weak PlayerViewController* weakSelf = self;

        [[ProximityChecker instance] startChecking:^(BOOL inDomain) {
            if (!inDomain) {
                [weakSelf processOutOfDomainState];
            }
        }];
    }
}

- (void)processOutOfDomainState {
    [self stopPlaying];

    UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:nil message:LS(@"alert_proximity") delegate:self cancelButtonTitle:LS(@"OK") otherButtonTitles:nil];
    [alertView show];
}

-(void) updateUIAcordingToPlayerState
{

    [self stopCheckLiveProgram];
    self.timeSlider.hidden = NO;
    self.sliderReferenceLiveProgress.hidden = NO;
    self.labelCurrentTime.hidden = NO;
    self.labelFullTime.hidden = NO;
    [self.buttonPlayPause resetRule];

    [self.companionModeOverlay animateOutWithCompletion:nil];
#ifdef PLAYER


    [self adjustSliderAccordingToCurrentState];

    if (self.playerMode == playerModeTrailer || self.playerMode == playerModeVOD)
    {
        if (_labelTitleFullscreen)
            _labelTitleFullscreen.text = self.mediaToPlayInfo.mediaItem.name;
    }
    else if (self.playedProgram)
    {
        [self updatePlayedProgramUI:self.playedProgram];

    } else if (self.npvrPlayerAdapter)
    {
        [self updatePlayedProgramUI:self.npvrPlayerAdapter.recordedAsset];
    }
    
    // set up slider enable
#endif

}


-(void) adjustSliderAccordingToCurrentState
{
    //UIImage * progressThumb = [UIImage imageNamed:@"slider_thubm"];
    if (self.playerMode == playerModeLIVE || self.playerMode == playerModeStartOver || self.playerMode == playerModeCatchUp)
    {
        self.timeSlider.UIUnitID = self.playerMode == playerModeLIVE || self.playerMode == playerModeStartOver? NU(UIUnit_SeekBar_Thumb_StartOver):NU(UIUnit_SeekBar_Thumb_Catchup);


        if (([self.playedProgram programIsOn] && ![self.playedProgram isStartOverEnabled] ) || ([self.playedProgram programFinished] && ![self.playedProgram isCathupEnabled] ))
        {
            self.timeSlider.userInteractionEnabled = NO;
        }
    }
    else if (self.playerMode == playerModeTrailer ||
             self.playerMode == playerModeVOD)
    {
        self.timeSlider.UIUnitID = nil;
        self.timeSlider.userInteractionEnabled = YES;
    }
    else
    {
        self.timeSlider.UIUnitID = nil;
        self.timeSlider.userInteractionEnabled = NO;
    }

     [self.timeSlider activateAppropriateRuleWithThumbImage:[UIImage imageNamed:@"slider_thubm"]];

}


#pragma mark - Episodes

- (void)loadEpisodes:(int)page {

    NSString *episodeTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeEpisode];
    NSDictionary *tagsDictionary = [NSDictionary  dictionaryWithObjectsAndKeys:_seriesName, @"Series Name", nil];

    __weak TVPAPIRequest *request = [TVPMediaAPI requestForSearchMediaByAndOrList:@[tagsDictionary] AndList:@[] mediaType:episodeTypeID pageSize:maxPageSize pageIndex:page orderBy:TVOrderByAndOrList_NAME orderDir:TVOrderDirectionByAndOrList_Ascending exact:NO orderMeta:@"" delegate:nil];

    [request setCompletionBlock:^{

        NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];

        if ([result count]) {

            for (id item in result) {
                [_fullList addObject:item];
            }

        }

        if ([result count] == maxPageSize) {

            [self loadEpisodes:page + 1];

        } else {

            [self sortEpisodes];

        }

    }];

    [request setFailedBlock:^{


    }];

    [self sendRequest:request];


}

- (void)sortEpisodes {

    NSDictionary *dictionarySeasonsBySeasonNumber = [Utils mapEpisodesBySeasonsNumber:_fullList];

    NSArray *seasons = [Utils sortSeasonsNumbers:dictionarySeasonsBySeasonNumber.allKeys];

    NSDictionary *item = [[NSDictionary alloc] initWithObjectsAndKeys:seasons, @"seasons", dictionarySeasonsBySeasonNumber, @"episodes", nil];
    [_seriesData setObject:item forKey:_seriesName];

    [self updateEpisodes];

}

- (NSArray *)episodesArray {

    NSDictionary *data = [_seriesData objectForKey:_seriesName];

    NSDictionary *dictionarySeasonsBySeasonNumber = [data objectForKey:@"episodes"];

    NSArray *seasons = [data objectForKey:@"seasons"];

    if (_currentSeason < 0 || _currentSeason >= [seasons count])  {
        _currentSeason = 0;
    }

    if (_currentSeason < [seasons count]) {
        NSString *season = [seasons objectAtIndex:_currentSeason];
        return [dictionarySeasonsBySeasonNumber objectForKey:season];
    } else {
        return [NSArray array];
    }
}

- (void)updateEpisodes {

    NSArray *episodes = [self episodesArray];

    [_playerSeriesEpisodesView updateData:episodes];
    [_playerSeriesEpisodesView hideHUD];


    NSDictionary *data = [_seriesData objectForKey:_seriesName];
    NSArray *seasons = [data objectForKey:@"seasons"];

    if ([episodes count] && _currentSeason < [seasons count]) {
        NSString *season = [seasons objectAtIndex:_currentSeason];

        [_buttonRightOpen setTitle:[NSString stringWithFormat:[LS(@"season") uppercaseString], season] forState:UIControlStateNormal];

        if (_buttonRightOpen.hidden) {
            _buttonRightOpen.hidden = [APP_SET isOffline]?YES:NO;

            UIViewController *controller = [self menuContainerViewController].rightMenuViewController;
            if (!controller || ![controller isKindOfClass:[PlayerSeasonsViewController class]]) {
                [self menuContainerViewController].rightMenuViewController = nil;
                [self createSeasonsController];
            }

            PlayerSeasonsViewController *seasonsController = (PlayerSeasonsViewController *)([self menuContainerViewController].rightMenuViewController);
            seasonsController.currentSeason = _currentSeason;
            seasonsController.seasons = seasons;
            [seasonsController reloadData];

        }

        if (_buttonTabRight.enabled) [self buttonTabBarPressed:_buttonTabRight];
    }


}

- (void)selectSeriesSeason:(NSInteger)seasonNum {

    _currentSeason = seasonNum;

    [self updateEpisodes];

}

#pragma mark - CoachMark
- (BOOL)showCoachMark {

    if (isPad) {
        if ([self.playerMediaItem isLive]) {
            if ((![[NSUserDefaults standardUserDefaults] objectForKey:@"UserDefaultsPlayerVCLiveIsFirstTime"])){
                return YES;
            }
        } else {
            if (![[NSUserDefaults standardUserDefaults] objectForKey:@"UserDefaultsPlayerVCVODIsFirstTime"]) {
                return YES;
            }
        }
    }
    return NO;
}


- (void)didShowCoachMark {
    if ([self.playerMediaItem isLive]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UserDefaultsPlayerVCLiveIsFirstTime"];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UserDefaultsPlayerVCVODIsFirstTime"];
    }

    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)didDismissCoachMarksView {

}

#pragma mark -

- (void)checkMediaRules {

    __weak PlayerViewController *playerController = self;

    if(![APP_SET isOffline])
    {
        [MediaGRA getGroupMediaRules:self.playerMediaItem delegate:self completion:^(NSDictionary *result) {

            playerController.groupMediaRules = result;
            [self getMediaMark];
        }];

    }
    else
    {
        //call getgroupMediaRules for offline and in the completion get media mark
        [self getMediaMark];
    }


}

- (void)getMediaMark {

    if (self.npvrPlayerAdapter)
    {
        __weak PlayerViewController *playerController = self;
        [PlayerCDA requestForGetMediaMarkNPVR:self.npvrPlayerAdapter.recordedAsset delegate:self completion:^(NSDictionary *result) {
            playerController.mediaMark = result;
            [playerController hideHUD];
            [self buttonPlayPressed];
        }];
    }
    else {
        if (_playerMediaItem.isLive) {
            [self hideHUD];

            if ([self canPlay])
            {
                [self buttonPlayPressed];
            }

        } else {

            if ([_itemsPricesWithCoupons count] == 0 && ![APP_SET isOffline]) {

                [self hideHUD];

                return;

            } else if ([_groupMediaRules count]) {

                if ([_groupMediaRules objectForKey:@"BlockType_AgeBlock"]) {

                    [self hideHUD];

                    return;

                } else if ([_groupMediaRules objectForKey:@"BlockType_GeoBlock"]) {

                    [self hideHUD];

                    return;

                } else if ([_groupMediaRules objectForKey:@"BlockType_Validation"]) {

                }
            }

            __weak PlayerViewController *playerController = self;

            if ([APP_SET isOffline])
            {
                playerController.mediaMark = @{@"nLocationSec":[NSString stringWithFormat:@"%f",((KADownloadItem *)self.playerMediaItem.downloadItemRef).mediaMark.locationSec]};
                [playerController hideHUD];

            }
            else
            {
                [PlayerCDA requestForGetMediaMark:self.playerMediaItem delegate:self completion:^(NSDictionary *result) {

                    if (self.playerDTGAdapter) {
                        result = [self.playerDTGAdapter syncMediaMark:result];
                    }
                    playerController.mediaMark = result;
                    [playerController hideHUD];
                }];

            }

        }
    }
}

- (void)checkLiveParentalForProgram:(APSTVProgram *)program completion:(void(^)(bool hasParentalRule)) completion
{
    [PlayerCDA checkLiveParental:program channel:_playerMediaItem delegate:self completion:^(NSArray *result) {

        for (NSDictionary *ruleDic in result) {
            TVRuleItem *rule = [[TVRuleItem alloc] initWithDictionary:ruleDic];
            NSLog(@"\n\nRULE %@\n%@  %@\n", [ruleDic description], _playerMediaItem.mediaID, program.epgId);
            if (rule.blockType == BlockType_Validation) {
                _playerPinView.pinRuleID = rule.ruleId;
            }
        }
        completion (_playerPinView.pinRuleID > 0?YES:NO);
    }];
}

#pragma mark - Pin Popup


- (void)playerPinViewShowWithcompletion:(void(^)(BOOL isUnlocked, NSString *enteredCode)) completion {
    [_playerPinView initParentScreen];
    _playerPinView.parentalPinCompletionBlock = completion;
    [NavM setPhonePortrait];
    [self performSelector:@selector(showParentalPinProcess) withObject:nil afterDelay:0.5];
    [self changePinViewHidden:NO];
}

- (void)playerPinViewClose {
    _playerPinView.hidden = YES;
    [_playerPinView removeFromSuperview];
}

- (void)showParentalPinProcess {
    self.blockedUI = YES;
    _playerPinView.hidden = NO;
    _playerPinView.frame = _viewCenter.frame;

    if (isPad) {
        _playerPinView.frame = CGRectMake(_playerPinView.frame.origin.x, _viewCenter.frame.origin.y, _playerPinView.frame.size.width, _playerPinView.frame.size.height);
        [self.viewContent addSubview:_playerPinView];
        [self.viewContent bringSubviewToFront:_viewRightOpen];
    }

    if (isPhone) {
        _playerPinView.frame = CGRectMake(_playerPinView.frame.origin.x, super.viewContent.y + _viewCenter.frame.origin.y, _playerPinView.frame.size.width, _playerPinView.frame.size.height);
        [self.view addSubview:_playerPinView];
    }
}

- (void)changePinViewHidden:(BOOL)hidden {

#ifdef PLAYER

    if (hidden) {
        [_videoPlayerControl unForceMute];
    } else {
        [_videoPlayerControl forceMute];
    }

#endif

}

#pragma mark - PlayerSoundSliderDelegate

- (void)volumeValueChanged:(CGFloat)volumeValue {
    [self startTimer];
    NSString *imageName = (volumeValue > 0.0) ? @"player_sound_icon" : @"player_sound_mute_icon";

    [_buttonSound setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [_buttonSound setImage:[[UIImage imageNamed:imageName] imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
    [_buttonSound setImage:[[UIImage imageNamed:imageName] imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
}

#pragma mark - PlayerSeriesDetailsViewDelegate

- (void)buttonShareSeriesPressed {

    [self buttonSharePressed:_buttonShare];

}

#pragma mark - Companion
-(void)showLoginView
{

}

- (IBAction)buttonCompanionPressed:(UIButton *)button {

    if (isPhone)
    {

        self.companionView = [CompanionView companionView];
        [self.companionView updateTexts];
        self.companionView.delegate = self;

        if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {

            self.companionView.frame = CGRectMake(playerFullscreenX, playerFullscreenY, playerFullscreenWidth, playerFullscreenHeight);

        } else {
            self.companionView.frame = super.viewContent.frame;
        }

        [((CompanionViewPhone *)self.companionView) updateScreenSize];

        [self.companionView updateUIAccordingToConnectionMode];
        if ([TVMixedCompanionManager sharedInstance].isConnected == NO)
        {
            [self.companionView buttonScanPressed:nil];
        }
        
        CompanionPlayerState state = (CompanionPlayerState)[TVMixedCompanionManager sharedInstance].currentCompanionPlayerState;
        if ([TVMixedCompanionManager sharedInstance].isConnected == YES && (state == CompanionPlayerStatePlay || state == CompanionPlayerStatePause))
        {
            [self.companionView showCompanionPlayView];
        }
        [self.view addSubview:self.companionView];
    }
}

- (void)hideCompanionScreen {

    [self.companionView removeFromSuperview];
    if (isPhone)
    {
        
    }
}

#pragma mark - VOD Continue Popup

- (void)showContinuePopup {

    PlayerVODContinueWatchingPopUp *popup = nil;

    if (!APST.STBFollowMe)
    {
        popup = [[PlayerVODContinueWatchingPopUp alloc] initWithMedia:self.playerMediaItem continueFrom:[[_mediaMark objectForKey:@"nLocationSec"] floatValue]];
    }
    else
    {
        popup = [[PlayerVODContinueWatchingPopUp alloc] initWithMedia:self.playerMediaItem continueFrom:[[_mediaMark objectForKey:@"nLocationSec"] floatValue] STBcontinueFrom:150];
    }

    //self.mediaMark = nil;

    popup.delegate = self;

    popup.frame = self.view.bounds;

    [self.view addSubview:popup];

}


#pragma mark - PlayerSocialViewDelegate - PlayerVODInfoViewDelegate

- (void)intermediateLogin:(NSString*)segue {

    returnToViewAfterLogin = YES;

    if ([segue isEqualToString:@"fbLogin"]) {
        SocialManagement *sm = [SocialManagement new];
        sm.delegateForProvider = self;
        
        [[sm socialProviderById:SocialProviderTypeFacebook] aquirePermissions:^(BOOL success) {
            if (success){
                [FBM requestForGetFBUserDataWithToken:^(TVFacebookUser *user) {
                    LoginViewController *controller = [[ViewControllersFactory defaultFactory] loginViewController];
                    controller.isNotRoot = YES;
                    controller.segue = segue;
                    controller.fbUser = user;
                    [self.navigationController pushViewController:controller animated:YES];
                }];
            }
            
            if (![AnalyticsM.currentEvent.category length]) {
                [AnalyticsM.currentEvent clearFields];
                AnalyticsM.currentEvent.category = @"Login and registration";
                AnalyticsM.currentEvent.action = @"Facebook User Sign in";
                AnalyticsM.currentEvent.label = @"Login screen";
            }
        }];
    }
    else {
        LoginViewController *controller = [[ViewControllersFactory defaultFactory] loginViewController];
        controller.isNotRoot = YES;
        controller.segue = segue;
        controller.fbUser = nil;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - PlayerVODContinueWatchingPopUpDelegate

- (void)playerVODContinueWatchingStartOverPressed {

    _mediaMark = nil;

    //[self buttonPlayPressed];

    [self playerVODPlayFromTime:0];


}

- (void)playerVODContinueWatchingContinueFrom:(CGFloat)startTime {

    _mediaMark = nil;
    [self playerVODPlayFromTime:startTime];
}

- (void)playerVODPlayFromTime:(float)startTime {

    NSString *fileTypeFormatKey = [self bestFileformat];

    [self playMediaType:fileTypeFormatKey startTime:startTime ];
    [_playerVODDetailsView updatePlayButtonWithState:kVODButtonPlayngState];

}

- (void)playerVODContinueWatchingStartOverClosed {
    [_playerVODDetailsView updatePlayButtonWithState:kVODButtonPausedState];
    _mediaStarted = NO;
}

#pragma mark - LangViewDelegate


- (void)langAudioSelected:(NSInteger)index {


    self.selectedAudioIndex = index;

#ifdef PLAYER
    [self.videoPlayerControl changeAudioLanguageTo:index];
#endif

}

- (void)langSubSelected:(NSInteger)index {

    self.selectedSubIndex = index;

#ifdef PLAYER

    if (self.selectedSubIndex > 0) {
        [self.videoPlayerControl turnSubtitle:YES];
        [self.videoPlayerControl changeSubtitlesLanguageTo:index - 1];
    } else {
        [self.videoPlayerControl turnSubtitle:NO];
    }

#endif

}

- (void)langSelectionClose:(UIView *)view {

    if (isPhone) {
        [[view superview] removeFromSuperview];
        _popUpOpen = NO;
        [self startTimer];
    }

}

- (void)showLangScreen:(NSArray *)audioList selectedAudio:(NSInteger)selectedAudio subList:(NSArray *)subList selectedSub:(NSInteger)selectedSub {

    [self stopTimer];

    if (isPhone) {

        PlayerLangViewPhone *langView = [[PlayerLangViewPhone alloc] init];
        langView.frame = self.view.bounds;
        langView.langContentView.delegate = self;

        [langView.langContentView initContent:audioList selectedAudio:selectedAudio subList:subList selectedSub:selectedSub];

        [self.view addSubview:langView];
        _popUpOpen = YES;

    } else {

        if (!self.langPopUpView) {

            self.langPopUpView = [[PlayerLangViewPad alloc] init];
            self.langPopUpView.frame = CGRectMake(popUpLangViewPositionX, popUpLangViewPositionY, self.langPopUpView.frame.size.width, self.langPopUpView.frame.size.height);
            self.langPopUpView.langContentView.delegate = self;

            [self.langPopUpView.langContentView initContent:audioList selectedAudio:selectedAudio subList:subList selectedSub:selectedSub];

            self.langPopUpView.popUpRectView = self.langPopUpView.frame;

            self.langPopUpView.popUpSuperView = self.view;
            self.langPopUpView.popUpDelegate = self;
        }
        if (self.langPopUpView.superview) {

            [self closeLangPopup];

        } else {


            [self.langPopUpView.langContentView initContent:audioList selectedAudio:selectedAudio subList:subList selectedSub:selectedSub];

            if (![self.view.subviews containsObject:self.sharePopUpView] && ![self.view.subviews containsObject:self.soundSiderView]) {

                _buttonLang.selected = YES;
                self.currentPopUp = self.langPopUpView;
                self.currentPopUp.popUpDelegate = self;
                self.currentbtnOfPopUp = _buttonLang;
                [self.langPopUpView showPopUp];
            } else{

                [self.currentPopUp dismissPopUp];
                self.popUpToShow = self.langPopUpView;
                self.btnOfPopUpToShow = _buttonLang;
            }
            _popUpOpen = YES;
        }

    }


}

-(void) closeLangPopup{
    _buttonLang.selected = NO;
    [self.langPopUpView dismissPopUp];
    _popUpOpen = NO;
}

#pragma mark - Slider


-(void) setupSlider
{
    UIImage * progressThumb = [UIImage imageNamed:@"slider_thubm"];
    UIImage * referenceThumb = [UIImage imageNamed:@"slider_thumb_clear"];

    UIImage * progressMin = [[UIImage imageNamed:@"slider_progress_min"] imageTintedWithColor:APST.brandColor];
    progressMin = [progressMin resizableImageWithCapInsets:UIEdgeInsetsMake(0, progressMin.size.width-1, 0, 0)];
    UIImage * progressMax = [UIImage imageNamed:@"slider_progress_max"];
    progressMax = [progressMax resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,progressMax.size.width-1)];
    UIImage * referenceMin = [UIImage imageNamed:@"slider_reference_min"];
    referenceMin = [referenceMin resizableImageWithCapInsets:UIEdgeInsetsMake(0, referenceMin.size.width-1, 0, 0)];
    UIImage * referenceMax = [UIImage imageNamed:@"slider_reference_max"];
    referenceMax = [referenceMax resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,referenceMax.size.width-1)];

    [self.sliderReferenceLiveProgress setThumbImage:referenceThumb forState:UIControlStateNormal];
    [self.sliderReferenceLiveProgress setMaximumTrackImage:referenceMax forState:UIControlStateNormal];
    [self.sliderReferenceLiveProgress setMinimumTrackImage:referenceMin forState:UIControlStateNormal];


    [self.timeSlider setThumbImage:progressThumb forState:UIControlStateNormal];
    [self.timeSlider setMaximumTrackImage:progressMax forState:UIControlStateNormal];
    [self.timeSlider setMinimumTrackImage:progressMin forState:UIControlStateNormal];
}


- (IBAction)onTimeSliderChange:(UISlider *)sender {
#ifdef PLAYER

    [self restartTimer];

    if (self.playerMode != playerModeStartOver && self.playerMode != playerModeLIVE)
    {
        TimeStruct timeStc = [self getTimeFromFloat:_timeSlider.value];

        if (timeStc.hours > 0) {
            _labelCurrentTime.text = [NSString stringWithFormat:@"%i:%02i:%02i", timeStc.hours, timeStc.minutes, timeStc.seconds];
        } else {
            _labelCurrentTime.text = [NSString stringWithFormat:@"%02i:%02i", timeStc.minutes, timeStc.seconds];
        }

    }
    else
    {
        if (sender.value>self.sliderReferenceLiveProgress.value)
        {
            sender.value= self.sliderReferenceLiveProgress.value;
        }
    }

#endif
}

- (IBAction)onTimeSliderStart:(UISlider *)sender {

    _sliderChanges = YES;
}

- (IBAction)onTimeSliderEnd:(UISlider *)sender {


    _sliderChanges = NO;
#ifdef PLAYER



    if (self.playerMode == playerModeLIVE)
    {
        if (self.timeSlider.value < self.sliderReferenceLiveProgress.value)
        {
            // start over and seek to selected time
            [self clearOldAttributes];
            self.programToPlay = self.playedProgram;
            [self playMediaType:self.mediaToPlayInfo.currentFile.format startTime:sender.value];
        }
    }
    else
    {
        if (self.playerMode == playerModeStartOver && self.timeSlider.value >= self.sliderReferenceLiveProgress.value)
        {
            // you reach the bufffer of the live streaming
            [self clearOldAttributes];
            [self selectMediaItem:self.mediaToPlayInfo.mediaItem];
        }
        else
        {
            // catch up or vod or in startover but in the downloaded buffer.

            [_videoPlayerControl seekToTimeFloat:sender.value];

        }


        [self updateTimeSlider];
        [self updateTimeLabels];

    }



#endif
}


#ifdef PLAYER
- (TimeStruct)getTimeFromFloat:(float)time {

    TimeStruct returnTime;
    int seconds =  time;
    int minutes = (int)(seconds / 60);
    int hours = (int)(minutes / 60);
    minutes = minutes % 60;
    seconds = seconds % 60;
    returnTime.seconds = seconds;
    returnTime.minutes = minutes;
    returnTime.hours = hours;
    return returnTime;
}

#endif


#pragma mark - Buttons

- (IBAction)buttonLangPressed:(UIButton *)button {

    [self startTimer];
    NSArray* audios = [NSArray array];
    NSArray* subs = [NSArray array];

#ifdef PLAYER
    audios = [self.videoPlayerControl getAudioLanguagesList];
    subs = [self.videoPlayerControl getSubtitlesList];

    NSLog(@"audios %d   subs %d", (int)[audios count], (int)[subs count]);

#endif

    [self showLangScreen:audios selectedAudio:self.selectedAudioIndex subList:subs selectedSub:self.selectedSubIndex];

}

- (IBAction)buttonFullScreenPressed:(UIButton *)button {

    if (isPhone) {

        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        UIDevice *device = [UIDevice currentDevice];

        if ((orientation == UIInterfaceOrientationLandscapeLeft)||(orientation == UIInterfaceOrientationLandscapeRight))
        {
            if ([device respondsToSelector:@selector(setOrientation:)])
                [device setOrientation:UIInterfaceOrientationPortrait];

            [button setImage:[UIImage imageNamed:@"player_fullscreen_icon"] forState:UIControlStateNormal];
        }
        else
        {
            if ([device respondsToSelector:@selector(setOrientation:)])
                [device setOrientation:UIInterfaceOrientationLandscapeRight];
            [button setImage:[UIImage imageNamed:@"player_fullscreen_back_icon"] forState:UIControlStateNormal];
        }

    }else {

        if (self.fullscreenModeForPad) {
            self.fullscreenModeForPad = NO;
            [button setImage:[UIImage imageNamed:@"player_maximize_icon"] forState:UIControlStateNormal];
            self.viewTopControls.hidden = NO;
            self.viewBottomControls.hidden = NO;
            self.viewTopControls.alpha = 1.f;
            self.viewBottomControls.alpha = 1.f;
        }else {
            self.fullscreenModeForPad = YES;
            [button setImage:[UIImage imageNamed:@"player_minimize_icon"] forState:UIControlStateNormal];

#ifdef PLAYER
            if (self.videoPlayerControl.playerStatus == TVPPlaybackStateIsPlaying) {
                [self hideControlPanels];

                if ([self.view.subviews containsObject:_langPopUpView]){
                    [self closeLangPopup];
                }

                if ([self.view.subviews containsObject:_sharePopUpView]){
                    [self.sharePopUpView dismissPopUp];
                }

                if ([self.view.subviews containsObject:_soundSiderView]){
                    [self.soundSiderView dismissPopUp];
                }

            }
#else
            [self hideControlPanels];
            if ([self.view.subviews containsObject:_langPopUpView]){
                [self closeLangPopup];
            }

            if ([self.view.subviews containsObject:_sharePopUpView]){
                [self.sharePopUpView dismissPopUp];
            }

            if ([self.view.subviews containsObject:_soundSiderView]){
                [self.soundSiderView dismissPopUp];
            }
#endif

        }
    }
}

- (IBAction)buttonPlayPausePressed:(UIButton *)button {
    
    if (self.blockedUI == YES)
    {
        return;
    }

    if (!_mediaStarted) {

        [self buttonPlayPressed];

    }
    else
    {

        if (_buttonPlayPause.selected) {//Pause Player

            if (self.playerMode == playerModeLIVE)
            {
                self.lastPausePosition = self.timeSlider.value;
                self.lastProgramPlayed = self.playedProgram;
            }

            [_buttonPlayPause setImage:[UIImage imageNamed:@"player_play"] forState:UIControlStateDisabled];

            _buttonPlayPause.selected = NO;
            _buttonFullScreen.enabled = NO;

#ifdef PLAYER
            if (![self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:APST.TVCMediaFormatTrailer]) {
                [_playerVODDetailsView updatePlayButtonWithState:kVODButtonPausedState];
            }
            if (self.videoPlayerControl.playerStatus == TVPPlaybackStateIsPlaying) {

                [self.videoPlayerControl Pause];
                [self stopTimer];
                [self showEndOfShowScreen];
            }
#endif
        } else  {//Play Player

            
            [_buttonPlayPause setImage:[UIImage imageNamed:@"player_pause_icon"] forState:UIControlStateSelected];
            [_buttonPlayPause setImage:[UIImage imageNamed:@"player_pause_icon"] forState:UIControlStateDisabled];

            _buttonPlayPause.selected = YES;
            _buttonFullScreen.enabled = YES;

#ifdef PLAYER
            if (![self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:APST.TVCMediaFormatTrailer]) {
                [_playerVODDetailsView updatePlayButtonWithState:kVODButtonPlayngState];
            }

            if (self.videoPlayerControl.playerStatus != TVPPlaybackStateIsPlaying)
            {
                if (self.playerMode == playerModeLIVE)
                {
                    // start over and seek to selected time
                    self.programToPlay = self.lastProgramPlayed;
                    [self playMediaType:self.mediaToPlayInfo.currentFile.format startTime:self.lastPausePosition];
                }
                else
                {
                    [self.videoPlayerControl Play];
                    [self startTimer];
                    [self hideEndOfShowScreen];
                }
            }
#endif
        }
    }
}

- (void)showBlockAlert:(int)groupModeRule {
    NSString *text = [self getBlockAlertText:groupModeRule stringFormat:@"%@"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:text message:nil delegate:nil cancelButtonTitle:[LS(@"ok") uppercaseString] otherButtonTitles:nil];
    
    [alert show];
}

- (NSString*) getBlockAlertText:(int)groupModeRule stringFormat:(NSString*) stringFormat {
    NSString *text = [NSString stringWithFormat:@"player_block_type_subtitle_%d", groupModeRule];
    return [NSString stringWithFormat:stringFormat, LS(text)];
}

#pragma mark - PlayerVODDetailsViewDelegate

- (UINavigationController*) getNavigationController {
    return self.navigationController;
}

- (void)buttonResumWatchingPressed
{
    if (self.companionModeOverlay.isPresnted == YES)
    {
        [self companionModeOverlay:nil didSelectStopCompanion:nil];
    }
    
    else
    {
        [self buttonPlayPausePressed:nil];
    }
}

- (void) removeActivityIndicator {
    [self hideHUD];
}

- (void)buttonPlayPressed {

    //remove endofshow if presented
    [self hideEndOfShowScreen];

    if ([self menuContainerViewController].menuState != MFSideMenuStateClosed){
        return;
    }

    BOOL disableMediaCheck = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"  "] boolValue];
    if (![self canPlay]) {
        if ([LoginM isSignedIn]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(@"player_watch_with_subscription") message:nil delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
            [alert show];
            return;
        }
        else if (isPhone) {
            [self showLogin];
            return;
        }
        else {
            return;
        }
    }

    if (([_groupMediaRules count] || ([APP_SET isOffline] && ((KADownloadItem *)self.playerMediaItem.downloadItemRef).hasWatchRestriction)) && !disableMediaCheck)
    {
        if ([_groupMediaRules objectForKey:@"BlockType_AgeBlock"]) {

            [self showBlockAlert:BlockType_AgeBlock];
        } else if ([_groupMediaRules objectForKey:@"BlockType_GeoBlock"]) {

            [self showBlockAlert:BlockType_GeoBlock];

        } else if ([_groupMediaRules objectForKey:@"BlockType_Validation"] || ((KADownloadItem *)self.playerMediaItem.downloadItemRef).hasWatchRestriction) {

            _playerPinView.pinRuleID = [[_groupMediaRules objectForKey:@"BlockType_Validation"] integerValue];

            [self playerPinViewShowWithcompletion:^(BOOL isUnlocked, NSString *enteredCode) {

                if (isUnlocked) {
                    self.blockedUI = NO;
                    self.groupMediaRules = [NSDictionary dictionary];
                    _playerPinView.pinRuleID = 0;

                    if (![APP_SET isOffline]) {
                        [self.playerDTGAdapter updateParentalAccessCode:enteredCode];
                    }
                    [self showFollowMeIfNeededAndPlay];
                }

                [self playerPinViewClose];
                [self changePinViewHidden:YES];
            }];

        }
        return;
    }

    [self showFollowMeIfNeededAndPlay];
}

-(void) showFollowMeIfNeededAndPlay
{
    _mediaStarted = YES;
    _buttonPlayPause.enabled = YES;// ![_playerMediaItem isLive];

    [AppDelegate sharedAppDelegate].blockOrientationRotation = NO;


    NSString *fileTypeFormatKey = [self bestFileformat];

    float startTime = 0;

    if (_mediaMark) {
        startTime = [[_mediaMark objectForKey:@"nLocationSec"] floatValue];
    }

    if (startTime < [APST.followMeMinimalTime floatValue]) {
        startTime = 0;
    }

    if (startTime > 0) {
        [self showContinuePopup];
    }
    else
    {
        [self playMediaType:fileTypeFormatKey startTime:startTime ];
        _buttonFullScreen.enabled = YES;
#ifdef PLAYER
        if (![self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:APST.TVCMediaFormatTrailer])
            [_playerVODDetailsView updatePlayButtonWithState:kVODButtonPlayngState];
        else
            [_playerVODDetailsView updatePlayButtonWithState:kVODButtonShouldPlayState];
#endif

    }

}

- (void)buttonPlayTrailerPressed {

    if (isPad && _detailsContainerOpen) {
        [self setContainerDetailsOpenState:!_detailsContainerOpen];
    }
    [AppDelegate sharedAppDelegate].blockOrientationRotation = NO;
    [self playMediaType:APST.TVCMediaFormatTrailer startTime:0 ];

    if (self.itemsPricesWithCoupons.count)
        [_playerVODDetailsView updatePlayButtonWithState:kVODButtonShouldPlayState];
}

- (void)endTrailerUIUpdate {

    //    _viewPlayerContainer.hidden = YES;
    //    _playerVODInfoView.hidden = NO;

    [NavM setPhonePortrait];

    _labelCurrentTime.text = @"";
    _labelFullTime.text = @"";

    if (![_playerMediaItem isLive]) {

        if ([_playerMediaItem.metaData objectForKey:@"Runtime"] && [[_playerMediaItem.metaData objectForKey:@"Runtime"] length] > 0) {

            int min = (int)([[_playerMediaItem.metaData objectForKey:@"Runtime"] intValue]) / 60;
            if (min < 5) {
                min = [[_playerMediaItem.metaData objectForKey:@"Runtime"] intValue];
            }

            int hours = min / 60;
            int minutes = min % 60;

            _labelCurrentTime.text = @"00:00";

            if (hours > 0) {
                _labelFullTime.text = [NSString stringWithFormat:@"%i:%02i:%02i", hours, minutes, 0];
            } else {
                _labelFullTime.text = [NSString stringWithFormat:@"%02i:%02i", minutes, 0];
            }
        }
    }

    _timeSlider.value = 0;
    _timeSlider.maximumValue = 0;
}

- (IBAction)buttonBackPressed:(UIButton *)button {
    if (_playerMediaItem && _initialMediaItem && ![_playerMediaItem isEqual:_initialMediaItem]) {
        if (_arrPlayerMediasStack.count)
            [_arrPlayerMediasStack removeLastObject];

        if (_arrPlayerMediasStack.count) {
            [self selectMediaItem:[_arrPlayerMediasStack lastObject]];
        }
        else
            [self selectMediaItem:_initialMediaItem];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    //resume all downloads if not playing local file
    [self.playerDTGAdapter resumeAllDownloads];
}

-(void)updateMediaItemAndFavoriteButton:(TVMediaItem *)mediaItem
{

}

- (IBAction)buttonMenuPressed:(UIButton *)button {

    [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{}];

}

- (IBAction)buttonTabBarPressed:(UIButton *)button {

    button.enabled = NO;

    if (button.tag == 0) {
        _buttonTabRight.enabled = YES;
    } else {
        _buttonTabLeft.enabled = YES;
    }

    if (_playerMediaItem.isSeries) {

        _playerSeriesEpisodesView.hidden = _buttonTabRight.enabled;

    } else {

        _playerSocialView.hidden = _buttonTabRight.enabled;
        if (!_playerSocialView.hidden) {
            [_playerSocialView updateMediaFeed];

            _playerLiveDetailsView.alpha = 0.0;
            _playerVODDetailsView.alpha = 0.0;
            _playerNPVRInfoView.alpha = 0.0;

        } else {

            _playerNPVRInfoView.alpha = 1.0;
            _playerLiveDetailsView.alpha = 1.0;
            _playerVODDetailsView.alpha = 1.0;

        }
    }
}

- (IBAction)buttonSoundPressed:(UIButton *)button {

    [self startTimer];
    if (!self.soundSiderView) {

        self.soundSiderView = [[PlayerSoundSlider alloc]initWithFrame:CGRectMake(_buttonSound.x + 5, playerSoundSliderRect.origin.y, playerSoundSliderRect.size.width, playerSoundSliderRect.size.height) ];
        self.soundSiderView.delegate = self;
        self.soundSiderView.popUpSuperView = self.view;
        self.soundSiderView.popUpDelegate = self;
        //    [self.soundSiderView.soundSlider addTarget:self action:@selector(soundSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
    if (self.soundSiderView.superview) {

        button.selected = NO;
        [self.soundSiderView dismissPopUp];
        _popUpOpen = NO;
    }

    else {

        if (![self.view.subviews containsObject:_sharePopUpView] && ![self.view.subviews containsObject:_langPopUpView]) {

            button.selected = YES;
            self.currentPopUp = self.soundSiderView;
            self.currentPopUp.popUpDelegate = self;
            self.currentbtnOfPopUp = button;
            //[self stopFullScreenModeTimer];
            [self.soundSiderView showPopUp];
        } else{

            [self.currentPopUp dismissPopUp];
            self.popUpToShow = self.soundSiderView;
            self.btnOfPopUpToShow = button;
        }
        _popUpOpen = YES;
    }
}

- (NSAttributedString*) attributedActionButtonTitleForState:(VODSubscriptionButtonState)state
                                          subscriptionPrice:(TVSubscriptionData_prise*)subscriptionPrice
                                          ppvPrice:(TVPPVData_prise*)ppvPrice
                                               baseFontSize:(CGFloat)fontSize {
    NSMutableAttributedString* attributedDescription = nil;
    NSString* descriptionText = nil;
    switch (state) {
        case kVODSubscriptionButtonSVODsingle:
        case kVODSubscriptionButtonSVODmulti:
        {
            NSString *baseText = (state == kVODSubscriptionButtonSVODsingle) ? LS(@"item_page_buy_svod_single_button_title") : LS(@"item_page_buy_svod_multi_button_title");
            descriptionText = [NSString stringWithFormat:baseText,
                               subscriptionPrice.name,
                               @"",
                               LS(@"item_page_buy_subscription_period")];

            NSDictionary* attributes = @{NSForegroundColorAttributeName : APST.brandTextColor, NSFontAttributeName : Font_Bold(fontSize)};
            attributedDescription = [[NSMutableAttributedString alloc] initWithString:descriptionText attributes:attributes];

            NSRange strRange = [descriptionText rangeOfString:[NSString stringWithFormat:@"/%@", LS(@"item_page_buy_subscription_period")]];
            if (strRange.location != NSNotFound) {
                [attributedDescription setAttributes:@{NSForegroundColorAttributeName : APST.brandTextColor, NSFontAttributeName : Font_Reg(fontSize)} range:strRange];
            }
        }
            break;
        case kVODSubscriptionButtonTVODsingle:
        {
            NSString *itemPrice= [NSString stringWithFormat:@"%@", ppvPrice.name];
            descriptionText = [NSString stringWithFormat:LS(@"item_page_buy_tvod_single_button_title"),
                               itemPrice,
                               @""];
            NSDictionary* attributes = @{NSForegroundColorAttributeName : APST.brandTextColor, NSFontAttributeName : Font_Bold(fontSize)};
            attributedDescription = [[NSMutableAttributedString alloc] initWithString:descriptionText attributes:attributes];
        }
            break;

        case kVODSubscriptionButtonEmpty:
        {
            descriptionText = @"Not Available";

            NSDictionary* attributes = @{NSForegroundColorAttributeName : APST.brandTextColor, NSFontAttributeName : Font_Bold(fontSize)};
            attributedDescription = [[NSMutableAttributedString alloc] initWithString:descriptionText attributes:attributes];
        }
            break;
        case kVODSubscriptionButtonInitial:
        {
            descriptionText = LS(@"loading");
            NSDictionary* attributes = @{NSForegroundColorAttributeName : APST.brandTextColor, NSFontAttributeName : Font_Bold(fontSize)};
            attributedDescription = [[NSMutableAttributedString alloc] initWithString:descriptionText attributes:attributes];
        }
            break;

    }

    return attributedDescription;
}

-(void) updateCurrentMediaState:(VODButtonState) state {
    _currentMediaState = state;
}

-(BOOL) isMediaAllowedForDownloading:(TVMediaItem*) item {
    BOOL isAllowedPriceType = (!item.isFreeMedia || (item.isFreeMedia && [APST.dtgParams[@"allowDownloadForFreeItems"] boolValue] && APST.anonymousUserAllowedToPlayFreeItems));
    BOOL hasFileTypeForDownload = [item hasDownloadFile];
    if ([NU(UIUnit_Download_buttons) isUIUnitExist] && isAllowedPriceType && item && !item.isLive && hasFileTypeForDownload)
        return TRUE;
    return FALSE;
}

-(BOOL) isPlayingLocalFile
{
    return [self.playerDTGAdapter isItemDownloaded];
}

#pragma mark - Screen Orientation

-(void)menuStateChange :(NSNotification*)notification {

    if ([notification.name isEqualToString:MFSideMenuStateNotificationEvent])
    {
        NSDictionary* userInfo = notification.userInfo;
        if (userInfo){
            if ([userInfo objectForKey:@"eventType"] && [[userInfo objectForKey:@"eventType"] integerValue] == MFSideMenuStateEventMenuWillClose){
                if (!_viewPlayerContainer.hidden)
                {
                    [AppDelegate sharedAppDelegate].blockOrientationRotation = NO;
                }
            }
        }
    }

    if ([self menuContainerViewController].menuState == MFSideMenuStateRightMenuOpen || [self menuContainerViewController].menuState == MFSideMenuStateLeftMenuOpen)
        if ([notification.name isEqualToString:MFSideMenuStateNotificationEvent])
        {
            NSDictionary* userInfo = notification.userInfo;
            if (userInfo){
                if ([userInfo objectForKey:@"eventType"] && [[userInfo objectForKey:@"eventType"] integerValue] == MFSideMenuStateEventMenuWillClose){

                    [self startTimer];

                }
            }
        }

    if ([self menuContainerViewController].menuState == MFSideMenuStateClosed)
        if ([notification.name isEqualToString:MFSideMenuStateNotificationEvent])
        {
            NSDictionary* userInfo = notification.userInfo;
            if (userInfo){
                if ([userInfo objectForKey:@"eventType"] && [[userInfo objectForKey:@"eventType"] integerValue] == MFSideMenuStateEventMenuWillOpen){

                    [self stopTimer];

                }
            }
        }

}

- (BOOL)shouldAutorotate
{
    return YES;
}


- (NSUInteger)supportedInterfaceOrientations {

    return (isPhone ? ((_viewPlayerContainer.hidden && _playerPinView.hidden) ? UIInterfaceOrientationMaskPortrait : UIInterfaceOrientationMaskAll) : UIInterfaceOrientationMaskLandscape);

}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration {


    if (isPhone) {

        if (([self menuContainerViewController].menuState == MFSideMenuStateLeftMenuOpen)) {
            [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{}];
        } else if (([self menuContainerViewController].menuState == MFSideMenuStateRightMenuOpen)) {
            [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
        }

        [UIView animateWithDuration:duration animations:^{
            _viewRightOpen.alpha = (UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 1.0 : 0.0);
            _labelTitleFullscreen.alpha = (UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 1.0 : 0.0);
            _buttonLang.alpha = (UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? 1.0 : 0.0);
            self.buttonStartover.hidden = (UIInterfaceOrientationIsLandscape(toInterfaceOrientation) ? !(self.playerMode==playerModeStartOver || self.playerMode == playerModeLIVE || self.playerMode == playerModeCatchUp) : YES);

            if (self.buttonStartover.hidden != YES)
            {
                self.buttonStartover.UIUnitID = NU(UIUnit_live_startOver_buttons);
                [self.buttonStartover activateAppropriateRule];

            }
        }];


        if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {

            [_buttonFullScreen setImage:[UIImage imageNamed:@"player_fullscreen_back_icon"] forState:UIControlStateNormal];

            [UIView animateWithDuration:duration animations:^{
                _viewPlayerContainer.frame = CGRectMake(playerFullscreenX, playerFullscreenY, playerFullscreenWidth, playerFullscreenHeight);
                CGRect frame = self.recoredStatusView.frame;
                frame.origin.y =30;
                self.recoredStatusView.frame = frame;
            }];

            if ([self.companionView superview]) {

                self.companionView.frame = CGRectMake(playerFullscreenX, playerFullscreenY, playerFullscreenWidth, playerFullscreenHeight);

                [((CompanionViewPhone *)self.companionView) updateScreenSize];


            }

            _viewRightOpen.hidden = [APP_SET isOffline]?YES:NO;
            _viewRightOpen.alpha = _viewTopControls.alpha;
            self.noInternetView.frame = CGRectMake(0.0, 28.0, self.view.frame.size.width, self.noInternetView.frame.size.height);

            //            _buttonFullScreen.hidden = YES;

            if(self.recoredIconImage.hidden == NO)
            {
                UILabel *lab  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 500, 22)];
                lab.font = _labelTitleFullscreen.font;
                lab.text = _labelTitleFullscreen.text;
                CGSize textSize = [[lab text] sizeWithAttributes:@{NSFontAttributeName:[lab font]}];
                CGFloat strikeWidth = textSize.width;

                if (strikeWidth > _labelTitleFullscreen.frame.size.width)
                {
                    strikeWidth = _labelTitleFullscreen.frame.size.width;
                }
                CGRect frame = self.recoredIconImage.frame;
                frame.origin.x = self.labelTitleFullscreen.frame.origin.x + strikeWidth;
                frame.origin.y =15;
                self.recoredIconImage.frame = frame;
                self.recoredIconImage.alpha =1.0;
                [self.innerTopControls addSubview:self.recoredIconImage];
            }
            if (self.recoredIconButton.hidden == NO)
            {
                self.recoredIconButton.alpha = 0.0;
            }


        }
        else
        {
            if(self.recoredIconImage.hidden == NO)
            {
                self.recoredIconImage.alpha =0.0;
            }
            if (self.recoredIconButton.hidden == NO)
            {
                self.recoredIconButton.alpha = 1.0;
            }
            [_buttonFullScreen setImage:[UIImage imageNamed:@"player_fullscreen_icon"] forState:UIControlStateNormal];

            [UIView animateWithDuration:duration animations:^{
                _viewPlayerContainer.frame = CGRectMake(playerX, playerY, playerWidth, playerHeight);
            }];

            if ([self.companionView superview]) {

                self.companionView.frame = CGRectMake(playerX, 20, playerWidth, playerFullscreenWidth - 20);

                [((CompanionViewPhone *)self.companionView) updateScreenSize];
            }

            _viewRightOpen.hidden = YES;
            _viewRightOpen.alpha = _viewTopControls.alpha;
            self.noInternetView.frame = CGRectMake(0.0,64.0,self.noInternetView.frame.size.width,self.noInternetView.frame.size.height);
            _buttonFullScreen.hidden = NO;

        }
    }
}
#ifdef PLAYER
- (void)updateTimeLabels:(TimeStruct)currentTime totalTime:(TimeStruct)totalTime {
 
    if (_sliderChanges) return;
    
    //_timeSlider.userInteractionEnabled = YES;
    _timeSlider.alpha = 1.0;
    

    
    if (self.playerMode == playerModeTrailer || self.playerMode == playerModeVOD || self.playerMode == playerModeCatchUp)
    {
        if (currentTime.hours > 0) {
            _labelCurrentTime.text = [NSString stringWithFormat:@"%i:%02i:%02i", currentTime.hours, currentTime.minutes, currentTime.seconds];
        }
        else {
            _labelCurrentTime.text = [NSString stringWithFormat:@"%02i:%02i", currentTime.minutes, currentTime.seconds];
        }
        if (totalTime.hours > 0) {
            _labelFullTime.text = [NSString stringWithFormat:@"%i:%02i:%02i", totalTime.hours, totalTime.minutes, totalTime.seconds];
        }
        else {
            _labelFullTime.text = [NSString stringWithFormat:@"%02i:%02i", totalTime.minutes, totalTime.seconds];
        }
        
    }
    else
    {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"HH:mm"];
        
        if ( (self.playerMode == playerModeLIVE || self.playerMode == playerModeCatchUp || self.playerMode == playerModeStartOver ) && self.npvrPlayerAdapter == nil)
        {
            _labelCurrentTime.text = [df stringFromDate:self.playedProgram.startDateTime];
            _labelFullTime.text = [df stringFromDate:self.playedProgram.endDateTime];
        }
        else {
            _labelCurrentTime.text = [df stringFromDate:self.npvrPlayerAdapter.recordedAsset.startDate];
            _labelFullTime.text = [df stringFromDate:self.npvrPlayerAdapter.recordedAsset.endDate];
        }
    }
}
#endif

- (void)updateTimeLabels {
#ifdef PLAYER
    TimeStruct totalTime, currentTime;
    if (self.playerMode == playerModeTrailer || self.playerMode == playerModeVOD || self.playerMode == playerModeCatchUp) {
        totalTime = [_videoPlayerControl getPlaybackDurationInStructFormat];
        currentTime = [_videoPlayerControl getCurrentPlaybackInStructFormat];
    }
    
    [self updateTimeLabels:currentTime totalTime:totalTime];
#endif

}

- (void)updateTimeSlider {

    if (_sliderChanges) return;

#ifdef PLAYER

    if (self.playerMode == playerModeTrailer || self.playerMode == playerModeVOD || self.playerMode == playerModeCatchUp)
    {
        float time = [_videoPlayerControl getPlaybackDuration];
        _timeSlider.maximumValue = time;
        _timeSlider.value = [_videoPlayerControl getCurrentPlaybackTimeInSeconds];
        self.sliderReferenceLiveProgress.maximumValue = _timeSlider.maximumValue;
        self.sliderReferenceLiveProgress.value = 0;

    }
    else if (self.playerMode == playerModeStartOver)
    {
        if (self.npvrPlayerAdapter)
        {
            _timeSlider.maximumValue = [self.npvrPlayerAdapter.recordedAsset.endDate timeIntervalSinceReferenceDate] - [self.npvrPlayerAdapter.recordedAsset.startDate timeIntervalSinceReferenceDate];
            _timeSlider.value = [_videoPlayerControl getCurrentPlaybackTimeInSeconds];

            self.sliderReferenceLiveProgress.maximumValue = _timeSlider.maximumValue;
            self.sliderReferenceLiveProgress.value = -[self.npvrPlayerAdapter.recordedAsset.startDate timeIntervalSinceNow];
        }
        else
        {
            _timeSlider.maximumValue = [self.playedProgram.endDateTime timeIntervalSinceReferenceDate] - [self.playedProgram.startDateTime timeIntervalSinceReferenceDate];
            _timeSlider.value = [_videoPlayerControl getCurrentPlaybackTimeInSeconds];

            self.sliderReferenceLiveProgress.maximumValue = _timeSlider.maximumValue;
            self.sliderReferenceLiveProgress.value = -[self.playedProgram.startDateTime timeIntervalSinceNow];

        }

    }
    else if (self.playerMode == playerModeLIVE)
    {
        // take live program
        _timeSlider.maximumValue = [_playedProgram.endDateTime timeIntervalSinceReferenceDate] - [_playedProgram.startDateTime timeIntervalSinceReferenceDate];
        _timeSlider.value = -[_playedProgram.startDateTime timeIntervalSinceNow];

        self.sliderReferenceLiveProgress.maximumValue = _timeSlider.maximumValue;
        self.sliderReferenceLiveProgress.value = -[_playedProgram.startDateTime timeIntervalSinceNow];

    }

#endif
}


#pragma mark - TVCplayerStatusProtocol

#ifdef PLAYER

- (void)monitorMediaLocation:(CGFloat) currentLocation {
    if (self.playerDTGAdapter) {
        [self.playerDTGAdapter updateMediaMarkForLocationChange: currentLocation];
    }
}

- (void) monitorPlaybackTimeAtPlayer:(TVCplayer*)player {

    if (!_playerIsVisible) {

        [self stopPlayer];

        return;

    }

    if (player.playerStatus == TVPPlaybackStateIsPlaying) {


        if (_sliderChanges)
        {
            return;
        }

        if (self.playerMode == playerModeStartOver)
        {
            // if you are on start over mode and you have played all the program , we are switching you to the live stream.
            if (self.timeSlider.value>=self.timeSlider.maximumValue)
            {
                self.programToPlay = nil;
                self.npvrPlayerAdapter = nil;
                NSString * fileFormat = [self fileFormatToPlayForMedia:self.mediaToPlayInfo.mediaItem itemPrices:_itemsPricesWithCoupons];
                [self playMediaType:fileFormat startTime:0];
            }
        }

        [self updateTimeSlider];
        [self updateTimeLabels];

        [self hideEndOfShowScreen];

        //pause all downloads if not playing local file
        [self.playerDTGAdapter pauseAllDownloads];

    }

}

- (void)movieShouldStartPlayingWithMediaItem:(TVMediaItem*)mediaItem atPlayer:(TVCplayer*)player {

    NSLog(@"----- > > > movieShouldStartPlayingWithMediaItem  ----- > > > %@ ----- > >  %f",[NSDate date], ([NSDate timeIntervalSinceReferenceDate] * 1000));

    self.labelAutomation.accessibilityValue = @"Movie Start Playing";

    [self.watchAdapter sendWatchAction];
    NSLog(@"==movieShouldStartPlayingWithMediaItem");

    [self startTimer];

    _buttonPlayPause.selected = YES;
    [_buttonPlayPause setImage:[UIImage imageNamed:@"player_pause_icon"] forState:UIControlStateSelected];
    [_buttonPlayPause setImage:[UIImage imageNamed:@"player_pause_icon"] forState:UIControlStateDisabled];

    _mediaStarted = YES;

    NSArray* audios = [player getAudioLanguagesList];
    NSArray* subs = [player getSubtitlesList];

    NSLog(@"audios %ld   subs %ld", [audios count], [subs count]);

    if (([audios count] > 1) || ([subs count] > 0)) {

        for (NSString *str in audios) {
            NSLog(@"audio %@", str);
        }

        if ([self.mediaToPlayInfo.currentFile.format isEqualToString:APST.TVCMediaFormatTrailer]) {
            _buttonLang.enabled = NO;
        } else {
            _buttonLang.enabled = YES;
        }
    }

    if (isPhone) {
        [UIViewController attemptRotationToDeviceOrientation];
    }
}

-(void)movieFinishedPresentationWithMediaItem:(TVMediaItem *)mediaItem atPlayer:(TVCplayer*)player {
    NSLog(@"==movieFinishedPresentationWithMediaItem");

    [self hideHUD];
    [self stopPlaying];
    
#ifdef PLAYER
    TimeStruct totalTime = [_videoPlayerControl getPlaybackDurationInStructFormat];
    [self updateTimeLabels:totalTime totalTime:totalTime];
#endif

    _mediaStarted = NO;

#ifdef PLAYER
    if ([_mediaToPlayInfo.fileTypeFormatKey isEqualToString:APST.TVCMediaFormatTrailer]) {
        [self endTrailerUIUpdate];
    }
#endif

    [self showEndOfShowScreen];
}

- (void)playerDetectedConcurrentWithMediaItem:(TVMediaItem*)mediaItem atPlayer:(TVCplayer*)player {

    [self stopPlayer];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:LS(@"playback_denied_concurrency"),LoginM.concurrentDeviceLimit] message:nil delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
    [alert show];

}

- (void)player:(TVCplayer *)player didDetectError:(NSDictionary *)info
{
    [self hideHUD];
}


-(void) player:(TVCplayer *)player licenceExpiredForMediaToPlay:(TVMediaToPlayInfo *) mediaToPlayInfo
{
    [self stopPlayer];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:LS(@"rental_license_expired") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
    [alert show];
}

#endif
- (void)stopPlaying {
#ifdef PLAYER
    [self.videoPlayerControl Stop];
#endif
    [self stopTimer];
    
    _buttonPlayPause.selected = NO;
    [_buttonPlayPause setImage:[UIImage imageNamed:@"player_play"] forState:UIControlStateDisabled];

    [[ProximityChecker instance] stopChecking];
}

//- (void)showHUD;
//- (void)hideHUD;

-(void)movieHasFinishedBufferingForMediaItem:(TVMediaItem *)mediaItem {
    NSLog(@"==movieHasFinishedBufferingForMediaItem");
#ifdef PLAYER
    [self hideHUD];
#endif
}

-(void)movieIsBufferingForMediaItem:(TVMediaItem *)mediaItem {
    NSLog(@"==movieIsBufferingForMediaItem");
#ifdef PLAYER
    [self showHUDBlockingUI:NO];
#endif
}

#ifdef PLAYER

-(void)movieProcessFailedWithStatusError:(TVCDrmStatus)status withMediaItem:(TVMediaItem *)mediaItem atPlayer:(TVCplayer *)player
{
    NSLog(@"==movieProcessFailedWithStatusError");

    NSString * message = nil;
    switch (status) {
        case TVCDrmStatusPersonalization_Failed:
            message = LS(@"error_personalization_Failed");
            break;
        case TVCDrmStatusDownloadContent_Failed:
            message = LS(@"error_downloadContent_Failed");
            break;
        case TVCDrmStatusIsDRM_Failed:
            message = LS(@"error_isDRM_Failed");
            break;
        case TVCDrmStatusAquireRights_Failed:
            message = LS(@"error_aquireRights_Failed");
            break;
        case TVCDrmStatus_Interrupted:
            message = LS(@"error_player_interrupted");
            break;
        case TVCDrmStatus_IncorrectFileUrl:
            message = LS(@"error_incorrectFileUrl");
            break;
        case TVCDrmStatus_IncorrectMediaItemOrNull:
            message = LS(@"incorrectMediaItemOrNull");
            break;
        case TVCDrmStatus_IncorrectLicensedURL:
            message = LS(@"error_message_incorrectLicensedURL");
            break;
        case TVCDrmStatus_UNKNOWN:
            message = LS(@"unknown_error");
            break;
        case TVCDrmStatus_DeviceNotSecured:
            message = LS(@"device_not_secured_error");
            break;
        default:
            break;
    }


    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"player_error_title") message:message delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];

    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {

    }];

}
#endif


- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {

    if ([_itemsPricesWithCoupons count] == 0) {

        if (operation == UINavigationControllerOperationPop && [toVC isKindOfClass:[LoginViewController class]]) {
            return [LoginAnimatorPop new];
        } else if (operation == UINavigationControllerOperationPush && [fromVC isKindOfClass:[LoginViewController class]]) {
            return [LoginAnimatorPush new];
        }

    } else{

        if (operation == UINavigationControllerOperationPop && [toVC isKindOfClass:[LoginViewController class]]) {
            return [LoginAnimatorPush new];
        } else if (operation == UINavigationControllerOperationPush && [fromVC isKindOfClass:[LoginViewController class]]) {
            return [LoginAnimatorPop new];
        }
    }

    return nil;
}

#ifdef PLAYER
-(void) playerDetectHeadPhpnesPullOut:(TVCplayer *) player
{
    [self buttonPlayPausePressed:nil];
}

#endif


#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark -  playerLiveDetails view delegate methods

-(void) playerLiveDetails:(PlayerViewController *) sender didSelectPlayProgram:(APSTVProgram *) program forChannel:(TVMediaItem *) channel
{
    [self openMediaItem:channel withProgram:program];
}


#pragma mark - Trick play Actions
- (IBAction)replayButtonPressed:(id)sender {
    [self hideEndOfShowScreen];

    if (self.playerMode == playerModeCatchUp || self.playerMode == playerModeStartOver) {
#ifdef PLAYER
        if (self.npvrPlayerAdapter)
        {
            [self playMediaType:nil startTime:0];
        }
        else
        {
            NSString * format = [self fileFormatToPlayForMedia:self.mediaToPlayInfo.mediaItem itemPrices:_itemsPricesWithCoupons];
            [self playMediaType:format startTime:0];
        }
#endif
    }
    else if (self.playerMode == playerModeLIVE) {
#ifdef PLAYER
        self.programToPlay = self.playedProgram;
        NSString * format = [self fileFormatToPlayForMedia:self.mediaToPlayInfo.mediaItem itemPrices:_itemsPricesWithCoupons];
        [self playMediaType:format startTime:0];
#endif
    }
}

#pragma mark - objects
-(void)toggleComapnionDiscovery
{

    if (!self.isCompanionDiscoveryOpned)
    {
        [self stopTimer];
    }
    else
    {
        [self startTimer];
    }

    [super toggleComapnionDiscovery];

}
#pragma mark - Swoosh - 

-(void)swiped:(UISwipeGestureRecognizer *)gesture
{
#ifdef PLAYER

    if ([NU(UIUnit_Companion_play) isUIUnitExist])
    {
        if (self.videoPlayerControl.playerStatus == TVPPlaybackStateIsPlaying && [[TVMixedCompanionManager sharedInstance] isConnected])
        {
            typeof(self) weakSelf = self;
            BOOL isCanSwoosh = [self canCastMedia:self.mediaToPlayInfo.mediaItem];
            if (!isCanSwoosh && [self.mediaToPlayInfo.currentFile.format isEqualToString:APST.TVCMediaFormatTrailer] == NO)
            {
                BOOL panelsHidden = (self.viewTopControls.hidden && self.viewBottomControls.hidden);

                if (panelsHidden)
                {
                    [self screenTaped:nil];
                    [self showTopBannerWithText:LS(@"chromecast_playready_failure_fileNotExists") duration:DBL_MAX];
                }
                else
                {
                    [self hideTopBannerWithCompletion:^{
                        typeof(weakSelf) strongSelf = weakSelf;
                        [strongSelf showTopBannerWithText:LS(@"chromecast_playready_failure_fileNotExists") duration:DBL_MAX];
                    }];
                }
                return;
            }
            [self stopTimer];

            APSTVProgram * programToSwoosh = nil;
            BOOL isVOD = YES;

            if (self.playerMode == playerModeLIVE || self.playerMode == playerModeCatchUp || self.playerMode == playerModeStartOver)
            {
                programToSwoosh = self.playedProgram;
                isVOD = NO;
            }

            self.mediaToPlayInfo.mediaItem.price = PlayerCDA.securedSiteGUID;
            NSMutableDictionary * dic = [NSMutableDictionary dictionary];

            NSArray * arr = [NSArray array];
            arr = [self.videoPlayerControl getAudioLanguagesList];
            if ([arr count]>0)
            {
                NSArray *langNames = [self convertIsoToLangStringArray:arr];
                if ([langNames count] > 0 &&  self.selectedAudioIndex < [langNames count])
                {
                    NSString * select = [langNames objectAtIndex:self.selectedAudioIndex];
                    if (select)
                    {
                        [dic setObject:select forKey:@"selectLang"];
                    }
                }
            }
            if (PlayerCDA.securedSiteGUID)
            {
                [dic setObject:PlayerCDA.securedSiteGUID forKey:@"securedSiteGUID"];
            }

            self.mediaToPlayInfo.mediaItem.advertisingParameters = [[NSDictionary alloc] initWithDictionary:dic];
            
            [TVMixedCompanionManager sharedInstance].casstingMediaItem = self.mediaToPlayInfo.mediaItem;
            self.playerStreamLocationBeforeCasting = [self.videoPlayerControl getCurrentPlaybackTime];
            if (isVOD)
            {
                [[TVMixedCompanionManager sharedInstance] sendToConnectedDeviceVodMedia:self.mediaToPlayInfo.mediaItem withFile:self.mediaToPlayInfo.currentFile currentProgress:self.videoPlayerControl.getCurrentPlaybackTimeInSeconds withCompletionBlock:^(CompanionHandlerResult result) {
                    NSLog(@"test");
                    typeof(weakSelf) strongSelf = weakSelf;
                    if (result == CompanionHandlerResultOk)
                    {
                        [TVMixedCompanionManager sharedInstance].currentUIState = Castting;
                        if(strongSelf.companionView != nil) // is open!
                        {
                            //change UI in main thread !!!
                            dispatch_async(dispatch_get_main_queue(), ^{
                                // do work here
                                typeof(weakSelf) strongSelf = weakSelf;
                                [strongSelf.companionView showCompanionPlayView];
                                [strongSelf updateForCasting];
                            });
                        }
                        else
                        {
                            //change UI in main thread !!!
                            dispatch_async(dispatch_get_main_queue(), ^{
                                // do work here
                                typeof(weakSelf) strongSelf = weakSelf;
                                [strongSelf updateForCasting];
                                
                            });
                        }

                    } else if (result == CompanionHandlerResultFailure) {
                        [strongSelf showCompanionConnectionErrorBanner];
                    }
                }];
            }
            else
            {
                NSString * playbackType = @"live";
                if ([self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:TVCMediaFormat_CatchUp])
                {
                    playbackType = @"catch_up";
                }
                else if ([self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:TVCMediaFormat_StartOver])
                {
                    playbackType = @"start_over";
                }
                else if ([self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:TVCMediaFormat_PauseAndPlay])
                {
                    playbackType = @"pltv";
                }
                self.playerStreamLocationBeforeCasting = 0;
                
                [[TVMixedCompanionManager sharedInstance]  sendToConnectedDeviceLiveMedia:self.mediaToPlayInfo.mediaItem withFile:self.mediaToPlayInfo.currentFile andProgram:[programToSwoosh EPGProgram] forPlaybackMode:playbackType progress:0 withCompletionBlock:^(CompanionHandlerResult result) {
                    NSLog(@"test");
                    typeof(weakSelf) strongSelf = weakSelf;
                    
                    if (result == CompanionHandlerResultOk)
                    {
                        [TVMixedCompanionManager sharedInstance].currentUIState = Castting;
                        if(strongSelf.companionView != nil) // is open!
                        {
                            //change UI in main thread !!!
                            dispatch_async(dispatch_get_main_queue(), ^{
                                // do work here
                                typeof(weakSelf) strongSelf = weakSelf;
                                [strongSelf.companionView showCompanionPlayView];
                                [strongSelf updateForCasting];
                            });
                        }
                        else
                        {
                            //change UI in main thread !!!
                            dispatch_async(dispatch_get_main_queue(), ^{
                                // do work here
                                typeof(weakSelf) strongSelf = weakSelf;
                                [strongSelf updateForCasting];
                                
                            });
                        }
                    }
                    else if (result == CompanionHandlerResultFailure)
                    {
                        [strongSelf showCompanionConnectionErrorBanner];
                    }
                }];
            }
        }
    }
#endif
}

- (void)showCompanionConnectionErrorBanner
{
    BOOL oldBannerHide = (self.topBanner.alpha < .1f);
    typeof(self) weakSelf = self;
    [self hideTopBannerWithCompletion:^{
        typeof(weakSelf) strongSelf = weakSelf;
        NSTimeInterval showTime = 3;
        NSString *oldBannerText = ([self.topBanner.text length]) ? self.topBanner.text : nil;
        [strongSelf showTopBannerWithText:LS(@"companion_swoosh_failed") duration:DBL_MAX];
        
        if (!oldBannerHide && oldBannerText) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, showTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf hideTopBannerWithCompletion:^{
                    [strongSelf showTopBannerWithText:oldBannerText duration:DBL_MAX];
                }];
            });
        }
    }];
}

- (void)updateForCasting
{
    [TVMixedCompanionManager sharedInstance].currentCompanionPlayerState = CompanionPlayerStatePlay;
    
    [self grayOutSlider:YES];

    if (self.companionModeOverlay == nil)
    {
        self.companionModeOverlay = [CompanionModeOverlay companionModeOverlay];
        self.companionModeOverlay.delegate = self;
    }
    
    [self.companionModeOverlay animateInOnView:self.viewPlayerContainer  withCompletion:nil];
    
	self.langButtonEnabledBeforeSwoosh = _buttonLang.enabled;
    _buttonLang.enabled = NO;

    [self stopTimer];
   
    
    if (self.buttonPlayPause.selected)
    {
        [self buttonPlayPausePressed:nil];
        [self stopPlayer];
        if (isPhone)
        {
            if (self.topBanner.superview)
            {
                [self hideTopBannerWithCompletion:^{
                    [self showTopAndBottomBanners];
                }];
            }
            else
            {
                [self showTopAndBottomBanners];
            }
        }
    }
}

-(BOOL)canCastMedia:(TVMediaItem *)media
{
    TVAbstractCompanionDevice * device = [[ TVMixedCompanionManager sharedInstance]getCurrentConnectedDevice];
    if( [device getType] == 0x3015)
    {
        BOOL mediaHaveIsmFile = NO;
        NSArray * filesArray = media.files;
        for (TVFile * file in filesArray)
        {
            NSString * str = [NSString stringWithFormat:@"%@",file.fileURL];
            NSRange  range = [str rangeOfString:@".ism"];
            if (range.length >0)
            {
                mediaHaveIsmFile = YES;
                break;
            }
        }
        return mediaHaveIsmFile;
    }
    else // is not chrome cast!
    {
        return YES;
    }
}

-(void)showTopAndBottomBanners
{
    self.viewTopControls.hidden = NO;
    self.viewBottomControls.hidden = NO;
    self.viewTopControls.alpha = 1.f;
    self.viewBottomControls.alpha = 1.f;
    [self.viewPlayerContainer bringSubviewToFront: self.viewBottomControls];
    [self.viewPlayerContainer bringSubviewToFront: self.viewTopControls];
}

#pragma mark - CompanionModeOverlayDelegate

-(void) companionModeOverlay:( CompanionModeOverlay *) sender didSelectStopCompanion:(UIButton *) button
{
    [self grayOutSlider:NO];

    [self startTimer];

#ifdef PLAYER
    [self.companionModeOverlay animateOutWithCompletion:^{
        
        if ([[TVMixedCompanionManager sharedInstance] isConnected])
        {
            if (self.topBanner.superview == nil)
            {

                [self showTopBannerWithText:[self getCompanionBannerText] duration:DBL_MAX];
            }
        }
       else
       {
           if (self.topBanner.superview)
           {
               [self hideTopBanner];
           }
       }
        [self startTimer];
        _buttonLang.enabled = self.langButtonEnabledBeforeSwoosh;
        
#ifdef PLAYER
        [self playMediaType:self.mediaToPlayInfo.currentFile.format startTime:self.playerStreamLocationBeforeCasting];
#endif
        [self buttonPlayPausePressed:nil];
    }];

#endif
}

#pragma mark - watch list actions -

- (void)addToWatchList {

    if ([LoginM isSignedIn])
    {
        __block PlayerViewController *sself = self;
        [PurchasesM addMediaToWatchList:[self.playerMediaItem.mediaID integerValue] delegate:self completion:^(BOOL success, NSError *error) {
            if (success) {
                [sself updateActionButtonForState:kVODButtonAddedToWatchListState];
            }
        }];
    }
    else
    {
        [self showLogin];
    }
}

- (void)removeFromWatchList {
    __block PlayerViewController *sself = self;
    [PurchasesM removeMediaFromWatchList:[self.playerMediaItem.mediaID integerValue] delegate:self completion:^(BOOL success, NSError *error) {
        if (success) {
            [sself updateActionButtonForState:kVODButtonAddToWatchListState];
        }
    }];
}

#pragma mark - purchase flow -

- (void)initializePurchaseFlow:(id) purchasingElement {
    if ([purchasingElement isKindOfClass:[TVSubscriptionData class]]) {
        TVSubscriptionData *subscriptionData = (TVSubscriptionData*)purchasingElement;
        //show loading indicator
        [self showHUDBlockingUI:YES];

        [IAP purchaseSubscription:^(SubscriptionPurchaseRequestParams *reqParams) {
            reqParams.subscriptionData = subscriptionData;
            reqParams.productCode = subscriptionData.productCode;
            reqParams.delegate = self;
        } completion:^(SubscriptionResponseParams *resParams) {
            //hude loading indicator
            [self hideHUD];
            
            if (resParams.success)
            {
                [self updateActionButtonForState:kVODButtonShouldPlayState subscriptionsPurchased:@[subscriptionData] subscriptionsToPurchase:nil];
            }
            else
            {
                [self showPurchasingError:resParams.error withPurchasingElement:purchasingElement];
            }
        }];
    }
    else if ([purchasingElement isKindOfClass:[PPVFullData class]]) {
        PPVFullData *fullData = (PPVFullData*)purchasingElement;
        //show loading indicator
        [self showHUDBlockingUI:YES];
        
        [IAP purchasePPVMediaFile:^(PPVPurchaseRequestParams *reqParams) {
            reqParams.ppvData = fullData;
            reqParams.productCode = fullData.rentalInfo.productCode;
            reqParams.delegate = self;
        } completion:^(PPVResponseParams *resParams) {
            //hude loading indicator
            [self hideHUD];
            
            if (resParams.success)
            {
                [self updateActionButtonForState:kVODButtonShouldPlayState subscriptionsPurchased:@[fullData] subscriptionsToPurchase:nil];
            }
            else
            {
                [self showPurchasingError:resParams.error withPurchasingElement:purchasingElement];
            }
        }];
    }
}

-(void)showPurchasingError:(NSError *)error withPurchasingElement:(id) purchasingElement
{
    NSString *errorDesc = (error) ? [NSString stringWithFormat:LS(@"item_page_buy_subscription_error_description_with_reason"), error.localizedDescription] : LS(@"item_page_buy_subscription_error_description");
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"item_page_buy_subscription_error_title")
                                                        message:errorDesc
                                                       delegate:nil
                                              cancelButtonTitle:LS(@"cancel")
                                              otherButtonTitles:LS(@"try_again"), nil];
    
    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        switch (buttonIndex) {
            case 1:
                [self initializePurchaseFlow:purchasingElement];
                break;
            default:
                break;
        }
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }];
}

#pragma mark - download to go
-(void) buttonDownloadPressed {
    if (self.itemsPricesWithCoupons.count) {
        if ([self.playerDTGAdapter isInitialDownloadState] && [_groupMediaRules count]) {
            if ([_groupMediaRules objectForKey:@"BlockType_AgeBlock"]) {
                NSString *text = [self getBlockAlertText:BlockType_AgeBlock stringFormat:LS(@"dtg_download_block_title")];
                [[ErrorView sharedInstance] showMessageOnTarget:text image:nil target:[self getContainerView]];
            }
            else if ([_groupMediaRules objectForKey:@"BlockType_GeoBlock"]) {
                NSString *text = [self getBlockAlertText:BlockType_GeoBlock stringFormat:LS(@"dtg_download_block_title")];
                [[ErrorView sharedInstance] showMessageOnTarget:text image:nil target:[self getContainerView]];            
            }
            else if ([_groupMediaRules objectForKey:@"BlockType_Validation"]) {

                _playerPinView.pinRuleID = [[_groupMediaRules objectForKey:@"BlockType_Validation"] integerValue];
                [self playerPinViewShowWithcompletion:^(BOOL isUnlocked, NSString *enteredCode) {

                    if (isUnlocked) {
                        self.groupMediaRules = [NSDictionary dictionary];
                        _playerPinView.pinRuleID = 0;

                        [self.playerDTGAdapter updateParentalAccessCode:enteredCode];
                        [self.playerDTGAdapter buttonDownloadPressed];
                    }

                    self.blockedUI = NO;
                    [self playerPinViewClose];
                    [self changePinViewHidden:YES];
                }];
            }
        }
        else {
            [self.playerDTGAdapter buttonDownloadPressed];
        }
    }
}

#pragma mark - PlayerDTGAdapterDelegate delegates
- (TVMediaItem*) getMediaItem {
    return self.playerMediaItem;
}

- (TVMediaItem*) getInitialMediaItem {
    return self.playerMediaItem;
}

- (VODButtonState) getCurrentMediaState {
    return self.currentMediaState;
}

- (UIView*) getContainerView {
    return self.noInternetView;
}

- (void) prepareToUpdateDownloadStateForListeners:(void(^)(YLProgressBar* downloadProgress, UIButton* downloadButton, UIImageView* downloadStatusImage, UIView* target))completion {
    if (isPad) {
        completion(self.playerVODInfoView.dtgProgress, self.playerVODInfoView.dtgButton, self.playerVODInfoView.dtgStatusImage, self.playerVODInfoView);
    }

    completion(self.playerVODDetailsView.dtgProgress, self.playerVODDetailsView.dtgButton, self.playerVODDetailsView.dtgStatusImage, self.playerVODDetailsView);
}

#pragma mark - top banner -

-(void)changeTopBannerText:(NSString *) text
{
    self.topBanner.font = [UIFont boldSystemFontOfSize:isPhone?12:14];
    self.topBanner.numberOfLines = 0;
    self.topBanner.text = text;
    self.topBanner.textColor = [UIColor whiteColor];
    CGSize size =[self.topBanner sizeThatFits:CGSizeMake(self.view.width,self.view.height)];
    NSInteger high = isPad? 42:(size.height+8);
    self.topBanner.size = CGSizeMake(self.view.size.width, high);
    self.topBanner.backgroundColor = APST.brandColor;
    self.topBanner.textAlignment = NSTextAlignmentCenter;
    self.topBanner.autoresizingMask = UIViewAutoresizingFlexibleWidth;
}

- (void) showTopBannerWithText:(NSString *) text duration:(NSTimeInterval) timeInterval
{
    self.topBanner = [self createBannerWithText:text];

    if (isPhone)
    {
        [self.viewTopControls addSubview:self.topBanner];
        self.topBanner.alpha=0;

        [UIView animateWithDuration:0.3 animations:^{

            self.innerTopControls.y += self.topBanner.size.height;
            self.viewTopControls.height += self.topBanner.size.height;
            self.topBanner.alpha=1;
        } completion:^(BOOL finished)
         {
             if (timeInterval!= DBL_MAX)
             {
                 [self performSelector:@selector(hideTopBanner) withObject:nil afterDelay:timeInterval];
             }

         }];

    }
    else
    {

        self.topBanner.y = self.viewTopControls.height;
        self.topBanner.alpha=0;
        [self.viewTopControls addSubview:self.topBanner];

        [UIView animateWithDuration:0.3 animations:^{
            self.topBanner.alpha=1;
        } completion:^(BOOL finished)
         {
             if (timeInterval!= DBL_MAX)
             {
                 [self performSelector:@selector(hideTopBanner) withObject:nil afterDelay:timeInterval];
             }

         }];

    }
}

- (void) hideTopBanner
{
    if (isPhone)
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.topBanner.alpha = 0;
            self.innerTopControls.y -= self.topBanner.size.height;
            self.viewTopControls.height -= self.topBanner.size.height;
        } completion:^(BOOL finished)
         {
             [self.topBanner removeFromSuperview];
         }];

    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.topBanner.alpha=0;
        } completion:^(BOOL finished)
         {
             [self.topBanner removeFromSuperview];
         }];
    }
}



-(NSString *) getCompanionBannerText
{
    TVAbstractCompanionDevice * connectedDevice = [[TVMixedCompanionManager sharedInstance] getCurrentConnectedDevice];
    return [NSString stringWithFormat:LS(@"companion_PlayerBannerText"),connectedDevice.name];
}

-(void)chromeCastForceDisconnect:(NSNotification *)notifcation
{
    if (isPhone)
    {
        if ([[TVMixedCompanionManager sharedInstance] isConnected])
        {
            [[TVMixedCompanionManager sharedInstance] disconnect];
        }
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // do work here
//            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:LS(@"chromecast") message:LS(@"chromecast_casting_otherAppTakeover") delegate:nil cancelButtonTitle:LS(@"close") otherButtonTitles: nil];
//            [alert show];
//            
//        });
    }
}

-(void) companionDeviceUnPaired:(NSNotification *) notification
{
    self.buttonCompanion.selected = NO;
    if (self.companionModeOverlay.isPresnted == YES)
    {
        [self companionModeOverlay:nil didSelectStopCompanion:nil];
    }
    else
    {
        if (self.topBanner.superview)
        {
            [self hideTopBanner];
        }
    }
}

-(void) companionDeviceStop:(NSNotification *) notification
{
    if (self.companionModeOverlay.isPresnted == YES)
    {
        [self companionModeOverlay:nil didSelectStopCompanion:nil];
    }
    else  if (self.topBanner.superview == nil && [[TVMixedCompanionManager sharedInstance] isConnected])
    {
        [self showTopBannerWithText:[self getCompanionBannerText] duration:DBL_MAX];
        _buttonLang.enabled = self.langButtonEnabledBeforeSwoosh;
    }
}

-(void) companionDevicePaired:(NSNotification *) notification
{
    self.buttonCompanion.selected = YES;
    [self showTopBannerWithText:[self getCompanionBannerText] duration:DBL_MAX];
}


#pragma mark - recored icons -

-(void)showRecoredIcon:(BOOL)isSeries
{
    if (isSeries)
    {
        self.recoredIconImage.image = [UIImage imageNamed:@"record_series_icon"];
        [self.recoredIconButton setImage:[UIImage imageNamed:@"record_series_icon"] forState:UIControlStateNormal];
    }
    else
    {
        self.recoredIconImage.image = [UIImage imageNamed:@"record_singleItem_icon"];
        [self.recoredIconButton setImage:[UIImage imageNamed:@"record_singleItem_icon"] forState:UIControlStateNormal];

    }
    self.recoredIconImage.hidden = NO;
    self.recoredIconButton.hidden = NO;
    if (isPhone)
    {
        self.recoredIconImage.alpha=0;
        self.recoredIconButton.alpha = 1;
    }
}

-(void)hideRecoredIcon
{
    if (isPhone)
    {
        self.recoredIconImage.alpha=0;
        self.recoredIconButton.alpha = 0;
    }
    self.recoredIconImage.hidden = YES;
    self.recoredIconButton.hidden = YES;
}

#pragma mark - open my zone differently from other pages -

- (void)openMyZone {

    if (isPad) {
        if (!super.myZoneViewController){
            [self stopTimer];
        }

        if (_buttonShare.selected) [self buttonSharePressed:_buttonShare];
        if (_buttonSound.selected) [self buttonSoundPressed:_buttonSound];
        if (self.langPopUpView) [self closeLangPopup];
    }

    [super openMyZone];

}

#pragma mark - NPVRDetailesViewDelegate -

-(void)playerNPVRInfoView:(PlayerNPVRInfoView *)sender didSelectStartOver:(UIButton *)startOver
{
    [self playMediaType:nil startTime:0];
}

-(void) clearOldAttributes
{
    self.npvrPlayerAdapter = nil;
    self.programToPlay = nil;
}

#pragma application states -

- (void)appDidBecomeActive:(NSNotification *)notification {
    NSLog(@"did become active notification");
    if (self.playerMode != playerModeNone)
    {
        [self screenTaped:nil];
    }
}

- (void)appDidEnterForeground:(NSNotification *)notification {
    NSLog(@"did enter foreground notification");
}

- (void)appWillResignActive:(NSNotification *)notification {
    NSLog(@"appWillResignActive");
#warning - Stop Player on incoming call or pressing Home button

    if (_buttonPlayPause.enabled && _buttonPlayPause.selected) {
        [self buttonPlayPausePressed:_buttonPlayPause];
    }

}

- (BOOL) canPlay {
    BOOL retValue = NO;
    if (LoginM.isSignedIn) {
        if (self.npvrPlayerAdapter)
        {
            retValue = YES;
        }
        else if ([APP_SET isOffline])
        {
            retValue = YES;
        }
        else
        {
            if (_itemsPricesWithCoupons.count) {
                retValue = _itemsPricesWithCoupons.count;
            }
            else if (APST.hasInAppPurchases) {
                retValue = hasPurchasedSubscriptions;
            }
        }

    }
    else {
        if (self.playerMediaItem.priceType == TVPriceTypeFree && APST.anonymousUserAllowedToPlayFreeItems == YES)
            retValue = YES;
    }
    return retValue;
}

-(void) showLogin
{
    returnToViewAfterLogin = YES;

    self.navigationController.delegate = self;
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"playerVODPaidContent" forKey:@"loginPoint"];
    [[NSNotificationCenter defaultCenter] postNotificationName:KLoginNotification
                                                        object:nil
                                                      userInfo:userInfo];
}


-(NSString *) bestFileformat {
    NSString *fileTypeFormatKey = APST.TVCMediaFormatMainSD;
    if ([_itemsPricesWithCoupons count]) {
        if ([_itemsPricesWithCoupons objectForKey:APST.TVCMediaFormatMainHD]) {
            fileTypeFormatKey = APST.TVCMediaFormatMainHD;
        } else {
            fileTypeFormatKey = APST.TVCMediaFormatMainSD;
        }
    }
    else
    {
        if ([_itemsPricesWithCouponsNotPurchase count]) {
            
            if ([_itemsPricesWithCouponsNotPurchase objectForKey:APST.TVCMediaFormatMainHD]) {
                fileTypeFormatKey = APST.TVCMediaFormatMainHD;
            } else {
                fileTypeFormatKey = APST.TVCMediaFormatMainSD;
            }
        }
    }
    
    return fileTypeFormatKey;
}

-(void)showDeleteRecoredTostStatusViewToProgram:(APSTVProgram *) epgProgram recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus mediaItem:(TVMediaItem *) mediaItem isSeries:(BOOL)isSeries
{
    if (self.recoredStatusPopupView ==  nil)
    {
        self.recoredStatusPopupView = [[CustomViewFactory defaultFactory] epgRcoredStatusView];
    }
    if (isPad)
    {
        self.recoredStatusView.hidden = NO;
        CGRect frame = self.recoredStatusView.frame;
        frame.origin.y = 20;
        self.recoredStatusView.frame = frame;
    }
    else
    {
        self.recoredStatusView.hidden = NO;
        CGRect frame = self.recoredStatusView.frame;
        frame.origin.y =65;
        self.recoredStatusView.frame = frame;
    }
    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewDeleteRecoredSecsses ViewOnView:self.recoredStatusView isSeries:isSeries];
    }
    else
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewDeleteRecoredFailed ViewOnView:self.recoredStatusView isSeries:isSeries];
    }
}

-(void)showRecoredTostStatusViewToProgram:(APSTVProgram *) epgProgram recoredAdapterStatus:(RecoredAdapterStatus)recoredAdapterStatus mediaItem:(TVMediaItem *) mediaItem recoredHelper:(RecoredHelper *)rec
{
    if (self.recoredStatusPopupView ==  nil)
    {
        self.recoredStatusPopupView = [[CustomViewFactory defaultFactory] epgRcoredStatusView];
    }
    if (isPad)
    {
        self.recoredStatusView.hidden = NO;
        CGRect frame = self.recoredStatusView.frame;
        frame.origin.y =20;
        self.recoredStatusView.frame = frame;
    }
    else
    {
        self.recoredStatusView.hidden = NO;
        CGRect frame = self.recoredStatusView.frame;
        frame.origin.y =65;
        self.recoredStatusView.frame = frame;
    }
    if (recoredAdapterStatus == RecoredAdapterStatusOk)
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewRcoredingSecsses ViewOnView:self.recoredStatusView isSeries:rec.isSeries];
    }
    else
    {
        [self.recoredStatusPopupView addEPGRcored:epgProgram status:EPGRcoredStatusViewRcoredingFailed ViewOnView:self.recoredStatusView isSeries:rec.isSeries];
    }
}


-(void) setupConstraints
{

    [APP_SET isOffline]?[self.buttonShare removeFromSuperview]:(isPad?[self.viewPanel addSubview:self.buttonShare]:[self.innerTopControls addSubview:self.buttonShare]);

    if (isPad)
    {
        NSArray * buttons = [APP_SET isOffline]?@[self.buttonFullScreen,self.buttonSound,self.buttonLang,self.sepratorView,self.buttonInfo]:@[self.buttonFullScreen,self.buttonSound,self.buttonLang,self.buttonShare,self.sepratorView,self.buttonInfo];

        [self addConstrainsForlinearLayoutRightToLeft:buttons inSuperView:self.viewPanel space:14];
        [self.sepratorView addConstraint:[NSLayoutConstraint constraintWithItem:self.sepratorView
                                                                      attribute:NSLayoutAttributeWidth
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1.0f constant:1]];
        

        [self.sepratorView addConstraint:[NSLayoutConstraint constraintWithItem:self.sepratorView
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1.0f constant:self.sepratorView.height]];

        [self.seekBarPlaceHolder removeAllConstraints];
        
        if([APP_SET isOffline])
        {
            self.seekBarPlaceHolder.translatesAutoresizingMaskIntoConstraints = NO;
            [self.seekBarPlaceHolder.superview addConstraint:[NSLayoutConstraint constraintWithItem:self.seekBarPlaceHolder attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.seekBarPlaceHolder.superview attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0]];
            [self.seekBarPlaceHolder.superview addConstraint:[NSLayoutConstraint constraintWithItem:self.seekBarPlaceHolder attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.seekBarPlaceHolder.superview attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0]];
            [self.seekBarPlaceHolder addConstraint:[NSLayoutConstraint constraintWithItem:self.seekBarPlaceHolder
                                                                                attribute:NSLayoutAttributeWidth
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:nil
                                                                                attribute:NSLayoutAttributeNotAnAttribute
                                                                               multiplier:1.0f constant:self.seekBarPlaceHolder.width]];

            [self.seekBarPlaceHolder addConstraint:[NSLayoutConstraint constraintWithItem:self.seekBarPlaceHolder
                                                                                attribute:NSLayoutAttributeHeight
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:nil
                                                                                attribute:NSLayoutAttributeNotAnAttribute
                                                                               multiplier:1.0f constant:self.seekBarPlaceHolder.height]];
        }
        else
        {
            self.seekBarPlaceHolder.translatesAutoresizingMaskIntoConstraints = NO;

            [self.seekBarPlaceHolder.superview addConstraint:[NSLayoutConstraint constraintWithItem:self.seekBarPlaceHolder attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.seekBarPlaceHolder.superview attribute:NSLayoutAttributeLeadingMargin multiplier:1.0f constant:93]];

            [self.seekBarPlaceHolder.superview addConstraint:[NSLayoutConstraint constraintWithItem:self.seekBarPlaceHolder.superview attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.seekBarPlaceHolder attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0]];

            [self.seekBarPlaceHolder addConstraint:[NSLayoutConstraint constraintWithItem:self.seekBarPlaceHolder
                                                                                attribute:NSLayoutAttributeWidth
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:nil
                                                                                attribute:NSLayoutAttributeNotAnAttribute
                                                                               multiplier:1.0f constant:self.seekBarPlaceHolder.width]];

            [self.seekBarPlaceHolder addConstraint:[NSLayoutConstraint constraintWithItem:self.seekBarPlaceHolder
                                                                                attribute:NSLayoutAttributeHeight
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:nil
                                                                                attribute:NSLayoutAttributeNotAnAttribute
                                                                               multiplier:1.0f constant:self.seekBarPlaceHolder.height]];

        }
    }
    else
    {
        [_buttonCompanion activateAppropriateRule];
        _buttonCompanion.hidden = [APP_SET isOffline]?YES:_buttonCompanion.hidden;
        NSArray * buttons = [APP_SET isOffline]?@[self.buttonLang,self.buttonStartover]:@[self.buttonShare,self.buttonLang,self.buttonStartover];
        [self addConstrainsForlinearLayoutRightToLeft:buttons inSuperView:self.innerTopControls space:20];
    }
}

- (NSArray*) convertIsoToLangStringArray:(NSArray*) isoArray
{
    // I get the iSO array in the following format:
    //    -audio_482_eng, English (UK)
    //    -audio_483_eng  English (AU)
    
    NSMutableArray* result = [NSMutableArray arrayWithCapacity:isoArray.count];
    
    for (int i = 0; i< isoArray.count; i++) {
        // take last 3 letters of each string
        NSString* isoString = [isoArray objectAtIndex:i];
        NSString* iso639 = [isoString length]>=3 ? [isoString substringFromIndex:[isoString length]-3] : isoString;
        if (iso639.length == 3) {
            /* "PSC : ONO : OPF-1492 VFGTVONES-524 - iOS/iPhone/iPad/Canales/Audio/Audio is in original version for all channels*/
            
            if([iso639 isEqualToString:@"org"])
            {
                iso639 = @"org";
            }
            if(iso639!= nil)
            {
                /* "PSC : ONO : OPF-1329 - App crashes when pressing the language button */
                // Make sure that we will not set index that is not exsist
                [result addObject:iso639];
            }
        }
    }
    return result;
}

- (void) hideTopBannerWithCompletion:(void (^)()) completion
{
    if (isPhone)
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.topBanner.alpha = 0;
            self.innerTopControls.y -= self.topBanner.size.height;
            self.viewTopControls.height -= self.topBanner.size.height;
        } completion:^(BOOL finished)
         {
             [self.topBanner removeFromSuperview];
             if (completion)
             {
                 completion();
             }
         }];
        
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.topBanner.alpha=0;
        } completion:^(BOOL finished)
         {
             [self.topBanner removeFromSuperview];
             if (completion)
             {
                 completion();
             }
         }];
    }
}

-(void)grayOutSlider:(BOOL)flag
{
    
    UIImage * progressThumb = [UIImage imageNamed:@"slider_thubm"];
    UIImage * referenceThumb = [UIImage imageNamed:@"slider_thumb_clear"];
    
    UIImage * progressMin = [[UIImage imageNamed:@"slider_progress_min"] imageTintedWithColor:APST.brandColor];
    progressMin = [progressMin resizableImageWithCapInsets:UIEdgeInsetsMake(0, progressMin.size.width-1, 0, 0)];
    UIImage * progressMax = [UIImage imageNamed:@"slider_progress_max"];
    progressMax = [progressMax resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,progressMax.size.width-1)];
    UIImage * referenceMin = [UIImage imageNamed:@"slider_reference_min"];
    referenceMin = [referenceMin resizableImageWithCapInsets:UIEdgeInsetsMake(0, referenceMin.size.width-1, 0, 0)];
    UIImage * referenceMax = [UIImage imageNamed:@"slider_reference_max"];
    referenceMax = [referenceMax resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,referenceMax.size.width-1)];
    
    if (flag)
    {
        [self.timeSlider setThumbImage:referenceThumb forState:UIControlStateNormal];
        [self.timeSlider setMaximumTrackImage:referenceMax forState:UIControlStateNormal];
        [self.timeSlider setMinimumTrackImage:referenceMin forState:UIControlStateNormal];
    }
    else
    {
        [self.timeSlider setThumbImage:progressThumb forState:UIControlStateNormal];
        [self.timeSlider setMaximumTrackImage:progressMax forState:UIControlStateNormal];
        [self.timeSlider setMinimumTrackImage:progressMin forState:UIControlStateNormal];
    }
}

@end
