//
//  EndOfShowView_iphoneVF.m
//  Vodafone
//
//  Created by Admin on 07.10.15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "EndOfShowView_iphoneVF.h"

@implementation EndOfShowView_iphoneVF

#define kButtonHeight @70
#define kButtonWidth @93

- (void) createButtonsForLiveMedia {
    
    UIButton *btnBackToLive= [self createButtonOnTarget:self text:LS(@"player_dialog_end_of_show_back_to_live") image:[UIImage imageNamed:@"endofshow_backtolive"] needSelectionColors:FALSE clickBlock:^(UIButton *btn) {
        [self.delegate buttonBackToLivePressed];
    }];
    UIButton *btnStartOver= [self createButtonOnTarget:self text:LS(@"player_dialog_end_of_show_start_over") image:[UIImage imageNamed:@"endofshow_startover"] needSelectionColors:FALSE clickBlock:^(UIButton *btn) {
        [self.delegate buttonStartOverPressed];
    }];
    btnStartOver.hidden=YES;
    /* "PSC : ONO : SYN-4027 Live - Return from background "back to live" button is not centered */
    NSDictionary *metrics = @{@"b1width": @0,
                              @"b2width": @([kButtonWidth integerValue]+17),
                              @"b3width": @0,
                              @"b1height": kButtonHeight,
                              @"b2height": kButtonHeight,
                              @"b3height": kButtonHeight,
                              @"space": @0};
    UIView *middleEmptyView = [self createMiddleEmptyViewOnTatget:self];
    [self updateUIWithButtons:@[middleEmptyView,btnBackToLive, middleEmptyView] metrics:metrics];
}

@end
