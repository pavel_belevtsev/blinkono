//
//  PlayerViewControllerVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "PlayerViewControllerVF.h"
#import "SocialManagement.h"
#import "PlayerVODInfoView.h"
#import "EndOfShowView.h"
#import "UIImage+Tint.h"
#import "UIView+RemoveConstraints.h"

#import <AVFoundation/AVFoundation.h>



@interface PlayerViewControllerVF ()<FavoriteAdapterDelegate,TVCplayerStatusProtocol>

@end

@implementation PlayerViewControllerVF

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.labelTitle.textColor = [UIColor whiteColor];
    [self registerToAudioSessionRouteChangeNotification];
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
/* "PSC : Vodafone Grup : add Favorit button to Player screen: JIRA ticket. */

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateViewItemsBackgroundColorAndTextColor];
    
    if (isPhone)
    {
        
        [self buildFavoriteButtonPhone];
    }
    else
    {
        [self buildFavoriteButtonPad];
    }
}

/* "PSC : Vodafone Grup : update Favorit button : JIRA ticket. */
- (void)selectMediaItem:(TVMediaItem *)mediaItem;
{
    [super selectMediaItem:mediaItem];
    if (isPhone)
    {
        [self updateFavoritButtonLocate];
        /* "PSC : Vodafone Group : SYN-4036 : VFGTVONES-155 - iPhone/Player/Player Audio/Subtitles selector missing on portraite mode */
        self.buttonLang.alpha = 1.0;
    }
    
    if (self.buttonFavorite != nil)
    {
        [self.favoriteAdapter  setupMediaItem:self.playerMediaItem];
    }
}

-(void)updateFavoritButtonLocate
{
    CGRect langFrame = self.buttonLang.frame;
    CGRect favFrame = self.buttonFavorite.frame;
    
    if ([self.playerMediaItem isLive])
    {
        langFrame = self.buttonStartover.frame;
    }
    favFrame.origin.y = 1;// self.buttonLang.frame.origin.y;
    favFrame.origin.x = langFrame.origin.x -(favFrame.size.width);
    self.buttonFavorite.frame  = favFrame;
    self.buttonFavorite.alpha = 1.0;
}

-(void)setupFavoritButton
{
    self.buttonFavorite = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.viewPanel addSubview:self.buttonFavorite];
    self.favoriteAdapter = [[FavoriteAdapter alloc] initWithMediaItem:self.playerMediaItem control:self.buttonFavorite];
    
    self.favoriteAdapter.delegate = self;
    //            self.shareContentView.favoriteAdapter.delegate =self;
    self.viewEndOfShow.endOfShowFavoriteAdapter.delegate = self;
    
    self.shareContentView.buttonFavorite.hidden = YES;
    self.shareContentView.labelFavorite.hidden = YES;
}

/* "PSC : Vodafone Grup : setup DRM : JIRA ticket. */
-(void)buildCustomData
{
    NSString * groupID = [TVConfigurationManager sharedTVConfigurationManager].groupID;
    if (groupID)
    {
        NSString * customDataStr = [NSString stringWithFormat:@"%@|%@|%@",PlayerCDA.securedSiteGUID,groupID,self.mediaToPlayInfo.currentFile.fileID];
        [self.mediaToPlayInfo setCustomData:customDataStr];
    }
}

-(void)updateIsClearContent
{
#ifdef PLAYER
    self.mediaToPlayInfo.isClearContent = NO;
#endif
}

-(void)updateViewItemsBackgroundColorAndTextColor
{
    self.viewTabBar.backgroundColor = [UIColor clearColor];
    self.viewTabBarBg.backgroundColor = [UIColor clearColor];
    
    // adjust the button tabs:
    [self.buttonTabLeft setBackgroundColor: [UIColor colorWithRed:26.0/255.0 green:26.0/255.0 blue:26.0/255.0 alpha:1.0]];
    [self.buttonTabRight setBackgroundColor: [UIColor colorWithRed:26.0/255.0 green:26.0/255.0 blue:26.0/255.0 alpha:1.0]];
    
    [self.buttonTabLeft setBackgroundImage:nil forState:UIControlStateNormal];
    [self.buttonTabLeft setBackgroundImage:[UIImage imageNamed:@"tab_highlight_background"] forState:UIControlStateDisabled];
    
    [self.buttonTabLeft setTitleColor:[VFTypography instance].grayScale5Color  forState:UIControlStateDisabled];
    [self.buttonTabLeft setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    
    [Utils setButtonFont:self.buttonTabLeft bold:YES];
    //  [self.buttonTabLeft.titleLabel setFont:[UIFont fontWithName:self.buttonTabLeft.titleLabel.font.fontName size:18]];
    
    [self.buttonTabRight setBackgroundImage:nil forState:UIControlStateNormal];
    [self.buttonTabRight setBackgroundImage:[UIImage imageNamed:@"tab_highlight_background"] forState:UIControlStateDisabled];
    
    [self.buttonTabRight setTitleColor:[VFTypography instance].grayScale5Color forState:UIControlStateDisabled];
    [self.buttonTabRight setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    
    [Utils setButtonFont:self.buttonTabRight bold:YES];
    //[self.buttonTabRight.titleLabel setFont:[UIFont fontWithName:self.buttonTabLeft.titleLabel.font.fontName size:18]];
    
    
    self.labelRightOpen.backgroundColor = [VFTypography instance].grayScale2Color;
    self.labelRightOpen.textColor = [VFTypography instance].grayScale6Color;
}

-(void)buildFavoriteButtonPhone
{
    if (self.buttonFavorite == nil)
    {
        self.buttonFavorite = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.buttonFavorite setFrame:CGRectMake(0, 0, self.buttonLang.frame.size.width, self.buttonLang.frame.size.height)];
        
        self.favoriteAdapter = [[FavoriteAdapter alloc] initWithMediaItem:self.playerMediaItem control:self.buttonFavorite label:nil];
        self.shareContentView.buttonFavorite.hidden = YES;
        self.shareContentView.labelFavorite.hidden = YES;
        
        self.favoriteAdapter.delegate = self;
        self.viewEndOfShow.endOfShowFavoriteAdapter.delegate = self;
        if ( self.shareContentView.buttonFavorite.hidden == NO)
        {
            self.shareContentView.favoriteAdapter.delegate =self;
        }

    }
    self.buttonFavorite.enabled = [[TVSessionManager sharedTVSessionManager] isSignedIn];
    
    [self.favoriteAdapter  setupMediaItem:self.playerMediaItem];
    
    [self addFavoriteImagesPhone];
    
    CGRect buttonBeforeFrame = self.buttonLang.frame;
    CGRect favFrame = self.buttonFavorite.frame;
    
    if ([self.playerMediaItem isLive])
    {
        buttonBeforeFrame = self.buttonStartover.frame;
    }
    if (self.playerMediaItem == nil)
    {
        self.buttonFavorite.alpha = 0;
        [self performSelector:@selector(updateFavoritButtonLocate) withObject:nil afterDelay:1.0];
    }
  //  favFrame.origin.y = self.buttonLang.frame.origin.y;
    NSInteger widht = 60;
    favFrame.origin.x = buttonBeforeFrame.origin.x -(widht);
    self.buttonFavorite.frame  = favFrame;
    
    [self.buttonFavorite setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin];
    
    [self.innerTopControls addSubview:self.buttonFavorite];
    [self.innerTopControls layoutSubviews];
}

-(void)addFavoriteImagesPhone
{
    UIImage * image = [UIImage imageNamed:@"player_add_phone"];
    if ([self.playerMediaItem isLive])
    {
        image = [UIImage imageNamed:@"player_bookmark_icon"];
    }
    [self.buttonFavorite setImage:image forState:UIControlStateNormal];
    UIImage * imageSelected = [UIImage imageNamed:@"player_add_phone"];
    if ([self.playerMediaItem isLive])
    {
        imageSelected = [UIImage imageNamed:@"player_bookmark_icon"];
    }
    [self.buttonFavorite setImage:[[imageSelected imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
}

-(void)buildFavoriteButtonPad
{
    if (self.buttonFavorite == nil)
    {
        self.buttonFavorite = [UIButton buttonWithType:UIButtonTypeCustom];
        self.buttonFavorite.translatesAutoresizingMaskIntoConstraints = NO;
        
       // [self.buttonFavorite setFrame:CGRectMake(0, 0, self.buttonLang.frame.size.width+10, self.buttonLang.frame.size.height)];
        
        
        
        self.favoriteAdapter = [[FavoriteAdapter alloc] initWithMediaItem:self.playerMediaItem control:self.buttonFavorite label:nil];
        self.shareContentView.buttonFavorite.hidden = YES;
        self.shareContentView.labelFavorite.hidden = YES;
        
        self.favoriteAdapter.delegate = self;
        self.viewEndOfShow.endOfShowFavoriteAdapter.delegate = self;
        if ( self.shareContentView.buttonFavorite.hidden == NO)
        {
            self.shareContentView.favoriteAdapter.delegate =self;
        }
    
//        [self.viewPanel addSubview:self.buttonFavorite];
        
        [self.view layoutSubviews];
    }
    [self.viewPanel removeAllConstraints];

    [self.favoriteAdapter  setupMediaItem:self.playerMediaItem];
    [self.viewPanel addSubview:self.buttonFavorite];
    NSDictionary *metrics = @{@"width": @(48),
                              @"height": @(48)};
 

    
//    CGRect shareFrame = self.buttonShare.frame;
//    CGRect favFrame = self.buttonFavorite.frame;
//    favFrame.origin.y = shareFrame.origin.y;
//    favFrame.origin.x = shareFrame.origin.x - (15 + favFrame.size.width);
//    
//    self.buttonFavorite.frame  = favFrame;
//
//    [self updateOtherObjectInLineFrame];
    
    NSArray * buttons = [APP_SET isOffline]?@[self.buttonFullScreen,self.buttonSound,self.buttonLang,self.buttonFavorite,self.sepratorView,self.buttonInfo]:@[self.buttonFullScreen,self.buttonSound,self.buttonLang,self.buttonShare,self.buttonFavorite,self.sepratorView,self.buttonInfo];
    
    [self addConstrainsForlinearLayoutRightToLeft:buttons inSuperView:self.viewPanel space:14];
    [self.sepratorView addConstraint:[NSLayoutConstraint constraintWithItem:self.sepratorView
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1.0f constant:1]];
    
    [self.sepratorView addConstraint:[NSLayoutConstraint constraintWithItem:self.sepratorView
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1.0f constant:self.sepratorView.height]];
    
    [self.viewPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_buttonFavorite(width)]" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_buttonFavorite)]];
    [self.viewPanel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_buttonFavorite(height)]" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_buttonFavorite)]];
    
    [self.view layoutSubviews];

    self.buttonFavorite.enabled = [[TVSessionManager sharedTVSessionManager] isSignedIn];
    
    [self addFavoriteImagesPad];
    
    
//   // [self.buttonFavorite setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin];
//    [self.view layoutSubviews];
//
//    [self updateOtherObjectInLineFrame];

}

-(void)updateOtherObjectInLineFrame
{
    CGRect seperatorFrmae = self.sepratorView.frame;
    seperatorFrmae.origin.x -= self.buttonFavorite.frame.size.width;
    self.sepratorView.frame = seperatorFrmae;
    [self.view layoutSubviews];

    CGRect infoButtomFrame = self.buttonInfo.frame;
    infoButtomFrame.origin.x -= self.buttonFavorite.frame.size.width;
    self.buttonInfo.frame = infoButtomFrame;
    [self.view layoutSubviews];

    CGRect lableTitleFrame = self.labelTitle.frame;
    lableTitleFrame.size.width -= self.buttonFavorite.frame.size.width;
    self.labelTitle.frame = lableTitleFrame;
    [self.view layoutSubviews];

}

-(void)addFavoriteImagesPad
{
    UIImage * image = [UIImage imageNamed:@"player_add"];
    if ([self.playerMediaItem isLive])
    {
        image = [UIImage imageNamed:@"player_bookmark_icon"];
    }
    [self.buttonFavorite setImage:image forState:UIControlStateNormal];
    UIImage * imageSelected = [UIImage imageNamed:@"player_add"];
    if ([self.playerMediaItem isLive])
    {
        imageSelected = [UIImage imageNamed:@"player_bookmark_icon"];
    }
    [self.buttonFavorite setImage:[[imageSelected imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
}

// "PSC : Vodafone Group :SYN-3579 -> After i finish playing a movie , the button is stuck on "playing now" /
-(void)movieFinishedPresentationWithMediaItem:(TVMediaItem *)mediaItem atPlayer:(TVCplayer*)player
{
    [super movieFinishedPresentationWithMediaItem:mediaItem atPlayer:player];
    [self.playerVODDetailsView updatePlayButtonWithState:kVODButtonShouldPlayState];
}

/* "PSC : Vodafone Group : SYN-4036 : VFGTVONES-155 - iPhone/Player/Player Audio/Subtitles selector missing on portraite mode */
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if (isPhone) {
        [UIView animateWithDuration:duration animations:^{
            self.buttonLang.alpha = 1.0;
        }];
    }
}

// "PSC : Vodafone Ono : OPF-1598 VFGTVONES-539 - iOS/iPhone/iPad/Player/Headphones/Player pauses and there's no way to resume playback
-(void)registerToAudioSessionRouteChangeNotification
{
    [self unRegisterToAudioSessionRouteChangeNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioSessionChange:) name:AVAudioSessionRouteChangeNotification object:nil];
}

-(void)unRegisterToAudioSessionRouteChangeNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
}

#pragma mark - Resume Watch -
-(void) audioSessionChange:(NSNotification *) notification
{
    // test if headphone was unplugged
    // Get array of current audio outputs (there should only be one)
    
    NSArray *currentOutputs = [[AVAudioSession sharedInstance] currentRoute].outputs;
    AVAudioSessionPortDescription *currentPortDescription = [currentOutputs firstObject];
    
    AVAudioSessionRouteDescription *previousRouteDescription = [notification.userInfo objectOrNilForKey:AVAudioSessionRouteChangePreviousRouteKey];
    AVAudioSessionPortDescription *previousPortDescription = [previousRouteDescription.outputs firstObject];
    BOOL portChanged = ![previousPortDescription.portType isEqualToString:currentPortDescription.portType];
    
    if (portChanged && ![currentPortDescription.portType isEqualToString:AVAudioSessionPortHeadphones])
    {
        if (self.buttonPlayPause.enabled && self.buttonPlayPause.selected) {
            [self buttonPlayPausePressed:self.buttonPlayPause];
        }
    }
}

#ifdef PLAYER
-(void) playerDetectHeadPhpnesPullOut:(TVCplayer *) player
{
    
}

-(void)movieHasFinishedBufferingForMediaItem:(TVMediaItem *)mediaItem{
    [super movieHasFinishedBufferingForMediaItem:mediaItem];
    [self.videoPlayerControl changeAudioLanguageTo:self.selectedAudioIndex];
}
#endif
@end
