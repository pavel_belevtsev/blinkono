//
//  CoachMarksView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "CoachMarksView.h"
#import "UIImage+Tint.h"
#import "UIImageView+WebCache.h"

@interface CoachMarksView()


- (void)setLblHeader:(UILabel*)lbl withText:(NSString*)text;
- (void)setLblDes:(UILabel*)lbl withText:(NSString*)text;

@end


const CGFloat TVNCoachMarksHeaderFontSize = 24.0f;
const CGFloat TVNCoachMarksDesFontSize = 18.0f;
const CGFloat TVNCoachMarksAnimationDuration = 0.3f;
const CGFloat TVNCoachMarksViewBgImageAlpha = 0.2f;

@implementation CoachMarksView

- (id)initWithMediaItem:(TVMediaItem*)mediaItem
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    if (self) {
        
        [self.imgBg sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:mediaItem andSize:CGSizeMake(_imgBg.frame.size.width, _imgBg.frame.size.height)] placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

        }];

        self.imgBg.alpha = TVNCoachMarksViewBgImageAlpha;
        
        self.brandImageView.image = [self.brandImageView.image imageTintedWithColor:APST.brandColor];
        [self.btnClose setImage:[self.btnClose.imageView.image imageTintedWithColor:APST.brandColor] forState:UIControlStateNormal];
        [self setLblHeader:self.lblPersonalZoneHeader withText:LS(@"coach_marks_personal_zone")];
        [self setLblDes:self.lblPersonalZoneDes withText:LS(@"coach_marks_personal_zone_details")];
 
        [self setLblHeader:self.lblMainMenuHeader withText:LS(@"coach_marks_main_menu")];
        [self setLblDes:self.lblMainMenuDes withText:LS(@"coach_marks_main_menu_details")];
        
        [self setLblHeader:self.lblInfoBuzzHeader withText:LS(@"coach_marks_info_and_buzz")];
        [self setLblDes:self.lblInfoBuzzDes withText:LS(@"coach_marks_info_and_buzz_details")];
        if (!mediaItem || mediaItem.isLive) {
            [self setLblHeader:self.lblChannelsListHeader withText:LS(@"coach_marks_channels_list")];
            [self setLblDes:self.lblChannelsListDes withText:LS(@"coach_marks_channels_list_details")];
        }
        else{
            [self setLblHeader:self.lblChannelsListHeader withText:LS(@"coach_marks_related")];
            [self setLblDes:self.lblChannelsListDes withText:LS(@"coach_marks_related_details")];
        }
    }
    return self;
}

- (void)setLblHeader:(UILabel*)lbl withText:(NSString*)text {
    lbl.font = Font_Bold(TVNCoachMarksHeaderFontSize);
    lbl.textColor = APST.brandColor;
    lbl.text = text;
}

- (void)setLblDes:(UILabel*)lbl withText:(NSString*)text  {
    lbl.font = Font_Reg(TVNCoachMarksDesFontSize);
    lbl.textColor = [UIColor whiteColor];
    lbl.alpha = 0.9f;
    lbl.numberOfLines = 0;
    lbl.text = text;
}

-(void)show {
    UIView *rootView = [[[[[UIApplication sharedApplication] windows] objectAtIndex:0] subviews] lastObject];
	
    CGRect fram = self.frame;
    fram.origin.y += 20;
    self.frame = fram;
    self.statusBarSpaceForIOS7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 20)];
    self.statusBarSpaceForIOS7.backgroundColor = [UIColor blackColor];
    [rootView addSubview:self.statusBarSpaceForIOS7];

	
    [rootView addSubview:self];
    self.alpha = 0.0f;
    [UIView animateWithDuration:TVNCoachMarksAnimationDuration animations:^{
        self.alpha = 1.0f;
    }];
}

-(IBAction)close:(id)sender {
    [UIView animateWithDuration:TVNCoachMarksAnimationDuration animations:^{
        self.alpha = 0.0f;
    } completion:^(BOOL finished){
        [self removeFromSuperview];
		
        [self.statusBarSpaceForIOS7 removeFromSuperview];
		
        [self.delegate didDismissCoachMarksView];
    }];
}

@end
