//
//  PlayerSoundSlider.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#ifdef PLAYER
#import <MediaPlayer/MediaPlayer.h>
#endif
#import "BasePopUp.h"

@protocol PlayerSoundSliderDelegate<NSObject>

- (void)volumeValueChanged:(CGFloat)volumeValue;

@end

@interface PlayerSoundSlider : BasePopUp

@property (nonatomic, strong) UISlider *soundSlider;
@property (nonatomic, weak) id <PlayerSoundSliderDelegate> delegate;

@end
