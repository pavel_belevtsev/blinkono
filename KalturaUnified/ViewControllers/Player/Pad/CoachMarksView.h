//
//  CoachMarks.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CoachMarksViewDelegate <NSObject>

- (void)didDismissCoachMarksView;

@end

@interface CoachMarksView : UIView

@property (nonatomic, strong) IBOutlet UIImageView *brandImageView;
@property (nonatomic, strong) IBOutlet UIButton *btnClose;
@property (nonatomic, strong) IBOutlet UILabel *lblPersonalZoneHeader;
@property (nonatomic, strong) IBOutlet UILabel *lblPersonalZoneDes;

@property (nonatomic, strong) IBOutlet UILabel *lblMainMenuHeader;
@property (nonatomic, strong) IBOutlet UILabel *lblMainMenuDes;

@property (nonatomic, strong) IBOutlet UILabel *lblInfoBuzzHeader;
@property (nonatomic, strong) IBOutlet UILabel *lblInfoBuzzDes;

@property (nonatomic, strong) IBOutlet UILabel *lblChannelsListHeader;
@property (nonatomic, strong) IBOutlet UILabel *lblChannelsListDes;
@property (nonatomic, strong) IBOutlet UIView *bgView;
@property (nonatomic, strong) IBOutlet UIImageView *imgBg;
@property (nonatomic, weak) id<CoachMarksViewDelegate>delegate;
@property (nonatomic, strong) UIView *statusBarSpaceForIOS7;

- (void)show;
- (IBAction)close:(id)sender;
- (id)initWithMediaItem:(TVMediaItem*)mediaItem;
@end
