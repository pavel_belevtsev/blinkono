//
//  PlayerSoundSlider.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 01.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "PlayerSoundSlider.h"
#import "UIImage+Tint.h"

@interface PlayerSoundSlider()

@property (nonatomic, strong) UISlider *volumeViewSlider;
@property (nonatomic) BOOL flag;
@end

NSString * const  PlayerVCSoundSliderImgBg = @"player_sound_control_bg";
const CGFloat PlayerVCSliderSpace = 5.0f;
NSString * const  PlayerVCSoundSliderMinimumVolumeSliderImage = @"player_sound_progress";
NSString * const  PlayerVCSoundSliderMaximumVolumeSliderImage = @"player_sound_progress_bg";
NSString * const  PlayerVCSoundSliderVolumeThumbImage = @"player_sound_handleios7";
NSString * const  PlayerVCSoundSliderVolumeThumbImageios6 = @"player_sound_handleios6";

@implementation PlayerSoundSlider

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.height, frame.size.width)];
    if (self) {
        self.popUpAnimationType = TVNPopUpAnimationTypeDownToUp;
        self.popUpRectView = frame;
        self.isDismissAutomatically = YES;
        UIImageView * imageView = [[UIImageView alloc]initWithFrame:self.bounds];
        imageView.image = [UIImage imageNamed:PlayerVCSoundSliderImgBg];
        [self addSubview:imageView];
        
#ifdef PLAYER
        MPVolumeView *myVolumeView = [[MPVolumeView alloc] init];
        [myVolumeView setMinimumVolumeSliderImage:[[[UIImage imageNamed:PlayerVCSoundSliderMinimumVolumeSliderImage] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 6, 3, 6)] forState:UIControlStateNormal];
        [myVolumeView setMaximumVolumeSliderImage:[[UIImage imageNamed:PlayerVCSoundSliderMaximumVolumeSliderImage]resizableImageWithCapInsets:UIEdgeInsetsMake(3, 6, 3, 6)] forState:UIControlStateNormal];
        NSString *thumbImage = @"";
        if (IS_IOS_7_OR_LATER) {
            thumbImage = PlayerVCSoundSliderVolumeThumbImage;
            myVolumeView.frame = CGRectMake(PlayerVCSliderSpace, 7, frame.size.height - PlayerVCSliderSpace*2 , frame.size.width);
        } else {
            thumbImage = PlayerVCSoundSliderVolumeThumbImageios6;
            myVolumeView.frame = CGRectMake(PlayerVCSliderSpace, 8, frame.size.height - PlayerVCSliderSpace*2 , frame.size.width);
        }
        [myVolumeView setVolumeThumbImage:[UIImage imageNamed:thumbImage] forState:UIControlStateNormal];

        myVolumeView.showsRouteButton = NO;
        myVolumeView.showsVolumeSlider = YES;
        [self addSubview:myVolumeView];
        
        // Find the MPVolumeSlider
        for (UIView *view in [myVolumeView subviews]){
            if ([[[view class] description] isEqualToString:@"MPVolumeSlider"]) {
                self.volumeViewSlider = (UISlider*)view;
            }
        }
        [self.volumeViewSlider addTarget:self action:@selector(onVolumeViewSliderChange) forControlEvents:UIControlEventValueChanged];
        self.flag = NO;
#endif
        self.transform = CGAffineTransformMakeRotation(M_PI * -0.5);
    }
    return self;
}


-(void)onVolumeViewSliderChange {
    [self resetTimer];
#ifdef PLAYER
    float val = [self.volumeViewSlider value];
    [self.delegate volumeValueChanged:val];
 #endif
}

@end
