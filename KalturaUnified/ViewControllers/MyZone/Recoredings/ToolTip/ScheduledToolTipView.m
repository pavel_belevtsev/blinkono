//
//  ScheduledToolTipView.m
//  KalturaUnified
//
//  Created by Israel Berezin on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "ScheduledToolTipView.h"

#define firstSpase 14
#define lastSpace 26
#define ButtonCloseWidht 30

@implementation ScheduledToolTipView


-(UILabel *)builTitel:(NSString*)recordItemName
{
    UILabel * title = [[UILabel alloc] init];
    title.text = recordItemName;
    [Utils setLabelFont:title size:24 bold:YES];
    [title setTextColor:[UIColor whiteColor]];
    [title sizeToFit];
    return title;
}

-(void)updateSubTitleColorAndFont:(UILabel*)subTitle
{
    [Utils setLabelFont:subTitle size:16 bold:NO];
    [subTitle setTextColor:CS(@"959595")];
}

-(void)updateDescriptionColorAndFont:(UILabel*)decription
{
    [Utils setLabelFont:decription size:16 bold:NO];
    [decription setTextColor:CS(@"959595")];
}

-(void)buildWith:(TVNPVRRecordItem*)recordItem OnLinkageView:(UIView *)linkageView toolTipArrowType:(ToolTipArrowType)toolTipArrowType
{
    self.record = recordItem;
    self.internalView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 400, 200)];
    if (isPhone)
    {
      self.internalView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 246, 200)];
    }
    [self.internalView setBackgroundColor:[UIColor clearColor]];
    UILabel * title = [self builTitel:recordItem.name];
    
    CGSize textSize = [[title text] sizeWithAttributes:@{NSFontAttributeName:[title font]}];
    CGFloat strikeWidth = textSize.width;
    NSInteger maxWidth = self.internalView.frame.size.width;
    if (isPhone)
    {
        maxWidth -= ButtonCloseWidht;
        title.textAlignment = NSTextAlignmentCenter;
    
        if (strikeWidth > maxWidth)
        {
            title.numberOfLines=2;
            CGRect frame = title.frame;
            frame.size.width = maxWidth;
            frame.size.height =  (frame.size.height*2)+5;
            title.frame = frame;
        }
        else
        {
            CGRect frame = title.frame;
            frame.size.width = maxWidth;
            title.frame = frame;
        }
    }
    NSString * start = LS(@"scheduledToolTipView_StartOfSubTitleString");
    NSString * fromStr = LS(@"from");
    NSString * toStr = LS(@"to");
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yy"];
    NSString * datetext =  [formatter stringFromDate:recordItem.startDate];

    [formatter setDateFormat:@"HH:mm"];
    NSString * startTimeText =  [formatter stringFromDate:recordItem.startDate];
    NSString * endTimeText =  [formatter stringFromDate:recordItem.endDate];

    NSString * text = [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@",start,datetext,fromStr,startTimeText,toStr,endTimeText];

    UILabel * subTitle = [[UILabel alloc] init];
    subTitle.text = text;
    [self updateSubTitleColorAndFont:subTitle];
    [subTitle sizeToFit];
    
    textSize = [[subTitle text] sizeWithAttributes:@{NSFontAttributeName:[subTitle font]}];
    strikeWidth = textSize.width;
    if (strikeWidth > self.internalView.frame.size.width)
    {
        subTitle.numberOfLines=2;
        CGRect frame = subTitle.frame;
        frame.size.width = self.internalView.frame.size.width;
        frame.size.height =  (frame.size.height*2)+5;
        subTitle.frame = frame;
    }
    
    
    UILabel * decription = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.internalView.frame.size.width , 50)];
    decription.preferredMaxLayoutWidth = decription.bounds.size.width;
    decription.numberOfLines = 0;
    [self updateDescriptionColorAndFont:decription];

    NSInteger programNameLineHeight = 22.f;
  
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = programNameLineHeight;
    style.maximumLineHeight = programNameLineHeight;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    if (recordItem.recordDescription)
    {
        decription.attributedText = [[NSAttributedString alloc] initWithString:recordItem.recordDescription
                                                                             attributes:attributtes];
    }
    [decription sizeToFit];
    
    NSInteger high =0;
    
    CGRect titleFrame = title.frame;
    titleFrame.origin.x=0;
    if (isPhone)
    {
        titleFrame.origin.x=ButtonCloseWidht/2;
    }
    titleFrame.origin.y=0;
    title.frame = titleFrame;
    high +=title.frame.size.height + firstSpase;
    
    CGRect subTitleFrame = subTitle.frame;
    subTitleFrame.origin.x=0;
    subTitleFrame.origin.y=high;
    subTitle.frame = subTitleFrame;
    
    high += subTitle.frame.size.height + lastSpace;
    
    CGRect decriptionFrame = decription.frame;
    decriptionFrame.origin.x=0;
    decriptionFrame.origin.y=high;
    decription.frame = decriptionFrame;
    
    high += decription.frame.size.height;
    
    
    [self.internalView addSubview:title];
    [self.internalView addSubview:subTitle];
    [self.internalView addSubview:decription];
    
    CGRect internalViewFrame = self.internalView.frame;
    internalViewFrame.size.height = high;
    self.internalView.frame = internalViewFrame;
    
    
    if (isPhone)
    {
          [self buildToolTipViewWithToolTipBoxView:self.internalView linkageView:linkageView toolTipArrowType:toolTipArrowType toolCloseType:ToolTipCloseButtonOnly];
    }
    else
    {
          [self buildToolTipViewWithToolTipBoxView:self.internalView linkageView:linkageView toolTipArrowType:toolTipArrowType toolCloseType:ToolTipCloseFullScreen];
    }
  
}

-(void)insertCloseButtonIfNeed
{
    if (isPhone)
    {
        UIButton * closeFull = [[UIButton alloc] initWithFrame:self.frame];
        [closeFull setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]];
        [closeFull addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
        [self insertSubview:closeFull atIndex:0];
        
        UIButton * close = [[UIButton alloc] initWithFrame:CGRectMake(self.toolShowView.size.width-ButtonCloseWidht, 5, 25, 25)];
        [close setBackgroundColor:[UIColor clearColor]];
        [close setImage:[UIImage imageNamed:@"pin_close_btn"] forState:UIControlStateNormal];
        [close addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
        [self.toolShowView addSubview:close];
    }
}

@end
