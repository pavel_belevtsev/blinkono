//
//  ScheduledToolTipView.h
//  KalturaUnified
//
//  Created by Israel Berezin on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "ToolTipView.h"
#import <TvinciSDK/TVNPVRRecordItem.h>

@interface ScheduledToolTipView : ToolTipView

@property (strong, nonatomic) TVNPVRRecordItem * record;
@property (strong,nonatomic ) UIView * internalView;

-(void)buildWith:(TVNPVRRecordItem*)recordItem OnLinkageView:(UIView *)linkageView toolTipArrowType:(ToolTipArrowType)toolTipArrowType;

-(void)insertCloseButtonIfNeed;
@end
