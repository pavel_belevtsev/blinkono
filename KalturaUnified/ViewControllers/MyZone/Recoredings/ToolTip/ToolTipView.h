//
//  ToolTipView.h
//  KalturaUnified
//
//  Created by Israel Berezin on 12/31/14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TouchBlocker.h"

typedef enum
{
    ToolTipTypeArrowUp =0,
    ToolTipTypeArrowDown,
    ToolTipTypeArrowDynamic,
    ToolTipTypeArrowNone
}ToolTipArrowType;

typedef enum
{
    ToolTipCloseFullScreen =0,
    ToolTipCloseButtonOnly
}ToolCloseType;


@interface ToolTipView : UIView

@property (nonatomic, strong) UIView * toolTipBoxView;
@property (nonatomic, strong) UIView * toolShowView;

@property (weak, nonatomic) IBOutlet UIView *toolintShowView;
@property (nonatomic, strong) UIButton * btnFullScreenClose;

@property (nonatomic) ToolTipArrowType toolTipArrowType;
@property (nonatomic) ToolCloseType toolCloseType;

@property (nonatomic,strong) UIView * linkageView;


@property (nonatomic) NSInteger extarnelMarginRight;
@property (nonatomic) NSInteger extarnelMarginLeft;

@property (nonatomic) NSInteger internalMarginRight;
@property (nonatomic) NSInteger internalMarginLeft;
@property (nonatomic) NSInteger internalMarginTop;
@property (nonatomic) NSInteger internalMarginDown;


-(void)buildToolTipViewWithToolTipBoxView:(UIView *)boxView
                              linkageView:(UIView *)linkageView
                         toolTipArrowType:(ToolTipArrowType)toolTipArrowType
                            toolCloseType:(ToolCloseType)toolCloseType;


-(void)buildToolTipViewWithToolTipBoxView:(UIView *)boxView
                              linkageView:(UIView *)linkageView
                         toolTipArrowType:(ToolTipArrowType)toolTipArrowType
                            toolCloseType:(ToolCloseType)toolCloseType
                   andInternalMarginRight:(NSInteger)internalMarginRight
                    andInternalMarginLeft:(NSInteger)internalMarginLeft
                     andInternalMarginTop:(NSInteger)internalMarginTop
                    andInternalMarginDown:(NSInteger)internalMarginDown
                   andExtarnelMarginRight:(NSInteger)extarnelMarginRight
                    andEnternalMarginLeft:(NSInteger)extarnelMarginLeft;

-(void)updateUI;
-(void)insertMarginsBgColor;
-(IBAction)close:(id)sender;
-(void)insertCloseButtonIfNeed;

@end
