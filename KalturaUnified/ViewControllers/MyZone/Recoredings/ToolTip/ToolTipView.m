//
//  ToolTipView.m
//  KalturaUnified
//
//  Created by Israel Berezin on 12/31/14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "ToolTipView.h"
#import "APSEventMonitorWindow.h"

#define  statusBarHigh 20
#define  TopBarHigh 50
#define  BottomBarHigh 50

@interface ToolTipView()<TouchBlockerDelegate>


@end

@implementation ToolTipView

#pragma mark - -

-(void)buildToolTipViewWithToolTipBoxView:(UIView *)boxView linkageView:(UIView *)linkageView toolTipArrowType:(ToolTipArrowType)toolTipArrowType toolCloseType:(ToolCloseType)toolCloseType
{
    self.toolTipBoxView = boxView;
    self.linkageView = linkageView;
    self.toolTipArrowType = toolTipArrowType;
    self.toolCloseType = toolCloseType;
    self.extarnelMarginLeft = 20;
    self.extarnelMarginRight = 20;
    self.internalMarginRight = 16;
    self.internalMarginLeft = 16;
    self.internalMarginTop = 20;
    self.internalMarginDown = 20;
    [self buildToolTipFrame];
}

/*
 @property (nonatomic) NSInteger internalMarginRight;
 @property (nonatomic) NSInteger internalMarginLeft;
 @property (nonatomic) NSInteger internalMarginTop;
 @property (nonatomic) NSInteger internalMarginDown;
 */

-(void)buildToolTipViewWithToolTipBoxView:(UIView *)boxView
                              linkageView:(UIView *)linkageView
                         toolTipArrowType:(ToolTipArrowType)toolTipArrowType
                            toolCloseType:(ToolCloseType)toolCloseType
                   andInternalMarginRight:(NSInteger)internalMarginRight
                    andInternalMarginLeft:(NSInteger)internalMarginLeft
                     andInternalMarginTop:(NSInteger)internalMarginTop
                    andInternalMarginDown:(NSInteger)internalMarginDown
                    andExtarnelMarginRight:(NSInteger)extarnelMarginRight
                    andEnternalMarginLeft:(NSInteger)extarnelMarginLeft

{
    self.toolTipBoxView = boxView;
    self.linkageView = linkageView;
    self.toolTipArrowType = toolTipArrowType;
    self.toolCloseType = toolCloseType;
    self.extarnelMarginLeft = extarnelMarginLeft;
    self.extarnelMarginRight = extarnelMarginRight;
    self.internalMarginRight = internalMarginRight;
    self.internalMarginLeft = internalMarginLeft;
    self.internalMarginTop = internalMarginTop;
    self.internalMarginDown = internalMarginDown;
   
    [self buildToolTipFrame];
}
#pragma mark - -

-(void)buildToolTipFrame
{    
    [self updateUI];
    
    [self locateToolTip];
    
    if (self.toolTipArrowType != ToolTipTypeArrowNone)
    {
        [self insertArrow];
    }
    
    [self insertFullScreenCloseButton];
}

-(void)insertMarginsBgColor
{
    self.toolShowView.layer.cornerRadius = 5.0;
    self.toolShowView.layer.borderWidth = 1.0f;
    self.toolShowView.layer.borderColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1.0].CGColor;
    [self.toolShowView setBackgroundColor:[UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1.0]];
}

-(void)updateUI
{
    NSInteger internalMarginWidth = self.internalMarginRight + self.internalMarginLeft;
    NSInteger internalMarginHigh = self.internalMarginTop + self.internalMarginDown;

    if (self.toolintShowView == nil)
    {
        self.toolShowView = [[UIView alloc] init];
        self.toolShowView.frame = CGRectMake(0, 0, self.toolTipBoxView.frame.size.width + internalMarginWidth, self.toolTipBoxView.frame.size.height + internalMarginHigh);
       
        [self.toolShowView addSubview:self.toolTipBoxView];
        self.toolTipBoxView.center = self.toolShowView.center;
        
        [self.toolShowView.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.toolShowView.layer setShadowOpacity:0.4];
        [self.toolShowView.layer setShadowRadius:6.0];
        [self.toolShowView.layer setShadowOffset:CGSizeMake(0, 1)];
        
        [self insertMarginsBgColor];
        
        CGRect frame = self.frame;
        frame.size.width = self.toolShowView.frame.size.width+10;
        frame.size.height = self.toolShowView.frame.size.height+20;
        self.frame = frame;
        self.toolShowView.center = self.center;

    }
    else
    {
        self.toolintShowView.frame = CGRectMake(0, 0, self.toolTipBoxView.frame.size.width + internalMarginWidth, self.toolTipBoxView.frame.size.height + internalMarginHigh);

        self.toolTipBoxView.center = self.toolintShowView.center;

        [self.toolintShowView.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.toolintShowView.layer setShadowOpacity:0.4];
        [self.toolintShowView.layer setShadowRadius:6.0];
        [self.toolintShowView.layer setShadowOffset:CGSizeMake(0, 1)];

        self.toolintShowView.layer.cornerRadius = 5.0;
        self.toolintShowView.layer.borderWidth = 1.0f;
        self.toolintShowView.layer.borderColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1.0].CGColor;
       [self.toolintShowView setBackgroundColor:[UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1.0]];

        CGRect frame = self.frame;
        frame.size.width = self.toolintShowView.frame.size.width+10;
        frame.size.height = self.toolintShowView.frame.size.height+20;
        self.frame = frame;
        self.toolintShowView.center = self.center;
    }
}

-(void)insertArrow
{
    NSInteger imageHige = 18;
    NSInteger imageWidth = 28;

    UIImage * image = nil;
    NSInteger y = 0;
    
    CGRect showViewFrame = self.toolintShowView.frame;
    
    if (self.toolintShowView == nil)
    {
        showViewFrame = self.toolShowView.frame;
    }
    
    if (self.toolTipArrowType == ToolTipTypeArrowUp)
    {
        image = [UIImage imageNamed:@"arrow_up_tooltip"];
        y = showViewFrame.origin.y - imageHige;
    }
    else if (self.toolTipArrowType == ToolTipTypeArrowDown)
    {
        image = [UIImage imageNamed:@"arrow_down_tooltip"];
        y = showViewFrame.origin.y + showViewFrame.size.height;
    }
    
    UIViewController * viewC = (UIViewController *) NavM.container.centerViewController;
    CGPoint globalOrigenPoint = [self.linkageView convertPoint:self.linkageView.bounds.origin toView:viewC.view];
    
    NSInteger  x=  (globalOrigenPoint.x - self.frame.origin.x) + (self.linkageView.frame.size.width/2) - (imageWidth/2) - self.toolShowView.x;
    
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, imageWidth, imageHige)];
    [imageView setImage:image];
    [self addSubview:imageView];
}

-(void)locateToolTip
{
    if (self.toolTipArrowType == ToolTipTypeArrowNone)
    {
        self.frame = self.linkageView.frame;
        self.center =  self.linkageView.center;
        self.toolShowView.center =  self.center;
    }
    else
    {
        
        NSInteger linkageViewWidth= self.linkageView.frame.size.width;
        NSInteger toolTipWidth = self.frame.size.width;
        
        //“global” position of object in ios
        UIViewController * viewC = (UIViewController *) NavM.container.centerViewController;
        CGPoint globalOrigenPoint = [self.linkageView convertPoint:self.linkageView.bounds.origin toView:viewC.view];
        
        NSInteger toolTipX = 0;
        NSInteger mainWidth = viewC.view.bounds.size.width;
        NSInteger mainHeight = viewC.view.bounds.size.height;

        if ((globalOrigenPoint.x +linkageViewWidth) < toolTipWidth) // right point
        {
            toolTipX = self.extarnelMarginLeft;
        }
        else if ((globalOrigenPoint.x + toolTipWidth) > mainWidth) // left point
        {
            toolTipX = mainWidth - (toolTipWidth + self.extarnelMarginRight);
        }
        else  // center
        {
            toolTipX = globalOrigenPoint.x + linkageViewWidth/2 - toolTipWidth/2;
        }
        
        NSInteger toolTipY = 0;
        NSInteger toolTipYArrowUp = globalOrigenPoint.y + self.linkageView.frame.size.height - TopBarHigh - 10;
        NSInteger toolTipYArrowDown = globalOrigenPoint.y - self.frame.size.height - (BottomBarHigh + statusBarHigh);

        if (self.toolTipArrowType ==ToolTipTypeArrowDynamic)
        {
            if ((toolTipYArrowUp + self.frame.size.height) > (mainHeight - (statusBarHigh + BottomBarHigh)))
            {
                toolTipY = toolTipYArrowDown;
                self.toolTipArrowType = ToolTipTypeArrowDown;
            }
            else if ((toolTipYArrowDown - self.frame.size.height) < 0)
            {
                toolTipY = toolTipYArrowUp;
                self.toolTipArrowType = ToolTipTypeArrowUp;
            }
            else
            {
                toolTipY = toolTipYArrowUp;
                self.toolTipArrowType = ToolTipTypeArrowUp;
            }
        }
        else if (self.toolTipArrowType == ToolTipTypeArrowUp)
        {
            toolTipY = toolTipYArrowUp;
        }
        else if (self.toolTipArrowType == ToolTipTypeArrowDown)
        {
            toolTipY = toolTipYArrowDown;
        }
        self.frame = CGRectMake(toolTipX, toolTipY, self.frame.size.width, self.frame.size.height);
    }
    if (self.toolintShowView == nil)
    {
        [self addSubview:self.toolShowView];
    }
}

-(void)insertFullScreenCloseButton
{
    if (self.toolCloseType == ToolTipCloseFullScreen)
    {
        [TouchBlocker blockExceptView:self withDelegate:self];
    }
    [self insertCloseButtonIfNeed];
}

-(void)insertCloseButtonIfNeed
{
    // implament in sub class.
}

#pragma mark - -

-(IBAction)close:(id)sender
{    
    if (self.toolCloseType == ToolTipCloseFullScreen)
    {
        [TouchBlocker blockExceptView:self withDelegate:self];
    }
    self.alpha= 0.1;
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha= 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];

    }];
}

-(void) dismiss:(NSNotification *) notification
{
    [self close:nil];
}


-(void) outerTouchWasReceived
{
    [self close:nil];
}
#pragma mark - -

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


@end

