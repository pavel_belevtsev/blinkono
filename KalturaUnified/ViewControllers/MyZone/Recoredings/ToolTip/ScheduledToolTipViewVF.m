//
//  ScheduledToolTipViewVF.m
//  Vodafone
//
//  Created by Israel Berezin on 2/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "ScheduledToolTipViewVF.h"

@implementation ScheduledToolTipViewVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(UILabel *)builTitel:(NSString*)recordItemName
{
    UILabel * title = [[UILabel alloc] init];
    title.text = recordItemName;
    [Utils setLabelFont:title size:20 bold:YES];
    [title setTextColor:[UIColor whiteColor]];
    [title sizeToFit];
    return title;
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)updateSubTitleColorAndFont:(UILabel*)subTitle
{
    [Utils setLabelFont:subTitle size:16 bold:NO];
    [subTitle setTextColor:[VFTypography instance].grayScale6Color];
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)updateDescriptionColorAndFont:(UILabel*)decription
{
    [Utils setLabelFont:decription size:16 bold:NO];
    [decription setTextColor:[VFTypography instance].grayScale6Color];
}

@end
