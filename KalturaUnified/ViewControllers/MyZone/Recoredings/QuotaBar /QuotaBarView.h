//
//  QuotaBarView.h
//  KalturaUnified
//
//  Created by Israel Berezin on 1/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TvinciSDK/TVNPVRQuota.h>

@interface QuotaBarView : UIView
@property (weak, nonatomic) IBOutlet UIView *baseProssesLineView;

@property (weak, nonatomic) IBOutlet UIView *fillProssedLineView;
@property (weak, nonatomic) IBOutlet UILabel *lbltitle;
@property (strong, nonatomic) TVNPVRQuota *quota;


@property (nonatomic) NSInteger totalTime;
@property (nonatomic) NSInteger usedTime;


+(QuotaBarView *) quotaBarView;
-(void)updateUIWIthTotalTime:(NSInteger)totalTime andUsedTime:(NSInteger)usedTime;

-(void)updateUIWIthQuotaData:(TVNPVRQuota *)quotaData;

@end
