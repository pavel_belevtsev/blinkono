//
//  QuotaBarView.m
//  KalturaUnified
//
//  Created by Israel Berezin on 1/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "QuotaBarView.h"

#define kAddToolTipToView @"kaddToolTipToView"
#define kRemoveToolTipToView @"kRemoveToolTipToView"

@implementation QuotaBarView

+(QuotaBarView *) quotaBarView
{
    QuotaBarView *view = nil;
    NSString *nibName = @"QuotaBarView";
    
    if (isPhone)
    {
        nibName = @"QuotaBarViewPhone";
    }
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.lbltitle.textColor = [UIColor colorWithRed:150.0/225.0 green:150.0/225.0 blue:150.0/225.0 alpha:1];
    if (isPhone)
    {
        [Utils setLabelFont:self.lbltitle size:13 bold:NO];
    }
    else
    {
        [Utils setLabelFont:self.lbltitle size:14 bold:NO];
    }
    [self.fillProssedLineView setBackgroundColor:APST.brandColor];
}

-(void)updateUIWIthTotalTime:(NSInteger)totalTime andUsedTime:(NSInteger)usedTime
{
    [self registerToNotifcation];
    self.totalTime = totalTime;
    self.usedTime = usedTime;
    [self updateTitleText];
    [self updateProgressBar];
}

-(void)updateTitleText
{
    double remainTime = self.totalTime - self.usedTime;
    double hours = (remainTime/60)/60;
    
    NSString * startStrring = nil;
    NSString * midStrring = nil;
    NSString * endString = LS(@"quotaBarView_EndOfRemainTimeString");
    if (isPhone)
    {
        endString = LS(@"quotaBarView_EndOfRemainTimeStringPhone");
    }
    if (hours >= 2)
    {
        startStrring = [NSString stringWithFormat:@"%.0f",hours];
        midStrring = LS(@"hrs");
    }
    else if (hours >= 1 && hours < 2)
    {
        startStrring = [NSString stringWithFormat:@"%.0f",hours];
        midStrring = LS(@"hr");
    }
    else if (hours > 0  && hours < 1)
    {
        startStrring = [NSString stringWithFormat:@"%.0f",remainTime/60];
        midStrring = LS(@"min");
    }
    else
    {
        startStrring = @"0";
        midStrring = LS(@"min");
    }
    
    NSString * text = [NSString stringWithFormat:@"%@ %@ %@",startStrring,midStrring,endString];
    self.lbltitle.text = text;
}

-(void)updateProgressBar
{
    float wightToFill = 2;
    if (self.totalTime!=0)
    {
        float prsent = (100*self.usedTime)/self.totalTime;
        
         wightToFill = (self.baseProssesLineView.frame.size.width * prsent) / 100;
        
        if (wightToFill <2)
        {
            wightToFill = 2;
        }
    }
    else
    {
        wightToFill=self.baseProssesLineView.frame.size.width;
    }
    CGRect frame = self.fillProssedLineView.frame;
    frame.size.width = wightToFill;
    self.fillProssedLineView.frame = frame;
}

-(void)updateUIWIthQuotaData:(TVNPVRQuota *)quotaData;
{
    self.quota = quotaData;
    [self updateUIWIthTotalTime:quotaData.totalQuota andUsedTime:quotaData.occupiedQuota];
}

-(void)registerToNotifcation
{
    [self unRegisterToNotifcation];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(toolTipToViewIsShow:) name:kAddToolTipToView object:nil];
    [center addObserver:self selector:@selector(toolTipToViewIsRemove:) name:kRemoveToolTipToView object:nil];

}

-(void)toolTipToViewIsShow:(NSNotification *) notification
{
    if (isPad)
    {
        [self.fillProssedLineView setBackgroundColor:CS(@"969696")];
    }
}

-(void)toolTipToViewIsRemove:(NSNotification *) notification
{
    if (isPad)
    {
        [self.fillProssedLineView setBackgroundColor:APST.brandColor];
    }
}

-(void)unRegisterToNotifcation
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:kAddToolTipToView object:nil];
    [center removeObserver:self name:kRemoveToolTipToView object:nil];

}

-(void)dealloc
{
    [self unRegisterToNotifcation];
}
@end
