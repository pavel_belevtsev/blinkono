//
//  QuotaBarViewVF.m
//  Vodafone
//
//  Created by Israel Berezin on 2/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "QuotaBarViewVF.h"

@implementation QuotaBarViewVF

/* "PSC : Vodafone Grup : change color  : JIRA ticket. */
-(void)awakeFromNib
{
    [super awakeFromNib];
    [self.fillProssedLineView setBackgroundColor:APST.brandColor];
    [Utils setLabelFont:self.lbltitle size:14 bold:NO];
    self.lbltitle.textColor = [VFTypography instance].grayScale5Color;
}
@end
