//
//  QuotaBarViewToolTip.h
//  KalturaUnified
//
//  Created by Israel Berezin on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "ToolTipView.h"
#import <TvinciSDK/TVNPVRQuota.h>
typedef enum
{
    QuotaBarToolTipWarning = 0,
    QuotaBarToolTipExceeded,
    QuotaBarToolTipNone
}QuotaBarToolTipType;

@interface QuotaBarViewToolTip : ToolTipView

@property (strong, nonatomic) TVNPVRQuota *quota;
@property (nonatomic) QuotaBarToolTipType toolTipType;

@property (strong,nonatomic ) UIView * internalView;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;
@property (weak, nonatomic) IBOutlet UIView *buttomView;

@property (weak, nonatomic) IBOutlet UIView *baseProssesLineView;

@property (weak, nonatomic) IBOutlet UIView *fillProssedLineView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblDontShow;
@property (weak, nonatomic) IBOutlet UIButton *btnGotIt;
@property (weak, nonatomic) IBOutlet UISwitch *dontShowSwitch;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

-(id)initWithQuotaData:(TVNPVRQuota *)quotaData;

-(void)buildWIthQuotaDataWithOnLinkageView:(UIView *)linkageView;

-(void)addToolTipToView:(UIView*)addView;

-(BOOL)isNeedToShowToolTip;
- (IBAction)closeView:(id)sender;

-(void)updateUIAndText;
- (void)initSwitchUI:(UISwitch *)switchRemeber;
@end


