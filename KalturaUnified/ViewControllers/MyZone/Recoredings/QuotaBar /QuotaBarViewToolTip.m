//
//  QuotaBarViewToolTip.m
//  KalturaUnified
//
//  Created by Israel Berezin on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "QuotaBarViewToolTip.h"
#import "UIImage+Tint.h"

#define QuotaBarToolTipWarningStatusKey @"QuotaBarToolTipWarningStatusKey"
#define QuotaBarToolTipExceededStatusKey @"QuotaBarToolTipExceededStatusKey"
#define QuotaBarToolTipNotShowAgain @"QuotaBarToolTipNotShowAgain"

#define FirstSpace 24
#define FirstSpacePhone 19

#define LastSpase 14
#define LastSpasePhone 21

#define kAddToolTipToView @"kaddToolTipToView"
#define kRemoveToolTipToView  @"kRemoveToolTipToView"


@interface QuotaBarViewToolTip()

@property BOOL dontShowAgainToolTip;

@end


@implementation QuotaBarViewToolTip

-(id)initWithQuotaData:(TVNPVRQuota *)quotaData
{
    self = [super init];
    if (self)
    {
        self.quota = quotaData;
        [self chooseNeededToolTipType];
    }
    return  self;
}

+(QuotaBarViewToolTip *) quotaBarViewToolTip
{
    QuotaBarViewToolTip *view = nil;
    NSString *nibName = @"QuotaBarViewToolTip";
    if (isPhone)
    {
        nibName = @"QuotaBarViewToolTipPhone";
    }
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}


-(void)chooseNeededToolTipType
{
//    self.quota.occupiedQuota = self.quota.totalQuota; // To test !!!
    NSInteger per = (self.quota.occupiedQuota * 100) / (self.quota.totalQuota);
    if (per ==0)
    {
        self.toolTipType = QuotaBarToolTipNone;
    }
    else if (self.quota.occupiedQuota >=  self.quota.totalQuota)
    {
        self.toolTipType = QuotaBarToolTipExceeded;
    }
    else if (per >94 && per <100)
    {
        self.toolTipType = QuotaBarToolTipWarning;
    }
    else if (per == 100)
    {
        self.toolTipType = QuotaBarToolTipExceeded;
    }
    else
    {
        self.toolTipType = QuotaBarToolTipNone;
    }
//    self.toolTipType = QuotaBarToolTipExceeded;
}

-(void)buildWIthQuotaDataWithOnLinkageView:(UIView *)linkageView
{
    self.internalView = [QuotaBarViewToolTip quotaBarViewToolTip];
    [self.internalView setBackgroundColor:[UIColor clearColor]];
    
    [self updateProgressBar];
    [self updateUIAndText];
    if (isPad)
    {
        [self locateUI_Pad];
    }
    else
    {
        [self locateUI_Phone];
    }
    
    ToolTipArrowType type =  ToolTipTypeArrowDynamic;
    if (isPhone)
    {
         type = ToolTipTypeArrowNone;
    }
    [self buildToolTipViewWithToolTipBoxView:self.internalView linkageView:linkageView toolTipArrowType:type toolCloseType:ToolTipCloseButtonOnly];
}

-(void)addToolTipToView:(UIView*)addView
{
    [addView addSubview: self];
    [[NSNotificationCenter defaultCenter] postNotificationName:kAddToolTipToView object:nil userInfo:nil];
}

-(void)updateProgressBar
{
    QuotaBarViewToolTip * toolTipview = (QuotaBarViewToolTip*)self.internalView;
    
    [toolTipview.fillProssedLineView setBackgroundColor:APST.brandColor];
    
    float prsent = 0;
    float wightToFill =0;
    if (self.quota.totalQuota >0)
    {
        prsent = (100*self.quota.occupiedQuota)/self.quota.totalQuota;
        wightToFill = (toolTipview.baseProssesLineView.frame.size.width * prsent) / 100;

    }
    
    CGRect frame = toolTipview.fillProssedLineView.frame;
    frame.size.width = wightToFill;
    toolTipview.fillProssedLineView.frame = frame;
}

-(void)updateUIAndText
{
    QuotaBarViewToolTip * toolTipview = (QuotaBarViewToolTip*)self.internalView;
    
    NSString * startTitle = [self updateTitleText];
    NSString * otherTitle = LS(@"quotaBarViewToolTip_endTitle");
    toolTipview.lblTitle.text = [NSString stringWithFormat:@"%@ %@",startTitle,otherTitle];
    if (isPad)
    {
        [Utils setLabelFont:toolTipview.lblTitle size:20 bold:YES];
    }
    else
    {
        [Utils setLabelFont:toolTipview.lblTitle size:14 bold:YES];

    }
    toolTipview.lblTitle.textColor = [UIColor whiteColor];
    
    NSString *  startDes = LS(@"quotaBarViewToolTip_WarningType_startDescription");
    NSString *  endString = LS(@"quotaBarViewToolTip_WarningType_endDescription");
    NSString * textToDes =[NSString stringWithFormat:@"%@ %@ %@",startDes,startTitle,endString];
    if (self.toolTipType == QuotaBarToolTipExceeded)
    {
        textToDes = LS(@"quotaBarViewToolTip_ExceededTypeDescription");
    }
    else
    {
        
    }
    toolTipview.lblDescription.text = textToDes;
    if (isPad)
    {
        [Utils setLabelFont:toolTipview.lblDescription size:16 bold:NO];
    }
    else
    {
        [Utils setLabelFont:toolTipview.lblDescription size:14 bold:NO];
    }
    toolTipview.lblDescription.textColor = CS(@"959595");
    
    toolTipview.lblDescription.numberOfLines=0;
    NSInteger programNameLineHeight = 22.f;
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = programNameLineHeight;
    style.maximumLineHeight = programNameLineHeight;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    if (toolTipview.lblDescription.text)
    {
        toolTipview.lblDescription.attributedText = [[NSAttributedString alloc] initWithString:toolTipview.lblDescription.text
                                                                    attributes:attributtes];
    }
    [toolTipview.lblDescription sizeToFit];

    toolTipview.lblDontShow.text = LS(@"quotaBarViewToolTip_BtnDontShowAgain");
    if (isPad)
    {
        [Utils setLabelFont:toolTipview.lblDontShow size:16 bold:NO];
    }
    else
    {
        [Utils setLabelFont:toolTipview.lblDontShow size:14 bold:NO];
    }
    toolTipview.lblDontShow.textColor = CS(@"959595");
    
    [toolTipview.seperatorView setBackgroundColor: CS(@"505050")];
    
    [toolTipview.btnGotIt setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    
    [Utils setButtonFont:toolTipview.btnGotIt bold:YES];
    [toolTipview.btnGotIt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [toolTipview.btnGotIt addTarget:self action:@selector(closeToolTip:) forControlEvents:UIControlEventTouchUpInside];

    [toolTipview.btnClose addTarget:self action:@selector(closeView:) forControlEvents:UIControlEventTouchUpInside];

    [self initSwitchUI:toolTipview.dontShowSwitch];
    
    [toolTipview.dontShowSwitch addTarget:self action:@selector(changeState:) forControlEvents:UIControlEventValueChanged];
}


-(void)locateUI_Pad
{
    QuotaBarViewToolTip * toolTipview = (QuotaBarViewToolTip*)self.internalView;
    
    CGRect frame1 =  toolTipview.lblDescription.frame;
    frame1.origin.y = toolTipview.lblTitle.frame.origin.y + toolTipview.lblTitle.frame.size.height +FirstSpace;
    toolTipview.lblDescription.frame = frame1;
    
    CGRect frame2 = toolTipview.buttomView.frame;
    frame2.origin.y =  toolTipview.lblDescription.frame.origin.y +  toolTipview.lblDescription.frame.size.height + LastSpase;
    toolTipview.buttomView.frame = frame2;
    
    NSInteger high = (frame2.origin.y + frame2.size.height);
    
    CGRect mainFrame = toolTipview.frame;
    mainFrame.size.height = high;
    toolTipview.frame = mainFrame;
}


-(void)locateUI_Phone
{
    QuotaBarViewToolTip * toolTipview = (QuotaBarViewToolTip*)self.internalView;
    
    CGRect frame1 =  toolTipview.lblDescription.frame;
    frame1.origin.y = toolTipview.baseProssesLineView.frame.origin.y + toolTipview.baseProssesLineView.frame.size.height +FirstSpacePhone;
    toolTipview.lblDescription.frame = frame1;
    
    CGRect frame2 = toolTipview.buttomView.frame;
    frame2.origin.y =  toolTipview.lblDescription.frame.origin.y +  toolTipview.lblDescription.frame.size.height + LastSpasePhone;
    toolTipview.buttomView.frame = frame2;
    
    NSInteger high = (frame2.origin.y + frame2.size.height);
    
    CGRect mainFrame = toolTipview.frame;
    mainFrame.size.height = high;
    toolTipview.frame = mainFrame;
}

- (void)initSwitchUI:(UISwitch *)switchRemeber {
    
    [switchRemeber setOnTintColor:APST.brandColor];
    [switchRemeber setTintColor:CS(@"bebebe")];
    [switchRemeber setThumbTintColor:[UIColor whiteColor]];
}

-(NSString * )updateTitleText
{
    double remainTime = self.quota.totalQuota - self.quota.occupiedQuota;
    double hours = (remainTime/60)/60;
    
    NSString * startStrring = nil;
    NSString * midStrring = nil;
    
    if (hours >= 2)
    {
        startStrring = [NSString stringWithFormat:@"%.0f",hours];
        midStrring = LS(@"hrs");
    }
    else if (hours >= 1 && hours < 2)
    {
        startStrring = [NSString stringWithFormat:@"%.0f",hours];
        midStrring = LS(@"hr");
    }
    else if (hours > 0  && hours < 1)
    {
        startStrring = [NSString stringWithFormat:@"%.0f",remainTime/60];
        midStrring = LS(@"min");
    }
    else
    {
        startStrring = @"0";
        midStrring = LS(@"min");
    }
    
    NSString * text = [NSString stringWithFormat:@"%@ %@",startStrring,midStrring];
    
    return text;
}

- (void)changeState:(id)sender
{
    UISwitch *state = sender;
    
    self.dontShowAgainToolTip =state.on;
}

- (IBAction)closeToolTip:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kRemoveToolTipToView object:nil userInfo:nil];

    if (self.dontShowAgainToolTip)
    {
        [self saveDontShowAgain];
    }
    [self close:nil];
    [self removeFromSuperview];
}


/*
 QuotaBarToolTipWarningStatusKey
 QuotaBarToolTipExceededStatusKey
 #define QuotaBarToolTipNotShowAgain
 */
-(void)saveDontShowAgain
{
    switch (self.toolTipType)
    {
        case QuotaBarToolTipWarning:
        {
            [[NSUserDefaults standardUserDefaults] setObject:QuotaBarToolTipNotShowAgain forKey:QuotaBarToolTipWarningStatusKey];
        }
            break;
        case QuotaBarToolTipExceeded:
        {
            [[NSUserDefaults standardUserDefaults] setObject:QuotaBarToolTipNotShowAgain forKey:QuotaBarToolTipExceededStatusKey];

        }
            break;
        default:
            break;
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)isNeedToShowToolTip
{
//    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:QuotaBarToolTipWarningStatusKey];
//    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:QuotaBarToolTipExceededStatusKey];
//    return YES;
    
    if (self.toolTipType == QuotaBarToolTipNone)
    {
        return NO;
    }
    NSString * status = nil;
    switch (self.toolTipType)
    {
        case QuotaBarToolTipWarning:
        {
             status = [[NSUserDefaults standardUserDefaults] objectForKey:QuotaBarToolTipWarningStatusKey];
        }
            break;
        case QuotaBarToolTipExceeded:
        {
             status =[[NSUserDefaults standardUserDefaults] objectForKey:QuotaBarToolTipExceededStatusKey];
        }
            break;
        default:
            break;
    }
    
    if ([status isEqualToString:QuotaBarToolTipNotShowAgain])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (IBAction)closeView:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kRemoveToolTipToView object:nil userInfo:nil];
    [self close:nil];
    [self removeFromSuperview];
}

@end


