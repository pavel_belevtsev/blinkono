//
//  QuotaBarViewToolTipVF.m
//  Vodafone
//
//  Created by Israel Berezin on 2/26/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "QuotaBarViewToolTipVF.h"

@implementation QuotaBarViewToolTipVF

/* "PSC : Vodafone Grup : change color  : JIRA ticket. */
-(void)updateUIAndText
{
    [super updateUIAndText];
    QuotaBarViewToolTip * toolTipview = (QuotaBarViewToolTip*)self.internalView;
    toolTipview.lblDescription.textColor = [VFTypography instance].grayScale6Color;
    toolTipview.lblDontShow.textColor = [VFTypography instance].grayScale6Color;

}
- (void)initSwitchUI:(UISwitch *)switchRemeber
{
    [super initSwitchUI:switchRemeber];
    
}

@end
