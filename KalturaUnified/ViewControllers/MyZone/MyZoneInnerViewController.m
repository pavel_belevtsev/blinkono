//
//  MyZoneInnerViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneInnerViewController.h"
#import "MyZoneItemCollectionViewCell.h"
#import "MyZoneMediaItemView.h"
#import "MyZonePurchasesListViewController.h"
#import "MyZoneHistoryViewController.h"
#import "UIAlertView+Blocks.h"
#import "UIImage+Tint.h"

const NSInteger zoneViewPadItemsCount = 8;
const NSInteger zoneViewPadRowsCount = 2;
const NSInteger zoneViewPadColsCount = 4;

@interface MyZoneInnerViewController ()

@end

@implementation MyZoneInnerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.


    [self defineControllerSettings];
    
    if([self isMyZoneFullPad])
    {
         self.zoneEmptyView = [MyZoneEmptyView MyZoneEmptyFullView];
    }
    else
    {
        self.zoneEmptyView = [[MyZoneEmptyView alloc] initWithNib];
        if ([LoginM isSignedIn]) {
            [self.zoneEmptyView.viewLogin setHidden:YES];
        }
        else {
            [self.zoneEmptyView.viewLogin setHidden:NO];
        }
        
    }
    self.myZoneList = [[NSMutableArray alloc] init];
    
    [_collectionViewMyZone registerClass:[MyZoneItemCollectionViewCell class] forCellWithReuseIdentifier:@"MyZoneItemCollectionViewCell"];
    [_collectionViewMyZoneInner registerClass:[MyZoneItemCollectionViewCell class] forCellWithReuseIdentifier:@"MyZoneItemCollectionViewCell"];
    _collectionViewMyZoneInner.alpha = 0.0;
    
    [self updateEditButtonsUI];
    
  
}

- (BOOL) isMyZoneFullPad {
    return [self.zoneViewController isMyZoneFullPad];
}

- (void)addingPagerView
{
    if( [self isMyZoneFullPad])
    {
        [self addingPagerViewToFullScreen];
        return;
    }
    CGRect pageControlFrame = _viewNavigation.bounds;
    self.viewPageControl = [[PageControlView alloc] initWithFrame:pageControlFrame];
    [self.viewPageControl.control addTarget:self action:@selector(pageControlChanged:) forControlEvents:UIControlEventValueChanged];
    self.viewPageControl.numberOfPages = 0;
    [_viewNavigation addSubview:self.viewPageControl];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self setupPageTitle];
    [self menuContainerViewController].rightMenuViewController = nil;
    [[self menuContainerViewController] setRightMenuWidth:0];

    if (isPad &&( _viewNavigation || [self isMyZoneFullPad]))
    {
        if (!_viewPageControl) [self addingPagerView];
    }
    
    if (!isInitialized && !self.innerMode)
    {
        //isInitialized = YES;
        [self reloadData];
        if(isPad && [self isMyZoneFullPad]==NO)
        {
            [self.zoneViewController updateEditButtonInMiniMyZone];
        }
    }
}

- (void)changeEditMode:(BOOL)mode
{
    editMode = mode;
    [self.zoneViewController changeClearAllButtonEnabled:mode];

    [_collectionViewMyZone reloadData];
    [_collectionViewMyZoneInner reloadData];
}

- (MediaItemBelongsToListType) mediaListType {
    return kMediaItemBelongsToListTypeUndefined;
}

- (void)initEmptyScreen:(BOOL)friends title:(NSString *)title text:(NSString *)text andImage:(UIImage *)image
{
    
    _zoneEmptyView.imageEmpty.image = [UIImage imageNamed:(friends ? @"myzone_friends_clear" : @"myzone_clear")];
    _zoneEmptyView.labelEmptyTitle.text = title;
    _zoneEmptyView.labelEmptyText.text = text;
    
    _zoneEmptyView.buttonFBLogin.hidden = !friends || [LoginM isFacebookUser];
    if (friends)
    {
        [self initFacebookButtonUI:_zoneEmptyView.buttonFBLogin];
        [_zoneEmptyView.buttonFBLogin addTarget:self action:@selector(loginFacebookPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        if (image)
        {
            _zoneEmptyView.imageEmpty.image = image;
        }
    }
}

- (void)loginFacebookPressed:(UIButton *)button
{
    [self intermediateLogin:@"fbLogin"];
}


- (void)reloadData
{
    [self backToMainList:NO];
    
    [_collectionViewMyZone reloadData];
    
    if (isPad)
    {
        self.viewPageControl.currentPage = 0;
        self.viewPageControl.numberOfPages = 0;
    }
    
    [_zoneEmptyView removeFromSuperview];
    [self.zoneViewController showHUDBlockingUI:YES];
    
    [_zoneViewController changeEditButtonEnabled:NO];
}

- (void)loadData:(int)pageIndex array:(NSMutableArray *)array
{
    __weak MyZoneInnerViewController *weakSelf = self;
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetUserItems:TVUserItemTypeFavorites mediaType:@"0" pictureSize:[TVPictureSize iPadItemCellPictureSize] pageSize:maxPageSize pageIndex:pageIndex delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
        
        for (id item in result) {
            [array addObject:item];
        }
        
        //if ([result count] == maxPageSize) {
        //    [weakSelf loadData:(pageIndex + 1) array:array];
        //} else {
        [weakSelf updateData:array useLive:NO];
        //}
        
    }];
    [request setFailedBlock:^{
        
        [self hideHUD];
        
    }];
    
    [self sendRequest:request];
}

- (void)showEmptyScreen {
    
    _zoneEmptyView.frame = self.view.bounds;
    [self.view addSubview:_zoneEmptyView];
    self.viewPageControl.hidden = YES;
    [_zoneViewController hideActionButtons];
}

- (void)updateData:(NSArray *)array {

    [self updateData:array useLive:YES];
}

- (void)updateData:(NSArray *)array useLive:(BOOL)useLive {

    [self hideHUD];
    
    [_myZoneList removeAllObjects];
    
    for (id item in array) {
        TVMediaItem * mediaItem = nil;
        if ([item isKindOfClass:[TVMediaItem class]]) {
            mediaItem = item;
        }
        else {
            mediaItem = [[TVMediaItem alloc] initWithDictionary:item];
        }
        
        //update list type to the type related to current view
        if (mediaItem.listType == 0)
            mediaItem.listType = [self mediaListType];
        
        if (![mediaItem isLive] || useLive) {
            [_myZoneList addObject:mediaItem];
        }
    }
    
    if(isPad && [[self.storyboard valueForKey:@"name"] isEqualToString:@"MyZonePad"])
    {
        [_zoneViewController changeEditButtonEnabled:NO];
    }
    else
    {
        NSMutableArray *objectTypes = [_myZoneList valueForKeyPath:@"listType"];
        if (objectTypes.count) {
            objectTypes = [objectTypes valueForKeyPath:@"@distinctUnionOfObjects.self"];
            if ([objectTypes containsObject:@(kMediaItemBelongsToListTypeUndefined)] ||
                [objectTypes containsObject:@(kMediaItemBelongsToListTypeFavorites)] ||
                [objectTypes containsObject:@(kMediaItemBelongsToListTypeDownload)] ||
                [objectTypes containsObject:@(kMediaItemBelongsToListTypeHistory)] ||
                [objectTypes containsObject:@(kMediaItemBelongsToListTypeWatchList)]) {
                [_zoneViewController changeEditButtonEnabled:YES];
            }
            else //dont allow editing if all items are purchased
                [_zoneViewController changeEditButtonEnabled:NO];
            
        }
        else {
            [_zoneViewController changeEditButtonEnabled:([_myZoneList count] ? YES : NO)];
        }
    }
    
    [self sortMediaArray:_myZoneList];
    
    [_collectionViewMyZone reloadData];
    self.viewPageControl.hidden = NO;

    if (![_myZoneList count])
    {
        [self showEmptyScreen];
    }
    else
    {
        NSInteger pages = [_myZoneList count] / 4;
        if (([_myZoneList count] % 4) > 0) pages++;
        
        self.viewPageControl.numberOfPages = pages;
         self.viewPageControl.hidden = NO;
    }
}

- (NSString *)getMediaTitle:(id)media {
    
    NSString *title = @"";
    
    if ([media isKindOfClass:[TVMediaItem class]]) {
        title = ((TVMediaItem *)media).name;
    } else if ([media isKindOfClass:[NSArray class]]) {
        
        TVMediaItem *item = [((NSArray *)media) lastObject];
        if ([item isKindOfClass:[TVMediaItem class]]) {
            title = [Utils getSeriesName:item];
        }
    }
    
    return title;
}

- (void)sortMediaArray:(NSMutableArray *)array {
    
    for (int i = 0; i < [array count]; i++) {
        TVMediaItem *item = [array objectAtIndex:i];
        
        if ([item isKindOfClass:[TVMediaItem class]] && (item.isEpisode || item.isSeries) && (i > 0)) {
        
            for (int j = 0; j < i; j++) {
                
                TVMediaItem *itemP = [array objectAtIndex:j];
                
                if ([itemP isKindOfClass:[TVMediaItem class]] && (item.isEpisode || item.isSeries)) {
                    
                    if ([[Utils getSeriesName:item] isEqualToString:[Utils getSeriesName:itemP]]) {
                        NSMutableArray *arrayP = [[NSMutableArray alloc] init];
                        [arrayP addObject:itemP];
                        
                        [array removeObjectAtIndex:j];
                        [array insertObject:arrayP atIndex:j];
                        
                        [arrayP addObject:item];
                        [array removeObjectAtIndex:i--];
                    }
                    
                } else if ([itemP isKindOfClass:[NSMutableArray class]]) {
                    
                    NSMutableArray *arrayP = (NSMutableArray *)itemP;
                    TVMediaItem *itemI = [arrayP objectAtIndex:0];
                    
                    if ([[Utils getSeriesName:item] isEqualToString:[Utils getSeriesName:itemI]]) {
                        
                        [arrayP addObject:item];
                        [array removeObjectAtIndex:i--];
                        
                    }
                    
                }
                
            }
            
            
            
        }
        
    }
    
    for (int i = 0; i < [array count]; i++) {
        TVMediaItem *item = [array objectAtIndex:i];
        
        if ([item isKindOfClass:[TVMediaItem class]]) {
            NSLog(@"item %@", item.name);
        } else if ([item isKindOfClass:[NSMutableArray class]]) {
            NSMutableArray *arrayI = (NSMutableArray *)item;
            item = [arrayI objectAtIndex:0];
            NSLog(@"array %ld %@", (long)[arrayI count], [Utils getSeriesName:item]);
            
            [arrayI sortUsingComparator:^NSComparisonResult(id media1, id media2) {
                
                return [[[self getMediaTitle:media1] uppercaseString] compare:[[self getMediaTitle:media2] uppercaseString]];
                
            }];
        }
        
    }
    
    
    [array sortUsingComparator:^NSComparisonResult(id media1, id media2) {
        
        return [[[self getMediaTitle:media1] uppercaseString] compare:[[self getMediaTitle:media2] uppercaseString]];
        
    }];
    
}

- (void)openMediaItem:(TVMediaItem *)mediaItem
{
    if( [self isMyZoneFullPad] == NO)
    {
        if (NavM.container.centerViewController)
        {
            BaseViewController *controller = [[NavM.container.centerViewController viewControllers] lastObject];
            [controller closeMyZone];
        }
    }
    
    [super openMediaItem:mediaItem];
    
}

- (void)openSeriesMediaArray:(NSMutableArray *)array {

    self.innerMode = YES;
    
    _seriesArray = array;
    [_collectionViewMyZoneInner reloadData];
    [_collectionViewMyZoneInner scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
//    if (editMode) {
//        [_zoneViewController buttonEditPressed:_zoneViewController.buttonEdit];
//    }
    
    [UIView animateWithDuration:kViewAnimationDuration animations:^{
    
        _zoneViewController.buttonBack.alpha = 1.0;
        _zoneViewController.buttonMenu.alpha = 0.0;
        
        _collectionViewMyZoneInner.alpha = 1.0;
        _collectionViewMyZone.alpha = 0.0;
        
        _viewPageControl.alpha = 0.0;
        
    }];
    
}

- (void)backToMainList:(BOOL)animated {
    
    self.innerMode = NO;
    
    //remove empty arrays
    [_myZoneList removeObjectsInArray: [_myZoneList filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id obj, NSDictionary *bindings) {
        return ([obj isKindOfClass:[NSArray class]] && ![(NSArray*)obj count]);
    }]]];
    
    for (int i = 0; i < [_myZoneList count]; i++) {
        TVMediaItem *item = [_myZoneList objectAtIndex:i];
        
        if ([item isKindOfClass:[TVMediaItem class]]) {

        } else if ([item isKindOfClass:[NSMutableArray class]]) {
            NSMutableArray *array = (NSMutableArray *)item;
            
            if ([array count] == 1) {
                
                TVMediaItem *item = [array objectAtIndex:0];
                [_myZoneList insertObject:item atIndex:i];
                [_myZoneList removeObjectAtIndex:i + 1];
                
            }
        }
    }
    
    [_collectionViewMyZone reloadData];
    [self updatePageControl];
    
    [UIView animateWithDuration:(animated ? kViewAnimationDuration : 0) animations:^{
        
        _collectionViewMyZoneInner.alpha = 0.0;
        _collectionViewMyZone.alpha = 1.0;
        
        _viewPageControl.alpha = 1.0;
        
    }];
    
}

- (BOOL)inEditMode {
    
    return editMode;
    
}

- (void)updatePageControl {
    
    if (isPad)
    {
        NSInteger zonesCount = [_myZoneList count];

        if( [self isMyZoneFullPad])
        {
            NSInteger num =  (zonesCount / zoneViewPadItemsCount) + ((zonesCount % zoneViewPadItemsCount) ? 1 : 0);
            
            self.viewPageControl.numberOfPages =num;
        }
        else
        {
            if (zonesCount > 0) {
                NSInteger pages = zonesCount / 4;
                if ((zonesCount % 4) > 0) pages++;
                
                if (self.viewPageControl.currentPage >= pages) {
                    self.viewPageControl.currentPage = pages - 1;
                }
                self.viewPageControl.numberOfPages = pages;
            }
        }
    }
    
    if ([_myZoneList count] == 0) {
    
        [self showEmptyScreen];
    }
    
}

#pragma mark - Collection View Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(isPad && [self isMyZoneFullPad])
    {
        NSInteger count =[_myZoneList count];
        NSInteger num =  (count / zoneViewPadItemsCount) + ((count % zoneViewPadItemsCount) ? 1 : 0);
        
        self.viewPageControl.numberOfPages =num;
        return num;

    }
    else
    {
        return 1;
    }
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(isPad && [self isMyZoneFullPad])
    {
        return zoneViewPadItemsCount;
    }
    else
    {
        NSInteger count = [_myZoneList count];
        
        if (collectionView == _collectionViewMyZoneInner) count = [_seriesArray count];
        
        if (isPad && count)
        {
            while ((count % 4) > 0)
            {
                count++;
            }
        }
        
        return count;
    }
    
}

- (NSString*) reusableCellIdentifier {
    return NSStringFromClass(MyZoneItemCollectionViewCell.class);
}

- (void) defineControllerSettings {
    self.purchasesLoadType = kPurchasesManagerLoadItemsObjects;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MyZoneItemCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:self.reusableCellIdentifier forIndexPath:indexPath];
    cellView.delegateController = self.zoneViewController;
    
    NSInteger index = indexPath.row;
    
    if(isPad && [self isMyZoneFullPad])
    {
        if (index % zoneViewPadRowsCount == 0)
        {
            index = index / zoneViewPadRowsCount;
        }
        else
        {
            index = index / zoneViewPadRowsCount;
            index += zoneViewPadColsCount;
        }
        //NSLog(@"index %d", (int)index);
        index += (indexPath.section * zoneViewPadItemsCount);
    }
    
    NSMutableArray *myZoneArray = (collectionView == _collectionViewMyZone) ? _myZoneList : _seriesArray;
    
    if (index < [myZoneArray count])
    {
        TVMediaItem *mediaItem = [myZoneArray objectAtIndex:index];
        [cellView updateCellWithMediaItem:mediaItem];
        cellView.mediaItemView.viewWatchList.hidden = YES;
        cellView.mediaItemView.delegateInnerController = self;
        
        BOOL mediaFromWatchList = NO;
        
        if ([mediaItem isKindOfClass:[TVMediaItem class]]) {
            BOOL showRemoveOption = FALSE;
            BOOL showGrayedOut = FALSE;

            switch (mediaItem.listType) {
                case kMediaItemBelongsToListTypeDownload:
                    showRemoveOption = TRUE;
                    break;
                case kMediaItemBelongsToListTypeWatchList:
                    showGrayedOut = showRemoveOption = (APST.hasWatchList);
                    break;
                case kMediaItemBelongsToListTypeExpiredSubscriptions:
                    showGrayedOut = (APST.hasInAppPurchases);
                    break;
                case kMediaItemBelongsToListTypeFavorites:
                case kMediaItemBelongsToListTypeHistory:
                    showRemoveOption = TRUE;
                    break;
                default:
                    break;
            }
            if (showRemoveOption) {
                cellView.mediaItemView.buttonCross.hidden = NO;
            }
            if (showGrayedOut) {
                cellView.mediaItemView.viewWatchList.hidden = NO;
            }

        }
        else {
            cellView.mediaItemView.buttonCross.hidden = YES;
        }
        
        BOOL hideCrossButton = !editMode;
        BOOL canAnimate = YES;
        
        if ([self isKindOfClass:[MyZonePurchasesListViewController class]]) {
            if (editMode) {
                hideCrossButton = cellView.mediaItemView.viewWatchList.hidden;
                if (hideCrossButton)
                    canAnimate = NO;
            }
            else {
                canAnimate = mediaFromWatchList;
            }
        }
        
        if ([self isKindOfClass:[MyZoneHistoryViewController class]] == NO)
        {
            if ([mediaItem isKindOfClass:[NSArray class]]) hideCrossButton = YES;
        }
        else
        {
            cellView.mediaItemView.buttonCross.hidden = NO;
        }
      
        BOOL priorValue = (cellView.mediaItemView.buttonCross.alpha == 0);
        
        if (priorValue != hideCrossButton && canAnimate)
        {
            [UIView animateWithDuration:0.3 animations:^{
                cellView.mediaItemView.buttonCross.alpha = (hideCrossButton ? 0.0 : 1.0);
                cellView.mediaItemView.buttonCross.transform = CGAffineTransformMakeScale(hideCrossButton ? 0.1 : 1.0, hideCrossButton ? 0.1 : 1.0);
                
            }];
        }
        
        if (!canAnimate && hideCrossButton) {
            
            cellView.mediaItemView.buttonCross.alpha = 0.0;
            cellView.mediaItemView.buttonCross.transform = CGAffineTransformMakeScale(0.1, 0.1);
            

        }
        
        cellView.mediaItemView.buttonCross.tag = index;
        [cellView.mediaItemView.buttonCross setTouchUpInsideAction:^(UIButton *btn) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"my_zone_delete_message_wo_params") delegate:nil cancelButtonTitle:LS(@"no") otherButtonTitles:LS(@"yes"), nil];
            
            [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
                
                if (buttonIndex == 1) {
                    
                    if (self.innerMode) {
                        
                        
                        if (btn.tag < [_seriesArray count]) {
                            
                            [self deleteMediaItem:[_seriesArray objectAtIndex:btn.tag]];
                        }
                    } else {
                        
                        if (btn.tag < [_myZoneList count]) {
                            
                            [self deleteMediaItem:[_myZoneList objectAtIndex:btn.tag]];
                        }
                    }
                }
            }];
        }];
        
    }
    else
    {
        [cellView clearMediaCell];
    }
    
    return cellView;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark -

- (void)showHUD {
    
    [_zoneViewController showHUDBlockingUI:YES];
}

- (void)hideHUD {
    
    [_zoneViewController hideHUD];
}

- (void)showHUDBlockingUI:(BOOL) blockingUI
{
    [_zoneViewController showHUDBlockingUI:blockingUI];
}

#pragma mark -

- (void)deleteMediaItem:(TVMediaItem *)mediaItem {
    
}

- (void)deleteMediaItemComplete:(TVMediaItem *)mediaItem {
    
    [self hideHUD];
    
    if (self.innerMode) {
        
        [_seriesArray removeObject:mediaItem];
        
        [_collectionViewMyZoneInner reloadData];
        
        if ([_seriesArray count] < 2) {
            [_zoneViewController buttonBackPressed:_zoneViewController.buttonBack];
        }
        
        //remove action buttons
        if ([_seriesArray count] == 0)
            [_zoneViewController hideActionButtons];
        
    } else {
        
        [_myZoneList removeObject:mediaItem];
        
        [_collectionViewMyZone reloadData];
        
        [self updatePageControl];
        
        //remove action buttons
        if ([_myZoneList count] == 0) {
            [_zoneViewController hideActionButtons];
        }
        
        [_zoneViewController changeClearAllButtonEnabled:(self.myZoneList.count > 1)];
    }
}

- (void)clearAllItems {
    
}

- (void)clearAllItemsComplete {
    
    [self hideHUD];
    
    if (self.innerMode) {
        
        [_seriesArray removeAllObjects];
        
        [_collectionViewMyZoneInner reloadData];
    
        [_zoneViewController buttonBackPressed:_zoneViewController.buttonBack];
        
    } else {
    
        [_myZoneList removeAllObjects];
        
        [_collectionViewMyZone reloadData];
        
        [self showEmptyScreen];
        
        [_zoneViewController changeEditButtonEnabled:NO];
    }
}

#pragma mark -

- (void)addingPagerViewToFullScreen
{
    CGRect pageControlFrame = self.viewToPageControl.bounds;
    self.viewPageControl = [[PageControlView alloc] initWithFrame:pageControlFrame];
    [self.viewPageControl.control addTarget:self action:@selector(pageControlChanged:) forControlEvents:UIControlEventValueChanged];
    self.viewPageControl.numberOfPages = 0;
    [self.viewToPageControl addSubview:self.viewPageControl];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (isPad)
    {
        
        CGFloat pageWidth = CGRectGetWidth(scrollView.frame);
        int page = scrollView.contentOffset.x / pageWidth;
        [self.viewPageControl setCurrentPage:page];
    }
}

- (void)pageControlChanged:(id)sender
{
    if (isPad)
    {
        UIPageControl *pageControl = sender;
        CGFloat pageWidth = _collectionViewMyZone.frame.size.width;
        CGPoint scrollTo = CGPointMake(pageWidth * pageControl.currentPage, 0);
        [_collectionViewMyZone setContentOffset:scrollTo animated:YES];
    }
}

#pragma mark -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)updateEditButtonsUI
{
    [self.editBtn setTitle:LS(@"edit") forState:UIControlStateNormal];
    [self.editBtn setTitleColor:CS(@"B2B2B2") forState:UIControlStateNormal];
    
    [self.clearAllBtn setTitle:LS(@"clear_all") forState:UIControlStateNormal];
    [self.clearAllBtn setTitleColor:CS(@"B2B2B2") forState:UIControlStateNormal];
    
    [Utils setButtonFont:self.editBtn bold:NO];
    [Utils setButtonFont:self.clearAllBtn bold:NO];
    
    [self.editBtn setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"4e4e4e")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    [self.editBtn setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateHighlighted];
    [self.editBtn setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    [self.editBtn setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [self.editBtn setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    
    [self.clearAllBtn setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"4e4e4e")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    [self.clearAllBtn setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateHighlighted];
    [self.clearAllBtn setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    [self.clearAllBtn setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [self.clearAllBtn setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
}

-(void)exitFromSeriesMode
{
    // implament in sub class.
}

-(void)enterToSeriesMode
{
    // implament in sub class.
}

- (void)initSelectedBrandButtonUI:(UIButton *)brandButton {
    
    UIImage *img = brandButton.imageView.image;
    if (img) {
        
        UIImage *coloredBrandImage = [img imageTintedWithColor:APST.brandTextColor];
        [brandButton setImage:coloredBrandImage forState:UIControlStateHighlighted];
        [brandButton setImage:coloredBrandImage forState:UIControlStateSelected];
        
    }
    
    UIImage *brandImage = [UIImage imageNamed:@"brand_button"];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:APST.brandColor];
    UIImage *sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateHighlighted];
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateSelected];
    
    coloredBrandImage = [brandImage imageTintedWithColor:CS(@"4e4e4e")];
    sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateNormal];
    
    [brandButton setTitleColor:CS(@"b2b2b2") forState:UIControlStateNormal];
    [brandButton setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    [brandButton setTitleColor:APST.brandTextColor forState:UIControlStateSelected];
    [Utils setButtonFont:brandButton bold:YES];
    
}

- (void)updateIPadButton:(UIButton *)button {
    
    [Utils setButtonFont:button bold:YES];
    
    [button setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:CS(@"4e4e4e")] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    
    [button setTitleColor:CS(@"bebebe") forState:UIControlStateNormal];
    
}


-(void) setupPageTitle
{

    self.zoneViewController.labelTitle.text = self.zoneViewController.titleString;

    if (self.zoneViewController.imageTitle)
    {
        self.zoneViewController.imageTitle.image = nil;
        [self.zoneViewController.viewNavigationBar layoutIfNeeded];
    }
    
}
@end
