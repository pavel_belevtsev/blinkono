//
//  MyZoneViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 22.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneViewController.h"
#import "MyZoneInnerViewController.h"
#import "MyZoneAnimatorPop.h"
#import "MyZoneAnimatorPush.h"
#import "UIImage+Tint.h"
#import "UIAlertView+Blocks.h"
#import "AnalyticsManager.h"
#import "myZoneRecordingsViewController.h"
#import "MyZoneTabCollectionViewCell.h"


@interface MyZoneViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>

@property UINavigationController  *currentDetailViewController;
@property (strong, nonatomic) MyZoneAnimatorPop *animatorPop;
@property (strong, nonatomic) MyZoneAnimatorPush *animatorPush;

@property (nonatomic, retain) NSLayoutConstraint *constraintButtonsLeft;

@property (nonatomic, weak) IBOutlet UILabel *labelAutomation;

@end

@implementation MyZoneViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupTitleConstraints];
    /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
    if (!APST.fbSupport && [LoginM isSignedIn]) {
        self.MyZonePages = [NSMutableArray arrayWithArray:APST.myZoneItems];
        NSMutableArray *discardedItems = [NSMutableArray array];
        NSDictionary *item;
        
        for (item in _MyZonePages) {
            if ([[item objectForKey:@"localizationKey"] isEqualToString:@"my_zone_tab_friends"])
                [discardedItems addObject:item];
        }
        [_MyZonePages removeObjectsInArray:discardedItems];
    }

    if ([NU(UIUnit_MyTV_RecordTab) isUIUnitExist]) {
        [self.MyZonePages insertObject:@{@"name":@"Recordings",
                                         @"vcClassName":@"MyZoneRecordingsViewController",
                                         @"localizationKey":@"my_zone_tab_recordings"} atIndex:0];
        
    }

    if ([NU(UIUnit_MyTV_DownloadsTab) isUIUnitExist]) {
        [self.MyZonePages insertObject:@{@"name":@"Downloads",
                                         @"subType": @(kMyZoneViewControllerSubTypeShowAllPermittedItems),
                                         @"vcClassName":@"MyZoneDownloadsViewController",
                                         @"localizationKey":@"Downloads"} atIndex:0];
        
    }


    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [Utils setLabelFont:_labelTitle bold:YES];
    
    self.animatorPop = [MyZoneAnimatorPop new];
    self.animatorPush = [MyZoneAnimatorPush new];
    
    if (isPad) {
        [self createActionButtons];
        [self initSelectedBrandButtonUI:_buttonEditIpad];
        [self initSelectedBrandButtonUI:_buttonClearAllIpad];
        [self initSelectedBrandButtonUI:_buttonFullPageIpad];
        
        [Utils setButtonFont:_buttonBack bold:YES];
        [_buttonBack setTitle:LS(@"back") forState:UIControlStateNormal];

    } else {
        [_buttonEdit setTitle:LS(@"edit") forState:UIControlStateNormal];
        [_buttonClearAll setTitle:LS(@"clear_all") forState:UIControlStateNormal];

        [self initSelectedBrandButtonUI:_buttonEdit];
        [self initClearAllButtonUI:_buttonClearAll];
    }

    _buttonBack.alpha = 0.0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authenticationCompleted) name:KAuthenticationCompletedNotification object:nil];
    
    [self.collectionViewTabs registerNib:[UINib nibWithNibName:@"MyZoneTabCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"MyZoneTabCollectionViewCell"];
    [self openFriensActivity];
    
    if (isPhone)
    {
        self.myZoneSegmentedControl.transform = CGAffineTransformMakeScale(.8f, .8f);
//        self.imageViewGradientShadowLeft.transform = CGAffineTransformRotate(self.imageViewGradientShadowLeft.transform, M_PI);

        self.navigationView.backgroundColor = CS(@"4f4f4f");
    }
    self.navigationView.hidden = [APP_SET isOffline];
    
    self.labelAutomation.isAccessibilityElement = YES;
    self.labelAutomation.accessibilityLabel = @"Label Automation";
    self.labelAutomation.accessibilityValue = @"";
    
}

- (void)initSelectedBrandButtonUI:(UIButton *)brandButton {
    [super initSelectedBrandButtonUI:brandButton];
    
    UIImage *brandImage = [UIImage imageNamed:@"brand_button"];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:CS(@"575757")];
    UIImage *sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateNormal];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    self.selectedItemIndex =[NSNumber numberWithInteger:0];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KAuthenticationCompletedNotification object:nil];
    
}

- (BOOL) isMyZoneFullPad {
    return [[self.storyboard valueForKey:@"name"] isEqualToString:@"MyZoneFullPad"];
}

- (void) createActionButtons {
    __block MyZoneViewController *weakSelf = self;
    _buttonFullPageIpad = [UIButton buttonWithType:UIButtonTypeCustom];
    _buttonFullPageIpad.translatesAutoresizingMaskIntoConstraints = NO;
    [_buttonFullPageIpad setTouchUpInsideAction:^(UIButton *btn) {
        [weakSelf openMyZoneFullPage:btn];
    }];
    [_buttonFullPageIpad setContentEdgeInsets:UIEdgeInsetsMake(0, 16, 0, 16)];
    [self.viewContent addSubview:_buttonFullPageIpad];

    _buttonClearAllIpad = [UIButton buttonWithType:UIButtonTypeCustom];
    _buttonClearAllIpad.translatesAutoresizingMaskIntoConstraints = NO;
    [_buttonClearAllIpad setTitle:LS(@"clear_all") forState:UIControlStateNormal];
    [_buttonClearAllIpad setTouchUpInsideAction:^(UIButton *btn) {
        [weakSelf buttonClearAllPressed:btn];
    }];
    [_buttonClearAllIpad setContentEdgeInsets:UIEdgeInsetsMake(0, 16, 0, 16)];
    [self.viewContent addSubview:_buttonClearAllIpad];
    _buttonClearAllIpad.hidden = TRUE;
    
    _buttonEditIpad = [UIButton buttonWithType:UIButtonTypeCustom];
    _buttonEditIpad.translatesAutoresizingMaskIntoConstraints = NO;
    [_buttonEditIpad setTitle:LS(@"edit") forState:UIControlStateNormal];
    [_buttonEditIpad setTouchUpInsideAction:^(UIButton *btn) {
        [weakSelf buttonEditPressed:btn];
    }];
    
    [_buttonEditIpad setContentEdgeInsets:UIEdgeInsetsMake(0, 16, 0, 16)];
    [self.viewContent addSubview:_buttonEditIpad];
    _buttonEditIpad.titleLabel.font = _buttonFullPageIpad.titleLabel.font = _buttonClearAllIpad.titleLabel.font = Font_Bold(14);
    _buttonEditIpad.hidden = TRUE;

    
    UIView *trailingView = [UIView new];
    trailingView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.viewContent addSubview:trailingView];
    
    NSDictionary *metrics = @{@"height": @27, @"bottomSpace": @60, @"bottomSpaceFullPage": @35};
    [self.viewContent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[trailingView][_buttonFullPageIpad][_buttonEditIpad]-10-[_buttonClearAllIpad]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_buttonEditIpad, _buttonClearAllIpad, _buttonFullPageIpad, trailingView)]];
    [self.viewContent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_buttonEditIpad(height)]-(bottomSpace)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_buttonEditIpad)]];
    [self.viewContent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_buttonFullPageIpad(height)]-(bottomSpaceFullPage)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_buttonFullPageIpad)]];
    [self.viewContent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_buttonClearAllIpad(height)]-(bottomSpace)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_buttonClearAllIpad)]];

    [self.viewContent addConstraint:_constraintButtonsLeft = [NSLayoutConstraint constraintWithItem:trailingView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:0]];
    
    if ([self isMyZoneFullPad] || isPhone) {
        _constraintButtonsLeft.constant = 0;
        _buttonFullPageIpad.hidden = TRUE;
    }
    else {
        _constraintButtonsLeft.constant = 20;
    }
}

- (void) resetActionButtons {
    _buttonClearAllIpad.hidden = TRUE;
    _buttonClearAll.hidden = TRUE;
    
    //deselect edit button
    if (isPad) {
        if (self.buttonEditIpad.selected)
            [self buttonEditPressed:self.buttonEditIpad];
    }
    else {
        if (self.buttonEdit.selected)
            [self buttonEditPressed:self.buttonEdit];
    }
    
    MyZoneInnerViewController *controller = (MyZoneInnerViewController *)(_currentDetailViewController.visibleViewController);
    if ([controller isKindOfClass:MyZoneInnerViewController.class] && controller.zoneEmptyView.superview) {
        _buttonEditIpad.hidden = TRUE;
        _buttonEdit.hidden = TRUE;
    }
}

- (void) hideActionButtons {
    [self resetActionButtons];
    [self.buttonEditIpad setHidden:TRUE];
}

- (void)setButtonEditTitle:(NSString *)title {
    
    if (isPad) {
        [_buttonEditIpad setTitle:title forState:UIControlStateNormal];
        [_buttonEditIpad setTitle:title forState:UIControlStateHighlighted];
        [_buttonEditIpad setTitle:title forState:UIControlStateSelected];
    }
    else {
        [_buttonEdit setTitle:title forState:UIControlStateNormal];
        [_buttonEdit setTitle:title forState:UIControlStateHighlighted];
        [_buttonEdit setTitle:title forState:UIControlStateSelected];
    }

}

- (void)setButtonFullPageTitle:(NSString *)title {
    
    [_buttonFullPageIpad setTitle:title forState:UIControlStateNormal];
    [_buttonFullPageIpad setTitle:title forState:UIControlStateHighlighted];
    [_buttonFullPageIpad setTitle:title forState:UIControlStateSelected];
}

- (void)initEditButtons:(BOOL)visible editMode:(BOOL)editMode {
    
    [self initEditButtons:visible editMode:editMode clearAll:NO];
    
}

- (void)initEditButtons:(BOOL)visible editMode:(BOOL)editMode clearAll:(BOOL)clearAll {
    
    [self setButtonEditTitle:editMode ? (isPhone ? LS(@"done") : LS(@"done_editing")) : LS(@"edit")];
    _buttonEdit.selected = NO;
    _buttonEdit.hidden = !visible;
    
    _buttonEditIpad.selected = NO;
    _buttonEditIpad.hidden = !visible;
    
    _buttonEdit.selected = editMode;
    _buttonEditIpad.selected = editMode;

    _buttonClearAll.alpha = (clearAll ? 1.0 : 0.0);
    _buttonClearAll.hidden = !_buttonEdit.selected;
    _buttonClearAllIpad.alpha = (clearAll ? 1.0 : 0.0);
    _buttonClearAllIpad.hidden = !_buttonEditIpad.selected;
}

- (void)changeEditButtonEnabled:(BOOL)enabled {
    [self changeEditButtonEnabled:enabled allowForMini:FALSE];
}

- (void)changeClearAllButtonEnabled:(BOOL)enabled {
    _buttonClearAllIpad.hidden = !enabled;
    _buttonClearAll.hidden = !enabled;
}

- (void)changeEditButtonEnabled:(BOOL)enabled allowForMini:(BOOL)allowForMini {

    MyZoneInnerViewController *controller = (MyZoneInnerViewController *)(_currentDetailViewController.visibleViewController);
    if ([controller inEditMode]) {
        [self buttonEditPressed:_buttonEdit];
    }
    _buttonEdit.alpha = (enabled ? 1.0 : 0.0);
    _buttonEdit.hidden = !enabled;
    if (isPad) {
        _buttonEditIpad.hidden = !enabled;
        if ([[self.storyboard valueForKey:@"name"] isEqualToString:@"MyZonePad"] && !allowForMini) {
            _buttonEditIpad.hidden = TRUE;
        }
    }
}

-(void) relaodButtomMyzoneTabs
{
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self menuContainerViewController].panMode = MFSideMenuPanModeDefault;
    
    [self menuContainerViewController].rightMenuViewController = nil;
    
    if (!isInitialized) {
        
        isInitialized = YES;
        
        [self.collectionViewTabs reloadData];
        
        _buttonEdit.userInteractionEnabled = [LoginM isSignedIn];
        
        if ([LoginM isSignedIn] || [APP_SET isOffline])
        {
            
            _collectionViewTabs.userInteractionEnabled = YES;
            [self selectTabAtIndex:[self.selectedItemIndex integerValue]];
            NSDictionary *item = self.MyZonePages[[self.selectedItemIndex integerValue]];
            NSString *itemName = item[@"name"];
            
            [AnalyticsM trackScreen:itemName];
            
            MyZoneInnerViewController *controller = [[ViewControllersFactory defaultFactory] myZoneInnerViewControllerWithId:item[@"vcClassName"] andMyZoneType:[self.storyboard valueForKey:@"name"]];
            controller.subType = (item[@"subType"]) ? [item[@"subType"] integerValue] : kMyZoneViewControllerSubTypeShowAll;
            controller.zoneViewController = self;
            UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:controller];
            navigation.navigationBarHidden = YES;
            navigation.delegate = self;
            if (isPhone)
            {
                [self updateTopNavigationView:controller];
            }
            [self presentDetailController:navigation];
            
        } else {
            
            _collectionViewTabs.userInteractionEnabled = NO;
            
            if (!_viewLogin){
                UIView *black = [[UIView alloc] initWithFrame:self.view.bounds];
                black.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
                black.userInteractionEnabled = NO;
                _viewLogin = [[LoginInterMediateView alloc] initWithNib];
               
                if (isPhone)
                {
                    _viewLogin.frame =  [self.view convertRect:self.innerView.frame fromView:self.viewContent];
                    [self.view addSubview:black];
                    [self.view addSubview:_viewLogin];
                    [self.view bringSubviewToFront:self.noInternetView];
                    
                    self.buttonEdit.hidden = YES;
                }
                else
                {
                    if([self isMyZoneFullPad])
                    {
                         [self.innerView addSubview:_viewLogin];
                        _viewLogin.frame = self.view.frame;
                        _viewLogin.center = self.view.center;
                        _viewLogin.scrollViewLogin.center = self.view.center;
                    }
                    else
                    {
                        [self.view addSubview:black];
                        [self.view addSubview:_viewLogin];
                        _viewLogin.frame =  self.view.frame;
                    }
                }
                
                if (_delegateController) {
                    _viewLogin.delegateController = _delegateController;
                } else {
                    _viewLogin.delegateController = self;
                }
                
            }
            [self initBrandButtonUI:_viewLogin.buttonLogin];
            [_viewLogin.buttonLogin setTitle:[NSString stringWithFormat:LS(@"login_brand"), APST.brandName] forState:UIControlStateNormal];
            
            [self initFacebookButtonUI:_viewLogin.buttonFacebook];
            [_viewLogin.buttonFacebook setTitle:LS(@"login_with_facebook") forState:UIControlStateNormal];
            
            if (isPhone){
                CGPoint center = _viewLogin.buttonLogin.center;
                _viewLogin.buttonLogin.frame = CGRectMake(_viewLogin.buttonLogin.frame.origin.x , _viewLogin.buttonLogin.frame.origin.y , self.view.frame.size.width*0.85, _viewLogin.buttonLogin.frame.size.height);
                _viewLogin.buttonLogin.center = center;

                center = _viewLogin.buttonFacebook.center;
                _viewLogin.buttonFacebook.frame = CGRectMake(_viewLogin.buttonFacebook.frame.origin.x , _viewLogin.buttonFacebook.frame.origin.y , self.view.frame.size.width*0.85, _viewLogin.buttonFacebook.frame.size.height);
                _viewLogin.buttonFacebook.center = center;

                /* "PSC : Vodafone Grup : OPF-645 Facebook Detachments Task [IOS] */
                if (!APST.fbSupport) {
                    [_viewLogin.buttonLogin setFrame:_viewLogin.buttonFacebook.frame];
                    [_viewLogin.buttonFacebook setHidden:YES];
                }
            }else{
                if (!APST.fbSupport) {
                    CGRect loginButtonFrame = _viewLogin.buttonLogin.frame;
                    loginButtonFrame.origin.x = (_viewLogin.frame.size.width/2) - (_viewLogin.buttonLogin.frame.size.width/2);
                    [_viewLogin.buttonLogin setFrame:loginButtonFrame];
                    [_viewLogin.buttonFacebook setHidden:YES];
                }
            }
            
            [Utils setLabelFont:_viewLogin.labelLogin bold:YES];
            _viewLogin.labelLogin.text = LS(@"my_zone_anonymous_sub_title");
            
            _viewLogin.hidden = NO;
            _viewLogin.alpha = 1.0;
           _viewLogin.buttonFacebook.enabled = YES;
            _viewLogin.buttonLogin.enabled = YES;
            
            _buttonClearAllIpad.hidden = YES;
            _buttonEditIpad.hidden = YES;
            
            self.labelAutomation.accessibilityValue = @"MyZone Login Screen";
            
        }
    }

}


-(void) closeMyZoneBeforeOpeningPlayer
{
    if (isPad && NavM.container.centerViewController) {
        BaseViewController *controller = [[NavM.container.centerViewController viewControllers] lastObject];
        [controller closeMyZone];
    }
    
   
}


-(void) didSelectActivityCellWithIndex:(NSInteger) index
{
    
}

-(void)openMediaItemWithMediaId:(NSString *)mediaID
{
    [self closeMyZoneBeforeOpeningPlayer];
    
    [super openMediaItemWithMediaId:mediaID];
    [AnalyticsM trackScreenSelectedItem:LS(@"screen_title_my_zone") item:mediaID];

}

- (void)openMediaItem:(TVMediaItem *)mediaItem {
    
    [self closeMyZoneBeforeOpeningPlayer];
    
    [super openMediaItem:mediaItem];
    [AnalyticsM trackScreenSelectedItem:LS(@"screen_title_my_zone") item:mediaItem.name];
}



#pragma mark - Button

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [[self menuContainerViewController] toggleLeftSideMenuCompletion:^{}];
}

- (void)buttonNavigationPressed:(UIButton *)button {

    for (UIButton *buttonInner in [_navigationView subviews]) {
        buttonInner.enabled = (button.tag != buttonInner.tag);
    }
    
    [self selectTabAtIndex:button.tag];
    

}

-(void) selectTabAtIndex:(NSInteger ) index
{
    [self resetActionButtons];
    
    self.selectedItemIndex = [NSNumber numberWithInteger:index];
    
    [self.collectionViewTabs selectItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    
    NSDictionary *item = self.MyZonePages[index];
    
    [AnalyticsM trackScreen:item[@"name"]];
    
    MyZoneInnerViewController *vc = nil;
    
    for (UIViewController *controller in _currentDetailViewController.viewControllers) {
        if ([controller isKindOfClass:NSClassFromString(item[@"vcClassName"])]) {
            
            if ([controller isKindOfClass:[MyZoneInnerViewController class]]) {
                vc = (MyZoneInnerViewController*)controller;
                [vc setSubType: (item[@"subType"]) ? [item[@"subType"] integerValue] : kMyZoneViewControllerSubTypeShowAll];
                if (!vc.myZoneList.count)
                    [self changeEditButtonEnabled:NO];
            }
            
            if (isPhone)
            {
                [self updateTopNavigationView:controller];
            }

            //update bg color to controller inner view bg
            [[self viewContent] setBackgroundColor:[controller.view backgroundColor]];
            [_currentDetailViewController popToViewController:controller animated:YES];
            
            if(vc!=nil){
                [self updateBackAndMenuButtons:vc];
            }
            
            return;
        }
    }
    
    MyZoneInnerViewController *controller = [[ViewControllersFactory defaultFactory] myZoneInnerViewControllerWithId:item[@"vcClassName"] andMyZoneType:[self.storyboard valueForKey:@"name"]];
    controller.subType = (item[@"subType"]) ? [item[@"subType"] integerValue] : kMyZoneViewControllerSubTypeShowAll;
    controller.zoneViewController = self;
    
    //update bg color to controller inner view bg
    [[self viewContent] setBackgroundColor:[controller.view backgroundColor]];
    
    if (isPhone)
    {
        [self updateTopNavigationView:controller];
    }
    
    [_currentDetailViewController pushViewController:controller animated:YES];
    [self updateBackAndMenuButtons:controller];
}

- (void)updateBackAndMenuButtons:(MyZoneInnerViewController*) innerViewController
{
    [UIView animateWithDuration:kViewAnimationDuration animations:^{
        
        if (innerViewController.innerMode) {
            self.buttonBack.alpha = 1.0;
            self.buttonMenu.alpha = 0.0;
        }else{
            
            self.buttonBack.alpha = 0.0;
            self.buttonMenu.alpha = 1.0;
        }
    }];

}

- (IBAction)buttonBackPressed:(UIButton *)button
{
    MyZoneInnerViewController *controller = (MyZoneInnerViewController *)(_currentDetailViewController.visibleViewController);
    if ([controller inEditMode])
    {
        [self buttonEditPressed:(isPad) ? _buttonEditIpad : _buttonEdit];
    }
    else if ([controller isKindOfClass:[myZoneRecordingsViewController class]] )
    {
        if([self isMyZoneFullPad] || isPhone)
        {
            [self exitFromSeriesMode:button];
            [UIView animateWithDuration:kViewAnimationDuration animations:^{
                _buttonBack.alpha = 0.0;
                _buttonMenu.alpha = 1.0;
            }];

            return;
        }
    }
    [UIView animateWithDuration:kViewAnimationDuration animations:^{
        _buttonBack.alpha = 0.0;
        _buttonMenu.alpha = 1.0;
    }];
    
    [controller backToMainList:YES];
    
}

- (IBAction)buttonEditPressed:(UIButton *)button {
    
    if ([button.titleLabel.text isEqualToString:LS(@"full_page")])
    {
        return;
    }
    else
    {
        
        BOOL newSelectionState = !button.selected;
        _buttonEdit.selected = newSelectionState;
        _buttonEditIpad.selected = newSelectionState;
        _buttonClearAll.hidden = !newSelectionState;
        
        [self setButtonEditTitle:button.selected ? (isPhone ? LS(@"done") : LS(@"done_editing")) : LS(@"edit")];
     
        MyZoneInnerViewController *controller = (MyZoneInnerViewController *)(_currentDetailViewController.visibleViewController);
        [controller changeEditMode:button.selected];
    }
    [button layoutIfNeeded];
}

- (IBAction)buttonClearAllPressed:(UIButton *)button {

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"my_zone_delete_all_items_message") delegate:nil cancelButtonTitle:LS(@"no") otherButtonTitles:LS(@"yes"), nil];
    
    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        
        if (buttonIndex == 1) {
            
            MyZoneInnerViewController *controller = (MyZoneInnerViewController *)(_currentDetailViewController.visibleViewController);
            [controller clearAllItems];
        }
    }];
}

#pragma mark - Navigation

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    
    if (operation == UINavigationControllerOperationPop) {
        return self.animatorPop;
    } else if (operation == UINavigationControllerOperationPush) {
        return self.animatorPush;
    }
    
    return nil;
}

- (void)presentDetailController:(UINavigationController *)detailVC {
    
    if (self.currentDetailViewController){
        [self removeCurrentDetailViewController];
    }
    
    [self addChildViewController:detailVC];
    
    detailVC.view.frame = [self frameForDetailController];
    
    [self.innerView addSubview:detailVC.view];
    self.currentDetailViewController = detailVC;
    
    [detailVC didMoveToParentViewController:self];
    [self openFriensActivity];

}

- (void)removeCurrentDetailViewController {
    
    [self.currentDetailViewController willMoveToParentViewController:nil];
    [self.currentDetailViewController.view removeFromSuperview];
    [self.currentDetailViewController removeFromParentViewController];
    
}

- (CGRect)frameForDetailController {
    
    CGRect detailFrame = self.innerView.bounds;
    return detailFrame;
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setActivityFeedLoadOnStart:(BOOL)activityFeedLoadOnStart{
    
    _activityFeedLoadOnStart = activityFeedLoadOnStart;
//    [self openFriensActivity];
}

-(void) openFriensActivity{
    if (_activityFeedLoadOnStart){
        [self reOpenFriensActivity];
    //    _activityFeedLoadOnStart = NO;
    }
}

- (void)authenticationCompleted {

    _collectionViewTabs.userInteractionEnabled = YES;
    
    if ([LoginM isFacebookUser]){
        _activityFeedLoadOnStart = YES;
        [self reOpenFriensActivity];
    }
}

-(void) reOpenFriensActivity {
    
    for (int i = 0; i < [_MyZonePages count]; i++) {
        NSDictionary *item = [_MyZonePages objectAtIndex:i];
        if ([[item objectForKey:@"localizationKey"] isEqualToString:@"my_zone_tab_friends"]) {
            UIButton* activityFeedButton = [UIButton buttonWithType:UIButtonTypeCustom];
            activityFeedButton.tag = i;
            [self buttonNavigationPressed:activityFeedButton];
            
            [_collectionViewTabs scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:NO];

        }
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)exitFromSeriesMode:(id)sender {
    self.buttonBack.hidden = YES;
    self.buttonMenu.hidden = NO;
    self.buttonBack.alpha = 1.0;
    self.buttonMenu.alpha = 1.0;
    MyZoneInnerViewController *controller = (MyZoneInnerViewController *)(_currentDetailViewController.visibleViewController);
    [controller exitFromSeriesMode];
}

- (void)enterToSeriesMode
{
    self.buttonBack.alpha = 1.0;
    self.buttonMenu.alpha = 1.0;

    self.buttonBack.hidden = NO;
    self.buttonMenu.hidden = YES;
}

-(void)updateEditButtonInMiniMyZone
{
    [self setButtonFullPageTitle:LS(@"Full Page")];
    _buttonEditIpad.hidden = YES;
    _buttonClearAllIpad.hidden = YES;
}

-(void)updateEditButtonRecordingMyZone
{
    _buttonEdit.hidden = YES;
    _buttonEditIpad.hidden = YES;
    _buttonClearAllIpad.hidden = YES;
}

-(void)openMyZoneFullPage:(id)sender
{
    if (isPad && NavM.container.centerViewController) {
        
        BaseViewController *controller = [[NavM.container.centerViewController viewControllers] lastObject];
        [controller closeMyZone];
    }
    MyZoneViewController *controller = [[ViewControllersFactory defaultFactory] myZoneFullScreenViewController];
    NSInteger  rcoredItemIndex = [self.selectedItemIndex integerValue];
    
    if (rcoredItemIndex >-1 && rcoredItemIndex < [self.MyZonePages count])
    {
        controller.selectedItemIndex = [NSNumber numberWithInteger:rcoredItemIndex];
    }
    [NavM.container.centerViewController pushViewController:controller animated:YES];
}

-(void)updateTopNavigationView:(UIViewController *)controller
{
    
        NSInteger t = self.topNavigationView.frame.origin.x + self.topNavigationView.frame.size.height;
        CGRect frame = self.topNavigationView.frame;
        frame.size.height = 44;
        self.topNavigationView.frame = frame;
        self.myZoneSegmentedControl.hidden = YES;
        
        CGRect frame1 = self.innerView.frame;
        frame1.origin.y = self.topNavigationView.frame.origin.y + self.topNavigationView.frame.size.height;
        frame.size.height = frame1.size.height + (t-frame1.origin.y);
        self.innerView.frame = frame1;

}

-(IBAction)chnageRecordingsLoad:(id)sender
{
    MyZoneInnerViewController *controller = (MyZoneInnerViewController *)(_currentDetailViewController.visibleViewController);
    if ([controller isKindOfClass:[myZoneRecordingsViewController class]])
    {
         myZoneRecordingsViewController * vc = (myZoneRecordingsViewController*)controller;
        [vc chnageRecordingsLoad:sender];
    }

}

#pragma mark - collection view methods
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary * myZoneItem =  self.MyZonePages[indexPath.row];
    
    MyZoneTabCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyZoneTabCollectionViewCell" forIndexPath:indexPath];
    
    cellView.menuItem = myZoneItem;
    
    if (indexPath.row != 0)
    {
        cellView.leftSeperator.hidden = YES;
    }
    
    [self updateTabBarShadowEffect:collectionView];
    
    return cellView;
}

-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * myZoneItem =  self.MyZonePages[indexPath.row];
    NSString * tabName = myZoneItem[@"name"];
    if ([tabName isEqualToString:@"Recordings"])
    {
        return [UICollectionViewCell shouldSelectCellForUIUnit:NU(UIUnit_MyTV_RecordTab)];
    };
    
    return YES;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   return self.MyZonePages.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size;
    if(isPad){
        size = CGSizeMake(self.collectionViewTabs.size.width/self.MyZonePages.count, self.collectionViewTabs.size.height);
    }else{
        NSString * title = LS(self.MyZonePages[indexPath.row][@"localizationKey"]);
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:14]};
        size = [title sizeWithAttributes:attributes];
        size.width += 20;
        size.height = self.collectionViewTabs.size.height;
    }
    return size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self isMyZoneFullPad] || isPhone)
    {
        self.buttonFullPageIpad.hidden = YES;
        _constraintButtonsLeft.constant = 0;
        
        self.buttonEdit.hidden = NO;
        self.buttonEditIpad.hidden = NO;
    }
    else
    {
        self.buttonFullPageIpad.hidden = NO;
        _constraintButtonsLeft.constant = 20;

        self.buttonEdit.hidden = YES;
        self.buttonEditIpad.hidden = YES;
    }
    
    [self selectTabAtIndex:indexPath.row];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}
#pragma mark - scroll view delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(isPhone)
        [self updateTabBarShadowEffect:scrollView];
}

- (void) updateTabBarShadowEffect:(UIScrollView *)scrollView
{
    
    int scrollLeftOffset = scrollView.contentOffset.x;
    int scrollRightOffset = scrollView.contentSize.width-scrollView.frame.size.width-scrollView.contentOffset.x;
    
    int shadowLeftWidth = MIN(scrollLeftOffset, 44);
    int shadowRightWidth = MIN(scrollRightOffset, 44);

    self.imageViewGradientShadowLeft.frame = CGRectMake(0, 0, shadowLeftWidth,self.imageViewGradientShadowLeft.frame.size.height);
    self.imageViewGradientShadowRight.frame  = CGRectMake(self.view.frame.size.width-shadowRightWidth, 0, shadowRightWidth,self.imageViewGradientShadowLeft.frame.size.height);

    

}

-(void) setupTitleConstraints
{

    if (self.imageTitle)
    {
        self.imageTitle.translatesAutoresizingMaskIntoConstraints = NO;
        self.labelTitle.translatesAutoresizingMaskIntoConstraints = NO;

        [self.viewNavigationBar addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.viewNavigationBar attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0]];

        [self.viewNavigationBar addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.viewNavigationBar attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0]];


        [self.viewNavigationBar addConstraint:[NSLayoutConstraint constraintWithItem:self.labelTitle attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.imageTitle attribute:NSLayoutAttributeRight multiplier:1.0f constant:10]];
        [self.viewNavigationBar addConstraint:[NSLayoutConstraint constraintWithItem:self.imageTitle attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.viewNavigationBar attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0]];
        [self.viewNavigationBar layoutIfNeeded];
    }
}

@end
