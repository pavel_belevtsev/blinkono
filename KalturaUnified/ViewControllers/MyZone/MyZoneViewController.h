//
//  MyZoneViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 22.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"
@class LoginInterMediateView;

@protocol MyZoneViewControllerDelegate <NSObject>

- (void)intermediateLogin:(nullable NSString *)segue;

@end

@interface MyZoneViewController : BaseViewController <UINavigationControllerDelegate> {
    
    BOOL isInitialized;
    
}

- (void)initEditButtons:(BOOL)visible editMode:(BOOL)editMode;
- (void)initEditButtons:(BOOL)visible editMode:(BOOL)editMode clearAll:(BOOL)clearAll;
- (void)changeEditButtonEnabled:(BOOL)enabled allowForMini:(BOOL)allowForMini;
- (void)changeEditButtonEnabled:(BOOL)enabled;
- (void)changeClearAllButtonEnabled:(BOOL)enabled;
- (IBAction)buttonEditPressed:(UIButton * __nullable)button;
- (IBAction)buttonBackPressed:(UIButton * __nullable)button;
- (void) hideActionButtons;
- (BOOL) isMyZoneFullPad;

@property (weak, nonatomic) IBOutlet UIImageView  * __nullable imageTitle;
@property (weak, nonatomic) IBOutlet UILabel * __nullable labelTitle;
@property (weak, nonatomic) IBOutlet UIView * __nullable innerView;
@property (weak, nonatomic) IBOutlet UIButton * __nullable buttonEdit;
@property (weak, nonatomic) IBOutlet UIButton * __nullable buttonClearAll;
@property (weak, nonatomic) IBOutlet UIView * __nullable navigationView;
@property (nonatomic, weak) BaseViewController * __nullable delegateController;

@property (weak, nonatomic) IBOutlet UIView * __nullable bgView;

@property (weak, nonatomic) IBOutlet UIButton * __nullable buttonBack;
@property (weak, nonatomic) IBOutlet UIButton * __nullable buttonMenu;
@property (nonatomic, strong)  LoginInterMediateView * __nullable viewLogin;
@property (nonatomic, assign) BOOL activityFeedLoadOnStart;
@property (weak, nonatomic) IBOutlet UIView * __nullable topNavigationView;
@property (weak, nonatomic) IBOutlet UIImageView * __nullable topNavigationViewBgImage;
@property (weak, nonatomic) IBOutlet UISegmentedControl * __nullable myZoneSegmentedControl;

@property (strong, nonatomic) NSMutableArray * __nullable MyZonePages;
@property (weak, nonatomic) IBOutlet UICollectionView * __nullable collectionViewTabs;

@property (strong, nonatomic) NSNumber *  __nullable selectedItemIndex;
@property (strong, nonatomic) NSString *  __nullable titleString;
@property (weak, nonatomic) IBOutlet UIImageView * __nullable imageViewGradientShadowLeft;
@property (weak, nonatomic) IBOutlet UIImageView * __nullable imageViewGradientShadowRight;

@property (nonatomic, retain) UIButton * __nullable buttonFullPageIpad, * __nullable buttonEditIpad, * __nullable buttonClearAllIpad;
@property (nonnull, strong) IBOutlet UIView * viewFotter;

- (IBAction)exitFromSeriesMode:(id __nullable )sender;
- (void)enterToSeriesMode;
-(void)updateEditButtonInMiniMyZone;
-(void)updateTopNavigationView:(UIViewController * __nullable )controller;
-(void)updateEditButtonRecordingMyZone;

- (IBAction)openMyZoneFullPage:(id __nullable )sender;

@end
