//
//  MyZoneDownloadsViewController.m
//  KalturaUnified
//
//  Created by Alex Zchut on 4/29/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneDownloadsViewController.h"
#import "MyZoneDownloadCollectionViewCell.h"
#import "DTG_QuotaBarView.h"
#import "DTG_QuotaBarViewToolTip.h"

#import "TVMediaItem+Additions.h"

@interface MyZoneDownloadsViewController () {
    BOOL editMode;
}
@property (strong, nonatomic) DTG_QuotaBarView *quotaBarView;
@property (strong, nonatomic) DTG_QuotaBarViewToolTip *quotaBarViewToolTip;

@property (weak, nonatomic) IBOutlet UIView *quotaBarPlaceholder;

@property (nonatomic) BOOL isShowQuotaBar;

@end

@implementation MyZoneDownloadsViewController

- (void)viewDidLoad {
    
    self.loadNoInternetView = YES;

    [super viewDidLoad];
    // Do any additional setup after loading the view.
        
    UIImage * image = [UIImage imageNamed:@"empty_state_download_icon"];
    
    [super initEmptyScreen:NO title:LS(@"dtg_no_downloads_title") text:LS(@"dtg_no_downloads_subtitle") andImage:image];
    
    self.noInternetView.constraintTopSpace.constant = 0;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.iPhoneMainScrollView  setContentOffset:CGPointMake(0, 40) animated:NO];
    self.isShowQuotaBar = NO;
    [self addQuotaBarView];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self hideHUD];
    
    [self.quotaBarViewToolTip closeView:nil];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeEditMode:(BOOL)mode {
    editMode = mode;
    [self.zoneViewController changeClearAllButtonEnabled:(mode && self.myZoneList.count > 1)];
    [self.collectionViewMyZone reloadData];
}

- (void)reloadData {
    
    [self.zoneViewController initEditButtons:NO editMode:editMode clearAll:YES];
    self.myZoneList = [NSMutableArray array];
    [super reloadData];
    
    [self showHUDBlockingUI:NO];
    [[DTGManager sharedInstance] getAllItems:^(NSMutableArray *arrItems) {
        
        if (!arrItems.count) {
            [super showEmptyScreen];
            [self hideHUD];
        }
        else {
            NSMutableArray * arrMediaItems = [NSMutableArray array];

            if ([APP_SET isOffline]) {

                dispatch_group_t group = dispatch_group_create();
                for (KADownloadItem *item in arrItems) {
                    TVMediaItem *mediaItem = [TVMediaItem objectFromDataAtPath:[item.metadataFolderPath stringByAppendingPathComponent: [NSStringFromClass([TVMediaItem class]) stringByAppendingString:@".bin"]]];
                    
                    if (mediaItem) {
                        mediaItem.listType = kMediaItemBelongsToListTypeDownload;
                        mediaItem.downloadItemRef = item;
                        [arrMediaItems addObject:mediaItem];

                        if (item.downloadState == KADownloadItemDownloadStateDownloaded) {
                            dispatch_group_enter(group);
                            [item isLicenseValid:^(BOOL success, NSTimeInterval ti) {
                                if (success && ti > 0) {
                                    mediaItem.priceType = TVPriceTypeFree;
                                }
                                else {
                                    mediaItem.priceType = TVPriceTypeForPurchase;
                                }
                                
                                dispatch_group_leave(group);
                            }];
                        }
                    }
                }
                
                dispatch_group_notify(group, dispatch_get_main_queue(), ^{
                    [self hideHUD];
                    [self updateData:arrMediaItems useLive:NO];
                });
            }
            else {
                for (KADownloadItem *item in arrItems) {
                    TVMediaItem *mediaItem = [TVMediaItem objectFromDataAtPath:[item.metadataFolderPath stringByAppendingPathComponent: [NSStringFromClass([TVMediaItem class]) stringByAppendingString:@".bin"]]];
                    
                    if (mediaItem) {
                        mediaItem.listType = kMediaItemBelongsToListTypeDownload;
                        mediaItem.downloadItemRef = item;
                        [arrMediaItems addObject:mediaItem];
                    }
                }
                [MediaGRA getItemsPricesWithCouponsForDownloadedMedias:arrMediaItems delegate:self completion:^(NSArray *updatedMediaItems) {
                    [self hideHUD];
                    [self updateData:updatedMediaItems useLive:NO];
                }];
            }
        }
    }];
}

- (void)sortMediaArray:(NSMutableArray *)array {
    //do nothing
}

- (void)deleteMediaItem:(TVMediaItem *)mediaItem {
    [self showHUD];
    KADownloadItem *downloadItem = mediaItem.downloadItemRef;
    [[DTGManager sharedInstance] deleteItem:downloadItem completion:^(BOOL success) {
        [self hideHUD];
        [self deleteMediaItemComplete:mediaItem];
        [self updateQuotaBarView];
    }];
}

- (void)clearAllItems {
    [self showHUD];
    [[DTGManager sharedInstance] deleteAllItems:^(BOOL success) {
        [self hideHUD];
        [self clearAllItemsComplete];
        editMode = NO;
        [self.zoneViewController initEditButtons:NO editMode:editMode clearAll:YES];
        [self updateQuotaBarView];
    }];
}

- (MediaItemBelongsToListType) mediaListType {
    return kMediaItemBelongsToListTypeDownload;
}

#pragma mark - collection view
- (NSString*) reusableCellIdentifier {
    return NSStringFromClass(MyZoneDownloadCollectionViewCell.class);
}

- (void) defineControllerSettings {
    self.subType = kMyZoneViewControllerSubTypeShowAllPermittedItems;
    self.purchasesLoadType = kPurchasesManagerLoadItemsIDsOnly;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MyZoneDownloadCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:self.reusableCellIdentifier forIndexPath:indexPath];
    
    NSInteger index = indexPath.row;
    if(isPad && [self isMyZoneFullPad]) {
        if (index % zoneViewPadRowsCount == 0) {
            index = index / zoneViewPadRowsCount;
        }
        else {
            index = index / zoneViewPadRowsCount;
            index += zoneViewPadColsCount;
        }
        index += (indexPath.section * zoneViewPadItemsCount);
    }
    else if (isPad) {
        index = indexPath.section * 4 + indexPath.row;
    }
    
    if (index < [self.myZoneList count]) {
        [cell.contentView performBlockOnSubviewsKindOfClass:[UIView class] block:^(UIView *subview) {
            subview.hidden = FALSE;
        }];
        
        TVMediaItem *mediaItem = [self.myZoneList objectAtIndex:index];
        cell.delegate = self;
        [cell setItemDetails:mediaItem editMode:editMode];
    }
    else {
        [cell.contentView performBlockOnSubviewsKindOfClass:[UIView class] block:^(UIView *subview) {
            subview.hidden = TRUE;
        }];
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger index = indexPath.row;
    if(isPad && [self isMyZoneFullPad])
    {
        if (index % zoneViewPadRowsCount == 0) {
            index = index / zoneViewPadRowsCount;
        } else {
            index = index / zoneViewPadRowsCount;
            index += zoneViewPadColsCount;
        }
        
        index += (indexPath.section * zoneViewPadItemsCount);
    }
    else if (isPad)
    {
        index = indexPath.section * 4 + indexPath.row;
    }
    if (index < [self.myZoneList count]) {
        TVMediaItem *mediaItem = [self.myZoneList objectAtIndex:index];
        KADownloadItem *downloadItem = mediaItem.downloadItemRef;
        
        if (downloadItem.downloadState == KADownloadItemDownloadStateDownloaded) {
            [self openMediaItem:mediaItem];
        }
    }
}

#pragma mark - cell delegates
- (void) deleteCellMediaItem:(TVMediaItem*)item {
    [self deleteMediaItem:item];
}

- (UIView*) getContainerView {
    self.noInternetView.constraintTopSpace.constant = 0;
    return self.noInternetView;
}

#pragma mark - 

-(void)addQuotaBarView
{
    if (self.quotaBarView == nil)
    {
        self.quotaBarView = [DTG_QuotaBarView quotaBarView];
    }
    
    self.quotaBarView.translatesAutoresizingMaskIntoConstraints = NO;
    self.quotaBarView.alpha = 0.0;

    NSDictionary *metrics = nil;
    if (isPad)
    {
        if([self isMyZoneFullPad])
        {
            if(![self.quotaBarView isDescendantOfView:[self viewToPageControl]])
            {
                metrics = @{@"left": @10, @"bottom": @8};
                [self.viewToPageControl addSubview:self.quotaBarView];
                [self.quotaBarView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_quotaBarView]-(left)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_quotaBarView)]];
                [self.quotaBarView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_quotaBarView]-(bottom)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_quotaBarView)]];
            }
        }
        else
        {
            if(![self.quotaBarView isDescendantOfView:[self quotaBarPlaceholder]])
            {
                metrics = @{@"left": @10, @"bottom": @0};
                [self.quotaBarPlaceholder addSubview:self.quotaBarView];
                [self.quotaBarView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_quotaBarView]-(left)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_quotaBarView)]];
                [self.quotaBarView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_quotaBarView]-(bottom)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_quotaBarView)]];
            }
        }
    }
    else
    {
        if(![self.quotaBarView isDescendantOfView:[self quotaBarPlaceholder]])
        {
            metrics = @{@"left": @0, @"bottom": @8};
            [self.quotaBarPlaceholder addSubview:self.quotaBarView];
            [self.quotaBarView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_quotaBarView]-(left)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_quotaBarView)]];
            [self.quotaBarView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_quotaBarView]-(bottom)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_quotaBarView)]];
        }
    }

    
    [self updateQuotaBarView];
}

- (void) updateQuotaBarView {
    [[DTGManager sharedInstance] getCurrentQuotaBasedLibrarySize:^(long long currentLibrarySize, long long allowedLibrarySize, long long freeSpaceOnDevice) {
        
        [self.quotaBarView updateUIwithCurrentLibrarySize:currentLibrarySize allowedLibrarySize:allowedLibrarySize freeSpaceOnDevice:freeSpaceOnDevice];
        self.quotaBarView.alpha = 1.0;

        NSInteger usedSpaceInPercent = MIN(100,ceil(100*((double)currentLibrarySize/(double)allowedLibrarySize)));
        
        self.quotaBarViewToolTip = [DTG_QuotaBarViewToolTip quotaBarViewToolTip];
        [self.quotaBarViewToolTip insertUsedSpaceInPercent:usedSpaceInPercent];
        if (isPad && [self isMyZoneFullPad])
        {
            if (self.isShowQuotaBar == NO && [self.quotaBarViewToolTip isNeedToShowToolTip])
            {
                self.isShowQuotaBar = YES;
                [self.viewToPageControl layoutSubviews];
                
                [self.quotaBarViewToolTip buildWIthQuotaDataWithOnLinkageView:self.quotaBarPlaceholder];
                [self.quotaBarViewToolTip addToolTipToView:self.view];
            }
        }
        else
        {
            [self.quotaBarViewToolTip buildWIthQuotaDataWithOnLinkageView:self.view];
        }
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (isPhone)
    {
        CGPoint p =  scrollView.contentOffset;
        if (p.y <-10 && self.isShowQuotaBar == NO)
        {
            self.isShowQuotaBar = YES;
            [self.iPhoneMainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            [self.quotaBarViewToolTip setBackgroundColor:[UIColor clearColor]];
            
            if ([self.quotaBarViewToolTip isNeedToShowToolTip])
            {
                [self.quotaBarViewToolTip addToolTipToView:self.view];
                [self.view bringSubviewToFront:self.quotaBarViewToolTip];
                [self.collectionViewMyZone reloadData];
            }
        }
    }
}




-(void) setupPageTitle
{
    [super setupPageTitle];

    if ([APP_SET isOffline])
    {
        self.zoneViewController.labelTitle.text = LS(@"dtgs_dp_main_titel_downloads");
        if (isPad)
        {
            self.zoneViewController.imageTitle.image = [UIImage imageNamed:@"header_offline_icon"];
        }
    }
}

@end
