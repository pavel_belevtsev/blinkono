//
//  myZoneRecordingsViewController.h
//  KalturaUnified
//
//  Created by Israel Berezin on 12/30/14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneInnerViewController.h"
#import "CustomViewFactory.h"

#import "ScheduledToolTipView.h"

typedef enum{
    RecordPageState=0,
    SchedulePageState
}RecorderPageState;

@interface myZoneRecordingsViewController : MyZoneInnerViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *statusSegmented;
@property (nonatomic) TVOrderBy orderBy;
@property (nonatomic, weak) IBOutlet UIButton *buttonSortOpen;
@property (nonatomic, strong) NSMutableArray *filterMenuItemList;

@property (nonatomic, weak) IBOutlet UIView *viewSortOpen;
@property (nonatomic, weak) IBOutlet UILabel *labelSortOpen;
@property (weak, nonatomic) IBOutlet UIView *quterPlaseholderMiniMyZone;

@property (strong, nonatomic) IBOutlet UIView *topIphoneView;
@property (nonatomic) RecorderPageState recorderPageState;
@property BOOL isEditMode;

@property (strong,nonatomic) NSString * useSeriesId;

@property (weak, nonatomic) IBOutlet UIScrollView *iPhoneMainScrollView;

@property (weak, nonatomic) IBOutlet UIView *iPhoneMainView;

@property (weak, nonatomic) IBOutlet UIView *iPhoneQuterPlaceholder;

@property (nonatomic) BOOL isShowQuterBar;
@property (weak, nonatomic) IBOutlet UIView *btnEditVIew;

- (IBAction)toggelEditMode:(id)sender;
- (IBAction)clearAllReccords:(id)sender;

- (void)changeEditMode:(BOOL)mode;;
-(void)exitFromSeriesMode;
-(void)enterToSeriesMode;
-(void)setupSegmentUI:(UISegmentedControl*)segmented;
-(IBAction)chnageRecordingsLoad:(id)sender;
@end
