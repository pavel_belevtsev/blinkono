//
//  MyZoneFavoritesViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneInnerViewController.h"

@interface MyZoneFavoritesViewController : MyZoneInnerViewController

@end
