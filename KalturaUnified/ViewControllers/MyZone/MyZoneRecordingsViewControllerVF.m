//
//  MyZoneRecordingsViewControllerVF.m
//  KalturaUnified
//
//  Created by Aviv Alluf on 1/21/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneRecordingsViewControllerVF.h"
#import "UIImage+Tint.h"

@interface MyZoneRecordingsViewControllerVF ()

@end

@implementation MyZoneRecordingsViewControllerVF

- (void)viewDidLoad {
    [super viewDidLoad];
       // Do any additional setup after loading the view.
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.labelSortOpen.backgroundColor = [VFTypography instance].grayScale2Color;
    self.labelSortOpen.textColor = [VFTypography instance].grayScale6Color;
    
    if(isPad)// && [[self.storyboard valueForKey:@"name"] isEqualToString:@"MyZoneFullPad"])
    {
        self.mainView.backgroundColor = [VFTypography instance].darkGrayColor;
        self.viewToPageControl.backgroundColor = [VFTypography instance].darkGrayColor;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)setupSegmentUI:(UISegmentedControl*)segmented
{
    [super setupSegmentUI:segmented];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                Font_Reg(16), NSFontAttributeName,
                                [VFTypography instance].grayScale4Color, NSForegroundColorAttributeName, nil];
    
    [segmented setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    NSDictionary *attributes1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 Font_Reg(16), NSFontAttributeName,
                                 [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    [segmented setTitleTextAttributes:attributes1 forState:UIControlStateSelected];
    
    segmented.tintColor =[VFTypography instance].grayScale2Color;
}

-(void)updateEditButtonsUI
{
    [super updateEditButtonsUI];
    [Utils setButtonFont:self.editBtn bold:YES];
    [Utils setButtonFont:self.clearAllBtn bold:YES];
    [self.editBtn setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:[VFTypography instance].grayScale3Color] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    [self.clearAllBtn setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:[VFTypography instance].brandColor]resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    
    [self.editBtn setTitleColor:[VFTypography instance].grayScale8Color forState:UIControlStateNormal];
    [self.clearAllBtn setTitleColor:[VFTypography instance].grayScale8Color forState:UIControlStateNormal];
}
@end
