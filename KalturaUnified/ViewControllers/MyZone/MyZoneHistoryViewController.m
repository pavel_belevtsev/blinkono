//
//  MyZoneHistoryViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneHistoryViewController.h"

@interface MyZoneHistoryViewController ()

@end

@implementation MyZoneHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImage * image = [UIImage imageNamed:@"myzone_clear"]; // SYN-2480
    //UIImage * image = [UIImage imageNamed:@"history_icon_empty"];
    
    [super initEmptyScreen:NO title:LS(@"my_zone_empty_history_title") text:LS(@"my_zone_empty_history_subtitle") andImage:image];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
}

#pragma mark -

- (void)reloadData {
    
    [super reloadData];
    
    [self.zoneViewController initEditButtons:YES editMode:editMode clearAll:YES];
    [self.zoneViewController changeEditButtonEnabled:NO];

    NSMutableArray *array = [[NSMutableArray alloc] init];
    [self loadData:0 array:array];

    
}

- (MediaItemBelongsToListType) mediaListType {
    return kMediaItemBelongsToListTypeHistory;
}

- (void)loadData:(int)pageIndex array:(NSMutableArray *)array {
    
    __weak MyZoneInnerViewController *weakSelf = self;
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetLastWatchedMediasByMediaID:0 mediaType:[[[TVConfigurationManager sharedTVConfigurationManager] mediaTypes] objectForKey:TVMediaTypeMovie] pictureSize:[TVPictureSize iPadItemCellPictureSize] pageSize:maxPageSize pageIndex:pageIndex delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
        
        for (id item in result) {
            [array addObject:item];
        }
        
        if ([result count] == maxPageSize) {
            [weakSelf loadData:(pageIndex + 1) array:array];
        } else {
            [weakSelf updateData:array];
        }
    }];
    
    [request setFailedBlock:^{
        [self hideHUD];
    }];
                                     
    [self sendRequest:request];
}

- (void)deleteMediaItem:(TVMediaItem *)mediaItem {
    
    if ([mediaItem isKindOfClass:[NSArray class]])
    {
        [self deleteSeries:mediaItem];
        return;
    }
    [self showHUD];
    
    //[self showHUDBlockingUI:YES];
    
    __weak MyZoneInnerViewController *weakSelf = self;
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForCleanUserHistoryWithMediaIDs:[NSArray arrayWithObject:mediaItem.mediaID] delegate:nil];
    
    [request setCompletionBlock:^{
        
        [weakSelf deleteMediaItemComplete:mediaItem];
        
    }];
    
    
    [request setFailedBlock:^{
        
        [self hideHUD];
        
    }];
    
    [self sendRequest:request];
    
}

- (void)deleteSeries:(TVMediaItem *)mediaItem
{
    NSArray * arr = (NSArray *) mediaItem;

    NSMutableArray * mediaItemsIds = [NSMutableArray array];
    for (TVMediaItem * media in arr)
    {
        [mediaItemsIds addObject:media.mediaID];
    }
    
    [self showHUD];
    
    //[self showHUDBlockingUI:YES];
    
    __weak MyZoneInnerViewController *weakSelf = self;
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForCleanUserHistoryWithMediaIDs:mediaItemsIds delegate:nil];
    
    [request setCompletionBlock:^{
        
        [weakSelf deleteMediaItemComplete:mediaItem];
        
    }];
    
    
    [request setFailedBlock:^{
        
        [self hideHUD];
        
    }];
    
    [self sendRequest:request];
    
}

- (void)clearAllItems {
    
    [self showHUD];
    
    //[self showHUDBlockingUI:YES];
    
    __weak MyZoneInnerViewController *weakSelf = self;
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (TVMediaItem *mediaItem in super.myZoneList) {
        
        if ([mediaItem isKindOfClass:[TVMediaItem class]]) {
            [array addObject:mediaItem.mediaID];
        } else if ([mediaItem isKindOfClass:[NSArray class]]) {
            
            NSArray *mediaArray = (NSArray *)mediaItem;
            
            for (TVMediaItem *mediaItemIn in mediaArray) {
                
                if ([mediaItemIn isKindOfClass:[TVMediaItem class]]) {
                    [array addObject:mediaItemIn.mediaID];
                }
                
            }
        }
        
       
    }
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForCleanUserHistoryWithMediaIDs:array delegate:nil];
    
    [request setCompletionBlock:^{
        
        [weakSelf clearAllItemsComplete];
        editMode = NO;
        [weakSelf.zoneViewController initEditButtons:YES editMode:editMode clearAll:YES];
        

    }];
    
    
    [request setFailedBlock:^{
        
        [self hideHUD];
        
    }];
    
    [self sendRequest:request];
    
}


#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
