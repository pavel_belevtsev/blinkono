//
//  myZoneRecordingsViewController.m
//  KalturaUnified
//
//  Created by Israel Berezin on 12/30/14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "myZoneRecordingsViewController.h"
#import "myZoneRecordingsCollectionViewCell.h"
#import "CategorySortViewController.h"
#import "QuotaBarView.h"
#import "UIImage+Tint.h"
#import <TvinciSDK/TVALUNPVRCompletedAndOngoingProvider.h>
#import <TvinciSDK/TVALUNPVRScheduledProvider.h>
#import <TvinciSDK/TVNPVRRecordItem.h>
#import <TvinciSDK/TVNPVRQuota.h>
#import "QuotaBarViewToolTip.h"
#import "RecordingHedearView.h"
#import <TvinciSDK/TVNPVRSeriesItem.h>


#define seriesSourceKey @"season"
#define seasonIdKey @"seasonId"

#define BtnDoneEditingExtraWight 35

#define RecoredsPageSize 16
#define CurrPageIndexLoadingStart -1

@interface myZoneRecordingsViewController ()<CategorySortViewControllerDelegate,myZoneRecordingsCollectionViewCellDelegate,UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong, nonatomic) QuotaBarView *quotaBarView;

@property (assign, nonatomic) TVRecordOrderBy orderByType;

@property (strong, nonatomic) QuotaBarViewToolTip *quotaBarViewToolTip;

@property (strong, nonatomic) TVALUNPVRCompletedAndOngoingProvider * completedAndOngoingProvider ;
@property (assign, nonatomic) NSInteger completedAndOngoingPageIndex;
@property (strong, nonatomic) NSMutableArray *completedAndOngoingData;
@property BOOL loadAllCompletedAndOngoing;

@property (strong, nonatomic) TVALUNPVRScheduledProvider * scheduledProvider ;
@property (assign, nonatomic) NSInteger scheduledPageIndex;
@property (strong, nonatomic) NSMutableArray *scheduledData;
@property BOOL loadAllscheduled;

@property BOOL isSeriesMode;
@property (strong, nonatomic) NSMutableArray *seriesData;

@property (strong, nonatomic) TVNPVRQuota *quota;

@property NSInteger currPageIndexLoading;
@end

@implementation myZoneRecordingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     [self.zoneViewController initEditButtons:NO editMode:editMode];
    
    
    // Do any additional setup after loading the view.
    self.recorderPageState = RecordPageState;
    UIImage * image = [UIImage imageNamed:@"recordings_icon_empty"];
    
    [super initEmptyScreen:NO title:LS(@"my_zone_empty_recordings_title") text:LS(@"my_zone_empty_recordings_List_sub_title") andImage:image];
    self.orderByType = TVRecordOrderByStartTime;
    self.completedAndOngoingProvider = [[TVALUNPVRCompletedAndOngoingProvider alloc] init];
    self.scheduledProvider = [[TVALUNPVRScheduledProvider alloc] init];
    self.completedAndOngoingData = [NSMutableArray array];
    self.scheduledPageIndex = 0;
    self.completedAndOngoingPageIndex = 0;
    self.currPageIndexLoading = CurrPageIndexLoadingStart;
    self.scheduledData = [NSMutableArray array];
    self.seriesData = [ NSMutableArray array];
    [self setupCollectionView];
 
    [self setupSegmentUI:self.statusSegmented];
    
    self.isEditMode = NO;
    
    _viewSortOpen.hidden = YES;
    [self.editBtn superview].hidden = YES;

    
    [self.statusSegmented addTarget:self
                                action:@selector(chnageRecordingsLoad:)
                      forControlEvents:UIControlEventValueChanged];
   
    
    [self loadCompletedAndOngoing];
    
    [self GetNPVRQuotar];

    self.collectionViewMyZone.delegate = self;
    self.collectionViewMyZone.dataSource = self;
    
    if (isPhone)
    {
        self.statusSegmented.transform = CGAffineTransformMakeScale(.8f, .8f);
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self hideHUD];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self menuContainerViewController].panMode = MFSideMenuPanModeDefault;

    
    UIStoryboard * story = [UIStoryboard storyboardWithName:[Utils nibName:@"Main"] bundle:[NSBundle mainBundle]];
    
//    MyZoneViewController *controller = [[MyZoneViewController alloc] init:story];

    CategorySortViewController *sortController = [story instantiateViewControllerWithIdentifier:@"CategorySortViewController"];
    
    sortController.delegate = self;
    
    [sortController updateWithMenuItem:nil orderBy:_orderBy filters:_filterMenuItemList];

    
    [self menuContainerViewController].rightMenuViewController = sortController;
    [[self menuContainerViewController] setRightMenuWidth:270];

    NSArray * sortByArray = @[@{@"title":LS(@"category_page_newest"), @"sortBy":@(TVOrderByDateAdded)},
                              @{@"title":LS(@"A - Z"), @"sortBy":@(TVOrderByNumberOfViews)}];
    
    sortController.orderBy = TVOrderByDateAdded;
    [sortController updateSortArrayAndReloadData:sortByArray];
    
    if (!isInitialized)

    {
        isInitialized = YES;

        _orderBy = TVOrderByDateAdded;

        self.filterMenuItemList = [[NSMutableArray alloc] init];

        [self updateSortByButton];
        
        if(isPad) {
            if ([self isMyZoneFullPad])
                [self.zoneViewController updateEditButtonRecordingMyZone];
            else
                [self.zoneViewController updateEditButtonInMiniMyZone];
        }
    }
    
    [self.iPhoneMainScrollView  setContentOffset:CGPointMake(0, 40) animated:NO];
    self.isShowQuterBar = NO;
    //[self hideHUD];
}

-(void)setupSegmentUI:(UISegmentedControl*)segmented
{
    [segmented  setTitle:LS(@"recorded") forSegmentAtIndex:0];
    [segmented  setTitle:LS(@"sheduled") forSegmentAtIndex:1];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont systemFontOfSize:16], NSFontAttributeName,
                                CS(@"959595"), NSForegroundColorAttributeName, nil];
    
    [segmented setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    NSDictionary *attributes1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont systemFontOfSize:16], NSFontAttributeName,
                                 [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    [segmented setTitleTextAttributes:attributes1 forState:UIControlStateSelected];
}

-(void) setupCollectionView
{
    NSString * identifier = NSStringFromClass([myZoneRecordingsCollectionViewCell class]);
    if (isPhone)
    {
        identifier = @"myZoneRecordingsCollectionViewCellPhone";
    }
    NSString * nibName = [myZoneRecordingsCollectionViewCell nibName];
    UINib * nib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionViewMyZone registerNib:nib forCellWithReuseIdentifier:identifier];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeEditMode:(BOOL)mode
{
    self.isEditMode = mode;
    [self.collectionViewMyZone reloadData];
}


- (void)showEmptyScreen {
    
    self.zoneEmptyView.frame = self.view.bounds;
    [self.view addSubview:self.zoneEmptyView];
    self.zoneEmptyView.userInteractionEnabled = NO;

    [self.view bringSubviewToFront:self.statusSegmented];
    if (self.quotaBarViewToolTip.superview)
        [self.view bringSubviewToFront:self.quotaBarViewToolTip];
}



- (void)updateData:(NSArray *)array useLive:(BOOL)useLive pageIndex:(NSInteger)pageIndex
{
    [self hideHUD];
    if (pageIndex == 0)
    {
        [self.myZoneList removeAllObjects];
    
        [self.myZoneList setArray:array];
    }
    else
    {
        [self.myZoneList addObjectsFromArray:array];
    }
    
    [self.collectionViewMyZone reloadData];
    
    self.viewPageControl.hidden = NO;
    
    if (![self.myZoneList count])
    {
        [self showEmptyScreen];
        _viewSortOpen.hidden = YES;
        [self.editBtn superview].hidden = YES;
    }
    else
    {
        [self.zoneEmptyView removeFromSuperview];
        NSInteger pages = [self.myZoneList count] / 4;
        if (([self.myZoneList count] % 4) > 0) pages++;
        
        self.viewPageControl.numberOfPages = pages;
        self.viewPageControl.hidden = NO;
        
        _viewSortOpen.hidden = NO;
        [self.editBtn superview].hidden = NO;
        
    }
    
    if(isPad && [self isMyZoneFullPad])
    {
        [self.zoneViewController changeEditButtonEnabled:([self.myZoneList count] ? YES : NO) allowForMini:YES];
    }
    else
    {
        [self.zoneViewController changeEditButtonEnabled:([self.myZoneList count] ? YES : NO)];
    }
}

- (MediaItemBelongsToListType) mediaListType {
    return kMediaItemBelongsToListTypeRecordings;
}

- (void)reloadData
{
    if (self.recorderPageState == RecordPageState)
    {
        self.isSeriesMode = NO;
        [self updateData:self.completedAndOngoingData useLive:YES];
    }
    else
    {
        self.isSeriesMode = NO;
        [self updateData:self.scheduledData useLive:YES];
    }
    [self.zoneEmptyView removeFromSuperview];
    
    [super reloadData];

    //[self hideHUD];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(isPad) {
        if ([self isMyZoneFullPad]) {
            NSInteger count =[self.myZoneList count];
            NSInteger num =  (count / zoneViewPadItemsCount) + ((count % zoneViewPadItemsCount) ? 1 : 0);
            
            self.viewPageControl.numberOfPages =num;
            return num;
        }
        else {
            NSInteger count =[self.myZoneList count];
            NSInteger num =  (count / 4) + ((count % 4) ? 1 : 0);
            
            self.viewPageControl.numberOfPages =num;
            return num;
        }
    }
    else {
        return 1;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(isPad) {
        if ([self isMyZoneFullPad])
            return zoneViewPadItemsCount;
        else {
            return 4;
        }
    }
    else {
        NSInteger count = [self.myZoneList count];
        
        if (count  > 0)
        {
            [self.zoneViewController initEditButtons:YES editMode:self.isEditMode clearAll:NO];
        }
        return count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString * className = NSStringFromClass([myZoneRecordingsCollectionViewCell class]);
    if (isPhone)
    {
        className = @"myZoneRecordingsCollectionViewCellPhone";
    }
    myZoneRecordingsCollectionViewCell *cellView = [collectionView dequeueReusableCellWithReuseIdentifier:className forIndexPath:indexPath];
    cellView.delegate = self;
    
    NSInteger index = indexPath.row;
    if(isPad && [self isMyZoneFullPad])
    {
        //int index = indexPath.row;
        if (index % zoneViewPadRowsCount == 0) {
            index = index / zoneViewPadRowsCount;
        } else {
            index = index / zoneViewPadRowsCount;
            index += zoneViewPadColsCount;
        }
        //NSLog(@"index %d", index);
        
        index += (indexPath.section * zoneViewPadItemsCount);
        
    }
    else if (isPad)
    {
        index = indexPath.section * 4 + indexPath.row;
    }
    
    if (index < [self.myZoneList count])
    {
        [cellView updateData:[self.myZoneList objectAtIndex:index] isEditMode:self.isEditMode isSeiresMode:self.isSeriesMode];
    }
    else
    {
        [cellView clearMediaCell];
    }
    
    return cellView;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    if(isPad && [self isMyZoneFullPad])
    {
        if (index % zoneViewPadRowsCount == 0) {
            index = index / zoneViewPadRowsCount;
        } else {
            index = index / zoneViewPadRowsCount;
            index += zoneViewPadColsCount;
        }
        
        index += (indexPath.section * zoneViewPadItemsCount);
    }
    else if (isPad)
    {
        index = indexPath.section * 4 + indexPath.row;
    }
    if (self.recorderPageState == RecordPageState)
    {

        if (index < [self.myZoneList count])
        {
            TVNPVRRecordItem * recordItem = [self.myZoneList objectAtIndex:index];
            if ([recordItem.recordSource isEqualToString:seriesSourceKey] && self.isSeriesMode == NO)
            {
                NSString * seriesId = [[recordItem.epgTags objectForKey:seasonIdKey] lastObject];
                if ([seriesId length]>0)
                {
                    [self loadCompletedAndOngoingEpisodeToSeiresId:seriesId];
                }
            }
            else
            {
                
                [self playRecordedItem:recordItem];
//                //[super openMediaItem:mediaItem];
//                myZoneRecordingsCollectionViewCell *cellView = (myZoneRecordingsCollectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
//                ScheduledToolTipView * tool = [[ScheduledToolTipView alloc] init];
//                [tool setBackgroundColor:[UIColor clearColor]];
//                
//                if (isPad)
//                {
//                    [tool buildWith:recordItem OnLinkageView:cellView.imageViewThumb toolTipArrowType:ToolTipTypeArrowDynamic];
//                }
//                else
//                {
//                    [tool buildWith:recordItem OnLinkageView:self.view toolTipArrowType:ToolTipTypeArrowNone];
//                    [tool setBackgroundColor:[UIColor clearColor]];
//                    CGRect frame = tool.frame;
//                    frame.origin.y -= 50;
//                    tool.frame = frame;
//                }
//                
//                [self.view addSubview: tool];
//                [self.view bringSubviewToFront:tool];
            
            }
        }
    }
    else
    {
        myZoneRecordingsCollectionViewCell *cellView = (myZoneRecordingsCollectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
        if (index < [self.myZoneList count])
        {
            TVNPVRRecordItem * recordItem = [self.myZoneList objectAtIndex:index];
            if ([recordItem.recordSource isEqualToString:seriesSourceKey] && self.isSeriesMode == NO)
            {
                NSString * seriesId = [[recordItem.epgTags objectForKey:seasonIdKey] lastObject];
                if ([seriesId length]>0)
                {
                    [self loadCompletedAndOngoingEpisodeToSeiresId:seriesId];
                }
            }
            else
            {
                TVNPVRRecordItem * recordItem = [self.myZoneList objectAtIndex:index];
                ScheduledToolTipView * tool = [[CustomViewFactory defaultFactory] scheduledToolTipView];
                [tool setBackgroundColor:[UIColor clearColor]];
                if (isPad)
                {
                    [tool buildWith:recordItem OnLinkageView:cellView.imageViewThumb toolTipArrowType:ToolTipTypeArrowDynamic];
                }
                else
                {
                    [tool buildWith:recordItem OnLinkageView:self.view toolTipArrowType:ToolTipTypeArrowNone];
                    [tool setBackgroundColor:[UIColor clearColor]];
//                    CGRect frame = tool.frame;
//                    frame.origin.y -= 100;
//                    tool.frame = frame;
                }
                [self.view addSubview: tool];
                [self.view bringSubviewToFront:tool];
            }
        }
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return [myZoneRecordingsCollectionViewCell cellEdgeInsets];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (isPad)
    {
        return 20;
    }
    return 0;
    
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if (isPad)
    {
        return 0;
    }
    return 0;
}

#pragma mark - -

- (void)updateSortByButton {
    
    if (_labelSortOpen) {
        
        [Utils setLabelFont:_labelSortOpen bold:YES];
        _labelSortOpen.backgroundColor = APST.brandColor;
        _labelSortOpen.textColor = APST.brandTextColor;
        _labelSortOpen.text = LS(@"category_page_sort_by");
        _viewSortOpen.frame = CGRectMake(_viewSortOpen.frame.origin.x, _viewSortOpen.frame.origin.y, [_labelSortOpen.text sizeWithAttributes:@{NSFontAttributeName:_labelSortOpen.font}].width + _viewSortOpen.frame.size.height, _viewSortOpen.frame.size.height);
        _viewSortOpen.transform = CGAffineTransformMakeRotation(-M_PI / 2);
        _viewSortOpen.center = CGPointMake(self.view.frame.size.width - (_viewSortOpen.frame.size.width / 2.0), (self.view.frame.size.height / 2.0) - 10.0);
        
    } else if (_buttonSortOpen) {
        
        [_buttonSortOpen setTitle:LS(@"category_page_sort_by") forState:UIControlStateNormal];
        [self initSelectedBrandButtonUI:_buttonSortOpen];
        
    }
}

- (IBAction)tapSortByButton:(UITapGestureRecognizer *)recognizer {
    
    [[self menuContainerViewController] toggleRightSideMenuCompletion:^{}];
}


- (void)selectSortOption:(TVOrderBy)orderBy title:(NSString *)title {
    
    if (_orderBy != orderBy)
    {
        _orderBy = orderBy;
        
        switch (orderBy)
        {
            case TVOrderByDateAdded:
                self.orderByType =  TVRecordOrderByStartTime;
                break;
            case TVOrderByNumberOfViews:
                self.orderByType = TVRecordOrderByName;
                break;
            default:
                self.orderByType =  TVRecordOrderByStartTime;
                break;
        }
        [self.completedAndOngoingData removeAllObjects];
        [self.scheduledData removeAllObjects];
        self.completedAndOngoingPageIndex = 0;
        self.currPageIndexLoading = CurrPageIndexLoadingStart;
        self.scheduledPageIndex = 0;
        self.loadAllscheduled = NO;
        self.loadAllCompletedAndOngoing=NO;
        
        [self.seriesData removeAllObjects];
        [self.myZoneList removeAllObjects];
        
        [self.collectionViewMyZone reloadData];
       
        if (self.isSeriesMode)
        {
            [self loadCompletedAndOngoingEpisodeToSeiresId:self.useSeriesId];
        }
        else if (self.recorderPageState == RecordPageState)
        {
            [self loadCompletedAndOngoing];
        }
        else
        {
            [self loadScheduled];
        }

    }
}

- (void)selectFilterOption:(TVMenuItem *)filterItem {
    
    if (![_filterMenuItemList count] || ![[_filterMenuItemList lastObject] isEqual:filterItem]) {
        [_filterMenuItemList addObject:filterItem];
        
//        [self categoryListInitialization];
//        
//        if (isPad && !searchMode) _labelTitle.text = [NSString stringWithFormat:@"%@ (%@)", _categoryMenuItem.name, filterItem.name];
        
    }
}

- (void)selectFilterBack
{
    if ([_filterMenuItemList count])
    {
        [_filterMenuItemList removeLastObject];
        
       // [self categoryListInitialization];
        
       // if (!searchMode) _labelTitle.text = _categoryMenuItem.name;
        
    }
}

-(void)addQuotaBarView
{
    self.quotaBarView = [QuotaBarView quotaBarView];
    
    if (isPad)
    {
        if([self isMyZoneFullPad])
        {
            CGRect frame = self.quotaBarView.frame;
            frame.origin.y=0;
            frame.origin.x = self.viewPageControl.frame.size.width - self.quotaBarView.frame.size.width;
            self.quotaBarView.frame = frame;
            [self.viewPageControl addSubview:self.quotaBarView];
        }
        else
        {
            CGRect frame = self.quotaBarView.frame;
            frame.origin.y=0;
            frame.origin.x = 0;
            self.quotaBarView.frame = frame;
            [self.quterPlaseholderMiniMyZone addSubview:self.quotaBarView];
        }
    }
    else
    {
        CGRect frame = self.quotaBarView.frame;
        frame.origin.y=0;
        frame.origin.x = 0;
        frame.size.width = self.view.frame.size.width;
        self.quotaBarView.frame = frame;
        [self.iPhoneQuterPlaceholder addSubview:self.quotaBarView];
    }
}

#pragma mark - Action -

- (IBAction)toggelEditMode:(id)sender
{
    UIButton * btn = (UIButton*)sender;
    if (btn.selected)
    {
        self.isEditMode = NO;
        btn.selected = NO;
        [self.editBtn setTitle:LS(@"edit") forState:UIControlStateNormal];
        self.clearAllBtn.hidden = YES;
        [self updateEditBtnWight:self.editBtn.frame.size.width - BtnDoneEditingExtraWight];
        if ([self.myZoneList count] ==0)
        {
            [self showEmptyScreen];
        }
    }
    else
    {
        self.isEditMode = YES;
        btn.selected = YES;
        [self.editBtn setTitle:LS(@"done_editing") forState:UIControlStateNormal];
        self.clearAllBtn.hidden = NO;
        [self updateEditBtnWight:self.editBtn.frame.size.width + BtnDoneEditingExtraWight];
        
    }
    [self.collectionViewMyZone reloadData];
}

- (IBAction)clearAllReccords:(id)sender
{
}

-(void)updateEditBtnWight:(NSInteger)wight
{
    CGRect frame = self.editBtn.frame;
    frame.size.width = wight;
    self.editBtn.frame = frame;
}

-(IBAction)chnageRecordingsLoad:(id)sender
{
    UISegmentedControl  * seg = (UISegmentedControl *) sender;
    [self.zoneEmptyView removeFromSuperview];
    [self.myZoneList removeAllObjects];
    [self.collectionViewMyZone reloadData];
    self.currPageIndexLoading = CurrPageIndexLoadingStart;
    switch (seg.selectedSegmentIndex)
    {
        case 0:
        {
            [self showHUD];
            self.recorderPageState = RecordPageState;
            self.loadAllCompletedAndOngoing = NO;
            self.completedAndOngoingPageIndex = 0;
            [self loadCompletedAndOngoing];
        }
            break;
        case 1:
        {
            [self showHUD];
            self.recorderPageState = SchedulePageState;
            self.loadAllscheduled = NO;
            self.scheduledPageIndex = 0;
            [self loadScheduled];
        }
            break;
        default:
            break;
    }
    //reset edit button
    [self.zoneViewController changeEditButtonEnabled: NO];
}

-(void)enterToSeriesMode
{
    self.isSeriesMode = YES;
    self.statusSegmented.hidden = YES;
    [self.zoneViewController enterToSeriesMode];
}

-(void)exitFromSeriesMode
{
    self.isSeriesMode = NO;
    self.statusSegmented.hidden = NO;
    self.currPageIndexLoading = CurrPageIndexLoadingStart;
    if (self.recorderPageState == RecordPageState)
    {
       
       self.recorderPageState = RecordPageState;
       self.loadAllCompletedAndOngoing = NO;
       self.completedAndOngoingPageIndex = 0;
       [self loadCompletedAndOngoing];
    }
    else
    {
        self.recorderPageState = SchedulePageState;
        self.loadAllscheduled = NO;
        self.scheduledPageIndex = 0;
       [self loadScheduled];

    }
   
    [self.myZoneList removeAllObjects];
    [self.zoneEmptyView removeFromSuperview];

  //  [self.collectionViewMyZone reloadData];
}

#pragma mark - myZoneRecordingsCollectionViewCell delegate  -

-(void) myZoneRecordingsCollectionViewCellDeleteItem:(myZoneRecordingsCollectionViewCell *)cell
{
    TVNPVRRecordItem * recordItem = cell.recordItem;
    NSString * title = LS(@"Delete Recording");
    NSString * bodyStart = LS(@"Are you sure want to delete");
    NSString * bodyEnd = LS(@"From family recordings?\n");
    NSString * body = [NSString stringWithFormat:@"%@ \"%@\" %@",bodyStart,recordItem.name,bodyEnd];
    if ([recordItem.recordSource isEqualToString:seriesSourceKey] && self.isSeriesMode == NO)
    {
        title= LS(@"Delete Series");
        bodyStart = LS(@"All recorded episodes of");
        bodyEnd = LS(@"will be deleted and future recordings will be canceled.\nAre you sure you want to continue?\n");
        body = [NSString stringWithFormat:@"%@ \"%@\" %@",bodyStart,recordItem.name,bodyEnd];
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:body delegate:nil cancelButtonTitle:LS(@"Cancel") otherButtonTitles:LS(@"OK"), nil];
    
    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex)
    {
        if (buttonIndex ==  [alertView cancelButtonIndex])
        {
            
        }
        else
        {
            [self deleteRecoredItem:recordItem];
        }
    }];
}


-(void)deleteRecoredItem:(TVNPVRRecordItem *) recordItem
{
//    [self deleteRecoredItemComplate:recordItem];
//    return;
    
    if ([recordItem.recordSource isEqualToString:seriesSourceKey] && self.isSeriesMode == NO)
    {
        [self getSeriesRecordingsWithRecoredItem:recordItem];
    }
    else
    {
        switch (recordItem.recordingStatus)
        {
            case TVRecordingStatusCompleted:
                [self deleteAssetRecording:recordItem];
                break;
            case TVRecordingStatusOngoing:
                [self deleteAssetRecording:recordItem];
                break;
            case TVRecordingStatusScheduled:
                [self cancelAssetRecording:recordItem];
                break;
            default:
                break;
        }
    }
}

-(void)deleteSeriesItem:(TVNPVRSeriesItem *)seriesItem withRecoreItem:(TVNPVRRecordItem *) recordItem
{
    [self deleteSeriesRecordingWithSeriesItem:seriesItem andRecoredItem:recordItem];
//    [self cancelSeriesRecordingWithSeriesItem:seriesItem andRecoredItem:recordItem];
}

-(void)deleteRecoredItemComplate:(TVNPVRRecordItem *) recordItem
{
    NSInteger itemIndex = -1;
    for (TVNPVRRecordItem * item in self.myZoneList)
    {
        if ([item.recordingID isEqualToString:recordItem.recordingID])
        {
            itemIndex++;
            break;
        }
        itemIndex++;
    }
    
    if (itemIndex >- 1)
    {
        [self.myZoneList removeObjectAtIndex:itemIndex];
        //[self.collectionViewMyZone reloadData];
        if (self.isSeriesMode)
        {
            [self.seriesData removeObjectAtIndex:itemIndex];
        }
        else if (self.recorderPageState == RecordPageState)
        {
            [self.completedAndOngoingData removeObjectAtIndex:itemIndex];
        }
        else
        {
            [self.scheduledData removeObjectAtIndex:itemIndex];
        }
        NSArray * arr = [NSArray arrayWithArray:self.myZoneList];
        [self updateData:arr useLive:NO pageIndex:0];
    }
}

#pragma mark - API -

- (void)GetNPVRQuotar
{
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetNPVRQuotaWithdelegate:nil];
    
    [request setFailedBlock:^{
        
        NSLog(@"%@",request);
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@",request);
        self.quota = [[TVNPVRQuota alloc] initWithDictionary:[request JSONResponse]];
        [self addQuotaBarView];
        [self.quotaBarView updateUIWIthQuotaData:self.quota];
        
        [self performSelector:@selector(addQuotaBarViewToolTip) withObject:nil afterDelay:0.3];
    }];
    
    
    [self sendRequest:request];
}

-(void)addQuotaBarViewToolTip
{
    
    self.quotaBarViewToolTip = [[CustomViewFactory defaultFactory] quotaBarViewToolTipWithQuotaData:self.quota];
    [self.quotaBarViewToolTip setBackgroundColor:[UIColor clearColor]];

    if ( [self.quotaBarViewToolTip isNeedToShowToolTip])
    {
        if (isPad)
        {
            [self.quotaBarViewToolTip buildWIthQuotaDataWithOnLinkageView:self.quotaBarView];
        }
        else
        {
            [self.quotaBarViewToolTip buildWIthQuotaDataWithOnLinkageView:self.view];
        }
        if (isPad)
        {
            [self.quotaBarViewToolTip addToolTipToView:self.view];
           // [self.view addSubview: self.quotaBarViewToolTip];
        }
    }

}

-(void)loadCompletedAndOngoing
{
    self.isSeriesMode = NO;
    __weak myZoneRecordingsViewController *weakSelf = self;
    if (self.completedAndOngoingPageIndex == self.currPageIndexLoading)
    {
        return;
    }
    if (self.completedAndOngoingPageIndex ==0)
    {
        //[self showHUDBlockingUI:NO];
        [self showHUD];
    }
    self.currPageIndexLoading = self.completedAndOngoingPageIndex;

    [self.completedAndOngoingProvider recoredsForPageSize:RecoredsPageSize pageIndex:self.completedAndOngoingPageIndex orderBy:self.orderByType
                                   withStartBlock:^{
                                   }
                                      failedBlock:^{
                                          [self hideHUD];
                                      }
                                  completionBlock:^(NSArray *records) {
                                      
                                      NSLog(@"%@",records);
                                      [self hideHUD];
                                      [self.completedAndOngoingData addObjectsFromArray:records];
                                      [weakSelf updateData:records useLive:NO pageIndex:self.completedAndOngoingPageIndex];
                                      self.completedAndOngoingPageIndex++;
                                      if (records.count < RecoredsPageSize)
                                      {
                                          self.loadAllCompletedAndOngoing = YES;
                                      }
                                      
                                  }];
}

-(void)loadScheduled
{
    
    self.isSeriesMode = NO;
    __weak myZoneRecordingsViewController *weakSelf = self;
    if (self.scheduledPageIndex == self.currPageIndexLoading)
    {
        return;
    }
    if (self.scheduledPageIndex ==0)
    {
        //[self showHUDBlockingUI:NO];
        [self showHUD];
    }
    self.currPageIndexLoading = self.scheduledPageIndex;
    [self.scheduledProvider recoredsForPageSize:RecoredsPageSize pageIndex:self.scheduledPageIndex  orderBy:self.orderByType
                                           withStartBlock:^{
                                           }
                                            failedBlock:^{
                                                [self hideHUD];
                                            }
                                          completionBlock:^(NSArray *records) {
                                              
                                              NSLog(@"%@",records);
                                                [self hideHUD];
                                              [self.scheduledData addObjectsFromArray:records];
                                              [weakSelf updateData:records useLive:NO pageIndex:self.scheduledPageIndex];
                                              self.scheduledPageIndex++;
                                              if ([records count] < RecoredsPageSize)
                                              {
                                                  self.loadAllscheduled = YES;
                                              }
                                             
                                          }];
}

-(void)loadCompletedAndOngoingEpisodeToSeiresId:(NSString *)seriesId
{
    //[self showHUDBlockingUI:NO];
    [self showHUD];
    self.useSeriesId = seriesId;
    __weak myZoneRecordingsViewController *weakSelf = self;
    __weak TVPAPIRequest * request =  [TVPMediaAPI requestForGetRecordingsWithPageSize:100
                                           pageIndex:0
                                            searchBy:TVRecordingSearchByTypeOther
                                        epgChannelID:nil
                                     recordingStatus:TVRecordingStatusUnknown
                                        recordingIDs:nil
                                         programsIds:nil
                                           seriesIds:[NSArray arrayWithObject:seriesId]
                                           startDate:[NSDate date]
                                             orderBy:self.orderByType
                                      orderDirection:TVRecordOrderDirectionAscending
                                        withdelegate:nil];
    [request setFailedBlock:^{
        
        NSLog(@"%@",request);
        [self hideHUD];
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@",request);
        [self hideHUD];
        NSArray *response = [request JSONResponse];
        NSMutableArray * records = [NSMutableArray array];
        for (NSDictionary * dictionary in response)
        {
            TVNPVRRecordItem * item = [[TVNPVRRecordItem alloc] initWithDictionary:dictionary];
           
            if (self.recorderPageState ==  RecordPageState)
            {
                if (item.recordingStatus == TVRecordingStatusOngoing || item.recordingStatus == TVRecordingStatusCompleted)
                {
                    [records addObject:item];
                }
                else
                {
                    ASLogInfo(@"%@ -> %d,",item.name, item.recordingStatus);
                }
            }
            else
            {
                if (item.recordingStatus == TVRecordingStatusScheduled || item.recordingStatus == TVRecordingStatusOngoing)
                {
                    [records addObject:item];
                }
                else
                {
                    ASLogInfo(@"%@ -> %d,",item.name, item.recordingStatus);
                }
            }
                
        }
        weakSelf.seriesData = [NSMutableArray arrayWithArray:records];
        [weakSelf enterToSeriesMode];
        [weakSelf updateData:records useLive:NO pageIndex:0];
    }];
    
    [self sendRequest:request];
}


- (void)cancelAssetRecording:(TVNPVRRecordItem *) recordItem
{
    NSString * recordID = recordItem.recordingID;
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForCancelAssetRecordingWithRecordingId:recordID delegate:nil];
    
    [request setFailedBlock:^{
        
        NSLog(@"%@",request);
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@",request);
        [self deleteRecoredItemComplate:recordItem];

    }];
    
    [self sendRequest:request];
}

- (void)deleteAssetRecording:(TVNPVRRecordItem *) recordItem
{
    NSString * recordID = recordItem.recordingID;
    
   __weak TVPAPIRequest * request = [TVPMediaAPI requestForDeleteAssetRecordingWithRecordingId:recordID delegate:nil];
    
    [request setFailedBlock:^{
        
        NSLog(@"%@",request);
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@",request);
        [self deleteRecoredItemComplate:recordItem];

    }];
    
    [self sendRequest:request];
}

- (void)deleteSeriesRecordingWithSeriesItem:(TVNPVRSeriesItem *) seriesItem andRecoredItem:(TVNPVRRecordItem *) recordItem
{
    
    NSString * recordID = seriesItem.recordID;
    
    if ([recordID length] ==0)
    {
        return;
    }
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForDeleteSeriesRecordingWithSeriesRecordingId:recordID withdelegate:nil];
    
    [request setFailedBlock:^{
        
        NSLog(@"%@",request);
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@",request);
        [self deleteRecoredItemComplate:recordItem];

    }];
    
    
    [self sendRequest:request];
}

- (void)cancelSeriesRecordingWithSeriesItem:(TVNPVRSeriesItem *) seriesItem andRecoredItem:(TVNPVRRecordItem *) recordItem
{
    NSString * recordID = seriesItem.recordID;
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForCancelSeriesRecordingWithSeriesRecordingId:recordID withdelegate:nil];
    
    [request setFailedBlock:^{
        
        NSLog(@"%@",request);
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@",request);
        [self deleteRecoredItemComplate:recordItem];

    }];
    
    [self sendRequest:request];
}

- (void)getSeriesRecordingsWithRecoredItem:(TVNPVRRecordItem *) recordItem
{
    __weak TVNPVRRecordItem * weakRecordItem = recordItem;
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetSeriesRecordingsWithPageSize:100 pageIndex:0 orderBy:TVRecordOrderByStartTime orderDirection:TVRecordOrderDirectionAscending withdelegate:nil];
    
    [request setFailedBlock:^{
        
        NSLog(@"%@",request);
    }];
    
    [request setCompletionBlock:^{
        //NSLog(@"%@",request);
        NSArray *response = [request JSONResponse];
        NSMutableArray * seriesRecoredItems = [NSMutableArray array];
        for (NSDictionary * dic in response) {
            TVNPVRSeriesItem * item = [[TVNPVRSeriesItem alloc] initWithDictionary:dic];
            if (item)
            {
                [seriesRecoredItems addObject:item];
            }
        }
        TVNPVRSeriesItem * currSeriesItem = [self findRecoredSeriesitemFrom:seriesRecoredItems ToRecoredItem:weakRecordItem];
        if (currSeriesItem != nil)
        {
            [self deleteSeriesItem:currSeriesItem withRecoreItem:weakRecordItem];
        }
        else
        {
            switch (weakRecordItem.recordingStatus)
            {
                case TVRecordingStatusCompleted:
                    [self deleteAssetRecording:weakRecordItem];
                    break;
                case TVRecordingStatusOngoing:
                    [self deleteAssetRecording:weakRecordItem];
                    break;
                case TVRecordingStatusScheduled:
                    [self cancelAssetRecording:weakRecordItem];
                    break;
                default:
                    break;
            }

        }
    }];
    
    
    [self sendRequest:request];
}

-(TVNPVRSeriesItem *)findRecoredSeriesitemFrom:(NSArray*)seriesRecoredItems ToRecoredItem:(TVNPVRRecordItem *) recordItem
{
    TVNPVRSeriesItem * seriesItem = nil;
    NSString * recordSeriesID = nil;
    
    if ([recordItem.recordSource isEqualToString:seriesSourceKey])
    {
        recordSeriesID = [[recordItem.epgTags objectForKey:seasonIdKey] lastObject];
    }

    if ([recordSeriesID length]>0)
    {
        for (TVNPVRSeriesItem * item in seriesRecoredItems)
        {
            if ([item.seriesId isEqual:recordSeriesID])
            {
                seriesItem = item;
                break;
            }
        }
    }
    return seriesItem;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (isPad)
    {
        CGFloat pageWidth = CGRectGetWidth(scrollView.frame);
        NSInteger contentOffsetX = scrollView.contentOffset.x;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        
        float remainder =  (float)((int)contentOffsetX  % (int)pageWidth);

        if (remainder > 0 && remainder < pageWidth)
        {
            page++;
        }
        ASLogInfo(@"remainder = %f",remainder);
        if (self.viewPageControl.currentPage == 0 || self.viewPageControl.currentPage != page)
        {
            [self.viewPageControl setCurrentPage:page];
        }
        
    }
    if (isPhone)
    {
        CGPoint p =  scrollView.contentOffset;
        if (p.y <-120 && self.isShowQuterBar == NO)
        {
            self.isShowQuterBar = YES;
            [self.iPhoneMainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            if ([self.quotaBarViewToolTip isNeedToShowToolTip])
            {
//                [self.view addSubview: self.quotaBarViewToolTip];
                [self.quotaBarViewToolTip addToolTipToView:self.view];
                [self.view bringSubviewToFront:self.quotaBarViewToolTip];
                [self.collectionViewMyZone reloadData];
            }
        }
    }
    if (self.isSeriesMode == NO)
    {
        switch (self.recorderPageState) {
            case RecordPageState:
            {
                if (self.loadAllCompletedAndOngoing == NO)
                {
                    [self loadCompletedAndOngoing];
                }
            }
                break;
            case SchedulePageState:
            {
                if (self.loadAllscheduled == NO)
                {
                    [self loadScheduled];
                }
            }
                break;
            default:
                break;
        }
    }
        
}
@end
