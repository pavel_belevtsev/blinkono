//
//  MyZoneInnerViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "BaseViewController.h"
#import "MyZoneEmptyView.h"
#import "PageControlView.h"
#import "MyZoneViewController.h"
#import "UIAlertView+Blocks.h"

typedef NS_ENUM (NSUInteger, PurchasesManagerLoadItems)
{
    kPurchasesManagerLoadItemsObjects = 1200,
    kPurchasesManagerLoadItemsIDsOnly
    
};

typedef NS_ENUM (NSUInteger, MyZoneViewControllerSubTypes)
{
    kMyZoneViewControllerSubTypeShowAll = 300,
    kMyZoneViewControllerSubTypeWatchListOnly,
    kMyZoneViewControllerSubTypeShowAllExceptWatchList,
    kMyZoneViewControllerSubTypeShowAllPermittedItems
    
};

@interface MyZoneInnerViewController : BaseViewController {
    
    BOOL isInitialized;
    BOOL editMode;
}

- (void)initEmptyScreen:(BOOL)friends title:(NSString *)title text:(NSString *)text andImage:(UIImage *)image;
- (void)reloadData;
- (void)loadData:(int)pageIndex array:(NSMutableArray *)array;
- (void)showEmptyScreen;
- (void)updateData:(NSArray *)array;
- (void)updateData:(NSArray *)array useLive:(BOOL)useLive;
- (void)sortMediaArray:(NSMutableArray *)array;
- (void)changeEditMode:(BOOL)mode;
- (MediaItemBelongsToListType) mediaListType;
- (void)deleteMediaItem:(TVMediaItem *)mediaItem;
- (void)deleteMediaItemComplete:(TVMediaItem *)mediaItem;
- (void)clearAllItems;
- (void)clearAllItemsComplete;
- (void)openSeriesMediaArray:(NSMutableArray *)array;
- (void)backToMainList:(BOOL)animated;
- (BOOL)inEditMode;
- (void)exitFromSeriesMode;
- (void)enterToSeriesMode;
- (void)updateEditButtonsUI;
- (void)showHUD;
- (BOOL)isMyZoneFullPad;
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
- (NSString*) reusableCellIdentifier;
- (void) defineControllerSettings;
- (void) setupPageTitle;

@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionViewMyZone;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionViewMyZoneInner;

@property (nonatomic, weak) IBOutlet UIView *viewNavigation;

@property (nonatomic, strong) MyZoneEmptyView *zoneEmptyView;
@property (nonatomic, strong) NSMutableArray *myZoneList;

@property (nonatomic, strong) PageControlView *viewPageControl;
@property (nonatomic, strong) PageControlView *viewPageControlInner;

@property (nonatomic, weak) MyZoneViewController *zoneViewController;
@property (nonatomic, weak) IBOutlet UIView *viewToPageControl;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *clearAllBtn;

@property (nonatomic, weak) NSMutableArray *seriesArray;
@property (nonatomic, readwrite) MyZoneViewControllerSubTypes subType;
@property (nonatomic, readwrite) PurchasesManagerLoadItems purchasesLoadType;

@property BOOL innerMode;


@end

extern const NSInteger zoneViewPadItemsCount;
extern const NSInteger zoneViewPadRowsCount;
extern const NSInteger zoneViewPadColsCount;
