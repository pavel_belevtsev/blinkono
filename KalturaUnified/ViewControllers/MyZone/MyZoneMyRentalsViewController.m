//
//  MyZoneMyRentalsViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneMyRentalsViewController.h"
#import <TvinciSDK/TVRental.h>

@interface MyZoneMyRentalsViewController ()

@end

@implementation MyZoneMyRentalsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage * image = [UIImage imageNamed:@"myzone_clear"];

    [super initEmptyScreen:NO title:LS(@"my_zone_empty_rentals_title") text:LS(@"my_zone_empty_rentals_sub_title") andImage:image];

    
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
}

#pragma mark -

- (void)reloadData {

    if ([LoginM isSignedIn]){

    
        [self.zoneViewController initEditButtons:NO editMode:editMode];
        
        [super reloadData];
        
        __weak MyZoneMyRentalsViewController *weakSelf = self;
        
        __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetUserPermittedItems];
        
        [request setCompletionBlock:^{
            
            NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
            
            [weakSelf updateRentalData:result];
            
        }];
        
        
        [request setFailedBlock:^{
            
            [self hideHUD];
            
        }];
        
        [self sendRequest:request];
    }
}

- (MediaItemBelongsToListType) mediaListType {
    return kMediaItemBelongsToListTypeRentals;
}

- (void)updateRentalData:(NSArray *)result {

    if ([LoginM isSignedIn]){
        if ([result count]) {
            
            NSMutableArray *mediaIDs = [[NSMutableArray alloc] init];
            
            for (NSDictionary * dict in result ) {
                
                TVRental * rentalItem = [[TVRental alloc] initWithDictionary:dict];
                [mediaIDs addObject:@(rentalItem.mediaID)];
            }
            
            __weak MyZoneInnerViewController *weakSelf = self;
            __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetMediasInfoWithMediaIDs:mediaIDs pictureSize:[TVPictureSize iPadItemCellPictureSize] includeDynamicInfo:YES delegate:nil];
            
            [request setCompletionBlock:^{
                
                NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
                [weakSelf updateData:result];
            }];
            
            
            [request setFailedBlock:^{
                
                [self hideHUD];
            }];
            
            [self sendRequest:request];
            
        } else {
            
            [super updateData:result];
            
        }
    
    }
}


#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
