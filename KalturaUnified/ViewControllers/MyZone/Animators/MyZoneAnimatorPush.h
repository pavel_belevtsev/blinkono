//
//  MyZoneAnimatorPush.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyZoneAnimatorPush : NSObject <UIViewControllerAnimatedTransitioning>

@end
