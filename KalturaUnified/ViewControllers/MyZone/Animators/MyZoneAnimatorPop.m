//
//  MyZoneAnimatorPop.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneAnimatorPop.h"

@implementation MyZoneAnimatorPop

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return kViewNavigationDuration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    [[transitionContext containerView] addSubview:toViewController.view];
    toViewController.view.frame = CGRectMake(toViewController.view.frame.origin.x, toViewController.view.frame.size.height, toViewController.view.frame.size.width, toViewController.view.frame.size.height);
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        fromViewController.view.frame = CGRectMake(fromViewController.view.frame.origin.x, fromViewController.view.frame.size.height, fromViewController.view.frame.size.width, fromViewController.view.frame.size.height);
        toViewController.view.frame = CGRectMake(toViewController.view.frame.origin.x, 0.0, toViewController.view.frame.size.width, toViewController.view.frame.size.height);
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
}


@end
