//
//  MyZonePurchasesListViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZonePurchasesListViewController.h"
#import "MyZoneItemCollectionViewCell.h"

@interface MyZonePurchasesListViewController () {
    BOOL isPersonal;
}
@end

@implementation MyZonePurchasesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view./

    [Utils setLabelFont:_labelWatchlistPersonal bold:YES];
    [Utils setLabelFont:_labelWatchlistHousehold bold:YES];
    
    _labelWatchlistPersonal.text = LS(@"my_zone_personal");
    _labelWatchlistHousehold.text = LS(@"my_zone_household");
    
    isPersonal = YES;
    
    _householdSwitch.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    _householdSwitch.layer.cornerRadius = 16.0;
    _householdSwitch.tintColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    _householdSwitch.onTintColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    
    if (self.householdSegment)
        [self setupSegmentUI];

    [self updateEmptyStateTexts];
    
    if (isPhone)
    {
        self.householdSegment.transform = CGAffineTransformMakeScale(.8f, .8f);
    }
}

- (void) updateEmptyStateTexts
{
    UIImage * image = [UIImage imageNamed:@"myzone_clear"];
    if (isPersonal) {
        // text to appear on "personal state":
        [super initEmptyScreen:NO title:LS(@"my_zone_empty_purchase_List_title") text:LS(@"my_zone_empty_purchase_List_sub_title") andImage:image];
    } else{
        // text to appear on "household state":
        [super initEmptyScreen:NO title:LS(@"my_zone_empty_purchase_List_title") text:LS(@"my_zone_empty_purchase_List_sub_title") andImage:image];
    }
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    editMode = NO;
    [self.zoneViewController initEditButtons:NO editMode:editMode];
}

#pragma mark -

- (void)showEmptyScreen {
    
    self.zoneEmptyView.frame = self.view.bounds;
    [self.view addSubview:self.zoneEmptyView];
    self.viewPageControl.hidden = YES;
    [self.view bringSubviewToFront:self.householdSegment];
    [self.view bringSubviewToFront:self.householdSwitchView];
}

- (IBAction)householdSwitchPressed:(UISwitch *)sender {
    
    isPersonal = !sender.on;
    editMode = NO;
    
    [self reloadData];
}

- (void)reloadData {

    [self updateEmptyStateTexts];

    [self.zoneViewController initEditButtons:isPersonal editMode:editMode];
    
    [super reloadData];
    
    __weak MyZonePurchasesListViewController *weakSelf = self;
    
    [PurchasesM getPurchasesForPersonal:isPersonal delegate:self completion:^(NSArray *arrRentals, NSError *error) {
        [weakSelf hideHUD];
        [super updateData:arrRentals];
    }];
}

- (void)deleteMediaItem:(TVMediaItem *)mediaItem {
    
    [self showHUD];
    __weak MyZoneInnerViewController *weakSelf = self;
    [PurchasesM removeMediaFromWatchList:[mediaItem.mediaID integerValue] delegate:self completion:^(BOOL success, NSError *error) {
        if (success) {
            [weakSelf deleteMediaItemComplete:mediaItem];
        }
        [self hideHUD];
    }];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setupSegmentUI
{
    [self.householdSegment addTarget:self
                              action:@selector(myAction:)
                    forControlEvents:UIControlEventValueChanged];
    
//This Text is not exist in the localizeable text , and have spesific client key if this is importent please change it accordingly:
    
    [self.householdSegment  setTitle:LS(@"my_zone_personal") forSegmentAtIndex:0];
    [self.householdSegment  setTitle:LS(@"my_zone_household") forSegmentAtIndex:1];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont systemFontOfSize:16], NSFontAttributeName,
                                CS(@"959595"), NSForegroundColorAttributeName, nil];
    
    [self.householdSegment setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    
    NSDictionary *attributes1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont systemFontOfSize:16], NSFontAttributeName,
                                 [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    [self.householdSegment setTitleTextAttributes:attributes1 forState:UIControlStateSelected];
}


-(IBAction)myAction:(id)sender
{
    UISegmentedControl  * seg = (UISegmentedControl *) sender;
    switch (seg.selectedSegmentIndex) {
        case 0:
        {
            isPersonal = YES;
        }
            break;
        case 1:
        {
            isPersonal = NO;
        }
            break;
        default:
            break;
    }
    
    [self reloadData];
}

//-(void)addBorderToCell:(MyZoneItemCollectionViewCell *)cellView
//{
//    ASLogInfo(@"");
//}
@end
