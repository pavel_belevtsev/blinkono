//
//  myZoneRecordingsCollectionViewCellVF.m
//  Vodafone
//
//  Created by Israel Berezin on 3/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "myZoneRecordingsCollectionViewCellVF.h"

@implementation myZoneRecordingsCollectionViewCellVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)updateFontAndColorToLableDate:(UILabel *)lblDate andLableTime:(UILabel*)lblTime
{
    if (isPad)
    {
        [Utils setLabelFont:lblDate size:14 bold:NO];
        [Utils setLabelFont:lblTime size:14 bold:NO];
    }
    else
    {
        [Utils setLabelFont:lblDate size:13 bold:NO];
        [Utils setLabelFont:lblTime size:13 bold:NO];
    }
    [lblDate setTextColor: [VFTypography instance].grayScale5Color];
    [lblTime setTextColor: [VFTypography instance].grayScale5Color];
}

@end
