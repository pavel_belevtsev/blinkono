//
//  MyZoneTabCollectionViewCell.m
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneTabCollectionViewCell.h"
#import "UIImage+Tint.h"
#import "UIButton+Color.h"

@implementation MyZoneTabCollectionViewCell

- (void)awakeFromNib
{

}

-(void)setMenuItem:(NSDictionary *)menuItem
{
    if (self.menuItem != menuItem)
    {
        _menuItem = menuItem;
        [self updateUI];
    }
}

-(void) updateUI
{
    self.button.enabled = YES;
    NSDictionary *item = self.menuItem;
    NSString * localizationKey = item[@"localizationKey"];
    NSString * tabTitle = LS(localizationKey);
    
    [self.button setTitle:tabTitle forState:UIControlStateNormal];
    self.button.exclusiveTouch = YES;
    
    if (isPad)
    {
        UIImage *btnImage = [UIImage imageNamed:@"myzone_toolbar_btn"];
        UIImage *img = [btnImage stretchableImageWithLeftCapWidth:btnImage.size.width / 2.0 topCapHeight:btnImage.size.height / 2.0];

        [self.button setBackgroundColor:[UIColor clearColor] forState:UIControlStateNormal];
        self.button.titleLabel.font = [UIFont fontWithName:self.button.titleLabel.font.fontName size:14];
        [self.button setBackgroundImage:[img imageTintedWithColor:APST.brandColor] forState:UIControlStateHighlighted];
        [self.button setBackgroundImage:[img imageTintedWithColor:APST.brandColor] forState:UIControlStateSelected];
        
        self.button.titleLabel.numberOfLines = 2;
        self.button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
   }
    [Utils setButtonFont:self.button bold:YES];

    // set background image - line for selected state
    [self.button setBackgroundImage:[[UIImage imageNamed:@"tab_highlight_background"] imageTintedWithColor:APST.brandColor]  forState:UIControlStateSelected];
    // set the title
    self.button.titleLabel.font = Font_Bold(14.0);
    self.button.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self.button setTitleColor:CS(@"a8a8a8") forState:UIControlStateNormal];
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self.button setHighlighted:highlighted];
    
}

-(void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self.button setSelected:selected];
}



@end
