//
//  MyZoneTabCollectionViewCellVF.h
//  Vodafone
//
//  Created by Israel Berezin on 3/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneTabCollectionViewCell.h"

@interface MyZoneTabCollectionViewCellVF : MyZoneTabCollectionViewCell

@property (strong, nonatomic)  UIView * selectedView;

@property BOOL isSelected;
@end
