//
//  MyZoneFriendsActivityCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyZoneViewController.h"

@protocol MyZoneFriendsActivityCellDelegate <NSObject>

-(void) myZoneFriendsActivityCell:(id) sender watchActivityOfIndex:(NSInteger) index;
@end

@interface MyZoneFriendsActivityCell : UITableViewCell

- (void)highlightUserName:(NSString *)userName andMovieTitle:(NSString*)movieTitle forMessageString:(NSString*)message;

@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UILabel *frienStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *activityActionButton;
@property (assign, nonatomic) NSInteger index;
@property (assign, nonatomic) id<MyZoneFriendsActivityCellDelegate> delegate;


@property (nonatomic, weak) BaseViewController *zoneViewController;


@end
