//
//  MyZoneDownloadCollectionViewCell.h
//  KalturaUnified
//
//  Created by Alex Zchut on 5/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneItemCollectionViewCell.h"

@protocol MyZoneDownloadCollectionViewCellDelegate <NSObject>

- (void) deleteCellMediaItem:(TVMediaItem*)item;
- (UIView*) getContainerView;

@end

@interface MyZoneDownloadCollectionViewCell : UICollectionViewCell <KADownloadItemDelegate>

@property (nonatomic, weak) KADownloadItem *downloadItem;
@property (nonatomic, weak) TVMediaItem *mediaItem;
@property (nonatomic, weak) id<MyZoneDownloadCollectionViewCellDelegate> delegate;

-(void) setItemDetails:(TVMediaItem*)mediaItem editMode:(BOOL)editMode;
@end
