//
//  myZoneRecordingsCollectionViewCell.m
//  KalturaUnified
//
//  Created by Israel Berezin on 12/30/14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "myZoneRecordingsCollectionViewCell.h"
#import "UIImageView+WebCache.h"

#define seriesSourceKey @"season"
#define seasonIdKey @"seasonId"

@implementation myZoneRecordingsCollectionViewCell


-(void)awakeFromNib
{
    [super awakeFromNib];
    
    // border
    [self.imageViewThumb.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.imageViewThumb.layer setBorderWidth:1.0f];
    self.seriesImage.hidden = NO;
    self.seriesImage.hidden = NO;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)updateData:(TVNPVRRecordItem *)item isEditMode:(BOOL)isEditMode isSeiresMode:(BOOL)isSeiresMode
{
    self.imageViewThumb.hidden = NO;

    self.recordItem = item;
    
    if (isEditMode)
    {
        self.x_btn.hidden = NO;
        [self bringSubviewToFront:self.x_btn];
        self.x_btn.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }
    else
    {
        self.x_btn.hidden = YES;
         self.x_btn.transform = CGAffineTransformMakeScale(0.1, 0.1);
    }

    [self.imageViewThumb sd_setImageWithURL:_recordItem.picURL placeholderImage:[UIImage imageNamed:@"placeholder_16_9.png"]];
    
    self.labelTitle.text = [_recordItem.name uppercaseString];
    if (isSeiresMode == YES)
    {
        NSString * text = @"";
        NSString * seasonText = nil;
        if ([_recordItem.epgTags objectForKey:@"season"])
        {
            seasonText = [NSString stringWithFormat:[LS(@"season") uppercaseString], [[_recordItem.epgTags objectForKey:@"season"] lastObject]];
            text = seasonText;
        }
        NSString * episodeText = nil;

        if ([_recordItem.epgTags objectForKey:@"episode"])
        {
            episodeText = [NSString stringWithFormat:[LS(@"episode") uppercaseString], [[_recordItem.epgTags objectForKey:@"episode"] lastObject]];
            if (text != nil)
            {
                text = [NSString stringWithFormat:@"%@ %@",text,episodeText];
            }
            else
            {
                text = episodeText;
            }
        }
        if (_recordItem.name)
        {
            if (text != nil)
            {
                text = [NSString stringWithFormat:@"%@\n%@",text, [_recordItem.name uppercaseString]];
            }
            else
            {
                text = [_recordItem.name uppercaseString];
            }
        }
        self.labelTitle.text = text;
    }
    [self updateLableTitleFontAndColor];
    
//    UIImage * firstImage= [UIImage imageNamed:@"Eye_icon"];
    UIImage * pointImage = [UIImage imageNamed:@"grey_bullet"];
    
    UIImageView * first = [UIImageView new];
    first.translatesAutoresizingMaskIntoConstraints = NO;
    [self.infoView addSubview:first];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[first]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(first)]];

    UIImageView * sec = [UIImageView new];
    sec.translatesAutoresizingMaskIntoConstraints = NO;
    [self.infoView addSubview:sec];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[sec]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(sec)]];
    
    UILabel *lblDate = [UILabel new];
    lblDate.translatesAutoresizingMaskIntoConstraints = NO;
    [self.infoView addSubview:lblDate];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[lblDate]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblDate)]];
    [lblDate setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [lblDate setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];


    UILabel *lblTime = [UILabel new];
    lblTime.translatesAutoresizingMaskIntoConstraints = NO;
    [self.infoView addSubview:lblTime];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[lblTime]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTime)]];
    [lblTime setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [lblTime setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];

    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yy"];
    NSString * text =  [formatter stringFromDate:_recordItem.startDate];
    lblDate.text = text;
    lblDate.textAlignment = NSTextAlignmentCenter;
    
    [self updateFontAndColorToLableDate:lblDate andLableTime:lblTime];
    
    int timeFull = ([_recordItem.endDate timeIntervalSince1970] - [_recordItem.startDate timeIntervalSince1970]);
 
    if ([_recordItem.recordSource isEqualToString:seriesSourceKey] && isSeiresMode == NO)
    {
        self.seriesImage.hidden = NO;
        first.hidden = YES;
        sec.hidden = YES;
        lblTime.hidden = YES;
        NSString * text = [[[_recordItem epgTags] objectForKey:@"seasonName"]firstObject];
        self.labelTitle.text = text;
    }
    
    else
    {
       
        int timeInMin = timeFull/60;
        NSString * minText = LS(@"min");
        lblTime.text = [NSString stringWithFormat:@"%d %@",timeInMin,minText];
        
        sec.image = pointImage;
        sec.contentMode = UIViewContentModeCenter;
        self.seriesImage.hidden = YES;
        if (_recordItem.recordingStatus == TVRecordingStatusOngoing)
        {
            UIImage * firstImage= [UIImage imageNamed:@"record_singleItem_icon"];
            first.image = firstImage;
            first.contentMode = UIViewContentModeScaleAspectFit;
            lblDate.text = (isPad) ? LS(@"my_zone_items_ongoing_recording_wo_param_ipad") : LS(@"my_zone_items_ongoing_recording_wo_param");
        }
        else
        {
            first.hidden = YES;
        }
    }
    
    //left space
    UIView *spacer1 = [UIView new];
    spacer1.translatesAutoresizingMaskIntoConstraints = NO;
    [self.infoView addSubview:spacer1];
    
    //right space
    UIView *spacer2 = [UIView new];
    spacer2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.infoView addSubview:spacer2];

    NSDictionary *metrics = @{@"s1": (!first.hidden)?@3:@0,
                              @"s2": (!lblDate.hidden)?@3:@0,
                              @"s3": (!sec.hidden)?@3:@0};
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[spacer1(>=0)]-[first]-(s1)-[lblDate]-(s2)-[sec]-(s3)-[lblTime]-[spacer2(==spacer1)]|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(first, lblDate, sec, lblTime, spacer1, spacer2)]];
    
}

-(void)updateLableTitleFontAndColor
{
    if (isPad)
    {
        [Utils setLabelFont:self.labelTitle size:16 bold:NO];
    }
    else
    {
        [Utils setLabelFont:self.labelTitle size:14 bold:NO];
    }
    [self.labelTitle setTextColor:[UIColor whiteColor]];
}

-(void)updateFontAndColorToLableDate:(UILabel *)lblDate andLableTime:(UILabel*)lblTime
{
    if (isPad)
    {
        [Utils setLabelFont:lblDate size:14 bold:NO];
        [Utils setLabelFont:lblTime size:14 bold:NO];
    }
    else
    {
        [Utils setLabelFont:lblDate size:13 bold:NO];
        [Utils setLabelFont:lblTime size:13 bold:NO];
    }
    [lblDate setTextColor: CS(@"959595")];
    [lblTime setTextColor: CS(@"959595")];
}

- (void)prepareForReuse
{
    _recordItem = nil;
    _labelTitle.text = nil;
    self.imageViewThumb.image = nil;
    self.imageViewThumb.hidden = NO;
    self.labelTitle.hidden = NO;
    self.x_btn.hidden = NO;
    self.infoView.hidden = NO;
    
    [self.infoView removeAllSubviews];
}

+(NSString *) nibName
{
    if (isPad)
    {
        return  @"myZoneRecordingsCollectionViewCell";
    }
    else
    {
        return  @"myZoneRecordingsCollectionViewCellPhone";
    }
}

+(UIEdgeInsets) cellEdgeInsets
{
    if (isPad)
    {
        return UIEdgeInsetsMake(0, 40, 0, 40);
    }
    else
    {
        return UIEdgeInsetsMake(0, 5, 5, 8);
    }
}

- (IBAction)delateItem:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(myZoneRecordingsCollectionViewCellDeleteItem:)])
    {
        [self.delegate myZoneRecordingsCollectionViewCellDeleteItem:self];
    }
}

- (void)clearMediaCell {
    
    self.imageViewThumb.hidden = YES;
    self.labelTitle.hidden = YES;
    self.x_btn.hidden = YES;
    self.infoView.hidden = YES;
    self.seriesImage.hidden = YES;
}


@end
