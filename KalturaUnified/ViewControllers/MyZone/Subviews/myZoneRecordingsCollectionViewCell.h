//
//  myZoneRecordingsCollectionViewCell.h
//  KalturaUnified
//
//  Created by Israel Berezin on 12/30/14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TvinciSDK/TVNPVRRecordItem.h>
#import <TvinciSDK/TVLinearLayout.h>

@class myZoneRecordingsCollectionViewCell;

@protocol myZoneRecordingsCollectionViewCellDelegate <NSObject>

-(void) myZoneRecordingsCollectionViewCellDeleteItem:(myZoneRecordingsCollectionViewCell *)cell;

@end


@interface myZoneRecordingsCollectionViewCell : UICollectionViewCell

@property (assign,nonatomic) id<myZoneRecordingsCollectionViewCellDelegate> delegate;

@property (nonatomic, strong) TVNPVRRecordItem *recordItem;

@property (weak, nonatomic) IBOutlet UIImageView *seriesImage;

@property (nonatomic, weak) IBOutlet UIImageView *imageViewThumb;
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UIImageView *imgHeart;
@property (nonatomic, weak) IBOutlet UILabel *subTitle;
@property (weak, nonatomic) IBOutlet UIView *infoView;

@property (nonatomic, weak) IBOutlet UIView *viewWatchList;
@property (weak, nonatomic) IBOutlet UIButton *x_btn;

@property (nonatomic, weak) BaseViewController *delegateController;

- (void)updateData:(TVNPVRRecordItem *)item isEditMode:(BOOL)isEditMode isSeiresMode:(BOOL)isSeiresMode;

+(NSString *) nibName;
+(UIEdgeInsets) cellEdgeInsets;
- (IBAction)delateItem:(id)sender;
- (void)clearMediaCell;
@end
