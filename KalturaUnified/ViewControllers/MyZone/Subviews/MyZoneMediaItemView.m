//
//  MyZoneMediaItemView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneMediaItemView.h"
#import "UIImageView+WebCache.h"
#import "MyZoneInnerViewController.h"

@implementation MyZoneMediaItemView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    UIView *borderView = [_imageViewThumb superview];
    borderView.layer.cornerRadius = 2;
    borderView.layer.masksToBounds = YES;
    
    _imageViewThumb.layer.cornerRadius = 2;
    _imageViewThumb.layer.masksToBounds = YES;
    
    [Utils setLabelFont:_labelTitle];
    
    titleMaxHeight = _labelTitle.frame.size.height;
    
    _imageViewThumb.image = [UIImage imageNamed:@"placeholder_2_3.png"];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRegnize:)];
    [self addGestureRecognizer:recognizer];
    
    _buttonCross.alpha = 0.0;
    _buttonCross.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    [Utils setLabelFont:_labelItems bold:YES];
    _labelItems.backgroundColor = [UIColor colorWithRed:245.0/256.0 green:245.0/256.0 blue:245.0/256.0 alpha:1];
    _labelItems.layer.cornerRadius = 2;
    _labelItems.clipsToBounds = YES;
    
}

- (void)tapGestureRegnize:(UITapGestureRecognizer *)recognizer
{
    if (_mediaArray && _delegateInnerController)
    {
        [_delegateInnerController openSeriesMediaArray:_mediaArray];
    }
    else if (_mediaItem && _delegateController)
    {
        [_delegateController openMediaItem:_mediaItem];
    }
}

- (void)updateData:(TVMediaItem *)item
{
    
    if ([item isKindOfClass:[NSMutableArray class]])
    {
        _mediaArray = (NSMutableArray *)item;
        _mediaItem = [_mediaArray firstObject];
        
        _imageViewGroupped.hidden = NO;
        _labelItems.hidden = NO;
        
        _labelItems.text = [NSString stringWithFormat:@"%ld %@", (long)[_mediaArray count], LS(@"my_zone_grouped_items")];
        CGSize sizeLbl = [_labelItems.text sizeWithAttributes:@{NSFontAttributeName:_labelItems.font}];
        CGFloat labelSideMargin = 5;
        int widthLbl = sizeLbl.width + (labelSideMargin * 2);
        _labelItems.frame = CGRectMake([_labelItems superview].frame.size.width - labelSideMargin - widthLbl, [_labelItems superview].frame.size.height - labelSideMargin - sizeLbl.height, widthLbl, sizeLbl.height);
        [self bringSubviewToFront:self.buttonCross];
    }
    else
    {
        _mediaItem = item;
        _mediaArray = nil;

        _imageViewGroupped.hidden = YES;
        _labelItems.hidden = YES;
    }
    
    [_imageViewThumb sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:_mediaItem andSize:CGSizeMake(_imageViewThumb.frame.size.width * 2, _imageViewThumb.frame.size.height * 2)] placeholderImage:[UIImage imageNamed:@"placeholder_2_3.png"]];
    
    if (_mediaArray)
    {
        _labelTitle.text = [[Utils getSeriesName:_mediaItem] uppercaseString];
    }
    else
    {
        _labelTitle.text = [_mediaItem.name uppercaseString];
    }
    
    _viewWatchList.hidden = YES;
}

@end
