//
//  MyZoneTabCollectionViewCell.h
//  KalturaUnified
//
//  Created by Rivka Peleg on 2/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyZoneTabCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) NSDictionary * menuItem;
@property (weak, nonatomic) IBOutlet UIButton *button;

@property (strong, nonatomic) IBOutlet UIView * rightSeperator;
@property (strong, nonatomic) IBOutlet UIView * leftSeperator;

@end
