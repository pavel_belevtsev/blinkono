//
//  MyZoneMediaItemView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 12.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MyZoneInnerViewController;

@interface MyZoneMediaItemView : UINibView {
    
    CGFloat titleMaxHeight;
    BOOL homePhone;
    
}

- (void)updateData:(TVMediaItem *)item;

@property (nonatomic, strong) TVMediaItem *mediaItem;
@property (nonatomic, strong) NSMutableArray *mediaArray;

@property (nonatomic, weak) IBOutlet UIImageView *imageViewThumb;
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UIImageView *imgHeart;
@property (nonatomic, weak) IBOutlet UILabel *labelLike;

@property (nonatomic, weak) IBOutlet UIButton *buttonCross;

@property (nonatomic, weak) IBOutlet UIView *viewWatchList;

@property (nonatomic, weak) BaseViewController *delegateController;

@property (nonatomic, weak) MyZoneInnerViewController *delegateInnerController;

@property (nonatomic, weak) IBOutlet UIImageView *imageViewGroupped;
@property (nonatomic, weak) IBOutlet UILabel *labelItems;

@end
