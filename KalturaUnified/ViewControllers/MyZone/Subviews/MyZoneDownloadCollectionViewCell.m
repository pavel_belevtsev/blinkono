//
//  MyZoneDownloadCollectionViewCell.m
//  KalturaUnified
//
//  Created by Alex Zchut on 5/18/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneDownloadCollectionViewCell.h"
#import "MyZoneMediaItemView.h"
#import "YLProgressBar.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Additions.h"
#import "UIView+Additions.h"
#import "HorizontalPaddingLabel.h"

@interface MyZoneDownloadCollectionViewCell () {
    YLProgressBar *downloadProgress;
    UIImageView *downloadIcon;
    UILabel *downloadIconTitle;
    UIButton *downloadAction;
    
    UIView *viewImageContainer;
    UIView *viewItemDescrption;
    UIButton *btnDelete;
}


@property (strong, nonatomic) HorizontalPaddingLabel * resumeWatchingLabel;

@end

@implementation MyZoneDownloadCollectionViewCell

#define kDownloadIcon_paused @"mytv_download_icon"
#define kDownloadIcon_resumed @"mytv_pause_icon"
#define kTagImage               1001
#define kTagDownloadOverlay     1002
#define kTagTicketsOverlay      1005
#define kTagDetailsTitle        1003
#define kTagDetailsStatus       1004
#define kBorderWidth            2
#define kTagResumeWatching      1006

- (void)prepareForReuse {
    [super prepareForReuse];
    [btnDelete removeFromSuperview];
    [viewImageContainer removeAllSubviews];
    
    if (self.downloadItem) {
        [self.downloadItem removeDelegate:self];
    }
}

//-(void) didEndDisplayingCell {
//    if (self.downloadItem) {
//        [self.downloadItem removeDelegate:self];
//    }
//}

-(void) setItemDetails:(TVMediaItem*)mediaItem editMode:(BOOL)editMode {
    self.downloadItem = mediaItem.downloadItemRef;
    self.mediaItem = mediaItem;
    [self createItemImage];
    [self createItemDescrption];
    
    if (editMode) {
        [self createDeleteButton];
    }

    [self.downloadItem addDelegate:self];
}

- (void) createDeleteButton {
    
    __block MyZoneDownloadCollectionViewCell *sself = self;
    
    btnDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDelete.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:btnDelete];
    [btnDelete setImage:[UIImage imageNamed:@"myzone_cross_btn"] forState:UIControlStateNormal];
    [btnDelete setTouchUpInsideAction:^(UIButton *btn) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:LS(@"my_zone_delete_message_wo_params") delegate:nil cancelButtonTitle:LS(@"no") otherButtonTitles:LS(@"yes"), nil];
        
        [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [sself.delegate deleteCellMediaItem:sself.mediaItem];
            }
        }];
    }];
    
    UIView *itemImage = [viewImageContainer viewWithTag:kTagImage];
    CGPoint offset = CGPointMake(itemImage.left, itemImage.top);
    CGSize buttonSize = (isPad) ? (CGSize){36, 36} : (CGSize){25, 25};
    NSDictionary *metrics = @{@"offsetX": @(offset.x - buttonSize.width/2 + 5),
                              @"offsetY": @(offset.y - ((isPad) ? buttonSize.height/3 : 0)),
                              @"width": @(buttonSize.width),
                              @"height": @(buttonSize.height)};
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(offsetY)-[btnDelete(width)]" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(btnDelete)]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnDelete(height)]-(offsetX)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(btnDelete)]];
    btnDelete.alpha = 0.2;
    [UIView animateWithDuration:0.3 animations:^{
        btnDelete.alpha = 1.0;
    }];
}

- (void) createItemImage {
    
    if (!viewImageContainer) {
        viewImageContainer = [UIView new];
        viewImageContainer.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:viewImageContainer];
        NSDictionary *metrics = @{@"height": (isPad) ? @(196) : @(128)};
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[viewImageContainer(height)]" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(viewImageContainer)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[viewImageContainer]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewImageContainer)]];
    }
    else {
        [viewImageContainer removeAllSubviews];
    }
    
    UIImageView *imgItemPicture = [UIImageView new];
    imgItemPicture.tag = kTagImage;
    imgItemPicture.translatesAutoresizingMaskIntoConstraints = NO;
    imgItemPicture.contentMode = UIViewContentModeScaleToFill;
    [viewImageContainer addSubview:imgItemPicture];

    //get viewImageContainer size
    [viewImageContainer layoutIfNeeded];
    
    NSDictionary *metrics;
    UIImage *placeholder;
    if (self.mediaItem.isEpisode) {
        placeholder = [UIImage imageNamed:@"item_placeholder_16_9"];
        metrics = @{@"top": (isPad) ? @(45.5) : @(26),
                    @"bottom": (isPad) ? @(45.5) : @(26),
                    @"side": (isPad) ? @(35.5) : @(5)};
    }
    else {
        placeholder = [UIImage imageNamed:@"item_placeholder_2_3"];
        metrics = @{@"top": (isPad) ? @(12) : @(0),
                    @"bottom": (isPad) ? @(0) : @(0),
                    @"side": (isPad) ? @(64) : @(30)};
    }

    [viewImageContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(top)-[imgItemPicture]-(bottom)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(imgItemPicture)]];
    [viewImageContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(side)-[imgItemPicture]-(side)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(imgItemPicture)]];
    
    //get viewImageContainer subviews sizes
    [viewImageContainer layoutSubviews];
    
    if (self.downloadItem.downloadState == KADownloadItemDownloadStateDownloaded) {
        if (![MediaGRA isPriceReasonPurchased:self.mediaItem.priceType]) {
            UIImageView *imgItemTicketsOverlay = [UIImageView new];
            imgItemTicketsOverlay.tag = kTagTicketsOverlay;
            imgItemTicketsOverlay.translatesAutoresizingMaskIntoConstraints = NO;
            imgItemTicketsOverlay.contentMode = UIViewContentModeCenter;
            imgItemTicketsOverlay.image = [UIImage imageNamed:@"myzone_watchlist_icon"];
            imgItemTicketsOverlay.backgroundColor = [CS(@"000000") colorWithAlphaComponent:.75];
            [imgItemPicture addSubview:imgItemTicketsOverlay];
            
            NSDictionary *metrics = @{@"border": @(kBorderWidth)};
            [imgItemPicture addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(border)-[imgItemTicketsOverlay]-(border)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(imgItemTicketsOverlay)]];
            [imgItemPicture addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(border)-[imgItemTicketsOverlay]-(border)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(imgItemTicketsOverlay)]];
            
            [self updateStatusTextLabel:YES];
        }
        else {

            if (_resumeWatchingLabel == nil) {
                _resumeWatchingLabel = [HorizontalPaddingLabel new];
                _resumeWatchingLabel.additionalPaddingSize = CGSizeMake(10, 10);
                _resumeWatchingLabel.textAlignment= NSTextAlignmentCenter;
                _resumeWatchingLabel.font = [UIFont boldSystemFontOfSize:isPad?10:7];
                _resumeWatchingLabel.text = [LS(@"media_item_label_continue") uppercaseString];
                _resumeWatchingLabel.backgroundColor = [UIColor colorWithRed:61.0/255.0 green:126.0/255.0 blue:0 alpha:1];
                _resumeWatchingLabel.textColor = [UIColor whiteColor];
                _resumeWatchingLabel.translatesAutoresizingMaskIntoConstraints = NO;
            }
            else {
                [_resumeWatchingLabel removeFromSuperview];
            }
            
            [imgItemPicture addSubview:_resumeWatchingLabel];
            NSDictionary * metrics = @{@"bottom": isPad?@(10):@(4)};
            [imgItemPicture addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_resumeWatchingLabel]-(bottom)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(_resumeWatchingLabel)]];
            [imgItemPicture addConstraint:[NSLayoutConstraint constraintWithItem:_resumeWatchingLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:imgItemPicture attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];

            CGFloat duration = CGFLOAT_MAX;
            duration = self.mediaItem.duration? self.mediaItem.duration: duration;
            CGFloat watchedDurationInPercents = self.downloadItem.mediaMark.locationSec/duration;
            BOOL watchedDurationInRange = (watchedDurationInPercents >0.05 && watchedDurationInPercents<0.95);
            self.resumeWatchingLabel.hidden = !watchedDurationInRange;
            [[imgItemPicture viewWithTag:kTagTicketsOverlay] removeFromSuperview];
        }
        
        placeholder = [placeholder imageBorderedWithColor:[UIColor whiteColor] borderWidth:kBorderWidth];
    }
    else {
        [self addOverlayToImageView:imgItemPicture forState: self.downloadItem.downloadState];
    }
    
    [imgItemPicture sd_setImageWithURL:[Utils getBestPictureURLForMediaItem:self.mediaItem andSize:imgItemPicture.size] placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        if (self.downloadItem.downloadState == KADownloadItemDownloadStateDownloaded) {
            imgItemPicture.image = [imgItemPicture.image imageBorderedWithColor:[UIColor whiteColor] borderWidth:kBorderWidth];
        }
    }];
}

- (void) createItemDescrption {
    
    UILabel *lblStatus = (UILabel*)[viewItemDescrption viewWithTag:kTagDetailsStatus];
    UILabel *lblTitle = (UILabel*)[viewItemDescrption viewWithTag:kTagDetailsTitle];

    if (!viewItemDescrption) {
        viewItemDescrption = [UIView new];
        viewItemDescrption.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:viewItemDescrption];
        NSDictionary *metrics = @{@"bottomSpace": (isPad) ? @10 : @0};

        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[viewImageContainer]-7-[viewItemDescrption(>=0)]-(bottomSpace)-|" options:0 metrics:metrics views:NSDictionaryOfVariableBindings(viewImageContainer, viewItemDescrption)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[viewItemDescrption]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewItemDescrption)]];

        lblStatus = [UILabel new];
        lblStatus.translatesAutoresizingMaskIntoConstraints = NO;
        lblStatus.tag = kTagDetailsStatus;
        lblStatus.font = Font_Reg(isPad ? 13 : 11);
        lblStatus.textColor = CS(@"959595");
        lblStatus.textAlignment = NSTextAlignmentCenter;
        [viewItemDescrption addSubview:lblStatus];
        [viewItemDescrption addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[lblStatus]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblStatus)]];

        lblTitle = [UILabel new];
        lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
        lblTitle.tag = kTagDetailsTitle;
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.numberOfLines = 2;
        lblTitle.lineBreakMode = NSLineBreakByWordWrapping;

        [viewItemDescrption addSubview:lblTitle];
        [viewItemDescrption addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(<=5)-[lblTitle]-(>=0)-[lblStatus]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle, lblStatus)]];
        [viewItemDescrption addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[lblTitle]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblTitle)]];
    }

    [self updateStatusTextLabel:YES];
    [self updateTitleTextLabel];
}

- (void) updateTitleTextLabel {
    UILabel *lblTitle = (UILabel*)[viewItemDescrption viewWithTag:kTagDetailsTitle];
    NSString *description = [self.mediaItem.name uppercaseString];
    
    if (self.downloadItem.downloadState == KADownloadItemDownloadStateDownloaded) {
        if (!self.downloadItem.wasWatched) {
            description = [@"•" stringByAppendingFormat:@" %@", [self.mediaItem.name uppercaseString]];
        }
    }
    
    NSDictionary* attributes = @{NSForegroundColorAttributeName : APST.brandTextColor, NSFontAttributeName : Font_Reg(isPad?15:12)};
    if (description)
    {
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:description attributes:attributes];
        
        NSRange dotStrRange = [description rangeOfString:@"•"];
        if (dotStrRange.location != NSNotFound) {
            [attributedText setAttributes:@{NSForegroundColorAttributeName : APST.brandTextColor, NSFontAttributeName : Font_Reg(14)} range:dotStrRange];
        }

        lblTitle.text = nil;
        lblTitle.attributedText = attributedText;
    }
    else
    {
        lblTitle.text = nil;
        lblTitle.attributedText = nil;
    }
}

- (void) updateStatusTextLabel:(BOOL)isInitial {
    UILabel *lblStatus = (UILabel*)[viewItemDescrption viewWithTag:kTagDetailsStatus];
    lblStatus.text = [self downloadStatusText:isInitial];
}

- (NSString*) downloadStatusText:(BOOL)isInitial {
    NSString *text = self.downloadItem.updateDateString;
    switch (self.downloadItem.downloadState) {
        case KADownloadItemDownloadStateResumed:
        case KADownloadItemDownloadStateInProgress:
            text = LS(@"downloading");
            break;
        case KADownloadItemDownloadStateDownloaded:
            if ([MediaGRA isPriceReasonPurchased:self.mediaItem.priceType]) {
                text = [text stringByAppendingFormat:@" - %@", self.downloadItem.sizeOnDisk];
            }
            else {
                text = [text stringByAppendingFormat:@" - %@", LS(@"expired")];
            }
            break;
        case KADownloadItemDownloadStateInterrupted:
        case KADownloadItemDownloadStatePaused:
            text = [text stringByAppendingFormat:@" - %@", LS(@"paused")];
            break;
        case KADownloadItemDownloadStateInDownloadQueue:
            text = [text stringByAppendingFormat:@" - %@", LS(@"pending")];
            break;
        case KADownloadItemDownloadStateFailed:
            text = [text stringByAppendingFormat:@" - %@", LS(@"failed")];
            if (!isInitial) {
                [[ErrorView sharedInstance] showMessageOnTarget:[NSString stringWithFormat:LS(@"dtg_download_failed_title"), self.mediaItem.name] image:nil target:[self.delegate getContainerView]];
            }
            break;
        default:
            break;
    }
    return text;
}

- (void) addOverlayToImageView:(UIImageView*) target forState:(KADownloadItemDownloadState) state {
    [[target viewWithTag:kTagDownloadOverlay] removeFromSuperview];
    UIView *view = [UIView new];
    view.tag = kTagDownloadOverlay;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [target addSubview:view];
    
    [target addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
    [target addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(view)]];
    
    view.backgroundColor = [CS(@"000000") colorWithAlphaComponent:.75];

    NSString *iconName = nil;
    NSString *titleText = nil;

    BOOL showOverlayButtons = FALSE;
    
    switch (state) {
        case KADownloadItemDownloadStateResumed:
        case KADownloadItemDownloadStateInProgress: {
            showOverlayButtons = [APP_SET isOffline]?NO:TRUE;
            iconName = kDownloadIcon_resumed;
            titleText = [LS(@"pause") lowercaseString];
        }
            break;
        case KADownloadItemDownloadStatePaused:
        case KADownloadItemDownloadStateFailed:
        case KADownloadItemDownloadStateInterrupted:
        case KADownloadItemDownloadStateInDownloadQueue:
        {
            showOverlayButtons = [APP_SET isOffline]?NO:TRUE;
            iconName = kDownloadIcon_paused;
            titleText = [LS(@"resume") lowercaseString];
        }
            break;
        default:
            break;
    }
    
    if (showOverlayButtons) {
        downloadIcon = [UIImageView new];
        downloadIcon.translatesAutoresizingMaskIntoConstraints = NO;
        [downloadIcon setImage:[UIImage imageNamed:iconName]];
        [view addSubview:downloadIcon];
        
        [view addConstraint:[NSLayoutConstraint constraintWithItem:downloadIcon
                                                           attribute:NSLayoutAttributeCenterY
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:view
                                                           attribute:NSLayoutAttributeCenterY
                                                          multiplier:1.0
                                                            constant:0.0]];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:downloadIcon
                                                           attribute:NSLayoutAttributeCenterX
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:view
                                                           attribute:NSLayoutAttributeCenterX
                                                          multiplier:1.0
                                                            constant:0.0]];
        
        [view addConstraint:[NSLayoutConstraint constraintWithItem:downloadIcon
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:downloadIcon
                                                         attribute:NSLayoutAttributeHeight
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        [view addConstraint:[NSLayoutConstraint constraintWithItem:downloadIcon
                                                         attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:(isPad) ? 26:20]];
        
        downloadIconTitle = [UILabel new];
        downloadIconTitle.translatesAutoresizingMaskIntoConstraints = NO;
        downloadIconTitle.font = Font_Reg((isPad) ? 14:10);
        downloadIconTitle.text = titleText;
        downloadIconTitle.textColor = CS(@"eaeaea");
        [view addSubview:downloadIconTitle];
        [view addConstraint:[NSLayoutConstraint constraintWithItem:downloadIconTitle
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:downloadIcon
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1.0
                                                          constant:0.0]];
        
        [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[downloadIcon]-5-[downloadIconTitle]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(downloadIcon, downloadIconTitle)]];
    }

    if (![APP_SET isOffline])
    {
        downloadProgress = [YLProgressBar new];
        downloadProgress.userInteractionEnabled = FALSE;
        downloadProgress.translatesAutoresizingMaskIntoConstraints = NO;
        [downloadProgress setProgress:self.downloadItem.currentProgress animated:NO];
        downloadProgress.type = YLProgressBarTypeFlat;
        downloadProgress.stripesOrientation = YLProgressBarStripesOrientationRight;
        downloadProgress.stripesDirection = YLProgressBarStripesDirectionRight;
        downloadProgress.indicatorTextDisplayMode = YLProgressBarIndicatorTextDisplayModeNone;
        downloadProgress.behavior = YLProgressBarBehaviorWaiting;

        [view addSubview:downloadProgress];
        [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[downloadProgress]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(downloadProgress)]];
        [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[downloadProgress(3)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(downloadProgress)]];
        downloadProgress.backgroundColor = [UIColor clearColor];
        downloadProgress.trackTintColor = [UIColor clearColor];
        if (state == KADownloadItemDownloadStateInProgress) {
            downloadProgress.progressTintColors = @[APST.brandColor];
        }
        else {
            downloadProgress.progressTintColors = @[CS(@"656565")];
        }

    }

    __weak MyZoneDownloadCollectionViewCell *sself = self;

    downloadAction = [UIButton buttonWithType:UIButtonTypeCustom];
    downloadAction.translatesAutoresizingMaskIntoConstraints = NO;
    [downloadAction setTitle:@"" forState:UIControlStateNormal];
    [downloadAction.titleLabel setFont:Font_Bold(14)];
    [downloadAction setTouchUpInsideAction:^(UIButton *btn) {

        if ([APP_SET isOffline])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"stg_Uncompleted_Download") message:LS(@"stg_Uncompleted_Download_message") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];
            [alertView show];
        }
        else
        {
            switch (sself.downloadItem.downloadState) {
                case KADownloadItemDownloadStateInProgress:
                case KADownloadItemDownloadStateResumed: {
                    [sself.downloadItem pause];
                }
                    break;
                case KADownloadItemDownloadStateGetInfo:
                case KADownloadItemDownloadStateFailed: {
                    [[DTGManager sharedInstance] isOkToDownload:^(BOOL result) {
                        if (result) {
                            [sself.downloadItem selectVideoStreams:@[@(0)] completion:^(BOOL success) {
                                if (success) {
                                    [sself.downloadItem selectAudioStreams:@[@(0)] completion:^(BOOL success) {
                                        if (success) {
                                            [sself.downloadItem startDownload];
                                        }
                                    }];
                                }
                            }];
                        }
                        else {
                            [[ErrorView sharedInstance] showMessageOnTarget:LS(@"dtg_connection_stopped_no_network_title") image:nil target:[sself.delegate getContainerView]];
                        }
                    }];
                }
                    break;
                case KADownloadItemDownloadStatePaused:
                case KADownloadItemDownloadStateInterrupted:
                case KADownloadItemDownloadStateInDownloadQueue: {
                    [[DTGManager sharedInstance] isOkToDownload:^(BOOL result) {
                        if (result) {
                            [sself.downloadItem resume];
                        }
                        else {
                            [[ErrorView sharedInstance] showMessageOnTarget:LS(@"dtg_connection_stopped_no_network_title") image:nil target:[sself.delegate getContainerView]];
                        }
                    }];
                }
                    break;
                default:
                    break;
                    
            }
        }

    }];
    [view addSubview:downloadAction];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[downloadAction]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(downloadAction)]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[downloadAction]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(downloadAction)]];
}

#pragma mark - KADownloadItemDelegates
-(void)downloadFinished:(KADownloadItem * __nonnull)item {
    downloadProgress.hidden = TRUE;
    UIImageView *imgItemPicture = (UIImageView*)[viewImageContainer viewWithTag:kTagImage];
    [[imgItemPicture viewWithTag:kTagDownloadOverlay] removeFromSuperview];
    [downloadAction removeFromSuperview];
    [self updateTitleTextLabel];
    imgItemPicture.image = [imgItemPicture.image imageBorderedWithColor:[UIColor whiteColor] borderWidth:2.0];
    [self updateStatusTextLabel:NO];
}

-(void)downloadInterrupted:(KADownloadItem * __nonnull)item {
    switch (item.lastInterruptionReason) {
        case KADownloadItemInterruptionReasonDownloadQuotaLimit:
            //no need to show toast message on quota, show only on media page
            break;
        case KADownloadItemInterruptionReasonDeviceOutOfFreeSpace: {
            NSString *message = LS(@"dtg_out_of_disk_space_title");
            [[ErrorView sharedInstance] showMessageOnTarget:message image:nil target:[self.delegate getContainerView]];
        }
            break;
        case KADownloadItemInterruptionReasonConnectivity:
            break;
        default:
            break;
    }
    [self downloadPaused:item];
}

-(void)downloadFailed:(KADownloadItem * __nonnull)item error:(NSError * __nullable)error {
    [self downloadPaused:item];
}

-(void)downloadPaused:(KADownloadItem * __nonnull)item {
    downloadProgress.progressTintColors = @[CS(@"747474")];
    [downloadIcon setImage:[UIImage imageNamed:kDownloadIcon_paused]];
    downloadIconTitle.text = [LS(@"resume") lowercaseString];
    [downloadProgress setProgress:item.currentProgress animated:NO];
    [self updateStatusTextLabel:NO];
}

-(void)downloadResumed:(KADownloadItem * __nonnull)item {
    downloadProgress.hidden = FALSE;
    downloadProgress.progressTintColors = @[APST.brandColor];
    [downloadIcon setImage:[UIImage imageNamed:kDownloadIcon_resumed]];
    downloadIconTitle.text = [LS(@"pause") lowercaseString];
    [self updateStatusTextLabel:NO];
}

-(void)downloadStarted:(KADownloadItem * __nonnull)item {
    downloadProgress.hidden = FALSE;
    downloadProgress.progressTintColors = @[APST.brandColor];
    [downloadProgress setProgress:0 animated:NO];
    [self updateStatusTextLabel:NO];
}

-(void)downloadWillStart:(KADownloadItem * __nonnull)item {
    downloadProgress.hidden = FALSE;
    downloadProgress.progressTintColors = @[CS(@"747474")];
    [downloadIcon setImage:[UIImage imageNamed:kDownloadIcon_resumed]];
    downloadIconTitle.text = [LS(@"pause") lowercaseString];
    [self updateStatusTextLabel:NO];
}

-(void)downloadInProgress:(KADownloadItem * __nonnull)item {
    [downloadProgress setProgress:item.currentProgress animated:YES];
    [self updateStatusTextLabel:NO];
}

@end
