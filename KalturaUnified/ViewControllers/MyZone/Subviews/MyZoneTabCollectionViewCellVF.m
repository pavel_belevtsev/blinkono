//
//  MyZoneTabCollectionViewCellVF.m
//  Vodafone
//
//  Created by Israel Berezin on 3/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneTabCollectionViewCellVF.h"
#import "UIButton+Color.h"

@implementation MyZoneTabCollectionViewCellVF

- (void)awakeFromNib
{
    [super awakeFromNib];
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)setMenuItem:(NSDictionary *)menuItem
{
    [super setMenuItem:menuItem];
    [self.button setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.button setBackgroundImage:nil forState:UIControlStateSelected];
    self.selectedView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-2, self.frame.size.width, 2)];
    [self.selectedView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.selectedView];
    self.selectedView.hidden=YES;
    if (self.isSelected)
    {
         self.selectedView.hidden=NO;
    }
    self.rightSeperator.hidden = YES;
    self.leftSeperator.hidden=YES;
    
//    if (isPhone)
//    {
////        UIColor * grayColor =[UIColor colorWithRed:77.0/255.0 green:77.0/255.0 blue:77.0/255.0 alpha:1];
////        [self.button setBackgroundColor:grayColor forState:UIControlStateNormal];
////        [self.button setBackgroundImage:[img imageWithColor:grayColor] forState:UIControlStateNormal];
////        self.button.titleLabel.font = [UIFont fontWithName:self.button.titleLabel.font.fontName size:11];
//    }
//    else
//    {
        [self.button setBackgroundColor:[UIColor clearColor] forState:UIControlStateNormal];
       // self.button.titleLabel.font = [UIFont fontWithName:self.button.titleLabel.font.fontName size:18];
        [self.button setTitleColor:[VFTypography instance].grayScale5Color forState:UIControlStateNormal];
        [self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
//    }

}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    [self.button setHighlighted:highlighted];
    self.selectedView.hidden = !highlighted;
}

-(void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self.button setSelected:selected];
    self.selectedView.hidden = !selected;
    self.isSelected = selected;
}


@end
