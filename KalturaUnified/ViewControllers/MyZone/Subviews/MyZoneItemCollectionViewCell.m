//
//  MyZoneItemCollectionViewCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneItemCollectionViewCell.h"
#import "MyZoneMediaItemView.h"

@implementation MyZoneItemCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        
        self.mediaItemView = [[MyZoneMediaItemView alloc] initWithNib];
        _mediaItemView.frame = self.bounds;
        [self addSubview:_mediaItemView];
        
    }
    return self;
}

- (void)updateCellWithMediaItem:(TVMediaItem *)mediaItem {
    
    _mediaItemView.hidden = NO;
    _mediaItemView.delegateController = _delegateController;
    [_mediaItemView updateData:mediaItem];
    
}

- (void)clearMediaCell {
    
    _mediaItemView.hidden = YES;
    
}

-(void) setItemDetails:(TVMediaItem*)mediaItem {
    
}

@end
