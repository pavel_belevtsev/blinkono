//
//  MyZoneItemCollectionViewCell.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MyZoneMediaItemView;

@interface MyZoneItemCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) MyZoneMediaItemView *mediaItemView;
@property (nonatomic, weak) BaseViewController *delegateController;

- (void) updateCellWithMediaItem:(TVMediaItem *)mediaItem;
- (void) clearMediaCell;
- (void) setItemDetails:(TVMediaItem*)mediaItem;
@end
