//
//  MyZoneFriendsActivityCell.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 16.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneFriendsActivityCell.h"
#import "UIImage+Tint.h"

const CGFloat TVNFriendsActivityCellWatchBtnFontSize = 14.0f;
const CGFloat TVNFriendsActivityCellTitlesFontSize = 16.0f;

@implementation MyZoneFriendsActivityCell

- (void)awakeFromNib {
    
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setupActivityActionButtonUI];
    
    _timeLabel.font = Font_RegItalic(_timeLabel.font.pointSize);
    
    self.avatarImage.layer.masksToBounds = YES;
    self.avatarImage.layer.cornerRadius = CGRectGetWidth(self.avatarImage.frame) / 2;
}

- (void)setupActivityActionButtonUI {
    UIImage *brandImage = [UIImage imageNamed:@"brand_button"];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:APST.brandColor];
    UIImage *sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [self.activityActionButton setBackgroundImage:sizeableBrandImage forState:UIControlStateHighlighted];
    UIImage *img = [UIImage imageNamed:@"brand_button"];
    img = [img imageTintedWithColor:CS(@"4e4e4e")];
    [self.activityActionButton setBackgroundImage:[img resizableImageWithCapInsets:UIEdgeInsetsMake(10,20,10,20)] forState:UIControlStateNormal];
    [self.activityActionButton setTitle:LS(@"my_zone_friends_activity_watch_now") forState:UIControlStateNormal];
    [self.activityActionButton setTitleColor:CS(@"bebebe") forState:UIControlStateNormal];
    [self.activityActionButton setTitleColor:APST.brandTextColor forState:UIControlStateHighlighted];
    self.activityActionButton.titleLabel.font = Font_Bold(TVNFriendsActivityCellWatchBtnFontSize);
}

- (void)highlightUserName:(NSString *)userName andMovieTitle:(NSString*)movieTitle forMessageString:(NSString*)message {
    
    NSString *fullMessage = [NSString stringWithFormat:@"%@ %@ %@",userName,message,movieTitle];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:fullMessage];
    if (userName.length) {
        NSRange userNameRange = [fullMessage rangeOfString:userName];
        [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:userNameRange];
        [attrString addAttribute:NSFontAttributeName value:Font_Bold(TVNFriendsActivityCellTitlesFontSize) range:userNameRange];
    }
    if (movieTitle.length) {
        NSRange movieTitleRange = [fullMessage rangeOfString:movieTitle];
        [attrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:movieTitleRange];
        [attrString addAttribute:NSFontAttributeName value:Font_Bold(TVNFriendsActivityCellTitlesFontSize) range:movieTitleRange];
    }
    if (message.length) {
        
        NSRange msgRange = [fullMessage rangeOfString:message];
        [attrString addAttribute:NSFontAttributeName value:Font_Reg(TVNFriendsActivityCellTitlesFontSize) range:msgRange];
    }
    self.frienStatusLabel.attributedText = attrString;
}

- (IBAction)activityActionBtnPressed {

    if ([self.delegate respondsToSelector:@selector( myZoneFriendsActivityCell:watchActivityOfIndex:)])
    {
        [self.delegate myZoneFriendsActivityCell:self watchActivityOfIndex:self.index];
    }
    
}


@end
