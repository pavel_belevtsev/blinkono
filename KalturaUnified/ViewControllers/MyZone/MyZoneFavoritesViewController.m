//
//  MyZoneFavoritesViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneFavoritesViewController.h"

@interface MyZoneFavoritesViewController ()

@end

@implementation MyZoneFavoritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImage * image = [UIImage imageNamed:@"favourites_icon_empty"];
    
    [super initEmptyScreen:NO title:LS(@"my_zone_empty_favorites_title") text:LS(@"my_zone_empty_favorites_sub_title") andImage:image];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
}

#pragma mark -

- (void)reloadData {
    
    [super reloadData];
    
    [self.zoneViewController initEditButtons:YES editMode:editMode];
    [self.zoneViewController changeEditButtonEnabled:NO];

    NSMutableArray *array = [[NSMutableArray alloc] init];
    [self loadData:0 array:array];
    
}

- (MediaItemBelongsToListType) mediaListType {
    return kMediaItemBelongsToListTypeFavorites;
}

- (void)loadData:(int)pageIndex array:(NSMutableArray *)array {
    
    __weak MyZoneInnerViewController *weakSelf = self;
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForGetUserItems:TVUserItemTypeFavorites mediaType:@"0" pictureSize:[TVPictureSize iPadItemCellPictureSize] pageSize:maxPageSize pageIndex:pageIndex delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
        
        for (id item in result) {
            [array addObject:item];
        }
        
        //if ([result count] == maxPageSize) {
        //    [weakSelf loadData:(pageIndex + 1) array:array];
        //} else {
            [weakSelf updateData:array useLive:NO];
        //}
        
    }];
    
    
    [request setFailedBlock:^{
        
        [self hideHUD];
        
    }];
    
    [self sendRequest:request];
    
}

- (void)deleteMediaItem:(TVMediaItem *)mediaItem {
    
    [self showHUD];

    //[self showHUDBlockingUI:YES];
    
    __weak MyZoneInnerViewController *weakSelf = self;
    
    __weak TVPAPIRequest *request = [TVPMediaAPI requestForActionDone:TVMediaActionRemoveFromFavorites mediaID:[mediaItem.mediaID integerValue] mediaType:mediaItem.mediaTypeID extraValue:0 delegate:nil];
    
    [request setCompletionBlock:^{
        
        [weakSelf deleteMediaItemComplete:mediaItem];
    }];
    
    
    [request setFailedBlock:^{
        
        [self hideHUD];
        
    }];
    
    [self sendRequest:request];
    
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
