//
//  MyZoneFavoritesViewControllerVF.m
//  Vodafone
//
//  Created by Israel Berezin on 3/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneFavoritesViewControllerVF.h"

@interface MyZoneFavoritesViewControllerVF ()

@end

@implementation MyZoneFavoritesViewControllerVF

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(isPad)// && [[self.storyboard valueForKey:@"name"] isEqualToString:@"MyZoneFullPad"])
    {
        self.mainView.backgroundColor = [VFTypography instance].darkGrayColor;
        self.viewToPageControl.backgroundColor = [VFTypography instance].darkGrayColor;
    }
}

@end
