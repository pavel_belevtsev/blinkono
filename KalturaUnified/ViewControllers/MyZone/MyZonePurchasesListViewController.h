//
//  MyZonePurchasesListViewController.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneInnerViewController.h"

@interface MyZonePurchasesListViewController : MyZoneInnerViewController 

@property (nonatomic, weak) IBOutlet UILabel *labelWatchlistPersonal;
@property (nonatomic, weak) IBOutlet UILabel *labelWatchlistHousehold;
@property (nonatomic, weak) IBOutlet UISwitch *householdSwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl *householdSegment;
@property (nonatomic, weak) IBOutlet UIView *householdSwitchView;

-(void)setupSegmentUI;

@end
