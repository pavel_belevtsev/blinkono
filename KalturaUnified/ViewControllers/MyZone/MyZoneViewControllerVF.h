//
//  MyZoneViewControllerVF.h
//  Vodafone
//
//  Created by Aviv Alluf on 1/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneViewController.h"

@interface MyZoneViewControllerVF : MyZoneViewController

@property (strong, nonatomic) NSIndexPath * selectedCell;
@end
