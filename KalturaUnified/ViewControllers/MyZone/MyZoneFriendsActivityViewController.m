//
//  MyZoneFriendsActivityViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneFriendsActivityViewController.h"
#import "MyZoneFriendsActivityCell.h"
#import "UIImageView+WebCache.h"


@interface MyZoneFriendsActivityViewController ()<MyZoneFriendsActivityCellDelegate>

@end

@implementation MyZoneFriendsActivityViewController

- (void)viewDidLoad {
    

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (isPad) {
        UIView *tableView = [_activityTableView superview];
        
        tableView.layer.cornerRadius = 6;
        tableView.layer.masksToBounds = YES;
        
    }
    
    _activityTableView.tableFooterView = [UIView new];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
 
    UIImage * image = [UIImage imageNamed:@"myzone_friends_clear"];
    [super initEmptyScreen:YES title:([LoginM isFacebookUser] ? LS(@"my_zone_friends_no_activity_title") : LS(@"my_zone_friends_activity_brand_message")) text:@"" andImage:image];


}

#pragma mark -

- (void)reloadData {

    [_activityTableView superview].hidden = YES;
    
    [super.myZoneList removeAllObjects];
    
    [self.zoneViewController initEditButtons:NO editMode:editMode];
    
    if ([LoginM isFacebookUser]) {
        
        [super reloadData];
        
        __weak MyZoneFriendsActivityViewController *weakSelf = self;
        
        NSString * siteGuid = [[[TVSessionManager sharedTVSessionManager] sharedInitObject] siteGUID];
        __weak TVPAPIRequest * request = [TVPSocialAPI requestforGetUserActivityFeedWithPageSize:100 pageIndex:0 picDimension:@"" siteGuid:siteGuid  delegate:nil];
        
        [request setCompletionBlock:^{
            NSArray *result = [[request JSONResponse] arrayByRemovingNSNulls];
            [weakSelf updateData:result];
        }];
        
        
        [request setFailedBlock:^{
            [self hideHUD];
        }];
        
        [self sendRequest:request];
        
    } else {
        [super showEmptyScreen];
    }
    
}

- (void)updateData:(NSArray *)result {
    
    if ([result count] > 0) {
        
        for (NSDictionary *item in result) {
            TVUserSocialActionDoc *action = [[TVUserSocialActionDoc alloc] initWithDictionary:item];
            [super.myZoneList addObject:action];
        }
        
        [self hideHUD];
        [self updateTableView];
        
    } else {
        [self hideHUD];
        [super showEmptyScreen];
    }
    
}



- (void)updateTableView {
    
    if ([super.myZoneList count]) {
    
        [_activityTableView reloadData];
        [_activityTableView superview].hidden = NO;
    
    } else {
        [super showEmptyScreen];
    }

}

#pragma mark - UITableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [super.myZoneList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *friendsActivityIdentifier = @"MyZoneFriendsActivityCell";
    MyZoneFriendsActivityCell *friendsActivityCell = [tableView dequeueReusableCellWithIdentifier:friendsActivityIdentifier];
    if(friendsActivityCell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:[Utils nibName:NSStringFromClass([MyZoneFriendsActivityCell class])]
                                                     owner:self
                                                   options:nil];
        friendsActivityCell = [nib objectAtIndex:0];
    }
    
    TVUserSocialActionDoc *action = [super.myZoneList objectAtIndex:indexPath.row];
    
    TVActivitySubject *user = action.activitySubject;
    TVActivityObject *mediaItem = action.activityObject;
    
    friendsActivityCell.avatarImage.image = [UIImage imageNamed:@"friend_activity_no_avatar"];
    if (user.actorPicUrl) {
        [friendsActivityCell.avatarImage sd_setImageWithURL:[NSURL URLWithString:user.actorPicUrl] placeholderImage:[UIImage imageNamed:@"friend_activity_no_avatar"]];

    }
    
    friendsActivityCell.delegate = self;
    friendsActivityCell.zoneViewController = self.zoneViewController;
    
    friendsActivityCell.index = indexPath.row;
    
    [friendsActivityCell highlightUserName:user.actorTvinciUsername
                             andMovieTitle:mediaItem.assetName
                          forMessageString:[Utils stringFromSocialAction:action]];
    friendsActivityCell.timeLabel.text = [Utils getActionDurationTimeFromNow:action.createDate];
    
    friendsActivityCell.backgroundColor = [UIColor clearColor];
    
    return friendsActivityCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    [self watchActivityOfIndex:indexPath.row];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - myZoneFriendsActivityCellDelegate methods
-(void) myZoneFriendsActivityCell:(id) sender watchActivityOfIndex:(NSInteger) index
{
    [self watchActivityOfIndex:index];
}

#pragma mark - selection

-(void) watchActivityOfIndex:(NSInteger) index
{
    TVUserSocialActionDoc *action = [super.myZoneList objectAtIndex:index];
    
    if (action.activityObject.assetType == TVAssetType_MEDIA)
    {
        
        NSString *mediaId = [Utils getStringFromInteger:action.activityObject.assetID];
        
        [self.zoneViewController openMediaItemWithMediaId:mediaId];
        
        
    }
    else
    {
        __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetEPGProgramsByIds:@[[NSString stringWithFormat:@"%d",action.activityObject.assetID]] programIdType:TVProgramIdTypeInternal pageSize:1 pageIndex:0 delegate:nil];
        
        [request setFailedBlock:^{
            NSLog(@"failed");
        }];
        
        [request setCompletionBlock:^{
            
            NSLog(@"%@",request);
            NSDictionary * dictionary = [[[request JSONResponse] lastObject] objectForKey:@"m_oProgram"];
            TVEPGProgram * program = [[TVEPGProgram alloc] initWithDictionary:dictionary];
            
            [self playChannelWithEPGChannelID:program.EPGChannelID];
            
        }];
        
        [self sendRequest:request];
        
        
    }

}
@end
