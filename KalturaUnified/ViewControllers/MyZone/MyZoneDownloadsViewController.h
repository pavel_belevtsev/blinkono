//
//  MyZoneDownloadsViewController.h
//  KalturaUnified
//
//  Created by Alex Zchut on 4/29/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneInnerViewController.h"
#import "MyZoneDownloadCollectionViewCell.h"

@interface MyZoneDownloadsViewController : MyZoneInnerViewController <MyZoneDownloadCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *iPhoneMainScrollView;
@property (weak, nonatomic) IBOutlet UIView *iPhoneMainView;

@end
