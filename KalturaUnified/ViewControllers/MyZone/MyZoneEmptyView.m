//
//  MyZoneEmptyView.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "MyZoneEmptyView.h"

@implementation MyZoneEmptyView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [Utils setLabelFont:_labelEmptyTitle bold:YES];
    [Utils setLabelFont:_labelEmptyText];
    
    [_buttonFBLogin setTitle:LS(@"login_facebook") forState:UIControlStateNormal];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+(MyZoneEmptyView *) MyZoneEmptyFullView
{
    MyZoneEmptyView *view = nil;
    NSString *nibName = @"MyZoneEmptyFullViewPad";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}
@end
