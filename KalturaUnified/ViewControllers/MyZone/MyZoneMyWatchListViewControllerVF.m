//
//  MyZoneMyWatchListViewControllerVF.m
//  Vodafone
//
//  Created by Israel Berezin on 3/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneMyWatchListViewControllerVF.h"

@interface MyZoneMyWatchListViewControllerVF ()

@end

@implementation MyZoneMyWatchListViewControllerVF

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(isPad)// && [[self.storyboard valueForKey:@"name"] isEqualToString:@"MyZoneFullPad"])
    {
        self.mainView.backgroundColor = [VFTypography instance].darkGrayColor;
        self.viewToPageControl.backgroundColor = [VFTypography instance].darkGrayColor;
    }
}

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
-(void)setupSegmentUI
{
    [super setupSegmentUI];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont systemFontOfSize:16], NSFontAttributeName,
                                [VFTypography instance].grayScale4Color, NSForegroundColorAttributeName, nil];
    
    [self.householdSegment setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    NSDictionary *attributes1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont systemFontOfSize:16], NSFontAttributeName,
                                 [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    [self.householdSegment setTitleTextAttributes:attributes1 forState:UIControlStateSelected];
    
    self.householdSegment.tintColor =[VFTypography instance].grayScale2Color;
}
@end
