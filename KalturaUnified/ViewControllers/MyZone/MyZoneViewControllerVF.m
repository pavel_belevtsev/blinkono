//
//  MyZoneViewControllerVF.m
//  Vodafone
//
//  Created by Aviv Alluf on 1/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneViewControllerVF.h"
#import "UIImage+Tint.h"

@interface MyZoneViewControllerVF ()

@end

@implementation MyZoneViewControllerVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    self.navigationView.backgroundColor = [UIColor orangeColor];

    [self.bgView setBackgroundColor:CS(@"232323")];
    if (isPhone)
    {
        [self.navigationView setBackgroundColor:CS(@"232323")];
    }
    if (isPad)
    {
        NSInteger collectionTabsWidth = [self collectionViewTabsWidth];
        CGRect frame = self.collectionViewTabs.frame;
        frame.size.width = collectionTabsWidth;
        self.collectionViewTabs.frame = frame;
      //  self.navigationView.frame = CGRectMake(0, self.navigationView.frame.origin.y, collectionTabsWidth, self.navigationView.frame.size.height);
        
        // "PSC : Vodafone Group : SYN-3580 -> My TV - The UI bottom menu is incorrect
        if([self isMyZoneFullPad])
        {
            self.collectionViewTabs.frame = CGRectMake(self.view.frame.size.width /2 - collectionTabsWidth/2, self.collectionViewTabs.frame.origin.y, collectionTabsWidth, self.collectionViewTabs.frame.size.height);
        }
        else
        {
            self.navigationView.frame = CGRectMake(self.view.frame.size.width /2 - collectionTabsWidth/2, self.navigationView.frame.origin.y, collectionTabsWidth, self.navigationView.frame.size.height);
        }

        [self.view layoutSubviews];
    }
}

- (void) rebrandTabButton:(UIButton *) button
{
    [button setBackgroundImage:nil forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"tab_highlight_background"] forState:UIControlStateDisabled];
    
    [button setTitleColor:[VFTypography instance].grayScale5Color  forState:UIControlStateDisabled];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    
    button.backgroundColor = [UIColor colorWithWhite:35.0/255.0 alpha:1];
}


- (void)updateIPadButton:(UIButton *)button {
    
    [Utils setButtonFont:button bold:YES];
    
    [button setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:[VFTypography instance].grayScale3Color] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    
    [button setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:[VFTypography instance].brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateSelected];
    
    [button setTitleColor:[VFTypography instance].grayScale8Color forState:UIControlStateNormal];
    
}

- (void)initBrandButtonUI:(UIButton *)brandButton
{
    [super initBrandButtonUI:brandButton];
    
    [[VFTypography instance] modifyBrandButtonToGray:brandButton];
}

/* "PSC : ONO : SYN-4068 CLONE - My TV - the menu is not centered */
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (isPad)
    {
        return UIEdgeInsetsZero;
    }
    CGFloat widthOfCells = 0;
    for (NSDictionary *page in self.MyZonePages) {
        NSString * title = LS(page[@"localizationKey"]);
        NSDictionary *attributes = @{NSFontAttributeName: Font_Bold(14.0)};
        CGSize size = [title sizeWithAttributes:attributes];
        if (isPad)
        {
            size.width += 40;
        }
        else
        {
            size.width += 20;
        }
        widthOfCells += size.width;
    }
    NSInteger edgeInsets = (self.view.frame.size.width - widthOfCells) / ([self.MyZonePages count] + 1);
    edgeInsets = (edgeInsets < 0) ? 0 : edgeInsets;
    
    return UIEdgeInsetsMake(0, edgeInsets, 0, 0);
}

/* "PSC : Vodafone Grup : collectionView cell to be dynamic size : JIRA ticket. */
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size;
//    if(isPad){
//        size = CGSizeMake(self.collectionViewTabs.size.width/self.MyZonePages.count, self.collectionViewTabs.size.height);
//    }else{
        NSString * title = LS(self.MyZonePages[indexPath.row][@"localizationKey"]);
        NSDictionary *attributes = @{NSFontAttributeName: Font_Bold(14.0)};
        size = [title sizeWithAttributes:attributes];
    if (isPad)
    {
        size.width += 40;
    }
    else
    {
        size.width += 20;
    }
        size.height = self.collectionViewTabs.size.height;
//    }
    return size;
}

-(NSInteger)collectionViewTabsWidth
{
    NSInteger allWidth = 0;
    CGSize size = CGSizeZero;
    for (NSDictionary * dic in self.MyZonePages)
    {
        NSString * title = LS(dic[@"localizationKey"]);
        NSDictionary *attributes = @{NSFontAttributeName: Font_Bold(14.0)};
        size = [title sizeWithAttributes:attributes];
        if (isPad)
        {
            size.width += 41;
        }
        else
        {
            size.width += 20;
        }
        allWidth +=  size.width;
    }
    return allWidth;
}
@end
