//
//  MyZoneEmptyView.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 24.12.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "UINibView.h"

@interface MyZoneEmptyView : UINibView

@property (weak, nonatomic) IBOutlet UIImageView *imageEmpty;
@property (weak, nonatomic) IBOutlet UILabel *labelEmptyTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelEmptyText;

@property (weak, nonatomic) IBOutlet UIButton *buttonFBLogin;

@property (strong, nonatomic) IBOutlet UIView *viewLogin;

+(MyZoneEmptyView *) MyZoneEmptyFullView;


@end
