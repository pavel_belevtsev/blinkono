//
//  DTG_QuotaBarViewToolTip.m
//  KalturaUnified
//
//  Created by Israel Berezin on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "DTG_QuotaBarViewToolTip.h"
#import "UIImage+Tint.h"

#define QuotaBarToolTipWarningStatusKey @"DTG_QuotaBarToolTipWarningStatusKey"
#define QuotaBarToolTipExceededStatusKey @"DTG_QuotaBarToolTipExceededStatusKey"
#define QuotaBarToolTipNotShowAgain @"DTG_QuotaBarToolTipNotShowAgain"
#define QuotaBarToolTipShowDate @"DTG_QuotaBarToolTipShowDate"

#define FirstSpace 24
#define FirstSpacePhone 19

#define LastSpase 14
#define LastSpasePhone 21

#define kAddToolTipToView @"DTG_kaddToolTipToView"
#define kRemoveToolTipToView  @"DTG_kRemoveToolTipToView"

#define WeekTime 60*60*24*7

#define frequency 7

@interface DTG_QuotaBarViewToolTip()

@property BOOL dontShowAgainToolTip;

@end


@implementation DTG_QuotaBarViewToolTip

-(id)initWithUsedSpaceInPercent:(NSInteger)usedSpace
{
    self = [super init];
    if (self)
    {
        self.usedSpaceInPercent = usedSpace;
        [self chooseNeededToolTipType];
    }
    return  self;
}

-(void)insertUsedSpaceInPercent:(NSInteger)usedSpace
{
    self.usedSpaceInPercent = usedSpace;
    [self chooseNeededToolTipType];
}

+(DTG_QuotaBarViewToolTip *) quotaBarViewToolTip
{
    DTG_QuotaBarViewToolTip *view = nil;

    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}


-(void)chooseNeededToolTipType
{
    NSInteger percent = self.usedSpaceInPercent;
    if (percent <= 94) {
        self.toolTipType = DTG_QuotaBarToolTipNone;
    }
    else if (percent > 94 && percent < 100) {
        self.toolTipType = DTG_QuotaBarToolTipWarning;
    }
    else {
        self.toolTipType = DTG_QuotaBarToolTipExceeded;
    }
}

-(void)buildWIthQuotaDataWithOnLinkageView:(UIView *)linkageView
{
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self updateProgressBar];
    [self updateUIAndText];
    if (isPad)
    {
        [self locateUI_Pad];
    }
    else
    {
        [self locateUI_Phone];
    }
    
    ToolTipArrowType type =  ToolTipTypeArrowDynamic;
    if (isPhone)
    {
         type = ToolTipTypeArrowNone;
    }
    [self buildToolTipViewWithToolTipBoxView:self.internalView linkageView:linkageView toolTipArrowType:type toolCloseType:ToolTipCloseButtonOnly];
}

-(void)addToolTipToView:(UIView*)addView
{
    [addView addSubview: self];
    [[NSNotificationCenter defaultCenter] postNotificationName:kAddToolTipToView object:nil userInfo:nil];
}

-(void)updateProgressBar
{
    [self.fillProssedLineView setBackgroundColor:APST.brandColor];
    
    float percent = 0;
    float widthToFill =0;
    if (self.usedSpaceInPercent > 0)
    {
        percent = self.usedSpaceInPercent;
        widthToFill = (self.baseProssesLineView.frame.size.width * percent) / 100;
    }
    
    CGRect frame = self.fillProssedLineView.frame;
    frame.size.width = widthToFill;
    self.fillProssedLineView.frame = frame;
}

-(void)updateUIAndText
{
    NSString * baseString =  LS(@"dtg_quotabar_title_ipad");
    self.lblTitle.text = [NSString stringWithFormat:baseString, [@(100-self.usedSpaceInPercent) stringValue]];
    if (isPad)
    {
        [Utils setLabelFont:self.lblTitle size:20 bold:YES];
    }
    else
    {
        [Utils setLabelFont:self.lblTitle size:14 bold:YES];

    }
    self.lblTitle.textColor = [UIColor whiteColor];
    
    NSString * textToDes = nil;
    if (self.toolTipType == DTG_QuotaBarToolTipExceeded) //100%
    {
        textToDes = LS(@"dtg_out_of_quota_title");
    }
    else
    {
        textToDes = LS(@"dtg_about_to_exceed_quota_title");
    }
    self.lblDescription.text = textToDes;
    if (isPad)
    {
        [Utils setLabelFont:self.lblDescription size:16 bold:NO];
    }
    else
    {
        [Utils setLabelFont:self.lblDescription size:14 bold:NO];
    }
    self.lblDescription.textColor = CS(@"959595");
    
    self.lblDescription.numberOfLines=0;
    NSInteger programNameLineHeight = 22.f;
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = programNameLineHeight;
    style.maximumLineHeight = programNameLineHeight;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    if (self.lblDescription.text)
    {
        self.lblDescription.attributedText = [[NSAttributedString alloc] initWithString:self.lblDescription.text
                                                                    attributes:attributtes];
    }
    [self.lblDescription sizeToFit];

    self.lblDontShow.text = LS(@"quotaBarViewToolTip_BtnDontShowAgain");
    if (isPad)
    {
        [Utils setLabelFont:self.lblDontShow size:16 bold:NO];
    }
    else
    {
        [Utils setLabelFont:self.lblDontShow size:14 bold:NO];
    }
    
    if (isPhone)
    {
        self.lblDescription.textAlignment = NSTextAlignmentCenter;
    }
    self.lblDontShow.textColor = CS(@"959595");
    
    [self.seperatorView setBackgroundColor: CS(@"505050")];
    
    [self.btnGotIt setBackgroundImage:[[[UIImage imageNamed:@"btn_base_layer"] imageTintedWithColor:APST.brandColor] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 20, 10, 20)] forState:UIControlStateNormal];
    
    [Utils setButtonFont:self.btnGotIt bold:YES];
    [self.btnGotIt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.btnGotIt addTarget:self action:@selector(closeToolTip:) forControlEvents:UIControlEventTouchUpInside];

    [self.btnClose addTarget:self action:@selector(closeView:) forControlEvents:UIControlEventTouchUpInside];

    [self initSwitchUI:self.dontShowSwitch];
    
    [self.dontShowSwitch addTarget:self action:@selector(changeState:) forControlEvents:UIControlEventValueChanged];
}


-(void)locateUI_Pad
{
    CGRect frame1 =  self.lblDescription.frame;
    frame1.origin.y = self.lblTitle.frame.origin.y + self.lblTitle.frame.size.height +FirstSpace;
    self.lblDescription.frame = frame1;
    
    CGRect frame2 = self.buttomView.frame;
    frame2.origin.y =  self.lblDescription.frame.origin.y +  self.lblDescription.frame.size.height + LastSpase;
    self.buttomView.frame = frame2;
    
    NSInteger high = (frame2.origin.y + frame2.size.height);
    
    CGRect mainFrame = self.frame;
    mainFrame.size.height = high;
    self.frame = mainFrame;
}


-(void)locateUI_Phone
{
    CGRect frame1 =  self.lblDescription.frame;
    frame1.origin.y = self.baseProssesLineView.frame.origin.y + self.baseProssesLineView.frame.size.height +FirstSpacePhone;
    self.lblDescription.frame = frame1;
    
    CGRect frame2 = self.buttomView.frame;
    frame2.origin.y =  self.lblDescription.frame.origin.y +  self.lblDescription.frame.size.height + LastSpasePhone;
    self.buttomView.frame = frame2;
    
    NSInteger high = (frame2.origin.y + frame2.size.height);
    
    CGRect mainFrame = self.frame;
    mainFrame.size.height = high;
    self.frame = mainFrame;
}

- (void)initSwitchUI:(UISwitch *)switchRemeber
{
    [switchRemeber setOnTintColor:APST.brandColor];
    [switchRemeber setTintColor:CS(@"bebebe")];
    [switchRemeber setThumbTintColor:[UIColor whiteColor]];
}

- (void)changeState:(id)sender
{
    UISwitch *state = sender;
    
    self.dontShowAgainToolTip =state.on;
}

- (IBAction)closeToolTip:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kRemoveToolTipToView object:nil userInfo:nil];

    if (self.dontShowAgainToolTip)
    {
        [self saveDontShowAgain];
    }
    
    //save Date
    NSDate * now = [NSDate date];
    NSTimeInterval timeIntervalNow = [now timeIntervalSince1970];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithDouble:timeIntervalNow] forKey:QuotaBarToolTipShowDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
     
    [self close:nil];
    [self removeFromSuperview];
}

-(void)saveDontShowAgain
{
    switch (self.toolTipType)
    {
        case DTG_QuotaBarToolTipWarning:
        {
            [[NSUserDefaults standardUserDefaults] setObject:QuotaBarToolTipNotShowAgain forKey:QuotaBarToolTipWarningStatusKey];
        }
            break;
        case DTG_QuotaBarToolTipExceeded:
        {
            [[NSUserDefaults standardUserDefaults] setObject:QuotaBarToolTipNotShowAgain forKey:QuotaBarToolTipExceededStatusKey];

        }
            break;
        default:
            break;
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)isNeedToShowToolTip
{
    if (self.toolTipType == DTG_QuotaBarToolTipNone)
    {
        return NO;
    }
    
    NSDate * now = [NSDate date];
    NSTimeInterval timeIntervalNow = [now timeIntervalSince1970];
    NSTimeInterval timeIntervalOld = [[[NSUserDefaults standardUserDefaults] objectForKey:QuotaBarToolTipShowDate] doubleValue];
    
    if (timeIntervalOld > 0 && (timeIntervalOld + WeekTime) > timeIntervalNow)
    {
        ASLogInfo(@"Not yet passed a week");
        return NO;
    }
    
    NSString * status = nil;
    switch (self.toolTipType)
    {
        case DTG_QuotaBarToolTipWarning:
        {
             status = [[NSUserDefaults standardUserDefaults] objectForKey:QuotaBarToolTipWarningStatusKey];
        }
            break;
        case DTG_QuotaBarToolTipExceeded:
        {
             status =[[NSUserDefaults standardUserDefaults] objectForKey:QuotaBarToolTipExceededStatusKey];
        }
            break;
        default:
            break;
    }
    
    return (![status isEqualToString:QuotaBarToolTipNotShowAgain]);
}

- (IBAction)closeView:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kRemoveToolTipToView object:nil userInfo:nil];
    [self close:nil];
    [self removeFromSuperview];
}

@end


