//
//  QuotaBarView.m
//  KalturaUnified
//
//  Created by Israel Berezin on 1/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "DTG_QuotaBarView.h"

#define kAddToolTipToView @"DTG_kaddToolTipToView"
#define kRemoveToolTipToView @"DTG_kRemoveToolTipToView"

@implementation DTG_QuotaBarView

+(DTG_QuotaBarView *) quotaBarView
{
    DTG_QuotaBarView *view = nil;
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.lbltitle.textColor = [UIColor colorWithRed:150.0/225.0 green:150.0/225.0 blue:150.0/225.0 alpha:1];
    if (isPhone)
    {
        [Utils setLabelFont:self.lbltitle size:13 bold:NO];
    }
    else
    {
        [Utils setLabelFont:self.lbltitle size:14 bold:NO];
    }
    self.progressBar = [YLProgressBar new];
    self.progressBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self.progressBar setProgress:1 animated:NO];
    self.progressBar.userInteractionEnabled = FALSE;
    self.progressBar.type = YLProgressBarTypeFlat;
    self.progressBar.stripesOrientation = YLProgressBarStripesOrientationRight;
    self.progressBar.stripesDirection = YLProgressBarStripesDirectionRight;
    self.progressBar.indicatorTextDisplayMode = YLProgressBarIndicatorTextDisplayModeNone;
    self.progressBar.behavior = YLProgressBarBehaviorIndeterminate;
    self.progressBar.backgroundColor = [UIColor clearColor];
    self.progressBar.trackTintColor  = [UIColor darkGrayColor];
    self.progressBar.progressTintColors = @[APST.brandColor];
    
    [self.progressBarView addSubview:self.progressBar];
    [self.progressBarView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_progressBar]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_progressBar)]];
    [self.progressBarView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_progressBar]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_progressBar)]];
}

-(void)updateUIwithCurrentLibrarySize:(long long)currentLibrarySize allowedLibrarySize:(long long)allowedLibrarySize freeSpaceOnDevice:(long long)freeSpaceOnDevice
{
    [self registerToNotifcation];
    double progress = (allowedLibrarySize > 0 && currentLibrarySize < allowedLibrarySize) ? (double)currentLibrarySize/(double)allowedLibrarySize : 1;

    [self updateTitleText: progress];
    [self updateProgressBar: progress];
}

-(void)updateTitleText:(double) progress
{
    NSInteger remainSpaceInPercent = (1-progress)*100;
    NSString * text = nil;
    if (isPad)
    {
        NSString * baseString =  LS(@"dtg_quotabar_title_ipad");
        text = [NSString stringWithFormat:baseString, [@(remainSpaceInPercent) stringValue]];
    }
    else
    {
        NSString * baseString =  LS(@"dtg_quotabar_title_iphone");
        text = [NSString stringWithFormat:baseString, [@(remainSpaceInPercent) stringValue]];
    }
    
    self.lbltitle.text = text;
}

-(void)updateProgressBar: (double) progress
{
    if (progress < 0.01f) {
        progress= 0.02f;
    }
    [self.progressBar setProgress:progress animated:NO];
}

-(void)registerToNotifcation
{
    [self unRegisterToNotifcation];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(toolTipToViewIsShow:) name:kAddToolTipToView object:nil];
    [center addObserver:self selector:@selector(toolTipToViewIsRemove:) name:kRemoveToolTipToView object:nil];
}

-(void)toolTipToViewIsShow:(NSNotification *) notification
{
    if (isPad)
    {
        self.progressBar.progressTintColors = @[CS(@"969696")];
    }
}

-(void)toolTipToViewIsRemove:(NSNotification *) notification
{
    if (isPad)
    {
        self.progressBar.progressTintColors = @[APST.brandColor];
    }
}

-(void)unRegisterToNotifcation
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:kAddToolTipToView object:nil];
    [center removeObserver:self name:kRemoveToolTipToView object:nil];
}

-(void)dealloc
{
    [self unRegisterToNotifcation];
}
@end
