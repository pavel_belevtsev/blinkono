//
//  QuotaBarViewToolTip.h
//  KalturaUnified
//
//  Created by Israel Berezin on 2/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "ToolTipView.h"

typedef enum
{
    DTG_QuotaBarToolTipWarning = 0,
    DTG_QuotaBarToolTipExceeded,
    DTG_QuotaBarToolTipNone
}DTG_QuotaBarToolTipType;

@interface DTG_QuotaBarViewToolTip : ToolTipView

@property (nonatomic) NSInteger usedSpaceInPercent;

@property (nonatomic) DTG_QuotaBarToolTipType toolTipType;
@property (weak, nonatomic) IBOutlet UIView *internalView;

@property (weak, nonatomic) IBOutlet UIView *seperatorView;
@property (weak, nonatomic) IBOutlet UIView *buttomView;

@property (weak, nonatomic) IBOutlet UIView *baseProssesLineView;

@property (weak, nonatomic) IBOutlet UIView *fillProssedLineView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblDontShow;
@property (weak, nonatomic) IBOutlet UIButton *btnGotIt;
@property (weak, nonatomic) IBOutlet UISwitch *dontShowSwitch;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

-(id)initWithUsedSpaceInPercent:(NSInteger)usedSpace;
-(void)insertUsedSpaceInPercent:(NSInteger)usedSpace;

-(void)buildWIthQuotaDataWithOnLinkageView:(UIView *)linkageView;

-(void)addToolTipToView:(UIView*)addView;

-(BOOL)isNeedToShowToolTip;
- (IBAction)closeView:(id)sender;

-(void)updateUIAndText;
- (void)initSwitchUI:(UISwitch *)switchRemeber;
+(DTG_QuotaBarViewToolTip *) quotaBarViewToolTip;

@end


