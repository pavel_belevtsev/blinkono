//
//  QuotaBarView.h
//  KalturaUnified
//
//  Created by Israel Berezin on 1/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLProgressBar.h"

@interface DTG_QuotaBarView : UIView
@property (strong, nonatomic) YLProgressBar *progressBar;

@property (weak, nonatomic) IBOutlet UILabel *lbltitle;
@property (weak, nonatomic) IBOutlet UIView *progressBarView;

+(DTG_QuotaBarView *) quotaBarView;

-(void)updateUIwithCurrentLibrarySize:(long long)currentLibrarySize allowedLibrarySize:(long long)allowedLibrarySize freeSpaceOnDevice:(long long)freeSpaceOnDevice;


@end
