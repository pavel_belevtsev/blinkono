//
//  MyZoneEmptyViewVF.m
//  Vodafone
//
//  Created by Israel Berezin on 3/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "MyZoneEmptyViewVF.h"

@implementation MyZoneEmptyViewVF

/* "PSC : Vodafone Grup : change color & font : JIRA ticket. */
- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self.labelEmptyTitle setTextColor:[UIColor whiteColor]];
    
    [self.labelEmptyText setTextColor:[VFTypography instance].grayScale5Color];
    
    [Utils setLabelFont:self.labelEmptyTitle size:30 bold:YES];
    [Utils setLabelFont:self.labelEmptyText size:18 bold:NO];
}


@end
