//
//  DTGViewController.m
//  KalturaUnified
//
//  Created by Alex Zchut on 4/14/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "DTGViewController.h"

#define kCellEstimatedHeight 100.0f

@interface DTGViewController ()
@property (strong, nonatomic) NSMutableDictionary *prototypeCells;

@end

@implementation DTGViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    _labelTitle.text = LS(@"Download2GO");
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeContactAdd];
    [btn setTintColor:[UIColor whiteColor]];
    [btn setTouchUpInsideAction:^(UIButton *btn) {
        
        //[[DTGManager sharedInstance] addDownloadTask:@"309a0024-f1c6-1111-b057-5608bccad05f" fileURL:@"http://download.wavetlan.com/SVV/Media/HTTP/H264/Other_Media/H264_test5_voice_mp4_480x360.mp4"];
        //[[DTGManager sharedInstance] addDownloadTask:@"309a0024-f1c6-2222-b057-5608bccad05f" fileURL:@"http://download.wavetlan.com/SVV/Media/HTTP/H264/Other_Media/H264_test7_voiceclip_mp4_480x360.mp4"];
        //[[DTGManager sharedInstance] addDownloadTask:@"file3" fileURL:@"http://download.wavetlan.com/SVV/Media/HTTP/MP4/ConvertedFiles/Media-Convert/Unsupported/dw11222.mp4"];
        //[[DTGManager sharedInstance] addDownloadTask:@"file4" fileURL:@"http://download.wavetlan.com/SVV/Media/HTTP/MP4/ConvertedFiles/QuickTime/QuickTime_test7_4m3s_MPEG4SP_CBR_96kbps_352x288_30fps_AAC-LCv4_CBR_320kbps_Stereo_48000Hz.mp4"];
        //[[DTGManager sharedInstance] addDownloadTask:@"file5" fileURL:@"http://download.wavetlan.com/SVV/Media/HTTP/MP4/ConvertedFiles/QuickTime/QuickTime_test10_3m2s_AVC_VBR_762kbps_176x144_25fps_AAC-LCv4_CBR_320kbps_Stereo_48000Hz.mp4"];
        
//        [[DTGManager sharedInstance] addDownloadTask:[NSUUID new].UUIDString fileURL:@"https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_16x9/bipbop_16x9_variant.m3u8"];
        
        
    }];
    btn.translatesAutoresizingMaskIntoConstraints = NO;
    [self.viewNavigationBar addSubview:btn];
    [self.viewNavigationBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btn]-240-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btn)]];
    [self.viewNavigationBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[btn]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btn)]];
}

- (IBAction)buttonMenuPressed:(UIButton *)button {
    
    [[self menuContainerViewController] toggleLeftSideMenuCompletion:NULL];
}

#pragma mark - table view delegates
- (NSArray*) items {
    return nil;//[DTGManager sharedInstance].arrCurrectDownloads;
}

- (SubscriptionListCell *)tableView:(UITableView *)tableView prototypeForCellIdentifier:(NSString *)cellIdentifier
{
    SubscriptionListCell *cell;
    if (cellIdentifier) {
        cell = [self.prototypeCells objectForKey:cellIdentifier];
        if (!cell) {
            if (!self.prototypeCells) {
                self.prototypeCells = [[NSMutableDictionary alloc] init];
            }
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [self.prototypeCells setObject:cell forKey:cellIdentifier];
        }
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView cellIdentifierAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = NSStringFromClass([DTGViewControllerDownloadCell class]);
    return [Utils nibName:identifier];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kCellEstimatedHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kCellEstimatedHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellId = [self tableView:tableView cellIdentifierAtIndexPath:indexPath];
    DTGViewControllerDownloadCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    cell.delegate = self;
    cell.contentView.backgroundColor = cell.backgroundColor = [UIColor clearColor];
    
    [cell setDetails:self.items[indexPath.row] indexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:FALSE];
}

- (void) cancelDownloadForIndex:(NSInteger) index {
    //[[DTGManager sharedInstance] cancelDownloadForIndex:index];
}

- (NSURLSessionTaskState) pauseResumeDownloadForIndex:(NSInteger) index {
    return 3;//[[DTGManager sharedInstance] pauseResumeDownloadForIndex:index];
}


@end
