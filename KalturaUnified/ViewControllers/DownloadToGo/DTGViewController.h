//
//  DTGViewController.h
//  KalturaUnified
//
//  Created by Alex Zchut on 4/14/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTGViewControllerDownloadCell.h"

@interface DTGViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, DTGViewControllerDownloadCellActions>

@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, retain) IBOutlet UITableView *tblMainList;
@property (nonatomic, weak) IBOutlet UIButton *btnBack;

@end
