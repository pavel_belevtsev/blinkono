//
//  SubscriptionListCell.m
//  KalturaUnified
//
//  Created by Alex Zchut on 2/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "DTGViewControllerDownloadCell.h"
#import "UIImage+Additions.h"

@implementation DTGViewControllerDownloadCell

#define kDateFormat             @"dd/MM/yy"
#define kTextColor              CS(@"959595")
#define kWhiteColor             CS(@"ffffff")
#define kButtonColor            CS(@"5ABAF4")
#define kButtonSelectedColor    (_viewSingleItemContent) ? CS(@"4F4F4F") : CS(@"474747")

- (void) setDetails:(NSMutableDictionary*) downloadTaskData indexPath:(NSIndexPath*)indexPath {
    _indexPath = indexPath;
    NSProgress *progress = downloadTaskData[@"progressInfo"];
    
    BOOL isCurrentStateSuspended =[downloadTaskData[@"statistics"][@"isSuspended"] boolValue];
    _lblTitle.text = downloadTaskData[@"parentFileId"];
    _lblDescription.text = downloadTaskData[@"parentURL"];
    
    
    _lblFileSize.text = progress.localizedAdditionalDescription;
    _progressView.progress = progress.fractionCompleted;
    _btnCancel.titleLabel.numberOfLines = 0;
    _btnCancel.titleLabel.textAlignment = NSTextAlignmentCenter;
    _btnCancel.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnCancel.layer.cornerRadius = 4.0;
    _btnCancel.layer.masksToBounds = YES;
    [_btnCancel setTitle:[LS(@"cancel") capitalizedString] forState:UIControlStateNormal<<UIControlStateHighlighted<<UIControlStateDisabled];
    [_btnCancel setTitleColor:kWhiteColor forState:UIControlStateNormal<<UIControlStateHighlighted<<UIControlStateDisabled];
    [_btnCancel setUserInteractionEnabled:YES];
    
    _btnPauseResume.titleLabel.numberOfLines = 0;
    _btnPauseResume.titleLabel.textAlignment = NSTextAlignmentCenter;
    _btnPauseResume.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnPauseResume.layer.cornerRadius = 4.0;
    _btnPauseResume.layer.masksToBounds = YES;
    [_btnPauseResume setTitle:[LS(@"pause") capitalizedString] forState:UIControlStateNormal<<UIControlStateDisabled];
    [_btnPauseResume setTitle:[LS(@"resume") capitalizedString] forState:UIControlStateSelected];
    [_btnPauseResume setTitleColor:kWhiteColor forState:UIControlStateNormal<<UIControlStateHighlighted<<UIControlStateDisabled];
    [_btnPauseResume setUserInteractionEnabled:YES];
    [_btnPauseResume setSelected:isCurrentStateSuspended];
}

- (IBAction)btnCancelPressed:(id)sender {
    [self.delegate cancelDownloadForIndex:self.indexPath.row];
}

- (IBAction)btnPauseResumePressed:(id)sender {
    NSURLSessionTaskState state = [self.delegate pauseResumeDownloadForIndex:self.indexPath.row];
    [_btnPauseResume setSelected: (state == NSURLSessionTaskStateSuspended) ? YES : NO];
}
@end
