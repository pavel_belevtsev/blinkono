//
//  SubscriptionListCell.h
//  KalturaUnified
//
//  Created by Alex Zchut on 2/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DTGViewControllerDownloadCellActions <NSObject>

- (void) cancelDownloadForIndex:(NSInteger) index;
- (NSURLSessionTaskState) pauseResumeDownloadForIndex:(NSInteger) index;

@end
@interface DTGViewControllerDownloadCell : UITableViewCell

- (void) setDetails:(NSMutableDictionary*) downloadTaskData indexPath:(NSIndexPath*)indexPath;

@property (nonatomic, retain) IBOutlet UILabel *lblTitle, *lblFileSize, *lblDescription;
@property (nonatomic, retain) IBOutlet UIButton *btnCancel, *btnPauseResume;
@property (nonatomic, weak) id<DTGViewControllerDownloadCellActions> delegate;
@property (nonatomic, retain) IBOutlet UIProgressView *progressView;
@property (nonatomic, retain) NSIndexPath *indexPath;

@end
