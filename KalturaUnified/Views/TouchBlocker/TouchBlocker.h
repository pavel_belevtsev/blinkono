//
//  TouchBlocker.h
//  testTouchesBlocker
//
//  Created by Yedidya Reiss on 4/30/13.
//  Copyright (c) 2013 Quickode Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TouchBlockerDelegate <NSObject>


-(void) outerTouchWasReceived;

@end

@interface TouchBlocker : UIView


/** should block the screen except this view */
@property (assign,nonatomic) IBOutlet UIView* viewToUnblock;


/** the clear context will be set to false untill the stop blocking will called */
@property (assign,nonatomic) BOOL oldViewClearContextSettings;

/** the object to call when 'out' touch received */
@property (assign,nonatomic) id<TouchBlockerDelegate> delegate;

/** if true, an outer touch will received the objects below the blocker */
@property (assign,nonatomic) BOOL allowOuterTouch;

/** count the touch events - should be 3 at any touch */
@property (assign,nonatomic) NSInteger eventCounter;



/**
 * calling this function will notify the delegate for any touch outside the given view,
 * and will block that touch.
 * after a touch (outside the view) it will reset.
 *
 *@arg delegate - the object to call when an 'outside' touch received
 *@arg view - touching in that view won't call the delegate function, and won't reset the blocker
 */
+(void) blockExceptView:(UIView*) view withDelegate:(id<TouchBlockerDelegate>)delegate;


/**
 * calling this function will notify the delegate for any touch outside the given view,
 * but will allow that touch.
 * after a touch (outside the view) it will reset.
 *
 *@arg delegate - the object to call when an 'outside' touch received
 *@arg view - touching in that view won't call the delegate function, and won't reset the blocker
 */
+(void) notifyTouchesExeptView:(UIView*) view withDelegate:(id<TouchBlockerDelegate>)delegate;


/**
 * calling this function will reset the blocker.
 */
+(void) stopBlock;

@end
