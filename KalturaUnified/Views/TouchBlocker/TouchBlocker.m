//
//  TouchBlocker.m
//  testTouchesBlocker
//
//  Created by Yedidya Reiss on 4/30/13.
//  Copyright (c) 2013 Quickode Ltd. All rights reserved.
//

#import "TouchBlocker.h"

@implementation TouchBlocker

static TouchBlocker* _blocker;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)dealloc {
}



-(BOOL) shoulBlockPoint: (CGPoint) point {
	CGRect fr = self.viewToUnblock.frame;
	NSInteger x = point.x;
	NSInteger y = point.y;
	CGSize size = fr.size;
	return (x < 0 || x > size.width || y < 0 || y > size.height);
}



-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    @try {
        CGPoint p = [self.viewToUnblock convertPoint:point fromView:self];
        //should block
        if ([self shoulBlockPoint:p]) {
            //should be 2 events on one touch
            if ((++self.eventCounter) == 2) {
                [self removeFromSuperview];
                if(self.delegate) [self.delegate outerTouchWasReceived];
                [self resetView];
            }
            return !(self.allowOuterTouch);
        }
        return NO;
    }
    @catch (NSException * e) {
       	return NO;
    }
}


-(void) resetView {
	if (self.superview == nil) return;
	
	if (self.viewToUnblock) {
		self.viewToUnblock.clearsContextBeforeDrawing = self.oldViewClearContextSettings;
	}
	
	self.delegate = nil;
	[self removeFromSuperview];
}


+(void) setupBlocker {
	if (_blocker == nil) {
		TouchBlocker* blocker = [[TouchBlocker alloc] initWithFrame: CGRectMake(-1000, -1000, 3000, 3000)];
		_blocker = blocker;
	}
	
	[_blocker resetView];
	_blocker.eventCounter = 0;
}



+(void) blockTapEndScrollExceptView:(UIView*) view withDelegate:(id<TouchBlockerDelegate>)delegate
{
    [TouchBlocker setupBlocker];
    
	_blocker.oldViewClearContextSettings = view.clearsContextBeforeDrawing;
	view.clearsContextBeforeDrawing = NO;
	
	[[UIApplication sharedApplication].keyWindow addSubview:_blocker];
	_blocker.viewToUnblock = view;
	_blocker.delegate = delegate;
	_blocker.allowOuterTouch = NO;

}

+(void) blockExceptView:(UIView*) view withDelegate:(id<TouchBlockerDelegate>)delegate {
	[TouchBlocker setupBlocker];

	_blocker.oldViewClearContextSettings = view.clearsContextBeforeDrawing;
	view.clearsContextBeforeDrawing = NO;
	
	[[UIApplication sharedApplication].keyWindow addSubview:_blocker];
	_blocker.viewToUnblock = view;
	_blocker.delegate = delegate;
	_blocker.allowOuterTouch = YES;
}


+(void)notifyTouchesExeptView:(UIView *)view withDelegate:(id<TouchBlockerDelegate>)delegate {
	[TouchBlocker setupBlocker];
	
	_blocker.oldViewClearContextSettings = view.clearsContextBeforeDrawing;
	view.clearsContextBeforeDrawing = NO;
	
	[[UIApplication sharedApplication].keyWindow addSubview:_blocker];
	
	_blocker.viewToUnblock = view;
	_blocker.delegate = delegate;
	_blocker.allowOuterTouch = YES;

}


+(void) stopBlock {
	if (_blocker) {
		[_blocker resetView];
	}
}



@end
