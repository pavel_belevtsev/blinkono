//
//  CustomViewFactory.m
//  KalturaUnified
//
//  Created by Israel Berezin on 1/7/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CustomViewFactory.h"

@implementation CustomViewFactory

static CustomViewFactory *_sharedInstance = nil;

+ (instancetype) defaultFactory
{
    
    @synchronized(self)
    {
        if (_sharedInstance == nil)
        {
            _sharedInstance = [[CustomViewFactory alloc] init];
        }
    }
    
    return _sharedInstance;
}

-(void) becomeDefaultFactory
{
    @synchronized (self)
    {
        if (_sharedInstance != self)
        {
            _sharedInstance = self;
        }
    }
}

-(PlayerVODDetailsView *) playerVODDetailsView
{
    return  [[PlayerVODDetailsView alloc] initWithNib];
}

-(ScheduledToolTipView *)scheduledToolTipView
{
    return [[ScheduledToolTipView alloc] init];
}

-(QuotaBarViewToolTip *) quotaBarViewToolTipWithQuotaData:(TVNPVRQuota *)quotaData
{
    return [[QuotaBarViewToolTip alloc] initWithQuotaData:quotaData];
}

-(EpgRecoredPopUp *)epgRecoredPopUp
{
    return  [[EpgRecoredPopUp alloc] init];
}

-(CancelSeriesRecordingPopUp *)cancelSeriesRecordingPopUpWithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView *) linkageView
{
    return  [[CancelSeriesRecordingPopUp alloc] initWithNibWithRecoredHelper:recoredHelper andLinkageView:linkageView];
}

-(EPGRcoredStatusView*)epgRcoredStatusView
{
    return [[EPGRcoredStatusView alloc] initWithNib];
}

-(MiniEpgRecoredPopUp *)miniEpgRecoredPopUp
{
    return [[MiniEpgRecoredPopUp alloc]init];
}
@end
