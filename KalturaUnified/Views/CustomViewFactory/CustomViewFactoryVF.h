//
//  CustomViewFactoryVF.h
//  KalturaUnified
//
//  Created by Israel Berezin on 1/7/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CustomViewFactory.h"

@interface CustomViewFactoryVF : CustomViewFactory

@end
