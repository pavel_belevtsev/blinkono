//
//  CustomViewFactoryVF.m
//  KalturaUnified
//
//  Created by Israel Berezin on 1/7/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "CustomViewFactoryVF.h"
#import "PlayerVODDetailsViewVF.h"
#import "ScheduledToolTipViewVF.h"
#import "QuotaBarViewToolTipVF.h"
#import "EpgRecoredPopUpVF.h"
#import "CancelSeriesRecordingPopUpVF.h"
#import "MiniEpgRecoredPopUpVF.h"

@implementation CustomViewFactoryVF
/* "PSC : Vodafone Grup :  : JIRA ticket. */


-(PlayerVODDetailsView *) playerVODDetailsView
{
    if (isPhone)
    {
        return  [[PlayerVODDetailsViewVF alloc] initWithNib];
    }
    else
    {
        return [super playerVODDetailsView];
    }
}

-(ScheduledToolTipView *)scheduledToolTipView
{
    return [[ScheduledToolTipViewVF alloc] init];
}

-(QuotaBarViewToolTip *) quotaBarViewToolTipWithQuotaData:(TVNPVRQuota *)quotaData
{
    return [[QuotaBarViewToolTipVF alloc] initWithQuotaData:quotaData];
}

-(EpgRecoredPopUp *)epgRecoredPopUp
{
    return  [[EpgRecoredPopUpVF alloc] init];
}

-(CancelSeriesRecordingPopUp *)cancelSeriesRecordingPopUpWithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView *) linkageView
{
    return  [[CancelSeriesRecordingPopUp alloc] initWithNibWithRecoredHelper:recoredHelper andLinkageView:linkageView];
}

-(EPGRcoredStatusView*)epgRcoredStatusView
{
    return [[EPGRcoredStatusView alloc] initWithNib];
}

-(MiniEpgRecoredPopUp *)miniEpgRecoredPopUp
{
    return [[MiniEpgRecoredPopUpVF alloc]init];
}
@end
