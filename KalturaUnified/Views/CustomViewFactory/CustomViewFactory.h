//
//  CustomViewFactory.h
//  KalturaUnified
//
//  Created by Israel Berezin on 1/7/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerVODDetailsView.h"
#import "ScheduledToolTipView.h"
#import "QuotaBarViewToolTip.h"
#import "EpgRecoredPopUp.h"
#import "CancelSeriesRecordingPopUp.h"
#import "EPGRcoredStatusView.h"
#import "MiniEpgRecoredPopUp.h"


@interface CustomViewFactory : NSObject

+ (instancetype) defaultFactory;
-(void) becomeDefaultFactory;

-(PlayerVODDetailsView *) playerVODDetailsView;
-(ScheduledToolTipView *)scheduledToolTipView;
-(QuotaBarViewToolTip *) quotaBarViewToolTipWithQuotaData:(TVNPVRQuota *)quotaData;
-(EpgRecoredPopUp *)epgRecoredPopUp;
-(CancelSeriesRecordingPopUp *)cancelSeriesRecordingPopUpWithRecoredHelper:(RecoredHelper*)recoredHelper andLinkageView:(UIView *) linkageView;
-(EPGRcoredStatusView*)epgRcoredStatusView;
-(MiniEpgRecoredPopUp *)miniEpgRecoredPopUp;


@end
