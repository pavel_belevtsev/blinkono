//
//  APSCollectionControl.h
//  Spas
//
//  Created by Israel Berezin on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSComplexButton : UIButton

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *subViewsOutlet;
@property (assign, nonatomic) CGFloat hightLightedAlpha;
@property (assign, nonatomic) CGFloat normalAlpha;


@end
