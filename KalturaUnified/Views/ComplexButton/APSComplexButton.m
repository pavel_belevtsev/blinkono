//
//  APSCollectionControl.m
//  Spas
//
//  Created by Israel Berezin on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSComplexButton.h"

@implementation APSComplexButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
    self.normalAlpha = self.alpha;
    self.hightLightedAlpha = 0.5;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setHighlighted:(BOOL)highlighted
{
// Hi israel , your code caused all sub controles to be transparent when they highlithed , another issue this code caused , you assume that the background color's alpha is always 1. another thing- you do not have to solve this problem by passing all elements it is enoght to do it on the super view and he is passing it to his sons. look at the new implementation nice & simple.
    
    [super setHighlighted:highlighted];
    
    if (highlighted)
    {
        self.alpha = self.hightLightedAlpha;

    }
    else
    {
        self.alpha = self.normalAlpha;
    }
    

    
//    NSInteger alpha = 1.0;
//    if (highlighted)
//    {
//        alpha = 0.5;
//    }
//    
//   // self.titleLabel.alpha = alpha;
//   // self.imageView.alpha = alpha;
//    const CGFloat* components = CGColorGetComponents(self.backgroundColor.CGColor);
//    NSLog(@"Red: %f", components[0]);
//    NSLog(@"Green: %f", components[1]);
//    NSLog(@"Blue: %f", components[2]);
//    NSLog(@"Alpha: %f", CGColorGetAlpha(self.backgroundColor.CGColor));
//    CGFloat red = components[0], green = components[1], blue = components[2];
//    
//    self.backgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
//
//   
//    for (UIView * view in self.subViewsOutlet)
//    {
//        view.alpha = alpha;
//    }
}




@end
