//
//  APSCollectionControl.m
//  Spas
//
//  Created by Israel Berezin on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSComplexButton.h"

@implementation APSComplexButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.normalAlpha = self.alpha;
    self.hightLightedAlpha = 0.5;
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    if (self.hightLightedAlpha == 0)
    {
        self.hightLightedAlpha=0.5;
    }
    if (self.normalAlpha == 0)
    {
        self.normalAlpha=1.0;
    }
    
    if (highlighted)
    {
        self.alpha = self.hightLightedAlpha;
    }
    else
    {
        self.alpha = self.normalAlpha;
    }
}




@end
