//
//  APSButtonLink.m
//  Spas
//
//  Created by Israel Berezin on 8/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSButtonLink.h"

#define SpaceUnderText 5
#define LineColor [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1].CGColor
#define TextColor [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1]
#define LineTouchColor [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:0.5].CGColor
#define TextTouchColor [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:0.5]

@implementation APSButtonLink


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    if (self.buttonType!= UIButtonTypeCustom) {
        // buttonType is a readonly property - should be set to UIButtonTypeCustom in the Xib/Storyboard
        NSLog(@"Warning: APSButtonLink: if the buttonType is not set for UIButtonTypeCustom the line will not be calculated correctly");
    }
    
//    if (is_iPad())
//    {
//        [self.titleLabel setFont:[APSTypography  fontMediumWithSize:16]];
//    }
//    else
//    {
//        [self.titleLabel setFont:[APSTypography  fontMediumWithSize:12]];
//    }
    
    [self setTitleColor:TextColor forState:UIControlStateNormal];//UIControlStateHighlighted
    [self setTitleColor:TextColor forState:UIControlStateHighlighted];
    [self setBackgroundColor:[UIColor clearColor]];
}

-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    [self drowLineWithColor:LineColor];
    
}

-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    BOOL toReturen = [super beginTrackingWithTouch:touch withEvent:event];
    self.alpha=0.5;
    [self setTitleColor:TextTouchColor forState:UIControlStateNormal];
    return toReturen;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super endTrackingWithTouch:touch withEvent:event];
    [self setTitleColor:TextColor forState:UIControlStateNormal];
    self.alpha=1.0;
}

//-(void)setTitle:(NSString *)title forState:(UIControlState)state
//{
//    [super setTitle:title forState:state];
//    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:title];
//    
//    [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
//    
//     [commentString setAttributes:@{NSForegroundColorAttributeName:TextColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[commentString length])];
//    [self setAttributedTitle:commentString forState:UIControlStateNormal];
//}

-(void)drowLineWithColor:(CGColorRef)color
{
    CGRect textRect = self.titleLabel.frame;
    
    // need to put the line at top of descenders (negative value)
    CGFloat descender = self.titleLabel.font.descender + SpaceUnderText;
    
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    // set to same colour as text
    CGContextSetStrokeColorWithColor(contextRef, color);
    
    CGContextMoveToPoint(contextRef, textRect.origin.x, textRect.origin.y + textRect.size.height + descender);
    
    CGContextAddLineToPoint(contextRef, textRect.origin.x + textRect.size.width, textRect.origin.y + textRect.size.height + descender);
    
    CGContextClosePath(contextRef);
    
    CGContextDrawPath(contextRef, kCGPathStroke);
}
@end
