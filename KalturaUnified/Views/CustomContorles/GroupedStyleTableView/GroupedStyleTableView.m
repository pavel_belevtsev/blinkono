//
//  GroupedStyleTableView.m
//  Vodafone
//
//  Created by Aviv Alluf on 2/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "GroupedStyleTableView.h"

@implementation GroupedStyleTableView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.style == UITableViewStyleGrouped)
    {
        // For each section, we round the first and last cell
        NSInteger numberOfSections = [self.dataSource numberOfSectionsInTableView:self];
        for (int i = 0 ; i < numberOfSections ; i++)
        {
            // Get first and last cell
            NSInteger numberOfRows = [self.dataSource tableView:self numberOfRowsInSection:i];
            UITableViewCell *topCell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
            UITableViewCell *bottomCell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows-1 inSection:i]];
            
            // If there is a single row, we round the cell by each corner
            if (topCell == bottomCell)
            {
                CAShapeLayer *shape = [[CAShapeLayer alloc] init];
                shape.path = [UIBezierPath bezierPathWithRoundedRect:topCell.bounds
                                                        cornerRadius:self.cornerRadius].CGPath;
                topCell.layer.mask = shape;
                topCell.layer.masksToBounds = YES;
            }
            else
            {
                // Create the top mask we will apply on the cell to round it.
                CAShapeLayer *topShape = [[CAShapeLayer alloc] init];
                topShape.path = [UIBezierPath bezierPathWithRoundedRect:topCell.bounds
                                                      byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight
                                                            cornerRadii:CGSizeMake(self.cornerRadius, self.cornerRadius)].CGPath;
                topCell.layer.mask = topShape;
                topCell.layer.masksToBounds = YES;
                
                // Create the bottom mask we will apply on the cell to round it.
                CAShapeLayer *bottomShape = [[CAShapeLayer alloc] init];
                bottomShape.path = [UIBezierPath bezierPathWithRoundedRect:bottomCell.bounds
                                                         byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight
                                                               cornerRadii:CGSizeMake(self.cornerRadius, self.cornerRadius)].CGPath;
                bottomCell.layer.mask = bottomShape;
                bottomCell.layer.masksToBounds = YES;
            }
            
            // For performance issue, and to avoid offscreen rendering, we rasterize our layers.
            topCell.layer.shouldRasterize = YES;
            topCell.layer.rasterizationScale = [[UIScreen mainScreen] scale];
            bottomCell.layer.shouldRasterize = YES;
            bottomCell.layer.rasterizationScale = [[UIScreen mainScreen] scale];
            
            // Because the TableView reuses some cells, we don't want our middle cells to be rounded,
            // so we force their mask to be nil (not a random rounded one).
            for (int j = 1 ; j < numberOfRows-1 ; j++)
            {
                UITableViewCell *cell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:j inSection:i]];
                cell.layer.mask = nil;
                cell.layer.shouldRasterize = NO;
            }
        }
    }
}


@end
