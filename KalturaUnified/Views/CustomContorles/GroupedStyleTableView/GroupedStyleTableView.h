//
//  GroupedStyleTableView.h
//  Vodafone
//
//  Created by Aviv Alluf on 2/4/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupedStyleTableView : UITableView

@property float cornerRadius; // sets the corner radius for each section in the table

@end
