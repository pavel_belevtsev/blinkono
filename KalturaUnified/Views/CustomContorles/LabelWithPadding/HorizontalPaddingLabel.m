//
//  LabelWithPadding.m
//  KalturaUnified
//
//  Created by Rivka Schwartz on 6/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "HorizontalPaddingLabel.h"

@implementation HorizontalPaddingLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


/* So that it will also work with auto layout */
-(CGSize)intrinsicContentSize
{
    CGSize size = [super intrinsicContentSize];

    if(self.text.length!=0)
    {
        size.width += self.additionalPaddingSize.width?self.additionalPaddingSize.width:10;
        size.height += self.additionalPaddingSize.height?self.additionalPaddingSize.height:5;
    }

    return size;
}


@end
