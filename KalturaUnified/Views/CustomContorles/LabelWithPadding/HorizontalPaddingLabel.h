//
//  LabelWithPadding.h
//  KalturaUnified
//
//  Created by Rivka Schwartz on 6/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HorizontalPaddingLabel : UILabel

@property (assign, nonatomic) CGSize additionalPaddingSize;

@end
