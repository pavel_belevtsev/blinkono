//
//  APSBackgroundColorButton.m
//  Spas
//
//  Created by Israel Berezin on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBackgroundColorButton.h"

@implementation APSBackgroundColorButton


-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if(!self.selected){
        self.backgroundColor = highlighted ? self.highlightedColor: self.defaultColor;
    }
}

-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    self.backgroundColor = selected ? self.selectedColor: self.defaultColor;
}




@end
