//
//  APSBackgroundColorButton.h
//  Spas
//
//  Created by Israel Berezin on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

// APSBackgroundColorButton
//  The background color of the button will change according to the state:
//  default/ highlighted/ selected 

@interface APSBackgroundColorButton : UIButton

@property (strong, nonatomic) UIColor* defaultColor;
@property (strong, nonatomic) UIColor* highlightedColor;
@property (strong, nonatomic) UIColor* selectedColor;

@end
