//
//  KAChromecastMediaControlChannel.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 7/15/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import UIKit

class KAChromecastMediaControlChannel: GCKMediaControlChannel {
   
    override init(namespace protocolNamespace: String!) {
        super.init()
    }
}
