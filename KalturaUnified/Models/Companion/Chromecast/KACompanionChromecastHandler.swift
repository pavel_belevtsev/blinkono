//
//  KACompanionChromecastHandler.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 7/15/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import UIKit

class KACompanionChromecastHandler: TVAbstractCompanionHandler, GCKDeviceScannerListener, GCKDeviceManagerDelegate, GCKMediaControlChannelDelegate  {
   
    var devices : [KACompanionChromecastDevice] = []
    var scanner : GCKDeviceScanner?
    var manager : GCKDeviceManager?
    var control : KAChromecastMediaControlChannel?
    var volume : Float = 1
    var appId : String = kGCKMediaDefaultReceiverApplicationID //default google app id

    
    init(applicationId: String?) {
        super.init()
        if let applicationId = applicationId {
            println("applicationId:  \(applicationId)")
            self.appId = applicationId
            println("self.appId:  \(self.appId)")
        }
        self.registerToUserActionNotification();
    }
    // MARK: - overriding base functions
    override func getType() -> Int32 {
        return kDeviceTypeChromecast
    }
    
    override func stopScanning() {
        self.scanner?.stopScan()
    }
    
    override func scan() {
        self.scanner = GCKDeviceScanner.new()
        self.scanner?.addListener(self)
        
        self.scanner?.startScan()
    }
    
    override func sendVodMedia(media: TVMediaItem!, toDevice device: TVAbstractCompanionDevice!, withFile file: TVFile!, currentProgress progress: Int32, withCompletionBlock result: ((CompanionHandlerResult) -> Void)!) {
        
        if (self.appId == kGCKMediaDefaultReceiverApplicationID) {
            var metadata : GCKMediaMetadata = GCKMediaMetadata.new()
            metadata.setString(media.name, forKey: kGCKMetadataKeyTitle)
            metadata.setString(media.mediaDescription, forKey: kGCKMetadataKeySubtitle)
            metadata.addImage(GCKImage(URL: media.pictureURLForSize(CGSize(width: 480, height: 360)), width: 480, height: 360))
            
            var mediaInformation : GCKMediaInformation = GCKMediaInformation(contentID: file.fileURL.absoluteString, streamType: GCKMediaStreamType.Buffered, contentType: "application/x-mpegurl", metadata: metadata, streamDuration: file.duration, customData: nil)
            
            var interval : NSTimeInterval = Double(progress)
            self.control?.loadMedia(mediaInformation, autoplay: true, playPosition: interval)
            
            return result! (CompanionHandlerResultOk)

        }
        else
        {
            var metadata : GCKMediaMetadata = GCKMediaMetadata.new()
            metadata.setString(media.name, forKey: kGCKMetadataKeyTitle)
            metadata.setString(media.mediaDescription, forKey: kGCKMetadataKeySubtitle)

            var imageUrl : NSURL = NSURL(string: "")!
            if (media.pictures.count > 0)
            {
                imageUrl = media.pictureURLForSize(CGSize(width: 480, height: 360));
            }
            metadata.addImage(GCKImage(URL: imageUrl, width: 480, height: 360))
            
            var mediaInformation : GCKMediaInformation = GCKMediaInformation(contentID: file.fileURL.absoluteString, streamType: GCKMediaStreamType.Buffered, contentType: "application/x-mpegurl", metadata: metadata, streamDuration: file.duration, customData: nil)
            
            var interval : NSTimeInterval = Double(progress)

            var payload : [String: AnyObject] = [ "title" : media.name ];
            
            var dic1 = media.advertisingParameters;
            let s: AnyObject? = dic1["selectLang"];
            if (s != nil)
            {
                payload["selectLang"] = dic1["selectLang"];
            }

            if (media.pictures.count > 0)
            {
                payload["thumb"] = Utils.getBestPictureURLForMediaItem(media, andSize: CGSizeMake(103, 154)).absoluteString //media.pictureURLForSize(CGSizeMake(103, 154)).absoluteString;
            }
                else
            {
                    payload["thumb"] = "";
            }
            
            payload["laurl"] = TVConfigurationManager.sharedTVConfigurationManager().LicenseServerUrl.absoluteString;
            
            payload["cdata"] = media.price ;// file.fileURL.absoluteString;
            
            payload["mediaid"] = media.mediaID;
            
            payload["fileid"] = file.fileID;
            
            payload["fileFormat"] = file.format;
            
            payload["groupname"] = TVConfigurationManager.sharedTVConfigurationManager().defaultInitObject.APIUsername;
            
            payload["groupid"] = TVConfigurationManager.sharedTVConfigurationManager().groupID;
            
            payload["siteguid"] = TVSessionManager.sharedTVSessionManager().sharedInitObject.siteGUID;
            
            payload["domainid"] = TVSessionManager.sharedTVSessionManager().sharedInitObject.domainID;
            
            payload["udid"] = TVSessionManager.sharedTVSessionManager().sharedInitObject.udid;
            
            var dic : [String: AnyObject] = [ "payload" : payload];
            
            if (self.control != nil)
            {
                var requestId : NSInteger = self.control!.loadMedia(mediaInformation, autoplay: true, playPosition: interval, customData: dic);
                println("requestId = \(requestId)")
                
             
                if (requestId == kGCKInvalidRequestID)
                {
                    println("WARN loadMedia: the Media could not be sent")
                    return result! (CompanionHandlerResultFailure)
                }
                else
                {
                    println("WARN loadMedia: the Media has be sent")
                    return result! (CompanionHandlerResultOk)

                }
            }
            else
            {
                println("WARN loadMedia: the Media could not be sent")
                return result! (CompanionHandlerResultFailure)
            }
        }
    }
    
    override func sendLiveMedia(media: TVMediaItem!, toDevice device: TVAbstractCompanionDevice!, withFile file: TVFile!, andProgram program: TVEPGProgram?, forPlaybackMode playbackType: String!, progress: Int32, withCompletionBlock result: ((CompanionHandlerResult) -> Void)!) {
        
        if (self.appId == kGCKMediaDefaultReceiverApplicationID) {

            var metadata : GCKMediaMetadata = GCKMediaMetadata.new()
            metadata.setString(media.name, forKey: kGCKMetadataKeyTitle)
            metadata.setString(media.mediaDescription, forKey: kGCKMetadataKeySubtitle)
            metadata.addImage(GCKImage(URL: media.pictureURLForSize(CGSize(width: 480, height: 360)), width: 480, height: 360))
            
            var mediaInformation : GCKMediaInformation = GCKMediaInformation(contentID: file.fileURL.absoluteString, streamType: GCKMediaStreamType.Live, contentType: "application/x-mpegurl", metadata: metadata, streamDuration: file.duration, customData: nil)
            
            self.control?.loadMedia(mediaInformation, autoplay: true, playPosition: 0)
            
            return result! (CompanionHandlerResultOk)
            
        }
        else {
            
            var metadata : GCKMediaMetadata = GCKMediaMetadata.new()
            metadata.setString(media.name, forKey: kGCKMetadataKeyTitle)
            metadata.setString(media.mediaDescription, forKey: kGCKMetadataKeySubtitle)
            metadata.addImage(GCKImage(URL: media.pictureURLForSize(CGSize(width: 480, height: 360)), width: 480, height: 360))
            
            println("metadata = \(metadata)");
            var mediaInformation : GCKMediaInformation = GCKMediaInformation(contentID: file.fileURL.absoluteString, streamType: GCKMediaStreamType.Live, contentType: "application/x-mpegurl", metadata: metadata, streamDuration: file.duration, customData: nil)
            
            var interval : NSTimeInterval = Double(progress)
            
            var payload : [String: AnyObject] = [ "title" : media.name ];
           // NSURL *pictureURL = [self.item pictureURLForSize:CGSizeMake(568, 320)]; (142,80)
            var imageUrl : NSURL = NSURL(string: "")!
            
            if  (program != nil){
                imageUrl = program!.pictureURLForSize(CGSizeMake(568, 320));
            }
           
            var request: NSMutableURLRequest = NSMutableURLRequest(URL: imageUrl)
            request.HTTPMethod = "HEAD"
            var session = NSURLSession.sharedSession()
            var err: NSError?
            let task = session.dataTaskWithRequest(request, completionHandler: { [weak self] data, response, error -> Void in
                if let strongSelf = self {

                    if (error != nil) {
                        imageUrl = Utils.getBestPictureURLForMediaItem(media, andSize: CGSizeMake(142, 80));
                    }
                    
                    var dic1 = media.advertisingParameters;
                    let s: AnyObject? = dic1["selectLang"];
                    if (s != nil)
                    {
                        payload["selectLang"] = dic1["selectLang"];
                    }
                    
                    payload["thumb"] = imageUrl.absoluteString; //media.pictureURLForSize(CGSizeMake(159, 159)).absoluteString;
                   
                    payload["fileFormat"] = file.format;

                    payload["laurl"] = TVConfigurationManager.sharedTVConfigurationManager().LicenseServerUrl.absoluteString;
                    
                    payload["cdata"] = media.price;
                    
                    payload["fileid"] = file.fileID;
                    
                    payload["mediaid"] = media.mediaID;
                    
                    payload["groupname"] = TVConfigurationManager.sharedTVConfigurationManager().defaultInitObject.APIUsername;
                    
                    payload["groupid"] = TVConfigurationManager.sharedTVConfigurationManager().groupID;
                    
                    payload["groupPass"] = TVSessionManager.sharedTVSessionManager().sharedInitObject.APIPassword;

                    payload["siteguid"] = TVSessionManager.sharedTVSessionManager().sharedInitObject.siteGUID;
                    
                    payload["domainid"] = TVSessionManager.sharedTVSessionManager().sharedInitObject.domainID;
                    
                    payload["udid"] = TVSessionManager.sharedTVSessionManager().sharedInitObject.udid;
                    
                    payload["playbackmode"] = playbackType;
                    
                    if (program != nil)
                    {
                        if program!.EPGIdentifier != nil
                        {
                            payload["programid"] = program!.EPGIdentifier;
                        }
                        if program!.EPGChannelID != nil
                        {
                            payload["channelid"] = program!.EPGChannelID;
                        }
                      
                        let pStart = program!.startDateTime.timeIntervalSince1970;
                        let start : NSNumber = pStart;
                        let pEnd = program!.endDateTime.timeIntervalSince1970;
                        let end : NSNumber = pEnd;
                        payload["programStart"] = start;
                        payload["programEnd"] = end;
                    }
                        println("payload = \(payload)");
                    var dic : [String: AnyObject] = ["payload" : payload];
                    
                    if (strongSelf.control != nil)
                    {
                        var requestId : NSInteger = strongSelf.control!.loadMedia(mediaInformation, autoplay: true, playPosition: interval, customData: dic);
                        println("requestId = \(requestId)")
                            
                        if (requestId == kGCKInvalidRequestID)
                        {
                            println("WARN loadMedia: the Media could not be sent")
                            return result! (CompanionHandlerResultFailure)
                        }
                        else
                        {
                            println("WARN loadMedia: the Media has be sent")
                            return result! (CompanionHandlerResultOk)
                        }
                    }
                    else
                    {
                        println("WARN loadMedia: the Media could not be sent")
                        return result! (CompanionHandlerResultFailure)
                    }

                }
            }).resume();

        }
    }
    
    override func connect(device: TVAbstractCompanionDevice!) {
        
        var castingDevice : KACompanionChromecastDevice = device as! KACompanionChromecastDevice
        let cfBundleIdentifier : String = NSBundle.mainBundle().infoDictionary?["CFBundleIdentifier"] as! String
        
        self.manager = GCKDeviceManager(device: castingDevice.device, clientPackageName: cfBundleIdentifier)
    
        self.manager!.delegate = self
        self.manager!.connect()

    }
    
    
    override func disconnect() {
        self.manager!.stopApplication();
        self.manager?.disconnectWithLeave(true)
        TVMixedCompanionManager.sharedInstance().currentCompanionPlayerState = CompanionPlayerState.Unknown;
        
    }
    
    override func sendMessage(message: String?, withExtras extras: [NSObject : AnyObject]?, toDevice device: TVAbstractCompanionDevice!, withCompletion completion: ((Bool, [NSObject : AnyObject]!) -> Void)?) {
        
        let managerOK : Bool = (self.manager != nil && self.manager?.applicationConnectionState == GCKConnectionState.Connected)
        let controlOK : Bool = (self.control != nil) && self.control!.isConnected
        
        if (message != nil) && controlOK && managerOK {
            //CompanionMessageGetVolume
            if message! == CompanionMessageGetVolume {
                completion?(true, ["volume": NSNumber(float: self.volume)])
            }
            //CompanionMessageSetVolume
            else if message! == CompanionMessageSetVolume {
                if let volume: NSNumber = extras?["volume"] as? NSNumber {
                    self.control?.setStreamVolume(volume.floatValue)
                    self.volume = volume.floatValue
                    completion?(true, nil)
                }
                else {
                    completion?(false, nil)
                }
            }
                
            else if message! == "CompanionMessageStop" {
                
                self.control?.stop()
                completion?(true, nil)
                
            }
                
            else {
                let jsonDictionary: [String:AnyObject?] = ["command": message, "extra": extras]
                let messageData = self.JSONStringify(jsonDictionary as! AnyObject, prettyPrinted: false)
                if messageData != "" {
                    self.control?.sendTextMessage(messageData)
                    completion?(true, nil)
                }
                else {
                    completion?(false, nil)
                }
            }
        }
        else {
            completion?(false, nil)
        }
    }
    
    // MARK: - GCKDeviceScannerListener
    func deviceDidComeOnline(device: GCKDevice!) {
        let ccDevice : KACompanionChromecastDevice = KACompanionChromecastDevice(device: device)
        var hasDevice = false
//        println("We got new Device for GG CC SDK: \(ccDevice.name) with UID: \(ccDevice.uid)");
//        println("this is the list if devices we have now");
        for dv in self.devices {
//            println(" device name : \(dv.name) with UID: \(dv.uid)");
            if dv.uid == ccDevice.uid {
                //this fix is needed in cases the device (CC) name has changed
                if dv.name != ccDevice.name{
                    dv.name = ccDevice.name;
                }
                hasDevice = true
                break
            }
        }
        
        if !hasDevice {
            self.devices.append(ccDevice)
            TVCompanionManager.sharedInstance().saveDevices(self.devices)
            self.delegate?.onDiscovery(self.devices)

        }
    }
    
    func deviceDidGoOffline(device: GCKDevice!) {
        let ccDevice : KACompanionChromecastDevice = KACompanionChromecastDevice(device: device)
        
        var arr : [KACompanionChromecastDevice]? = self.devices.filter {device in device.uid == ccDevice.uid }
        if let arr = arr {
//            println("The Device \(ccDevice.name) with UID: \(ccDevice.uid) just removed with GG CC SDK");
            self.devices.remove(arr.first!)
            TVCompanionManager.sharedInstance().saveDevices(self.devices)
            self.delegate?.onDiscovery(self.devices)
        }
    }
    
    // MARK: - GCKDeviceManagerDelegate
    func deviceManagerDidConnect(deviceManager: GCKDeviceManager!) {
        println("deviceManager: deviceManagerDidConnect")

        self.manager?.launchApplication(self.appId)
    }
    
    func deviceManager(deviceManager: GCKDeviceManager!, didConnectToCastApplication applicationMetadata: GCKApplicationMetadata!, sessionID: String!, launchedApplication: Bool) {
        self.control = KAChromecastMediaControlChannel(namespace: "urn:x-cast:com.kaltura")
        self.control?.delegate = self
        self.manager?.addChannel(self.control!)
        self.control!.requestStatus()
    }
    
    func deviceManager(deviceManager: GCKDeviceManager!, didDisconnectFromApplicationWithError error: NSError!) {
        
        if error != nil {
            switch (error.code) {
            case GCKErrorCode.ApplicationNotRunning.rawValue:
                NSNotificationCenter.defaultCenter().postNotificationName("chromeCastForceDisconnectNotifcation", object: nil, userInfo: nil);
                break;
            case GCKErrorCode.Replaced.rawValue:
                //stop casting media
                println("deviceManager: didDisconnectFromApplicationWithError GCKErrorCode.Replaced = \(error)")
                break;
            default:
                break
            }
        }

        self.manager!.stopApplication();
        self.manager?.disconnectWithLeave(true)
        println("deviceManager: didDisconnectFromApplicationWithError = \(error)")
    }
    
    func deviceManager(deviceManager: GCKDeviceManager!, didDisconnectWithError error: NSError!) {
        println("deviceManager: didDisconnectWithError = \(error)")
    }
    func deviceManager(deviceManager: GCKDeviceManager!, didFailToConnectWithError error: NSError!) {
        println("deviceManager: didFailToConnectWithError = \(error)")
         NSNotificationCenter.defaultCenter().postNotificationName("chromeCastForceDisconnectNotifcation", object: nil, userInfo: ["error":"didFailWithError =  \(error)"]);
    }
    func deviceManager(deviceManager: GCKDeviceManager!, didFailToConnectToApplicationWithError error: NSError!) {
        println("deviceManager: didFailToConnectToApplicationWithError = \(error)")
        NSNotificationCenter.defaultCenter().postNotificationName("chromeCastForceDisconnectNotifcation", object: nil, userInfo: ["error":"didFailWithError =  \(error)"]);
    }
    
    func deviceManager(deviceManager: GCKDeviceManager!,   activeInputStatus: GCKActiveInputStatus) {
        println("deviceManager: didReceiveActiveInputStatus = \(activeInputStatus.rawValue)")
    }
    
    func deviceManager(deviceManager: GCKDeviceManager!, didReceiveApplicationMetadata metadata: GCKApplicationMetadata!) {
        println("deviceManager: didReceiveApplicationMetadata: \(metadata)") // CCC
    }
    
    func deviceManager(deviceManager: GCKDeviceManager!, didReceiveApplicationStatusText applicationStatusText: String!) {
        println("deviceManager: didReceiveApplicationStatusText: \(applicationStatusText)") // DDD
    }
    
    func deviceManager(deviceManager: GCKDeviceManager!, didReceiveStandbyStatus standbyStatus: GCKStandbyStatus) {
        println("deviceManager: didReceiveStandbyStatus = \(standbyStatus.rawValue)")
    }
    
    func deviceManager(deviceManager: GCKDeviceManager!, didSuspendConnectionWithReason reason: GCKConnectionSuspendReason) {
        println("deviceManager: didSuspendConnectionWithReason = \(reason.rawValue)") // BBB
     
    }
    
    func deviceManager(deviceManager: GCKDeviceManager!, request requestID: Int, didFailWithError error: NSError!) {
        println("deviceManager: request: didFailWithError =  \(error)") // AAA brain freeze
      //  NSNotificationCenter.defaultCenter().postNotificationName("chromeCastForceDisconnectNotifcation", object: nil, userInfo: ["error":"didFailWithError =  \(error)"]);
        

        


    }
    
    func deviceManager(deviceManager: GCKDeviceManager!, volumeDidChangeToLevel volumeLevel: Float, isMuted: Bool) {
        self.volume = volumeLevel
    }
    
    // MARK: - helpers
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
        
        let options = prettyPrinted ? NSJSONWritingOptions.PrettyPrinted : NSJSONWritingOptions(rawValue: 0)
        
        if NSJSONSerialization.isValidJSONObject(value) {
            var error : NSError?
            if let data = NSJSONSerialization.dataWithJSONObject(value, options: options, error: &error) {
                if let string = NSString(data: data, encoding: NSUTF8StringEncoding) {
                    return string as String
                }
            }
        }
        return ""
    }
    
     func play() {
        NSLog("play");
        TVMixedCompanionManager.sharedInstance().currentCompanionPlayerState = CompanionPlayerState.Play;
        self.control!.play();
    }
    
     func pause() {
        NSLog("pause");
        TVMixedCompanionManager.sharedInstance().currentCompanionPlayerState = CompanionPlayerState.Pause;
        self.control!.pause();
    }
    
     func stop() {
        NSLog("stop");
        TVMixedCompanionManager.sharedInstance().currentCompanionPlayerState = CompanionPlayerState.Stop;
        self.control!.stop();
    }
    
    func registerToUserActionNotification()
    {
        self.unregisterToUserActionNotification();
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "makePlay:",
            name: "userMakePlayNotification",
            object: nil);
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "makePause:",
            name: "userMakePauseNotification",
            object: nil);
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "makeStop:",
            name: "userMakeStopNotification",
            object: nil);
    }
    
    
    func unregisterToUserActionNotification()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "userMakePlayNotification", object: nil);
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "userMakePauseNotification", object: nil);
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "userMakeStopNotification", object: nil);
    }
    
    @objc func makePlay(notification: NSNotification){
        self.play();
    }
    
    @objc func makePause(notification: NSNotification){
        self.pause();
    }
    
    @objc func makeStop(notification: NSNotification){
        self.stop();
    }

    func mediaControlChannel(mediaControlChannel: GCKMediaControlChannel!, didFailToLoadMediaWithError error: NSError!) {
        println("mediaControlChannel: didFailToLoadMediaWithError = \(error)") //
    }
    
    func mediaControlChannel(mediaControlChannel: GCKMediaControlChannel!, didCompleteLoadWithSessionID sessionID: Int) {
        println("mediaControlChannel: didCompleteLoadWithSessionID") //

    }
    
    func mediaControlChannelDidUpdateStatus(mediaControlChannel: GCKMediaControlChannel!) {
        println("mediaControlChannel: mediaControlChannelDidUpdateStatus = \(mediaControlChannel)") //
    }
    
    func mediaControlChannelDidUpdateMetadata(mediaControlChannel: GCKMediaControlChannel!) {
        println("mediaControlChannel: mediaControlChannelDidUpdateMetadata = \(mediaControlChannel)") //
    }
    
    func mediaControlChannel(mediaControlChannel: GCKMediaControlChannel!, didCancelRequestWithID requestID: Int) {
        println("mediaControlChannel: didCancelRequestWithID = \(mediaControlChannel)") //
    }
}
