//
//  KACompanionChromecastDevice.swift
//  KalturaUnified
//
//  Created by Alex Zchut on 7/15/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

import UIKit

let kDeviceTypeChromecast : Int32 = 0x3015
class KACompanionChromecastDevice: TVAbstractCompanionDevice {
    
    var device : GCKDevice?
    
    init(device: GCKDevice?) {
        super.init()
        
        if let device = device {
            self.device = device
            self.name = device.friendlyName
            self.baseAddress = device.ipAddress
            self.uid = device.deviceID
        }
    }
    
    override func getType() -> Int32 {
        return kDeviceTypeChromecast
    }
    
}
