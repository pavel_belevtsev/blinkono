//
//  TVNCompanionChromecastHandler.h
//  Tvinci
//
//  Created by Sagi Antebi on 8/25/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import <TvinciSDK/TVAbstractCompanionHandler.h>
//#import <GoogleCast/GoogleCast.h>
#import "TVCompanionChromecastDevice.h"
#import <TvinciSDK/TVMixedCompanionManager.h>

#define TYPE_CHROMECAST 0x3015

@interface TVCompanionChromecastHandler : TVAbstractCompanionHandler <GCKDeviceScannerListener, GCKDeviceFilterListener, GCKDeviceManagerDelegate, GCKMediaControlChannelDelegate>


-(id)initWithApplicationId:(NSString*)applicationId;

@end
