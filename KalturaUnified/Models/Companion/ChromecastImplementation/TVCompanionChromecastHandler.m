//
//  TVNCompanionChromecastHandler.m
//  Tvinci
//
//  Created by Sagi Antebi on 8/25/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import "TVCompanionChromecastHandler.h"
#import "TVChromecastCustomChannel.h"
#import <TvinciSDK/TVSessionManager.h>
#import <TvinciSDK/TVEPGProgram.h>
#import <TvinciSDK/TVConfigurationManager.h>


@interface TVCompanionChromecastHandler ()

@property NSString* appId;
@property GCKDeviceScanner* scanner;
@property GCKDeviceFilter* filter;
@property NSMutableArray* devices;
@property GCKDeviceManager* manager;
@property GCKMediaControlChannel* control;
@property TVChromecastCustomChannel* customcontrol;
@end

@implementation TVCompanionChromecastHandler

@synthesize appId = _appId;
@synthesize scanner = _scanner;
@synthesize devices = _devices;
@synthesize manager = _manager;
@synthesize filter = _filter;
@synthesize control = _control;
@synthesize customcontrol = _customcontrol;

float volume = 1.0;

-(id)init {
    id this = [super init];
    
    if (this) {
        _devices = [[NSMutableArray alloc] init];
    }
    return this;
}

-(id)initWithApplicationId:(NSString *)applicationId {
    id this = [self init];
    
    if (this) {
        [self setAppId:applicationId];
    }
    
    return this;
}

- (int) getType {
    return TYPE_CHROMECAST;
}

- (void) stopScanning {
    if (_scanner) {
        [_scanner stopScan];
    }
}

- (void) scan {
    _scanner = [[GCKDeviceScanner alloc] init];
    
    
    GCKFilterCriteria* filterc = [[GCKFilterCriteria alloc] init];
    filterc = [GCKFilterCriteria criteriaForAvailableApplicationWithID:_appId];
    _filter = [[GCKDeviceFilter alloc] initWithDeviceScanner:_scanner criteria:filterc];
    [_scanner addListener:self];
    [_filter addDeviceFilterListener:self];
    
    [_scanner startScan];
    //[self performSelector:@selector(doStop) withObject:nil afterDelay:10.0];
}

- (void) doStop {
    [self stopScanning];
}

- (void) sendVodMedia: (TVMediaItem*)media toDevice: (TVAbstractCompanionDevice*)device withFile: (TVFile*)file currentProgress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))result {
    
    GCKMediaMetadata* mediaData = [[GCKMediaMetadata alloc] init];
    
    GCKMediaInformation* information = [[GCKMediaInformation alloc] initWithContentID:[file.fileURL absoluteString] streamType:GCKMediaStreamTypeBuffered contentType:@"video/mp4" metadata:mediaData streamDuration:file.duration customData:nil];
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* payload = [[NSMutableDictionary alloc] init];
    
    [payload setValue:media.name forKey:@"title"];
    [payload setValue:[[media pictureURLForSize:CGSizeMake(159, 159)] absoluteString] forKey:@"thumb"];
    [payload setValue:@"http://54.72.216.77:4444/proxy.ashx?url=http://tvinci.playready.com/rightsmanager.asmx" forKey:@"laurl"];
    [payload setValue:[[file fileURL] absoluteString] forKey:@"cdata"];
    [payload setValue:media.mediaID forKey:@"mediaid"];
    NSString* gname = [TVConfigurationManager sharedTVConfigurationManager].defaultInitObject.APIUsername;
    [payload setValue:gname forKey:@"groupname"];
    
    NSRange origrange = [gname rangeOfString:@"_"];
    if (origrange.length != 0) {
        NSRange range = NSMakeRange(origrange.location + 1, gname.length - 1 - origrange.location);
        NSString* gid = [gname substringWithRange:range];
        [payload setValue:gid forKeyPath:@"groupid"];
    }
    
    [payload setValue:[TVSessionManager sharedTVSessionManager].sharedInitObject.siteGUID forKey:@"siteguid"];
    [payload setValue:[NSString stringWithFormat:@"@%ld",(long)[TVSessionManager sharedTVSessionManager].sharedInitObject.domainID] forKey:@"domainid"];
    [payload setValue:[TVSessionManager sharedTVSessionManager].sharedInitObject.udid forKey:@"udid"];
    
    
    [dic setValue:payload forKey:@"payload"];
    [_control loadMedia:information autoplay:YES playPosition:0 customData:dic];
    
    
    
}

- (void) sendLiveMedia: (TVMediaItem*)media toDevice:(TVAbstractCompanionDevice*)device withFile: (TVFile*)file andProgram: (TVEPGProgram*)program forPlaybackMode: (NSString*)playbackType progress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))result {
    
    GCKMediaMetadata* mediaData = [[GCKMediaMetadata alloc] init];
    
    GCKMediaInformation* information = [[GCKMediaInformation alloc] initWithContentID:[file.fileURL absoluteString] streamType:GCKMediaStreamTypeLive contentType:@"video/mp4" metadata:mediaData streamDuration:file.duration customData:nil];
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* payload = [[NSMutableDictionary alloc] init];
    
    [payload setValue:media.name forKey:@"title"];
    [payload setValue:[[media pictureURLForSize:CGSizeMake(159, 159)] absoluteString] forKey:@"thumb"];
    [payload setValue:@"http://54.72.216.77:4444/proxy.ashx?url=http://tvinci.playready.com/rightsmanager.asmx" forKey:@"laurl"];
    [payload setValue:[[file fileURL] absoluteString] forKey:@"cdata"];
    [payload setValue:media.mediaID forKey:@"mediaid"];
    [payload setValue:program.EPGIdentifier forKey:@"programid"];
    [payload setValue:program.EPGChannelID forKey:@"channelid"];
    NSString* gname = [TVConfigurationManager sharedTVConfigurationManager].defaultInitObject.APIUsername;
    [payload setValue:gname forKey:@"groupname"];
    
    NSRange origrange = [gname rangeOfString:@"_"];
    if (origrange.length != 0) {
        NSRange range = NSMakeRange(origrange.location + 1, gname.length - 1 - origrange.location);
        NSString* gid = [gname substringWithRange:range];
        [payload setValue:gid forKeyPath:@"groupid"];
    }
    
    [payload setValue:[TVSessionManager sharedTVSessionManager].sharedInitObject.siteGUID forKey:@"siteguid"];
    [payload setValue:[NSString stringWithFormat:@"%ld",(long)[TVSessionManager sharedTVSessionManager].sharedInitObject.domainID] forKey:@"domainid"];
    [payload setValue:playbackType forKey:@"playbackmode"];
    [payload setValue:[TVSessionManager sharedTVSessionManager].sharedInitObject.udid forKey:@"udid"];
    
    
    
    long long pStart = [program.startDateTime timeIntervalSince1970];
    NSNumber* start = [NSNumber numberWithLongLong:pStart * 1000];
    
    long long pEnd = [program.endDateTime timeIntervalSince1970];
    NSNumber* end = [NSNumber numberWithLongLong:pEnd * 1000];
    
    [payload setValue:start forKey:@"programStart"];
    [payload setValue:end forKey:@"programEnd"];
    [dic setValue:payload forKey:@"payload"];
    [_control loadMedia:information autoplay:YES playPosition:0 customData:dic];
    
    
}

- (void) connect: (TVAbstractCompanionDevice*)device {
    
    TVCompanionChromecastDevice *castDevice = (TVCompanionChromecastDevice*)device;
    GCKDevice* origDevice = [castDevice getGCKDevice];
    
    
    NSString* cfbundle2 = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    
    _manager = [[GCKDeviceManager alloc] initWithDevice:origDevice clientPackageName: cfbundle2];
    
    [_manager setDelegate:self];
    [_manager connect];
}

- (void) disconnect {
    if (_manager) {
        [_manager disconnect];
    }
}

- (void) didConnectWithSuccess {
    [_manager launchApplication:_appId];
}

- (void) didFailToConnect {
    
}

- (void)sendMessage:(NSString *)message withExtras:(NSDictionary *)extras toDevice:(TVAbstractCompanionDevice*)device withCompletion:(void (^)(bool, NSDictionary *))completion {
    
    if (message && _manager && [_manager isConnectedToApp] && _control && [_control isConnected]) {
        if ([message isEqualToString:CompanionMessageGetVolume]) {
            
            if (completion) {
                NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
                NSNumber* number = [NSNumber numberWithFloat:volume];
                [dictionary setValue:number forKey:@"volume"];
                completion(true, dictionary);
            }
            
        } else if ([message isEqualToString:CompanionMessageSetVolume]) {
            
            if (extras && [extras valueForKey:@"volume"]) {
                NSNumber* vol = [extras valueForKey:@"volume"];
                float newvol = [vol floatValue];
                [_control setStreamVolume:newvol];
                volume = newvol;
                if (completion) {
                    completion(true, nil);
                }
            } else {
                completion(false, nil);
            }
            
        } else {
            NSMutableDictionary* jsonDictionary = [[NSMutableDictionary alloc] init];
            [jsonDictionary setObjectOrNil:message forKey:@"command"];
            [jsonDictionary setObjectOrNil:extras forKey:@"extra"];
            NSError* error;
            NSData* data = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:0 error:&error];
            
            if (!error) {
                NSString* strdata = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                [_customcontrol sendTextMessage:strdata];
                if (completion) {
                    completion(true, nil);
                }
            } else {
                if (completion) {
                    completion(false, nil);
                }
            }
        }
    } else {
        if (completion ) {
            completion(false, nil);
        }
    }
    
}


#pragma mark GCKDeviceScannerListener protocol

- (void)deviceDidComeOnline:(GCKDevice *)device {
    NSLog(@"deviceDidComeOnline (no filtering)");
}

- (void)deviceDidGoOffline:(GCKDevice *)device {
    NSLog(@"deviceDidGoOfflier (no filtering)");
}

#pragma mark GCKDeviceFilterListener

- (void)deviceDidComeOnline:(GCKDevice *)device forDeviceFilter:(GCKDeviceFilter *)deviceFilter {
    TVCompanionChromecastDevice* myDevice = [[TVCompanionChromecastDevice alloc] initWithGCKDevice:device];
    
    BOOL hasDevice = NO;
    for (TVCompanionChromecastDevice* someDevice in _devices) {
        if (someDevice.uid == myDevice.uid) {
            hasDevice = YES;
        }
    }
    
    if (!hasDevice) {
        [_devices addObject:myDevice];
        if ([self delegate] && [[self delegate] respondsToSelector:@selector(onDiscovery:)]) {
            [[self delegate] onDiscovery:_devices];
        }
    }
}

- (void)deviceDidGoOffline:(GCKDevice *)device forDeviceFilter:(GCKDeviceFilter *)deviceFilter {
    TVCompanionChromecastDevice* myDevice = [[TVCompanionChromecastDevice alloc] initWithGCKDevice:device];
    TVCompanionChromecastDevice* tDevice = nil;
    for (TVCompanionChromecastDevice* someDevice in _devices) {
        if (someDevice.uid == myDevice.uid) {
            tDevice = someDevice;
            break;
        }
    }
    
    if (tDevice) {
        [_devices removeObject:tDevice];
        if ([self delegate] && [[self delegate] respondsToSelector:@selector(onDiscovery:)]) {
            [[self delegate] onDiscovery:_devices];
        }
    }
}

#pragma mark GCKDeviceManagerDelegate


-(void)deviceManagerDidConnect:(GCKDeviceManager *)deviceManager {
    [self didConnectWithSuccess];
}

-(void)deviceManager:(GCKDeviceManager *)deviceManager didConnectToCastApplication:(GCKApplicationMetadata *)applicationMetadata sessionID:(NSString *)sessionID launchedApplication:(BOOL)launchedApplication {
    _control = [[GCKMediaControlChannel alloc] init ];//:@"urn:x-cast:com.kaltura"];
    _customcontrol = [[TVChromecastCustomChannel alloc] initWithNamespace:@"urn:x-cast:com.kaltura"];
    [_manager addChannel:_customcontrol];
    [_control setDelegate:self];
    [_manager addChannel:_control];
}

-(void)deviceManager:(GCKDeviceManager *)deviceManager didDisconnectFromApplicationWithError:(NSError *)error {
    NSLog(@"[didDisconnectFromApplicationWithError");
}

-(void)deviceManager:(GCKDeviceManager *)deviceManager didDisconnectWithError:(NSError *)error {
    NSLog(@"[didDisconnectWithError");
}

-(void)deviceManager:(GCKDeviceManager *)deviceManager didFailToConnectToApplicationWithError:(NSError *)error {
    [self didFailToConnect];
}

-(void)deviceManager:(GCKDeviceManager *)deviceManager didFailToStopApplicationWithError:(NSError *)error {
    NSLog(@"[didFailToStopApplicationWithError");
}

-(void)deviceManager:(GCKDeviceManager *)deviceManager didReceiveActiveInputStatus:(GCKActiveInputStatus)activeInputStatus {
    NSLog(@"[didReceiveActiveInputStatus");
}

-(void)deviceManager:(GCKDeviceManager *)deviceManager didReceiveStatusForApplication:(GCKApplicationMetadata *)applicationMetadata {
    NSLog(@"[didReceiveStatusForApplication");
}

-(void)deviceManager:(GCKDeviceManager *)deviceManager volumeDidChangeToLevel:(float)volumeLevel isMuted:(BOOL)isMuted {
    NSLog(@"[volumeDidChangeToLevel");
    volume = volumeLevel;
}

#pragma mark GCKMediaControlChannelDelegate






@end
