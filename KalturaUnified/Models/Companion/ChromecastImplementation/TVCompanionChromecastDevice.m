//
//  TVNCompanionChromecastDevice.m
//  Tvinci
//
//  Created by Sagi Antebi on 8/25/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import "TVCompanionChromecastDevice.h"
#import "TVCompanionChromecastHandler.h"

@interface TVCompanionChromecastDevice ()

@property (nonatomic)  GCKDevice* device;

@end

@implementation TVCompanionChromecastDevice

@synthesize device = _device;


-(id)initWithGCKDevice:(GCKDevice *)device {
    id this = [super init];
    if (this) {
        [this setDevice:device];
    }
    return this;
}

-(GCKDevice *)getGCKDevice {
    return _device;
}


- (int)getType {
    return TYPE_CHROMECAST;
}

- (void)setDevice:(GCKDevice *)device {
    _device = device;
    [self setName:[device friendlyName]];
    [self setBaseAddress:[device ipAddress]];
    [self setUid:[device deviceID]];
}



@end
