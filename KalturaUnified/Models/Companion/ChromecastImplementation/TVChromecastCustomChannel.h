//
//  TVNChromecastCustomChannel.h
//  Tvinci
//
//  Created by Sagi Antebi on 8/28/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import <GoogleCast/GoogleCast.h>

@interface TVChromecastCustomChannel : GCKCastChannel

@end
