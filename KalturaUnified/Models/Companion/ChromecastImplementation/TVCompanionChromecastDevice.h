//
//  TVNCompanionChromecastDevice.h
//  Tvinci
//
//  Created by Sagi Antebi on 8/25/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//


#import <TvinciSDK/TVAbstractCompanionHandler.h>
//#import <GoogleCast/GCKDevice.h>

@interface TVCompanionChromecastDevice : TVAbstractCompanionDevice

- (id)initWithGCKDevice: (GCKDevice*)device;

- (GCKDevice*) getGCKDevice;

@end
