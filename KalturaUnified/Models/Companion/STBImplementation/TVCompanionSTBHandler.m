//
//  TVCompanionSTBHandler.m
//  Tvinci
//
//  Created by Sagi Antebi on 8/24/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import "TVCompanionSTBHandler.h"
#import <TvinciSDK/CompanionManager.h>
#import <TvinciSDK/TVCompanionAPI.h>
#import "TVCompanionSTB.h"
#import <TvinciSDK/TVCompanionHandler.h>


@interface TVCompanionSTBHandler ()<CompanionModeProtocol>

@property (strong, nonatomic) TVDomain * domain ;
@end

@implementation TVCompanionSTBHandler



- (instancetype)init
{
    self = [super init];
    if (self) {
           }
    return self;
}


- (int) getType {
    return TYPE_STB;
}

- (void) stopScanning {
    //unsupported
}

- (void) scan {
    
    CompanionManager * companionManager = [CompanionManager sharedManager];
    companionManager.delegate = (id<CompanionModeProtocol>)self;
    [companionManager searchForDevices];
}




- (void) sendVodMedia: (TVMediaItem*)media toDevice: (TVAbstractCompanionDevice*)device withFile: (TVFile*)file currentProgress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))completion {
    
    
    
    __weak TVPAPIRequest *request = request = [TVCompanionAPI requestForSendPlayToSTBWithBaseUrl:device.baseAddress
                                                                                    playback_type:nil
                                                                                     epgProgramId:nil
                                                                                         siteGuid:[[[TVSessionManager sharedTVSessionManager] currentUser] siteGUID]
                                                                                         itemType:@"vod"
                                                                                          mediaId:media.mediaID
                                                                                           fileId:file.fileID
                                                                                         domainId:self.domain.domainID
                                                                                        mediaMark:progress
                                                                                     doRequestPin:NO
                                                                                         delegate:nil];
    
    
    [request setFailedBlock:^{
        
        if (completion)
        {
            completion(CompanionHandlerResultFailure);
        }
        
    }];
    
    [request setCompletionBlock:^{
        
        NSDictionary * dict = [request JSONResponse];
        NSString* statusStr = [dict objectOrNilForKey:@"status"];
        CompanionHandlerResult result;
        
        if ([[statusStr lowercaseString] isEqualToString:@"unknown_format"]) {
            result = CompanionHandlerResultFailure;
        }else if ([[statusStr lowercaseString] isEqualToString:@"success"]) {
            result = CompanionHandlerResultOk;
        }else {
            result = CompanionHandlerResultFailure;
        }
        completion(result);
    }];
    
    [request startAsynchronous];
}

- (void) sendLiveMedia: (TVMediaItem*)media toDevice:(TVAbstractCompanionDevice*)device withFile: (TVFile*)file andProgram: (TVEPGProgram*)program forPlaybackMode: (NSString*)playbackType progress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result)) completion
{
    
    __weak TVPAPIRequest *request = request = [TVCompanionAPI 
                                               requestForSendPlayToSTBWithBaseUrl:device.baseAddress
                                                                                    playback_type:playbackType
                                                                                     epgProgramId:program.EPGId
                                                                                         siteGuid:[[[TVSessionManager sharedTVSessionManager] currentUser] siteGUID]
                                                                                         itemType:@"live"
                                                                                          mediaId:media.mediaID
                                                                                           fileId:file.fileID
                                                                                         domainId:self.domain.domainID
                                                                                        mediaMark:progress
                                                                                     doRequestPin:NO
                                                                                         delegate:nil];
    
    
    [request setFailedBlock:^{
        
        if (completion)
        {
            completion(CompanionHandlerResultFailure);
        }
        
    }];
    
    [request setCompletionBlock:^{
        
        NSDictionary * dict = [request JSONResponse];
        NSString* statusStr = [dict objectOrNilForKey:@"status"];
        CompanionHandlerResult result;
        
        if ([[statusStr lowercaseString] isEqualToString:@"unknown_format"]) {
            result = CompanionHandlerResultFailure;
        }else if ([[statusStr lowercaseString] isEqualToString:@"success"]) {
            result = CompanionHandlerResultOk;
        }else {
            result = CompanionHandlerResultFailure;
        }
        completion(result);
    }];
    
    [request startAsynchronous];
}

- (void) connect: (TVAbstractCompanionDevice*)device {
    //does nothing
    TVCompanionSTB * theDevice = (TVCompanionSTB*)device;
    [[CompanionManager sharedManager] pairDevice:theDevice.wrappedDevice];
}

- (void) disconnect {
    //does nothing

    [[CompanionManager sharedManager] unPairCurrentDevice];
}

- (void)sendMessage:(NSString *)message withExtras:(NSDictionary *)extras toDevice:(TVAbstractCompanionDevice*)device withCompletion:(void (^)(bool, NSDictionary *))completion
{
    TVCompanionSTB * STBDevice = (TVCompanionSTB *)device;
   TVPAPIRequest * request = [TVCompanionAPI requestForSendRemoteMessageToSTBWithBaseUrl:STBDevice.baseAddress message:message delegate:nil];
    
    [request setFailedBlock:^{
       
        if (completion)
        {
            completion(NO,nil);
        }
    }];
    
    [request setCompletionBlock:^{
        if (completion)
        {
            completion(YES,nil);
        }
    }];
    
    [request startAsynchronous];
}



#pragma mark - companion delegate
- (void)companionSearchFinishedWithDevices:(NSArray *)devices
{

    if (self.domain == nil)
    {
        if ([TVSessionManager sharedTVSessionManager].currentDomain != nil)
        {
            self.domain = [TVSessionManager sharedTVSessionManager].currentDomain;
            [self deviceDomainKnownForFilteringDiscoveredDeives:devices];
        }
        else
        {
            __weak TVPAPIRequest * request = [TVPDomainAPI requestForGetDomainInfo:nil];
            
            [request setFailedBlock:^{
                
                [self companionSearchFinishedWithError:nil];
            }];
            
            [request setCompletionBlock:^{
                
                NSDictionary* domainDic = [request JSONResponse];
                self.domain = [[TVDomain alloc] initWithDictionary:domainDic];
                [self deviceDomainKnownForFilteringDiscoveredDeives:devices];
            }];
            
            [request startAsynchronous];
        }
    }
    else
    {
        [self deviceDomainKnownForFilteringDiscoveredDeives:devices];
    }
    
    
    
}

- (void)companionSearchFinishedWithError:(NSError *)error
{
    if ([self delegate] && [[self delegate] respondsToSelector:@selector(onDiscovery:)]) {
        [[self delegate] onDiscovery:nil];
    }
}


-(void) deviceDomainKnownForFilteringDiscoveredDeives:(NSArray *) devices
{
    NSArray *knownDevices = self.domain.deviceFamilies;
    
    //  Merge nearby devices with the registered STBs, means "registeredSTBs" array will have only registered STBs that are positioned nearby.
    NSArray* registeredSTBs = [TVCompanionHandler crossDevicesArray:devices withUDIDsArray:knownDevices];
    
    //registeredSTBs = [NSArray arrayWithObject:devices[1]];
    
    if ([registeredSTBs count]) {
        
        //  Download essentail XML data for the list of STBs
        
        [[CompanionManager sharedManager] downloadXMLDataForDevices:registeredSTBs WithCompletionBlock:^(NSArray *downloadedDevices) {
            
            //  XML data was downloaded, just refresh the UI
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            for (TVCompanionDevice *device in [CompanionManager sharedManager].devicesItems)
            {
                NSLog(@"device name %@", device.friendlyName);
                
                [array addObject:device];
                
            }
            
            NSMutableArray* retarr = [[NSMutableArray alloc] init];
            
            for (TVCompanionDevice* device in array) {
                TVCompanionSTB* stb = [[TVCompanionSTB alloc] init];
                [stb setWrappedDevice:device];
                [retarr addObject:stb];
            }
            
            if ([self delegate] && [[self delegate] respondsToSelector:@selector(onDiscovery:)]) {
                [[self delegate] onDiscovery:retarr];
            }
            
            
        }];
    }
    else
    {
        if ([self delegate] && [[self delegate] respondsToSelector:@selector(onDiscovery:)]) {
            [[self delegate] onDiscovery:nil];
        }
    }

}


@end
