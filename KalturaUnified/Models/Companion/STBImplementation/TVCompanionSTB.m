//
//  TVNCompanionSTB.m
//  Tvinci
//
//  Created by Sagi Antebi on 8/24/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import "TVCompanionSTB.h"
#import "TVCompanionSTBHandler.h"

@implementation TVCompanionSTB

@synthesize wrappedDevice = _wrappedDevice;

- (int) getType {
    return TYPE_STB;
}

- (NSString*)uid {
    return [_wrappedDevice udid];
}

- (NSString*)baseAddress {
    return [_wrappedDevice basUrlString];
}

- (NSString*)name {
    if ([_wrappedDevice friendlyName] == nil || [[_wrappedDevice friendlyName] isEqualToString:@""])
    {
            return LS(@"nameless_stb");
    }
    return [_wrappedDevice friendlyName];
}

- (void)setName:(NSString *)name {
    //unsupprted
}

- (void)setUid:(NSString *)uid {
    //unsupported
}

- (void)setBaseAddress:(NSString *)baseAddress {
    //unsupported
}


- (BOOL)isEqual:(TVCompanionSTB * )object
{
    if ([self.uid isEqualToString:object.uid])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


@end


