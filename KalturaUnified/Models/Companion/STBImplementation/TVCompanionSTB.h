//
//  TVNCompanionSTB.h
//  Tvinci
//
//  Created by Sagi Antebi on 8/24/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import <TvinciSDK/TVAbstractCompanionDevice.h>
#import <TvinciSDK/TVCompanionDevice.h>

@interface TVCompanionSTB : TVAbstractCompanionDevice

@property (nonatomic, strong) TVCompanionDevice* wrappedDevice;

@end
