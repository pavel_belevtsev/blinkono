//
//  OnoConfigurationManager.m
//  
//
//  Created by Israel Berezin on 23/12/2015.
//
//

#import "OnoConfigurationManager.h"

@implementation OnoConfigurationManager


-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    NSDictionary *params = [dictionary objectOrNilForKey:@"params"];

     ASLogInfo(@"conf dictionary = %@",dictionary);
    self.chromecastReciverId = [params objectOrNilForKey:@"ChromecastAppID"];
}

-(void)setChromecastReciverId:(NSString *)chromecastReciverId
{
    if (chromecastReciverId)
    {
        _chromecastReciverId = chromecastReciverId;
    }
    else
    {
        _chromecastReciverId = nil; // default app;
    }
    [[TVMixedCompanionManager sharedInstance] addHandler:[[KACompanionChromecastHandler alloc] initWithApplicationId:_chromecastReciverId]];// ono app
  //  ASLogInfo(@"ChromecastAppID = %@",self.chromecastReciverId);
//    [[TVMixedCompanionManager sharedInstance] addHandler:[[KACompanionChromecastHandler alloc] initWithApplicationId:@"7EB6270C"]];//@"5E529EEF"]]; // test app
}

@end
