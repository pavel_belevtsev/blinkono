//
//  OnoConfigurationManager.h
//  
//
//  Created by Israel Berezin on 23/12/2015.
//
//

#import <TvinciSDK/TvinciSDK.h>

@interface OnoConfigurationManager : TVConfigurationManager

@property (nonatomic, retain) NSString * chromecastReciverId;

@end
