//
//  Utils.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "Utils.h"
#import "UIImage+Tint.h"
#define TenHours 60*60*10


@implementation Utils

typedef NS_ENUM(NSInteger,TVNPictureShape) {
    TVNPictureShapePortrait,
    TVNPictureShapeLandscape,
    TVNPictureShapeSquare
};

+ (CGFloat)colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

+ (UIColor *)colorFromString:(NSString *)colorString {
    
    if ([colorString length] > 6) {
        colorString = [colorString substringFromIndex:[colorString length] - 6];
    }
    CGFloat red   = [self colorComponentFrom: colorString start: 0 length: 2];
    CGFloat green = [self colorComponentFrom: colorString start: 2 length: 2];
    CGFloat blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

+ (void)setButtonFont:(UIButton *)button {
    
    [self setButtonFont:button bold:NO];
    
}

+ (void)setButtonFont:(UIButton *)button bold:(BOOL)bold {
    
    UIFont *currentFont = button.titleLabel.font;
    
    NSString *fontName = bold ? FONT_BOLD : FONT_REGULAR;
    button.titleLabel.font = [UIFont fontWithName:fontName size:currentFont.pointSize];
    
}

+ (void)setButtonFont:(UIButton *)button semibold:(BOOL)semibold {
    
    UIFont *currentFont = button.titleLabel.font;
    
    NSString *fontName = semibold ? FONT_SEMIBOLD : FONT_REGULAR;
    button.titleLabel.font = [UIFont fontWithName:fontName size:currentFont.pointSize];
    
    
}

+ (void)setLabelFont:(UILabel *)label {
    
    [self setLabelFont:label bold:NO];
    
}

+ (void)setLabelFont:(UILabel *)label bold:(BOOL)bold {
    
    UIFont *currentFont = label.font;
    
    NSString *fontName = bold ? FONT_BOLD : FONT_REGULAR;
    label.font = [UIFont fontWithName:fontName size:currentFont.pointSize];
    
}

+ (void)setLabelFont:(UILabel *)label size:(CGFloat)size bold:(BOOL)bold
{
    NSString *fontName = bold ? FONT_BOLD : FONT_REGULAR;
    label.font = [UIFont fontWithName:fontName size:size];
    
}

+ (void)setTextFieldFont:(UITextField *)textField {
    
    [self setTextFieldFont:textField bold:NO];
}

+ (void)setTextFieldFont:(UITextField *)textField bold:(BOOL)bold {
    
    UIFont *currentFont = textField.font;
    
    NSString *fontName = bold ? FONT_BOLD : FONT_REGULAR;
    textField.font = [UIFont fontWithName:fontName size:currentFont.pointSize];
    
}

+ (void)setTextFieldFont:(UITextField *)textField size:(CGFloat)size bold:(BOOL)bold
{
    NSString *fontName = bold ? FONT_BOLD : FONT_REGULAR;
    textField.font = [UIFont fontWithName:fontName size:size];
    
}

+ (void)setTextViewFont:(UITextView *)textView {
    
    [self setTextViewFont:textView bold:NO];
}

+ (void)setTextViewFont:(UITextView *)textView bold:(BOOL)bold {
    
    UIFont *currentFont = textView.font;
    
    NSString *fontName = bold ? FONT_BOLD : FONT_REGULAR;
    textView.font = [UIFont fontWithName:fontName size:currentFont.pointSize];
    
}

+ (void)setTextViewFont:(UITextView *)textView size:(CGFloat)size bold:(BOOL)bold {
    
    NSString *fontName = bold ? FONT_BOLD : FONT_REGULAR;
    textView.font = [UIFont fontWithName:fontName size:size];
    
}

+ (BOOL)NSStringIsValidEmail:(NSString *)checkString {
    
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
}

+ (BOOL)NSStringIsOnlyDigitsAndChars:(NSString *)checkString {
    NSMutableCharacterSet * digitsAndLetters = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
    [digitsAndLetters formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
    
    NSCharacterSet *notDigitsOrLetters = [digitsAndLetters invertedSet];
    
    if ([checkString rangeOfCharacterFromSet:notDigitsOrLetters].location == NSNotFound)
    {
        // newString consists only letters and digits 0 through 9
        return YES;
    }
    return NO;
}

+ (NSString *)nibName:(NSString *)name {

    return [NSString stringWithFormat:@"%@%@", name, (isPhone ? @"Phone" : @"Pad")];
    
}

+ (NSURL *)getBestPictureURLForMediaItem:(TVMediaItem *)mediaItem andSize:(CGSize)size  {
    NSArray *picturesDataArray = mediaItem.pictures;
    
    NSInteger shape;
    CGFloat width = size.width;
    CGFloat height = size.height;
    
    if (width > height) {
        shape = TVNPictureShapeLandscape;
    } else if (height > width) {
        shape = TVNPictureShapePortrait;
    } else {
        shape = TVNPictureShapeSquare;
    }
    
    CGFloat minIndex = 0;
    NSInteger counter = 0;
    NSInteger minCounterPlace = 0;


    for (NSInteger i = 0; i < picturesDataArray.count; i++) {
        TVImage *im = (TVImage *)picturesDataArray[i];
        
        if (!im.imageUrl)
            continue;
        
        CGSize picSize = im.imageSize;
        if (!picSize.height || !picSize.width) {
            //DDLogWarn(@"Pic size string not valid");
        } else {
            CGFloat widthNumber = picSize.width;
            CGFloat heightNumber = picSize.height;
            BOOL newIndexUpdated = NO;
            CGFloat index = 0;


            // This calculating the diagonal of the square that represent the difference betweens the 2 images.
            
            if (shape == TVNPictureShapeLandscape && widthNumber > heightNumber) { // if landscape
                index = powf((size.width-widthNumber), 2) + powf((size.height-heightNumber), 2);
                newIndexUpdated = YES;
            } else if (shape == TVNPictureShapePortrait && widthNumber < heightNumber) { // if portrait
                index = powf((size.width-widthNumber), 2) + powf((size.height-heightNumber), 2);
                newIndexUpdated = YES;
            } else if (shape == TVNPictureShapeSquare && widthNumber == heightNumber) { // if square
                index = powf((size.width-widthNumber), 2) + powf((size.height-heightNumber), 2);
                newIndexUpdated = YES;
            }
            
            if (newIndexUpdated) {
                if (counter == 0) {
                    minIndex = index;
                    minCounterPlace = i;
                } else {
                    if (index < minIndex) {
                        minIndex = index;
                        minCounterPlace = i;
                    }
                }
                counter++;
            }
        }
    }
    if(picturesDataArray.count){
        return ((TVImage *)picturesDataArray[minCounterPlace]).imageUrl;
    }
    return nil;
}

+ (CGSize)getBestPictureSizeForMediaItem:(TVMediaItem *)mediaItem andSize:(CGSize)size  {
    NSArray *picturesDataArray = mediaItem.pictures;
    
    NSInteger shape;
    CGFloat width = size.width * 2;
    CGFloat height = size.height * 2;
    
    if (width > height) {
        shape = TVNPictureShapeLandscape;
    } else if (height > width) {
        shape = TVNPictureShapePortrait;
    } else {
        shape = TVNPictureShapeSquare;
    }
    
    CGFloat minIndex = 0;
    NSInteger counter = 0;
    NSInteger minCounterPlace = 0;
    
    for (int i = 0; i < picturesDataArray.count; i++) {
        CGSize picSize = ((TVImage *)picturesDataArray[i]).imageSize;
        if (!picSize.height || !picSize.width) {
            //DDLogWarn(@"Pic size string not valid");
        } else {
            CGFloat widthNumber = picSize.width;
            CGFloat heightNumber = picSize.height;
            BOOL newIndexUpdated = NO;
            CGFloat index = 0;
            
            if (shape == TVNPictureShapeLandscape && widthNumber > heightNumber) { // if landscape
                index = powf((size.width-widthNumber), 2) + powf((size.height-heightNumber), 2);
                newIndexUpdated = YES;
            } else if (shape == TVNPictureShapePortrait && widthNumber < heightNumber) { // if portrait
                index = powf((size.width-widthNumber), 2) + powf((size.height-heightNumber), 2);
                newIndexUpdated = YES;
            } else if (shape == TVNPictureShapeSquare && widthNumber == heightNumber) { // if square
                index = powf((size.width-widthNumber), 2) + powf((size.height-heightNumber), 2);
                newIndexUpdated = YES;
            }
            
            if (newIndexUpdated) {
                if (counter == 0) {
                    minIndex = index;
                    minCounterPlace = i;
                } else {
                    if (index < minIndex) {
                        minIndex = index;
                        minCounterPlace = i;
                    }
                }
                counter++;
            }
        }
    }
    if(picturesDataArray.count){
        return ((TVImage *)picturesDataArray[minCounterPlace]).imageSize;
    }
    return CGSizeZero;
}


+ (NSString*)getStringFromInteger:(NSInteger) value {
    return [NSString stringWithFormat:@"%ld", (long)value];
}

+ (CGRect)getLabelFrame:(UILabel *)label maxHeight:(float)maxHeight minHeight:(float)minHeight {
    
    /*
     CGSize maximumSize = CGSizeMake(label.frame.size.width, maxHeight);
     CGSize newSize = [label.text sizeWithFont:label.font
     constrainedToSize:maximumSize
     lineBreakMode:label.lineBreakMode];
     
     return CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, (newSize.height < minHeight ? minHeight : newSize.height) + 10.0);
     */
    if (!label.text) label.text = @"";
    NSString *text = label.text;
    CGFloat width = label.frame.size.width;
    UIFont *font = label.font;
    NSAttributedString *attributedText =
    [[NSAttributedString alloc]
      initWithString:text
      attributes:@
      {
      NSFontAttributeName: font
      }];
    
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, maxHeight}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;
    
    float height = (size.height < minHeight ? minHeight : size.height + 3.0);//(size.height / 10));
    if (height > maxHeight) height = maxHeight;
    
    return CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, height);
    
}

+ (void)initButtonUI:(UIButton *)brandButton title:(NSString *)title backGroundColor:(UIColor *)color titleColor:(UIColor *)titleColor {
    UIImage *brandImage = [UIImage imageNamed:@"brand_button"];
    UIImage *coloredBrandImage = [brandImage imageTintedWithColor:color];
    UIImage *sizeableBrandImage = [coloredBrandImage resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [brandButton setTitle:title forState:UIControlStateNormal];
    [brandButton setBackgroundImage:sizeableBrandImage forState:UIControlStateNormal];
    [brandButton setTitleColor:titleColor forState:UIControlStateNormal];
    [brandButton setTitleColor:titleColor forState:UIControlStateHighlighted];
    [Utils setLabelFont:brandButton.titleLabel size:brandButton.titleLabel.font.pointSize bold:YES];
    
}

+ (NSString *)getSeriesName:(TVMediaItem *)mediaItem {
    
    NSString *title = @"";
    
    if ([mediaItem.mediaTypeName isEqualToString:@"Episode"] && [mediaItem.tags objectForKey:@"Series name"] && [[mediaItem.tags objectForKey:@"Series name"] count] > 0) {
        title = [[mediaItem.tags objectForKey:@"Series name"] objectAtIndex:0];
    }
    if ([mediaItem.mediaTypeName isEqualToString:@"Series"]) {
        title = mediaItem.name;
    }
    return title;
    
}

+ (NSDictionary *) mapEpisodesBySeasonsNumber:(NSArray *) episodes {
    
    NSMutableDictionary * episodesBySeasons = [NSMutableDictionary dictionary];
    
    for (NSDictionary *dic in episodes) {
        
        TVMediaItem *item = [[TVMediaItem alloc]initWithDictionary:dic];
        
        NSString * seasonNumber = [[item metaData] objectForKey:@"Season number"];
        if (seasonNumber) {
            NSMutableArray * mediaArray = [episodesBySeasons objectForKey:seasonNumber];
            if (mediaArray == nil)
            {
                [episodesBySeasons setObject:[NSMutableArray arrayWithObject:item] forKey:seasonNumber];
            }
            else
            {   [mediaArray addObject:item];
                [episodesBySeasons setObject:mediaArray forKey:seasonNumber];
            }
        }
    }
    
    for (NSMutableArray *array in [episodesBySeasons allValues]) {
        [array sortUsingComparator:^NSComparisonResult(TVMediaItem *item1, TVMediaItem *item2)
         {
             
             NSInteger number1 = [[item1 getMediaItemEpisodeNumber] intValue];
             NSInteger number2 = [[item2 getMediaItemEpisodeNumber] intValue];
             
             if (number1 == number2) {
                 
                 return NSOrderedSame;
                 
             } else {
                 
                 if ((number1 < number2 && episodeListOrdering == SORT_ASC) || (number1 > number2 && episodeListOrdering == SORT_DESC)) {
                     return NSOrderedAscending;
                 } else {
                     return NSOrderedDescending;
                 }
                 
             }
             
             
         }];
        
    }
    return [NSDictionary dictionaryWithDictionary:episodesBySeasons];
    
}

+ (NSArray *)sortSeasonsNumbers:(NSArray *) seasons {
    // sort seasons
    
    NSArray * sortedSeasons =  [seasons sortedArrayUsingComparator:^NSComparisonResult(NSString * seasonNumber1, NSString * seasonNumber2)
                                {
                                    
                                    NSInteger number1 = [seasonNumber1 integerValue];
                                    NSInteger number2 = [seasonNumber2 integerValue];
                                    
                                    if (number1 == number2) {
                                        
                                        return NSOrderedSame;
                                        
                                    } else {
                                        
                                        if ((number1 < number2 && seasonListOrdering == SORT_ASC) || (number1 > number2 && seasonListOrdering == SORT_DESC)) {
                                            return NSOrderedAscending;
                                        } else {
                                            return NSOrderedDescending;
                                        }
                                        
                                    }
                                    
                                    
                                }];
    
    
    return sortedSeasons;
}

+ (NSString *)stringFromSocialAction:(TVUserSocialActionDoc *)socialAction {

    NSString *action = @"";
    if ([socialAction.activityVerb.actionName isEqualToString:@"LIKE"])
    {
        action = LS(@"frindsActivitySocialAction_like");
    }
    else if ([socialAction.activityVerb.actionName isEqualToString:@"WATCHES"])
    {
        action = LS(@"frindsActivitySocialAction_watch");
    }
    else if ([socialAction.activityVerb.actionName isEqualToString:@"SHARE"])
    {
        action = LS(@"frindsActivitySocialAction_share");
    }
    
    return action;
}

+ (NSString *)getActionDurationTimeFromNow:(NSDate *)addedDate {
    __block NSString *retValue = @"";
    NSString *scannedFormat = @"%d %@ %@";
    [self getDurationTimeFromNow:addedDate completion:^(NSInteger number, NSString *unit, NSString *altValue) {
        
        if (altValue)
            retValue = altValue;
        else
            retValue = [NSString stringWithFormat:scannedFormat, number, unit, LS(@"dateTimeStr_ago")];
    }];
    return retValue;
}

+ (void) getDurationTimeFromNow:(NSDate *)addedDate completion:(void(^)(NSInteger number, NSString *unit, NSString *altValue))completion {
    [self getDurationTimeFromDate:addedDate toDate:[NSDate date] completion:completion];
}

+ (void) getDurationTimeFromDate:(NSDate *)startDate toDate:(NSDate *)endDate completion:(void(^)(NSInteger number, NSString *unit, NSString *altValue))completion {
    unsigned unitFlags = NSSecondCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSWeekOfYearCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit;
    NSDateComponents *components = [[NSCalendar currentCalendar] components:unitFlags fromDate:startDate toDate:endDate options:0];
    
    NSInteger number = 0;
    NSString *unit = @"", *altValue = nil;
    if (components.year > 0) {
        number = components.year;
        if (number > 1) {
            unit = [unit stringByAppendingString:LS(@"dateTimeStr_years")];
        } else if (number == 1) {
            unit = [NSString stringWithFormat:LS(@"dateTimeStr_year")];
        }
    }
    else if (components.month > 0) {
        number = components.month;
        if (number > 1) {
            unit = [unit stringByAppendingString:LS(@"dateTimeStr_months")];
        } else if (number == 1) {
            unit = [NSString stringWithFormat:LS(@"dateTimeStr_month")];
        }
    }
    else if (components.weekOfYear > 0) {
        number = components.weekOfYear;
        if (number > 1) {
            unit = [unit stringByAppendingString:LS(@"dateTimeStr_weeks")];
        } else if (number == 1) {
            unit = [NSString stringWithFormat:LS(@"dateTimeStr_week")];
        }
    }
    else if (components.day > 0) {
        number = components.day;
        if (number > 1) {
            unit = [unit stringByAppendingString:LS(@"dateTimeStr_days")];
        } else if (number == 1) {
            unit = [NSString stringWithFormat:LS(@"dateTimeStr_day")];
        }
    }
    else if (components.hour > 0) {
        number = components.hour;
        if (number > 1) {
            unit = [unit stringByAppendingString:LS(@"dateTimeStr_hours")];
        } else if (number == 1) {
            unit = [NSString stringWithFormat:LS(@"dateTimeStr_hour")];
        }
    } else {
        number = components.minute;
        if (number <= 0) {
            altValue =  LS(@"just_now");
        }
        if (number > 1) {
            unit = [unit stringByAppendingString:LS(@"dateTimeStr_minutes")];
        } else if (number == 1) {
            unit = [NSString stringWithFormat:LS(@"dateTimeStr_minute")];
        }
    }
    
    completion(number, unit, altValue);
}

+ (NSString *)convertTextToHTML:(NSString *)str {
    
    NSMutableString *html = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"<head><style type=\"text/css\">body {margin: 0px; color: #A1A1A1; font-family: ProximaNova-Regular; font-size: %d; line-height : %dpx} a:link {color: #A1A1A1 } a:visited { color: #A1A1A1 }</style></head><body>", (isPad ? 15 : 12), (isPad ? 17 : 14)]];
    
    while (([str rangeOfString:@"http://"].location != NSNotFound) || [str rangeOfString:@"https://"].location != NSNotFound) {
        NSUInteger location = MIN([str rangeOfString:@"http://"].location, [str rangeOfString:@"https://"].location) ;
        
        if (location > 0) {
            [html appendString:[str substringToIndex:location]];
            str = [str substringFromIndex:location];
        }
        
        NSUInteger locNewLine = [str rangeOfString:@"\n"].location;
        NSUInteger locSpace = [str rangeOfString:@" "].location;
        
        NSString *url = str;
        if ((locNewLine != NSNotFound) || (locSpace != NSNotFound)) {
            
            NSUInteger length = [str length];
            
            if ((locNewLine != NSNotFound) && (locSpace != NSNotFound)) {
                length = MIN(locNewLine, locSpace);
            } else  if (locNewLine != NSNotFound) {
                length = locNewLine;
            } else  if (locSpace != NSNotFound) {
                length = locSpace;
            }
            
            url = [str substringToIndex:length];
            
        }
        
        str = [str substringFromIndex:[url length]];
        
        [html appendString:[NSString stringWithFormat:@"<a href=\"%@\">%@</a>", url, url]];
    }
    
    [html appendString:str];
    [html appendString:@"</body>"];
    return [html stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    
}

+ (NSString *) md5:(NSString*) string {
    const char *str = [string UTF8String];
    if (str == NULL) {
        str = "";
    }
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), r);
    NSString *md5 = [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                     r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]];
    
    return md5;
}

// "PSC : Vodafone Grup : SYN-3577 -> playout errors appears from time to time [IOS]
 
+(BOOL)canTrickPlayCachupToProgram:(APSTVProgram *) program
{
    NSDate * startDate = program.startDateTime;
    NSDate * endDate = program.endDateTime;
    NSTimeInterval startInSec =[startDate timeIntervalSince1970];
    NSTimeInterval endInSec =[endDate timeIntervalSince1970];
    NSTimeInterval dif = endInSec - startInSec;
    if (dif >= TenHours)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}
/*”PSC : ONO : OS-91 Ono - CR - Rating needs ot be presented in each media page */
+(NSString *)ratingToShow:(NSArray*) raitings{
    int ratingToshow = 0;
    for (NSString *strNum in raitings) {
        if([strNum intValue]==16){
            ratingToshow=[strNum intValue];
            return [NSString stringWithFormat:@"%d",ratingToshow];
        }
        if([strNum intValue]>ratingToshow){
            ratingToshow=[strNum intValue];
        }
        
           }
    
    return [NSString stringWithFormat:@"%d",ratingToshow] ;
}
@end
