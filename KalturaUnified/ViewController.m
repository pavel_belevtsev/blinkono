//
//  ViewController.m
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"
#import "MenuViewController.h"
#import "UIImageView+Animation.h"
#import "UIAlertView+Blocks.h"
#import "UIImageView+WebCache.h"
#import "HomeViewController.h"
#import "BaseSideMenuContainerViewController.h"
#import "ForceUpgraadeManager.h"
#import "RefreshDataManager.h"
#import <TvinciSDK/UAObfuscatedString.h>
#import "UIRulesManagementSystemProvider.h"


@interface ViewController ()
@property (strong, nonatomic) MenuViewController *menuController;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(restartApp:) name:notificationNameRestartApp object:Nil];

    [_imgSpinner startBrandAnimation];
    _viewActivity.alpha = 0.0;
    [UIView animateWithDuration:kViewNavigationDuration animations:^{
                         _viewActivity.alpha = 1.0;
                     }
                     completion:^(BOOL finished){
                         
                         [self takeOff];
                         
                     }];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (APST.logoUrl) {
        [_imgLogo sd_setImageWithURL:APST.logoUrl];
    } else {
        _imgLogo.image = [UIImage imageNamed:@"logo_login"];
    }
    
}

-(void)dealloc
{
    [self unregisterToMenuItemsUpdatedNotification];
}

#pragma mark - Download Configuration

- (void)takeOff {

        [[APSEPGManager sharedEPGManager] setLifeCycleTime:60*60*4];

        [TVConfigurationManager sharedTVConfigurationManager].appIdentifier = (isPhone ? APST.appBundlePhone : APST.appBundlePad);
        [TVConfigurationManager sharedTVConfigurationManager].appVersion = (isPhone ? APST.appVersionPhone: APST.appVersionPad);

        [self registerForConfigurationNotification];
        [self registerToMenuItemsUpdatedNotification];
        [[TVConfigurationManager sharedTVConfigurationManager] downloadConfigurationFile];




}

- (void)unregisterFromConfigurationNotification {
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:TVConfigurationManagerDidLoadConfigurationNotification object:nil];
    [center removeObserver:self name:TVConfigurationManagerDidFailToLoadConfigurationNotification object:nil];
    
}
 
- (void)registerForConfigurationNotification {
    
    [self unregisterFromConfigurationNotification];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(configurationFileLoaded:)
                   name:TVConfigurationManagerDidLoadConfigurationNotification
                 object:nil];
    
    [center addObserver:self selector:@selector(configurationFileFailedToLoad:)
                   name:TVConfigurationManagerDidFailToLoadConfigurationNotification
                 object:nil];
}


- (void)configurationFileFailedToLoad:(NSNotification *)notification {

    if (APST.hasDownloadToGo && ![[OfflineManager sharedInstance] isOffline])
    {
        [[OfflineManager sharedInstance] setIsOffline:YES];
        [[TVConfigurationManager sharedTVConfigurationManager] downloadConfigurationFile];
    }
    else
    {
        [self showNoInternetConnectionAlert];
    }
    
}

- (void)configurationFileLoaded:(NSNotification *)notification
{

    [[UIRulesManagementSystemProvider sharedInstance] startTrackigForUserStatusWithCompletion:^{
        
        if( [APP_SET isOffline] && [LoginM isSignedIn] && [NU(UIUnit_Device_Downloads) isUIUnitExist])
        {
            [self showMainScreen];
            [self updatFileTypes];
        }
        else if (LoginM.internetReachability.currentReachabilityStatus == NotReachable)
        {
            [[OfflineManager sharedInstance] setIsOffline:NO];
            [self showNoInternetConnectionAlert];
            return;
        }

        [self setupTokenization];
        [[[TVSessionManager sharedTVSessionManager] tokenizationManager] generateAccessTokenWithStartBlock:nil failedBlock:^{
            [self generateDeviceTokenFailedToLoad];
        } completionBlock:^{

            if (![[ForceUpgraadeManager sharedInstance] isApplicationContinuencePermitted])
            {
                [[ForceUpgraadeManager sharedInstance] handleForceDownload];
                return;
            }

            [[RefreshDataManager sharedInstance]  addAllTasks];

            [self updatFileTypes];


            if ([TVConfigurationManager sharedTVConfigurationManager].defaultIPNOID.length > 0)
            {
                [MenuM updateDomainMenu];
            }
            else
            {
                [self getMasterMenu];
            }
            
        }];
    }];
}

-(void) updatFileTypes
{
    APST.TVCMediaFormatMainSD = [[TVConfigurationManager sharedTVConfigurationManager].fileFormatNames objectForKey:@"Main"];
    APST.TVCMediaFormatMainHD = [[TVConfigurationManager sharedTVConfigurationManager].fileFormatNames objectForKey:@"Main HD"];
    if (!APST.TVCMediaFormatMainHD) APST.TVCMediaFormatMainHD = APST.TVCMediaFormatMainSD;
    APST.TVCMediaFormatTrailer = [[TVConfigurationManager sharedTVConfigurationManager].fileFormatNames objectForKey:@"Trailer"];
    APST.TVCMediaFormatDownload = [[TVConfigurationManager sharedTVConfigurationManager].fileFormatNames objectForKey:@"Download"];
}

-(void)getMasterMenu
{
    NSInteger menuID = [TVConfigurationManager sharedTVConfigurationManager].mainMenuID;
    
    __weak TVPAPIRequest *request = [TVPSiteAPI requestForGetMenu:menuID delegate:nil];
    
    [request setCompletionBlock:^{
        
        NSDictionary *result = [request JSONResponse];
        //NSLog(@"%@", [result description]);
        
        [MenuM updateMenuItems:result];
        
        if (APST.multiUser) {
            [self checkDeviceDomain];
        } else {
            [self showMainScreen];
        }
        
    }];
    
    [request setFailedBlock:^{
        
    }];
    
    [self sendRequest:request];

    
}

-(void)registerToMenuItemsUpdatedNotification
{
    [self unregisterToMenuItemsUpdatedNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenu) name:KMenuItemsUpdatedNotification object:nil];// KMenuItemsUpdatedErrorNotification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMenuFaild) name:KMenuItemsUpdatedErrorNotification object:nil];
}
-(void)unregisterToMenuItemsUpdatedNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KMenuItemsUpdatedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KMenuItemsUpdatedErrorNotification object:nil];

}

-(void)updateMenuFaild
{
    [self getMasterMenu];
    [self unregisterToMenuItemsUpdatedNotification];
}

- (void)updateMenu
{
    if ([TVConfigurationManager sharedTVConfigurationManager].defaultIPNOID.length > 0)
    {
        if (APST.multiUser) {
            [self checkDeviceDomain];
        } else {
            [self showMainScreen];
        }
    }
    [self unregisterToMenuItemsUpdatedNotification];
}




-(void) generateDeviceTokenFailedToLoad
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"application_cannot_load")
                                                        message:LS(@"alert_no_internet_connection")
                                                       delegate:nil
                                              cancelButtonTitle:LS(@"try_again")
                                              otherButtonTitles:nil];
    
    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        
        if (buttonIndex == 0) {
            
            [self restartHUD];
            
            [self configurationFileLoaded:nil];
            
        }
        
        
    }];

}

- (void)loadUsersList {
    
    [super.userManager loadDomainUsers:^{
    
        [self showMainScreen];
        
    }];
    
}

- (void)checkDeviceDomain {
    
    [super.deviceManager checkDeviceDomain:^{
    
        [self loadUsersList];
        
    }];
    
}

- (void)showMainScreen {
    
    UINavigationController *contentController = [[ViewControllersFactory defaultFactory] contentViewController];
    
    TVUser *user = [TVSessionManager sharedTVSessionManager].currentUser;
    if (user && !LoginM.didRememberMePressed ) {
        
        [LoginM logout];
        
    }
     BaseViewController * controller =  nil;

    if (user && ([user.siteGUID intValue] > 0) && LoginM.didRememberMePressed) {



        if ([APP_SET isOffline])
        {
                controller = isPad?[[ViewControllersFactory defaultFactory] myZoneFullScreenViewController]:[[ViewControllersFactory defaultFactory] myZoneViewController];

        }
        else
        {
            controller = [[ViewControllersFactory defaultFactory] homeViewController];
        }

        [contentController pushViewController:controller animated:NO];

    }
    
    if (self.menuController == nil)
    {
        self.menuController = [[ViewControllersFactory defaultFactory] menuViewController];
        
        NavM.container = [BaseSideMenuContainerViewController
                          containerWithCenterViewController:contentController
                          leftMenuViewController:self.menuController
                          rightMenuViewController:nil];
        
        
    
    }

    [UIView animateWithDuration:kViewNavigationDuration animations:^{
        _viewActivity.alpha = 0.0;
    } completion:^(BOOL finished){

        if ([[self.navigationController viewControllers] lastObject] != NavM.container ) {
             [self.navigationController pushViewController:NavM.container animated:YES];
        }
        else
        {
            BaseSideMenuContainerViewController * sideMenu = [[self.navigationController viewControllers] lastObject] ;
            [sideMenu.centerViewController pushViewController:controller animated:YES];
        }


        NavM.container.menuWidth = self.menuController.menuScrollView.width;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kMainScreenLoadedNotification" object:nil];
        });
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setupTokenization
{
    NSString * appID = Obfuscate.e._8._7._8.f.d._0._0.e.e.a.e._4._5._1._2._9._4._4.a._1._8._7._2._2._7._9.b._3._8.a.a;
    NSString * appSecret = Obfuscate._2._8.f.d.b._6._8._8._6._3._2._5._4.f._0._2._8._3._4.e._5._6._4._9._5.f._6._2._7.d._7._2;
    [[[TVSessionManager sharedTVSessionManager] tokenizationManager] setTokenizationAppID:appID];
    [[[TVSessionManager sharedTVSessionManager] tokenizationManager] setTokenizationAppSecret:appSecret];
}

-(void) showNoInternetConnectionAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"application_cannot_load")
                                                        message:LS(@"alert_no_internet_connection")
                                                       delegate:nil
                                              cancelButtonTitle:LS(@"try_again")
                                              otherButtonTitles:nil];

    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {

        if (buttonIndex == 0) {

            [self restartHUD];
            [[TVConfigurationManager sharedTVConfigurationManager] downloadConfigurationFile];

        }


    }];
}

-(void) restartApp:(NSNotification *) notification
{
    [self takeOff];
}

@end
