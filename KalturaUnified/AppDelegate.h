//
//  AppDelegate.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 04.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//
 
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,assign) BOOL blockOrientationRotation;
@property (nonatomic,assign) BOOL tryToOpenFBSession;
@property (copy) void (^backgroundSessionCompletionHandler)();
+ (AppDelegate *)sharedAppDelegate;

@end

