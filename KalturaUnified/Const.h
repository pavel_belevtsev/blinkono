//
//  Const.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 10.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#ifndef KalturaUnified_Const_h
#define KalturaUnified_Const_h

#import "FeaturesMap.h"
#import "APSTypography.h"
#import "LoginManager.h"
#import "MenuManager.h"
#import "NavigationManager.h"
#import "EPGManager.h"
#import "IAPManager.h"
#import "SubscriptionManager.h"
#import "PurchasesManager.h"
#import "PPVManager.h"

#import "DeepLinkingManager.h"
#import "DeepLinkingAction.h"

#import "ParentalAdapter.h"
#import "MediaGroupRulesAdapter.h"
#import "PlayerCustomDataAdapter.h"

#import "Utils.h"
#import "BaseViewController.h"
#import "UINibView.h"
#import "MFSideMenu.h"
#import "FBManagement.h"

#import "APSTVProgram+Time.h"
#import "ViewHelper.h"

#import "ViewControllersFactory.h"
#import "Unifide-Swift.h"
#import "APSTVProgram+MetaDataTagsHandling.h"

#define kViewNavigationDuration     0.5
#define kViewAnimationDuration     0.3
#define kViewMyZoneWidth            1024.0
#define kViewMyZoneHeight           380.0
#define kViewCloseMyZoneTag         99

#define LS(S)   NSLocalizedString(S, nil)
#define CS(S)   [Utils colorFromString:S]

#define FM              [FeaturesMap instance]
#define FBM             [FBManagement instance]
#define APST            [APSTypography instance]
#define LoginM          [LoginManager instance]
#define MenuM           [MenuManager instance]
#define NavM            [NavigationManager instance]
#define EPGM            [EPGManager instance]
#define DeepLinkingM    [DeepLinkingManager instance]
#define AnalyticsM      [AnalyticsManager instance]
#define IAP             [IAPManager instance]
#define SubscriptionM   [SubscriptionManager instance]
#define PPVM            [PPVManager instance]
#define PurchasesM      [PurchasesManager instance]
#define ParentalA       [ParentalAdapter instance]
#define MediaGRA        [MediaGroupRulesAdapter instance]
#define PlayerCDA       [PlayerCustomDataAdapter instance]

#define Use_Vodafone_Branding

#ifdef Use_Vodafone_Branding
#define FONT_REGULAR    @"VodafoneRg-Regular"
#define FONT_BOLD       @"VodafoneRg-Bold"
#define FONT_SEMIBOLD   @"VodafoneRg-Bold"
#define FONT_REGITALIC  @"VodafoneLt-Regular"
#else

#define FONT_REGULAR    @"ProximaNova-Regular"
#define FONT_BOLD       @"ProximaNova-Bold"
#define FONT_SEMIBOLD   @"ProximaNova-SemiBold"
#define FONT_REGITALIC  @"ProximaNova-RegularIt"
#endif

#define Font_Bold(SIZE) [UIFont fontWithName:FONT_BOLD size:SIZE]
#define Font_Reg(SIZE) [UIFont fontWithName:FONT_REGULAR size:SIZE]
#define Font_Semibold(SIZE) [UIFont fontWithName:FONT_SEMIBOLD size:SIZE]
#define Font_RegItalic(SIZE) [UIFont fontWithName:FONT_REGITALIC size:SIZE]

#define k_signUp_OK 0
#define k_signUp_UserExists 1
#define k_signUp_UserNotExists 2
#define k_signUp_UserNotActivated 6

#define isPhone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define isPad   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define ConstKeyMenuItemType        @"Type"
#define ConstKeyMenuItemIcon        @"Icon"
#define ConstKeyMenuItemChannel     @"ChannelID"

#define BlurRadius  12.0

#define optionButtonIconTag 101
#define optionButtonLabelTag 102

#define sortRowHeight   (isPhone ? 34.0 : 44.0)

#define SORT_ASC    0
#define SORT_DESC   1

#define seasonListOrdering              SORT_DESC
#define episodeListOrdering             SORT_DESC

#define IS_IOS_7_OR_LATER ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] floatValue] >= 7.0)
#define IS_IOS_8_OR_LATER ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] floatValue] >= 8.0)

#define maxPageSize (APST.pageSize)


#define kEPGFlagStartOver @"startover"
#define kEPGFlagCatchup @"catchupInHome"

#endif
