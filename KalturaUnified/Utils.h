//
//  Utils.h
//  KalturaUnified
//
//  Created by Pavel Belevtsev on 11.11.14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TvinciSDK/TVMediaItem.h>


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface Utils : NSObject

+ (CGFloat)colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length;
+ (UIColor *)colorFromString:(NSString *)colorString;
+ (void)setButtonFont:(UIButton *)button;
+ (void)setButtonFont:(UIButton *)button bold:(BOOL)bold;
+ (void)setButtonFont:(UIButton *)button semibold:(BOOL)semibold;
+ (void)setLabelFont:(UILabel *)label;
+ (void)setLabelFont:(UILabel *)label bold:(BOOL)bold;
+ (void)setLabelFont:(UILabel *)label size:(CGFloat)size bold:(BOOL)bold;
+ (void)setTextFieldFont:(UITextField *)textField;
+ (void)setTextFieldFont:(UITextField *)textField bold:(BOOL)bold;
+ (void)setTextFieldFont:(UITextField *)textField size:(CGFloat)size bold:(BOOL)bold;
+ (void)setTextViewFont:(UITextView *)textView;
+ (void)setTextViewFont:(UITextView *)textView bold:(BOOL)bold;
+ (void)setTextViewFont:(UITextView *)textView size:(CGFloat)size bold:(BOOL)bold;
+ (BOOL)NSStringIsValidEmail:(NSString *)checkString;
+ (BOOL)NSStringIsOnlyDigitsAndChars:(NSString *)checkString;
+ (NSString *)nibName:(NSString *)name;
+ (NSURL *)getBestPictureURLForMediaItem:(TVMediaItem *)mediaItem andSize:(CGSize)size;
+ (CGSize)getBestPictureSizeForMediaItem:(TVMediaItem *)mediaItem andSize:(CGSize)size;
+ (CGRect)getLabelFrame:(UILabel *)label maxHeight:(float)maxHeight minHeight:(float)minHeight;
+ (void)initButtonUI:(UIButton *)brandButton title:(NSString *)title backGroundColor:(UIColor *)color titleColor:(UIColor *)titleColor;
+ (NSString *)getSeriesName:(TVMediaItem *)mediaItem;
+ (NSDictionary *) mapEpisodesBySeasonsNumber:(NSArray *) episodes;
+ (NSArray *)sortSeasonsNumbers:(NSArray *) seasons;
+ (NSString *)stringFromSocialAction:(TVUserSocialActionDoc *)socialAction;
+ (NSString *)getActionDurationTimeFromNow:(NSDate *)addedDate;
+ (void) getDurationTimeFromNow:(NSDate *)addedDate completion:(void(^)(NSInteger number, NSString *unit, NSString *altValue))completion;
+ (NSString *)getStringFromInteger:(NSInteger) value;
+ (NSString *)convertTextToHTML:(NSString *)str;
+ (NSString *) md5:(NSString*) string;
+ (void) getDurationTimeFromDate:(NSDate *)startDate toDate:(NSDate *)endDate completion:(void(^)(NSInteger number, NSString *unit, NSString *altValue))completion;

+(BOOL)canTrickPlayCachupToProgram:(APSTVProgram *) program;
/*”PSC : ONO : OS-91 Ono - CR - Rating needs ot be presented in each media page */
+(NSString *)ratingToShow:(NSArray*) raitings;

@end
