
UIATarget.onAlert = function onAlert(alert) {
      var title = alert.name();
      UIALogger.logWarning("Alert with title ’" + title + "’ encountered!");
      
      if (title == "Salir") {
      	alert.buttons()["Salir"].tap();
      	return true; 
      }

	return false;
}

var target = UIATarget.localTarget();

target.delay(10);

var buttonMenu = target.frontMostApp().mainWindow().buttons()["Button Menu Home"];

if (buttonMenu.checkIsValid()) {
    
    target.frontMostApp().mainWindow().buttons()["Button Menu Home"].tap();
	target.frontMostApp().mainWindow().scrollViews()[0].buttons()["Salir"].tap();

	target.delay(5);

}
    
    var buttonLogin = target.frontMostApp().mainWindow().buttons()["Button Login Start"];

	if (buttonLogin.checkIsValid()) {

		buttonLogin.tap();
    
    	target.delay(2);
    
    	target.frontMostApp().mainWindow().textFields()["Text Field Login"].tap();
    	target.frontMostApp().keyboard().typeString("Israel.berezin1@kaltura.com\n");
    
    	target.frontMostApp().keyboard().typeString("123456\n");
    
    	target.frontMostApp().mainWindow().buttons()["Button Login"].tap();
    
    	target.delay(10);
    
    	var button = target.frontMostApp().mainWindow().buttons()["Button Login Start"];
    	if (button.checkIsValid()) {
        	UIALogger.logFail("Login Failure");
        	target.captureScreenWithName("screenshot");
    	} else {
        	UIALogger.logPass("Login Success");
        	target.captureScreenWithName("screenshot");
        
    	}
    
	} else {
	
    	UIALogger.logFail("Login Failure");
       	target.captureScreenWithName("screenshot");
    	
    }


