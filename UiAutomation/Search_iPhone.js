
var target = UIATarget.localTarget();

target.delay(10);

var buttonSkip = target.frontMostApp().mainWindow().buttons()["Button Skip Start"];

if (buttonSkip.checkIsValid()) {
    buttonSkip.tap();
	target.delay(5);
}

target.frontMostApp().mainWindow().buttons()["Button Menu Home"].tap();
target.frontMostApp().mainWindow().textFields()["Text Field Search"].tap();
target.frontMostApp().keyboard().typeString("you\n");

target.delay(10);

var label = target.frontMostApp().mainWindow().staticTexts()["Label Automation"];

if ((label.value() == "") || !label.checkIsValid()) {
    UIALogger.logFail("No Items Found");
    target.captureScreenWithName("screenshot");
} else {
    UIALogger.logPass(label.value());
    target.captureScreenWithName("screenshot");
}
