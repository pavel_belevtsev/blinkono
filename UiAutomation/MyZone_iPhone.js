
UIATarget.onAlert = function onAlert(alert) {
      var title = alert.name();
      UIALogger.logWarning("Alert with title ’" + title + "’ encountered!");
      
      if (title == "Salir") {
      	alert.buttons()["Salir"].tap();
      	return true; 
      }

	return false;
}

var target = UIATarget.localTarget();

target.delay(10);

var buttonMenu = target.frontMostApp().mainWindow().buttons()["Button Menu Home"];

if (buttonMenu.checkIsValid()) {
    
    target.frontMostApp().mainWindow().buttons()["Button Menu Home"].tap();
	target.frontMostApp().mainWindow().scrollViews()[0].buttons()["Salir"].tap();

	target.delay(5);

}

target.frontMostApp().mainWindow().buttons()["Button Skip Start"].tap();

target.delay(5);

target.frontMostApp().mainWindow().buttons()["Button Avatar Home"].tap();

target.delay(2);

var label = target.frontMostApp().mainWindow().staticTexts()["Label Automation"];

if ((label.value() == "") || !label.checkIsValid()) {
    UIALogger.logFail("No MyZone Login");
    target.captureScreenWithName("screenshot");
} else {
    UIALogger.logPass(label.value());
    target.captureScreenWithName("screenshot");
}
