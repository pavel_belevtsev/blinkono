//
//  TVSubscription.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 9/3/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

extern NSString *const TVSubscriptionIsRenewableKey;
extern NSString *const TVSubscriptionEndDateKey;
extern NSString *const TVSubscriptionLastViewDateKey;
extern NSString *const TVSubscriptionPurchaseDateKey;
extern NSString *const TVSubscriptionCurrentUsesKey;
extern NSString *const TVSubscriptionMaxUsesKey;
extern NSString *const TVSubscriptionSubscriptionPurchaseIDKey;
extern NSString *const TVSubscriptionPaymentMethodKey;
extern NSString *const TVSubscriptionSubscriptionCodeKey;
extern NSString *const TVSubscriptionDeviceNameKey;
extern NSString *const TVSubscriptionDeviceUDIDKey;
extern NSString *const TVSubscriptionCurrentDate;
extern NSString *const TVSubscriptionNextRenewalDate;
extern NSString *const TVSubscriptionRecurringStatus;

#import "BaseModelObject.h"

@interface TVSubscription : BaseModelObject
@property (nonatomic, assign) BOOL isRenewable;
@property (nonatomic, retain) NSDate *endDate;
@property (nonatomic, retain) NSDate *lastViewDate;
@property (nonatomic, retain) NSDate *purchaseDate;
@property (nonatomic, assign) NSInteger currentUses;
@property (nonatomic, assign) NSInteger maxUses;
@property (nonatomic, assign) NSInteger subscriptionPurchaseID;
@property (nonatomic, assign) NSInteger paymentMethod;
@property (nonatomic, assign) NSInteger subscriptionCode;
@property (nonatomic, copy) NSString *deviceName;
@property (nonatomic, copy) NSString *deviceUDID;
@property (nonatomic, copy) NSString *objectVirtualName;

@property (nonatomic, retain) NSDate *currentDate;
@property (nonatomic, retain) NSDate *nextRenewalDate;

@property (nonatomic, assign) BOOL recurringStatus;
@property (nonatomic, assign) BOOL cancelWindow;



@end
