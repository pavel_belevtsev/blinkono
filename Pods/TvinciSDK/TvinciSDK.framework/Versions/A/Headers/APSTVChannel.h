//
//  APSTVChannel.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/16/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "APSBaseManagedObject.h"

@class APSTVProgram;

@interface APSTVChannel : APSBaseManagedObject

@property (nonatomic, retain) NSString * channelID;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * pictureURL;
@property (nonatomic, retain) NSString * channelDescription;
@property (nonatomic, retain) NSString * orderNumber;
@property (nonatomic, retain) NSString * isActive;
@property (nonatomic, retain) NSString * groupID;
@property (nonatomic, retain) NSString * editorRemarks;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * updaterID;
@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSDate * publishDate;
@property (nonatomic, retain) NSString * mediaID;
@property (nonatomic, retain) NSString * epgChannelID;
@property (nonatomic, retain) NSSet *programs;
@end

@interface APSTVChannel (CoreDataGeneratedAccessors)

+ (APSTVChannel *) channelWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;


- (void)addProgramsObject:(APSTVProgram *)value;
- (void)removeProgramsObject:(APSTVProgram *)value;
- (void)addPrograms:(NSSet *)values;
- (void)removePrograms:(NSSet *)values;

@end
