//
//  UIButton+ImageResize.h
//  TvinciSDK
//
//  Created by Quickode Ltd. on 10/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+Resize.h"

@interface UIButton (ImageResize)

-(void) stretchBackgroundImageUsing1X1;
-(void) stretchSelectedBackgroundImageUsing1X1;
-(void) stretchBackgroundImageUsing1XHeight;

@end
