//
//  TVDomainLimitationModule.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVDomainLimitation : BaseModelObject

@property (assign, nonatomic) NSInteger concurrency;
@property (assign, nonatomic) NSInteger quantity;
@property (assign, nonatomic) NSInteger deviceFrequency;
@property (assign, nonatomic) NSInteger domainLimitationID;
@property (strong, nonatomic) NSString * domainLimitationName;
@property (assign, nonatomic) NSInteger npvrQuotaInSeconds;
@property (assign, nonatomic) NSInteger userLimit;
@property (assign, nonatomic) NSInteger userFrequency;
@property (strong, nonatomic) NSString * userFrequencyDescription;
@property (strong, nonatomic) NSString * deviceFrequencyDescription;
@property (assign, nonatomic) NSInteger frequency;


@property (strong, nonatomic) NSArray * arrayFamilyLimitations;



@end
