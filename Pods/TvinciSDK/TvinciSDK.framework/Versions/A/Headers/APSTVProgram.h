//
//  APSTVProgram.h
//  Spas
//
//  Created by Rivka Peleg on 9/29/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "APSBaseManagedObject.h"

@class APSTVChannel;

@interface APSTVProgram : APSBaseManagedObject

@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSDate * endDateTime;
@property (nonatomic, retain) NSString * epgChannelID;
@property (nonatomic, retain) NSString * epgId;
@property (nonatomic, retain) NSString * epgIdentifier;
@property (nonatomic, retain) NSString * epgTag;
@property (nonatomic, retain) NSString * groupID;
@property (nonatomic, retain) NSString * isActive;
@property (nonatomic, retain) NSNumber * likeConter;
@property (nonatomic, retain) NSString * mediaID;
@property (nonatomic, retain) NSDictionary  * metaData;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * pictureURL;
@property (nonatomic, retain) NSString * programDescription;
@property (nonatomic, retain) NSDate * publishDate;
@property (nonatomic, retain) NSDate * startDateTime;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSDictionary * tags;
@property (nonatomic, retain) NSDate * updateDate;
@property (nonatomic, retain) NSString * updaterID;
@property (nonatomic, retain) NSDate * insertionDate;
@property (nonatomic, retain) APSTVChannel *channel;

@end
