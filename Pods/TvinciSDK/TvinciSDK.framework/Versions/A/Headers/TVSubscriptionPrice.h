//
//  TVSubscriptionPrice.h
//  TvinciSDK
//
//  Created by Alex Zchut on 2/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TvinciSDK.h"

@class PriceObj;
@interface TVSubscriptionPrice : BaseModelObject {
    
}

@property (nonatomic, assign) NSInteger priceReason;
@property (nonatomic, retain) NSString *subscriptionCode;
@property (nonatomic, retain) PriceObj *price;

@end
