//
//  TVLinearLayout.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 7/12/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum 
{
    TVLayoutOrientationHorizontal = 0,
    TVLayoutOrientationVertical,
    TVLayoutOrientationRTLHorizontally
}TVLayoutOrientation;

@interface TVLinearLayout : UIView
@property (nonatomic, assign) CGFloat margins;
@property (nonatomic, assign) TVLayoutOrientation orientation;
@end
