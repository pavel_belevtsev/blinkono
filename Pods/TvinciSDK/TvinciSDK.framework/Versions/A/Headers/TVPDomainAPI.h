//
//  TVPDomainAPI.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/29/13.
//  Copyright (c) 2013 Quickode. All rights reserved.

//

#import "TVConstants.h"
#import "BaseTVPClient.h"

@interface TVPDomainAPI : BaseTVPClient


+(TVPAPIRequest *) requestForAddDeviceToDomainWithDeviceName : (NSString *) deviceName
                                               deviceBrandID : (NSInteger) deviceBrandID
                                                    delegate : (id<ASIHTTPRequestDelegate>) delegate;

//+(TVPAPIRequest *) requestForAddDeviceToDomainWithMasterSiteGuid: (NSString *) siteGuid
//                                                 WithDeviceName : (NSString *) deviceName
//                                                  deviceBrandID : (NSInteger) deviceBrandID
//                                                       delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForRemoveDeviceFromDomainWithDeviceName : (NSString *) deviceName
                                                    deviceBrandID : (NSInteger) deviceBrandID
                                                          domainID: (NSInteger) domainID
                                                         delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForAddUserToDomainIsMaster : (BOOL) isMaster
                                             domainId:(NSInteger) domainID
                                             userGuid:(NSString *) userGuid
                                            delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForAddUserToDomainWithDomainId : (NSInteger) domainID
                                                userGuid : (NSString *) userGuid
                                           masteUserGuid : (NSString *) masterUserGuid
                                                delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForRemoveUserFromDomainIsMaster : (BOOL) isMaster
                                                  domainId:(NSInteger) domainID
                                                  userGuid:(NSString *) userGuid
                                                 delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForGetDeviceDomains : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetDomainInfo : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForAddDomainWithDomainName:(NSString *)domainName
                                   domainDescription:(NSString *)domainDescription
                                          masterGuid:(int)masterGuid
                                            delegate:(id<ASIHTTPRequestDelegate>)delegate __attribute__((deprecated));




#pragma Houshold API methods
+(TVPAPIRequest *) requestForRemoveDomainWithDomainID:(NSInteger)domainID
                                             delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForSendPasswordMailWithUsername:(NSString *)username
                                                 delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetDomainByCoGuid:(NSString *)coGuid
                                      delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForAddDomainWithCoGuid:(NSString *)coGuid
                                      domainName:(NSString *)domainName
                                      domainDesc:(NSString *)domainDesc
                                      masterGuid:(NSString *)masterGuid
                                        delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetDomainIDByCoGuid:(NSString *)coGuid
                                        delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForSubmitAddUserToDomainWithMasterUsername:(NSString *)masterUsername
                                                            delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForActivateAccountByDomainMaster:(NSString *)masterUsername
                                                  username:(NSString *)username
                                                     token:(NSString *)token
                                                  delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForSubmitAddDeviceToDomainRequestWithDeviceName:(NSString *)deviceName
                                                            deviceBrandId:(NSInteger)deviceBrandId
                                                                 delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForMethodNameGetDeviceInfoWithDeviceUDID:(NSString *)deviceUdid
                                                            isUdid:(BOOL)isUdid
                                                          delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForAddHomeNetworkToDomainWithNetworkId:(NSString *)networkId
                                                     networkName:(NSString *)networkName
                                              networkDescription:(NSString *)networkDesc
                                                        delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetDomainHomeNetworksWithdelegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForRemoveDomainHomeNetworkWithNetworkId:(NSString *)networkId
                                            delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForUpdateDomainHomeNetworkWithNetworkId :(NSString *)networkId
                                         networkName : (NSString *)networkName
                                         networkDescription : (NSString *)networkDesc
                                            isActive : (BOOL) isActive
                                            delegate : (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForConfirmDeviceByDomainMasterWithUdid :(NSString *)udid
                                                        masterUn : (NSString *)masterUn
                                                           token : (NSString *)token
                                                        delegate : (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForChangeDomainMasterWithCurrentMasterID  : (NSInteger)currentMasterID
                                                        newMasterID : (NSInteger)newMasterID
                                                           delegate : (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForResetDomainFrequencyWithFrequencyType  : (FrequencyType)frequencyType
                                                           delegate : (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForSubmitAddDeviceToDomainRequestWithSpecificSiteGuid : (NSString *) masterUserGuid
                                                            andSpecificDomainID : (NSInteger)domainID
                                                                 WithDeviceName : (NSString *)deviceName
                                                                  deviceBrandId : (NSInteger)deviceBrandId
                                                                       delegate : (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetDomainLastPosition :(NSString *) mediaID
                                          delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetDomainLimitationModuleWithDomainLimitationID :(NSInteger ) limitationID
                                                                    delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSuspendDomain:(NSInteger) domainID;

+(TVPAPIRequest *) requestForResumeDomain:(NSInteger) domainID;

+(TVPAPIRequest *) requestForGetRegions:(NSArray *) regionIds;


+(TVPAPIRequest *) requestForSetDomainRegionWithDomainID:(NSInteger ) domainID
                                        externalRegionID:(NSString *) regionID
                                               lookupKey:(NSString *) lookupKey;

extern NSString * const MethodNameGetDomainLastPosition;
extern NSString * const MethodNameSetDomainInfo;
extern NSString * const MethodNameAddDeviceToDomain;
extern NSString * const MethodNameAddUserToDomain;
extern NSString * const MethodNameRemoveDeviceFromDomain;
extern NSString * const MethodNameRemoveUserFromDomain;
extern NSString * const MethodNameGetDeviceDomains;
extern NSString * const MethodNameGetDomainInfo;
extern NSString * const MethodNameAddDomain;
extern NSString * const MethodNameActivateAccountByDomainMaster;
extern NSString * const MethodNameSubmitAddUserToDomainRequest;
extern NSString * const MethodNameAddDomainWithCoGuid;
extern NSString * const MethodNameGetDomainIDByCoGuid;
extern NSString * const MethodNameGetDomainByCoGuid;
extern NSString * const MethodNameSendPasswordMail;
extern NSString * const MethodNameRemoveDomain;
extern NSString * const MethodNameSubmitAddDeviceToDomainRequest;
extern NSString * const MethodNameGetDeviceInfo;
extern NSString * const MethodNameResetDomainFrequency;

extern NSString * const MethodNameAddHomeNetworkToDomain;
extern NSString * const MethodNameGetDomainHomeNetworks;
extern NSString * const MethodNameRemoveDomainHomeNetwork;
extern NSString * const MethodNameUpdateDomainHomeNetwork;
extern NSString * const MethodNameConfirmDeviceByDomainMaster;
extern NSString * const MethodNameChangeDomainMaster;
@end
