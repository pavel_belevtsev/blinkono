//
//  APSMediaItem.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TVMediaItem.h"
#import "APSBaseManagedObject.h"

@class APSImage;

@interface APSMediaItem : APSBaseManagedObject

@property (nonatomic, retain) NSDate * catalogStartDate;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) id externalIDs;
@property (nonatomic, retain) NSString * fileID;
@property (nonatomic, retain) id files;
@property (nonatomic, retain) NSNumber * likeCounter;
@property (nonatomic, retain) NSString * mediaDescription;
@property (nonatomic, retain) NSString * mediaID;
@property (nonatomic, retain) NSString * mediaName;
@property (nonatomic, retain) NSString * mediaTypeID;
@property (nonatomic, retain) NSString * mediaTypeName;
@property (nonatomic, retain) NSString * mediaWebLink;
@property (nonatomic, retain) id metaData;
@property (nonatomic, retain) NSString * pictureURL;
@property (nonatomic, retain) NSNumber * rating;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) id tags;
@property (nonatomic, retain) NSNumber * totalItems;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * viewCounter;
@property (nonatomic, retain) id plainJson;
@property (nonatomic, retain) NSSet *pictures;
@end

@interface APSMediaItem (CoreDataGeneratedAccessors)

+ (APSMediaItem *) mediaWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;
- (TVMediaItem *) tvMediaItem;

- (void)addPicturesObject:(APSImage *)value;
- (void)removePicturesObject:(APSImage *)value;
- (void)addPictures:(NSSet *)values;
- (void)removePictures:(NSSet *)values;

@end
