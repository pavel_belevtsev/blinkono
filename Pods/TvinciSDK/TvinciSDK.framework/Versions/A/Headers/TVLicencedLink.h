//
//  TVLicencedLink.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVLicencedLink : BaseModelObject

@property (strong, nonatomic) NSURL * mainURL;
@property (strong, nonatomic) NSURL * alternativeURL;
@end
