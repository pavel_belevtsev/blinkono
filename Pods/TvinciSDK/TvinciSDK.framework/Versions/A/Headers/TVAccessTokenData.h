//
//  TVAccessTokenData.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/26/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVAccessTokenData : BaseModelObject

@property (strong, nonatomic) NSString * accessToken;
@property (strong, nonatomic) NSString * refreshToken;
@property (strong, nonatomic) NSDate * expiration;
@end
