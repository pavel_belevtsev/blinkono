//
//  TvpAPIRequest.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "ASIHTTPRequest.h"
#import "TVInitObject.h"

@interface TVPAPIRequest : ASIHTTPRequest




@property (nonatomic , retain, readonly) NSMutableDictionary *postParameters;
@property (nonatomic , retain, readonly) id JSONResponse;
@property (nonatomic , assign) BOOL doNotUseInitObj;
@property (nonatomic, retain) id context;
@property (nonatomic, assign) BOOL ignoreToken;

-(id)initRequestFor:(NSString *)functionName WithCustomInitObject:(TVInitObject *)customInitObject AndOtherPostData:(NSDictionary *)postData;
-(id)initBasicRequestFor:(NSURL *)customUrl;

-(id) bakedJSONResponse;
-(id) JSONResponseJSONOrString;

-(NSString *) debugPostBudyString __attribute__((deprecated));
-(NSString *) debugPostBodyString;


+(void) addFailedStatusCodes:(NSInteger) statusCode;
+(void) removeFailedStatusCodes:(NSInteger) statusCode;

@end

extern NSString *const TVPAPIRerquestErrorMessageKey;
