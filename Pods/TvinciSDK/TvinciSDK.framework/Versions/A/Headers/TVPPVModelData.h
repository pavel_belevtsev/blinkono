//
//  TVPPVModelData.h
//  TvinciSDK
//
//  Created by Israel Berezin on 6/3/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

#import "JSONModel.h"

//m_sCodes
@protocol TVPPVData_codes @end
@interface TVPPVData_codes : JSONModel

@property (nonatomic, readwrite) NSInteger code;
@property (nonatomic, retain) NSString *name;

@end

//m_sName
@protocol TVPPVData_name @end
@interface TVPPVData_name : JSONModel

@property (nonatomic, retain) NSString *languageCode3, *value;

@end

//m_oPrise > m_oCurrency
@interface TVPPVData_prise_currency : JSONModel

@property (nonatomic, retain) NSString *currencyCD3, *currencyCD2, *currencySign;
@property (nonatomic, readwrite) NSInteger currencyId;

@end

//m_oPrise
@interface TVPPVData_prise : JSONModel

@property (nonatomic, readwrite) CGFloat price;
@property (nonatomic, retain) NSString<Optional> *name;
@property (nonatomic, retain) TVPPVData_prise_currency *currency;

@end

//m_oPPVPriceCode
@interface TVPPVData_PPVPriceCode : JSONModel

@property (nonatomic, readwrite) NSInteger code;
@property (nonatomic, retain) NSString<Optional> *name;
@property (nonatomic, retain) TVPPVData_prise *prise;
@property (nonatomic, readwrite) NSInteger objectId;
@property (nonatomic, retain) NSString<Optional> *description1;

@end

//m_oPPVPriceCode
@interface TVPPVData_PPVUsageModule : JSONModel

@property (nonatomic, readwrite) NSInteger waiver, couponId, deviceLimitId, extDiscountId, internalDiscountId, isRenew, maxNumberOfViews, objectId, waiverPeriod, numOfRecPeriods, pricingId, maxUsageModuleLifeCycle, viewLifeCycle, type;
@end

//m_oPPVPriceCode
@interface TVPPVData_purchasingInfo : JSONModel

@property (nonatomic, readwrite) NSInteger PPVCode, maxUses, currentUses, paymentMethod, PPVPurchaseId;
@property (nonatomic, retain) NSString *stringCurrentDate, *stringLastViewDate, *stringPurchaseDate, *stringEndDate;
@property (nonatomic, readwrite) BOOL recurringStatus, isSubRenewable;
@property (nonatomic, retain) NSString<Optional> *deviceUDID, *deviceName;

@property (nonatomic, retain, readonly) NSDate<Ignore> *lastViewDate, *currentDate, *purchaseDate, *endDate;

@end

@class TVMediaItem;

@interface TVPPVModelData : JSONModel

@property NSInteger firstDeviceLimitation;
@property (nonatomic, retain) NSString *productCode;
@property (nonatomic, readwrite) BOOL isSubscriptionOnly;
@property (nonatomic, retain) NSDictionary *couponsGroup;
@property (nonatomic, retain) NSDictionary<Optional> *discountModule;
@property (nonatomic, retain) TVPPVData_PPVPriceCode *PPVPriceCode;
@property (nonatomic, retain) TVPPVData_PPVUsageModule *PPVUsageModule;
@property (nonatomic, retain) NSArray<Optional> * relatedFileTypes;
@property (nonatomic, retain) NSArray<TVPPVData_name ,ConvertOnDemand> * arrDescription;
@property (nonatomic, readwrite) NSInteger objectCode;
@property (nonatomic, retain) NSString *objectVirtualName;

@end
