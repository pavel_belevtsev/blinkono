//
//  APSBaseManagedObject.h
//  Spas
//
//  Created by Rivka Peleg on 11/18/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <CoreData/CoreData.h>

#define NSStringFromProperty(prop) NSStringFromSelector(@selector(prop))

@interface APSBaseManagedObject : NSManagedObject

@end
