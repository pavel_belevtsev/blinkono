//
//  TVSocialUtils.h
//  TvinciSDK
//
//  Created by Tarek Issa on 1/15/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

//#import "BaseNetworkObject.h"
#import "ASIHTTPRequest.h"

@class TVMediaItem;
@class TVPAPIRequest;
@class ASIHTTPRequestDelegate;

@interface TVSocialUtils : NSObject

+ (TVPAPIRequest *)RequestForDoLikeForMediaItem : (TVMediaItem*)mediaItem delegate : (id<ASIHTTPRequestDelegate>) delegate;

@end
