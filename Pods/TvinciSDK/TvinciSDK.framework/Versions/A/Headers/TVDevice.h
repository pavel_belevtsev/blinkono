//
//  TVDevice.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

typedef enum
{
	TVDeviceStateUnknown = -1,
    TVDeviceStateNotRegistered,
	TVDeviceStateActivated = 4,
	TVDeviceStateNotActivated = 5,
} TVDeviceState;

@interface TVDevice : BaseModelObject
@property (nonatomic, assign) TVDeviceState deviceState;
@property (nonatomic, assign) NSInteger deviceID;
@property (nonatomic, copy) NSString *UDID;
@property (nonatomic, copy) NSString *familyName;
@property (nonatomic, assign) NSInteger deviceFamilyID;
@property (nonatomic, copy) NSString *deviceName;
@property (nonatomic, copy) NSString *brandName;
@property (nonatomic, copy) NSString *devicePin;
@property (nonatomic, assign) NSInteger brandID;
@property (nonatomic, assign) NSInteger domainID;
@property (nonatomic, copy) NSString *activationDate;

@end
