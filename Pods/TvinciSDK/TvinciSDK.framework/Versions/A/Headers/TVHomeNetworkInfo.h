//
//  TVHomeNetworkInfo.h
//  TvinciSDK
//
//  Created by Israel Berezin on 4/9/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

/* 
 CreateDate = "2014-04-09T05:57:15.543";
 Description = "very good home network";
 IsActive = 1;
 Name = "Tvinci network";
 UID = 0987654321;
 */

typedef enum
{
    HomeNetworkStatus_OK =0,
    HomeNetworkStatus_QuantityLimitation,
    HomeNetworkStatus_FrequencyLimitation,
    HomeNetworkStatus_NetworkExists,
    HomeNetworkStatus_NetworkDoesNotExist,
    HomeNetworkStatus_InvalidInput,
    HomeNetworkStatus_Error
    
}HomeNetworkStatus;

NSString * TVNameHomeNetworkStatus(HomeNetworkStatus action);

@interface TVHomeNetworkInfo : BaseModelObject

@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSString * networkDescription;
@property (nonatomic, retain ) NSString * networkName;
@property BOOL isActive;
@property (nonatomic, retain ) NSString * uid;

@end
