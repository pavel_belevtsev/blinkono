//
//  NSArray+Equivalent.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/15/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Equivalent)

-(BOOL) isEquivalent:(NSArray *) array;
@end
