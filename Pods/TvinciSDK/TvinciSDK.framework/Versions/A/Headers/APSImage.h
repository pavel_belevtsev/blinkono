//
//  APSImage.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "APSBaseManagedObject.h"

@class APSMediaItem;

@interface APSImage : APSBaseManagedObject

@property (nonatomic, retain) NSString * size;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSNumber * direction;
@property (nonatomic, retain) NSNumber * ratio;
@property (nonatomic, retain) APSMediaItem *mediaItem;

+ (APSImage *) imageWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;

@end
