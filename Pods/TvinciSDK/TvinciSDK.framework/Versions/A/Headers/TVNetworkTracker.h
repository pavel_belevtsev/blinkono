//
//  TVNetworkStatusManager.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 8/7/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>


#define NotificationName_NetworkStatusChanged @"Network Status changed"

typedef enum TVNetworkStatus
{
    TVNetworkStatus_NoConnection,
    TVNetworkStatus_WiFi,
    TVNetworkStatus_3G
    
}TVNetworkStatus;

@interface TVNetworkTracker : NSObject



+ (TVNetworkTracker *) sharedNetworkStatusManager;
-(void) startTracking;
-(void) stopTracking;
-(void) updateStatus;
@property (assign, nonatomic) TVNetworkStatus status;

@end
