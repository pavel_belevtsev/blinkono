//
//  TVWatchListItem.h
//  TvinciSDK
//
//  Created by Tarek Issa on 12/16/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"





@interface TVWatchListItem : BaseModelObject {
    
}


@property (retain, readonly) NSString *siteGuid;
@property (retain, readonly) NSArray *items;
@property (assign, readonly) TVListItemType itemType;
@property (assign, readonly) TVListType listType;


@end


@interface ItemObj : BaseModelObject {
    
}

@property (assign, nonatomic) int order;
@property (retain, nonatomic) NSString* itemID;

@end
