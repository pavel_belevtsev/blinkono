//
//  NSCoder+NSNullAvoidance.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 8/29/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCoder (NSNullAvoidance)

-(id) decodeObjectOrNilForKey:(id)aKey;
-(void) encodeObjectOrNil : (id) object forKey : (id) key;
@end
