//
//  TBCoreDataStore.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/17/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APSCoreDataStore : NSObject





+ (NSManagedObjectContext *)newMainQueueContext;
+ (NSManagedObjectContext *)newPrivateQueueContext;
+ (NSManagedObjectContext *)defaultPrivateQueueContext;


+ (instancetype) defaultStore ;
-(NSPersistentStoreCoordinator *)persistentStoreCoordinator;
-(NSManagedObjectModel *)managedObjectModel;



@end
