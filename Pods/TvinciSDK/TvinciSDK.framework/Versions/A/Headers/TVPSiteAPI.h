//
//  TVPSiteAPI.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseTVPClient.h"
#import "TVConstants.h"
@class TVUser;

/*!
 @interface TVPSiteAPI
 @abstract A Client of the TVinci Site API, providing convinient methods to preform all the available calls to the API
 @discussion You should use this class as much as possible to make calls to the TVP API.
 The reason for that is to ensure that all the parameters are of the right type, the request URL is correct and so on.
 
 You build a request by using one of the API methods that returns TVPAPIRequest object.
 Once you have a request object you can configure it to fill your needs, like changing the timeout, specifiying your own
 callback blocks and selector and so on.
 
 It is not recommended to change the paramaters of the request returned by one of the API methods.
 
 @availability 1.0
 */
@interface TVPSiteAPI : BaseTVPClient


#pragma mark - API Requests




#pragma mark - FB methods








/*!
    @functiongroup Session Related functions
 */


/**
*  Sign in with encrippted token
*
*  @param token - The enctipted token for the sign in process
*
*  @return A request with the specified parameters and a URL for the requested method.
*  @throws NSInvalidArgumentException if token is nil
*/
+(TVPAPIRequest *) requestforSignInWithToken:(NSString *) token
                                   delegate :(id<ASIHTTPRequestDelegate>) delegate;


/*!
 
 @abstract return a request for signing up with new user details
 @param newUser - The new user object. must contain email, first name and last name at least.
 @param password must not be nil.
 @param affiliateCode
 @return A request with the specified parameters and a URL for the requested method.
 @throws NSInvalidArgumentException if either newUser or password are nil
 */




+(TVPAPIRequest *) requestForSignUpWithNewUser : (TVUser *) newUser 
                                      password : (NSString *) password 
                                 affiliateCode : (NSString *) affiliateCode 
                                      delegate : (id<ASIHTTPRequestDelegate>) delegate;







+(TVPAPIRequest *) requestforSigninSecureWithUserName:(NSString *) username
                                             password: (NSString *) password
                                             delegate: (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForSSOSigninWithUserName:(NSString *) username
                                          password: (NSString *) password
                                          delegate: (id<ASIHTTPRequestDelegate>)delegate;

/*!
 @abstract return a request for signing in with specific username and password.
 @param username must not be nil.
 @param password must not be nil.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 @throws NSInvalidArgumentException if either username or password are nil
 */
+(TVPAPIRequest *) requestForSignInWithUserName : (NSString *) username 
                                       password : (NSString *) password 
                                       delegate : (id<ASIHTTPRequestDelegate>) delegate;
/*!
 @abstract return a request for signing out.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForSignOut : (id<ASIHTTPRequestDelegate>) delegate;

/*!
 @abstract return a request for checking if the current user is sign in.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForIsUserSignedIn : (id<ASIHTTPRequestDelegate>) delegate;

/*!
 @abstract return a request for getting the current user details.
 @param siteGUID I have no idea what this parameter is.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForGetUserDetails : (NSString *) siteGUID delegate : (id<ASIHTTPRequestDelegate>) delegate;


/*!
 @abstract return a request for getting the user details by a given username.
 @param username - the username of the user.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForGetUserDetailsByUsername : (NSString *) username delegate : (id<ASIHTTPRequestDelegate>) delegate;

/*!
 @abstract return a request for Get Users Data
 @param siteGuids parameter that represent array of user ids on tvinci.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestforGetUsersData:(NSArray *) siteGuids delegate:(id<ASIHTTPRequestDelegate>)delegate;



/*!
 @abstract return a request for setting user details.
 @param siteGUID parameter that represent the user id on tvinci.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForSetUserDataWithUser:(TVUser *) user
                                        delegate:(id<ASIHTTPRequestDelegate>)delegate;

/*!
 @abstract return a request for checking if the current user is logged in through Facebook.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForGetSiteMap : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_ATTRIBUTE;
+(TVPAPIRequest *) requestForIsFacebookUser : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *)requestForGetMenu : (NSInteger) menuID
                           delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetPageWithID : (NSInteger) pageID 
                               includeMenu : (BOOL) includeMenu 
                             includeFooter : (BOOL) includeFooter DEPRECATED_ATTRIBUTE;

/*!
 @functiongroup Domain Related functions
 */

/*!
 @functiongroup Content Related functions
 */
+(TVPAPIRequest *) requestForGetGalleryWithID : (long) galleryID
                                       atPage : (long) pageID
                                     delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetGalleyContent : (long) galleryID 
                                       atPage : (long) pageID
                                  pictureSize : (CGSize) pictureSize
                                     pageSize : (NSInteger) pageSize     
                               startFromIndex : (NSUInteger) startIndex
                                     delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetGalleryItemContent : (long) itemID
                                         atGallery : (long) galleryID
                                            atPage : (long) pageID
                                       pictureSize : (CGSize) pictureSize
                                          pageSize : (NSInteger) pageSize
                                         pageIndex : (NSInteger) pageIndex
                                           orderBy : (TVOrderBy) orderBy
                                          delegate : (id<ASIHTTPRequestDelegate>) delegate;

/*!
 @functiongroup Social Related functions
 */
+(TVPAPIRequest *) requestForDoSocialAction : (TVSocialAction) action
                                 withMediaId: (NSInteger) mediaID
                                 onPlatform : (TVSocialPlatform) platform
                             additionalInfo : (NSString *) extraInfo 
                                   delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForChangeDeviceDomainStatus : (BOOL) isActive
                                             delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSendNewPassword : (NSString *) username
                                             delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetClearSiteGUIDFromEncryptedSiteGUID : (NSString *) encryptedSiteGUID
                                    delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_ATTRIBUTE;



+(TVPAPIRequest *) requestForGetSiteGuidWithUserName:(NSString *) userName
                                            password:(NSString *) password
                                            delegate:(id<ASIHTTPRequestDelegate>)delegate;



+(TVPAPIRequest *) requestForCleanUserHistoryWithMediaIDs:(NSArray *) mediaIDArray
                                                 delegate:(id<ASIHTTPRequestDelegate>)delegate;


/*!
 @abstract return a request for getting a secured Site Guid.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 @functiongroup Player related functions

 Use the string in the response to set it in TVMediaToPlayInfo object in order
 to acquire rights for encrypted streams.
 */
+(TVPAPIRequest *) requestForGetSecuredSiteGUIDWithDelegate:(id<ASIHTTPRequestDelegate>)delegate;

#pragma mark - facebook
+(TVPAPIRequest *) requestForFBConfigWithDelegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetFBUserDataWithToken:(NSString *) token delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForFBUserMergeWithToken:(NSString *) token facebookID:(NSString *) facebookID username:(NSString *) username password:(NSString *) password  delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForFBUserRegisterWithToken:(NSString *) token
                                     createNewDomain:(BOOL) createNewDomain
                                            delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetOperatorsWithCustomInitObject:(TVInitObject *)customInitObject AndOtherPostData:(NSDictionary *)postData delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetOperatorsWithOperators:(NSArray *) operaors
                                              delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetUserStartedWatchingMediasWithNumOfItems:(NSInteger) numOfItems
                                                               delegate:(id<ASIHTTPRequestDelegate>)delegate;


#pragma Linear Pin

+(TVPAPIRequest *) requestForSetDomainGroupRuleWithRuleID : (NSInteger)ruleID
                                                 isActive : (BOOL) isActive
                                                      pin : (NSString *)pin
                                                 delegate : (id<ASIHTTPRequestDelegate>)delegate;


+(TVPAPIRequest *) requestForGetEPGProgramRulesForMediaID : (NSInteger) mediaId
                                             andProgramID : (NSInteger) programID
                                                    andIP : (NSString *) IP
                                                 delegate : (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetGroupMediaRules:(NSInteger)mediaID
                                       delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForCheckParentalPINWithRuleID:(NSInteger)ruleID
                                                 andPin:(NSString *)pin
                                               delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetDomainGroupRulesWithDelegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForSetRuleStateWithRuleID : (NSInteger)ruleID
                                           isActive : (BOOL) isActive
                                           delegate : (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetGroupOperators : (NSInteger)ruleID
                                         scope : (NSString *)scope
                                      delegate : (id<ASIHTTPRequestDelegate>)delegate;


+(TVPAPIRequest *) requestforRecordAllWithAccountNumber:(NSString *) accountNumber
                                            channelName:(NSString *) channelName
                                       recoredStartDate:(NSString *) recoredStartDate
                                      reconredStartTime:(NSString *) recoredStartTime
                                          EPGIdeitifier:(NSString *) EPGIdeitifier
                                           SerialNumber:(NSString *) serialNumber
                                               delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForFBUserUnmergeWithToken:(NSString *) token
                                           username:(NSString *) username
                                           password:(NSString *) password
                                           delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGenerateDeviceTokenWithAppID:(NSString *) appID
                                                 delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForExchangeDeviceTokenWithAppID:(NSString *) appID
                                                appSecret:(NSString *) appSecret
                                              deviceToken:(NSString *) deviceToken
                                                 delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForRefreshAccessTokenAppID:(NSString *) appID
                                                appSecret:(NSString *) appSecret
                                                refreshToken:(NSString *) refreshToken
                                                 delegate:(id<ASIHTTPRequestDelegate>)delegate;







@end


#pragma mark - Constants

extern NSString * const MethodNameRecordAll;
extern NSString * const MethodNameGetSiteGuid;
extern NSString * const MethodNameChangeDeviceDomainStatus;
extern NSString * const MethodNameDoSocialAction;
extern NSString * const MethodNameGetBottomProfile; // TODO
extern NSString * const MethodNameGetFooter; //TODO
extern NSString * const MethodNameGetGalley;
extern NSString * const MethodNameGetGalleyContent;
extern NSString * const MethodNameGetGalleyItemContent;
extern NSString * const MethodNameGetMenu;
extern NSString * const MethodNameGetPINForDevice; // TODO
extern NSString * const MethodNameGetPage; // TODO
extern NSString * const MethodNameGetPageByToken;// TODO
extern NSString * const MethodNameGetPageGalleries; // TODO
extern NSString * const MethodNameGetSecuredSiteGUID; //TODO
extern NSString * const MethodNameGetSiteGUIDFromEncryptedSiteGUID;
extern NSString * const MethodNameGetSideProfile;// TODO
extern NSString * const MethodNameGetSiteMap;
extern NSString * const MethodNameGetUserCAStatus;//TODO
extern NSString * const MethodNameGetUserDeatils;
extern NSString * const MethodNameIsFacebookUser;
extern NSString * const MethodNameIsUserSignedIn;
extern NSString * const MethodNameRegisterDeviceByPIN;//TODO
extern NSString * const MethodNameSetUserData; // TODO
extern NSString * const MethodNameSignIn;
extern NSString * const methodNameSignInSecure ;
extern NSString * const MethodNameSignOut;
extern NSString * const MethodNameSignUp;
extern NSString * const MethodNameSendNewPassword;
extern NSString * const MethodNameFBConfig ;
extern NSString * const MethodNamegetFBuserData ;
extern NSString * const MethodNameFBUserMerge;
extern NSString * const MethodNameFBUserRegister ;
extern NSString * const MethodNameGetOperators;
extern NSString * const MethodNameCleanUserHistory;
extern NSString * const MethodNameGetEPGProgramRules;
extern NSString * const MethodNameSetRuleState;
extern NSString * const MethodNameGetDomainGroupRules;
extern NSString * const MethodNameSetDomainGroupRule;
extern NSString * const MethodNameGetGroupOperators;
extern NSString * const MethodNameSignInWithToken;
extern NSString * const MethodNaemSignInWithToken;
extern NSString * const MethodNameGetGroupMediaRules;
extern NSString * const MethodNameCheckParentalPIN;
extern NSString * const methodNameSSOSignIn;
extern NSString * const MethodNameFBUserUnmerge;
extern NSString * const MethodNameGenerateDeviceToken;
extern NSString * const methodNameExchangeDeviceToken;
extern NSString * const MethodNameRefreshAccessToken;



