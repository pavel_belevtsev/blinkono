//
//  TVComponentRules.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 5/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModelObject.h"
#import "TVConstants.h"


/**
 *  This class represend a logic unit for a set of controles or behaviur , each unit contains the rule for every situation.
 */

@interface TVUIRulesUnit : BaseModelObject


@property (assign, nonatomic) NSString * UIUnitID;




@property (assign, nonatomic) TVUIRule anonymousRule; // the rule for the controles of this unit when the user is on anonumouse mode
@property (assign, nonatomic) TVUIRule suspendedRule; // the rule for the controles of this unit when the user is suspended
@property (assign, nonatomic) TVUIRule deniedServiceRule; // the rule for the controles of this unit when the user didn't purchased the service related to this unit.
@property (assign, nonatomic) TVUIRule detachedRule; // the rule for the controles of this unit when the application build settings is to detach this the feature related this unit.



@property (assign, nonatomic) TVUIUnitRelatedService relatedService; // The unit has a strong connection with a service , for ex. all the start over buttons connected to the startOver service, when startOver service is denied for the user the controles behave according the deniedServiceRule. ( When user is denied for other services that not related to this unit, they dont have affect on this unit )







@end
