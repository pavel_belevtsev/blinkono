//
//  TVBaseNetworkObject.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/5/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>


@class TVNetworkQueue;
@class TVPAPIRequest;

@interface TVBaseNetworkObject : NSObject

@property (nonatomic, readwrite, retain) TVNetworkQueue *networkQueue;
-(void) sendRequest:(TVPAPIRequest *)request;

@end
