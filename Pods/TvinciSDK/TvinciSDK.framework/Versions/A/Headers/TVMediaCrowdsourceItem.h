//
//  TVMediaCrowdsourceItem.h
//  TvinciSDK
//
//  Created by Amit Bar-Shai on 12/28/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>
#import "TVCrowdsourceItem.h"

@interface TVMediaCrowdsourceItem : TVCrowdsourceItem

@property (nonatomic, assign) NSInteger action;
@property (nonatomic, retain) NSString * actionDescription;
@property (nonatomic, assign) NSInteger actionVal;
@property (nonatomic, assign) NSInteger period;
@property (nonatomic, retain) NSString * periodDescription;

@end
