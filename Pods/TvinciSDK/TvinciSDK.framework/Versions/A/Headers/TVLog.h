//
//  QLog.h
//  QadabraSDK
//
//  Created by Nissim Pardo on 3/25/14.
//  Copyright (c) 2014 Marimedia LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVLogHandler.h"



extern NSString *const LoggingNotification;
extern NSString *const LogMessageKey;
extern NSString *const LogMessageLevelKey;


void _TVLog(TVLogLevel logLevel,NSString * fileName,int lineNumber, NSString *format, ...);
void notifyListener(NSString *message, NSInteger messageLevel);


#define __FileName__ [[NSString stringWithUTF8String:__FILE__] lastPathComponent]
#define __LineNumber__ __LINE__
#define __MethodName__ [[NSString stringWithUTF8String:__func__] lastPathComponent]



#define TVLogTrace(...) ([TVLogHandler getLogLevel] <= TVLogLevelTrace)? _TVLog(TVLogLevelTrace,__FileName__,__LineNumber__,__VA_ARGS__):nil
#define TVLogDebug(...) [TVLogHandler getLogLevel] <= TVLogLevelDebug? _TVLog(TVLogLevelDebug,__FileName__,__LineNumber__,__VA_ARGS__):nil
#define TVLogInfo(...) [TVLogHandler getLogLevel] <= TVLogLevelInfo? _TVLog(TVLogLevelInfo,__FileName__,__LineNumber__,__VA_ARGS__):nil
#define TVLogWarn(...) [TVLogHandler getLogLevel] <= TVLogLevelWarn? _TVLog(TVLogLevelWarn,__FileName__,__LineNumber__,__VA_ARGS__):nil
#define TVLogError(...) [TVLogHandler getLogLevel] <= TVLogLevelError? _TVLog(TVLogLevelError,__FileName__,__LineNumber__,__VA_ARGS__):nil




