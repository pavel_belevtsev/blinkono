//
//  TVInitObject.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@class TVLocale;

@interface TVInitObject : BaseModelObject

-(BOOL) belongsToValidDomain;
@property (copy , nonatomic) NSString *siteGUID;
@property (nonatomic , copy) NSString *platform;
@property (copy , nonatomic) NSString *APIUsername;
@property (copy , nonatomic) NSString *APIPassword;
@property (copy , nonatomic) NSString *token;
@property (nonatomic , retain) TVLocale *locale;

-(id) initWithArray:(NSArray *)array;
-(void) setAttributesFromArray:(NSArray *)array;

/*!
 @abstract The unique user idendifier. Retrieved after successfull login.
 @discussion This property determines if the current session is valid or not. If the value of
 this property is not nil, the session is cosidered valid. Otherwise login is required.
 */
@property (assign , nonatomic) NSInteger domainID;
@property (copy, nonatomic) NSString *udid;

-(NSDictionary *) JSONObject;
+(TVInitObject *) getInitObjFrom:(TVInitObject *)originalInitObj;
@end

extern NSString *const TVConfigAPIUsernameKey;
extern NSString *const TVConfigAPIPasswordKey;
extern NSString *const TVConfigDomainIDKey;
extern NSString *const TVConfigSiteGUIDKey;
extern NSString *const TVConfigPlatformKey;
extern NSString *const TVConfigLocaleKey;
extern NSString *const TVConfigUDIDGetKey;
extern NSString *const TVPlatformiPad;
extern NSString *const TVPlatformiPhone;
extern NSString *const TVPLatformUnknown;
extern NSString *const TVPLatformCellular;
