//
//  AnalyticsAbstractEvent.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 12/25/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TVAnalyticsEvent : NSObject


@property (retain, nonatomic) NSString * name;
@property (retain, nonatomic) NSString * category;

@end
