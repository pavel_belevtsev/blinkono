//
//  TVLocale.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/14/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

typedef enum 
{
	TVLocaleUserStateUnknown,
	TVLocaleUserStateAnonymous,
	TVLocaleUserStateNew,
	TVLocaleUserStateSub,
	TVLocaleUserStateExSub,
	TVLocaleUserStatePPV,
	TVLocaleUserStateExPPV
} TVLocaleUserState;

@interface TVLocale : BaseModelObject

+(NSString *) userStateString : (TVLocaleUserState) userState;

+(TVLocale *) localeWithDictionary : (NSDictionary *) dictionary;

+(TVLocale *) localeWithArray :(NSArray*) array;

@property (copy , nonatomic) NSString *localeLanguage;
@property (copy , nonatomic) NSString *localeCountry;
@property (copy , nonatomic) NSString *localeDevice;
@property (assign , nonatomic) TVLocaleUserState localeUserState;
@property (nonatomic ,copy) NSString *localeUserStateString;

@end

extern NSString *const TVLocaleLanguageKey;
extern NSString *const TVLocaleCountryKey;
extern NSString *const TVLocaleDeviceKey;
extern NSString *const TVLocaleUserStateKey;
