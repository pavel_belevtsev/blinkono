//
//  TVUniqueIDPovider.h
//  TvinciSDK
//
//  Created by Israel on 2/20/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    OpenUDIDType = 0,
    TvinciUDIDType,
    TemporaryPredefinedUDIDType,
}UDIDType;

@interface TVUniqueIDPovider : NSObject

+(NSString *)getUniqueIDByType:(UDIDType)udidType;
+(void) setTemporaryPredefinedUDID:(NSString *) udidValue;

@end
