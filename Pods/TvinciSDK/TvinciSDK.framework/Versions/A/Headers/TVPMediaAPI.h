//
//  TVPMediaAPI.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/23/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseTVPClient.h"
#import "TVConstants.h"
#import "TVPMediaAPIRequestParams.h"

@class TVPictureSize;
/*!
 @abstract A Client of the TVinci Media API, providing convinient methods to preform all the available calls to the API
 @discussion You should use this class as much as possible to make calls to the TVP API.
 The reason for that is to ensure that all the parameters are of the right type, the request URL is correct and so on.
 
 You build a request by using one of the API methods that returns TVPAPIRequest object.
 Once you have a request object you can configure it to fill your needs, like changing the timeout, specifiying your own
 callback blocks and selector and so on.
 
 It is not recommended to change the paramaters of the request returned by one of the API methods.
 
 @availability 1.0
 */
@interface TVPMediaAPI : BaseTVPClient

/*!
 @abstract Returns a request with the parameter ready to report on action that has been performed on a media
 @param actionName The name of the action. Should be one of the constants starting with TVMediaAction...
 */
+(TVPAPIRequest *) requestForActionDone : (TVMediaAction) action 
                                mediaID : (NSInteger) mediaID 
                              mediaType : (NSString *) mediaType 
                             extraValue : (NSInteger) extraValue
                               delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForAddUserSocialAction : (TVSocialAction) action 
                                      onPlatform : (TVSocialPlatform) platform 
                                       withMedia :(NSInteger) mediaID 
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE("Use requestForDoUserActionWithUserAction: instead");

+(TVPAPIRequest *) requestForChargeMediaWithPrepaid : (NSInteger)mediaFileID 
                                              price : (CGFloat) price
                                           currency : (NSString *) currency
                                      ppvModuleCode : (NSString *) ppvModuleCode
                                          cuponCode : (NSString *) copunCode
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForChargeUserForMediaFile : (NSInteger)mediaFileID
                                              price : (CGFloat) price
                                           currency : (NSString *) currency
                                      ppvModuleCode : (NSString *) ppvModuleCode
                                          cuponCode : (NSString *) copunCode
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForChargeUserForMediaSubsciption : (NSString *)subscriptionID
                                                     price : (CGFloat) price
                                                  currency : (NSString *) currency
                                                 cuponCode : (NSString *) copunCode
                                           extraParameters : (NSString *) extraParameters
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetAutoCompleteSearchList : (NSString *) searchText
                                        fromMediaTypes : (NSArray *) mediaTypes
                                              delegate : (id<ASIHTTPRequestDelegate>)delegate DEPRECATED_MSG_ATTRIBUTE("Use requestForGetAutoCompleteSearch Instead");

+(TVPAPIRequest *) requestForGetAutoCompleteSearch : (NSString *) searchText
                                    fromMediaTypes : (NSArray *) mediaTypes
                                        startIndex : (NSInteger) startIndex
                                        recordsNum : (NSInteger ) recordsNum
                                          delegate : (id<ASIHTTPRequestDelegate>)delegate;



+(TVPAPIRequest *) requestForGetCategory : (NSString *) categoryID 
                                delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetChannelMediaList : (NSInteger) channelID 
                                     pictureSize : (TVPictureSize *) pictureSize 
                                        pageSize : (NSInteger) pageSize 
                                       pageIndex : (NSInteger) pageIndex 
                                         orderBy : (TVOrderBy) orderBy 
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE("Use RequestForGetChannelMultiFilterWithChannelID Instead");

+(TVPAPIRequest *) requestForGetChannelMediaList : (NSInteger) channelID
                                     pictureSize : (TVPictureSize *) pictureSize
                                        pageSize : (NSInteger) pageSize
                                       pageIndex : (NSInteger) pageIndex
                                         orderBy : (TVOrderBy) orderBy
                                      mediaCount : (NSInteger) mediaCount
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE("Use RequestForGetChannelMultiFilterWithChannelID:delegate Instead");

+(TVPAPIRequest *) requestForGetFullCategory : (NSString *) categoryID;

+(TVPAPIRequest *) requestForGetUserTransactionHistory : (NSInteger) startFromIndex 
                                  numberOfItemsToFetch : (NSInteger) count 
                                              delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetUserPermittedSubscriptions;

+(TVPAPIRequest *) requestForGetUserPermittedItems;

+(TVPAPIRequest *) requestForGetDomainPermittedItems;

+(TVPAPIRequest *) requestForGetUserExpiredSubscriptionsWithBatchSize : (NSInteger) numberOfItems 
                                                             delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetUserFavorites : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetAreMediasFavorites: (NSArray *)mediaIDsArray delegate: (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetPrepaidBallanceWithCurrencyCode : (NSString *) currencyCode 
                                                       delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetRecommendedMediasWithPictureSize : (TVPictureSize *) pictureSize 
                                                        pageSize : (NSInteger) pageSize 
                                                       pageIndex : (NSInteger) pageIndex 
                                                        delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSearchMediaWithText : (NSString *) text
                                       mediaType : (NSString *) mediaType
                                     pictureSize : (TVPictureSize *) pictureSize
                                        pageSize : (NSInteger) paeSize
                                       pageIndex : (NSUInteger) pageIndex
                                         orderBy : (TVOrderBy) orderBy
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE(" Use requestForSearchMediaByAndOrList: Instead");

+(TVPAPIRequest *) requestForSearchMediaByTag : (NSString *) tagName
                                  wantedValue : (NSString *) wantedValue
                                    mediaType : (NSString *) mediaType
                                  pictureSize : (TVPictureSize *) pictureSize
                                     pageSize : (NSInteger) pageSize
                                    pageIndex : (NSUInteger) pageIndex
                                      orderBy : (TVOrderBy) orderBy
                                     delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE(" Use requestForSearchMediaByAndOrList: Instead");


+(TVPAPIRequest *) requestForSearchMediaByMetaDataPairs : (NSDictionary *) metaDataPairs
                                             orTagPairs : (NSDictionary *) tagPairs
                                         mediaType : (NSString *) mediaType
                                       pictureSize : (TVPictureSize *) pictureSize
                                          pageSize : (NSInteger) pageSize
                                         pageIndex : (NSUInteger) pageIndex
                                           orderBy : (TVOrderBy) orderBy
                                          delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE(" Use requestForSearchMediaByAndOrList: Instead");

+(TVPAPIRequest *) requestForGetRelatedMediasByTypesWithMediaID: (NSString *) mediaID
                                              mediaType : (NSString *) mediaTypeID
                                            pictureSize : (TVPictureSize *) pictureSize
                                     requestedMediaTypes:(NSArray *) requestedMediaTypes
                                               pageSize : (NSInteger) pageSize
                                              pageIndex : (NSUInteger) pageIndex
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetRelatedMediasWithMediaID: (NSString *) mediaID
                                              mediaType : (NSString *) mediaTypeID
                                            pictureSize : (TVPictureSize *) pictureSize
                                               pageSize : (NSInteger) pageSize
                                              pageIndex : (NSUInteger) pageIndex
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForIsItemPurchased : (NSInteger) fileID 
                                    delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetPeopleWhoWatchedMedia : (NSInteger) mediaID 
                                         mediaTypeID : (NSString *) mediaTypeID 
                                         pictureSize : (TVPictureSize *) pictureSize 
                                            pageSize : (NSInteger) pageSize 
                                           pageIndex : (NSInteger) pageIndex 
                                            delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetMediaLicenseLink : (NSInteger) fileID 
                                         baseURL : (NSURL *) baseURL 
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetLicensedLinksWithFileID:(NSInteger) fileID
                                                baseURL:(NSURL *) baseURL
                                               delegate:(id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForGetMediaMarkForMediaID : (NSInteger) mediaID 
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE("Use requestForGetMediaMarkForMediaID:npvrID instead");
+(TVPAPIRequest *) requestForGetMediaMarkForMediaID : (NSInteger) mediaID
                                             npvrID : (NSString *) npvrID
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetMediaInfoWithMediaID : (NSInteger) mediaID 
                                         pictureSize : (TVPictureSize *) pictureSize 
                                  includeDynamicInfo : (BOOL) includeDynamicInfo 
                                            delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetMediaInfoWithMediaID : (NSInteger) mediaID
                                            mediaType: (NSInteger) mediaType
                                         pictureSize : (TVPictureSize *) pictureSize
                                  includeDynamicInfo : (BOOL) includeDynamicInfo
                                            delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForGetMediasInfoWithMediaIDs : (NSArray *) mediaIDs 
                                           pictureSize : (TVPictureSize *) pictureSize 
                                    includeDynamicInfo : (BOOL) includeDynamicInfo 
                                              delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetItemPriceReason : (NSInteger) fileID 
                                       delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE("Use requestForGetItemPrice instead");

+(TVPAPIRequest *) requestForGetItemsPricesWithCoupons : (NSArray *) fileIDsArray
                                          LanguageCode : (NSString *)langCode
                                            couponCode : (NSString *)couponCode
                                            onlyLowest : (BOOL)onlyLowest
                                           countryCode : (NSString *)countryCode
                                            deviceName : (NSString *)deviceName
                                              userGuid : (NSString *)userGuid
                                              delegate : (id<ASIHTTPRequestDelegate>) delegate
                                            DEPRECATED_MSG_ATTRIBUTE("Use [TVPConditionalAccessAPI requestForGetItemsPricesWithCoupons:] instead");

+(TVPAPIRequest *) requestForGetItemPrice : (NSArray *) fileIDs
                                 delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForMediaHitWithMediaID : (NSInteger) mediaID
                                     mediaTypeID : (NSString *) typeID
                                          fileID : (NSInteger) fileID 
                               locationInSeconds : (NSTimeInterval) location
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE("requestForMediaHitWithMediaID :mediaTypeID :fileID :locationInSeconds :npvrID :delegate: instead");

+(TVPAPIRequest *) requestForMediaHitWithMediaID : (NSInteger) mediaID
                                     mediaTypeID : (NSString *) typeID
                                          fileID : (NSInteger) fileID
                               locationInSeconds : (NSTimeInterval) location
                                          npvrID : (NSString *) npvrID
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForMediaMarkWithAction : (TVMediaPlayerAction) action
                                         onMedia : (NSInteger) mediaID
                                     mediaTypeID : (NSString *) typeID
                                          fileID : (NSInteger) fileID 
                               locationInSeconds : (NSTimeInterval) location 
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE("requestForMediaMarkWithAction:onMedia::mediaTypeID :fileID :locationInSeconds :npvrID :delegate: instead");

+(TVPAPIRequest *) requestForMediaMarkWithAction : (TVMediaPlayerAction) action
                                         onMedia : (NSInteger) mediaID
                                     mediaTypeID : (NSString *) typeID
                                          fileID : (NSInteger) fileID
                               locationInSeconds : (NSTimeInterval) location
                                          npvrID : (NSString *) npvrID
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetUserItems : (TVUserItemType) userItemType 
                                mediaType : (NSString *) mediaType
                              pictureSize : (TVPictureSize *) pictureSize
                                 pageSize : (NSInteger) paeSize
                                pageIndex : (NSUInteger) pageIndex
                                  delegate:  (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForCheckGeoBlockForMedia : (NSInteger) mediaID 
                                          delegate : (id<ASIHTTPRequestDelegate>) delegate;



+(TVPAPIRequest*)requestForGetEPGMultiChannelProgram:(NSArray *) epgIDs
                                        pictureSize : (TVPictureSize *) pictureSize
                                               oUnit:(NSString *)oUnit
                                          fromOffset:(NSInteger)fromOffset
                                            toOffset:(NSInteger)toOffset
                                           utcOffset:(NSInteger)utcOffset
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest*)requestForGetEPGChannelsPrograms:(NSString *) epgID
                                     pictureSize : (TVPictureSize *) pictureSize
                                            oUnit:(NSString *)oUnit
                                       fromOffset:(NSInteger)fromOffset
                                         toOffset:(NSInteger)toOffset
                                        utcOffset:(NSInteger)utcOffset
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE("Use GetEPGMultiChannelProgram: instead");


+(TVPAPIRequest*)requestForGetEPGProgramsByIds:(NSArray *) Ids
                                 programIdType: (TVProgramIdType) programIdType
                                     pageSize : (NSInteger) pageSize
                                    pageIndex : (NSUInteger) pageIndex
                                     delegate : (id<ASIHTTPRequestDelegate>) delegate;



+(TVPAPIRequest*)requestForGetEPGChannelsWithPictureSize:(TVPictureSize *) pictureSize
                                                 orderBy:(TVOrderBy)order
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForRateMediaWithMediaID: (NSInteger) mediaID
                              mediaType : (NSString *) mediaType
                             extraValue : (NSInteger) extraValue
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_ATTRIBUTE;


//GetUserPermittedPackages

+(TVPAPIRequest *) requestForGetUserPermittedPackagesWithDelegate:(id<ASIHTTPRequestDelegate>)delegate;

//SearchMediaByMeta
+(TVPAPIRequest *) requestForSearchMediaByMeta: (NSString *)metaName
                                         value:(NSString *)value
                                     mediaType: (NSString *)mediaType
                                  pictureSize : (TVPictureSize *) pictureSize
                                     pageSize : (NSInteger) pageSize
                                    pageIndex : (NSUInteger) pageIndex
                                      orderBy : (TVOrderBy) orderBy
                                     delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE(" Use requestForSearchMediaByAndOrList: Instead");

+(TVPAPIRequest *) requestForGetMediasInPackage: (long)baseId
                                   pictureSize : (TVPictureSize *) pictureSize
                                      pageSize : (NSInteger) pageSize
                                     pageIndex : (NSUInteger) pageIndex
                                      delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForCustomDownloadFileWithFileURL : (NSURL *) url
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForCheckIsMediaFavorite : (NSInteger) mediaId
                                         delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE("Use requestForGetAreMediasFavorites: Instead");

+(TVPAPIRequest *) requestForSearchMediaByMetasTagsExactWithPairs : (NSDictionary *) metaDataPairs
                                                       orTagPairs : (NSDictionary *) tagPairs
                                                        mediaType : (NSString *) mediaType
                                                      pictureSize : (TVPictureSize *) pictureSize
                                                         pageSize : (NSInteger) pageSize
                                                        pageIndex : (NSUInteger) pageIndex
                                                          orderBy : (TVOrderBy) orderBy
                                                         delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE(" Use requestForSearchMediaByAndOrList: Instead");



+(TVPAPIRequest *) requestForGetChannelMultiFilterWithChannelID:(NSInteger) channelID
                                                   pictureSize : (TVPictureSize *) pictureSize
                                                      pageSize : (NSInteger) pageSize
                                                     pageIndex : (NSUInteger) pageIndex
                                                       orderBy : (TVOrderBy) orderBy
                                                       orderDir: (TVOrderDirection ) orderDir
                                                          tags : (NSArray *) tags
                                                        catWith: (TVCutWith)catWith
                                                      delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest*)requestForGetEPGChannelProgrammeByDates : (NSString *) channelID
                                             pictureSize : (TVPictureSize *) pictureSize
                                                fromDate : (NSString*)fromDate
                                                 toODate : (NSString*)toDate
                                               utcOffset : (NSInteger)utcOffset
                                                delegate : (id<ASIHTTPRequestDelegate>) delegate /* DEPRECATED_MSG_ATTRIBUTE("Use GetEPGChannelProgrammeByDates instead")*/;

+(TVPAPIRequest*)requestForGetEPGLicensedLinkForFileID:(NSInteger) fileID
                                             EPGItemID:(NSInteger) itemID
                                             startTime:(NSDate *) startTime
                                             basicLink:(NSString*) basicLink
                                                userIP:(NSString *) userIP
                                              refferer:(NSString *) refferer
                                            countryCd2:(NSString *) countryCd2
                                         languageCode3:(NSString *) languageCode3
                                            deviceName:(NSString *) deviceName
                                            formatType:(EPGLicensedLinkFormatType) format
                                              delegate:(id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE("Use GetEPGLicencsedData instead");

+(TVPAPIRequest *) requestForGetLastWatchedMediasByMediaID : (NSInteger) mediaID
                                                 mediaType : (NSString *) mediaType
                                               pictureSize : (TVPictureSize *) pictureSize
                                                  pageSize : (NSInteger) pageSize
                                                 pageIndex : (NSInteger) pageIndex
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForSendToFriendWithMediaID: (NSInteger) mediaID
                                          senderName: (NSString *) senderName
                                         senderEmail: (NSString *) senderEmail
                                      recipientEmail: (NSString *) recipientEmail
                                             message:(NSString *) message
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForAddCommentWithMediaID: (NSInteger) mediaID
                                         mediaType: (NSInteger) mediaType
                                            writer: (NSString *) writer
                                            header: (NSString *) header
                                         subheader: (NSString *) subheader
                                           content:(NSString *) content
                                        autoActive:(BOOL) autoActive
                                         delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforGetEPGCommentsListEPGProgramID:(NSInteger ) epgProgramID
                                                  pageSize : (NSInteger) pageSize
                                                 pageIndex : (NSInteger ) pageIndex
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForMediaLicenseData : (NSInteger) fileID
                                   andMediaID : (NSInteger) mediaID
                                     delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForGetMediaCommentsWithMediaID : (NSInteger ) mediaID
                                              pageSize : (NSInteger) pageSize
                                               pageIndex : (NSInteger ) pageIndex
                                                delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest*)requestForGetTranslations:(id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForGetSearchEPG : (NSString *) text
                                  picSize : (TVPictureSize*) pictureSize
                                  pageSize: (NSInteger ) pageSize
                                 pageIndex: (NSInteger ) pageIndex
                                   orderBy: (TVOrderBy ) orderBy
                                 delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforGetRecommendationsByGalleryWithMediaID:(NSInteger ) mediaID
                                                        pictureSize:(TVPictureSize *) pictureSize
                                                      parentalLevel:(NSInteger) parentalLevel
                                                             coGuid:(NSString*)coGuid
                                          recommendationGalleryType:(TVRecommendationGalleryType) galleryType
                                                          delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestforAddEPGCommentWithEPGProgramID:(NSInteger ) epgProgramID
                                               contentText:(NSString *) contentText
                                                    header:(NSString *) headerStr
                                                 subHeader:(NSString *) subHeader
                                                    writer:(NSString *) writer
                                                autoActive:(BOOL) isAutoActive
                                                 delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestforGetEPGCommentWithEPGProgramID:(NSInteger ) epgProgramID
                                                 pageSize : (NSInteger) pageSize
                                                pageIndex : (NSInteger) pageIndex
                                              commentType : (TVEPGCommentType) commentType
                                                 delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest*)requestForGetDomainPermittedItems:(id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest*)requestForGetDomainPermittedSubscriptions:(id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforGetSubscriptionIDsContainingMediaID : (NSInteger) mediaId
                                                          fileID : (NSInteger) fileId
                                                        delegate : (id<ASIHTTPRequestDelegate>) delegate;
+(TVPAPIRequest *) requestForSearchEPGProgramsWithText : (NSString *) text
                                              pageSize : (NSInteger) pageSize
                                             pageIndex : (NSUInteger) pageIndex
                                              delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSearchMediaOrList : (NSDictionary *)orList
                                       AndList : (NSDictionary *)andList
                                     mediaType : (NSString *)mediaType
                                      pageSize : (NSInteger) pageSize
                                     pageIndex : (NSUInteger) pageIndex
                                       orderBy : (TVOrderByAndOrList) orderBy
                                      orderDir : (TVOrderDirectionByAndOrList)orderDir
                                         exact : (BOOL) exact
                                     orderMeta : (NSString*) orderMeta
                                      delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE(" Use requestForSearchMediaByAndOrList: Instead");

+(TVPAPIRequest *) requestForSearchMediaByAndOrList: (NSArray *)orList
                                           AndList : (NSArray *)andList
                                         mediaType : (NSString *)mediaType
                                          pageSize : (NSInteger) pageSize
                                         pageIndex : (NSUInteger) pageIndex
                                           orderBy : (TVOrderByAndOrList) orderBy
                                          orderDir : (TVOrderDirectionByAndOrList)orderDir
                                             exact : (BOOL) exact
                                         orderMeta : (NSString*) orderMeta
                                          delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforGetEPGAutoComplete :(NSString *) searchText
                                       pageSize : (NSInteger) pageSize
                                      pageIndex : (NSInteger ) pageIndex
                                       delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforGetAssetsStatsWithAssetsIDs:(NSArray *) assetsIDs
                                             assetType  : (TVAssetType) assetType
                                               pageSize : (NSInteger) pageSize
                                              pageIndex : (NSInteger ) pageIndex
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforGetAssetsStatsForTimePeriodWithAssetsIDs:(NSArray *) assetsIDs
                                                          assetType  : (TVAssetType) assetType
                                                            pageSize : (NSInteger) pageSize
                                                           pageIndex : (NSInteger ) pageIndex
                                                           startTime : (NSDate *)  startTime
                                                             endTime : (NSDate *) endTime
                                                            delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetOrderedChannelMultiFilterChannelID : (NSInteger) channelID
                                                       pictureSize : (TVPictureSize *) pictureSize
                                                          pageSize : (NSInteger) pageSize
                                                         pageIndex : (NSUInteger) pageIndex
                                                           orderBy : (TVMultiFilterOrderBy) orderBy
                                                          orderDir : (TVChannelMFOrderDirection ) orderDir
                                                        orderValue : (NSString *)orderValue
                                                              tags : (NSArray *) tags
                                                           catWith : (TVCutWith)catWith
                                                          delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSearchEPGByAndOrList : (NSArray *)orList
                                          AndList : (NSArray *)andList
                                         pageSize : (NSInteger) pageSize
                                        pageIndex : (NSUInteger) pageIndex
                                         delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSearchMediaByTypesWithText : (NSString *)text
                                          AndMediaTypes : (NSArray *)mediaTypes
                                            pictureSize : (TVPictureSize *) pictureSize
                                               pageSize : (NSInteger) pageSize
                                              pageIndex : (NSUInteger) pageIndex
                                                orderBy : (TVOrderBy) orderBy
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate DEPRECATED_MSG_ATTRIBUTE(" Use requestForSearchMediaByAndOrList: Instead");



#pragma mark -  NPVR Rquests

+(TVPAPIRequest *) requestForRecordAssetWithEPGId : (NSString *) epgID
                                         delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForCancelAssetRecordingWithRecordingId : (NSString *) recordingID
                                                       delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForDeleteAssetRecordingWithRecordingId: (NSString *) recordingID
                                                       delegate: (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetNPVRQuotaWithdelegate:(id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForRecordSeriesByProgramId:(NSString *) programID
                                       withdelegate:(id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForDeleteSeriesRecordingWithSeriesRecordingId:(NSString *) recordingId
                                                          withdelegate:(id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForCancelSeriesRecordingWithSeriesRecordingId:(NSString *) recordingId
                                                          withdelegate:(id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSetAssetProtectionStatusWithRecordingId:(NSString *) recordingId
                                                          isProtect:(BOOL) isProtect
                                                       withdelegate:(id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForGetNPVRLicensedLinkWithRecordingId:(NSString *) recordingId
                                                     startTime:(NSDate *) startTime
                                                        fileID:(NSInteger ) fileID
                                                     basicLink:(NSString *) basicLink
                                                      refferer:(NSString *) refferer
                                                    couponCode:(NSString *) couponCode
                                                  withdelegate:(id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetRecordingsWithPageSize:(NSInteger ) pageSize
                                            pageIndex:(NSInteger ) pageIndex
                                             searchBy:(TVRecordingSearchByType) searchBy
                                         epgChannelID:(NSString *) epgChannelId
                                      recordingStatus:(TVRecordingStatus)recoredingStatus
                                         recordingIDs:(NSArray *) recordingsIds
                                          programsIds:(NSArray *) programIds
                                            seriesIds:(NSArray *) seriesIds
                                            startDate:(NSDate *) date
                                              orderBy:(TVRecordOrderBy) orderBy
                                       orderDirection:(TVRecordOrderDirection) orderDirection
                                         withdelegate:(id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForGetSeriesRecordingsWithPageSize:(NSInteger ) pageSize
                                                  pageIndex:(NSInteger ) pageIndex
                                                    orderBy:(TVRecordOrderBy) orderBy
                                             orderDirection:(TVRecordOrderDirection) orderDirection
                                               withdelegate:(id<ASIHTTPRequestDelegate>) delegate;



+(TVPAPIRequest *) requestForGetSubscriptionProductCode: (void(^)(RPGetSubscriptionProductCode *requestParams))requestParams;
+(TVPAPIRequest *) requestForChargeUserWithInApp: (void(^)(RPChargeUserWithInApp *requestParams))requestParams;
+(TVPAPIRequest *) requestForGetSubscriptionMedias: (void(^)(RPGetSubscriptionMedias *requestParams))requestParams;
+(TVPAPIRequest *) requestForChargeUserForMediaFileInApp: (void(^)(RPChargeUserPPVWithInApp *requestParams))requestParams;

+(TVPAPIRequest *) requestForSearchAssetsWithPageSize:(NSInteger ) pageSize
                                            pageIndex:(NSInteger ) pageIndex
                                          filterTypes:(NSArray *) filterTypes
                                               filter:(NSString *) filter
                                              orderBy:(TVSearchOrderBy) orderBy
                                       additionalData:(NSArray *) additionalData;







@end

#pragma mark - Constants for the API method names
extern NSString * const MethodNameGetMediaLicenseData;
extern NSString * const MethodNameSearchEPG ;
extern NSString * const MethodNameSendToFriend ;
extern NSString * const MethodNameAddComment ;
extern NSString * const MethodNameGetEPGChannelProgrammeByDates;
extern NSString * const MethodNameGetChannelMultiFilter;
extern NSString * const MethodNameRateMedia;
extern NSString * const MethodNameGetEPGMultiChannelProgram ;
extern NSString * const MethodNameGetEPGChannelsPrograms;
extern NSString * const MethodNameGetEPGChannels;
extern NSString * const MethodNameActionDone;
extern NSString * const MethodNameAddUserSocialAction;
extern NSString * const MethodNameChargeMediaWithPrepaid;
extern NSString * const MethodNameChargeUserForMediaFile;
extern NSString * const MethodNameChargeUserForMediaSubscription;
extern NSString * const MethodNameGetAutoCompleteSearchList;
extern NSString * const MethodNameGetAutoCompleteSearch;
extern NSString * const MethodNameGetCategory;
extern NSString * const MethodNameGetChannelMediaList;
extern NSString * const MethodNameGetChannelMediaListWithMediaCount;
extern NSString * const MethodNameGetFullCategory;
extern NSString * const MethodNameGetItemPriceReason;
extern NSString * const MethodNameGetItemPrices;
extern NSString * const MethodNameGetLastWatchedMedia;
extern NSString * const MethodNameGetLastWatchedMediaByPeriod;
extern NSString * const MethodNameGetMediaComments;
extern NSString * const MethodNameGetMediaInfo;
extern NSString * const MethodNameGetLicensedLinks;
extern NSString * const MethodNameGetMediaLicenseLink;
extern NSString * const MethodNameGetMediaMark;
extern NSString * const MethodNameGetMediasByMostAction;
extern NSString * const MethodNameGetMediasByRating;
extern NSString * const MethodNameGetMediasInPackage;
extern NSString * const MethodNameGetMediasInfo;
extern NSString * const MethodNameGetNMostSearchedText;
extern NSString * const MethodNameGetPeopleWhoWatched;
extern NSString * const MethodNameGetPrepaidBalance;
extern NSString * const MethodNameGetRecommendedMedias;
extern NSString * const MethodNameGetRelatedMediaWithMediaCount;
extern NSString * const MethodNameGetRelatedMedias;
extern NSString * const MethodNameGetSubscriptionMedia;
extern NSString * const MethodNameGetSubscriptionsContainingMediaFile;
extern NSString * const MethodNameGetSubscriptionsPrices;
extern NSString * const MethodNameGetUserExpiredItems;
extern NSString * const MethodNameGetUserExpiredSubscriptions;
extern NSString * const MethodNameGetUserFavorites;
extern NSString * const MethodNameAreMediasFavorite;
extern NSString * const MethodNameGetUserItems;
extern NSString * const MethodNameGetUserPermittedSubscriptions;
extern NSString * const MethodNameGetUserPermittedItems;
extern NSString * const MethodNameGetUserSocialMedia;
extern NSString * const MethodNameGetUserTransactionHistory;
extern NSString * const MethodNameGetVoteRatio;
extern NSString * const MethodNameIsItemPurchased;
extern NSString * const MethodNameGetItemsPricesWithCoupons;
extern NSString * const MethodNameIsMediaFavorite;
extern NSString * const MethodNameIsUserSocialActionPerformed;
extern NSString * const MethodNameIsUserVoted;
extern NSString * const MethodNameMediaHit;
extern NSString * const MethodNameMediaMark;
extern NSString * const MethodNameSearchMedia;
extern NSString * const MethodNameSearchMediaByMeta; // Deprecated
extern NSString * const MethodNameSearchMediaByMultiMeta; // Deprecated
extern NSString * const MethodNameSearchMediaByMetaWithMediaCount;
extern NSString * const MethodNameSearchMediaByTag; // Deprecated
extern NSString * const MethodNameSearchMediaByMultiTag; // Deprecated
extern NSString * const MethodNameSearchMediaByMetasTags;
extern NSString * const MethodNameSearchMediaWithMediaCount;
extern NSString * const MethodNameCheckGeoBlockForMedia;
extern NSString * const MethodNameGetUserPermittedPackages;
extern NSString * const MethodNameSearchMediaByMetasTagsExact;
extern NSString * const MethodNameGetEPGLicensedLink;
extern NSString * const MethodNameGetLastWatchedMedias;
extern NSString * const MethodNameGetTranslations;
extern NSString * const MethodNameGetRecommendationsByGallery;
extern NSString * const MethodNameAddEPGComment;
extern NSString * const MethodNameGetEPGCommentList;
extern NSString * const MethodNameGetAssetsStats;
extern NSString * const MethodNameGetAssetsStatsForTimePeriod;
extern NSString * const MethodNameGetOrderedChannelMultiFilter;

extern NSString * const MethodNameGetDomainPermittedItems;
extern NSString * const MethodNameGetDomainPermittedSubscriptions;
extern NSString * const MethodNameGetSubscriptionIDsContainingMediaFile;
extern NSString * const MethodNameSearchEPGPrograms;
extern NSString * const MethodNameSearchMediaByAndOrList;
extern NSString * const MethodNameGetEPGAutoComplete;
extern NSString * const MethodNameSearchEPGByAndOrList;
extern NSString * const MethodNameSearchMediaByTypes;
extern NSString * const MethodNameGetRelatedMediasByTypes;

extern NSString * const MethodNameRecordAsset;
extern NSString * const MethodNameCancelAssetRecording;
extern NSString * const MethodNameDeleteAssetRecording;
extern NSString * const MethodNameGetNPVRQuota ;
extern NSString * const MethodNameRecordSeriesByProgramId;
extern NSString * const MethodNameDeleteSeriesRecording;
extern NSString * const MethodNameCancelSeriesRecording;
extern NSString * const MethodNameSetAssetProtectionStatus;
extern NSString * const MethodNameGetNPVRLicensedLink;
extern NSString * const MethodNameGetRecordings;
extern NSString * const MethodNameGetSeriesRecordings;
extern NSString * const MethodNameGetEPGProgramsByIds;

extern NSString * const MethodNameSearchAssets;




