//
//  DomainServicesResponse.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>
@class TVResponseStatus;


@interface TVDomainServicesResponse : BaseModelObject

@property (strong, nonatomic) NSArray * arrayServices;
@property (strong, nonatomic) TVResponseStatus * responseStatus;
@end
