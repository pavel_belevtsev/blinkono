//
//  AnalyticsManager.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 12/25/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVAnalyticsProvider.h"
#import "TVAnalyticsEvent.h"

@interface TVAnalyticsManager : NSObject



+(TVAnalyticsManager *) sharedAnalyticsManager;

-(void) addProvider:(TVAnalyticsProvider*) provider;
-(void) removeProvider:(TVAnalyticsProvider*) provider;

-(void) sendEvent:(TVAnalyticsEvent *) event;


@end
