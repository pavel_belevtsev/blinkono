//
//  TVPSocialAPI.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 10/3/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "BaseTVPClient.h"
#import "TVConstants.h"


@interface TVPSocialAPI : BaseTVPClient

+(TVPAPIRequest *) requestForDoUserActionWithUserAction : (TVUserSocialActionType) userAction
                                              assetType : (TVAssetType) assetType
                                                assetID : (NSInteger) assetID
                                            extraParams : (NSArray *) extraParams
                                         SocialPlatform : (TVSocialPlatform) socialPlatform
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforGetUserActionsWithUserAction : (TVUserSocialActionType) userAction
                                                assetType : (TVAssetType) assetType
                                                  assetID : (NSInteger) assetID
                                               startIndex : (NSInteger) startIndex
                                               recordsNum : (NSInteger ) recordsNum
                                           socialPlatform : (TVSocialPlatform ) socialPlatform
                                                 delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforGetUserActivityFeedWithPageSize : (NSInteger) nPageSize
                                                   pageIndex : (NSInteger) nPageIndex
                                                picDimension : (NSString *) sPicDimension
                                                    delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforGetUserActivityFeedWithPageSize : (NSInteger) nPageSize
                                                   pageIndex : (NSInteger) nPageIndex
                                                picDimension : (NSString *) sPicDimension
                                                    siteGuid : (NSString *) siteGuid
                                                    delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetUsersLikedMediaWithMediaID : (NSInteger) mediaID
                                               onlyFriends : (BOOL) onlyFriends
                                                startIndex : (NSInteger) startIndex
                                                  pageSize : (NSInteger) pageSize
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetUserFriendsWithDelegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetFriendsActionsWithUserActions : (NSArray *) userActions
                                                    assetType : (TVAssetType ) assetType
                                                      assetID : ( NSInteger ) assetID
                                                   startIndex : ( NSInteger ) startIndex
                                                 numOfRecords : ( NSInteger ) numOfRecords
                                               socialPlatform : (TVSocialPlatform ) socialPlatform
                                                     delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetCrowdsourceFeedWithPageSize : (NSString *)pageSize
                                              epochLastTime : (NSString *)epochLastTime
                                                  assetType : (TVAssetType ) assetType
                                                    assetID : ( NSInteger ) assetID
                                             socialPlatform : (TVSocialPlatform ) socialPlatform
                                                   delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForSetUserFBActionPrivacyWithUserAction:(TVUserSocialActionType) userAction
                                                  socialPlatform :( TVSocialPlatform ) socialPLatform
                                                   privacyAction :(TVUserActionPrivacy) privacyAction
                                                        delegate : (id<ASIHTTPRequestDelegate>) delegate __attribute__((deprecated)); 

+(TVPAPIRequest *) requestforGetUserFBActionPrivacyWithUserAction:(TVUserSocialActionType) userAction
                                                  socialPlatform : (TVSocialPlatform) socialPlatfom
                                                        delegate : (id<ASIHTTPRequestDelegate>) delegate __attribute__((deprecated));

+(TVPAPIRequest *) requestforGetUserInternalActionPrivacyWithUserAction:(TVUserSocialActionType) userAction
                                                        socialPlatform : (TVSocialPlatform) socialPlatfom
                                                              delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestForSetUserInternalActionPrivacyWithUserAction:(TVUserSocialActionType) userAction
                                                        socialPlatform :( TVSocialPlatform ) socialPLatform
                                                         privacyAction :(TVUserPrivacy) userPrivacy
                                                              delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSetUserExternalActionShare:(TVUserSocialActionType) userAction
                                        socialPlatform :( TVSocialPlatform ) socialPLatform
                                         actionPrivacy :(TVUserPrivacy)userPrivacy
                                              delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetUserSocialPrivacyWithUserAction:(TVUserSocialActionType) userAction
                                                socialPlatform :( TVSocialPlatform ) socialPLatform
                                                      delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSetUserSocialPrivacy:(TVUserSocialActionType) userAction
                                                      socialPlatform :(TVSocialPlatform ) socialPLatform
                                                       actionPrivacy :(TVUserActionPrivacy)actionPrivacy
                                                            delegate : (id<ASIHTTPRequestDelegate>) delegate;



+(TVPAPIRequest *) requestForGetUserExternalActionShare:(TVUserSocialActionType) userAction
                                        socialPlatform :( TVSocialPlatform ) socialPLatform
                                              delegate : (id<ASIHTTPRequestDelegate>) delegate;


+(TVPAPIRequest *) requestforGetSocialFeedWithMediaID : (NSInteger) mediaId
                                             delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforFBTokenValidation : (NSString *) token
                                             delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestforFBUserSigninWithToken:(NSString *) token
                                         delegate : (id<ASIHTTPRequestDelegate>) delegate;


extern NSString * const MethodNameGetUserInternalActionPrivacy;
extern NSString * const MethodNameGetUserFBActionPrivacy;
extern NSString * const MethodNameDoUserAction;
extern NSString * const MethodNameGetUsersLikedMedia;
extern NSString * const MethodNameGetUserFriends;
extern NSString * const MethodNameGetFriendsActions;
extern NSString * const MethodNameGetUserActions;
extern NSString * const MethodNameSetUserInternalActionPrivacy;
extern NSString * const MethodNameGetUserInternalActionPrivacy;
extern NSString * const MethodNameSetUserExternalActionShare;
extern NSString * const MethodNameGetUserExternalActionShare;
extern NSString * const MethodNameGetUserSocialPrivacy;
extern NSString * const MethodNameSetUserSocialPrivacy;
extern NSString * const MethodNameGetSocialFeed;
extern NSString * const MethodNameFBTokenValidation;
extern NSString * const MethodNameFBUserSignin;


@end
