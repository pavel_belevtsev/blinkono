//
//  MacAdressDiscovery.h
//  MacAdressDiscovery
//
//  Created by Tarek Issa on 3/10/14.
//  Copyright (c) 2014 Stas Shulgin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MacAdressDiscovery : NSObject


- (NSString *)getMacAddressWith3GStatus: (bool)is3G;

- (NSString *)getIpAddressWith3GStatus: (bool)is3G;

@end
