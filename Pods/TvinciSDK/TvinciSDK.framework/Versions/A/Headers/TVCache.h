//
//  TVCache.h
//  TVinci
//
//  Created by Avraham Shukron on 7/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TVCache : NSCache
+(TVCache *) sharedTVCache;
@end
