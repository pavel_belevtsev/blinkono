//
//  TVPUsersAPI.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 10/3/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "BaseTVPClient.h"
#import "TVConstants.h"

@interface TVPUsersAPI : BaseTVPClient

+(TVPAPIRequest *) requestForRenewUserPasswordWithUserName : (NSString *) username
                                               newPassword : (NSString *) newPassword
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForChangeUserPasswordWithUserName : (NSString *) username
                                                oldPassword : (NSString *) oldPassword
                                               newPassword : (NSString *) newPassword
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate;



+(TVPAPIRequest *) requestForGetItemsFromListWithItemType : (TVListItemType) itemType
                                                listType : (TVListType) listType
                                              arrayofIds : (NSArray *) idsArr
                                         shouldAutoOrder : (BOOL) autoOrder
                                                delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForIsItemExistsInListWithItemType : (TVListItemType) itemType
                                                   listType : (TVListType) listType
                                                 arrayofIds : (NSArray *) idsArr
                                            shouldAutoOrder : (BOOL) autoOrder
                                                   delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForAddItemToListWithItemType : (TVListItemType) itemType
                                                   listType : (TVListType) listType
                                                 arrayofIds : (NSArray *) idsArr
                                            shouldAutoOrder : (BOOL) autoOrder
                                                   delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForUpdateItemFromListWithItemType : (TVListItemType) itemType
                                                   listType : (TVListType) listType
                                                 arrayofIds : (NSArray *) idsArr
                                            shouldAutoOrder : (BOOL) autoOrder
                                                   delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForRemoveItemFromListWithItemType : (TVListItemType) itemType
                                                   listType : (TVListType) listType
                                                 arrayofIds : (NSArray *) idsArr
                                            shouldAutoOrder : (BOOL) autoOrder
                                                   delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSetUserDynamicDataExWithKey : (NSString *) key
                                                   value : (NSString *) value
                                                siteGUID : (NSString *) siteGUID
                                                domainID : (NSInteger) domainID
                                                delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetAccountSTBsWithAccountNumber : (NSString *) accountNumber
                                                    delegate : (id<ASIHTTPRequestDelegate>) delegate;

extern NSString * const MethodNameRenewUserPassword ;
extern NSString * const MethodNameChangeUserPassword ;

extern NSString * const MethodNameAddItemToList;
extern NSString * const MethodNameRemoveItemFromList;
extern NSString * const MethodNameUpdateItemInList;
extern NSString * const MethodNameGetItemFromList;
extern NSString * const MethodNameIsItemExistsInList;

@end
