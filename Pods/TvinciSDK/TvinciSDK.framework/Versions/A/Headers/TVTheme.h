//
//  TVTheme.h
//  TVinci
//
//  Created by Avraham Shukron on 6/27/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const NavigationBarTintColorKey;
extern NSString * const PrimaryTextColorKey;
extern NSString * const SecondaryTextColorKey;
extern NSString * const PrimaryAccentColorKey;
extern NSString * const SecondaryAccentColorKey;
extern NSString * const TextShadowColorKey;
extern NSString * const MetaDataTextColorKey;

extern NSString * const TextSizeLargeKey;
extern NSString * const TextSizeMediumKey;
extern NSString * const TextSizeSmallKey;
extern NSString * const TextSizeTinyKey;

@interface TVTheme : NSObject
+(TVTheme *) mainTheme;
-(UIColor *) colorForKey : (NSString *) key;
-(CGFloat) floatForKey : (NSString *) key;
-(UIFont *) mainFont;
-(UIFont *) mainFontWithSize : (CGFloat) textSize;

-(UIColor *) colorFromString : (NSString *) rgbString;
@end
