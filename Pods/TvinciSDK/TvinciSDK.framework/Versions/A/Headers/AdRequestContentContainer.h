//
//  AdRequestContentContainer.h
//  TvinciSDK
//
//  Created by iosdev1 on 6/13/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TVMediaItem;
@class TVFile;

@interface AdRequestContentContainer : NSObject
@property (nonatomic, retain) NSArray * preRollsRequests;       //  [AdRequestContentMetadata]
@property (nonatomic, retain) NSArray * midRollsRequests;      //  AdRequestContentMetadata]
@property (nonatomic, retain) NSArray * postRollsRequests;     //  AdRequestContentMetadata]
@property (nonatomic, retain) TVMediaItem * mediaItem;

@property NSInteger currentPreRoleIndex;
@property NSInteger currentMidRollIndex;
@property NSInteger currentPostRoleIndex;

-(id)initWithMediaItem:(TVMediaItem *)media;
-(NSInteger)chackAndReturnAdLocateifNeedPlayMidPollwithTime:(NSInteger)time;
-(void)buildAdsRequestsWithFile:(TVFile *)file;



@end
