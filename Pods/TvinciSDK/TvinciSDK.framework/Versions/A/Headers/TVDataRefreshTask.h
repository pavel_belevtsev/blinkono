//
//  TVRefreshTask.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/16/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    TVDataRefreshTaskResultRefreshed,
    TVDataRefreshTaskResultNotRefreshed,
    TVDataRefreshTaskResultFailed
} TVDataRefreshTaskResult;

@interface TVDataRefreshTask : NSObject

@property (copy, nonatomic) void(^refreshTaskBlock)(void(^completionBlock)(TVDataRefreshTaskResult result));
@property (retain, nonatomic) NSDate * nextRefreshDate;
@property (assign, nonatomic) NSTimeInterval refreshTimePeriod;
@property (weak, nonatomic) id target;

+(instancetype) dataRefreshTaskWithRefreshTimePeriod:(NSTimeInterval) timeInterval
                                        refreshBlock: (void(^)( void(^completionBlock)(TVDataRefreshTaskResult) )) refreshBlock
                                         nextRefreshDate:(NSDate *) nextRefreshDate
                                              target:(id) target;

@end

