//
//  TVCompanionAPI.h
//  TvinciSDK
//
//  Created by Tarek Issa on 4/8/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVCompanionAPI : BaseTVPClient


/**********
 * Get request to send STB an http command.
 *
 * @param   deviceIp    The STB ip address
 * @param   EpgProgramID    program id (optional)
 * @param   siteGuid    tvinci's user's site_Guid id
 * @param   mediaId    TVMendiaItem property mediaID
 * @param   fileId    TVFile fileID
 * @param   port    The port to be sent through
 * @param   playbackTimeSeconds    last watch playback, to make it the same "last watch" after the swoosh
 * @param   domainId    TVDomain domainId
 *
 * @param   playback_type    String that indecates weather the stream is live or recorded. Use consts
 TVCompanionItemType_Live TVCompanionItemType_VOD
 *
 * @param   itemType    String that indecates type of PLTV mode. Use consts TVCompanionPlaybackType_StartOver
 TVCompanionPlaybackType_CatchUp TVCompanionPlaybackType_PLTV
 *
 *
 * @return  {@link TVPAPIRequest}
 ********/

+(TVPAPIRequest *) requestForSendPlayToSTBIp : (NSString *) deviceIp
                               playback_type : (NSString *) playback_type
                                epgProgramId : (NSString *) EpgProgramID
                                    siteGuid : (NSString *) siteGuid
                                    itemType : (NSString *) itemType
                                     mediaId : (NSString *) mediaId
                                      fileId : (NSString *) fileId
                                        port : (NSString *) port
                                    domainId : (NSInteger) domainId
                                   mediaMark : (NSInteger) seconds
                                doRequestPin : (BOOL) requestPin
                                    delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSendPlayToSTBWithBaseUrl : (NSString *) baseUrlString
                                        playback_type : (NSString *) playback_type
                                         epgProgramId : (NSString *) EpgProgramID
                                             siteGuid : (NSString *) siteGuid
                                             itemType : (NSString *) itemType
                                              mediaId : (NSString *) mediaId
                                               fileId : (NSString *) fileId
                                             domainId : (NSInteger) domainId
                                            mediaMark : (NSInteger) seconds
                                         doRequestPin : (BOOL) requestPin
                                             delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSendPlayToNetgemSTBIp : (NSString *) deviceIp
                                        playbackUrl:(NSString *) url
                                     playback_type : (NSString *) playback_type
                                      epgProgramId : (NSString *) EpgProgramID
                                          siteGuid : (NSString *) siteGuid
                                          itemType : (NSString *) itemType
                                           mediaId : (NSString *) mediaId
                                            fileId : (NSString *) fileId
                                              port : (NSString *) port
                                          domainId : (NSInteger) domainId
                                         mediaMark : (NSInteger) seconds
                                      doRequestPin : (BOOL) requestPin
                                          delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForSendRemoteMessageToSTBWithBaseUrl : (NSString *) baseUrlString
                                                        message:(NSString *) message
                                            delegate : (id<ASIHTTPRequestDelegate>) delegate;

extern const NSString * TVCompanionItemType_Live;
extern const NSString * TVCompanionItemType_VOD;

extern const NSString * TVCompanionPlaybackType_StartOver;
extern const NSString * TVCompanionPlaybackType_CatchUp;
extern const NSString * TVCompanionPlaybackType_PLTV;



@end
