//
//  TVEPGcomment.h
//  TvinciSDK
//
//  Created by Israel Berezin on 12/24/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVEPGcomment : BaseModelObject

@property (retain, nonatomic) NSString * author;
@property (retain, nonatomic) NSString * header;
@property (retain, nonatomic) NSString * content;
@property (retain, nonatomic) NSDate * addedDate;
@property (retain, nonatomic) NSString * languageName;
@property (assign, nonatomic) NSInteger assetId;
@property (assign, nonatomic) NSInteger languageId;
@property (retain, nonatomic) NSString * userpicUrl;

@end
