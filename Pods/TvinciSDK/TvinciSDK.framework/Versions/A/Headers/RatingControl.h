//
//  RatingControl.h
//  Sherut
//
//  Created by Gilad Novik on 12-01-30.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingControl : UIControl

@property (nonatomic, copy) NSString *onImageName;
@property (nonatomic, copy) NSString *offImageName;

@property (nonatomic, retain) UIImageView *onImageView;
@property (nonatomic, retain) UIImageView *offImageView;
@property(nonatomic,assign) CGFloat rating;	// In the range [0,1]
@property (nonatomic, retain) NSArray *starsPositions;

-(void) setupStarsPositions:(NSArray *)starsPoss;
@end
