//
//  TVFacebookUser.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
#import "TVConstants.h"

@interface TVFacebookUser : BaseModelObject

@property (assign, nonatomic) TVFacebookStatus status;
@property (retain, nonatomic) NSString * siteGuid;
@property (retain, nonatomic) NSString * tvinciName;
@property (retain, nonatomic) NSString * facebookName;
@property (retain, nonatomic) NSURL * pictureURL;
@property (retain, nonatomic) NSString  * data;
@property (retain, nonatomic) NSString * minFriends;
@property (retain, nonatomic) NSDictionary * facebookUser;
@property (retain, nonatomic) NSString * token;











@end
