//
//  TVNCompanionDiscoveryDelegate.h
//  Tvinci
//
//  Created by Sagi Antebi on 8/24/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TVCompanionDiscoveryDelegate <NSObject>


- (void) onDiscovery: (NSArray*)devices;


@end
