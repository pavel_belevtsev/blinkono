//
//  AdRequestContentMetadata.h
//  TvinciSDK
//
//  Created by iosdev1 on 6/13/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"
#import "TVConstants.h"

typedef enum{
    ShortMovie = 1,
    LongMovie = 2
}ContentForm;



@interface AdRequestContentMetadata : BaseModelObject
@property (nonatomic, retain) NSString * category;
@property (nonatomic, assign) ContentForm contentForm;
@property (nonatomic, retain) NSString *contentIdentifier;
@property (nonatomic, retain) NSString *contentPartner;
@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, retain) NSArray *flags; //NSString[]
@property (nonatomic, retain) NSArray *tags ; //NSString[]
@property (nonatomic, assign) NSTimeInterval timeToShow;
@property (nonatomic, assign) AdType adType;

@end
