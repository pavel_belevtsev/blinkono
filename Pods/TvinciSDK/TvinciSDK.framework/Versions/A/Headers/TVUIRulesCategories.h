//
//  NSObject+TVObjectAssociatedResourceID.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 5/25/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//


/**
 *  To simplyfy the implementatin for each controls to behave according to it's rule at a time, we have created these categories,
 *  This way, each NSObject has a UnitID, and his main responsibility will be to know only his UIUnitID and to activate the rule on him self.
 */




#import <Foundation/Foundation.h>


@interface NSObject (TVUIRules)
@property (nonatomic, strong) NSString * UIUnitID;
@end


@interface NSString (TVUIRules)

/**
 *  In order to know if the rule for the unit is enabled or not hidden we can ask if unit exist on the string of the UIUnitID
 *  @return if unit exist or not.
 */
-(BOOL) isUIUnitExist;

@end



@interface UICollectionViewCell (TVUIRules)
/**
 *  To deside if cell will be clickable or not , we should call this method.
    The cell will be unclickable if the rule is hidden or disable
    ( in order to know if to show this cell or not, you can ask if the unit exist at NSString category ).
    Pay attention that you should set the UIUnitID of the CollectionViewCell before calling this method.
 */
+(BOOL) shouldSelectCellForUIUnit:(NSString *) unitID;
@end

@interface UIView (TVUIRules)

/**
 *  In order to activate the approproate rull for a UIView we should call this method.
    Pay attention that you should set the UIUnitID of the View before calling this method.
 */
-(void) activateAppropriateRule;

/**
 *  set visabled and allowed behaviur.
 */
-(void) resetRule;
@end


@interface UIControl (TVUIRules)

/**
 *  In order to activate the approproate rull for a Control we should call this method.
 Pay attention that you should set the UIUnitID of the Control before calling this method.
 */
-(void) activateAppropriateRule;

/**
 *  set visabled and allowed behaviur.
 */
-(void) resetRule;
@end


@interface UISlider (TVUIRules)


/**
 *  In order to activate the approproate rull for a UISlider we should call this method.
    It's importane to send the Thumb image cause in such of the cases this image is gone so we need to know what to put back on normal state.
 Pay attention that you should set the UISlider of the Control before calling this method.
 */
-(void) activateAppropriateRuleWithThumbImage:(UIImage *) image;

/**
 *  set visabled and allowed behaviur.
 */
-(void) resetRuleWithThumbImage:(UIImage *) image;
@end












