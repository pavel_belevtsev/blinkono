//
//  TVPPricingAPIRequestParams.h
//  TvinciSDK
//
//  Created by Alex Zchut on 2/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPGetSubscriptionsContainingMediaFile : NSObject

@property (nonatomic, readwrite) NSInteger mediaId;
@property (nonatomic, readwrite) NSInteger fileId;

@end

@interface RPGetSubscriptionIDsContainingMediaFile : NSObject

@property (nonatomic, readwrite) NSInteger mediaId;
@property (nonatomic, readwrite) NSInteger fileId;

@end

@interface RPGetSubscriptionData : NSObject

@property (nonatomic, strong) NSArray *arrSubscriptionIds;

@end