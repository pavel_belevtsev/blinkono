//
//  TVPNotificationAPI.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/8/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseTVPClient.h"
#import "TVConstants.h"

@interface TVPNotificationAPI : BaseTVPClient

+(TVPAPIRequest *) requestforGetDeviceNotificationsType: (TVNotificationType) notificationType
                                                 viewStatus: (TVNotificationViewStatus) notificationViewStatus
                                     notificationsCount:(NSInteger) count
                                               delegate: (id<ASIHTTPRequestDelegate>)delegate;


+(TVPAPIRequest *) requestForGetUserStatusSubscriptionWithDelegate: (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestforSetNotificationMessageViewStatus : (TVNotificationViewStatus) notificationViewStatus
                                                 notificationRequestID : (NSString *) notificationRequestID
                                                notificationMessageID : (NSString *) notificationMessageID
                                               delegate: (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestforSubscribeByTag: (NSArray *) tags
                                  delegate : (id<ASIHTTPRequestDelegate>)delegate;

+(TVPAPIRequest *) requestForUnsubscribeFollowUpByTag : (NSArray *) tags
                                             delegate : (id<ASIHTTPRequestDelegate>)delegate;



extern NSString * const MethodNameGetDeviceNotifications ;
extern NSString * const MethodNameGetUserStatusSubscriptions ;
extern NSString * const MethodNameSetNotificationMessageViewStatus ;
extern NSString * const MethodNameSubscribeByTag ;
extern NSString * const MethodNameUnsubscribeFollowUpByTag ;


@end
