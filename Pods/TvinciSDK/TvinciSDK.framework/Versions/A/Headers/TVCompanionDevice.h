//
//  TVCompanionDevice.h
//  TvinciSDK
//
//  Created by Tarek Issa on 3/18/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

//#import "TvinciSDK.h"
#import "TvinciSDK.h"


@interface TVCompanionDevice : BaseModelObject {
    
}



- (instancetype)initWithString:(NSString *)string;
- (void) downloadXmlDataWithCompletionBlock : (void (^)(NSError* error)) completion;
- (void)changeFriendlyNameTo:(NSString *)newName;

@property (retain,readonly) NSString *friendlyName;
@property (retain,readonly) NSString *basUrlString;
@property (retain,readonly) NSString *CacheControl;
@property (retain,readonly) NSString *userAgent;
@property (retain,readonly) NSString *ipAddress;
@property (retain,readonly) NSString *server;
@property (retain,readonly) NSString *port;
@property (retain,readwrite) NSString *udid;
//@property (retain,readonly) NSString *udid_serverFormat;
@property (retain,readonly) NSString *USN;
@property (retain,readonly) NSString *ext;
@property (retain,readonly) NSString *OPT;
@property (retain,readonly) NSString *NLS;
@property (retain,readonly) NSString *ST;
@property (retain,readonly) NSDate *date;
@property (retain,readonly) NSURL *location;


@end
