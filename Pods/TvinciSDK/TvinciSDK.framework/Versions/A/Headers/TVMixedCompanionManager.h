//
//  TVNMixedCompanionManager.h
//  Tvinci
//
//  Created by Sagi Antebi on 8/24/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVCompanionDiscoveryDelegate.h"
#import "TVAbstractCompanionDevice.h"
#import "TVAbstractCompanionHandler.h"




@class TVEPGProgram;


@interface TVMixedCompanionManager : NSObject <TVCompanionDiscoveryDelegate>

extern NSString* const CompanionMessageGetVolume;
extern NSString* const CompanionMessageSetVolume;
extern NSString* const CompanionMessageShowUI;
extern NSString* const CompanionMessageHideUI;
extern NSString* const CompanionMessageVolumeUp;
extern NSString* const CompanionMessageVolumeDown;


+ (TVMixedCompanionManager*)sharedInstance;
- (void) scan;
- (void) connect:(TVAbstractCompanionDevice*)device;
- (void) disconnect;
- (BOOL) isConnected;
- (TVAbstractCompanionDevice*) getCurrentConnectedDevice;

- (NSArray*) getDiscoveredDevices;

- (void) addHandler: (TVAbstractCompanionHandler*) handler;

- (void) sendToConnectedDeviceVodMedia: (TVMediaItem*)media withFile: (TVFile*)file currentProgress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))result;

- (void) sendToConnectedDeviceLiveMedia: (TVMediaItem*)media withFile: (TVFile*)file andProgram: (TVEPGProgram*)program forPlaybackMode: (NSString*)playbackType progress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))result;

- (void) sendMessageToCurrentDevice:(NSString*) message withExtras:(NSDictionary*) extras withCompletion:(void(^)(bool success,NSDictionary* extras)) completion;
- (void) sendEmptyMessageToCurrentDevice:(NSString*) message withCompletion:(void(^)(bool success,NSDictionary* extras)) completion;


@end
