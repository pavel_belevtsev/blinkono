//
//  NSArray+FirstObject.h
//  TVinci
//
//  Created by Avraham Shukron on 7/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (FirstObject)
// Returns the first object in an array or nil if array is empty
-(id) firstObject;
@end
