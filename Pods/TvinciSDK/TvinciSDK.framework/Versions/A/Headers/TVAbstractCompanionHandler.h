//
//  TVNAbstractCompanionHandler.h
//  Tvinci
//
//  Created by Sagi Antebi on 8/24/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVAbstractCompanionDevice.h"
#import <TvinciSDK/TVMediaItem.h>
#import "TVCompanionDiscoveryDelegate.h"

typedef enum {
    CompanionHandlerResultOk,
    CompanionHandlerResultFailure
} CompanionHandlerResult;


@class TVEPGProgram;

@interface TVAbstractCompanionHandler : NSObject

@property (assign) id<TVCompanionDiscoveryDelegate> delegate;

- (int) getType;

- (void) stopScanning;

- (void) scan;



- (void) sendVodMedia: (TVMediaItem*)media toDevice: (TVAbstractCompanionDevice*)device withFile: (TVFile*)file currentProgress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))result;

- (void) sendLiveMedia: (TVMediaItem*)media toDevice:(TVAbstractCompanionDevice*)device withFile: (TVFile*)file andProgram: (TVEPGProgram*)program forPlaybackMode: (NSString*)playbackType progress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))result;

- (void) connect: (TVAbstractCompanionDevice*)device;

- (void) disconnect;

- (void) sendMessage:(NSString*)message withExtras: (NSDictionary*)extras  toDevice:(TVAbstractCompanionDevice*)device withCompletion:(void(^)(bool success,NSDictionary* extras)) completion;

@end
