//
//  TVActivityObject.h
//  TvinciSDK
//
//  Created by Magen Yadid on 9/11/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
#import "TVConstants.h"


@interface TVActivityObject : BaseModelObject

@property (assign, nonatomic) NSInteger  assetID;
@property (assign, nonatomic) NSInteger  objectID;
@property (assign, nonatomic) TVAssetType assetType;
@property (strong, nonatomic) NSString * assetName;
@property (strong, nonatomic) NSString * picUrl;


@end
