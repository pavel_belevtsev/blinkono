//
//  TVChannel.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 9/2/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@interface TVChannel : BaseModelObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger channelID;
@end
