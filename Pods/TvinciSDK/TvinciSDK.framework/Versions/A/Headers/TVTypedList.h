//
//  TVItemList.h
//  TvinciSDK
//
//  Created by Israel Berezin on 1/28/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

typedef enum
{
	TVListMediaItem = 0,
    TVListEPGProgram = 1,
} TVTypedListType;

@interface TVTypedList : BaseModelObject

@property (nonatomic, retain) NSArray * mediaArray;
@property TVTypedListType itemsListType;

@end
