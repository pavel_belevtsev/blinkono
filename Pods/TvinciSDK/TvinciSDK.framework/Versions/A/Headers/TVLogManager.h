//
//  LogManager.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 7/30/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PLAYER_LOG_MESSAGE_ON YES

typedef enum LOG_GROUP
{
    LOG_GROUP_NONE = 0,
    LOG_GROUP_PLAYER = 1<<0,
    LOG_GROUP_CONFIGURATION = 1<<1,
    LOG_GROUP_ALL = LOG_GROUP_PLAYER | LOG_GROUP_CONFIGURATION,
}LOG_GROUP;
NSString * stringForGroup(LOG_GROUP);


typedef enum LOG_LEVEL
{
    LOG_LEVEL_NONE = 0,
    LOG_LEVEL_DEBUG = 1<<0,
    LOG_LEVEL_INFO = 1<<1,
    LOG_LEVEL_WARN = 1<<2,
    LOG_LEVEL_ERROR = 1<<3,
    LOG_LEVEL_FATAL = 1<<4
    
}LOG_LEVEL;
NSString * stringForLevel(LOG_LEVEL);

typedef enum LOG_OUTPUT
{
    LOG_OUTPUT_NONE = 0,
    LOG_OUTPUT_CONSOLE = 1<<0,
    LOG_OUTPUT_POPUP = 1<<1,
    LOG_OUTPUT_FILE = 1<<2,
    LOG_OUTPUT_ALL = LOG_OUTPUT_CONSOLE | LOG_OUTPUT_POPUP | LOG_OUTPUT_FILE,
    
}OUTPUT_TYPE;




@interface TVLogManager : NSObject

+(TVLogManager *) sharedLogManager;
-(void) setOutputMask:(NSUInteger) outputMask;
-(void) setGroupMask:(NSUInteger) groupMask;
-(void) setLevel:(LOG_LEVEL) level;
-(void) sendLogMessage: (NSString *) text group:(LOG_GROUP) group  level:(LOG_LEVEL) level;

@end
