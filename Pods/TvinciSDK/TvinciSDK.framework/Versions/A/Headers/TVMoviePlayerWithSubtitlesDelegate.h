//
//  MoviePlayerWithSubtitlesDelegate.h
//  SubtitlesTest
//
//  Created by Avraham Shukron on 11/21/12.
//  Copyright (c) 2012 quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVSubtitleFactory.h"

@protocol TVMoviePlayerWithSubtitlesDelegate <NSObject>

//to know wich subtitles to show

//time interval of the player in seconds
-(NSTimeInterval) timeIntervalSinceMovieBegin;


@end
