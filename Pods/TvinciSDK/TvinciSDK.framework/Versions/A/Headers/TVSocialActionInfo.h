//
//  TVSocialActionInfo.h
//  TvinciSDK
//
//  Created by Tarek Issa on 4/27/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

//! \brief
//! TVSocialActionInfo supplies an data object that encapsulates the DoUserAction response.
//! \details
//! TVSocialActionInfo is supported for TVP-api 2.7 and above.

@interface TVSocialActionInfo : BaseModelObject {
    
}

@property (nonatomic)           NSInteger externalStatus;
@property (nonatomic)           NSInteger internalStatus;
@property (nonatomic)           NSInteger status;
@property (retain, nonatomic)   NSString *responseID;
@property (retain, nonatomic)   NSString *error;


@end
