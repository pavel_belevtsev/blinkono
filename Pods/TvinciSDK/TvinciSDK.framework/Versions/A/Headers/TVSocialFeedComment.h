//
//  TVSocialFeedComment.h
//  TvinciSDK
//
//  Created by Tarek Issa on 3/31/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVSocialFeedComment : BaseModelObject {
    
}


@property (retain, readonly) NSString *commentBody;
@property (retain, readonly) NSString *commentTitle;
@property (retain, readonly) NSString *creatorName;
@property (retain, readonly) NSDate *createDate;
@property (retain, readonly) NSURL *creatorUrl;
@property (assign, readonly) NSInteger popularityCounter;
@property (retain, readonly) NSArray *subComments;
@property (retain, readonly) NSURL *feedItemLinkUrl;


@end
