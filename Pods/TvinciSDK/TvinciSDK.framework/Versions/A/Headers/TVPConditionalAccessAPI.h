//
//  TVPConditionalAccessAPI.h
//  TvinciSDK
//
//  Created by Alex Zchut on 2/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseTVPClient.h"
#import "TVConstants.h"
#import "TVPConditionalAccessAPIRequestParams.h"



extern NSString * const MethodNameGetDomainServices;
extern NSString * const MethodNameCancelServiceNow;
extern NSString * const MethodNameCancelSubscriptionRenewal;


@interface TVPConditionalAccessAPI : BaseTVPClient

+(TVPAPIRequest *) requestForGetSubscriptionsPricesWithCoupon: (void(^)(RPGetSubscriptionsPricesWithCoupon *requestParams))requestParams;

+(TVPAPIRequest *) requestForGetItemsPricesWithCoupons: (void(^)(RPGetItemsPricesWithCoupons *requestParams))requestParams;

+(TVPAPIRequest *) requestForIsPermittedSubscription: (void(^)(RPIsPermittedSubscription *requestParams))requestParams;

+(TVPAPIRequest *) requestForCancelSubscription: (void(^)(RPCancelSubscription *requestParams)) requestParams DEPRECATED_MSG_ATTRIBUTE("Use requestForGetMediaMarkForMediaID:npvrID instead");

+(TVPAPIRequest *) requestForGetDomainPermittedSubscriptions;


+(TVPAPIRequest *) requestForGetDomainServicesWithDomainID :(NSString *) domainID
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate;

+(TVPAPIRequest *) requestForGetEPGLicensedDataWithFileID:(NSInteger)  fileID
                                               EPGItemID:(NSInteger)  itemID
                                               startTime:(NSDate *)   startTime
                                               basicLink:(NSString*)  basicLink
                                                  userIP:(NSString *) userIP
                                                refferer:(NSString *) refferer
                                              countryCd2:(NSString *) countryCd2
                                           languageCode3:(NSString *) languageCode3
                                              formatType:(EPGLicensedLinkFormatType) format
                                                 delegate:(id<ASIHTTPRequestDelegate>) delegate;



+(TVPAPIRequest *) requestForCancelServiceNowWithDomainID:(NSInteger) domainID
                                                serviceID:(NSInteger) serviceID
                                              serviceType:(TVServiceType) serviceType
                                              forceCancel:(BOOL) forceCancel;

+(TVPAPIRequest *) requestForCancelSubscriptionRenewalWithDomainID:(NSInteger) domainID
                                                         serviceID:(NSInteger) serviceID;




@end
