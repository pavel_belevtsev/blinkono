//
//  PaginationArray.h
//  TvinciSDK
//
//  Created by Israel Berezin on 1/31/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVinci.h"

// --------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- propertys -------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

@interface PaginationArray : NSObject
@property (nonatomic , retain) NSMutableArray * pageCollection;
@property (nonatomic) NSInteger lastDownloadPage;
@property (nonatomic) NSInteger unickKey;
@property (nonatomic, retain) NSString * className;
@property (nonatomic, retain)  NSArray  * mediaItems;
@property (nonatomic) NSInteger numberOfPages;
@property (nonatomic) NSInteger currMainDownloadPage;
@property (nonatomic) NSInteger numOfMinumumPageToDownload;
@property (nonatomic) NSInteger numberOfItemsInPage;
@property (nonatomic) BOOL isDownloadNow;
@property (nonatomic) NSInteger numberOfItems;

// --------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- function --------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

// Init the "PaginationArray" with ClassName and unickNumber (used to build unick save chash name)
// and number of Minimum Page to download if first time.
// Important!!! Immediately after initialization should be the following line:
// paginationMangar.numberOfItemsInPage = NUMBER__OF_ITEMS_TO_DOWNLOAD_IN_PAGE;
-(id)initWithClassName:(NSString*)theClassName andChannelId:(NSInteger)unickNum andMinimumPage:(NSInteger)minPage;

// Chack if downlaod all pages.
-(BOOL)isComplateDownlaod;

// Init "PageCollection" array and set for all indexes EMPTY value.
-(void)initPageCollection;

// Returns the last page is downloaded (calculate it as "pageCollection" array).
-(NSInteger)takeLastPage;

// Replace object state at index in "pageCollection" array.
-(void)replaceObjectAtIndex:(NSInteger)place WithState:(NSInteger)state toChashSave:(BOOL)isSave;

// Add items (medias) to "mediaItems" array.
-(void)addObjectToMediaIems:(NSArray *)items toChaseSave:(BOOL)isSave;

// Return value of object in "PageCollection" array in place index.
-(NSInteger)stateOfObjectInPageCollectionAtIndex:(NSInteger)place;

// Return array with pages that need to download (if have page that not download in place before last download page).
-(NSArray *)SearchPagesThatReadyToDownloadWithLastPage:(NSInteger)lastPage;

// Update value of property "lastDownloadPage" (used "takeLastPage" function).
-(void)updateLastlastDownloadPage;

// Return number of object in "pageCollection" array.
-(NSInteger)pageCollectionCount;

// Chack if need to download this Page.
-(BOOL)isNeedToDownloadPageWithPageNum:(NSInteger)currPageNum andCurrShowPage:(NSInteger)currShowPage; // -not used!
-(BOOL)isNeedToDownloadPageWithPageNum:(NSInteger)currPageNum;

// Updating the array - for the first time know how many pages need to download.
// Use it immediately after receiving the data from first download.
-(void)insertNumberOfPagesAndInitPageCollection;

// Updates and compares the number of received data and "pageCollection" array!
// Need to do this when coming from another page,
// (ie, we have now reached a page from the "back" and not from the menu), or returning from the background.
-(void)pageCollectionUpdate;

// Updates all local prorety that app returning from the background
// Need use it only if save "mediaItem" array in chash!
-(void)restoreDataAppDidBecomeActiveWithMediaItems:(NSArray*)medias;

// Return number of page that need to download, else if not have page return -1.
-(NSInteger)checkIfNeedToDownloadMore;

// Return YES only if all indexes in "pageCollection" array no have value "PreparingToDownload".
-(BOOL)CheckIfCanDownloadMore;

// Puts all the places in the array the value
// Use it when you want to download all the data (one by one) without having to provision a user,
// Or when an order is given Download the last page, to make sure that all the pages before they are downloaded or "PreparingToDownload"
-(void)prepareAllItemsToDownload;


// insert new doawnload media to mediaList array and update other prametares
// and update stateValue for this page in "pageCollection" array
// if page =0, insert Number Of Pages And Init "PageCollection" array
// and preper numOfMinumumPageToDownload to doawnload.
-(void)insertDateToPage:(NSInteger)page withMediaItems:(NSArray*)mediaList toMediaCacheSave:(BOOL)save;

-(void)cancelDownload;


// Chash utility function
-(void)savePageCollectionInChash;
-(void)loadPageCollectionFromChash;
-(void)saveLastDownloadPageInChash;
-(void)loadLastDownloadPageFromChash;
-(void)saveMediaItemsInChash;
-(void)loadMediaItemsFromChash;
-(void)removeMediaItemsFromChash;
-(void)removeLastDownloadPageFromChash;
-(void)removePageCollectionFromChash;
-(void)saveAllInChash;
-(void)loadAllFromChash;
-(void)removeAllInChash;

// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

@end
