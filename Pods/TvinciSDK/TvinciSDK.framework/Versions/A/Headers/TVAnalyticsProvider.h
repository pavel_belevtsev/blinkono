//
//  AnalyticsAbstractProvider.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 12/25/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVAnalyticsEvent.h"

@interface TVAnalyticsProvider : NSObject

-(void) sendEvent :(TVAnalyticsEvent *) event;

@end
