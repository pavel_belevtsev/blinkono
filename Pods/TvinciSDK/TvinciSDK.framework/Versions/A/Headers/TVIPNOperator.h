//
//  TVIPNOperator.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@class TVDisplay;

@interface TVIPNOperator : BaseModelObject

@property (retain, nonatomic) NSString * operatorUsername;
@property (retain, nonatomic) NSString * operatorPassword;
@property (retain, nonatomic) NSString * operatorMenuId;


@property (assign, nonatomic) NSInteger  operatorID;
@property (assign, nonatomic) NSInteger  operatorCoGuid;

@property (assign, nonatomic) NSInteger  operatorSubGroupID;

@property (retain, nonatomic) NSString * operatorName;
@property (assign, nonatomic) NSInteger  operatorType;
@property (retain, nonatomic) NSURL * operatorLoginURL;
@property (retain, nonatomic) NSURL * operatorLogoutURL;
@property (retain, nonatomic) NSString * operatorsScopes;

@property (retain, nonatomic) NSString * operatorAboutUs;
@property (retain, nonatomic) NSString * operatorConrtactUs;
@property (retain, nonatomic) NSString * colorCode;
@property (retain, nonatomic) NSURL * picURL;

@property (retain, nonatomic) TVDisplay * operatorsDisplay;








@end
