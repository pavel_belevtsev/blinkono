//
//  TVTokenizationManager.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/29/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVBaseNetworkObject.h"

@class TVTokenizationManager;

@protocol TVTokenizationManagerDelegate <NSObject>
-(void) tokenizationManager:(TVTokenizationManager *) sender tokenDidChanged:(NSString *) token;
@end


@interface TVTokenizationManager : TVBaseNetworkObject

@property (nonatomic, weak) id<TVTokenizationManagerDelegate> delegate;
@property (nonatomic, strong) NSString * tokenizationAppID;
@property (nonatomic, strong) NSString * tokenizationAppSecret;


-(BOOL) isTokenExpired;
-(BOOL) onTokenRefreshProcess;
-(void) generateAccessTokenWithStartBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock;
-(void) refreshAccessTokenWithStartBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock;

@end
