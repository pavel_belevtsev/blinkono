//
//  NSDictionary+TVHandling.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/1/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (TVHandling)


-(NSArray *) wrappedToArray;
@end
