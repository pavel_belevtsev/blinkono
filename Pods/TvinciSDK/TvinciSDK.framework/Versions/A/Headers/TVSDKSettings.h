//
//  TVSDKSettings.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface TVSDKSettings : NSObject

@property (assign, nonatomic) BOOL offlineMode;



+ (id) sharedInstance;
@end
