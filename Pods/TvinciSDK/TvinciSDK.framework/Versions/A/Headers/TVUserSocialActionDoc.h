//
//  TVUserSocialActionDoc.h
//  TvinciSDK
//
//  Created by Magen Yadid on 9/10/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
#import "TVConstants.h"
#import "TVActivityObject.h"
#import "TVActivitySubject.h"
#import "TVActivityVerb.h"

@interface TVUserSocialActionDoc : BaseModelObject


@property (strong, nonatomic) NSString * mediaId;
@property (strong, nonatomic) NSString * userSiteGuid;
@property (assign, nonatomic) TVSocialPlatform socialPlatform ;
@property (strong, nonatomic) NSString * docType ;
@property (strong, nonatomic) NSDate *createDate;
@property (strong, nonatomic) NSDate *lastUpdate;
@property (nonatomic) BOOL isActive;
@property (nonatomic) BOOL permitSharing;

@property (strong,nonatomic) TVActivitySubject * activitySubject;
@property (strong,nonatomic) TVActivityObject * activityObject;
@property (strong,nonatomic) TVActivityVerb * activityVerb;

@end