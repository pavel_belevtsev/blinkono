//
//  TVItemPrice.h
//  TvinciSDK
//
//  Created by Tarek Issa on 11/27/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVItemsPrices : BaseModelObject {
    
}


@property (nonatomic, assign) NSInteger fileID;
@property (nonatomic, retain) NSString* productCode;
@property (nonatomic, retain) NSArray* itemsPrices;

@end

extern NSString *const TVItemPrice_nMediaFileIDKey;
extern NSString *const TVItemPrice_oItemPricesKey;
extern NSString *const TVItemPrice_sProductCode;

