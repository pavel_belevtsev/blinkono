//
//  TVEPGProgram.h
//  TvinciSDK
//
//  Created by Quickode Ltd. on 8/26/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModelObject.h"

@class TVPictureSize;

@interface TVEPGProgram : BaseModelObject <NSCoding>

@property (nonatomic, copy) NSString *EPGChannelID;
@property (nonatomic, copy) NSString *EPGIdentifier;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, retain) NSDate *startDateTime;
@property (nonatomic, retain) NSDate *endDateTime;
@property (nonatomic, retain) NSURL *pictureURL;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *isActive;
@property (nonatomic, copy) NSString *groupID;
@property (nonatomic, copy) NSString *updaterID;
@property (nonatomic, retain) NSDate *updateDate;
@property (nonatomic, retain) NSDate *publishDate;
@property (nonatomic, retain) NSDate *createDate;
@property (nonatomic, copy) NSString *EPGTag;
@property (nonatomic, copy) NSString *mediaID;
@property (nonatomic, copy) NSString *EPGId;
@property (nonatomic, retain) NSString * documentID;

@property NSInteger likeConter;

@property (nonatomic, retain) NSDictionary *tags;
@property (nonatomic, retain) NSDictionary *metaData;

@property (nonatomic, retain) NSArray *cleanTags;
@property (nonatomic, retain) NSArray *cleanMetaData;

@property (nonatomic, retain) NSDictionary *preseredTags;
@property (nonatomic, retain) NSDictionary *preseredMetaData;

-(NSString *) asString;
-(BOOL) programFinished;
-(BOOL) programIsOn;
-(BOOL) isPlaying;

-(NSURL *) pictureURLForPictureSize : (TVPictureSize *) size;
-(NSURL *) pictureURLForSize : (CGSize ) size;
-(NSURL *) pictureURLForSizeWithRetinaSupport:(CGSize)size;

+(NSDateFormatter *) programDurationDateFormatter;
+(NSDateFormatter *) programUpdateDateFormatter;
+(void) setProgramDurationDateFormatter:(NSDateFormatter *) formmater;
+(void) setProgramUpdateDateFormatter:(NSDateFormatter *) formmater;
+(void) setTimeReferenceToGMT;



@end

extern NSString *const TVEPGProgramEPGChannelIDKey;
extern NSString *const TVEPGProgramEPGIdentifierKey;
extern NSString *const TVEPGProgramNameKey;
extern NSString *const TVEPGProgramDescriptionKey;
extern NSString *const TVEPGProgramStartDateKey;
extern NSString *const TVEPGProgramEndDateKey;
extern NSString *const TVEPGProgramPicURLKey;
extern NSString *const TVEPGProgramStatusKey;
extern NSString *const TVEPGProgramIsActiveKey;
extern NSString *const TVEPGProgramGroupIDKey;
extern NSString *const TVEPGProgramUpdaterIDKey;
extern NSString *const TVEPGProgramUpdateDateKey;
extern NSString *const TVEPGProgramPublishDateKey;
extern NSString *const TVEPGProgramCreateDateKey;
extern NSString *const TVEPGProgramEPGTagKey;
extern NSString *const TVEPGProgramMediaIDKey;
extern NSString *const EpgMediaItemTagsKey;
extern NSString *const EpgMediaItemMetaDataKey;
extern NSString *const EpgTVMediaItemTagTitleKey;
extern NSString *const EpgTVMediaItemTagValuesKey;
extern NSString *const TVEPGProgramEPGLikeCounterKey;
extern NSString *const TVEPGProgramEPGIDKey;