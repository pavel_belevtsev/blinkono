//
//  NSArray+TVHandling.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/11/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (TVHandling)

-(NSArray *) wrappedArray;

@end
