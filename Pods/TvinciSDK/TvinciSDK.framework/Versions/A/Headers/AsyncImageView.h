//
//  ASImageView.h
//  ASImageView
//
//  Created by Avraham Shukron on 1/1/11.
//  Copyright 2011 Avraham Shukron. All rights reserved.
//
//  
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <UIKit/UIKit.h>

@protocol ASImageViewDelegate;
@interface AsyncImageView : UIImageView 
@property (nonatomic, readonly, retain) NSURL *imageURL;

// If YES, the view will show a centered activity indicator while the photo loads. You can customize the
// appearance of the activity indicator using through the activityIndicator property. The indicator is lazy-loaded
// after showsLoadingActivity is set to YES
@property (nonatomic, assign) BOOL showsLoadingActivity; 
@property (nonatomic, readonly, retain) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, assign) IBOutlet id<ASImageViewDelegate> delegate;

@property (nonatomic,retain) UIImage * myPlaceholderImage;
@property (nonatomic,retain) NSURL * placeHolderURL;
@property (nonatomic, assign) BOOL placeHolderURLWasUsed;



- (void)loadImageAtURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage;
- (void)loadImageAtURL:(NSURL *)url andPlaceholderImage:(UIImage *)placeholderImage;
- (void)loadImageAtURL:(NSURL *)url placeholderURL:(NSURL *) placeHolderURL andPlaceholderImage:(UIImage *)placeholderImage;

@end

@protocol ASImageViewDelegate <NSObject>
@optional
-(void) asyncImageView : (AsyncImageView *) sender didFailToLoadImageWithError : (NSError *) error;
-(void) asyncImageView : (AsyncImageView *) sender didFinishLoadingImage : (UIImage *) image;

-(void) unregisterFromNotificationCenter;


@end
