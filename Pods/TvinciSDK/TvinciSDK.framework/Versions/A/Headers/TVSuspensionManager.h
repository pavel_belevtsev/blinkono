//
//  TVSuspensionManager.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 5/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

#define NotificationNameSuspendedStatusChanged @"NotificationNameSuspendedStatusChanged"


@interface TVSuspensionManager : TVBaseNetworkObject

@property (assign, nonatomic) bool isSuspended;

+ (id) sharedInstance;
-(void) startTrakingForSuspnesionWithCompletion:(void(^)()) completion;
-(BOOL) scanForSuspensionForAPIResponse:(id) response;


@end
