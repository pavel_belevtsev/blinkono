//
//  MethodsURLPipeline.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 10/2/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MethodsURLPipeline : NSObject

+ (instancetype)sharedMethodsURLPipeline;
+ (void) becomeDefaultPipline:(MethodsURLPipeline*) defaultPipeIne;
-(NSURL *) transferURL:(NSURL *)url methodName:(NSString *) methodName;
@end
