//
//  TVGalleryItem.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/2/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

/*!
 * @class TVGalleryItem 
 * @abstract represents a single item inside a TVGallery
 */




@interface TVGalleryItem : BaseModelObject
@property (nonatomic, assign, readonly) long galleryID;
@property (nonatomic, assign , readonly) long itemID;
@property (nonatomic, assign) long TVMChannelID;
@property (nonatomic, assign) long mainPicture;
@property (nonatomic, assign) NSInteger numberOfItemsPerPage;
@property (nonatomic, assign) NSInteger numberOfItems;
@property (nonatomic, assign) BOOL isPoster;
@property (nonatomic, assign) CGSize pictureSize;
@property (nonatomic, copy) NSString *viewType;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *link;
@property (nonatomic, copy) NSString *mainDescription;
@property (nonatomic, copy) NSString *culture;
@end