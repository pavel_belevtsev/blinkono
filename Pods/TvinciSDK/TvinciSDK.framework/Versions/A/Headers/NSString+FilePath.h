//
//  NSString+FilePath.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/15/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FilePath)

+(NSString *) stringfilePathForCashesDirectoryWithName:(NSString *) name extension:(NSString *) extension;

@end
