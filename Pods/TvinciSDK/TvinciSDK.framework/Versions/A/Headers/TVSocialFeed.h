//
//  TVSocialFeed.h
//  TvinciSDK
//
//  Created by Tarek Issa on 3/31/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVSocialFeed : BaseModelObject {
    
}


@property (retain,readonly) NSArray *facebookCommentsArr;
@property (retain,readonly) NSArray *twitterCommentsArr;
@property (retain,readonly) NSArray *inAppCommentsArr;


@end
