//
//  TVUser.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/8/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"


/**************
 kRememberMeState
 -----------------
 NONE - Remember me is totally OFF
 ACTIVE_NOT_VERIFIED - ON - but didn't login yet - so need to login at least once before can skip
 VERIFIED - can login directly
 
 * Used outside this class since this class is only about User data from server and RememberMe status is local
 
 **************/

enum kRememberMeState {
    kRememberMeState_NONE=1,
    kRememberMeState_NOT_ACTIVE,
    
    kRememberMeState_ACTIVE_NOT_VERIFIED,
    kRememberMeState_VERIFIED,
};

enum kPreferedLoginType{
    kPreferedLoginType_Unknown = 0,
    kPreferedLoginType_Registered = 1,
    kPreferedLoginType_Facebook,
    
};

@interface TVUser : BaseModelObject 

@property (nonatomic , copy) NSString *username;
@property (nonatomic , copy) NSString *firstName;
@property (nonatomic , copy) NSString *lastName;
@property (nonatomic , copy) NSString *email;
@property (nonatomic , copy) NSString *address;
@property (nonatomic , copy) NSString *city;
@property (nonatomic , copy) NSString *state;
@property (nonatomic , retain) NSDictionary *country;
@property (nonatomic , copy) NSString *zipCode;
@property (nonatomic , copy) NSString *phoneNumber;
@property (nonatomic , copy) NSString *facebookID;
@property (nonatomic , copy) NSString *facebookImage;
@property (nonatomic , copy) NSString *affiliateCode;
@property (nonatomic , assign) BOOL isFacebookImagePermitted;
@property (nonatomic , retain) NSDictionary *dynamicData;
@property (nonatomic , retain) NSDictionary *basicData;
@property (nonatomic , copy) NSString *siteGUID;
@property(readwrite, assign) enum kPreferedLoginType preferedLoginType;
@property (nonatomic, assign) BOOL isAllowNewsLetter;
@property (retain, nonatomic) NSString * coGuid;
@property (retain, nonatomic) NSString * externalToken ;
@property (retain, nonatomic) NSString * faceBookToken;
@property (retain, nonatomic) NSDictionary * userType ;
@property (nonatomic , copy)   NSString * purchasePin;
@property (nonatomic , assign) NSInteger  purchasePinEnable;
@property (nonatomic, retain) NSString * twitterToken;
@property (nonatomic, retain) NSString * twitterTokenSecret;
@property (nonatomic, assign) BOOL isSuspended;


-(NSURL *) facebookPictureURLForSizeType : (NSString *) size;
// oded need to fix the requested keys in setuserdata request that will match the getuserdata request till then we can't use the original keyValueRepresentation.
-(NSDictionary *) keyValueRepresentationTemporary;

@end

extern NSString *const TVUserFacebookPictureTypeSmall;
extern NSString *const TVUserFacebookPictureTypeNormal;
extern NSString *const TVUserFacebookPictureTypeLarge;
extern NSString *const TVUserFacebookPictureTypeSquare;

extern NSString *const TVUserUsernameKey;
extern NSString *const TVUserFirstNameKey;
extern NSString *const TVUserLastNameKey;
extern NSString *const TVUserEmailKey;
extern NSString *const TVUserAddressKey;
extern NSString *const TVUserCityKey;
extern NSString *const TVUserStateKey;
extern NSString *const TVUserCountryKey;
extern NSString *const TVUserZipCodeKey;
extern NSString *const TVUserPhoneNumberKey;
extern NSString *const TVUserFacebookIDKey;
extern NSString *const TVUserFacebookImageKey;
extern NSString *const TVUserAffiliateCodeKey;
extern NSString *const TVUserIsFacebookImagePermittedKey;
extern NSString *const TVUserDynamicDataKey;
extern NSString *const TVUserBasicInfoKey;
extern NSString *const TVUserSiteGUIDKey;
extern NSString *const TVUSerPreferedLoginType;
extern NSString *const TVUserPurchasePinKey;
extern NSString *const TVUserPurchasePinEnableKey;