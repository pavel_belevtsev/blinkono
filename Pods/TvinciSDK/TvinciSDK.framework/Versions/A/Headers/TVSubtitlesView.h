//
//  subtitlesView.h
//  SubtitlesTest
//
//  Created by Avraham Shukron on 11/21/12.
//  Copyright (c) 2012 quickode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVMoviePlayerWithSubtitlesDelegate.h"
#import "TVSubtitleFactory.h"

@interface TVSubtitlesView : UILabel<TVSubtitleFactoryDelegate>

@property (nonatomic, assign) id<TVMoviePlayerWithSubtitlesDelegate> delegate;// after init
@property (nonatomic, retain) TVSubtitleFactory * subtitlesManager;
@property (nonatomic, retain) NSString * subtitleFile; //first
@property(readwrite, assign) BOOL subsAreOff;

-(void) startShowingSubtitles; //secound
-(void) stopShowingSubtitles;
-(void) removeDelegates;
@end
