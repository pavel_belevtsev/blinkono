//
//  TVPConditionalAccessAPIRequestParams.h
//  TvinciSDK
//
//  Created by Alex Zchut on 2/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPGetSubscriptionsPricesWithCoupon : NSObject

@property (nonatomic, strong) NSArray *arrSubscriptionIds;
@property (nonatomic, strong) NSString *userGuid;
@property (nonatomic, strong) NSString *couponCode;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *deviceName;
@property (nonatomic, strong) NSString *languageCode;

@end

@interface RPGetItemsPricesWithCoupons : NSObject

@property (nonatomic, strong) NSArray *fileIDsArray;
@property (nonatomic, strong) NSString *userGuid;
@property (nonatomic, strong) NSString *couponCode;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *deviceName;
@property (nonatomic, strong) NSString *languageCode;
@property (nonatomic, readwrite) BOOL onlyLowest;

@end

@interface RPIsPermittedSubscription : NSObject

@property (nonatomic, readwrite) NSInteger subId;

@end

@interface RPCancelSubscription : NSObject

@property (nonatomic, readwrite) NSInteger subId;
@property (nonatomic, readwrite) NSInteger purchaseId;

@end