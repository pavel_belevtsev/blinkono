//
//  TVSubscriptionData.h
//  TvinciSDK
//
//  Created by Alex Zchut on 2/21/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

//m_sCodes
@protocol TVSubscriptionData_codes @end
@interface TVSubscriptionData_codes : JSONModel

@property (nonatomic, readwrite) NSInteger code;
@property (nonatomic, retain) NSString *name;

@end

//m_sName
@protocol TVSubscriptionData_name @end
@interface TVSubscriptionData_name : JSONModel

@property (nonatomic, retain) NSString *languageCode3, *value;

@end

//m_oPrise > m_oCurrency
@interface TVSubscriptionData_prise_currency : JSONModel

@property (nonatomic, retain) NSString *currencyCD3, *currencyCD2, *currencySign;
@property (nonatomic, readwrite) NSInteger currencyId;

@end

//m_oPrise
@interface TVSubscriptionData_prise : JSONModel

@property (nonatomic, readwrite) CGFloat price;
@property (nonatomic, retain) NSString<Optional> *name;
@property (nonatomic, retain) TVSubscriptionData_prise_currency *currency;

@end

//m_oSubscriptionPriceCode
@interface TVSubscriptionData_subscriptionPriceCode : JSONModel

@property (nonatomic, readwrite) NSInteger code;
@property (nonatomic, retain) NSString<Optional> *name;
@property (nonatomic, retain) TVSubscriptionData_prise *prise;
@property (nonatomic, readwrite) NSInteger objectId;
@property (nonatomic, retain) NSString<Optional> *description1;

@end

//m_oSubscriptionPriceCode
@interface TVSubscriptionData_subscriptionUsageModule : JSONModel

@property (nonatomic, readwrite) NSInteger waiver, couponId, deviceLimitId, extDiscountId, internalDiscountId, isRenew, maxNumberOfViews, objectId, waiverPeriod, numOfRecPeriods, pricingId, maxUsageModuleLifeCycle, viewLifeCycle, type;
@property (nonatomic, readwrite) BOOL isSubscriptionOnly;
@property (nonatomic, readwrite) BOOL isOfflinePlayback;
@property (nonatomic, retain) NSString *virtualName;

@end

//m_oSubscriptionPriceCode
@interface TVSubscriptionData_purchasingInfo : JSONModel

@property (nonatomic, readwrite) NSInteger subscriptionCode, maxUses, currentUses, paymentMethod, subscriptionPurchaseId;
@property (nonatomic, retain) NSString *stringCurrentDate, *stringLastViewDate, *stringPurchaseDate, *stringEndDate;
@property (nonatomic, readwrite) BOOL recurringStatus, isSubRenewable;
@property (nonatomic, retain) NSString<Optional> *deviceUDID, *deviceName;

@property (nonatomic, retain, readonly) NSDate<Ignore> *lastViewDate, *currentDate, *purchaseDate, *endDate;

@end

@class TVMediaItem;
@interface TVSubscriptionData : JSONModel

@property (nonatomic, retain) NSArray<TVSubscriptionData_codes ,ConvertOnDemand ,Optional> *codes;
@property (nonatomic, retain) NSString *stringStartDate;
@property (nonatomic, retain) NSString *stringEndDate;

@property (nonatomic, retain) NSArray<Optional> *fileTypes;
@property (nonatomic, readwrite) BOOL isRecurring;
@property (nonatomic, readwrite) NSInteger numberOfRecPeriods;
@property (nonatomic, retain) TVSubscriptionData_subscriptionPriceCode *subscriptionPriceCode;
@property (nonatomic, retain) NSArray<TVSubscriptionData_name ,ConvertOnDemand> *arrName, *arrDescription;
@property (nonatomic, retain) TVSubscriptionData_subscriptionUsageModule *subscriptionUsageModule;
@property (nonatomic, readwrite) NSInteger priority;
@property (nonatomic, retain) NSString *productCode;
@property (nonatomic, retain) NSString *subscriptionCode;
@property (nonatomic, retain) TVSubscriptionData_subscriptionPriceCode<Optional> *priceCode;
@property (nonatomic, retain) NSDictionary *usageModule;
@property (nonatomic, retain) NSDictionary *discountModule;
@property (nonatomic, retain) NSDictionary *couponsGroup;
@property (nonatomic, readwrite) NSInteger objectCode;
@property (nonatomic, retain) NSString *objectVirtualName;
@property (nonatomic, readwrite) BOOL subscriptionOnly;
@property (nonatomic, retain) TVMediaItem<Ignore> *virtualMediaItem;
@property (nonatomic, retain) TVSubscriptionData_purchasingInfo<Ignore> *purchasingInfo;

@end

