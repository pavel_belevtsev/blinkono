//
//  MediaAssetsStats.h
//  TvinciSDK
//
//  Created by Israel on 4/1/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"


@interface MediaAssetsStats : BaseModelObject

@property NSInteger rate;
@property NSInteger assetId;
@property NSInteger likes;
@property NSInteger views;
@property NSInteger votes;

//@property TVAssetType assetType;

@end

extern const NSString * AssetTypekey;
