//
//  UserActionResponse.h
//  TvinciSDK
//
//  Created by Israel Berezin on 4/2/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface UserActionResponse : BaseModelObject

@property TVSocialActionResponseStatus externalStatus;
@property TVSocialActionResponseStatus internalStatus;

@property BOOL isExternalSecssesd;
@property BOOL isInternalSecssesd;

-(NSString*)takeExternalFailureReason;
-(NSString*)takeInternalFailureReason;

@end
