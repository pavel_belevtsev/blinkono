//
//  NSURL+SpaciousIgnore.h
//  TvinciSDK
//
//  Created by Kaltura Support on 12/11/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (TVNSURLUtilities)

-(NSString *)stringByTrimmingSpaces;


@end
