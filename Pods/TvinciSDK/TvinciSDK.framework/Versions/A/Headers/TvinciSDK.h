//
//  TvinciSDK.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 7/31/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVinciUI.h"
#import "TVinci.h"


@interface TvinciSDK : NSObject
+(void) takeOff;


@end
