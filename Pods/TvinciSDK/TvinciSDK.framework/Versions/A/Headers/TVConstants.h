//
//  Media Constants.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/2/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define kAngaOrangeColor [UIColor colorWithRed:0.898 green:0.49 blue:0.02 alpha:1]
#define kColor_dbdbdb [UIColor colorWithRed:0.859 green:0.859 blue:0.859 alpha:1]
#define kColor_6d6d6d [UIColor colorWithRed:0.427 green:0.427 blue:0.427 alpha:1]
#define kColor_c9c8c8 [UIColor colorWithRed:0.788 green:0.784 blue:0.784 alpha:1]
#define kColor_e28821 [UIColor colorWithRed:0.886 green:0.533 blue:0.129 alpha:1]


extern NSString * const TVErrorKey;
extern NSString * const TVImageSizeFull;

extern NSString * const TVPageLayoutHome;
extern NSString * const TVPageLayoutMyZone;
extern NSString * const TVPageLayoutUserDetailLabel;
extern NSString * const TVPageLayoutAllMedias;
extern NSString * const TVPageLayoutVOD;
extern NSString * const TVPageLayoutLogOut;
extern NSString * const TVPageLayoutSignIn;
extern NSString * const TVPageLayoutEPG;
extern NSString * const TVPageLayoutMovies;
extern NSString * const TVPageLayoutShows;
extern NSString * const TVPageLayoutRecordings;
extern NSString * const TVPageLayoutLive;
extern NSString * const TVPageLayoutLiveTV;
extern NSString * const TVPageLayoutSpotlight2;
extern NSString * const TVPageLayoutSpotlight4;
extern NSString * const TVPageLayoutGrid2x2;
extern NSString * const TVPageLayoutGrid3x3;
extern NSString * const TVPageLayoutSpotlight2X3P4;


extern NSString * const TVNotificationName_AudioLanguageChanged;
extern NSString *const TVNotificationName_BackPressedOnCustomIPhoneNav;
extern NSString *const kUserInfoKey_AudioLanguageSelectedIndex;
extern NSString * const TVNotificationName_FinishedSeeingMovie;

extern NSString * const TVNotificationName_MasterAboutToRevealContent;
extern NSString * const TVNotificationInfoKey_RevealSide;
extern NSString * const TVNotificationInfoValue_RevealLeft ;
extern NSString * const TVNotificationInfoValue_RevealRight;
extern NSString * const TVNotificationInfoValue_RevealTop ;
extern NSString * const TVNotificationInfoValue_RevealBottom ;
extern NSString * const TVNotificationInfoValue_RevealCenter;
extern NSString * const TVNotificationInfoKey_LastContentPosition;

extern NSString *const kPackageInfoKey_isRenewing;
extern NSString *const kPackageInfoKey_endDate ;

extern NSString * const TVNotificationName_AppWillResigneActive;
extern NSString * const TVNotificationName_AppDidBecomeActive;

extern NSString * const kNotificationName_WillCloseLiteBox;
extern NSString *const kUserInfoKey_LiteBoxClosingType;
extern NSString *const kUserInfoValue_LiteBoxClosingType_Close;
extern NSString *const kUserInfoValue_LiteBoxClosingType_PlayFull;
extern NSString *const kUserInfoValue_LiteBoxClosingType_PlayTrailer;
extern NSString *const kUserInfoValue_TVMediaItemViewController_back;

extern NSString * const TVNotificationName_LiveMediaCreated;


extern NSString * const TVNotificationCompanionDevicePaired;
extern NSString * const TVNotificationCompanionDeviceUnPaired;

extern NSString * const TVNotificationInfoKey_UIGestureRecognizerStateBegan;
extern NSString * const TVNotificationName_MasterAboutUIGestureRecognizerStateBegan;

extern NSString * const TVNotificationName_FinishedMediaPlayOnFullScreenWithCustom;
extern NSString * const kNotificationName_HomePageLoadPage;

extern NSString * const kNotificationName_SwipedUpWith2Fingures;
extern NSString * const kNotificationName_SwipedDownWith2Fingures;


extern NSString * const kSessionFlag_KANGUROO;
extern NSString * const kSessionFlag_AppForIphone;

extern NSString * const kSessionFlag_UsingOmniture;
extern NSString * const kSessionFlag_UsingGoogleAnalitics;

extern NSString * const kSessionFlag_IgnoreConcurent;
extern NSString * const kSessionFlag_ShowSwipesInstructions;
extern NSString * const kSessionFlag_UseDeviceFamilyID_Tablet;
extern NSString * const kSessionFlag_UseDeviceFamilyID_Smartphone;
extern NSString * const kSessionFlag_ToggleDevice;

extern NSString * const kSessionFlag_EnableBrowseMode;
extern NSString * const kSessionFlag_BrowseModeKeepingLocationWhenLogingOutOrIn;


extern NSString * const kSessionFlag_UsingStartOver;

extern NSString * const kSessionFlag_AppLanguage_Germen;
extern NSString * const kSessionFlag_CompanionModeEnabled;

extern NSString * const kSessionFlag_UseNavLogo;
extern NSString * const kSessionFlag_UseNavDarkBG;
extern NSString * const kSessionFlag_UseNavDarkBackBt;

extern NSString * const kSessionFlag_UseDefaultLoginLogoutIconNames;

extern NSString * const kSessionFlag_UseStyleOrange;

extern NSString * const kSessionFlag_UseBusinessModelForSpotlightTag;

extern NSString * const kSessionFlag_UseSplash4inchsImage;

extern NSString * const kSessionFlag_MediaPlayer_SkipCheckingRules;

extern NSString * const  kSessionFlag_PRESENTATION_replaceLiveURL;
extern NSString * const  kSessionFlag_PRESENTATION_replaceVodUrlStart;

extern NSString * const kSessionFlag_SpotlightUsePromotionText;

extern NSString * const kSessionAddedData_RandomSessionID;
extern NSString * const kSessionAddedData_CustomLiveURL;
extern NSString * const kSessionAddedData_CustomVodUrlStartReplace;
extern NSString * const kSessionAddedData_4InchesSplashImageName;
extern NSString * const kSessionAddedData_4InchesSplashImagePositionY;
extern NSString * const kSessionAddedData_PlayerDoneBtContentEdgeInsets;
extern NSString * const kSessionAddedData_MediaItemKey_iPhoneTrailer;

extern NSString * const kCellInfoDictKey_ImageSize;

extern NSString * const kSessionFlag_UseDeviceFamilyForToggle;
extern NSString * const TVNotificationInfoKey_WillAnimateRotation;

extern NSString * const kCellInfoDictKey_titleFrame_originX;
extern NSString * const kCellInfoDictKey_titleFrame_originY;
extern NSString * const kCellInfoDictKey_titleIsHidden;
extern NSString * const kCellInfoDictKey_SeconderyTitleFrame_originY;
extern NSString * const kCellInfoDictKey_titleFrame_sizeHeight;
extern NSString * const kCellInfoDictKey_addFrameToSelectedCell;
extern NSString * const kCellInfoDictKey_bgColorForTitle;
extern NSString * const kCellInfoDictKey_titleFrame_numberOfLines;
extern NSString * const kCellInfoDictKey_tileIsAnimated;
extern NSString * const kCellInfoDictKey_SelectedFrameHeight;
extern NSString * const kCellInfoDictKey_SelectedPictureFrameHeight;

extern NSString * const kItemInfoDictKey_nibName;
extern NSString * const kItemInfoDictKey_pictureSizeName;
extern NSString * const kItemInfoDictKey_ShowDesription;

extern NSString *const kUserInfo_myZoonOpenNow;

extern NSString *const TVNotificationName_LoginApper;

extern NSString * const TVNotificationInfo_MenuComplateDownlaud;

extern NSString * const TVNotificationName_myAppDidBecomeActive;


extern NSString *const kUserDefaultsKey_guestSeenSwipesInstructions;
extern NSString *const kUserDefaultsKey_liveMinutesOffset;
extern NSString *const kUserDefaultsKey_companionModeDeviceIP;


extern NSString * const TVCompanionMixedNotificationDeviceDiscovery;
extern NSString * const TVCompanionMixedNotificationDeviceConnected;
extern NSString * const TVCompanionMixedNotificationDeviceDisconnected;


#define kAPIResponseKey_UserTypeKey @"m_eUserType"
typedef enum TVUserType
{
    TVUserType_HOUSEHOLD,
    TVUserType_PERSONAL,

} TVUserType;


#define kAPIResponseKey_DomainResponseStatusKey @"m_oDomainResponseStatus"

typedef enum{
    kDomainResponseStatus_LimitationPeriod=0,
    kDomainResponseStatus_UnKnown=1,
    kDomainResponseStatus_Error=2,
    kDomainResponseStatus_DomainAlreadyExists=3,
    kDomainResponseStatus_ExceededLimit=4,
    kDomainResponseStatus_DeviceTypeNotAllowed=5,
    kDomainResponseStatus_DeviceNotInDomin=6,
    kDomainResponseStatus_DeviceNotExists=7,
    kDomainResponseStatus_DeviceAlreadyExists=8,
    kDomainResponseStatus_UserNotExistsInDomain=9,
    kDomainResponseStatus_OK=10,
    kDomainResponseStatus_ActionUserNotMaster = 11,
    kDomainResponseStatus_UserNotAllowed = 12,
    kDomainResponseStatus_ExceededUserLimit = 13,
    kDomainResponseStatus_NoUsersInDomain = 14,
    kDomainResponseStatus_UserExistsInOtherDomains = 15,
    kDomainResponseStatus_DomainNotExists = 16,
    kDomainResponseStatus_HouseholdUserFailed = 17,
    kDomainResponseStatus_DeviceExistsInOtherDomains = 18,
    kDomainResponseStatus_DomainNotInitialized = 19,
    kDomainResponseStatus_RequestSent = 20,
    kDomainResponseStatus_DeviceNotConfirmed = 21,
    kDomainResponseStatus_RequestFailed = 22,
    kDomainResponseStatus_InvalidUser = 23,
    kDomainResponseStatus_ConcurrencyLimitation = 24,
    kDomainResponseStatus_MediaConcurrencyLimitation = 25,
    kDomainResponseStatus_DomainSuspended = 26,
    
} kDomainResponseStatus;

typedef enum
{
    TagTypeNone = 0,
    TagTypeGenre,
    TagTypeDirectors,
    TagTypeCast,
    TagTypeLand,
    TagTypeTags
} TagType;

typedef enum
{
    TVPageSizeDefault = 10
}TVPageSize;

typedef enum
{
    kDeviceBrand_iPhone = 1,
    kDeviceBrand_iPad=3,
    
} kDeviceBrand;

typedef enum
{
    kDeviceFamilyID_Tablet =4,
    kDeviceFamilyID_Smartphone =1,
    
} kDeviceFamilyID;

typedef enum
{
	TVPriceTypePPVPurchased = 0,
	TVPriceTypeFree = 1,
	TVPriceTypeForPurchaseSubscriptionOnly = 2,
	TVPriceTypeSubscriptionPurchased = 3,
	TVPriceTypeForPurchase = 4,
    TVPriceTypeUnknown = 5,
	TVPriceTypeSubscriptionPurchasedWrongCurrency = 6,
	TVPriceTypePrepaidPurchased = 7,
    TVPriceTypeGeoCommerceBlocked = 8,
    TVPriceTypeEntitledToPreviewModule = 9,
    TVPriceTypeFirstDeviceLimitation = 10
    
} TVPriceType;

/*!
 @abstract list the supported social actions
 
 @constant TVSocialActionUnknown Unknown
 @constant TVSocialActionLike Like
 @constant TVSocialActionUnknown Unlike
 @constant TVSocialActionShare Share action
 @constant TVSocialActionPost Post
 */
typedef enum
{
    TVSocialActionUnknown = 0,
    TVSocialActionLike,
    TVSocialActionUnlike,
    TVSocialActionShare,
    TVSocialActionPost
}TVSocialAction;


typedef enum
{
    TVAlertTag_NotSpecified = 0,
    TVAlertTag_ParentControlPinInput = 10,
    TVAlertTag_WorkingOn3G = 30,
    TVAlertTag_PurchaseMediaDetailsNotAvailableAtTheMoment=40,
    TVAlertTag_PurchaseMediaWasSuccessFull=41,
    TVAlertTag_PurchaseMediaFailed=42,
    
}TVAlertTag;


/*!
 @function TVNameForSocialAction
 @param action The action that you want its name
 @return The name of the passed action
 */
NSString * TVNameForSocialAction(TVSocialAction action);

typedef enum
{
    TVMediaActionRate,
    TVMediaActionVote,
    TVMediaActionRecommend,
    TVMediaActionShare,
    TVMediaActionAddToFavorites,
    TVMediaActionRemoveFromFavorites,
    TVMediaActionComment,
    TVMediaActionRecord,
    TVMediaActionReminder,
    TVMediaActionWatch,
}TVMediaAction;
NSString *TVNameForMediaAction(TVMediaAction action);

typedef enum
{
    TVMediaPlayerActionNone = 0,
    TVMediaPlayerActionStop,
    TVMediaPlayerActionFinish,
    TVMediaPlayerActionPause,
    TVMediaPlayerActionPlay,
    TVMediaPlayerActionFirstPlay,
    TVMediaPlayerActionLoad,
    TVMediaPlayerActionSwoosh,
    TVMediaPlayerActionBitRateChanged
}TVMediaPlayerAction;
NSString *TVNameForMediaPlayerAction(TVMediaPlayerAction action);

/*!
 @abstract list the supported social platforms
 */
typedef enum
{
    TVSocialPlatformUnknown = 0,
    TVSocialPlatformFacebook,
    TVSocialPlatformGoogle
}TVSocialPlatform;
NSString *TVNameForSocialPlatform(TVSocialPlatform platform);

/*!
 @abstract the available sorting options
 */
typedef enum
{
    TVOrderByNoOrder = 0,
    TVOrderByDateAdded,
    TVOrderByNumberOfViews,
    TVOrderByRating,
    TVOrderByName,
    TVOrderByMeta
}TVOrderBy;
NSString *TVNameForOrderBy(TVOrderBy orderBy);

//MultiFilter
/*!
 @abstract the available sorting options
 */
typedef enum
{
    TVMFOrderById = 0,
    TVMFOrderByRelated,
    TVMFOrderByMeta,
    TVMFOrderByNone,
    TVMFOrderByVotesCount,
    TVMFOrderByCreateDate,
    TVMFOrderByName,
    TVMFOrderByStartDate,
    TVMFOrderByLikeCounter,
    TVMFOrderByRating,
    TVMFOrderByViews,
    TVMFOrderByRandom
}TVMultiFilterOrderBy;

NSString *TVNameForMultiFilterOrderBy(TVMultiFilterOrderBy orderBy);

/*!
 @abstract the available sorting direction
 */
typedef enum
{
    TVOrderDirectionByAndOrList_None=0,
    TVOrderDirectionByAndOrList_Ascending,
    TVOrderDirectionByAndOrList_Descending,
}TVOrderDirectionByAndOrList;
NSString *TVNameForOrderDirectionByAndOrList(TVOrderDirectionByAndOrList orderDirection);


/*!
 @abstract the available sorting options
 */
typedef enum
{
    TVOrderByAndOrList_NONE = 0,
    TVOrderByAndOrList_ID,
    TVOrderByAndOrList_RELATED,
    TVOrderByAndOrList_META,
    TVOrderByAndOrList_VOTES_COUNT,
    TVOrderByAndOrList_CREATE_DATE,
    TVOrderByAndOrList_NAME,
    TVOrderByAndOrList_START_DATE,
    TVOrderByAndOrList_LIKE_COUNTER,
    TVOrderByAndOrList_RATING,
    TVOrderByAndOrList_VIEWS,
    TVOrderByAndOrList_RANDOM
}TVOrderByAndOrList;
NSString *TVNameForOrderByAndOrList(TVOrderByAndOrList orderBy);

typedef enum
{
    OR = 0,
    AND
}TVCutWith;
NSString *TVNameForCutWith(TVCutWith orderBy);

/*!
 @abstract the available sorting direction
 */
typedef enum
{
    TVOrderDirectionNone=0,
    TVOrderDirectionAscending,
    TVOrderDirectionDescending,
    
}TVOrderDirection;


NSString *TVNameForOrderDirection(TVOrderDirection orderDirection);


/*!
 @abstract the available sorting direction
 */
typedef enum
{
    TVChannelMFOrderDirectionAscending = 0,
    TVChannelMFOrderDirectionDescending,
    TVChannelMFOrderDirectionNone
    
}TVChannelMFOrderDirection;


NSString *TVNameForChannelMFOrderDirection(TVChannelMFOrderDirection orderDirection);

/*
 0 = OK, 1 = UserExists, 2 = UserDoesNotExist, 3 = WrongPasswordOrUserName, 4 = InsideLockTime, 5 = NotImplementedYet, 6 = UserNotActivated, 7 = UserAllreadyLoggedIn, 8 = UserDoubleLogIn, 9 = SessionLoggedOut, 10 = DeviceNotRegistered, 11 = ErrorOnSendingMail, 12 = UserEmailAlreadyExists, 13 = ErrorOnUpdatingUserType, 14 = UserTypeNotExist ,15 = UserNotMasterApproved, 16 = ErrorOnInitUser, 17 = ErrorOnSaveUser, 18 = UserRemovedFromDomain
 
 */


// !!! pay attnetion that "TVLoginStatusCustomError" was changed from 11 to 1000 !!!;
typedef enum
{
    TVLoginStatusOK                         = 0,
    TVLoginStatusUserExists                 = 1,
    TVLoginStatusUserDoesNotExist           = 2,
    TVLoginStatusWrongPasswordOrUserName    = 3,
    TVLoginStatusInsideLockTime             = 4,
    TVLoginStatusNotImplementedYet          = 5,
    TVLoginStatusUserNotActivated           = 6,
    TVLoginStatusUserAllreadyLoggedIn       = 7,
    TVLoginStatusUserDoubleLogIn            = 8,
    TVLoginStatusSessionLoggedOut           = 9,
    TVLoginStatusDeviceNotRegistered        = 10,
    TVLoginStatusErrorOnSendingMail         = 11,
    TVLoginStatusUserEmailAlreadyExists     = 12,
    TVLoginStatusErrorOnUpdatingUserType    = 13,
    TVLoginStatusUserTypeNotExist           = 14,
    TVLoginStatusUserNotMasterApproved      = 15,
    TVLoginStatusErrorOnInitUser            = 16,
    TVLoginStatusErrorOnSaveUser            = 17,
    TVLoginStatusUserRemovedFromDomain      = 18,
    TVLoginStatusTokenNotFound              = 19,
    TVLoginStatusUserAlreadyMasterApproved  = 20,
    TVLoginStatusUserWithNoDomain           = 21,
    TVLoginStatusInternalError              = 22,
    TVLoginStatusLoginServerDown            = 23,
    TVLoginStatusUserSuspended              = 24,
    TVLoginStatusCustomError                = 1000,
}TVLoginStatus;



NSString *ErrorMessageForLoginStatus(TVLoginStatus status);

typedef enum
{
    TVUserItemTypeRental = 0,
    TVUserItemTypeSubscription,
    TVUserItemTypePackage,
    TVUserItemTypeFavorites,
    TVUserItemTypeAll,
    TVUserItemTypeForYou,
    TVUserItemTypeFriendlyActivity,
    TVUserItemTypeAngaSpecialSettings,
}TVUserItemType;

typedef enum
{
    TVMediaTypeCode_Movie = 361,
    TVMediaTypeCode_Episode = 362,
    TVMediaTypeCode_Linear=363,
    
}TVMediaTypeCode;

typedef enum{
    PreRoll=0,
    MidRoll,
    PostRoll,
    Canceled
} AdType;

NSString * TVNameForUserItemType (TVUserItemType type);


typedef enum
{
    TVNotificationTypePush,
    TVNotificationTypeAlert,
    TVNotificationTypeLive,
    TVNotificationTypePull,
    TVNotificationTypeAll,
}TVNotificationType;

NSString * TVNameForNotificationType (TVNotificationType type);


typedef enum
{
    TVNotificationViewStatusUnread,
    TVNotificationViewStatusRead,
    TVNotificationViewStatusAll,
    
}TVNotificationViewStatus;

NSString * TVNameForNotificationViewStatus (TVNotificationViewStatus type);


typedef enum
{
    TVFacebookStatusUNKNOWN,
    TVFacebookStatusOK,
    TVFacebookStatusERROR,
    TVFacebookStatusNOACTION,
    TVFacebookStatusNOTEXIST,
    TVFacebookStatusCONFLICT,
    TVFacebookStatusMERGE,
    TVFacebookStatusMERGEOK,
    TVFacebookStatusNEWUSER,
    TVFacebookStatusMINFRIENDS,
    TVFacebookStatusINVITEOK,
    TVFacebookStatusINVITEERROR,
    TVFacebookStatusACCESSDENIED,
    TVFacebookStatusWRONGPASSWORDORUSERNAME,
}TVFacebookStatus;

NSString * TVNameForFacebookStatus (TVFacebookStatus status);
TVFacebookStatus TVFacebookStatusForName (NSString * name);


typedef enum {
    TVUserSocialActionUNKNOWN = 1,
    TVUserSocialActionLIKE    = 2,
    TVUserSocialActionUNLIKE  = 4,
    TVUserSocialActionSHARE   = 8,
    TVUserSocialActionPOST    = 16,
    TVUserSocialActionWATCHES = 32,
    TVUserSocialActionWANT_TO_WATCH = 64,
    TVUserSocialActionRATES   = 128,
    TVUserSocialActionFOLLOWS = 256,
    TVUserSocialActionUNFOLLOW = 512,
}TVUserSocialActionType;

NSString * TVNameForUserAction (TVUserSocialActionType actions);


typedef enum {
    TVAssetType_UNKNOWN = 1,
    TVAssetType_MEDIA = 2,
    TVAssetType_PROGRAM = 4,
    TVAssetType_EPG = 8,
}TVAssetType;

NSString * TVNameForAssetType (TVAssetType assetType);


typedef enum  {
    TVSocialActionResponseStatus_UNKNOWN =0,
    TVSocialActionResponseStatus_OK,
    TVSocialActionResponseStatus_ERROR,
    TVSocialActionResponseStatus_UNKNOWN_ACTION,
    TVSocialActionResponseStatus_INVALID_ACCESS_TOKEN,
    TVSocialActionResponseStatus_INVALID_PLATFORM_REQUEST,
    TVSocialActionResponseStatus_MEDIA_DOESNT_EXISTS,
    TVSocialActionResponseStatus_MEDIA_ALREADY_LIKED,
    TVSocialActionResponseStatus_INVALID_PARAMETERS,
    TVSocialActionResponseStatus_USER_DOES_NOT_EXIST,
    TVSocialActionResponseStatus_NO_FB_ACTION,
    TVSocialActionResponseStatus_EMPTY_FB_OBJECT_ID,
    TVSocialActionResponseStatus_MEDIA_ALEADY_FOLLOWED,
    TVSocialActionResponseStatus_CONFIG_ERROR,
    TVSocialActionResponseStatus_MEDIA_ALREADY_RATED,
    TVSocialActionResponseStatus_NOT_ALLOWED
}TVSocialActionResponseStatus;

NSString * TVNameForSocialActionResponseStatus (TVSocialActionResponseStatus reponseStatus);



typedef enum
{
    TVRecommendationGalleryType_HomeVODPersonal,
    TVRecommendationGalleryType_HomeVODPromotions,
    TVRecommendationGalleryType_HomeLivePersonal,
    TVRecommendationGalleryType_HomeLivePromotions,
    TVRecommendationGalleryType_CatalogVODPromotions,
    TVRecommendationGalleryType_CategoryCatalogVODPromotions,
    TVRecommendationGalleryType_MovieRelated,
    TVRecommendationGalleryType_SeriesRelated,
    TVRecommendationGalleryType_EpisodeRelated,
    TVRecommendationGalleryType_PersonalRecommendationsVOD,
    TVRecommendationGalleryType_PersonalRecommendationsLive,
    TVRecommendationGalleryType_EndOfMovie,
    TVRecommendationGalleryType_EndOfEpisode,
    TVRecommendationGalleryType_VODAction,
    TVRecommendationGalleryType_VODDrama,
    TVRecommendationGalleryType_VODComedy,
    TVRecommendationGalleryType_VODThriller,
    TVRecommendationGalleryType_VODDoco,
    TVRecommendationGalleryType_VODKids,
}TVRecommendationGalleryType;

NSString * TVNameForRecommendationGalleryType (TVRecommendationGalleryType galleryType);


typedef enum
{
    TVUserActionPrivacyUNKNOWN,
    TVUserActionPrivacyEVERYONE,
    TVUserActionPrivacyALL_FRIENDS,
    TVUserActionPrivacyFRIENDS_OF_FRIENDS,
    TVUserActionPrivacySELF,
    TVUserActionPrivacyCUSTOM,
}TVUserActionPrivacy;
NSString *TVNameForSocialPrivacy(TVUserActionPrivacy privacy);

/*!
 @abstract privacy options
 */
typedef enum
{
    TVUserPrivacy_UNKNOWN,
    TVUserPrivacy_ALLOW,
    TVUserPrivacy_DONT_ALLOW,
}TVUserPrivacy;
NSString *TVNameForUserPrivacy(TVUserPrivacy privacy);


/*!
 @abstract the available sorting options
 */
typedef enum
{
    TVEPGCommentType_ALL,
    TVEPGCommentType_Useres,
    TVEPGCommentType_Reviews,
    TVEPGCommentType_News
}TVEPGCommentType;
NSString *TVNameForEPGCommentBy(TVEPGCommentType commentType);

/*!
 @abstract the available sorting options
 */
typedef enum
{
    TVListItemType_ALL,
    TVListItemType_Media
}TVListItemType;
NSString *TVNameForWatchListItemTypeBy(TVListItemType itemType);

/*!
 @abstract the available sorting options
 */
typedef enum
{
    TVListType_ALL,
    TVListType_Watch,
    TVListType_Purchase
}TVListType;
NSString *TVNameForWatchListTypeBy(TVListType listType);

/*!
 @abstract the available sorting options
 */
typedef enum
{
    EPGLicensedLinkFormatType_Catchup   = 0,
    EPGLicensedLinkFormatType_StartOver = 1,
    EPGLicensedLinkFormatType_LivePause = 2
}EPGLicensedLinkFormatType;
NSString *TVNameForEPGLicensedLinkBy(EPGLicensedLinkFormatType format);


#define kPrepaidActiveDefaultValue YES


typedef enum {
    ResetDeviceAndUserFrequency  = 0,
    ResetOnlyDeviceFrequency  = 1,
    ResetOnlyUserFrequency  = 2,

}FrequencyType;


/**
 *  This enum is describe billing type
 */
typedef enum
{
    TVBillTypeUnknown,
    TVBillTypePPV,
    TVBillTypeSubscription,
    TVBillTypePrePaid,
    TVBillTypePrePaidExpired
}TVBillType;



typedef enum
{
    TVRecordingSearchByTypeOther,
    TVRecordingSearchByTypeRecordingID,
    TVRecordingSearchByTypeStartDate,
    TVRecordingSearchByTypeRecordingStatus,
}TVRecordingSearchByType;

NSString * TVNameForRecordingSearchByType (TVRecordingSearchByType searchType);



typedef enum
{
    TVRecordingStatusCompleted,
    TVRecordingStatusOngoing,
    TVRecordingStatusScheduled,
    TVRecordingStatusCancelled,
    TVRecordingStatusUnknown,
    
}TVRecordingStatus;


NSString * TVNameForRecordingStatus (TVRecordingStatus recordStatus);
TVRecordingStatus TVRecordingStatusForString(NSString * recordingStatus);

typedef enum
{
    TVRecordOrderByStartTime,
    TVRecordOrderByName,
    TVRecordOrderByChannelID,
}TVRecordOrderBy;

NSString * TVNameForRecordOrderBy (TVRecordOrderBy recordOrderBy);


typedef enum
{
    TVRecordOrderDirectionDescending,
    TVRecordOrderDirectionAscending,
}TVRecordOrderDirection;

NSString * TVNameForRecordOrderDirection (TVRecordOrderDirection recordDirection);



typedef enum
{
    TVNPVRResponseStatusUnknown,
    TVNPVRResponseStatusOK,
    TVNPVRResponseStatusError,
    TVNPVRResponseStatusBadRequest,
    TVNPVRResponseStatusInvalidUser,
    TVNPVRResponseStatusInvalidAssetID,
    TVNPVRResponseStatusAssetAlreadyScheduled,
    TVNPVRResponseStatusAssetAlreadyCanceled,
    TVNPVRResponseStatusAssetDoesNotExist,
    TVNPVRResponseStatusAssetAlreadyRecorded,
    TVNPVRResponseStatusQuotaExceeded,
    TVNPVRResponseStatusSuspended,
    TVNPVRResponseStatusServiceNotAllowed,
    TVNPVRResponseStatusNotPurchased,
} TVNPVRResponseStatus;

TVNPVRResponseStatus TVNPVRResponseStatusForString (NSString * responseStatusString);
NSString * TVNameForNPVRResponseStatus (TVNPVRResponseStatus responseStatus);

typedef enum
{
    TVProgramIdTypeExternal,
    TVProgramIdTypeInternal,
}TVProgramIdType;

NSString * TVNameForProgramIdType (TVProgramIdType type);

typedef enum
{
    TVServiceType_PPV,
    TVServiceType_Subscription,
    TVServiceType_Collection,
    
}TVServiceType;

NSString * TVNameForServiceType (TVServiceType serviceType);

typedef enum
{
    TVResponseStatusCode_Ok =0,
    TVResponseStatusCode_Error =1,
    TVResponseStatusCode_InternalError =2,
    //// Domain Section 1000 - 1999
    TVResponseStatusCode_DomainAlreadyExists = 1000,
    TVResponseStatusCode_ExceededLimit = 1001,
    TVResponseStatusCode_DeviceTypeNotAllowed = 1002,
    TVResponseStatusCode_DeviceNotInDomain = 1003,
    TVResponseStatusCode_MasterEmailAlreadyExists = 1004,
    TVResponseStatusCode_UserNotInDomain = 1005,
    TVResponseStatusCode_DomainNotExists = 1006,
    TVResponseStatusCode_HouseholdUserFailed = 1007,
    TVResponseStatusCode_DomainCreatedWithoutNPVRAccount = 1008,
    TVResponseStatusCode_DomainSuspended = 1009,
    TVResponseStatusCode_DlmNotExist = 1010,
    TVResponseStatusCode_WrongPasswordOrUserName = 1011,
    TVResponseStatusCode_DomainAlreadySuspended = 1012,
    TVResponseStatusCode_DomainAlreadyActive = 1013,
    TVResponseStatusCode_LimitationPeriod = 1014,//
    TVResponseStatusCode_DeviceAlreadyExists = 1015,
    TVResponseStatusCode_DeviceExistsInOtherDomains = 1016,
    
    //// User Section 2000 - 2999
    TVResponseStatusCode_UserNotExists = 2000,
    TVResponseStatusCode_UserSuspended = 2001,
    
    //// CAS Section 3000 - 3999
    TVResponseStatusCode_InvalidPurchase = 3000,
    TVResponseStatusCode_CancelationWindowPeriodExpired = 3001,
    TVResponseStatusCode_SubscriptionNotRenewable = 3002,
    TVResponseStatusCode_ServiceNotAllowed = 3003,
    TVResponseStatusCode_InvalidBaseLink = 3004,
    TVResponseStatusCode_ContentAlreadyConsumed = 3005,
    
    //ResponseStatus_Catalog 4000 - 4999
    TVResponseStatusCode_MediaConcurrencyLimitation = 4000,
    TVResponseStatusCode_ConcurrencyLimitation = 4001,
    TVResponseStatusCode_BadSearchRequest = 4002,
    TVResponseStatusCode_IndexMissing = 4003,
    TVResponseStatusCode_SyntaxError = 4004,
    //InvalidSearchField = 4005
}TVResponseStatusCode;




typedef enum
{
    TVChargingResponseStatus_Success,
    TVChargingResponseStatus_Fail,
    TVChargingResponseStatus_UnKnown,
    TVChargingResponseStatus_PriceNotCorrect,
    TVChargingResponseStatus_UnKnownUser,
    TVChargingResponseStatus_UnKnownPPVModule,
    TVChargingResponseStatus_ExpiredCard,
    TVChargingResponseStatus_ExternalError,
    TVChargingResponseStatus_CellularPermissionsError,
    TVChargingResponseStatus_UnKnownBillingProvider,
    TVChargingResponseStatus_UserSuspended,
} TVChargingResponseStatus;

TVChargingResponseStatus chargingResponseStatusForString (NSString * chargingResponseStatus);

typedef enum
{
    TVUIRule_NotExist,
    TVUIRule_Disabled,
    TVUIRule_Exist,
}TVUIRule;
TVUIRule UIRuleForString (NSString * rule);
NSString * NSStringForUIRuleType (TVUIRule ruleType);


typedef enum
{
    TVUIUnitRelatedService_NPVR,
    TVUIUnitRelatedService_StartOver,
    TVUIUnitRelatedService_Cachup,
    TVUIUnitRelatedService_Companion,
    TVUIUnitRelatedService_Download,
    TVUIUnitRelatedService_Unknown,

} TVUIUnitRelatedService;


TVUIUnitRelatedService UIUnitRelatedServiceForString (NSString * relatedService);
NSString * NSStringForUIUnitRelatedServiceType (TVUIUnitRelatedService serviceType);



typedef enum
{
    TVCommercializationServiceType_CatchUp = 1,
    TVCommercializationServiceType_StartOver,
    TVCommercializationServiceType_NPVR,
    TVCommercializationServiceType_Download,
    TVCommercializationServiceType_Unknown,
}TVCommercializationServiceType;

NSString * TVNameForCommercializationServiceType (TVCommercializationServiceType serviceType);

typedef enum
{
    TVSearchOrderBy_relevancy,
    TVSearchOrderBy_a_to_z,
    TVSearchOrderBy_z_to_a,
    TVSearchOrderBy_views,
    TVSearchOrderBy_ratings,
    TVSearchOrderBy_votes,
    TVSearchOrderBy_newest,
}TVSearchOrderBy;

NSString * TVNameForSearchOrderBy (TVSearchOrderBy serviceType);




