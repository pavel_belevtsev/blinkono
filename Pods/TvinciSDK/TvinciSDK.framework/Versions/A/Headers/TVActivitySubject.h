//
//  TVActivitySubject.h
//  TvinciSDK
//
//  Created by Magen Yadid on 9/11/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
#import "TVConstants.h"


@interface TVActivitySubject : BaseModelObject


@property (strong, nonatomic) NSString  * actorSiteGuid;
@property (strong, nonatomic) NSString  * actorPicUrl;
@property (strong, nonatomic) NSString  * actorTvinciUsername;
@property (assign, nonatomic) NSInteger   groupID;
@property (strong, nonatomic) NSString  * deviceUdid;

@end
