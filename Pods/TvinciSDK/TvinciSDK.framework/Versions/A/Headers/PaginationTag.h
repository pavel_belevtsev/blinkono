//
//  PaginationTag.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 12/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//


#import <Foundation/Foundation.h>

typedef enum
{
    PaginationTagType_previuse,
    PaginationTagType_Next,
}PaginationTagType;

@interface PaginationTag : NSObject


-(id) initWithPaginationType:(PaginationTagType) paginationType pageIndex:(NSInteger) pageIndex pageSize:(NSInteger) pageSize;

@property (assign, nonatomic) PaginationTagType paginationTagType;
@property (assign, nonatomic) NSInteger pageIndex;
@property (assign, nonatomic) NSInteger pageSize;



@end
