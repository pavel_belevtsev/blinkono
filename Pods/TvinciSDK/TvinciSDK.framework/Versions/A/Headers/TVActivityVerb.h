//
//  TVActivityVerb.h
//  TvinciSDK
//
//  Created by Magen Yadid on 9/11/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
#import "TVConstants.h"


@interface TVActivityVerb : BaseModelObject

@property (strong, nonatomic) NSString * socialActionID;
@property (assign, nonatomic) TVUserSocialActionType socialAction;
@property (strong, nonatomic) NSString * actionName;
@property (assign, nonatomic) NSInteger  rateValue;
@property (strong, nonatomic) NSArray *   actionProperties;

@end
