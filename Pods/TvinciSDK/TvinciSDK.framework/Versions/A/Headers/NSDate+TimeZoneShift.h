//
//  NSDate+TimeZoneShift.h
//  TvinciSDK
//
//  Created by Quickode Ltd. on 3/5/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (TimeZoneShift)
-(NSDate *) toLocalTime;
-(NSDate *) toGlobalTime;
-(NSDate *) fromGMTtoTimeZone:(NSTimeZone*)tz;
@end
