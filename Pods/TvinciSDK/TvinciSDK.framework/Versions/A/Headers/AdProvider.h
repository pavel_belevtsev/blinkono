//
//  AdProvider.h
//  TvinciSDK
//
//  Created by iosdev1 on 7/1/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModelObject.h"
#import "TVConstants.h"


@interface AdProvider : BaseModelObject

@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, assign) AdType  adType;

-(id)initWithAdType:(AdType)type;

@end
