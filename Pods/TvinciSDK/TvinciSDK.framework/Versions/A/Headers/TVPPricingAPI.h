//
//  TVPPricingAPI.h
//  TvinciSDK
//
//  Created by Alex Zchut on 2/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseTVPClient.h"
#import "TVConstants.h"
#import "TVPPricingAPIRequestParams.h"

@interface TVPPricingAPI : BaseTVPClient

+(TVPAPIRequest *) requestForGetSubscriptionsContainingMediaFile: (void(^)(RPGetSubscriptionsContainingMediaFile *requestParams))requestParams;
+(TVPAPIRequest *) requestforGetSubscriptionIDsContainingMediaFile: (void(^)(RPGetSubscriptionIDsContainingMediaFile *requestParams))requestParams;
+(TVPAPIRequest *) requestforGetSubscriptionData: (void(^)(RPGetSubscriptionData *requestParams))requestParams;

+(TVPAPIRequest *) requestForGetPPVModuleData : (NSString *) PPVModule
                                     delegate : (id<ASIHTTPRequestDelegate>) delegate;


@end
