//
//  TVSessionManager.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/29/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVInitObject.h"
#import "BaseModelObject.h"
#import "TVDevice.h"
#import "TVIPNOperator.h"
#import "TVDomain.h"
#import "TVUser.h"
#import "TVTokenizationManager.h"

@class TVInitObject;
@class TVPAPIRequest;
@class TVUser;
@class TVTokenizationManager;


typedef  BOOL (^assertLogin) (NSDictionary * loginResponse);




typedef enum
{
    TVAuthenticationStateUnknown = 0,
    /*!
     * The state when no user is logged in. No domain info.
     */
    TVAuthenticationStateNotSignedIn,
    /*!
     * This state occures after successfull authentication. User logged in, device registered and activated.
     * You can make authenticated requests to the TVP server while in this state.
     */
    TVAuthenticationStateSignedIn,
}TVAuthenticationState;

/**********************
 TVAppType
 ----------
 - in order to know which kind of app we are using
 - used to know the type of file to use when playing movie (iPhone Main, iPad Main)
 
 
 *********************/

typedef enum
{
    TVAppType_Unknown = 0,
    TVAppType_iPad2,
    TVAppType_iPhone,
   
}TVAppType;



@interface TVSessionManager : BaseModelObject
@property (nonatomic, retain) NSMutableArray *sessionFlags; // to enable different flags for different customaers that would be available from both the SDK and the Project
@property (nonatomic, retain) NSMutableDictionary *sessionAddedData;// to enable additional  for different customaers that would be available from both the SDK and the Project

@property (nonatomic, copy)  NSDictionary  *userData;
@property (retain, readonly) TVUser *currentUser;
@property (retain, nonatomic) TVDomain *currentDomain;
@property (nonatomic, retain , readonly) TVDevice *currentDevice;
@property (nonatomic, assign, readonly) TVAuthenticationState authenticationState;
@property (assign, readonly) TVDeviceState deviceState;
@property (retain, nonatomic) NSDate * lastOpeningSessionDate;


@property (readwrite, assign) TVAppType appTypeUsed;
@property (readwrite, assign) BOOL showedSignInFailedError;
@property (copy, nonatomic)  assertLogin assertLoginBlock;
@property (nonatomic, retain) TVIPNOperator *userOperator;

// for merged users
@property (readwrite, assign) enum kPreferedLoginType lastPreferedLoginType;
@property(readwrite, assign) BOOL lastLoginIsForNewUser;

/*!
 @abstract And object that should be passed with every request to the TVP API
 */
@property (nonatomic , retain , readonly) TVInitObject *sharedInitObject;
@property (nonatomic, retain) TVTokenizationManager * tokenizationManager;




+(TVSessionManager *) sharedTVSessionManager;

/*!
 @abstract Returns if there is a domainID and SiteGUID
 */
-(BOOL) isSignedIn;

- (BOOL)isSignedInToIPNO;

// Returns wether the device is presence, registered and activated.
/*!
 @abstract Returns if there is a domainID and SiteGUID
 */
-(BOOL) isDeviceReady;

/*!
 @abstract sso sign in.
 */

-(void) ssoSignInWithUsername:(NSString *)username password:(NSString *)password;

/*!
 @abstruct sign is with enctipted password
 */
-(void) signInSecureWithUserName:(NSString *) userName securePassword:(NSString *) securePassword;

/*!
 @abstruct sign is with username & password
 */
-(void) signInWithUsername:(NSString *)username password:(NSString *)password;

/*!
 @abstruct sign is with encripted token.  For example can be after deep link action
 */
-(void) signInWithToken:(NSString *) token;

/*!
 @abstruct sign after external varification whem you have already the site guid
 */
-(void) signInAfterExternalVerificationWithSiteGuid:(NSString * ) siteGuid;


/*!
 @abstract Find if the device is inside the current domain, and what it's status.
 */
-(void) checkDeviceState;
-(void) forceCheckDeviceState;
/*!
 @abstract Register the current device in the current domain.
 */
-(void) registerDevice;

/*!
 @abstract Mark the device as activated.
 */
-(void) activateDevice;

// Remove the specified user from the list of users who have previously loggen in from this device.
//-(BOOL) removeUserFromKnownUsersList : (TVUser *) userToRemove;

/*!
 @abstract Passes the call to the TVInitObject to reset the domainID and siteGUID
 */
-(void) logout;

/*!
 @abstract logoutFromIPNO: Logout from IPNO account. And ready to login to a new IPNO account.
 */
-(void) logoutFromIPNO;

/*!
 @abstract Refreshing the InitObj Taken from configuration manager
 */
-(void) loadSharedInitObject;

-(void) setOprator:(TVIPNOperator *) tvoperator;

-(BOOL) isUserConnectedToFacebook;



// and data
-(void) addAddedDataArray:(NSArray *)data forKey:(NSString *)key;
-(NSArray *) takeAddedDataForKey:(NSString *)key;
-(BOOL) hasAddedDataForKey:(NSString *)key;


/******************
 getDomainIDFromUserDetailsWithSiteGUID
 ========================================
 
 - To enable login with remember me
 - Still Testing
 *******************/

-(void) getDomainIDFromUserDetailsWithSiteGUID : (NSString *) siteGUID;

#pragma mark - problem issues


-(void) postNotification:(NSString *)notification error:(NSError *)error;
-(void) storeUserData : (NSDictionary *)userDataDict;
-(void) storeOpeningSessionDate;
-(void) updateUserDataFromSignInResponse : (TVPAPIRequest *) loginRequest;
-(void) parseRegisterDeviceResponse : (TVPAPIRequest *) request;
-(TVDevice *)  findCurrentDeviceInCurrentDomainInALlFamiliesID;

-(void) getCurrentUserDetailsWithStartBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock;





@end

// Error Domain
extern NSString * const TVSignInErrorDomain;
extern NSString * const TVDeviceRegistrationErrorDomain;
extern NSString * const TVDeviceActivationErrorDomain;
extern NSString * const TVLogoutErrorDomain;

// Device Sign In Notification
extern NSString * const TVSessionManagerSignInCompletedNotification;
extern NSString * const TVSessionManagerSignInFailedNotification;


// Logout Notification
extern NSString * const TVSessionManagerLogoutCompletedNotification;
extern NSString * const TVSessionManagerLogoutFailedNotification;

// Device Registration Notification
extern NSString * const TVSessionManagerDeviceRegistrationCompletedNotification;
extern NSString * const TVSessionManagerDeviceRegistrationFailedNotification;
extern NSString * const TVSessionManagerDeviceRegistrationFailedWithKnownErrorNotification;

// Device Activation Notification
extern NSString * const TVSessionManagerDeviceActivationCompletedNotification;
extern NSString * const TVSessionManagerDeviceActivationFailedNotification;

extern NSString * const TVSessionManagerDeviceStateAvailableNotification;
extern NSString * const TVSessionManagerUserDetailsAvailableNotification;

extern NSString * const TVSessionManagerCurrentUserDetailsErrorNotification;

extern NSString *const TVUserDataKey;
extern NSString * const TVSignInErrorDomain;



