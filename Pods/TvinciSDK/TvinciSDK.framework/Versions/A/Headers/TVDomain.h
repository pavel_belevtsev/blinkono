//
//  TVDomain.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/10/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@interface TVDomain : BaseModelObject
@property (nonatomic , copy) NSString *domainName;
@property (nonatomic , copy) NSString *domainDescription;
@property (nonatomic , assign) NSInteger domainID;
@property (nonatomic , assign) NSInteger groupID;
@property (nonatomic , assign) NSInteger limit;
@property (nonatomic , assign) NSInteger domainStatus;
@property (nonatomic , assign) BOOL isActive;
@property (nonatomic , retain) NSArray *userIDs;
@property (nonatomic , retain) NSArray *deviceFamilies;
@property (nonatomic , retain) NSArray *masterGUIDs;
@property (nonatomic , assign) NSInteger status;

@property (nonatomic , assign) NSInteger deviceLimit;   
@property (nonatomic , assign) NSInteger userLimit;
@property (nonatomic , assign) NSInteger concurrentLimit;
@property (nonatomic , assign) NSInteger frequencyFlag;
@property (nonatomic , assign) NSInteger domainRestriction;
@property (nonatomic , assign) NSInteger ssoOperatorID;
@property (nonatomic , retain) NSDate *nextActionFreq;
@property (nonatomic , retain) NSDate *nextUserActionFreq;
@property (nonatomic , retain) NSArray *pendingUsersIDs;
@property (nonatomic , retain) NSArray *defaultUsersIDs;

@property (nonatomic, retain) NSArray * homeNetworks;
@property (nonatomic ,assign) NSInteger coGuid;

@end

extern NSString *const TVDomainKey;