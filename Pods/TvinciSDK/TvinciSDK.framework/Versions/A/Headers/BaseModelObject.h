//
//  BaseModelObject.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  This class is a base class for all the Model - Objects in this app.
 *  It containes the ability to serialize and de-serialize objects from dictionaries and vise-versa
 */
@interface BaseModelObject : NSObject

/**
 *  This initializer take a dictionary with attributes and assign them to the matching fields in the object,
 */
-(id) initWithDictionary : (NSDictionary *) dictionary;

// Returns a Key-Value representation of the object.
// Used for saving to file, or sending over the net as a JSON string.
-(NSDictionary *) keyValueRepresentation;

// This method returns an array with the names of all the attributes
// Each subclass should implement this and returns the keys that should be serialized.
-(NSArray *) attributeKeys;

// Subclasses should implement those to methods in order to enable parsing from JSON and XML
-(void) setAttributesFromDictionary : (NSDictionary *) dictionary;

#pragma mark Memory Management
-(void) didReceiveMemoryWarning;
-(void) registerForMemoryWarningNotifications;
-(void) unregisterForMemoryWarningNotifications;

#pragma mark - Notification Sending
-(void) postNotification : (NSString *) notification userInfo : (NSDictionary *) dictionary;
@end
