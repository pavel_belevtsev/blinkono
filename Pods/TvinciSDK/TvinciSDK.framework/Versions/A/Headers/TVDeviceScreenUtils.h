//
//  TVDeviceScreenUtils.h
//  TvinciSDK
//
//  Created by Aviv Alluf on 9/10/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TVDeviceScreenUtils : NSObject

+ (BOOL)isRetina;

@end
