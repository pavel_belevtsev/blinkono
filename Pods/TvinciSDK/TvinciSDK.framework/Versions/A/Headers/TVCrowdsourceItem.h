//
//  TVCrowdsourceFeed.h
//  TvinciSDK
//
//  Created by Amit Bar-Shai on 12/16/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

typedef enum  {
    kCrowdsourceType_LiveViews,
    kCrowdsourceType_SlidingWindow,
    kCrowdsourceType_Orca
}kCrowdsourceType;

@interface TVCrowdsourceItem : BaseModelObject

@property (nonatomic, copy , readonly) NSNumber *mediaID;
@property (nonatomic, strong) NSArray *mediaImages;
@property (nonatomic, copy) NSString *mediaName;
@property (nonatomic, assign) NSInteger order;
@property (nonatomic, assign) NSInteger timestamp;
@property (nonatomic, assign) kCrowdsourceType type;
@property (nonatomic, retain) NSString * typeDescription;


+ (TVCrowdsourceItem *)crowdsourceItemWithDictionary:(NSDictionary *)dictionary;

@end
