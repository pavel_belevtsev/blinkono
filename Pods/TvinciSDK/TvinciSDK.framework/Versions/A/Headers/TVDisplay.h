//
//  TVDisplay.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@interface TVDisplay : BaseModelObject

@property (retain, nonatomic) NSURL * displayLogoURL;
@property (copy, nonatomic) NSString * displayColor;
@property (copy, nonatomic) NSString * displayTextColor;
@property (assign, nonatomic) NSInteger  operatorID;




@end
