//
//  TVCompanionHandler.h
//  TvinciSDK
//
//  Created by Tarek Issa on 4/9/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TVCompanionDevice;
@class TVMediaItem;

typedef enum {
    TVCompanionSwoosh_Success = 0,
    TVCompanionSwoosh_Fail	,
    TVCompanionSwoosh_UknownFormat
} TVCompanionSwooshResult;

@interface TVCompanionHandler : NSObject


/*!
 *  Retrns an array of TVCompanionDevice's from 'devicesArray'  and 'usidsArray', that have in common.
 *
 *  @param devicesArray         - Array of  TVCompanionDevice's.
 *  @param deviceFamilyArray    - Array of  TVDeviceFamily.
 *******************************************************************************/
+ (NSArray *)crossDevicesArray:(NSArray *)devicesArray withUDIDsArray:(NSArray *)deviceFamilyArray;

/**********
 * sendMediaToSTB...: Get request to send STB an http command.
 *
 * @param   stbDevice       The STB device object
 * @param   mediaId         TVMendiaItem property mediaID
 * @param   seconds         media mark rate in seconds
 * @param   siteGuid        tvinci's user's site_Guid id
 * @param   EpgProgramID    program id (optional)
 * @param   fileId          TVFile fileID
 * @param   port            The port to be sent through
 * @param   playbackTimeSeconds    last watch playback, to make it the same "last watch" after the swoosh
 * @param   domainId        TVDomain domainId
 * @param   play_time       Where to start the playback from, in seconds.
 * @param   playback_type    String that indecates weather the stream is live or recorded. Use consts
 TVCompanionItemType_Live TVCompanionItemType_VOD
 *
 * @param   itemType    String that indecates type of PLTV mode. Use consts TVCompanionPlaybackType_StartOver
 TVCompanionPlaybackType_CatchUp TVCompanionPlaybackType_PLTV
 *
 *
 * @return  {@link TVPAPIRequest}
 ********/
+ (void)sendMediaToSTB : (TVCompanionDevice *)stbDevice
               mediaID : (NSString *)mediaID
     withMediaMarkRate : (NSInteger)seconds
              siteGuid : (NSString *)siteGuid
         playback_type : (NSString *)playback_type
          epgProgramId : (NSString *)epgProgramId
              itemType : (NSString *)itemType
                fileId : (NSString *)fileId
              domainID : (NSInteger)domainId
          doRequestPin : (BOOL)requestPin
   withCompletionBlock : (void (^)(TVCompanionSwooshResult SwooshResult)) completion;


+ (void)sendMediaToSTB : (TVCompanionDevice *)stbDevice
               mediaID : (NSString *)mediaID
     withMediaMarkRate : (NSInteger)seconds
         playback_type : (NSString *)playback_type
          epgProgramId : (NSString *)epgProgramId
              itemType : (NSString *)itemType
                fileId : (NSString *)fileId
             play_time : (NSInteger)play_time
          doRequestPin : (BOOL)requestPin
   withCompletionBlock : (void (^)(TVCompanionSwooshResult SwooshResult)) completion;

+ (void)sendMediaToNetgemSTB : (TVCompanionDevice *)stbDevice
                     mediaID : (NSString *)mediaID
                    mediaUrl : (NSString *)mediaUrl
           withMediaMarkRate : (NSInteger)seconds
                    siteGuid : (NSString *)siteGuid
               playback_type : (NSString *)playback_type
                epgProgramId : (NSString *)epgProgramId
                    itemType : (NSString *)itemType
                      fileId : (NSString *)fileId
                    domainID : (NSInteger)domainId
                doRequestPin : (BOOL)requestPin
         withCompletionBlock : (void (^)(TVCompanionSwooshResult SwooshResult)) completion;
@end
